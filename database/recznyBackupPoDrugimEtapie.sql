-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Czas generowania: 04 Gru 2018, 19:34
-- Wersja serwera: 10.3.11-MariaDB-1:10.3.11+maria~bionic
-- Wersja PHP: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `prestashop`
--
CREATE DATABASE IF NOT EXISTS `prestashop` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `prestashop`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_access`
--

DROP TABLE IF EXISTS `ps_access`;
CREATE TABLE `ps_access` (
  `id_profile` int(10) UNSIGNED NOT NULL,
  `id_tab` int(10) UNSIGNED NOT NULL,
  `view` int(11) NOT NULL,
  `add` int(11) NOT NULL,
  `edit` int(11) NOT NULL,
  `delete` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_access`
--

INSERT INTO `ps_access` (`id_profile`, `id_tab`, `view`, `add`, `edit`, `delete`) VALUES
(1, 0, 1, 1, 1, 1),
(1, 1, 1, 1, 1, 1),
(1, 5, 1, 1, 1, 1),
(1, 7, 1, 1, 1, 1),
(1, 9, 1, 1, 1, 1),
(1, 10, 1, 1, 1, 1),
(1, 11, 1, 1, 1, 1),
(1, 13, 1, 1, 1, 1),
(1, 14, 1, 1, 1, 1),
(1, 15, 1, 1, 1, 1),
(1, 16, 1, 1, 1, 1),
(1, 19, 1, 1, 1, 1),
(1, 20, 1, 1, 1, 1),
(1, 21, 1, 1, 1, 1),
(1, 22, 1, 1, 1, 1),
(1, 23, 1, 1, 1, 1),
(1, 24, 1, 1, 1, 1),
(1, 25, 1, 1, 1, 1),
(1, 26, 1, 1, 1, 1),
(1, 27, 1, 1, 1, 1),
(1, 28, 1, 1, 1, 1),
(1, 29, 1, 1, 1, 1),
(1, 31, 1, 1, 1, 1),
(1, 32, 1, 1, 1, 1),
(1, 33, 1, 1, 1, 1),
(1, 34, 1, 1, 1, 1),
(1, 35, 1, 1, 1, 1),
(1, 36, 1, 1, 1, 1),
(1, 38, 1, 1, 1, 1),
(1, 39, 1, 1, 1, 1),
(1, 40, 1, 1, 1, 1),
(1, 41, 1, 1, 1, 1),
(1, 42, 1, 1, 1, 1),
(1, 44, 1, 1, 1, 1),
(1, 45, 1, 1, 1, 1),
(1, 48, 1, 1, 1, 1),
(1, 50, 1, 1, 1, 1),
(1, 52, 1, 1, 1, 1),
(1, 53, 1, 1, 1, 1),
(1, 54, 1, 1, 1, 1),
(1, 55, 1, 1, 1, 1),
(1, 56, 1, 1, 1, 1),
(1, 57, 1, 1, 1, 1),
(1, 58, 1, 1, 1, 1),
(1, 59, 1, 1, 1, 1),
(1, 61, 1, 1, 1, 1),
(1, 62, 1, 1, 1, 1),
(1, 63, 1, 1, 1, 1),
(1, 66, 1, 1, 1, 1),
(1, 67, 1, 1, 1, 1),
(1, 68, 1, 1, 1, 1),
(1, 69, 1, 1, 1, 1),
(1, 70, 1, 1, 1, 1),
(1, 71, 1, 1, 1, 1),
(1, 73, 1, 1, 1, 1),
(1, 74, 1, 1, 1, 1),
(1, 75, 1, 1, 1, 1),
(1, 76, 1, 1, 1, 1),
(1, 77, 1, 1, 1, 1),
(1, 78, 1, 1, 1, 1),
(1, 80, 1, 1, 1, 1),
(1, 81, 1, 1, 1, 1),
(1, 82, 1, 1, 1, 1),
(1, 83, 1, 1, 1, 1),
(1, 84, 1, 1, 1, 1),
(1, 86, 1, 1, 1, 1),
(1, 87, 1, 1, 1, 1),
(1, 88, 1, 1, 1, 1),
(1, 89, 1, 1, 1, 1),
(1, 92, 1, 1, 1, 1),
(1, 93, 1, 1, 1, 1),
(1, 94, 1, 1, 1, 1),
(1, 95, 1, 1, 1, 1),
(1, 96, 1, 1, 1, 1),
(1, 99, 1, 1, 1, 1),
(1, 100, 1, 1, 1, 1),
(1, 101, 1, 1, 1, 1),
(1, 102, 1, 1, 1, 1),
(1, 103, 1, 1, 1, 1),
(1, 104, 1, 1, 1, 1),
(1, 105, 1, 1, 1, 1),
(1, 106, 1, 1, 1, 1),
(2, 0, 1, 1, 1, 1),
(2, 1, 0, 0, 0, 0),
(2, 2, 0, 0, 0, 0),
(2, 3, 0, 0, 0, 0),
(2, 4, 0, 0, 0, 0),
(2, 5, 0, 0, 0, 0),
(2, 6, 0, 0, 0, 0),
(2, 7, 0, 0, 0, 0),
(2, 8, 0, 0, 0, 0),
(2, 9, 1, 1, 1, 1),
(2, 10, 1, 1, 1, 1),
(2, 11, 1, 1, 1, 1),
(2, 12, 0, 0, 0, 0),
(2, 13, 1, 0, 1, 0),
(2, 14, 1, 1, 1, 1),
(2, 15, 0, 0, 0, 0),
(2, 16, 0, 0, 0, 0),
(2, 17, 0, 0, 0, 0),
(2, 18, 0, 0, 0, 0),
(2, 19, 0, 0, 0, 0),
(2, 20, 1, 1, 1, 1),
(2, 21, 1, 1, 1, 1),
(2, 22, 1, 1, 1, 1),
(2, 23, 1, 1, 1, 1),
(2, 24, 0, 0, 0, 0),
(2, 25, 0, 0, 0, 0),
(2, 26, 0, 0, 0, 0),
(2, 27, 1, 1, 1, 1),
(2, 28, 0, 0, 0, 0),
(2, 29, 0, 0, 0, 0),
(2, 30, 1, 1, 1, 1),
(2, 31, 1, 1, 1, 1),
(2, 32, 1, 1, 1, 1),
(2, 33, 1, 1, 1, 1),
(2, 34, 1, 1, 1, 1),
(2, 35, 1, 1, 1, 1),
(2, 36, 0, 0, 0, 0),
(2, 37, 1, 1, 1, 1),
(2, 38, 1, 1, 1, 1),
(2, 39, 0, 0, 0, 0),
(2, 40, 0, 0, 0, 0),
(2, 41, 0, 0, 0, 0),
(2, 42, 0, 0, 0, 0),
(2, 43, 0, 0, 0, 0),
(2, 44, 0, 0, 0, 0),
(2, 45, 0, 0, 0, 0),
(2, 46, 0, 0, 0, 0),
(2, 47, 0, 0, 0, 0),
(2, 48, 1, 1, 1, 1),
(2, 49, 1, 1, 1, 1),
(2, 50, 0, 0, 0, 0),
(2, 51, 0, 0, 0, 0),
(2, 52, 0, 0, 0, 0),
(2, 53, 0, 0, 0, 0),
(2, 54, 0, 0, 0, 0),
(2, 55, 0, 0, 0, 0),
(2, 56, 0, 0, 0, 0),
(2, 57, 0, 0, 0, 0),
(2, 58, 0, 0, 0, 0),
(2, 59, 0, 0, 0, 0),
(2, 60, 1, 0, 1, 0),
(2, 61, 0, 0, 0, 0),
(2, 62, 0, 0, 0, 0),
(2, 63, 0, 0, 0, 0),
(2, 64, 0, 0, 0, 0),
(2, 65, 0, 0, 0, 0),
(2, 66, 0, 0, 0, 0),
(2, 67, 0, 0, 0, 0),
(2, 68, 0, 0, 0, 0),
(2, 69, 0, 0, 0, 0),
(2, 70, 0, 0, 0, 0),
(2, 71, 0, 0, 0, 0),
(2, 72, 0, 0, 0, 0),
(2, 73, 0, 0, 0, 0),
(2, 74, 0, 0, 0, 0),
(2, 75, 0, 0, 0, 0),
(2, 76, 0, 0, 0, 0),
(2, 77, 0, 0, 0, 0),
(2, 78, 0, 0, 0, 0),
(2, 79, 0, 0, 0, 0),
(2, 80, 0, 0, 0, 0),
(2, 81, 0, 0, 0, 0),
(2, 82, 0, 0, 0, 0),
(2, 83, 0, 0, 0, 0),
(2, 84, 0, 0, 0, 0),
(2, 85, 0, 0, 0, 0),
(2, 86, 0, 0, 0, 0),
(2, 87, 0, 0, 0, 0),
(2, 88, 0, 0, 0, 0),
(2, 89, 0, 0, 0, 0),
(2, 90, 0, 0, 0, 0),
(2, 91, 0, 0, 0, 0),
(2, 92, 0, 0, 0, 0),
(2, 93, 0, 0, 0, 0),
(2, 94, 1, 1, 1, 1),
(2, 95, 1, 1, 1, 1),
(2, 96, 1, 1, 1, 1),
(2, 97, 0, 0, 0, 0),
(2, 98, 0, 0, 0, 0),
(2, 99, 1, 1, 1, 1),
(2, 100, 1, 1, 1, 1),
(2, 101, 0, 0, 0, 0),
(2, 102, 0, 0, 0, 0),
(2, 103, 0, 0, 0, 0),
(2, 104, 0, 0, 0, 0),
(2, 105, 0, 0, 0, 0),
(2, 106, 0, 0, 0, 0),
(3, 0, 1, 1, 1, 1),
(3, 1, 0, 0, 0, 0),
(3, 2, 0, 0, 0, 0),
(3, 3, 0, 0, 0, 0),
(3, 4, 0, 0, 0, 0),
(3, 5, 1, 0, 0, 0),
(3, 6, 0, 0, 0, 0),
(3, 7, 0, 0, 0, 0),
(3, 8, 0, 0, 0, 0),
(3, 9, 1, 1, 1, 1),
(3, 10, 0, 0, 0, 0),
(3, 11, 0, 0, 0, 0),
(3, 12, 0, 0, 0, 0),
(3, 13, 0, 0, 0, 0),
(3, 14, 0, 0, 0, 0),
(3, 15, 1, 0, 0, 0),
(3, 16, 1, 0, 0, 0),
(3, 17, 0, 0, 0, 0),
(3, 18, 0, 0, 0, 0),
(3, 19, 0, 0, 0, 0),
(3, 20, 0, 0, 0, 0),
(3, 21, 1, 1, 1, 1),
(3, 22, 1, 1, 1, 1),
(3, 23, 0, 0, 0, 0),
(3, 24, 0, 0, 0, 0),
(3, 25, 0, 0, 0, 0),
(3, 26, 0, 0, 0, 0),
(3, 27, 0, 0, 0, 0),
(3, 28, 0, 0, 0, 0),
(3, 29, 0, 0, 0, 0),
(3, 30, 0, 0, 0, 0),
(3, 31, 0, 0, 0, 0),
(3, 32, 0, 0, 0, 0),
(3, 33, 0, 0, 0, 0),
(3, 34, 0, 0, 0, 0),
(3, 35, 0, 0, 0, 0),
(3, 36, 0, 0, 0, 0),
(3, 37, 0, 0, 0, 0),
(3, 38, 0, 0, 0, 0),
(3, 39, 0, 0, 0, 0),
(3, 40, 0, 0, 0, 0),
(3, 41, 0, 0, 0, 0),
(3, 42, 0, 0, 0, 0),
(3, 43, 0, 0, 0, 0),
(3, 44, 0, 0, 0, 0),
(3, 45, 0, 0, 0, 0),
(3, 46, 0, 0, 0, 0),
(3, 47, 0, 0, 0, 0),
(3, 48, 0, 0, 0, 0),
(3, 49, 0, 0, 0, 0),
(3, 50, 0, 0, 0, 0),
(3, 51, 0, 0, 0, 0),
(3, 52, 0, 0, 0, 0),
(3, 53, 0, 0, 0, 0),
(3, 54, 0, 0, 0, 0),
(3, 55, 0, 0, 0, 0),
(3, 56, 0, 0, 0, 0),
(3, 57, 0, 0, 0, 0),
(3, 58, 0, 0, 0, 0),
(3, 59, 1, 1, 1, 1),
(3, 60, 0, 0, 0, 0),
(3, 61, 0, 0, 0, 0),
(3, 62, 0, 0, 0, 0),
(3, 63, 0, 0, 0, 0),
(3, 64, 0, 0, 0, 0),
(3, 65, 0, 0, 0, 0),
(3, 66, 0, 0, 0, 0),
(3, 67, 0, 0, 0, 0),
(3, 68, 0, 0, 0, 0),
(3, 69, 0, 0, 0, 0),
(3, 70, 1, 1, 1, 1),
(3, 71, 0, 0, 0, 0),
(3, 72, 0, 0, 0, 0),
(3, 73, 0, 0, 0, 0),
(3, 74, 0, 0, 0, 0),
(3, 75, 0, 0, 0, 0),
(3, 76, 0, 0, 0, 0),
(3, 77, 0, 0, 0, 0),
(3, 78, 0, 0, 0, 0),
(3, 79, 0, 0, 0, 0),
(3, 80, 0, 0, 0, 0),
(3, 81, 0, 0, 0, 0),
(3, 82, 0, 0, 0, 0),
(3, 83, 0, 0, 0, 0),
(3, 84, 0, 0, 0, 0),
(3, 85, 0, 0, 0, 0),
(3, 86, 0, 0, 0, 0),
(3, 87, 0, 0, 0, 0),
(3, 88, 0, 0, 0, 0),
(3, 89, 0, 0, 0, 0),
(3, 90, 0, 0, 0, 0),
(3, 91, 0, 0, 0, 0),
(3, 92, 0, 0, 0, 0),
(3, 93, 0, 0, 0, 0),
(3, 94, 0, 0, 0, 0),
(3, 95, 0, 0, 0, 0),
(3, 96, 0, 0, 0, 0),
(3, 97, 0, 0, 0, 0),
(3, 98, 0, 0, 0, 0),
(3, 99, 0, 0, 0, 0),
(3, 100, 0, 0, 0, 0),
(3, 101, 0, 0, 0, 0),
(3, 102, 0, 0, 0, 0),
(3, 103, 0, 0, 0, 0),
(3, 104, 0, 0, 0, 0),
(3, 105, 0, 0, 0, 0),
(3, 106, 0, 0, 0, 0),
(4, 0, 1, 1, 1, 1),
(4, 1, 0, 0, 0, 0),
(4, 2, 0, 0, 0, 0),
(4, 3, 0, 0, 0, 0),
(4, 4, 0, 0, 0, 0),
(4, 5, 1, 0, 0, 0),
(4, 6, 0, 0, 0, 0),
(4, 7, 0, 0, 0, 0),
(4, 8, 0, 0, 0, 0),
(4, 9, 1, 1, 1, 1),
(4, 10, 1, 1, 1, 1),
(4, 11, 1, 1, 1, 1),
(4, 12, 0, 0, 0, 0),
(4, 13, 1, 0, 1, 0),
(4, 14, 0, 0, 0, 0),
(4, 15, 0, 0, 0, 0),
(4, 16, 0, 0, 0, 0),
(4, 17, 0, 0, 0, 0),
(4, 18, 0, 0, 0, 0),
(4, 19, 1, 1, 1, 1),
(4, 20, 1, 0, 0, 0),
(4, 21, 1, 1, 1, 1),
(4, 22, 1, 1, 1, 1),
(4, 23, 0, 0, 0, 0),
(4, 24, 0, 0, 0, 0),
(4, 25, 0, 0, 0, 0),
(4, 26, 1, 0, 0, 0),
(4, 27, 0, 0, 0, 0),
(4, 28, 0, 0, 0, 0),
(4, 29, 0, 0, 0, 0),
(4, 30, 1, 1, 1, 1),
(4, 31, 1, 1, 1, 1),
(4, 32, 0, 0, 0, 0),
(4, 33, 0, 0, 0, 0),
(4, 34, 1, 1, 1, 1),
(4, 35, 0, 0, 0, 0),
(4, 36, 1, 1, 1, 1),
(4, 37, 1, 1, 1, 1),
(4, 38, 1, 1, 1, 1),
(4, 39, 1, 1, 1, 1),
(4, 40, 1, 1, 1, 1),
(4, 41, 0, 0, 0, 0),
(4, 42, 0, 0, 0, 0),
(4, 43, 0, 0, 0, 0),
(4, 44, 0, 0, 0, 0),
(4, 45, 0, 0, 0, 0),
(4, 46, 0, 0, 0, 0),
(4, 47, 0, 0, 0, 0),
(4, 48, 0, 0, 0, 0),
(4, 49, 0, 0, 0, 0),
(4, 50, 0, 0, 0, 0),
(4, 51, 0, 0, 0, 0),
(4, 52, 0, 0, 0, 0),
(4, 53, 0, 0, 0, 0),
(4, 54, 0, 0, 0, 0),
(4, 55, 0, 0, 0, 0),
(4, 56, 0, 0, 0, 0),
(4, 57, 0, 0, 0, 0),
(4, 58, 0, 0, 0, 0),
(4, 59, 0, 0, 0, 0),
(4, 60, 1, 0, 1, 0),
(4, 61, 0, 0, 0, 0),
(4, 62, 0, 0, 0, 0),
(4, 63, 0, 0, 0, 0),
(4, 64, 0, 0, 0, 0),
(4, 65, 0, 0, 0, 0),
(4, 66, 0, 0, 0, 0),
(4, 67, 0, 0, 0, 0),
(4, 68, 0, 0, 0, 0),
(4, 69, 0, 0, 0, 0),
(4, 70, 0, 0, 0, 0),
(4, 71, 0, 0, 0, 0),
(4, 72, 0, 0, 0, 0),
(4, 73, 0, 0, 0, 0),
(4, 74, 0, 0, 0, 0),
(4, 75, 0, 0, 0, 0),
(4, 76, 0, 0, 0, 0),
(4, 77, 0, 0, 0, 0),
(4, 78, 0, 0, 0, 0),
(4, 79, 0, 0, 0, 0),
(4, 80, 0, 0, 0, 0),
(4, 81, 0, 0, 0, 0),
(4, 82, 0, 0, 0, 0),
(4, 83, 0, 0, 0, 0),
(4, 84, 1, 1, 1, 1),
(4, 85, 0, 0, 0, 0),
(4, 86, 0, 0, 0, 0),
(4, 87, 0, 0, 0, 0),
(4, 88, 0, 0, 0, 0),
(4, 89, 0, 0, 0, 0),
(4, 90, 0, 0, 0, 0),
(4, 91, 1, 1, 1, 1),
(4, 92, 0, 0, 0, 0),
(4, 93, 1, 1, 1, 1),
(4, 94, 0, 0, 0, 0),
(4, 95, 0, 0, 0, 0),
(4, 96, 0, 0, 0, 0),
(4, 97, 0, 0, 0, 0),
(4, 98, 0, 0, 0, 0),
(4, 99, 1, 0, 0, 0),
(4, 100, 0, 0, 0, 0),
(4, 101, 0, 0, 0, 0),
(4, 102, 0, 0, 0, 0),
(4, 103, 0, 0, 0, 0),
(4, 104, 0, 0, 0, 0),
(4, 105, 0, 0, 0, 0),
(4, 106, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_accessory`
--

DROP TABLE IF EXISTS `ps_accessory`;
CREATE TABLE `ps_accessory` (
  `id_product_1` int(10) UNSIGNED NOT NULL,
  `id_product_2` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_address`
--

DROP TABLE IF EXISTS `ps_address`;
CREATE TABLE `ps_address` (
  `id_address` int(10) UNSIGNED NOT NULL,
  `id_country` int(10) UNSIGNED NOT NULL,
  `id_state` int(10) UNSIGNED DEFAULT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `id_manufacturer` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `id_supplier` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `id_warehouse` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `alias` varchar(32) NOT NULL,
  `company` varchar(255) DEFAULT NULL,
  `lastname` varchar(32) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `address1` varchar(128) NOT NULL,
  `address2` varchar(128) DEFAULT NULL,
  `postcode` varchar(12) DEFAULT NULL,
  `city` varchar(64) NOT NULL,
  `other` text DEFAULT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `phone_mobile` varchar(32) DEFAULT NULL,
  `vat_number` varchar(32) DEFAULT NULL,
  `dni` varchar(16) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_address`
--

INSERT INTO `ps_address` (`id_address`, `id_country`, `id_state`, `id_customer`, `id_manufacturer`, `id_supplier`, `id_warehouse`, `alias`, `company`, `lastname`, `firstname`, `address1`, `address2`, `postcode`, `city`, `other`, `phone`, `phone_mobile`, `vat_number`, `dni`, `date_add`, `date_upd`, `active`, `deleted`) VALUES
(1, 8, 0, 1, 0, 0, 0, 'Mon adresse', 'My Company', 'DOE', 'John', '16, Main street', '2nd floor', '75002', 'Paris ', '', '0102030405', '', '', '', '2018-12-03 17:18:12', '2018-12-03 17:18:12', 1, 0),
(2, 21, 32, 0, 0, 1, 0, 'supplier', 'Fashion', 'supplier', 'supplier', '767 Fifth Ave.', '', '10153', 'New York', '', '(212) 336-1440', '', '', '', '2018-12-03 17:18:12', '2018-12-03 23:46:39', 1, 1),
(3, 21, 32, 0, 1, 0, 0, 'manufacturer', 'Fashion', 'manufacturer', 'manufacturer', '767 Fifth Ave.', '', '10154', 'New York', '', '(212) 336-1666', '', '', '', '2018-12-03 17:18:12', '2018-12-03 23:46:31', 1, 1),
(4, 21, 9, 1, 0, 0, 0, 'My address', 'My Company', 'DOE', 'John', '16, Main street', '2nd floor', '33133', 'Miami', '', '0102030405', '', '', '', '2018-12-03 17:18:12', '2018-12-03 17:18:12', 1, 0),
(5, 14, 0, 2, 0, 0, 0, 'Mój adres', '', 'FDSFSF', 'Asdad', 'SDSSFDS', '', '80-188', 'gggggg', '', '43243423423', '32131231', '', '', '2018-12-04 12:10:58', '2018-12-04 12:10:58', 1, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_address_format`
--

DROP TABLE IF EXISTS `ps_address_format`;
CREATE TABLE `ps_address_format` (
  `id_country` int(10) UNSIGNED NOT NULL,
  `format` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_address_format`
--

INSERT INTO `ps_address_format` (`id_country`, `format`) VALUES
(1, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(2, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(3, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(4, 'firstname lastname\ncompany\naddress1\naddress2\ncity State:name postcode\nCountry:name\nphone\nphone_mobile'),
(5, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(6, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(7, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(8, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(9, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(10, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nState:name\nCountry:name\nphone\nphone_mobile'),
(11, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nState:name\nCountry:name\nphone\nphone_mobile'),
(12, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(13, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(14, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(15, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(16, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(17, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\ncity\npostcode\nCountry:name\nphone\nphone_mobile'),
(18, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(19, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(20, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(21, 'firstname lastname\ncompany\naddress1 address2\ncity, State:name postcode\nCountry:name\nphone\nphone_mobile'),
(22, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(23, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(24, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(25, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(26, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(27, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(28, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(29, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(30, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(31, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(32, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(33, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(34, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(35, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(36, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(37, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(38, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(39, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(40, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(41, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(42, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(43, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(44, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nState:name\nCountry:name\nphone\nphone_mobile'),
(45, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(46, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(47, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(48, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(49, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(50, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(51, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(52, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(53, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(54, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(55, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(56, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(57, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(58, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(59, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(60, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(61, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(62, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(63, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(64, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(65, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(66, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(67, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(68, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(69, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(70, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(71, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(72, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(73, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(74, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(75, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(76, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(77, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(78, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(79, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(80, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(81, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(82, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(83, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(84, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(85, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(86, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(87, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(88, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(89, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(90, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(91, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(92, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(93, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(94, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(95, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(96, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(97, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(98, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(99, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(100, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(101, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(102, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(103, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(104, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(105, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(106, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(107, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(108, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(109, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(110, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(111, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nState:name\nCountry:name\nphone\nphone_mobile'),
(112, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(113, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(114, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(115, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(116, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(117, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(118, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(119, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(120, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(121, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(122, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(123, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(124, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(125, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(126, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(127, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(128, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(129, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(130, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(131, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(132, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(133, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(134, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(135, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(136, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(137, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(138, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(139, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(140, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(141, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(142, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(143, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(144, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(145, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nState:name\nCountry:name\nphone\nphone_mobile'),
(146, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(147, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(148, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(149, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(150, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(151, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(152, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(153, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(154, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(155, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(156, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(157, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(158, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(159, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(160, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(161, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(162, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(163, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(164, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(165, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(166, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(167, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(168, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(169, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(170, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(171, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(172, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(173, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(174, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(175, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(176, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(177, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(178, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(179, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(180, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(181, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(182, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(183, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(184, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(185, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(186, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(187, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(188, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(189, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(190, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(191, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(192, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(193, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(194, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(195, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(196, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(197, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(198, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(199, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(200, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(201, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(202, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(203, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(204, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(205, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(206, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(207, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(208, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(209, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(210, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(211, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(212, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(213, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(214, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(215, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(216, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(217, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(218, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(219, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(220, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(221, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(222, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(223, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(224, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(225, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(226, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(227, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(228, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(229, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(230, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(231, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(232, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(233, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(234, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(235, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(236, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(237, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(238, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(239, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(240, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(241, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(242, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(243, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile'),
(244, 'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone\nphone_mobile');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_advice`
--

DROP TABLE IF EXISTS `ps_advice`;
CREATE TABLE `ps_advice` (
  `id_advice` int(11) NOT NULL,
  `id_ps_advice` int(11) NOT NULL,
  `id_tab` int(11) NOT NULL,
  `ids_tab` text DEFAULT NULL,
  `validated` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `hide` tinyint(1) NOT NULL DEFAULT 0,
  `location` enum('after','before') NOT NULL,
  `selector` varchar(255) DEFAULT NULL,
  `start_day` int(11) NOT NULL DEFAULT 0,
  `stop_day` int(11) NOT NULL DEFAULT 0,
  `weight` int(11) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_advice_lang`
--

DROP TABLE IF EXISTS `ps_advice_lang`;
CREATE TABLE `ps_advice_lang` (
  `id_advice` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `html` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_alias`
--

DROP TABLE IF EXISTS `ps_alias`;
CREATE TABLE `ps_alias` (
  `id_alias` int(10) UNSIGNED NOT NULL,
  `alias` varchar(255) NOT NULL,
  `search` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_attachment`
--

DROP TABLE IF EXISTS `ps_attachment`;
CREATE TABLE `ps_attachment` (
  `id_attachment` int(10) UNSIGNED NOT NULL,
  `file` varchar(40) NOT NULL,
  `file_name` varchar(128) NOT NULL,
  `file_size` bigint(10) UNSIGNED NOT NULL DEFAULT 0,
  `mime` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_attachment_lang`
--

DROP TABLE IF EXISTS `ps_attachment_lang`;
CREATE TABLE `ps_attachment_lang` (
  `id_attachment` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_attribute`
--

DROP TABLE IF EXISTS `ps_attribute`;
CREATE TABLE `ps_attribute` (
  `id_attribute` int(10) UNSIGNED NOT NULL,
  `id_attribute_group` int(10) UNSIGNED NOT NULL,
  `color` varchar(32) DEFAULT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_attribute_group`
--

DROP TABLE IF EXISTS `ps_attribute_group`;
CREATE TABLE `ps_attribute_group` (
  `id_attribute_group` int(10) UNSIGNED NOT NULL,
  `is_color_group` tinyint(1) NOT NULL DEFAULT 0,
  `group_type` enum('select','radio','color') NOT NULL DEFAULT 'select',
  `position` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_attribute_group_lang`
--

DROP TABLE IF EXISTS `ps_attribute_group_lang`;
CREATE TABLE `ps_attribute_group_lang` (
  `id_attribute_group` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL,
  `public_name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_attribute_group_shop`
--

DROP TABLE IF EXISTS `ps_attribute_group_shop`;
CREATE TABLE `ps_attribute_group_shop` (
  `id_attribute_group` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_attribute_impact`
--

DROP TABLE IF EXISTS `ps_attribute_impact`;
CREATE TABLE `ps_attribute_impact` (
  `id_attribute_impact` int(10) UNSIGNED NOT NULL,
  `id_product` int(11) UNSIGNED NOT NULL,
  `id_attribute` int(11) UNSIGNED NOT NULL,
  `weight` decimal(20,6) NOT NULL,
  `price` decimal(17,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_attribute_lang`
--

DROP TABLE IF EXISTS `ps_attribute_lang`;
CREATE TABLE `ps_attribute_lang` (
  `id_attribute` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_attribute_shop`
--

DROP TABLE IF EXISTS `ps_attribute_shop`;
CREATE TABLE `ps_attribute_shop` (
  `id_attribute` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_badge`
--

DROP TABLE IF EXISTS `ps_badge`;
CREATE TABLE `ps_badge` (
  `id_badge` int(11) NOT NULL,
  `id_ps_badge` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `id_group` int(11) NOT NULL,
  `group_position` int(11) NOT NULL,
  `scoring` int(11) NOT NULL,
  `awb` int(11) DEFAULT 0,
  `validated` tinyint(1) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_badge_lang`
--

DROP TABLE IF EXISTS `ps_badge_lang`;
CREATE TABLE `ps_badge_lang` (
  `id_badge` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `group_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_carrier`
--

DROP TABLE IF EXISTS `ps_carrier`;
CREATE TABLE `ps_carrier` (
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `id_reference` int(10) UNSIGNED NOT NULL,
  `id_tax_rules_group` int(10) UNSIGNED DEFAULT 0,
  `name` varchar(64) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `shipping_handling` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `range_behavior` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `is_module` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `is_free` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `shipping_external` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `need_range` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `external_module_name` varchar(64) DEFAULT NULL,
  `shipping_method` int(2) NOT NULL DEFAULT 0,
  `position` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `max_width` int(10) DEFAULT 0,
  `max_height` int(10) DEFAULT 0,
  `max_depth` int(10) DEFAULT 0,
  `max_weight` decimal(20,6) DEFAULT 0.000000,
  `grade` int(10) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_carrier`
--

INSERT INTO `ps_carrier` (`id_carrier`, `id_reference`, `id_tax_rules_group`, `name`, `url`, `active`, `deleted`, `shipping_handling`, `range_behavior`, `is_module`, `is_free`, `shipping_external`, `need_range`, `external_module_name`, `shipping_method`, `position`, `max_width`, `max_height`, `max_depth`, `max_weight`, `grade`) VALUES
(1, 1, 0, '0', '', 1, 0, 0, 0, 0, 1, 0, 0, '', 0, 0, 0, 0, 0, '0.000000', 0),
(2, 2, 0, 'My carrier', '', 1, 0, 1, 0, 0, 0, 0, 0, '', 0, 1, 0, 0, 0, '0.000000', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_carrier_group`
--

DROP TABLE IF EXISTS `ps_carrier_group`;
CREATE TABLE `ps_carrier_group` (
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_carrier_group`
--

INSERT INTO `ps_carrier_group` (`id_carrier`, `id_group`) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 1),
(2, 2),
(2, 3);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_carrier_lang`
--

DROP TABLE IF EXISTS `ps_carrier_lang`;
CREATE TABLE `ps_carrier_lang` (
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT 1,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `delay` varchar(512) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_carrier_lang`
--

INSERT INTO `ps_carrier_lang` (`id_carrier`, `id_shop`, `id_lang`, `delay`) VALUES
(1, 1, 1, 'OdbiĂłr w sklepie'),
(2, 1, 1, 'Delivery next day!');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_carrier_shop`
--

DROP TABLE IF EXISTS `ps_carrier_shop`;
CREATE TABLE `ps_carrier_shop` (
  `id_carrier` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_carrier_shop`
--

INSERT INTO `ps_carrier_shop` (`id_carrier`, `id_shop`) VALUES
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_carrier_tax_rules_group_shop`
--

DROP TABLE IF EXISTS `ps_carrier_tax_rules_group_shop`;
CREATE TABLE `ps_carrier_tax_rules_group_shop` (
  `id_carrier` int(11) UNSIGNED NOT NULL,
  `id_tax_rules_group` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_carrier_tax_rules_group_shop`
--

INSERT INTO `ps_carrier_tax_rules_group_shop` (`id_carrier`, `id_tax_rules_group`, `id_shop`) VALUES
(1, 1, 1),
(2, 1, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_carrier_zone`
--

DROP TABLE IF EXISTS `ps_carrier_zone`;
CREATE TABLE `ps_carrier_zone` (
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `id_zone` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_carrier_zone`
--

INSERT INTO `ps_carrier_zone` (`id_carrier`, `id_zone`) VALUES
(1, 1),
(2, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_cart`
--

DROP TABLE IF EXISTS `ps_cart`;
CREATE TABLE `ps_cart` (
  `id_cart` int(10) UNSIGNED NOT NULL,
  `id_shop_group` int(11) UNSIGNED NOT NULL DEFAULT 1,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT 1,
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `delivery_option` text NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `id_address_delivery` int(10) UNSIGNED NOT NULL,
  `id_address_invoice` int(10) UNSIGNED NOT NULL,
  `id_currency` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_guest` int(10) UNSIGNED NOT NULL,
  `secure_key` varchar(32) NOT NULL DEFAULT '-1',
  `recyclable` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `gift` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `gift_message` text DEFAULT NULL,
  `mobile_theme` tinyint(1) NOT NULL DEFAULT 0,
  `allow_seperated_package` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_cart`
--

INSERT INTO `ps_cart` (`id_cart`, `id_shop_group`, `id_shop`, `id_carrier`, `delivery_option`, `id_lang`, `id_address_delivery`, `id_address_invoice`, `id_currency`, `id_customer`, `id_guest`, `secure_key`, `recyclable`, `gift`, `gift_message`, `mobile_theme`, `allow_seperated_package`, `date_add`, `date_upd`) VALUES
(1, 1, 1, 2, '{\"3\":\"2,\"}', 1, 4, 4, 1, 1, 1, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 0, 0, '', 0, 0, '2018-12-03 17:18:12', '2018-12-03 17:18:12'),
(2, 1, 1, 2, '{\"3\":\"2,\"}', 1, 4, 4, 1, 1, 1, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 0, 0, '', 0, 0, '2018-12-03 17:18:12', '2018-12-03 17:18:12'),
(3, 1, 1, 2, '{\"3\":\"2,\"}', 1, 4, 4, 1, 1, 1, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 0, 0, '', 0, 0, '2018-12-03 17:18:12', '2018-12-03 17:18:12'),
(4, 1, 1, 2, '{\"3\":\"2,\"}', 1, 4, 4, 1, 1, 1, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 0, 0, '', 0, 0, '2018-12-03 17:18:12', '2018-12-03 17:18:12'),
(5, 1, 1, 2, '{\"3\":\"2,\"}', 1, 4, 4, 1, 1, 1, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 0, 0, '', 0, 0, '2018-12-03 17:18:12', '2018-12-03 17:18:12'),
(6, 1, 1, 0, '', 1, 0, 0, 1, 0, 5, '', 0, 0, '', 0, 0, '2018-12-04 00:17:38', '2018-12-04 00:17:39'),
(7, 1, 1, 0, '', 1, 0, 0, 1, 0, 4, '', 0, 0, '', 0, 0, '2018-12-04 01:07:30', '2018-12-04 01:07:42'),
(8, 1, 1, 1, '{\"5\":\"1,\"}', 1, 5, 5, 1, 2, 6, '86cfa6358d0176a108f7a1e958cdbeee', 0, 0, '', 0, 0, '2018-12-04 12:09:01', '2018-12-04 12:11:19');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_cart_cart_rule`
--

DROP TABLE IF EXISTS `ps_cart_cart_rule`;
CREATE TABLE `ps_cart_cart_rule` (
  `id_cart` int(10) UNSIGNED NOT NULL,
  `id_cart_rule` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_cart_product`
--

DROP TABLE IF EXISTS `ps_cart_product`;
CREATE TABLE `ps_cart_product` (
  `id_cart` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_address_delivery` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `id_product_attribute` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_cart_product`
--

INSERT INTO `ps_cart_product` (`id_cart`, `id_product`, `id_address_delivery`, `id_shop`, `id_product_attribute`, `quantity`, `date_add`) VALUES
(6, 2, 0, 1, 0, 1, '2018-12-04 00:17:39'),
(8, 2, 5, 1, 0, 1, '2018-12-04 12:09:01');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_cart_rule`
--

DROP TABLE IF EXISTS `ps_cart_rule`;
CREATE TABLE `ps_cart_rule` (
  `id_cart_rule` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `date_from` datetime NOT NULL,
  `date_to` datetime NOT NULL,
  `description` text DEFAULT NULL,
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `quantity_per_user` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `priority` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `partial_use` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `code` varchar(254) NOT NULL,
  `minimum_amount` decimal(17,2) NOT NULL DEFAULT 0.00,
  `minimum_amount_tax` tinyint(1) NOT NULL DEFAULT 0,
  `minimum_amount_currency` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `minimum_amount_shipping` tinyint(1) NOT NULL DEFAULT 0,
  `country_restriction` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `carrier_restriction` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `group_restriction` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `cart_rule_restriction` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `product_restriction` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `shop_restriction` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `free_shipping` tinyint(1) NOT NULL DEFAULT 0,
  `reduction_percent` decimal(5,2) NOT NULL DEFAULT 0.00,
  `reduction_amount` decimal(17,2) NOT NULL DEFAULT 0.00,
  `reduction_tax` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `reduction_currency` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `reduction_product` int(10) NOT NULL DEFAULT 0,
  `gift_product` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `gift_product_attribute` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `highlight` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_cart_rule_carrier`
--

DROP TABLE IF EXISTS `ps_cart_rule_carrier`;
CREATE TABLE `ps_cart_rule_carrier` (
  `id_cart_rule` int(10) UNSIGNED NOT NULL,
  `id_carrier` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_cart_rule_combination`
--

DROP TABLE IF EXISTS `ps_cart_rule_combination`;
CREATE TABLE `ps_cart_rule_combination` (
  `id_cart_rule_1` int(10) UNSIGNED NOT NULL,
  `id_cart_rule_2` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_cart_rule_country`
--

DROP TABLE IF EXISTS `ps_cart_rule_country`;
CREATE TABLE `ps_cart_rule_country` (
  `id_cart_rule` int(10) UNSIGNED NOT NULL,
  `id_country` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_cart_rule_group`
--

DROP TABLE IF EXISTS `ps_cart_rule_group`;
CREATE TABLE `ps_cart_rule_group` (
  `id_cart_rule` int(10) UNSIGNED NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_cart_rule_lang`
--

DROP TABLE IF EXISTS `ps_cart_rule_lang`;
CREATE TABLE `ps_cart_rule_lang` (
  `id_cart_rule` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(254) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_cart_rule_product_rule`
--

DROP TABLE IF EXISTS `ps_cart_rule_product_rule`;
CREATE TABLE `ps_cart_rule_product_rule` (
  `id_product_rule` int(10) UNSIGNED NOT NULL,
  `id_product_rule_group` int(10) UNSIGNED NOT NULL,
  `type` enum('products','categories','attributes','manufacturers','suppliers') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_cart_rule_product_rule_group`
--

DROP TABLE IF EXISTS `ps_cart_rule_product_rule_group`;
CREATE TABLE `ps_cart_rule_product_rule_group` (
  `id_product_rule_group` int(10) UNSIGNED NOT NULL,
  `id_cart_rule` int(10) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_cart_rule_product_rule_value`
--

DROP TABLE IF EXISTS `ps_cart_rule_product_rule_value`;
CREATE TABLE `ps_cart_rule_product_rule_value` (
  `id_product_rule` int(10) UNSIGNED NOT NULL,
  `id_item` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_cart_rule_shop`
--

DROP TABLE IF EXISTS `ps_cart_rule_shop`;
CREATE TABLE `ps_cart_rule_shop` (
  `id_cart_rule` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_category`
--

DROP TABLE IF EXISTS `ps_category`;
CREATE TABLE `ps_category` (
  `id_category` int(10) UNSIGNED NOT NULL,
  `id_parent` int(10) UNSIGNED NOT NULL,
  `id_shop_default` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `level_depth` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `nleft` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `nright` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `is_root_category` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_category`
--

INSERT INTO `ps_category` (`id_category`, `id_parent`, `id_shop_default`, `level_depth`, `nleft`, `nright`, `active`, `date_add`, `date_upd`, `position`, `is_root_category`) VALUES
(1, 0, 1, 0, 1, 46, 1, '2018-12-03 17:18:00', '2018-12-03 17:18:00', 0, 0),
(2, 1, 1, 1, 2, 45, 1, '2018-12-03 17:18:00', '2018-12-03 17:18:00', 0, 1),
(9, 8, 1, 4, 5, 6, 1, '2018-12-03 17:18:12', '2018-12-03 17:18:12', 0, 0),
(10, 8, 1, 4, 7, 8, 1, '2018-12-03 17:18:12', '2018-12-03 17:18:12', 0, 0),
(11, 8, 1, 4, 9, 10, 1, '2018-12-03 17:18:12', '2018-12-03 17:18:12', 0, 0),
(12, 2, 1, 2, 3, 4, 1, '2018-12-03 23:49:07', '2018-12-04 00:23:15', 0, 0),
(13, 2, 1, 2, 5, 6, 1, '2018-12-03 23:49:07', '2018-12-04 00:23:15', 1, 0),
(14, 2, 1, 2, 7, 8, 1, '2018-12-03 23:49:07', '2018-12-04 00:23:15', 2, 0),
(15, 2, 1, 2, 9, 10, 1, '2018-12-03 23:49:07', '2018-12-04 00:23:15', 3, 0),
(16, 2, 1, 2, 11, 12, 1, '2018-12-03 23:49:07', '2018-12-04 00:23:15', 4, 0),
(17, 2, 1, 2, 13, 14, 1, '2018-12-03 23:49:07', '2018-12-04 00:23:15', 5, 0),
(18, 2, 1, 2, 15, 16, 1, '2018-12-03 23:49:07', '2018-12-04 00:23:15', 6, 0),
(19, 2, 1, 2, 17, 18, 1, '2018-12-03 23:49:07', '2018-12-04 00:23:15', 7, 0),
(20, 2, 1, 2, 19, 20, 1, '2018-12-03 23:49:08', '2018-12-04 00:23:15', 8, 0),
(21, 2, 1, 2, 21, 22, 1, '2018-12-03 23:49:08', '2018-12-04 00:23:15', 9, 0),
(22, 2, 1, 2, 23, 24, 1, '2018-12-03 23:49:08', '2018-12-04 00:23:15', 10, 0),
(23, 2, 1, 2, 25, 26, 1, '2018-12-03 23:49:08', '2018-12-04 00:23:15', 11, 0),
(24, 2, 1, 2, 27, 28, 1, '2018-12-03 23:49:09', '2018-12-04 00:23:15', 12, 0),
(25, 2, 1, 2, 29, 30, 1, '2018-12-03 23:49:09', '2018-12-04 00:23:15', 13, 0),
(26, 2, 1, 2, 31, 32, 1, '2018-12-03 23:49:09', '2018-12-04 00:23:15', 14, 0),
(27, 2, 1, 2, 33, 34, 1, '2018-12-03 23:49:09', '2018-12-04 00:23:15', 15, 0),
(28, 2, 1, 2, 35, 36, 1, '2018-12-03 23:49:10', '2018-12-04 00:23:15', 16, 0),
(29, 2, 1, 2, 37, 38, 1, '2018-12-03 23:49:10', '2018-12-04 00:23:15', 17, 0),
(30, 2, 1, 2, 39, 40, 1, '2018-12-03 23:49:11', '2018-12-04 00:23:15', 18, 0),
(31, 2, 1, 2, 41, 42, 1, '2018-12-03 23:49:11', '2018-12-04 00:23:15', 19, 0),
(32, 2, 1, 2, 43, 44, 1, '2018-12-03 23:49:15', '2018-12-04 00:23:15', 20, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_category_group`
--

DROP TABLE IF EXISTS `ps_category_group`;
CREATE TABLE `ps_category_group` (
  `id_category` int(10) UNSIGNED NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_category_group`
--

INSERT INTO `ps_category_group` (`id_category`, `id_group`) VALUES
(2, 0),
(2, 1),
(2, 2),
(2, 3),
(9, 1),
(9, 2),
(9, 3),
(10, 1),
(10, 2),
(10, 3),
(11, 1),
(11, 2),
(11, 3),
(12, 1),
(12, 2),
(12, 3),
(13, 1),
(13, 2),
(13, 3),
(14, 1),
(14, 2),
(14, 3),
(15, 1),
(15, 2),
(15, 3),
(16, 1),
(16, 2),
(16, 3),
(17, 1),
(17, 2),
(17, 3),
(18, 1),
(18, 2),
(18, 3),
(19, 1),
(19, 2),
(19, 3),
(20, 1),
(20, 2),
(20, 3),
(21, 1),
(21, 2),
(21, 3),
(22, 1),
(22, 2),
(22, 3),
(23, 1),
(23, 2),
(23, 3),
(24, 1),
(24, 2),
(24, 3),
(25, 1),
(25, 2),
(25, 3),
(26, 1),
(26, 2),
(26, 3),
(27, 1),
(27, 2),
(27, 3),
(28, 1),
(28, 2),
(28, 3),
(29, 1),
(29, 2),
(29, 3),
(30, 1),
(30, 2),
(30, 3),
(31, 1),
(31, 2),
(31, 3),
(32, 1),
(32, 2),
(32, 3);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_category_lang`
--

DROP TABLE IF EXISTS `ps_category_lang`;
CREATE TABLE `ps_category_lang` (
  `id_category` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT 1,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text DEFAULT NULL,
  `link_rewrite` varchar(128) NOT NULL,
  `meta_title` varchar(128) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_category_lang`
--

INSERT INTO `ps_category_lang` (`id_category`, `id_shop`, `id_lang`, `name`, `description`, `link_rewrite`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
(1, 1, 1, 'Bazowa', '', 'bazowa', '', '', ''),
(2, 1, 1, 'GĹĂłwna', '', 'glowna', '', '', ''),
(9, 1, 1, 'Casual Dresses', '<p>You are looking for a dress for every day? Take a look at</p>\r\n<p>our selection of dresses to find one that suits you.</p>', 'casual-dresses', '', '', ''),
(10, 1, 1, 'Evening Dresses', 'Browse our different dresses to choose the perfect dress for an unforgettable evening!', 'evening-dresses', '', '', ''),
(11, 1, 1, 'Summer Dresses', 'Short dress, long dress, silk dress, printed dress, you will find the perfect dress for summer.', 'summer-dresses', '', '', ''),
(12, 1, 1, 'Metalik', '', 'metalik', '', '', ''),
(13, 1, 1, 'Benzyna', '', 'benzyna', '', '', ''),
(14, 1, 1, 'Wielu właścicieli', '', 'wielu-wlascicieli', '', '', ''),
(15, 1, 1, 'Przednia oś', '', 'przednia-os', '', '', ''),
(16, 1, 1, 'Powypadkowy', '', 'powypadkowy', '', '', ''),
(17, 1, 1, 'Polska', '', 'polska', '', '', ''),
(18, 1, 1, 'Leasing', '', 'leasing', '', '', ''),
(19, 1, 1, 'Tylko w ASO', '', 'tylko-w-aso', '', '', ''),
(20, 1, 1, 'Diesel', '', 'diesel', '', '', ''),
(21, 1, 1, 'Pierwszy właściciel', '', 'pierwszy-wlasciciel', '', '', ''),
(22, 1, 1, 'Bezwypadkowy', '', 'bezwypadkowy', '', '', ''),
(23, 1, 1, 'USA', '', 'usa', '', '', ''),
(24, 1, 1, 'Nie serwisowany w ASO', '', 'nie-serwisowany-w-aso', '', '', ''),
(25, 1, 1, 'Matowy', '', 'matowy', '', '', ''),
(26, 1, 1, '4x4', '', '4x4', '', '', ''),
(27, 1, 1, 'Tylko prywatne', '', 'tylko-prywatne', '', '', ''),
(28, 1, 1, 'Tylko firmy', '', 'tylko-firmy', '', '', ''),
(29, 1, 1, 'Tylnia oś', '', 'tylnia-os', '', '', ''),
(30, 1, 1, 'Perłowy', '', 'perlowy', '', '', ''),
(31, 1, 1, 'Unia Europejska', '', 'unia-europejska', '', '', ''),
(32, 1, 1, 'Niemcy', '', 'niemcy', '', '', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_category_product`
--

DROP TABLE IF EXISTS `ps_category_product`;
CREATE TABLE `ps_category_product` (
  `id_category` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_category_product`
--

INSERT INTO `ps_category_product` (`id_category`, `id_product`, `position`) VALUES
(12, 1, 0),
(12, 2, 1),
(12, 3, 2),
(12, 6, 3),
(12, 8, 4),
(12, 10, 5),
(12, 12, 6),
(12, 17, 7),
(12, 20, 8),
(12, 23, 9),
(12, 25, 10),
(12, 28, 11),
(12, 32, 12),
(12, 37, 13),
(12, 43, 14),
(12, 44, 15),
(12, 48, 16),
(12, 53, 17),
(12, 54, 18),
(12, 56, 19),
(12, 57, 20),
(12, 63, 21),
(12, 64, 22),
(12, 72, 23),
(12, 77, 24),
(12, 78, 25),
(12, 82, 26),
(12, 84, 27),
(12, 85, 28),
(12, 86, 29),
(12, 88, 30),
(12, 91, 31),
(12, 94, 32),
(12, 102, 33),
(12, 107, 34),
(12, 109, 35),
(12, 114, 36),
(12, 115, 37),
(12, 117, 38),
(12, 118, 39),
(12, 124, 40),
(12, 125, 41),
(12, 126, 42),
(12, 133, 43),
(12, 137, 44),
(12, 138, 45),
(12, 141, 46),
(12, 143, 47),
(12, 152, 48),
(12, 156, 49),
(12, 159, 50),
(12, 162, 51),
(12, 163, 52),
(12, 170, 53),
(12, 174, 54),
(12, 177, 55),
(12, 179, 56),
(12, 186, 57),
(12, 192, 58),
(12, 194, 59),
(12, 197, 60),
(12, 200, 61),
(12, 201, 62),
(12, 205, 63),
(12, 208, 64),
(12, 209, 65),
(12, 214, 66),
(12, 216, 67),
(12, 217, 68),
(12, 223, 69),
(12, 224, 70),
(12, 231, 71),
(12, 235, 72),
(12, 237, 73),
(12, 238, 74),
(12, 241, 75),
(12, 243, 76),
(12, 255, 77),
(12, 260, 78),
(12, 262, 79),
(12, 269, 80),
(12, 271, 81),
(12, 272, 82),
(12, 275, 83),
(12, 279, 84),
(12, 281, 85),
(12, 282, 86),
(12, 285, 87),
(12, 286, 88),
(12, 289, 89),
(12, 293, 90),
(12, 294, 91),
(12, 295, 92),
(12, 296, 93),
(12, 301, 94),
(12, 304, 95),
(12, 305, 96),
(12, 306, 97),
(12, 307, 98),
(12, 309, 99),
(12, 311, 100),
(12, 318, 101),
(12, 323, 102),
(12, 327, 103),
(12, 330, 104),
(12, 332, 105),
(12, 345, 106),
(12, 346, 107),
(12, 349, 108),
(12, 350, 109),
(12, 353, 110),
(12, 356, 111),
(12, 358, 112),
(12, 359, 113),
(12, 360, 114),
(12, 369, 115),
(12, 371, 116),
(12, 373, 117),
(12, 375, 118),
(12, 377, 119),
(12, 379, 120),
(12, 382, 121),
(13, 1, 0),
(13, 5, 1),
(13, 6, 2),
(13, 7, 3),
(13, 8, 4),
(13, 10, 5),
(13, 11, 6),
(13, 15, 7),
(13, 17, 8),
(13, 18, 9),
(13, 19, 10),
(13, 20, 11),
(13, 23, 12),
(13, 24, 13),
(13, 27, 14),
(13, 28, 15),
(13, 35, 16),
(13, 38, 17),
(13, 39, 18),
(13, 41, 19),
(13, 42, 20),
(13, 43, 21),
(13, 45, 22),
(13, 46, 23),
(13, 49, 24),
(13, 52, 25),
(13, 53, 26),
(13, 56, 27),
(13, 58, 28),
(13, 61, 29),
(13, 62, 30),
(13, 64, 31),
(13, 66, 32),
(13, 67, 33),
(13, 70, 34),
(13, 71, 35),
(13, 72, 36),
(13, 78, 37),
(13, 80, 38),
(13, 82, 39),
(13, 83, 40),
(13, 84, 41),
(13, 91, 42),
(13, 95, 43),
(13, 96, 44),
(13, 100, 45),
(13, 103, 46),
(13, 104, 47),
(13, 106, 48),
(13, 107, 49),
(13, 108, 50),
(13, 109, 51),
(13, 110, 52),
(13, 111, 53),
(13, 112, 54),
(13, 115, 55),
(13, 116, 56),
(13, 130, 57),
(13, 131, 58),
(13, 135, 59),
(13, 136, 60),
(13, 137, 61),
(13, 138, 62),
(13, 139, 63),
(13, 140, 64),
(13, 141, 65),
(13, 143, 66),
(13, 146, 67),
(13, 150, 68),
(13, 151, 69),
(13, 152, 70),
(13, 154, 71),
(13, 155, 72),
(13, 158, 73),
(13, 163, 74),
(13, 165, 75),
(13, 166, 76),
(13, 168, 77),
(13, 170, 78),
(13, 171, 79),
(13, 174, 80),
(13, 175, 81),
(13, 177, 82),
(13, 180, 83),
(13, 184, 84),
(13, 188, 85),
(13, 190, 86),
(13, 192, 87),
(13, 193, 88),
(13, 194, 89),
(13, 197, 90),
(13, 199, 91),
(13, 207, 92),
(13, 216, 93),
(13, 217, 94),
(13, 219, 95),
(13, 220, 96),
(13, 221, 97),
(13, 222, 98),
(13, 223, 99),
(13, 225, 100),
(13, 228, 101),
(13, 229, 102),
(13, 232, 103),
(13, 233, 104),
(13, 236, 105),
(13, 237, 106),
(13, 238, 107),
(13, 239, 108),
(13, 240, 109),
(13, 241, 110),
(13, 242, 111),
(13, 243, 112),
(13, 247, 113),
(13, 253, 114),
(13, 256, 115),
(13, 257, 116),
(13, 258, 117),
(13, 260, 118),
(13, 261, 119),
(13, 262, 120),
(13, 265, 121),
(13, 266, 122),
(13, 267, 123),
(13, 269, 124),
(13, 270, 125),
(13, 271, 126),
(13, 272, 127),
(13, 274, 128),
(13, 275, 129),
(13, 277, 130),
(13, 280, 131),
(13, 281, 132),
(13, 282, 133),
(13, 284, 134),
(13, 285, 135),
(13, 286, 136),
(13, 288, 137),
(13, 289, 138),
(13, 290, 139),
(13, 292, 140),
(13, 294, 141),
(13, 296, 142),
(13, 301, 143),
(13, 304, 144),
(13, 305, 145),
(13, 310, 146),
(13, 311, 147),
(13, 313, 148),
(13, 314, 149),
(13, 316, 150),
(13, 318, 151),
(13, 319, 152),
(13, 321, 153),
(13, 323, 154),
(13, 325, 155),
(13, 327, 156),
(13, 328, 157),
(13, 330, 158),
(13, 337, 159),
(13, 338, 160),
(13, 339, 161),
(13, 340, 162),
(13, 343, 163),
(13, 344, 164),
(13, 345, 165),
(13, 346, 166),
(13, 348, 167),
(13, 349, 168),
(13, 351, 169),
(13, 352, 170),
(13, 353, 171),
(13, 354, 172),
(13, 355, 173),
(13, 356, 174),
(13, 358, 175),
(13, 361, 176),
(13, 363, 177),
(13, 366, 178),
(13, 371, 179),
(13, 372, 180),
(13, 373, 181),
(13, 376, 182),
(13, 378, 183),
(13, 379, 184),
(13, 380, 185),
(13, 381, 186),
(14, 1, 0),
(14, 4, 1),
(14, 5, 2),
(14, 6, 3),
(14, 10, 4),
(14, 12, 5),
(14, 13, 6),
(14, 16, 7),
(14, 17, 8),
(14, 18, 9),
(14, 23, 10),
(14, 24, 11),
(14, 25, 12),
(14, 27, 13),
(14, 28, 14),
(14, 29, 15),
(14, 34, 16),
(14, 37, 17),
(14, 42, 18),
(14, 44, 19),
(14, 46, 20),
(14, 47, 21),
(14, 48, 22),
(14, 49, 23),
(14, 51, 24),
(14, 58, 25),
(14, 59, 26),
(14, 62, 27),
(14, 63, 28),
(14, 65, 29),
(14, 67, 30),
(14, 70, 31),
(14, 72, 32),
(14, 73, 33),
(14, 75, 34),
(14, 76, 35),
(14, 82, 36),
(14, 83, 37),
(14, 88, 38),
(14, 94, 39),
(14, 97, 40),
(14, 98, 41),
(14, 99, 42),
(14, 100, 43),
(14, 101, 44),
(14, 105, 45),
(14, 106, 46),
(14, 107, 47),
(14, 110, 48),
(14, 113, 49),
(14, 115, 50),
(14, 118, 51),
(14, 121, 52),
(14, 122, 53),
(14, 123, 54),
(14, 124, 55),
(14, 126, 56),
(14, 127, 57),
(14, 130, 58),
(14, 133, 59),
(14, 136, 60),
(14, 137, 61),
(14, 138, 62),
(14, 139, 63),
(14, 140, 64),
(14, 144, 65),
(14, 146, 66),
(14, 147, 67),
(14, 149, 68),
(14, 150, 69),
(14, 151, 70),
(14, 153, 71),
(14, 155, 72),
(14, 156, 73),
(14, 158, 74),
(14, 159, 75),
(14, 161, 76),
(14, 162, 77),
(14, 167, 78),
(14, 168, 79),
(14, 169, 80),
(14, 174, 81),
(14, 175, 82),
(14, 176, 83),
(14, 177, 84),
(14, 178, 85),
(14, 179, 86),
(14, 182, 87),
(14, 184, 88),
(14, 186, 89),
(14, 187, 90),
(14, 189, 91),
(14, 190, 92),
(14, 193, 93),
(14, 194, 94),
(14, 195, 95),
(14, 199, 96),
(14, 200, 97),
(14, 202, 98),
(14, 206, 99),
(14, 209, 100),
(14, 210, 101),
(14, 212, 102),
(14, 213, 103),
(14, 218, 104),
(14, 219, 105),
(14, 221, 106),
(14, 222, 107),
(14, 223, 108),
(14, 227, 109),
(14, 228, 110),
(14, 229, 111),
(14, 230, 112),
(14, 231, 113),
(14, 233, 114),
(14, 234, 115),
(14, 240, 116),
(14, 243, 117),
(14, 245, 118),
(14, 246, 119),
(14, 247, 120),
(14, 249, 121),
(14, 251, 122),
(14, 252, 123),
(14, 253, 124),
(14, 254, 125),
(14, 258, 126),
(14, 260, 127),
(14, 262, 128),
(14, 263, 129),
(14, 266, 130),
(14, 267, 131),
(14, 270, 132),
(14, 271, 133),
(14, 272, 134),
(14, 273, 135),
(14, 276, 136),
(14, 278, 137),
(14, 279, 138),
(14, 281, 139),
(14, 282, 140),
(14, 283, 141),
(14, 284, 142),
(14, 285, 143),
(14, 286, 144),
(14, 290, 145),
(14, 292, 146),
(14, 294, 147),
(14, 295, 148),
(14, 301, 149),
(14, 302, 150),
(14, 306, 151),
(14, 311, 152),
(14, 312, 153),
(14, 315, 154),
(14, 316, 155),
(14, 317, 156),
(14, 320, 157),
(14, 321, 158),
(14, 322, 159),
(14, 329, 160),
(14, 330, 161),
(14, 331, 162),
(14, 336, 163),
(14, 342, 164),
(14, 343, 165),
(14, 344, 166),
(14, 345, 167),
(14, 346, 168),
(14, 349, 169),
(14, 350, 170),
(14, 351, 171),
(14, 352, 172),
(14, 354, 173),
(14, 356, 174),
(14, 359, 175),
(14, 360, 176),
(14, 362, 177),
(14, 363, 178),
(14, 365, 179),
(14, 366, 180),
(14, 367, 181),
(14, 369, 182),
(14, 373, 183),
(14, 374, 184),
(14, 378, 185),
(14, 379, 186),
(14, 381, 187),
(14, 382, 188),
(15, 1, 0),
(15, 2, 1),
(15, 3, 2),
(15, 5, 3),
(15, 7, 4),
(15, 14, 5),
(15, 15, 6),
(15, 16, 7),
(15, 19, 8),
(15, 20, 9),
(15, 21, 10),
(15, 26, 11),
(15, 30, 12),
(15, 32, 13),
(15, 40, 14),
(15, 43, 15),
(15, 47, 16),
(15, 48, 17),
(15, 54, 18),
(15, 55, 19),
(15, 57, 20),
(15, 61, 21),
(15, 67, 22),
(15, 69, 23),
(15, 74, 24),
(15, 76, 25),
(15, 77, 26),
(15, 78, 27),
(15, 79, 28),
(15, 82, 29),
(15, 86, 30),
(15, 90, 31),
(15, 92, 32),
(15, 98, 33),
(15, 106, 34),
(15, 107, 35),
(15, 111, 36),
(15, 112, 37),
(15, 116, 38),
(15, 117, 39),
(15, 118, 40),
(15, 120, 41),
(15, 122, 42),
(15, 128, 43),
(15, 129, 44),
(15, 130, 45),
(15, 131, 46),
(15, 132, 47),
(15, 133, 48),
(15, 136, 49),
(15, 137, 50),
(15, 138, 51),
(15, 139, 52),
(15, 144, 53),
(15, 149, 54),
(15, 154, 55),
(15, 155, 56),
(15, 156, 57),
(15, 157, 58),
(15, 158, 59),
(15, 159, 60),
(15, 160, 61),
(15, 172, 62),
(15, 173, 63),
(15, 175, 64),
(15, 176, 65),
(15, 178, 66),
(15, 181, 67),
(15, 183, 68),
(15, 186, 69),
(15, 189, 70),
(15, 191, 71),
(15, 195, 72),
(15, 200, 73),
(15, 204, 74),
(15, 208, 75),
(15, 209, 76),
(15, 213, 77),
(15, 223, 78),
(15, 228, 79),
(15, 231, 80),
(15, 232, 81),
(15, 233, 82),
(15, 251, 83),
(15, 253, 84),
(15, 261, 85),
(15, 262, 86),
(15, 264, 87),
(15, 267, 88),
(15, 272, 89),
(15, 274, 90),
(15, 275, 91),
(15, 276, 92),
(15, 277, 93),
(15, 279, 94),
(15, 280, 95),
(15, 282, 96),
(15, 283, 97),
(15, 285, 98),
(15, 288, 99),
(15, 293, 100),
(15, 294, 101),
(15, 299, 102),
(15, 301, 103),
(15, 303, 104),
(15, 306, 105),
(15, 308, 106),
(15, 311, 107),
(15, 313, 108),
(15, 315, 109),
(15, 320, 110),
(15, 321, 111),
(15, 322, 112),
(15, 329, 113),
(15, 332, 114),
(15, 334, 115),
(15, 335, 116),
(15, 337, 117),
(15, 344, 118),
(15, 347, 119),
(15, 349, 120),
(15, 350, 121),
(15, 353, 122),
(15, 354, 123),
(15, 358, 124),
(15, 359, 125),
(15, 368, 126),
(15, 369, 127),
(15, 370, 128),
(15, 372, 129),
(15, 373, 130),
(15, 377, 131),
(15, 380, 132),
(15, 382, 133),
(16, 1, 0),
(16, 4, 1),
(16, 6, 2),
(16, 7, 3),
(16, 13, 4),
(16, 15, 5),
(16, 16, 6),
(16, 20, 7),
(16, 23, 8),
(16, 25, 9),
(16, 26, 10),
(16, 27, 11),
(16, 28, 12),
(16, 33, 13),
(16, 34, 14),
(16, 36, 15),
(16, 37, 16),
(16, 41, 17),
(16, 42, 18),
(16, 45, 19),
(16, 46, 20),
(16, 47, 21),
(16, 55, 22),
(16, 56, 23),
(16, 58, 24),
(16, 61, 25),
(16, 62, 26),
(16, 65, 27),
(16, 70, 28),
(16, 73, 29),
(16, 75, 30),
(16, 78, 31),
(16, 79, 32),
(16, 81, 33),
(16, 82, 34),
(16, 86, 35),
(16, 88, 36),
(16, 89, 37),
(16, 90, 38),
(16, 95, 39),
(16, 96, 40),
(16, 99, 41),
(16, 100, 42),
(16, 101, 43),
(16, 104, 44),
(16, 105, 45),
(16, 110, 46),
(16, 111, 47),
(16, 112, 48),
(16, 115, 49),
(16, 118, 50),
(16, 121, 51),
(16, 123, 52),
(16, 124, 53),
(16, 127, 54),
(16, 128, 55),
(16, 129, 56),
(16, 132, 57),
(16, 135, 58),
(16, 137, 59),
(16, 139, 60),
(16, 140, 61),
(16, 141, 62),
(16, 142, 63),
(16, 143, 64),
(16, 147, 65),
(16, 149, 66),
(16, 150, 67),
(16, 151, 68),
(16, 152, 69),
(16, 154, 70),
(16, 155, 71),
(16, 156, 72),
(16, 158, 73),
(16, 161, 74),
(16, 162, 75),
(16, 164, 76),
(16, 173, 77),
(16, 183, 78),
(16, 184, 79),
(16, 186, 80),
(16, 188, 81),
(16, 189, 82),
(16, 190, 83),
(16, 192, 84),
(16, 193, 85),
(16, 196, 86),
(16, 197, 87),
(16, 198, 88),
(16, 201, 89),
(16, 205, 90),
(16, 206, 91),
(16, 207, 92),
(16, 210, 93),
(16, 211, 94),
(16, 212, 95),
(16, 213, 96),
(16, 215, 97),
(16, 216, 98),
(16, 218, 99),
(16, 222, 100),
(16, 223, 101),
(16, 227, 102),
(16, 229, 103),
(16, 232, 104),
(16, 235, 105),
(16, 239, 106),
(16, 242, 107),
(16, 243, 108),
(16, 245, 109),
(16, 246, 110),
(16, 248, 111),
(16, 249, 112),
(16, 251, 113),
(16, 253, 114),
(16, 256, 115),
(16, 262, 116),
(16, 264, 117),
(16, 266, 118),
(16, 270, 119),
(16, 274, 120),
(16, 275, 121),
(16, 276, 122),
(16, 277, 123),
(16, 279, 124),
(16, 283, 125),
(16, 287, 126),
(16, 288, 127),
(16, 294, 128),
(16, 295, 129),
(16, 297, 130),
(16, 305, 131),
(16, 307, 132),
(16, 309, 133),
(16, 313, 134),
(16, 314, 135),
(16, 316, 136),
(16, 317, 137),
(16, 321, 138),
(16, 323, 139),
(16, 324, 140),
(16, 325, 141),
(16, 327, 142),
(16, 329, 143),
(16, 331, 144),
(16, 332, 145),
(16, 333, 146),
(16, 337, 147),
(16, 342, 148),
(16, 343, 149),
(16, 344, 150),
(16, 345, 151),
(16, 348, 152),
(16, 353, 153),
(16, 354, 154),
(16, 355, 155),
(16, 356, 156),
(16, 357, 157),
(16, 360, 158),
(16, 361, 159),
(16, 365, 160),
(16, 370, 161),
(16, 371, 162),
(16, 372, 163),
(16, 373, 164),
(16, 378, 165),
(16, 380, 166),
(16, 381, 167),
(17, 1, 0),
(17, 7, 1),
(17, 10, 2),
(17, 12, 3),
(17, 13, 4),
(17, 15, 5),
(17, 16, 6),
(17, 18, 7),
(17, 26, 8),
(17, 28, 9),
(17, 30, 10),
(17, 31, 11),
(17, 37, 12),
(17, 39, 13),
(17, 48, 14),
(17, 49, 15),
(17, 56, 16),
(17, 60, 17),
(17, 63, 18),
(17, 73, 19),
(17, 75, 20),
(17, 79, 21),
(17, 83, 22),
(17, 84, 23),
(17, 85, 24),
(17, 89, 25),
(17, 90, 26),
(17, 94, 27),
(17, 102, 28),
(17, 104, 29),
(17, 107, 30),
(17, 108, 31),
(17, 109, 32),
(17, 115, 33),
(17, 116, 34),
(17, 128, 35),
(17, 129, 36),
(17, 130, 37),
(17, 139, 38),
(17, 143, 39),
(17, 144, 40),
(17, 151, 41),
(17, 155, 42),
(17, 161, 43),
(17, 164, 44),
(17, 167, 45),
(17, 169, 46),
(17, 172, 47),
(17, 173, 48),
(17, 180, 49),
(17, 183, 50),
(17, 184, 51),
(17, 195, 52),
(17, 198, 53),
(17, 205, 54),
(17, 207, 55),
(17, 211, 56),
(17, 212, 57),
(17, 215, 58),
(17, 216, 59),
(17, 224, 60),
(17, 229, 61),
(17, 232, 62),
(17, 235, 63),
(17, 236, 64),
(17, 239, 65),
(17, 240, 66),
(17, 247, 67),
(17, 249, 68),
(17, 250, 69),
(17, 255, 70),
(17, 259, 71),
(17, 260, 72),
(17, 264, 73),
(17, 271, 74),
(17, 273, 75),
(17, 275, 76),
(17, 277, 77),
(17, 279, 78),
(17, 293, 79),
(17, 295, 80),
(17, 306, 81),
(17, 324, 82),
(17, 326, 83),
(17, 331, 84),
(17, 336, 85),
(17, 337, 86),
(17, 339, 87),
(17, 346, 88),
(17, 353, 89),
(17, 355, 90),
(17, 359, 91),
(17, 362, 92),
(17, 363, 93),
(17, 366, 94),
(17, 367, 95),
(17, 368, 96),
(17, 370, 97),
(17, 371, 98),
(17, 373, 99),
(17, 374, 100),
(17, 377, 101),
(17, 378, 102),
(17, 380, 103),
(18, 1, 0),
(18, 2, 1),
(18, 3, 2),
(18, 6, 3),
(18, 8, 4),
(18, 10, 5),
(18, 11, 6),
(18, 13, 7),
(18, 15, 8),
(18, 16, 9),
(18, 19, 10),
(18, 23, 11),
(18, 25, 12),
(18, 26, 13),
(18, 28, 14),
(18, 30, 15),
(18, 32, 16),
(18, 36, 17),
(18, 39, 18),
(18, 41, 19),
(18, 44, 20),
(18, 45, 21),
(18, 51, 22),
(18, 52, 23),
(18, 54, 24),
(18, 60, 25),
(18, 66, 26),
(18, 68, 27),
(18, 70, 28),
(18, 71, 29),
(18, 74, 30),
(18, 81, 31),
(18, 82, 32),
(18, 83, 33),
(18, 84, 34),
(18, 88, 35),
(18, 90, 36),
(18, 95, 37),
(18, 96, 38),
(18, 98, 39),
(18, 105, 40),
(18, 106, 41),
(18, 107, 42),
(18, 109, 43),
(18, 113, 44),
(18, 116, 45),
(18, 118, 46),
(18, 122, 47),
(18, 125, 48),
(18, 126, 49),
(18, 128, 50),
(18, 129, 51),
(18, 131, 52),
(18, 132, 53),
(18, 133, 54),
(18, 136, 55),
(18, 140, 56),
(18, 141, 57),
(18, 143, 58),
(18, 144, 59),
(18, 150, 60),
(18, 152, 61),
(18, 153, 62),
(18, 155, 63),
(18, 156, 64),
(18, 162, 65),
(18, 163, 66),
(18, 169, 67),
(18, 171, 68),
(18, 177, 69),
(18, 181, 70),
(18, 184, 71),
(18, 186, 72),
(18, 190, 73),
(18, 193, 74),
(18, 196, 75),
(18, 197, 76),
(18, 199, 77),
(18, 203, 78),
(18, 206, 79),
(18, 207, 80),
(18, 213, 81),
(18, 214, 82),
(18, 220, 83),
(18, 221, 84),
(18, 223, 85),
(18, 225, 86),
(18, 227, 87),
(18, 228, 88),
(18, 230, 89),
(18, 234, 90),
(18, 237, 91),
(18, 238, 92),
(18, 241, 93),
(18, 243, 94),
(18, 248, 95),
(18, 254, 96),
(18, 259, 97),
(18, 266, 98),
(18, 273, 99),
(18, 275, 100),
(18, 279, 101),
(18, 289, 102),
(18, 292, 103),
(18, 295, 104),
(18, 296, 105),
(18, 297, 106),
(18, 298, 107),
(18, 303, 108),
(18, 304, 109),
(18, 308, 110),
(18, 311, 111),
(18, 312, 112),
(18, 316, 113),
(18, 319, 114),
(18, 320, 115),
(18, 321, 116),
(18, 328, 117),
(18, 334, 118),
(18, 341, 119),
(18, 342, 120),
(18, 344, 121),
(18, 346, 122),
(18, 356, 123),
(18, 361, 124),
(18, 362, 125),
(18, 363, 126),
(18, 365, 127),
(18, 371, 128),
(18, 374, 129),
(18, 375, 130),
(18, 377, 131),
(18, 379, 132),
(18, 381, 133),
(19, 1, 0),
(19, 2, 1),
(19, 4, 2),
(19, 6, 3),
(19, 7, 4),
(19, 11, 5),
(19, 12, 6),
(19, 14, 7),
(19, 15, 8),
(19, 16, 9),
(19, 17, 10),
(19, 18, 11),
(19, 19, 12),
(19, 22, 13),
(19, 24, 14),
(19, 25, 15),
(19, 26, 16),
(19, 28, 17),
(19, 33, 18),
(19, 34, 19),
(19, 35, 20),
(19, 36, 21),
(19, 42, 22),
(19, 43, 23),
(19, 45, 24),
(19, 46, 25),
(19, 47, 26),
(19, 50, 27),
(19, 51, 28),
(19, 52, 29),
(19, 56, 30),
(19, 58, 31),
(19, 59, 32),
(19, 60, 33),
(19, 64, 34),
(19, 69, 35),
(19, 70, 36),
(19, 71, 37),
(19, 72, 38),
(19, 76, 39),
(19, 81, 40),
(19, 82, 41),
(19, 84, 42),
(19, 85, 43),
(19, 86, 44),
(19, 87, 45),
(19, 90, 46),
(19, 91, 47),
(19, 92, 48),
(19, 93, 49),
(19, 94, 50),
(19, 95, 51),
(19, 98, 52),
(19, 100, 53),
(19, 101, 54),
(19, 102, 55),
(19, 103, 56),
(19, 104, 57),
(19, 106, 58),
(19, 107, 59),
(19, 109, 60),
(19, 110, 61),
(19, 111, 62),
(19, 112, 63),
(19, 114, 64),
(19, 115, 65),
(19, 117, 66),
(19, 119, 67),
(19, 120, 68),
(19, 121, 69),
(19, 122, 70),
(19, 124, 71),
(19, 125, 72),
(19, 126, 73),
(19, 127, 74),
(19, 130, 75),
(19, 131, 76),
(19, 136, 77),
(19, 137, 78),
(19, 138, 79),
(19, 139, 80),
(19, 140, 81),
(19, 142, 82),
(19, 143, 83),
(19, 145, 84),
(19, 149, 85),
(19, 150, 86),
(19, 154, 87),
(19, 156, 88),
(19, 157, 89),
(19, 158, 90),
(19, 160, 91),
(19, 163, 92),
(19, 164, 93),
(19, 165, 94),
(19, 166, 95),
(19, 169, 96),
(19, 171, 97),
(19, 172, 98),
(19, 173, 99),
(19, 177, 100),
(19, 179, 101),
(19, 181, 102),
(19, 185, 103),
(19, 186, 104),
(19, 187, 105),
(19, 188, 106),
(19, 195, 107),
(19, 196, 108),
(19, 197, 109),
(19, 199, 110),
(19, 200, 111),
(19, 201, 112),
(19, 207, 113),
(19, 208, 114),
(19, 212, 115),
(19, 213, 116),
(19, 214, 117),
(19, 219, 118),
(19, 220, 119),
(19, 222, 120),
(19, 223, 121),
(19, 224, 122),
(19, 225, 123),
(19, 228, 124),
(19, 229, 125),
(19, 231, 126),
(19, 233, 127),
(19, 235, 128),
(19, 237, 129),
(19, 238, 130),
(19, 243, 131),
(19, 244, 132),
(19, 245, 133),
(19, 246, 134),
(19, 250, 135),
(19, 251, 136),
(19, 252, 137),
(19, 253, 138),
(19, 260, 139),
(19, 261, 140),
(19, 262, 141),
(19, 263, 142),
(19, 267, 143),
(19, 268, 144),
(19, 269, 145),
(19, 271, 146),
(19, 276, 147),
(19, 277, 148),
(19, 278, 149),
(19, 279, 150),
(19, 281, 151),
(19, 282, 152),
(19, 285, 153),
(19, 286, 154),
(19, 288, 155),
(19, 289, 156),
(19, 290, 157),
(19, 293, 158),
(19, 294, 159),
(19, 296, 160),
(19, 298, 161),
(19, 300, 162),
(19, 301, 163),
(19, 304, 164),
(19, 306, 165),
(19, 307, 166),
(19, 308, 167),
(19, 309, 168),
(19, 315, 169),
(19, 319, 170),
(19, 320, 171),
(19, 321, 172),
(19, 322, 173),
(19, 324, 174),
(19, 327, 175),
(19, 329, 176),
(19, 330, 177),
(19, 333, 178),
(19, 335, 179),
(19, 336, 180),
(19, 337, 181),
(19, 338, 182),
(19, 339, 183),
(19, 342, 184),
(19, 345, 185),
(19, 346, 186),
(19, 347, 187),
(19, 349, 188),
(19, 351, 189),
(19, 352, 190),
(19, 353, 191),
(19, 354, 192),
(19, 356, 193),
(19, 358, 194),
(19, 361, 195),
(19, 363, 196),
(19, 365, 197),
(19, 366, 198),
(19, 369, 199),
(19, 373, 200),
(19, 377, 201),
(19, 381, 202),
(20, 2, 0),
(20, 3, 1),
(20, 4, 2),
(20, 9, 3),
(20, 12, 4),
(20, 13, 5),
(20, 14, 6),
(20, 16, 7),
(20, 21, 8),
(20, 22, 9),
(20, 25, 10),
(20, 26, 11),
(20, 29, 12),
(20, 30, 13),
(20, 31, 14),
(20, 32, 15),
(20, 33, 16),
(20, 34, 17),
(20, 36, 18),
(20, 37, 19),
(20, 40, 20),
(20, 44, 21),
(20, 47, 22),
(20, 48, 23),
(20, 50, 24),
(20, 51, 25),
(20, 54, 26),
(20, 55, 27),
(20, 57, 28),
(20, 59, 29),
(20, 60, 30),
(20, 63, 31),
(20, 65, 32),
(20, 68, 33),
(20, 69, 34),
(20, 73, 35),
(20, 74, 36),
(20, 75, 37),
(20, 76, 38),
(20, 77, 39),
(20, 79, 40),
(20, 81, 41),
(20, 85, 42),
(20, 86, 43),
(20, 87, 44),
(20, 88, 45),
(20, 89, 46),
(20, 90, 47),
(20, 92, 48),
(20, 93, 49),
(20, 94, 50),
(20, 97, 51),
(20, 98, 52),
(20, 99, 53),
(20, 101, 54),
(20, 102, 55),
(20, 105, 56),
(20, 113, 57),
(20, 114, 58),
(20, 117, 59),
(20, 118, 60),
(20, 119, 61),
(20, 120, 62),
(20, 121, 63),
(20, 122, 64),
(20, 123, 65),
(20, 124, 66),
(20, 125, 67),
(20, 126, 68),
(20, 127, 69),
(20, 128, 70),
(20, 129, 71),
(20, 132, 72),
(20, 133, 73),
(20, 134, 74),
(20, 142, 75),
(20, 144, 76),
(20, 145, 77),
(20, 147, 78),
(20, 148, 79),
(20, 149, 80),
(20, 153, 81),
(20, 156, 82),
(20, 157, 83),
(20, 159, 84),
(20, 160, 85),
(20, 161, 86),
(20, 162, 87),
(20, 164, 88),
(20, 167, 89),
(20, 169, 90),
(20, 172, 91),
(20, 173, 92),
(20, 176, 93),
(20, 178, 94),
(20, 179, 95),
(20, 181, 96),
(20, 182, 97),
(20, 183, 98),
(20, 185, 99),
(20, 186, 100),
(20, 187, 101),
(20, 189, 102),
(20, 191, 103),
(20, 195, 104),
(20, 196, 105),
(20, 198, 106),
(20, 200, 107),
(20, 201, 108),
(20, 202, 109),
(20, 203, 110),
(20, 204, 111),
(20, 205, 112),
(20, 206, 113),
(20, 208, 114),
(20, 209, 115),
(20, 210, 116),
(20, 211, 117),
(20, 212, 118),
(20, 213, 119),
(20, 214, 120),
(20, 215, 121),
(20, 218, 122),
(20, 224, 123),
(20, 226, 124),
(20, 227, 125),
(20, 230, 126),
(20, 231, 127),
(20, 234, 128),
(20, 235, 129),
(20, 244, 130),
(20, 245, 131),
(20, 246, 132),
(20, 248, 133),
(20, 249, 134),
(20, 250, 135),
(20, 251, 136),
(20, 252, 137),
(20, 254, 138),
(20, 255, 139),
(20, 259, 140),
(20, 263, 141),
(20, 264, 142),
(20, 268, 143),
(20, 273, 144),
(20, 276, 145),
(20, 278, 146),
(20, 279, 147),
(20, 283, 148),
(20, 287, 149),
(20, 291, 150),
(20, 293, 151),
(20, 295, 152),
(20, 297, 153),
(20, 298, 154),
(20, 299, 155),
(20, 300, 156),
(20, 302, 157),
(20, 303, 158),
(20, 306, 159),
(20, 307, 160),
(20, 308, 161),
(20, 309, 162),
(20, 312, 163),
(20, 315, 164),
(20, 317, 165),
(20, 320, 166),
(20, 322, 167),
(20, 324, 168),
(20, 326, 169),
(20, 329, 170),
(20, 331, 171),
(20, 332, 172),
(20, 333, 173),
(20, 334, 174),
(20, 335, 175),
(20, 336, 176),
(20, 341, 177),
(20, 342, 178),
(20, 347, 179),
(20, 350, 180),
(20, 357, 181),
(20, 359, 182),
(20, 360, 183),
(20, 362, 184),
(20, 364, 185),
(20, 365, 186),
(20, 367, 187),
(20, 368, 188),
(20, 369, 189),
(20, 370, 190),
(20, 374, 191),
(20, 375, 192),
(20, 377, 193),
(20, 382, 194),
(21, 2, 0),
(21, 3, 1),
(21, 7, 2),
(21, 8, 3),
(21, 9, 4),
(21, 11, 5),
(21, 14, 6),
(21, 15, 7),
(21, 19, 8),
(21, 20, 9),
(21, 21, 10),
(21, 22, 11),
(21, 26, 12),
(21, 30, 13),
(21, 31, 14),
(21, 32, 15),
(21, 33, 16),
(21, 35, 17),
(21, 36, 18),
(21, 38, 19),
(21, 39, 20),
(21, 40, 21),
(21, 41, 22),
(21, 43, 23),
(21, 45, 24),
(21, 50, 25),
(21, 52, 26),
(21, 53, 27),
(21, 54, 28),
(21, 55, 29),
(21, 56, 30),
(21, 57, 31),
(21, 60, 32),
(21, 61, 33),
(21, 64, 34),
(21, 66, 35),
(21, 68, 36),
(21, 69, 37),
(21, 71, 38),
(21, 74, 39),
(21, 77, 40),
(21, 78, 41),
(21, 79, 42),
(21, 80, 43),
(21, 81, 44),
(21, 84, 45),
(21, 85, 46),
(21, 86, 47),
(21, 87, 48),
(21, 89, 49),
(21, 90, 50),
(21, 91, 51),
(21, 92, 52),
(21, 93, 53),
(21, 95, 54),
(21, 96, 55),
(21, 102, 56),
(21, 103, 57),
(21, 104, 58),
(21, 108, 59),
(21, 109, 60),
(21, 111, 61),
(21, 112, 62),
(21, 114, 63),
(21, 116, 64),
(21, 117, 65),
(21, 119, 66),
(21, 120, 67),
(21, 125, 68),
(21, 128, 69),
(21, 129, 70),
(21, 131, 71),
(21, 132, 72),
(21, 134, 73),
(21, 135, 74),
(21, 141, 75),
(21, 142, 76),
(21, 143, 77),
(21, 145, 78),
(21, 148, 79),
(21, 152, 80),
(21, 154, 81),
(21, 157, 82),
(21, 160, 83),
(21, 163, 84),
(21, 164, 85),
(21, 165, 86),
(21, 166, 87),
(21, 170, 88),
(21, 171, 89),
(21, 172, 90),
(21, 173, 91),
(21, 180, 92),
(21, 181, 93),
(21, 183, 94),
(21, 185, 95),
(21, 188, 96),
(21, 191, 97),
(21, 192, 98),
(21, 196, 99),
(21, 197, 100),
(21, 198, 101),
(21, 201, 102),
(21, 203, 103),
(21, 204, 104),
(21, 205, 105),
(21, 207, 106),
(21, 208, 107),
(21, 211, 108),
(21, 214, 109),
(21, 215, 110),
(21, 216, 111),
(21, 217, 112),
(21, 220, 113),
(21, 224, 114),
(21, 225, 115),
(21, 226, 116),
(21, 232, 117),
(21, 235, 118),
(21, 236, 119),
(21, 237, 120),
(21, 238, 121),
(21, 239, 122),
(21, 241, 123),
(21, 242, 124),
(21, 244, 125),
(21, 248, 126),
(21, 250, 127),
(21, 255, 128),
(21, 256, 129),
(21, 257, 130),
(21, 259, 131),
(21, 261, 132),
(21, 264, 133),
(21, 265, 134),
(21, 268, 135),
(21, 269, 136),
(21, 274, 137),
(21, 275, 138),
(21, 277, 139),
(21, 280, 140),
(21, 287, 141),
(21, 288, 142),
(21, 289, 143),
(21, 291, 144),
(21, 293, 145),
(21, 296, 146),
(21, 297, 147),
(21, 298, 148),
(21, 299, 149),
(21, 300, 150),
(21, 303, 151),
(21, 304, 152),
(21, 305, 153),
(21, 307, 154),
(21, 308, 155),
(21, 309, 156),
(21, 310, 157),
(21, 313, 158),
(21, 314, 159),
(21, 318, 160),
(21, 319, 161),
(21, 323, 162),
(21, 324, 163),
(21, 325, 164),
(21, 326, 165),
(21, 327, 166),
(21, 328, 167),
(21, 332, 168),
(21, 333, 169),
(21, 334, 170),
(21, 335, 171),
(21, 337, 172),
(21, 338, 173),
(21, 339, 174),
(21, 340, 175),
(21, 341, 176),
(21, 347, 177),
(21, 348, 178),
(21, 353, 179),
(21, 355, 180),
(21, 357, 181),
(21, 358, 182),
(21, 361, 183),
(21, 364, 184),
(21, 368, 185),
(21, 370, 186),
(21, 371, 187),
(21, 372, 188),
(21, 375, 189),
(21, 376, 190),
(21, 377, 191),
(21, 380, 192),
(22, 2, 0),
(22, 3, 1),
(22, 5, 2),
(22, 8, 3),
(22, 9, 4),
(22, 10, 5),
(22, 11, 6),
(22, 12, 7),
(22, 14, 8),
(22, 17, 9),
(22, 18, 10),
(22, 19, 11),
(22, 21, 12),
(22, 22, 13),
(22, 24, 14),
(22, 29, 15),
(22, 30, 16),
(22, 31, 17),
(22, 32, 18),
(22, 35, 19),
(22, 38, 20),
(22, 39, 21),
(22, 40, 22),
(22, 43, 23),
(22, 44, 24),
(22, 48, 25),
(22, 49, 26),
(22, 50, 27),
(22, 51, 28),
(22, 52, 29),
(22, 53, 30),
(22, 54, 31),
(22, 57, 32),
(22, 59, 33),
(22, 60, 34),
(22, 63, 35),
(22, 64, 36),
(22, 66, 37),
(22, 67, 38),
(22, 68, 39),
(22, 69, 40),
(22, 71, 41),
(22, 72, 42),
(22, 74, 43),
(22, 76, 44),
(22, 77, 45),
(22, 80, 46),
(22, 83, 47),
(22, 84, 48),
(22, 85, 49),
(22, 87, 50),
(22, 91, 51),
(22, 92, 52),
(22, 93, 53),
(22, 94, 54),
(22, 97, 55),
(22, 98, 56),
(22, 102, 57),
(22, 103, 58),
(22, 106, 59),
(22, 107, 60),
(22, 108, 61),
(22, 109, 62),
(22, 113, 63),
(22, 114, 64),
(22, 116, 65),
(22, 117, 66),
(22, 119, 67),
(22, 120, 68),
(22, 122, 69),
(22, 125, 70),
(22, 126, 71),
(22, 130, 72),
(22, 131, 73),
(22, 133, 74),
(22, 134, 75),
(22, 136, 76),
(22, 138, 77),
(22, 144, 78),
(22, 145, 79),
(22, 146, 80),
(22, 148, 81),
(22, 153, 82),
(22, 157, 83),
(22, 159, 84),
(22, 160, 85),
(22, 163, 86),
(22, 165, 87),
(22, 166, 88),
(22, 167, 89),
(22, 168, 90),
(22, 169, 91),
(22, 170, 92),
(22, 171, 93),
(22, 172, 94),
(22, 174, 95),
(22, 175, 96),
(22, 176, 97),
(22, 177, 98),
(22, 178, 99),
(22, 179, 100),
(22, 180, 101),
(22, 181, 102),
(22, 182, 103),
(22, 185, 104),
(22, 187, 105),
(22, 191, 106),
(22, 194, 107),
(22, 195, 108),
(22, 199, 109),
(22, 200, 110),
(22, 202, 111),
(22, 203, 112),
(22, 204, 113),
(22, 208, 114),
(22, 209, 115),
(22, 214, 116),
(22, 217, 117),
(22, 219, 118),
(22, 220, 119),
(22, 221, 120),
(22, 224, 121),
(22, 225, 122),
(22, 226, 123),
(22, 228, 124),
(22, 230, 125),
(22, 231, 126),
(22, 233, 127),
(22, 234, 128),
(22, 236, 129),
(22, 237, 130),
(22, 238, 131),
(22, 240, 132),
(22, 241, 133),
(22, 244, 134),
(22, 247, 135),
(22, 250, 136),
(22, 252, 137),
(22, 254, 138),
(22, 255, 139),
(22, 257, 140),
(22, 258, 141),
(22, 259, 142),
(22, 260, 143),
(22, 261, 144),
(22, 263, 145),
(22, 265, 146),
(22, 267, 147),
(22, 268, 148),
(22, 269, 149),
(22, 271, 150),
(22, 272, 151),
(22, 273, 152),
(22, 278, 153),
(22, 280, 154),
(22, 281, 155),
(22, 282, 156),
(22, 284, 157),
(22, 285, 158),
(22, 286, 159),
(22, 289, 160),
(22, 290, 161),
(22, 291, 162),
(22, 292, 163),
(22, 293, 164),
(22, 296, 165),
(22, 298, 166),
(22, 299, 167),
(22, 300, 168),
(22, 301, 169),
(22, 302, 170),
(22, 303, 171),
(22, 304, 172),
(22, 306, 173),
(22, 308, 174),
(22, 310, 175),
(22, 311, 176),
(22, 312, 177),
(22, 315, 178),
(22, 318, 179),
(22, 319, 180),
(22, 320, 181),
(22, 322, 182),
(22, 326, 183),
(22, 328, 184),
(22, 330, 185),
(22, 334, 186),
(22, 335, 187),
(22, 336, 188),
(22, 338, 189),
(22, 339, 190),
(22, 340, 191),
(22, 341, 192),
(22, 346, 193),
(22, 347, 194),
(22, 349, 195),
(22, 350, 196),
(22, 351, 197),
(22, 352, 198),
(22, 358, 199),
(22, 359, 200),
(22, 362, 201),
(22, 363, 202),
(22, 364, 203),
(22, 366, 204),
(22, 367, 205),
(22, 368, 206),
(22, 369, 207),
(22, 374, 208),
(22, 375, 209),
(22, 376, 210),
(22, 377, 211),
(22, 379, 212),
(22, 382, 213),
(23, 2, 0),
(23, 3, 1),
(23, 4, 2),
(23, 5, 3),
(23, 6, 4),
(23, 9, 5),
(23, 20, 6),
(23, 21, 7),
(23, 22, 8),
(23, 23, 9),
(23, 24, 10),
(23, 27, 11),
(23, 29, 12),
(23, 33, 13),
(23, 34, 14),
(23, 35, 15),
(23, 36, 16),
(23, 42, 17),
(23, 44, 18),
(23, 47, 19),
(23, 51, 20),
(23, 55, 21),
(23, 61, 22),
(23, 67, 23),
(23, 68, 24),
(23, 72, 25),
(23, 87, 26),
(23, 88, 27),
(23, 92, 28),
(23, 93, 29),
(23, 95, 30),
(23, 96, 31),
(23, 97, 32),
(23, 100, 33),
(23, 101, 34),
(23, 112, 35),
(23, 113, 36),
(23, 118, 37),
(23, 121, 38),
(23, 122, 39),
(23, 126, 40),
(23, 132, 41),
(23, 134, 42),
(23, 140, 43),
(23, 141, 44),
(23, 146, 45),
(23, 147, 46),
(23, 163, 47),
(23, 165, 48),
(23, 168, 49),
(23, 171, 50),
(23, 174, 51),
(23, 187, 52),
(23, 203, 53),
(23, 206, 54),
(23, 208, 55),
(23, 209, 56),
(23, 217, 57),
(23, 220, 58),
(23, 221, 59),
(23, 223, 60),
(23, 234, 61),
(23, 237, 62),
(23, 241, 63),
(23, 243, 64),
(23, 244, 65),
(23, 245, 66),
(23, 248, 67),
(23, 251, 68),
(23, 265, 69),
(23, 274, 70),
(23, 276, 71),
(23, 278, 72),
(23, 283, 73),
(23, 285, 74),
(23, 288, 75),
(23, 289, 76),
(23, 290, 77),
(23, 292, 78),
(23, 294, 79),
(23, 298, 80),
(23, 302, 81),
(23, 303, 82),
(23, 305, 83),
(23, 307, 84),
(23, 312, 85),
(23, 314, 86),
(23, 316, 87),
(23, 323, 88),
(23, 328, 89),
(23, 329, 90),
(23, 332, 91),
(23, 333, 92),
(23, 341, 93),
(23, 347, 94),
(23, 348, 95),
(23, 350, 96),
(23, 351, 97),
(23, 372, 98),
(23, 375, 99),
(23, 379, 100),
(24, 3, 0),
(24, 5, 1),
(24, 8, 2),
(24, 9, 3),
(24, 10, 4),
(24, 13, 5),
(24, 20, 6),
(24, 21, 7),
(24, 23, 8),
(24, 27, 9),
(24, 29, 10),
(24, 30, 11),
(24, 31, 12),
(24, 32, 13),
(24, 37, 14),
(24, 38, 15),
(24, 39, 16),
(24, 40, 17),
(24, 41, 18),
(24, 44, 19),
(24, 48, 20),
(24, 49, 21),
(24, 53, 22),
(24, 54, 23),
(24, 55, 24),
(24, 57, 25),
(24, 61, 26),
(24, 62, 27),
(24, 63, 28),
(24, 65, 29),
(24, 66, 30),
(24, 67, 31),
(24, 68, 32),
(24, 73, 33),
(24, 74, 34),
(24, 75, 35),
(24, 77, 36),
(24, 78, 37),
(24, 79, 38),
(24, 80, 39),
(24, 83, 40),
(24, 88, 41),
(24, 89, 42),
(24, 96, 43),
(24, 97, 44),
(24, 99, 45),
(24, 105, 46),
(24, 108, 47),
(24, 113, 48),
(24, 116, 49),
(24, 118, 50),
(24, 123, 51),
(24, 128, 52),
(24, 129, 53),
(24, 132, 54),
(24, 133, 55),
(24, 134, 56),
(24, 135, 57),
(24, 141, 58),
(24, 144, 59),
(24, 146, 60),
(24, 147, 61),
(24, 148, 62),
(24, 151, 63),
(24, 152, 64),
(24, 153, 65),
(24, 155, 66),
(24, 159, 67),
(24, 161, 68),
(24, 162, 69),
(24, 167, 70),
(24, 168, 71),
(24, 170, 72),
(24, 174, 73),
(24, 175, 74),
(24, 176, 75),
(24, 178, 76),
(24, 180, 77),
(24, 182, 78),
(24, 183, 79),
(24, 184, 80),
(24, 189, 81),
(24, 190, 82),
(24, 191, 83),
(24, 192, 84),
(24, 193, 85),
(24, 194, 86),
(24, 198, 87),
(24, 202, 88),
(24, 203, 89),
(24, 204, 90),
(24, 205, 91),
(24, 206, 92),
(24, 209, 93),
(24, 210, 94),
(24, 211, 95),
(24, 215, 96),
(24, 216, 97),
(24, 217, 98),
(24, 218, 99),
(24, 221, 100),
(24, 226, 101),
(24, 227, 102),
(24, 230, 103),
(24, 232, 104),
(24, 234, 105),
(24, 236, 106),
(24, 239, 107),
(24, 240, 108),
(24, 241, 109),
(24, 242, 110),
(24, 247, 111),
(24, 248, 112),
(24, 249, 113),
(24, 254, 114),
(24, 255, 115),
(24, 256, 116),
(24, 257, 117),
(24, 258, 118),
(24, 259, 119),
(24, 264, 120),
(24, 265, 121),
(24, 266, 122),
(24, 270, 123),
(24, 272, 124),
(24, 273, 125),
(24, 274, 126),
(24, 275, 127),
(24, 280, 128),
(24, 283, 129),
(24, 284, 130),
(24, 287, 131),
(24, 291, 132),
(24, 292, 133),
(24, 295, 134),
(24, 297, 135),
(24, 299, 136),
(24, 302, 137),
(24, 303, 138),
(24, 305, 139),
(24, 310, 140),
(24, 311, 141),
(24, 312, 142),
(24, 313, 143),
(24, 314, 144),
(24, 316, 145),
(24, 317, 146),
(24, 318, 147),
(24, 323, 148),
(24, 325, 149),
(24, 326, 150),
(24, 328, 151),
(24, 331, 152),
(24, 332, 153),
(24, 334, 154),
(24, 340, 155),
(24, 341, 156),
(24, 343, 157),
(24, 344, 158),
(24, 348, 159),
(24, 350, 160),
(24, 355, 161),
(24, 357, 162),
(24, 359, 163),
(24, 360, 164),
(24, 362, 165),
(24, 364, 166),
(24, 367, 167),
(24, 368, 168),
(24, 370, 169),
(24, 371, 170),
(24, 372, 171),
(24, 374, 172),
(24, 375, 173),
(24, 376, 174),
(24, 378, 175),
(24, 379, 176),
(24, 380, 177),
(24, 382, 178),
(25, 4, 0),
(25, 5, 1),
(25, 9, 2),
(25, 13, 3),
(25, 14, 4),
(25, 15, 5),
(25, 18, 6),
(25, 27, 7),
(25, 31, 8),
(25, 33, 9),
(25, 34, 10),
(25, 35, 11),
(25, 38, 12),
(25, 39, 13),
(25, 41, 14),
(25, 42, 15),
(25, 47, 16),
(25, 50, 17),
(25, 51, 18),
(25, 55, 19),
(25, 60, 20),
(25, 61, 21),
(25, 62, 22),
(25, 65, 23),
(25, 66, 24),
(25, 68, 25),
(25, 69, 26),
(25, 71, 27),
(25, 73, 28),
(25, 75, 29),
(25, 76, 30),
(25, 79, 31),
(25, 83, 32),
(25, 87, 33),
(25, 89, 34),
(25, 93, 35),
(25, 97, 36),
(25, 98, 37),
(25, 101, 38),
(25, 103, 39),
(25, 106, 40),
(25, 108, 41),
(25, 112, 42),
(25, 113, 43),
(25, 119, 44),
(25, 120, 45),
(25, 121, 46),
(25, 122, 47),
(25, 123, 48),
(25, 130, 49),
(25, 131, 50),
(25, 134, 51),
(25, 142, 52),
(25, 147, 53),
(25, 150, 54),
(25, 151, 55),
(25, 153, 56),
(25, 160, 57),
(25, 167, 58),
(25, 168, 59),
(25, 169, 60),
(25, 176, 61),
(25, 182, 62),
(25, 184, 63),
(25, 190, 64),
(25, 193, 65),
(25, 196, 66),
(25, 199, 67),
(25, 202, 68),
(25, 203, 69),
(25, 204, 70),
(25, 207, 71),
(25, 210, 72),
(25, 212, 73),
(25, 213, 74),
(25, 219, 75),
(25, 222, 76),
(25, 225, 77),
(25, 227, 78),
(25, 228, 79),
(25, 229, 80),
(25, 230, 81),
(25, 232, 82),
(25, 233, 83),
(25, 234, 84),
(25, 236, 85),
(25, 239, 86),
(25, 240, 87),
(25, 242, 88),
(25, 244, 89),
(25, 245, 90),
(25, 247, 91),
(25, 250, 92),
(25, 251, 93),
(25, 256, 94),
(25, 257, 95),
(25, 258, 96),
(25, 259, 97),
(25, 263, 98),
(25, 265, 99),
(25, 270, 100),
(25, 274, 101),
(25, 280, 102),
(25, 290, 103),
(25, 291, 104),
(25, 292, 105),
(25, 298, 106),
(25, 299, 107),
(25, 302, 108),
(25, 303, 109),
(25, 310, 110),
(25, 312, 111),
(25, 316, 112),
(25, 317, 113),
(25, 319, 114),
(25, 320, 115),
(25, 321, 116),
(25, 326, 117),
(25, 331, 118),
(25, 333, 119),
(25, 335, 120),
(25, 336, 121),
(25, 338, 122),
(25, 339, 123),
(25, 340, 124),
(25, 342, 125),
(25, 352, 126),
(25, 354, 127),
(25, 357, 128),
(25, 362, 129),
(25, 364, 130),
(25, 365, 131),
(25, 366, 132),
(25, 376, 133),
(25, 378, 134),
(25, 380, 135),
(26, 4, 0),
(26, 9, 1),
(26, 13, 2),
(26, 18, 3),
(26, 22, 4),
(26, 24, 5),
(26, 27, 6),
(26, 28, 7),
(26, 29, 8),
(26, 34, 9),
(26, 36, 10),
(26, 37, 11),
(26, 38, 12),
(26, 42, 13),
(26, 46, 14),
(26, 51, 15),
(26, 52, 16),
(26, 53, 17),
(26, 58, 18),
(26, 62, 19),
(26, 64, 20),
(26, 72, 21),
(26, 73, 22),
(26, 88, 23),
(26, 89, 24),
(26, 93, 25),
(26, 94, 26),
(26, 96, 27),
(26, 97, 28),
(26, 99, 29),
(26, 100, 30),
(26, 102, 31),
(26, 108, 32),
(26, 125, 33),
(26, 126, 34),
(26, 127, 35),
(26, 140, 36),
(26, 142, 37),
(26, 143, 38),
(26, 146, 39),
(26, 147, 40),
(26, 150, 41),
(26, 151, 42),
(26, 163, 43),
(26, 164, 44),
(26, 165, 45),
(26, 166, 46),
(26, 167, 47),
(26, 168, 48),
(26, 169, 49),
(26, 177, 50),
(26, 182, 51),
(26, 184, 52),
(26, 192, 53),
(26, 196, 54),
(26, 197, 55),
(26, 198, 56),
(26, 202, 57),
(26, 203, 58),
(26, 211, 59),
(26, 212, 60),
(26, 214, 61),
(26, 217, 62),
(26, 218, 63),
(26, 219, 64),
(26, 220, 65),
(26, 222, 66),
(26, 224, 67),
(26, 225, 68),
(26, 226, 69),
(26, 227, 70),
(26, 229, 71),
(26, 230, 72),
(26, 234, 73),
(26, 236, 74),
(26, 239, 75),
(26, 241, 76),
(26, 243, 77),
(26, 244, 78),
(26, 246, 79),
(26, 252, 80),
(26, 255, 81),
(26, 256, 82),
(26, 259, 83),
(26, 263, 84),
(26, 266, 85),
(26, 271, 86),
(26, 278, 87),
(26, 284, 88),
(26, 292, 89),
(26, 295, 90),
(26, 298, 91),
(26, 300, 92),
(26, 304, 93),
(26, 305, 94),
(26, 307, 95),
(26, 309, 96),
(26, 310, 97),
(26, 312, 98),
(26, 317, 99),
(26, 318, 100),
(26, 319, 101),
(26, 324, 102),
(26, 325, 103),
(26, 326, 104),
(26, 327, 105),
(26, 328, 106),
(26, 330, 107),
(26, 333, 108),
(26, 338, 109),
(26, 340, 110),
(26, 343, 111),
(26, 351, 112),
(26, 356, 113),
(26, 357, 114),
(26, 360, 115),
(26, 361, 116),
(26, 363, 117),
(26, 364, 118),
(26, 371, 119),
(26, 374, 120),
(26, 375, 121),
(26, 376, 122),
(27, 4, 0),
(27, 9, 1),
(27, 14, 2),
(27, 17, 3),
(27, 24, 4),
(27, 31, 5),
(27, 33, 6),
(27, 34, 7),
(27, 43, 8),
(27, 46, 9),
(27, 48, 10),
(27, 49, 11),
(27, 50, 12),
(27, 55, 13),
(27, 57, 14),
(27, 58, 15),
(27, 59, 16),
(27, 61, 17),
(27, 62, 18),
(27, 64, 19),
(27, 73, 20),
(27, 76, 21),
(27, 77, 22),
(27, 78, 23),
(27, 79, 24),
(27, 87, 25),
(27, 89, 26),
(27, 91, 27),
(27, 92, 28),
(27, 97, 29),
(27, 99, 30),
(27, 100, 31),
(27, 104, 32),
(27, 108, 33),
(27, 110, 34),
(27, 111, 35),
(27, 112, 36),
(27, 114, 37),
(27, 115, 38),
(27, 123, 39),
(27, 124, 40),
(27, 127, 41),
(27, 134, 42),
(27, 135, 43),
(27, 138, 44),
(27, 139, 45),
(27, 142, 46),
(27, 147, 47),
(27, 149, 48),
(27, 154, 49),
(27, 159, 50),
(27, 173, 51),
(27, 174, 52),
(27, 175, 53),
(27, 179, 54),
(27, 180, 55),
(27, 185, 56),
(27, 188, 57),
(27, 189, 58),
(27, 191, 59),
(27, 192, 60),
(27, 198, 61),
(27, 200, 62),
(27, 201, 63),
(27, 209, 64),
(27, 210, 65),
(27, 211, 66),
(27, 212, 67),
(27, 215, 68),
(27, 216, 69),
(27, 217, 70),
(27, 219, 71),
(27, 224, 72),
(27, 226, 73),
(27, 235, 74),
(27, 236, 75),
(27, 240, 76),
(27, 244, 77),
(27, 245, 78),
(27, 246, 79),
(27, 250, 80),
(27, 251, 81),
(27, 253, 82),
(27, 255, 83),
(27, 257, 84),
(27, 258, 85),
(27, 261, 86),
(27, 267, 87),
(27, 269, 88),
(27, 272, 89),
(27, 274, 90),
(27, 278, 91),
(27, 280, 92),
(27, 282, 93),
(27, 283, 94),
(27, 288, 95),
(27, 290, 96),
(27, 293, 97),
(27, 300, 98),
(27, 301, 99),
(27, 306, 100),
(27, 307, 101),
(27, 309, 102),
(27, 310, 103),
(27, 318, 104),
(27, 323, 105),
(27, 324, 106),
(27, 325, 107),
(27, 327, 108),
(27, 330, 109),
(27, 332, 110),
(27, 335, 111),
(27, 336, 112),
(27, 338, 113),
(27, 339, 114),
(27, 345, 115),
(27, 348, 116),
(27, 351, 117),
(27, 352, 118),
(27, 353, 119),
(27, 355, 120),
(27, 357, 121),
(27, 364, 122),
(27, 367, 123),
(27, 368, 124),
(27, 372, 125),
(27, 376, 126),
(27, 378, 127),
(28, 5, 0),
(28, 7, 1),
(28, 12, 2),
(28, 18, 3),
(28, 20, 4),
(28, 21, 5),
(28, 22, 6),
(28, 27, 7),
(28, 29, 8),
(28, 35, 9),
(28, 37, 10),
(28, 38, 11),
(28, 40, 12),
(28, 42, 13),
(28, 47, 14),
(28, 53, 15),
(28, 56, 16),
(28, 63, 17),
(28, 65, 18),
(28, 67, 19),
(28, 69, 20),
(28, 72, 21),
(28, 75, 22),
(28, 80, 23),
(28, 85, 24),
(28, 86, 25),
(28, 93, 26),
(28, 94, 27),
(28, 101, 28),
(28, 102, 29),
(28, 103, 30),
(28, 117, 31),
(28, 119, 32),
(28, 120, 33),
(28, 121, 34),
(28, 130, 35),
(28, 137, 36),
(28, 145, 37),
(28, 146, 38),
(28, 148, 39),
(28, 151, 40),
(28, 157, 41),
(28, 158, 42),
(28, 160, 43),
(28, 161, 44),
(28, 164, 45),
(28, 165, 46),
(28, 166, 47),
(28, 167, 48),
(28, 168, 49),
(28, 170, 50),
(28, 172, 51),
(28, 176, 52),
(28, 178, 53),
(28, 182, 54),
(28, 183, 55),
(28, 187, 56),
(28, 194, 57),
(28, 195, 58),
(28, 202, 59),
(28, 204, 60),
(28, 205, 61),
(28, 208, 62),
(28, 218, 63),
(28, 222, 64),
(28, 229, 65),
(28, 231, 66),
(28, 232, 67),
(28, 233, 68),
(28, 239, 69),
(28, 242, 70),
(28, 247, 71),
(28, 249, 72),
(28, 252, 73),
(28, 256, 74),
(28, 260, 75),
(28, 262, 76),
(28, 263, 77),
(28, 264, 78),
(28, 265, 79),
(28, 268, 80),
(28, 270, 81),
(28, 271, 82),
(28, 276, 83),
(28, 277, 84),
(28, 281, 85),
(28, 284, 86),
(28, 285, 87),
(28, 286, 88),
(28, 287, 89),
(28, 291, 90),
(28, 294, 91),
(28, 299, 92),
(28, 302, 93),
(28, 305, 94),
(28, 313, 95),
(28, 314, 96),
(28, 315, 97),
(28, 317, 98),
(28, 322, 99),
(28, 326, 100),
(28, 329, 101),
(28, 331, 102),
(28, 333, 103),
(28, 337, 104),
(28, 340, 105),
(28, 343, 106),
(28, 347, 107),
(28, 349, 108),
(28, 350, 109),
(28, 354, 110),
(28, 358, 111),
(28, 359, 112),
(28, 360, 113),
(28, 366, 114),
(28, 369, 115),
(28, 370, 116),
(28, 373, 117),
(28, 380, 118),
(28, 382, 119),
(29, 6, 0),
(29, 8, 1),
(29, 10, 2),
(29, 11, 3),
(29, 12, 4),
(29, 17, 5),
(29, 23, 6),
(29, 25, 7),
(29, 31, 8),
(29, 33, 9),
(29, 35, 10),
(29, 39, 11),
(29, 41, 12),
(29, 44, 13),
(29, 45, 14),
(29, 49, 15),
(29, 50, 16),
(29, 56, 17),
(29, 59, 18),
(29, 60, 19),
(29, 63, 20),
(29, 65, 21),
(29, 66, 22),
(29, 68, 23),
(29, 70, 24),
(29, 71, 25),
(29, 75, 26),
(29, 80, 27),
(29, 81, 28),
(29, 83, 29),
(29, 84, 30),
(29, 85, 31),
(29, 87, 32),
(29, 91, 33),
(29, 95, 34),
(29, 101, 35),
(29, 103, 36),
(29, 104, 37),
(29, 105, 38),
(29, 109, 39),
(29, 110, 40),
(29, 113, 41),
(29, 114, 42),
(29, 115, 43),
(29, 119, 44),
(29, 121, 45),
(29, 123, 46),
(29, 124, 47),
(29, 134, 48),
(29, 135, 49),
(29, 141, 50),
(29, 145, 51),
(29, 148, 52),
(29, 152, 53),
(29, 153, 54),
(29, 161, 55),
(29, 162, 56),
(29, 170, 57),
(29, 171, 58),
(29, 174, 59),
(29, 179, 60),
(29, 180, 61),
(29, 185, 62),
(29, 187, 63),
(29, 188, 64),
(29, 190, 65),
(29, 193, 66),
(29, 194, 67),
(29, 199, 68),
(29, 201, 69),
(29, 205, 70),
(29, 206, 71),
(29, 207, 72),
(29, 210, 73),
(29, 215, 74),
(29, 216, 75),
(29, 221, 76),
(29, 235, 77),
(29, 237, 78),
(29, 238, 79),
(29, 240, 80),
(29, 242, 81),
(29, 245, 82),
(29, 247, 83),
(29, 248, 84),
(29, 249, 85),
(29, 250, 86),
(29, 254, 87),
(29, 257, 88),
(29, 258, 89),
(29, 260, 90),
(29, 265, 91),
(29, 268, 92),
(29, 269, 93),
(29, 270, 94),
(29, 273, 95),
(29, 281, 96),
(29, 286, 97),
(29, 287, 98),
(29, 289, 99),
(29, 290, 100),
(29, 291, 101),
(29, 296, 102),
(29, 297, 103),
(29, 302, 104),
(29, 314, 105),
(29, 316, 106),
(29, 323, 107),
(29, 331, 108),
(29, 336, 109),
(29, 339, 110),
(29, 341, 111),
(29, 342, 112),
(29, 345, 113),
(29, 346, 114),
(29, 348, 115),
(29, 352, 116),
(29, 355, 117),
(29, 362, 118),
(29, 365, 119),
(29, 366, 120),
(29, 367, 121),
(29, 378, 122),
(29, 379, 123),
(29, 381, 124),
(30, 7, 0),
(30, 11, 1),
(30, 16, 2),
(30, 19, 3),
(30, 21, 4),
(30, 22, 5),
(30, 24, 6),
(30, 26, 7),
(30, 29, 8),
(30, 30, 9),
(30, 36, 10),
(30, 40, 11),
(30, 45, 12),
(30, 46, 13),
(30, 49, 14),
(30, 52, 15),
(30, 58, 16),
(30, 59, 17),
(30, 67, 18),
(30, 70, 19),
(30, 74, 20),
(30, 80, 21),
(30, 81, 22),
(30, 90, 23),
(30, 92, 24),
(30, 95, 25),
(30, 96, 26),
(30, 99, 27),
(30, 100, 28),
(30, 104, 29),
(30, 105, 30),
(30, 110, 31),
(30, 111, 32),
(30, 116, 33),
(30, 127, 34),
(30, 128, 35),
(30, 129, 36),
(30, 132, 37),
(30, 135, 38),
(30, 136, 39),
(30, 139, 40),
(30, 140, 41),
(30, 144, 42),
(30, 145, 43),
(30, 146, 44),
(30, 148, 45),
(30, 149, 46),
(30, 154, 47),
(30, 155, 48),
(30, 157, 49),
(30, 158, 50),
(30, 161, 51),
(30, 164, 52),
(30, 165, 53),
(30, 166, 54),
(30, 171, 55),
(30, 172, 56),
(30, 173, 57),
(30, 175, 58),
(30, 178, 59),
(30, 180, 60),
(30, 181, 61),
(30, 183, 62),
(30, 185, 63),
(30, 187, 64),
(30, 188, 65),
(30, 189, 66),
(30, 191, 67),
(30, 195, 68),
(30, 198, 69),
(30, 206, 70),
(30, 211, 71),
(30, 215, 72),
(30, 218, 73),
(30, 220, 74),
(30, 221, 75),
(30, 226, 76),
(30, 246, 77),
(30, 248, 78),
(30, 249, 79),
(30, 252, 80),
(30, 253, 81),
(30, 254, 82),
(30, 261, 83),
(30, 264, 84),
(30, 266, 85),
(30, 267, 86),
(30, 268, 87),
(30, 273, 88),
(30, 276, 89),
(30, 277, 90),
(30, 278, 91),
(30, 283, 92),
(30, 284, 93),
(30, 287, 94),
(30, 288, 95),
(30, 297, 96),
(30, 300, 97),
(30, 308, 98),
(30, 313, 99),
(30, 314, 100),
(30, 315, 101),
(30, 322, 102),
(30, 324, 103),
(30, 325, 104),
(30, 328, 105),
(30, 329, 106),
(30, 334, 107),
(30, 337, 108),
(30, 341, 109),
(30, 343, 110),
(30, 344, 111),
(30, 347, 112),
(30, 348, 113),
(30, 351, 114),
(30, 355, 115),
(30, 361, 116),
(30, 363, 117),
(30, 367, 118),
(30, 368, 119),
(30, 370, 120),
(30, 372, 121),
(30, 374, 122),
(30, 381, 123),
(31, 8, 0),
(31, 11, 1),
(31, 40, 2),
(31, 41, 3),
(31, 43, 4),
(31, 46, 5),
(31, 50, 6),
(31, 54, 7),
(31, 57, 8),
(31, 58, 9),
(31, 62, 10),
(31, 69, 11),
(31, 74, 12),
(31, 77, 13),
(31, 80, 14),
(31, 81, 15),
(31, 82, 16),
(31, 91, 17),
(31, 106, 18),
(31, 117, 19),
(31, 123, 20),
(31, 124, 21),
(31, 125, 22),
(31, 135, 23),
(31, 145, 24),
(31, 149, 25),
(31, 154, 26),
(31, 156, 27),
(31, 159, 28),
(31, 160, 29),
(31, 166, 30),
(31, 175, 31),
(31, 179, 32),
(31, 182, 33),
(31, 185, 34),
(31, 188, 35),
(31, 189, 36),
(31, 190, 37),
(31, 191, 38),
(31, 194, 39),
(31, 196, 40),
(31, 197, 41),
(31, 199, 42),
(31, 201, 43),
(31, 202, 44),
(31, 204, 45),
(31, 213, 46),
(31, 218, 47),
(31, 222, 48),
(31, 225, 49),
(31, 226, 50),
(31, 227, 51),
(31, 228, 52),
(31, 230, 53),
(31, 238, 54),
(31, 252, 55),
(31, 257, 56),
(31, 263, 57),
(31, 267, 58),
(31, 268, 59),
(31, 269, 60),
(31, 270, 61),
(31, 272, 62),
(31, 281, 63),
(31, 282, 64),
(31, 284, 65),
(31, 286, 66),
(31, 291, 67),
(31, 297, 68),
(31, 300, 69),
(31, 304, 70),
(31, 309, 71),
(31, 310, 72),
(31, 315, 73),
(31, 317, 74),
(31, 320, 75),
(31, 322, 76),
(31, 325, 77),
(31, 327, 78),
(31, 330, 79),
(31, 334, 80),
(31, 342, 81),
(31, 344, 82),
(31, 352, 83),
(31, 356, 84),
(31, 360, 85),
(31, 364, 86),
(31, 365, 87),
(31, 376, 88),
(31, 381, 89),
(31, 382, 90),
(32, 14, 0),
(32, 17, 1),
(32, 19, 2),
(32, 25, 3),
(32, 32, 4),
(32, 38, 5),
(32, 45, 6),
(32, 52, 7),
(32, 53, 8),
(32, 59, 9),
(32, 64, 10),
(32, 65, 11),
(32, 66, 12),
(32, 70, 13),
(32, 71, 14),
(32, 76, 15),
(32, 78, 16),
(32, 86, 17),
(32, 98, 18),
(32, 99, 19),
(32, 103, 20),
(32, 105, 21),
(32, 110, 22),
(32, 111, 23),
(32, 114, 24),
(32, 119, 25),
(32, 120, 26),
(32, 127, 27),
(32, 131, 28),
(32, 133, 29),
(32, 136, 30),
(32, 137, 31),
(32, 138, 32),
(32, 142, 33),
(32, 148, 34),
(32, 150, 35),
(32, 152, 36),
(32, 153, 37),
(32, 157, 38),
(32, 158, 39),
(32, 162, 40),
(32, 170, 41),
(32, 176, 42),
(32, 177, 43),
(32, 178, 44),
(32, 181, 45),
(32, 186, 46),
(32, 192, 47),
(32, 193, 48),
(32, 200, 49),
(32, 210, 50),
(32, 214, 51),
(32, 219, 52),
(32, 231, 53),
(32, 233, 54),
(32, 242, 55),
(32, 246, 56),
(32, 253, 57),
(32, 254, 58),
(32, 256, 59),
(32, 258, 60),
(32, 261, 61),
(32, 262, 62),
(32, 266, 63),
(32, 280, 64),
(32, 287, 65),
(32, 296, 66),
(32, 299, 67),
(32, 301, 68),
(32, 308, 69),
(32, 311, 70),
(32, 313, 71),
(32, 318, 72),
(32, 319, 73),
(32, 321, 74),
(32, 335, 75),
(32, 338, 76),
(32, 340, 77),
(32, 343, 78),
(32, 345, 79),
(32, 349, 80),
(32, 354, 81),
(32, 357, 82),
(32, 358, 83),
(32, 361, 84),
(32, 369, 85);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_category_shop`
--

DROP TABLE IF EXISTS `ps_category_shop`;
CREATE TABLE `ps_category_shop` (
  `id_category` int(11) NOT NULL,
  `id_shop` int(11) NOT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_category_shop`
--

INSERT INTO `ps_category_shop` (`id_category`, `id_shop`, `position`) VALUES
(1, 1, 0),
(2, 1, 0),
(9, 1, 0),
(10, 1, 1),
(11, 1, 2),
(12, 1, 0),
(13, 1, 1),
(14, 1, 2),
(15, 1, 3),
(16, 1, 4),
(17, 1, 5),
(18, 1, 6),
(19, 1, 7),
(20, 1, 8),
(21, 1, 9),
(22, 1, 10),
(23, 1, 11),
(24, 1, 12),
(25, 1, 13),
(26, 1, 14),
(27, 1, 15),
(28, 1, 16),
(29, 1, 17),
(30, 1, 18),
(31, 1, 19),
(32, 1, 20);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_cms`
--

DROP TABLE IF EXISTS `ps_cms`;
CREATE TABLE `ps_cms` (
  `id_cms` int(10) UNSIGNED NOT NULL,
  `id_cms_category` int(10) UNSIGNED NOT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `indexation` tinyint(1) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_cms`
--

INSERT INTO `ps_cms` (`id_cms`, `id_cms_category`, `position`, `active`, `indexation`) VALUES
(1, 1, 0, 1, 0),
(2, 1, 1, 1, 0),
(3, 1, 2, 1, 0),
(4, 1, 3, 1, 0),
(5, 1, 4, 1, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_cms_block`
--

DROP TABLE IF EXISTS `ps_cms_block`;
CREATE TABLE `ps_cms_block` (
  `id_cms_block` int(10) UNSIGNED NOT NULL,
  `id_cms_category` int(10) UNSIGNED NOT NULL,
  `location` tinyint(1) UNSIGNED NOT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `display_store` tinyint(1) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_cms_block`
--

INSERT INTO `ps_cms_block` (`id_cms_block`, `id_cms_category`, `location`, `position`, `display_store`) VALUES
(1, 1, 0, 0, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_cms_block_lang`
--

DROP TABLE IF EXISTS `ps_cms_block_lang`;
CREATE TABLE `ps_cms_block_lang` (
  `id_cms_block` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(40) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_cms_block_lang`
--

INSERT INTO `ps_cms_block_lang` (`id_cms_block`, `id_lang`, `name`) VALUES
(1, 1, 'Informacja');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_cms_block_page`
--

DROP TABLE IF EXISTS `ps_cms_block_page`;
CREATE TABLE `ps_cms_block_page` (
  `id_cms_block_page` int(10) UNSIGNED NOT NULL,
  `id_cms_block` int(10) UNSIGNED NOT NULL,
  `id_cms` int(10) UNSIGNED NOT NULL,
  `is_category` tinyint(1) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_cms_block_page`
--

INSERT INTO `ps_cms_block_page` (`id_cms_block_page`, `id_cms_block`, `id_cms`, `is_category`) VALUES
(1, 1, 1, 0),
(2, 1, 2, 0),
(3, 1, 3, 0),
(4, 1, 4, 0),
(5, 1, 5, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_cms_block_shop`
--

DROP TABLE IF EXISTS `ps_cms_block_shop`;
CREATE TABLE `ps_cms_block_shop` (
  `id_cms_block` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_cms_block_shop`
--

INSERT INTO `ps_cms_block_shop` (`id_cms_block`, `id_shop`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_cms_category`
--

DROP TABLE IF EXISTS `ps_cms_category`;
CREATE TABLE `ps_cms_category` (
  `id_cms_category` int(10) UNSIGNED NOT NULL,
  `id_parent` int(10) UNSIGNED NOT NULL,
  `level_depth` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_cms_category`
--

INSERT INTO `ps_cms_category` (`id_cms_category`, `id_parent`, `level_depth`, `active`, `date_add`, `date_upd`, `position`) VALUES
(1, 0, 1, 1, '2018-12-03 17:18:01', '2018-12-03 17:18:01', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_cms_category_lang`
--

DROP TABLE IF EXISTS `ps_cms_category_lang`;
CREATE TABLE `ps_cms_category_lang` (
  `id_cms_category` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `name` varchar(128) NOT NULL,
  `description` text DEFAULT NULL,
  `link_rewrite` varchar(128) NOT NULL,
  `meta_title` varchar(128) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_cms_category_lang`
--

INSERT INTO `ps_cms_category_lang` (`id_cms_category`, `id_lang`, `id_shop`, `name`, `description`, `link_rewrite`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
(1, 1, 1, 'GĹĂłwna', '', 'glowna', '', '', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_cms_category_shop`
--

DROP TABLE IF EXISTS `ps_cms_category_shop`;
CREATE TABLE `ps_cms_category_shop` (
  `id_cms_category` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_cms_category_shop`
--

INSERT INTO `ps_cms_category_shop` (`id_cms_category`, `id_shop`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_cms_lang`
--

DROP TABLE IF EXISTS `ps_cms_lang`;
CREATE TABLE `ps_cms_lang` (
  `id_cms` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `meta_title` varchar(128) NOT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `content` longtext DEFAULT NULL,
  `link_rewrite` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_cms_lang`
--

INSERT INTO `ps_cms_lang` (`id_cms`, `id_lang`, `id_shop`, `meta_title`, `meta_description`, `meta_keywords`, `content`, `link_rewrite`) VALUES
(1, 1, 1, 'Delivery', 'Our terms and conditions of delivery', 'conditions, delivery, delay, shipment, pack', '<h2>Shipments and returns</h2><h3>Your pack shipment</h3><p>Packages are generally dispatched within 2 days after receipt of payment and are shipped via UPS with tracking and drop-off without signature. If you prefer delivery by UPS Extra with required signature, an additional cost will be applied, so please contact us before choosing this method. Whichever shipment choice you make, we will provide you with a link to track your package online.</p><p>Shipping fees include handling and packing fees as well as postage costs. Handling fees are fixed, whereas transport fees vary according to total weight of the shipment. We advise you to group your items in one order. We cannot group two distinct orders placed separately, and shipping fees will apply to each of them. Your package will be dispatched at your own risk, but special care is taken to protect fragile objects.<br /><br />Boxes are amply sized and your items are well-protected.</p>', 'delivery'),
(2, 1, 1, 'Legal Notice', 'Legal notice', 'notice, legal, credits', '<h2>Legal</h2><h3>Credits</h3><p>Concept and production:</p><p>This Web site was created using <a href=\"http://www.prestashop.com\">PrestaShop</a>&trade; open-source software.</p>', 'legal-notice'),
(3, 1, 1, 'Terms and conditions of use', 'Our terms and conditions of use', 'conditions, terms, use, sell', '<h2>Your terms and conditions of use</h2><h3>Rule 1</h3><p>Here is the rule 1 content</p>\r\n<h3>Rule 2</h3><p>Here is the rule 2 content</p>\r\n<h3>Rule 3</h3><p>Here is the rule 3 content</p>', 'terms-and-conditions-of-use'),
(4, 1, 1, 'About us', 'Learn more about us', 'about us, informations', '<h2>About us</h2>\r\n<h3>Our company</h3><p>Our company</p>\r\n<h3>Our team</h3><p>Our team</p>\r\n<h3>Informations</h3><p>Informations</p>', 'about-us'),
(5, 1, 1, 'Secure payment', 'Our secure payment mean', 'secure payment, ssl, visa, mastercard, paypal', '<h2>Secure payment</h2>\r\n<h3>Our secure payment</h3><p>With SSL</p>\r\n<h3>Using Visa/Mastercard/Paypal</h3><p>About this services</p>', 'secure-payment');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_cms_role`
--

DROP TABLE IF EXISTS `ps_cms_role`;
CREATE TABLE `ps_cms_role` (
  `id_cms_role` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `id_cms` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_cms_role_lang`
--

DROP TABLE IF EXISTS `ps_cms_role_lang`;
CREATE TABLE `ps_cms_role_lang` (
  `id_cms_role` int(11) UNSIGNED NOT NULL,
  `id_lang` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  `name` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_cms_shop`
--

DROP TABLE IF EXISTS `ps_cms_shop`;
CREATE TABLE `ps_cms_shop` (
  `id_cms` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_cms_shop`
--

INSERT INTO `ps_cms_shop` (`id_cms`, `id_shop`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_compare`
--

DROP TABLE IF EXISTS `ps_compare`;
CREATE TABLE `ps_compare` (
  `id_compare` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_compare_product`
--

DROP TABLE IF EXISTS `ps_compare_product`;
CREATE TABLE `ps_compare_product` (
  `id_compare` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_condition`
--

DROP TABLE IF EXISTS `ps_condition`;
CREATE TABLE `ps_condition` (
  `id_condition` int(11) NOT NULL,
  `id_ps_condition` int(11) NOT NULL,
  `type` enum('configuration','install','sql') NOT NULL,
  `request` text DEFAULT NULL,
  `operator` varchar(32) DEFAULT NULL,
  `value` varchar(64) DEFAULT NULL,
  `result` varchar(64) DEFAULT NULL,
  `calculation_type` enum('hook','time') DEFAULT NULL,
  `calculation_detail` varchar(64) DEFAULT NULL,
  `validated` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_condition_advice`
--

DROP TABLE IF EXISTS `ps_condition_advice`;
CREATE TABLE `ps_condition_advice` (
  `id_condition` int(11) NOT NULL,
  `id_advice` int(11) NOT NULL,
  `display` tinyint(1) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_condition_badge`
--

DROP TABLE IF EXISTS `ps_condition_badge`;
CREATE TABLE `ps_condition_badge` (
  `id_condition` int(11) NOT NULL,
  `id_badge` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_configuration`
--

DROP TABLE IF EXISTS `ps_configuration`;
CREATE TABLE `ps_configuration` (
  `id_configuration` int(10) UNSIGNED NOT NULL,
  `id_shop_group` int(11) UNSIGNED DEFAULT NULL,
  `id_shop` int(11) UNSIGNED DEFAULT NULL,
  `name` varchar(254) NOT NULL,
  `value` text DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_configuration`
--

INSERT INTO `ps_configuration` (`id_configuration`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES
(1, NULL, NULL, 'PS_LANG_DEFAULT', '1', '2018-12-03 17:17:58', '2018-12-03 17:17:58'),
(2, NULL, NULL, 'PS_VERSION_DB', '1.6.1.22', '2018-12-03 17:17:58', '2018-12-03 17:17:58'),
(3, NULL, NULL, 'PS_INSTALL_VERSION', '1.6.1.22', '2018-12-03 17:17:58', '2018-12-03 17:17:58'),
(4, NULL, NULL, 'PS_CARRIER_DEFAULT', '1', '2018-12-03 17:18:00', '2018-12-03 17:18:00'),
(5, NULL, NULL, 'PS_GROUP_FEATURE_ACTIVE', '1', '2018-12-03 17:18:00', '2018-12-03 17:18:00'),
(6, NULL, NULL, 'PS_SEARCH_INDEXATION', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, NULL, NULL, 'PS_ONE_PHONE_AT_LEAST', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, NULL, NULL, 'PS_CURRENCY_DEFAULT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, NULL, NULL, 'PS_COUNTRY_DEFAULT', '14', '0000-00-00 00:00:00', '2018-12-03 17:18:05'),
(10, NULL, NULL, 'PS_REWRITING_SETTINGS', '0', '0000-00-00 00:00:00', '2018-12-03 17:18:09'),
(11, NULL, NULL, 'PS_ORDER_OUT_OF_STOCK', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, NULL, NULL, 'PS_LAST_QTIES', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, NULL, NULL, 'PS_CART_REDIRECT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, NULL, NULL, 'PS_CONDITIONS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, NULL, NULL, 'PS_RECYCLABLE_PACK', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, NULL, NULL, 'PS_GIFT_WRAPPING', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, NULL, NULL, 'PS_GIFT_WRAPPING_PRICE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, NULL, NULL, 'PS_STOCK_MANAGEMENT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, NULL, NULL, 'PS_NAVIGATION_PIPE', '>', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, NULL, NULL, 'PS_PRODUCTS_PER_PAGE', '12', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, NULL, NULL, 'PS_PURCHASE_MINIMUM', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, NULL, NULL, 'PS_PRODUCTS_ORDER_WAY', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, NULL, NULL, 'PS_PRODUCTS_ORDER_BY', '7', '0000-00-00 00:00:00', '2018-12-04 00:28:57'),
(24, NULL, NULL, 'PS_DISPLAY_QTIES', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, NULL, NULL, 'PS_SHIPPING_HANDLING', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, NULL, NULL, 'PS_SHIPPING_FREE_PRICE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, NULL, NULL, 'PS_SHIPPING_FREE_WEIGHT', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, NULL, NULL, 'PS_SHIPPING_METHOD', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, NULL, NULL, 'PS_TAX', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, NULL, NULL, 'PS_SHOP_ENABLE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, NULL, NULL, 'PS_NB_DAYS_NEW_PRODUCT', '5', '0000-00-00 00:00:00', '2018-12-04 00:28:57'),
(32, NULL, NULL, 'PS_SSL_ENABLED', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, NULL, NULL, 'PS_WEIGHT_UNIT', 'kg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, NULL, NULL, 'PS_BLOCK_CART_AJAX', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, NULL, NULL, 'PS_ORDER_RETURN', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, NULL, NULL, 'PS_ORDER_RETURN_NB_DAYS', '14', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, NULL, NULL, 'PS_MAIL_TYPE', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, NULL, NULL, 'PS_PRODUCT_PICTURE_MAX_SIZE', '8388608', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, NULL, NULL, 'PS_PRODUCT_PICTURE_WIDTH', '64', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, NULL, NULL, 'PS_PRODUCT_PICTURE_HEIGHT', '64', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, NULL, NULL, 'PS_INVOICE_PREFIX', '#IN', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, NULL, NULL, 'PS_INVCE_INVOICE_ADDR_RULES', '{\"avoid\":[]}', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, NULL, NULL, 'PS_INVCE_DELIVERY_ADDR_RULES', '{\"avoid\":[]}', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, NULL, NULL, 'PS_DELIVERY_PREFIX', '#DE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, NULL, NULL, 'PS_DELIVERY_NUMBER', NULL, '0000-00-00 00:00:00', '2018-12-04 12:12:59'),
(46, NULL, NULL, 'PS_RETURN_PREFIX', '#RE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, NULL, NULL, 'PS_INVOICE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, NULL, NULL, 'PS_PASSWD_TIME_BACK', '360', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, NULL, NULL, 'PS_PASSWD_TIME_FRONT', '360', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, NULL, NULL, 'PS_DISP_UNAVAILABLE_ATTR', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, NULL, NULL, 'PS_SEARCH_MINWORDLEN', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, NULL, NULL, 'PS_SEARCH_BLACKLIST', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, NULL, NULL, 'PS_SEARCH_WEIGHT_PNAME', '10', '0000-00-00 00:00:00', '2018-12-04 00:30:18'),
(54, NULL, NULL, 'PS_SEARCH_WEIGHT_REF', '10', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, NULL, NULL, 'PS_SEARCH_WEIGHT_SHORTDESC', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, NULL, NULL, 'PS_SEARCH_WEIGHT_DESC', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, NULL, NULL, 'PS_SEARCH_WEIGHT_CNAME', '5', '0000-00-00 00:00:00', '2018-12-04 00:30:18'),
(58, NULL, NULL, 'PS_SEARCH_WEIGHT_MNAME', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, NULL, NULL, 'PS_SEARCH_WEIGHT_TAG', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, NULL, NULL, 'PS_SEARCH_WEIGHT_ATTRIBUTE', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, NULL, NULL, 'PS_SEARCH_WEIGHT_FEATURE', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, NULL, NULL, 'PS_SEARCH_AJAX', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, NULL, NULL, 'PS_TIMEZONE', 'Europe/Warsaw', '0000-00-00 00:00:00', '2018-12-03 17:18:05'),
(64, NULL, NULL, 'PS_THEME_V11', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, NULL, NULL, 'PRESTASTORE_LIVE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, NULL, NULL, 'PS_TIN_ACTIVE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, NULL, NULL, 'PS_SHOW_ALL_MODULES', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, NULL, NULL, 'PS_BACKUP_ALL', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, NULL, NULL, 'PS_1_3_UPDATE_DATE', '2011-12-27 10:20:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, NULL, NULL, 'PS_PRICE_ROUND_MODE', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, NULL, NULL, 'PS_1_3_2_UPDATE_DATE', '2011-12-27 10:20:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, NULL, NULL, 'PS_CONDITIONS_CMS_ID', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, NULL, NULL, 'TRACKING_DIRECT_TRAFFIC', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, NULL, NULL, 'PS_META_KEYWORDS', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, NULL, NULL, 'PS_DISPLAY_JQZOOM', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, NULL, NULL, 'PS_VOLUME_UNIT', 'L', '0000-00-00 00:00:00', '2018-12-03 23:27:11'),
(77, NULL, NULL, 'PS_CIPHER_ALGORITHM', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, NULL, NULL, 'PS_ATTRIBUTE_CATEGORY_DISPLAY', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, NULL, NULL, 'PS_CUSTOMER_SERVICE_FILE_UPLOAD', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, NULL, NULL, 'PS_CUSTOMER_SERVICE_SIGNATURE', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, NULL, NULL, 'PS_BLOCK_BESTSELLERS_DISPLAY', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, NULL, NULL, 'PS_BLOCK_NEWPRODUCTS_DISPLAY', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, NULL, NULL, 'PS_BLOCK_SPECIALS_DISPLAY', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, NULL, NULL, 'PS_STOCK_MVT_REASON_DEFAULT', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, NULL, NULL, 'PS_COMPARATOR_MAX_ITEM', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, NULL, NULL, 'PS_ORDER_PROCESS_TYPE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, NULL, NULL, 'PS_SPECIFIC_PRICE_PRIORITIES', 'id_shop;id_currency;id_country;id_group', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, NULL, NULL, 'PS_TAX_DISPLAY', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, NULL, NULL, 'PS_SMARTY_FORCE_COMPILE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, NULL, NULL, 'PS_DISTANCE_UNIT', 'km', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, NULL, NULL, 'PS_STORES_DISPLAY_CMS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, NULL, NULL, 'PS_STORES_DISPLAY_FOOTER', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, NULL, NULL, 'PS_STORES_SIMPLIFIED', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, NULL, NULL, 'SHOP_LOGO_WIDTH', '218', '0000-00-00 00:00:00', '2018-12-04 01:42:21'),
(95, NULL, NULL, 'SHOP_LOGO_HEIGHT', '80', '0000-00-00 00:00:00', '2018-12-04 01:42:21'),
(96, NULL, NULL, 'EDITORIAL_IMAGE_WIDTH', '530', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, NULL, NULL, 'EDITORIAL_IMAGE_HEIGHT', '228', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, NULL, NULL, 'PS_STATSDATA_CUSTOMER_PAGESVIEWS', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, NULL, NULL, 'PS_STATSDATA_PAGESVIEWS', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, NULL, NULL, 'PS_STATSDATA_PLUGINS', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, NULL, NULL, 'PS_GEOLOCATION_ENABLED', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, NULL, NULL, 'PS_ALLOWED_COUNTRIES', 'AF;ZA;AX;AL;DZ;DE;AD;AO;AI;AQ;AG;AN;SA;AR;AM;AW;AU;AT;AZ;BS;BH;BD;BB;BY;BE;BZ;BJ;BM;BT;BO;BA;BW;BV;BR;BN;BG;BF;MM;BI;KY;KH;CM;CA;CV;CF;CL;CN;CX;CY;CC;CO;KM;CG;CD;CK;KR;KP;CR;CI;HR;CU;DK;DJ;DM;EG;IE;SV;AE;EC;ER;ES;EE;ET;FK;FO;FJ;FI;FR;GA;GM;GE;GS;GH;GI;GR;GD;GL;GP;GU;GT;GG;GN;GQ;GW;GY;GF;HT;HM;HN;HK;HU;IM;MU;VG;VI;IN;ID;IR;IQ;IS;IL;IT;JM;JP;JE;JO;KZ;KE;KG;KI;KW;LA;LS;LV;LB;LR;LY;LI;LT;LU;MO;MK;MG;MY;MW;MV;ML;MT;MP;MA;MH;MQ;MR;YT;MX;FM;MD;MC;MN;ME;MS;MZ;NA;NR;NP;NI;NE;NG;NU;NF;NO;NC;NZ;IO;OM;UG;UZ;PK;PW;PS;PA;PG;PY;NL;PE;PH;PN;PL;PF;PR;PT;QA;DO;CZ;RE;RO;GB;RU;RW;EH;BL;KN;SM;MF;PM;VA;VC;LC;SB;WS;AS;ST;SN;RS;SC;SL;SG;SK;SI;SO;SD;LK;SE;CH;SR;SJ;SZ;SY;TJ;TW;TZ;TD;TF;TH;TL;TG;TK;TO;TT;TN;TM;TC;TR;TV;UA;UY;US;VU;VE;VN;WF;YE;ZM;ZW', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, NULL, NULL, 'PS_GEOLOCATION_BEHAVIOR', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, NULL, NULL, 'PS_LOCALE_LANGUAGE', 'pl', '0000-00-00 00:00:00', '2018-12-03 17:18:05'),
(105, NULL, NULL, 'PS_LOCALE_COUNTRY', 'pl', '0000-00-00 00:00:00', '2018-12-03 17:18:05'),
(106, NULL, NULL, 'PS_ATTACHMENT_MAXIMUM_SIZE', '8', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, NULL, NULL, 'PS_SMARTY_CACHE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, NULL, NULL, 'PS_DIMENSION_UNIT', 'cm', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, NULL, NULL, 'PS_GUEST_CHECKOUT_ENABLED', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(110, NULL, NULL, 'PS_DISPLAY_SUPPLIERS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(111, NULL, NULL, 'PS_DISPLAY_BEST_SELLERS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(112, NULL, NULL, 'PS_CATALOG_MODE', '0', '0000-00-00 00:00:00', '2018-12-04 00:33:41'),
(113, NULL, NULL, 'PS_GEOLOCATION_WHITELIST', '127;209.185.108;209.185.253;209.85.238;209.85.238.11;209.85.238.4;216.239.33.96;216.239.33.97;216.239.33.98;216.239.33.99;216.239.37.98;216.239.37.99;216.239.39.98;216.239.39.99;216.239.41.96;216.239.41.97;216.239.41.98;216.239.41.99;216.239.45.4;216.239.46;216.239.51.96;216.239.51.97;216.239.51.98;216.239.51.99;216.239.53.98;216.239.53.99;216.239.57.96;91.240.109;216.239.57.97;216.239.57.98;216.239.57.99;216.239.59.98;216.239.59.99;216.33.229.163;64.233.173.193;64.233.173.194;64.233.173.195;64.233.173.196;64.233.173.197;64.233.173.198;64.233.173.199;64.233.173.200;64.233.173.201;64.233.173.202;64.233.173.203;64.233.173.204;64.233.173.205;64.233.173.206;64.233.173.207;64.233.173.208;64.233.173.209;64.233.173.210;64.233.173.211;64.233.173.212;64.233.173.213;64.233.173.214;64.233.173.215;64.233.173.216;64.233.173.217;64.233.173.218;64.233.173.219;64.233.173.220;64.233.173.221;64.233.173.222;64.233.173.223;64.233.173.224;64.233.173.225;64.233.173.226;64.233.173.227;64.233.173.228;64.233.173.229;64.233.173.230;64.233.173.231;64.233.173.232;64.233.173.233;64.233.173.234;64.233.173.235;64.233.173.236;64.233.173.237;64.233.173.238;64.233.173.239;64.233.173.240;64.233.173.241;64.233.173.242;64.233.173.243;64.233.173.244;64.233.173.245;64.233.173.246;64.233.173.247;64.233.173.248;64.233.173.249;64.233.173.250;64.233.173.251;64.233.173.252;64.233.173.253;64.233.173.254;64.233.173.255;64.68.80;64.68.81;64.68.82;64.68.83;64.68.84;64.68.85;64.68.86;64.68.87;64.68.88;64.68.89;64.68.90.1;64.68.90.10;64.68.90.11;64.68.90.12;64.68.90.129;64.68.90.13;64.68.90.130;64.68.90.131;64.68.90.132;64.68.90.133;64.68.90.134;64.68.90.135;64.68.90.136;64.68.90.137;64.68.90.138;64.68.90.139;64.68.90.14;64.68.90.140;64.68.90.141;64.68.90.142;64.68.90.143;64.68.90.144;64.68.90.145;64.68.90.146;64.68.90.147;64.68.90.148;64.68.90.149;64.68.90.15;64.68.90.150;64.68.90.151;64.68.90.152;64.68.90.153;64.68.90.154;64.68.90.155;64.68.90.156;64.68.90.157;64.68.90.158;64.68.90.159;64.68.90.16;64.68.90.160;64.68.90.161;64.68.90.162;64.68.90.163;64.68.90.164;64.68.90.165;64.68.90.166;64.68.90.167;64.68.90.168;64.68.90.169;64.68.90.17;64.68.90.170;64.68.90.171;64.68.90.172;64.68.90.173;64.68.90.174;64.68.90.175;64.68.90.176;64.68.90.177;64.68.90.178;64.68.90.179;64.68.90.18;64.68.90.180;64.68.90.181;64.68.90.182;64.68.90.183;64.68.90.184;64.68.90.185;64.68.90.186;64.68.90.187;64.68.90.188;64.68.90.189;64.68.90.19;64.68.90.190;64.68.90.191;64.68.90.192;64.68.90.193;64.68.90.194;64.68.90.195;64.68.90.196;64.68.90.197;64.68.90.198;64.68.90.199;64.68.90.2;64.68.90.20;64.68.90.200;64.68.90.201;64.68.90.202;64.68.90.203;64.68.90.204;64.68.90.205;64.68.90.206;64.68.90.207;64.68.90.208;64.68.90.21;64.68.90.22;64.68.90.23;64.68.90.24;64.68.90.25;64.68.90.26;64.68.90.27;64.68.90.28;64.68.90.29;64.68.90.3;64.68.90.30;64.68.90.31;64.68.90.32;64.68.90.33;64.68.90.34;64.68.90.35;64.68.90.36;64.68.90.37;64.68.90.38;64.68.90.39;64.68.90.4;64.68.90.40;64.68.90.41;64.68.90.42;64.68.90.43;64.68.90.44;64.68.90.45;64.68.90.46;64.68.90.47;64.68.90.48;64.68.90.49;64.68.90.5;64.68.90.50;64.68.90.51;64.68.90.52;64.68.90.53;64.68.90.54;64.68.90.55;64.68.90.56;64.68.90.57;64.68.90.58;64.68.90.59;64.68.90.6;64.68.90.60;64.68.90.61;64.68.90.62;64.68.90.63;64.68.90.64;64.68.90.65;64.68.90.66;64.68.90.67;64.68.90.68;64.68.90.69;64.68.90.7;64.68.90.70;64.68.90.71;64.68.90.72;64.68.90.73;64.68.90.74;64.68.90.75;64.68.90.76;64.68.90.77;64.68.90.78;64.68.90.79;64.68.90.8;64.68.90.80;64.68.90.9;64.68.91;64.68.92;66.249.64;66.249.65;66.249.66;66.249.67;66.249.68;66.249.69;66.249.70;66.249.71;66.249.72;66.249.73;66.249.78;66.249.79;72.14.199;8.6.48', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(114, NULL, NULL, 'PS_LOGS_BY_EMAIL', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(115, NULL, NULL, 'PS_COOKIE_CHECKIP', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(116, NULL, NULL, 'PS_STORES_CENTER_LAT', '25.948969', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(117, NULL, NULL, 'PS_STORES_CENTER_LONG', '-80.226439', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(118, NULL, NULL, 'PS_USE_ECOTAX', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(119, NULL, NULL, 'PS_CANONICAL_REDIRECT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(120, NULL, NULL, 'PS_IMG_UPDATE_TIME', '1543884142', '0000-00-00 00:00:00', '2018-12-04 01:42:22'),
(121, NULL, NULL, 'PS_BACKUP_DROP_TABLE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(122, NULL, NULL, 'PS_OS_CHEQUE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(123, NULL, NULL, 'PS_OS_PAYMENT', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(124, NULL, NULL, 'PS_OS_PREPARATION', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(125, NULL, NULL, 'PS_OS_SHIPPING', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(126, NULL, NULL, 'PS_OS_DELIVERED', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(127, NULL, NULL, 'PS_OS_CANCELED', '6', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(128, NULL, NULL, 'PS_OS_REFUND', '7', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(129, NULL, NULL, 'PS_OS_ERROR', '8', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(130, NULL, NULL, 'PS_OS_OUTOFSTOCK', '9', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(131, NULL, NULL, 'PS_OS_BANKWIRE', '10', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(132, NULL, NULL, 'PS_OS_PAYPAL', '11', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(133, NULL, NULL, 'PS_OS_WS_PAYMENT', '12', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(134, NULL, NULL, 'PS_OS_OUTOFSTOCK_PAID', '9', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(135, NULL, NULL, 'PS_OS_OUTOFSTOCK_UNPAID', '13', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(136, NULL, NULL, 'PS_OS_COD_VALIDATION', '14', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(137, NULL, NULL, 'PS_LEGACY_IMAGES', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(138, NULL, NULL, 'PS_IMAGE_QUALITY', 'jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(139, NULL, NULL, 'PS_PNG_QUALITY', '7', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(140, NULL, NULL, 'PS_JPEG_QUALITY', '90', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(141, NULL, NULL, 'PS_COOKIE_LIFETIME_FO', '480', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(142, NULL, NULL, 'PS_COOKIE_LIFETIME_BO', '480', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(143, NULL, NULL, 'PS_RESTRICT_DELIVERED_COUNTRIES', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(144, NULL, NULL, 'PS_SHOW_NEW_ORDERS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(145, NULL, NULL, 'PS_SHOW_NEW_CUSTOMERS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(146, NULL, NULL, 'PS_SHOW_NEW_MESSAGES', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(147, NULL, NULL, 'PS_FEATURE_FEATURE_ACTIVE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(148, NULL, NULL, 'PS_COMBINATION_FEATURE_ACTIVE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(149, NULL, NULL, 'PS_SPECIFIC_PRICE_FEATURE_ACTIVE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(150, NULL, NULL, 'PS_SCENE_FEATURE_ACTIVE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(151, NULL, NULL, 'PS_VIRTUAL_PROD_FEATURE_ACTIVE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(152, NULL, NULL, 'PS_CUSTOMIZATION_FEATURE_ACTIVE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(153, NULL, NULL, 'PS_CART_RULE_FEATURE_ACTIVE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(154, NULL, NULL, 'PS_PACK_FEATURE_ACTIVE', NULL, '0000-00-00 00:00:00', '2018-12-03 23:12:41'),
(155, NULL, NULL, 'PS_ALIAS_FEATURE_ACTIVE', NULL, '0000-00-00 00:00:00', '2018-12-04 00:29:20'),
(156, NULL, NULL, 'PS_TAX_ADDRESS_TYPE', 'id_address_delivery', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(157, NULL, NULL, 'PS_SHOP_DEFAULT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(158, NULL, NULL, 'PS_CARRIER_DEFAULT_SORT', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(159, NULL, NULL, 'PS_STOCK_MVT_INC_REASON_DEFAULT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(160, NULL, NULL, 'PS_STOCK_MVT_DEC_REASON_DEFAULT', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(161, NULL, NULL, 'PS_ADVANCED_STOCK_MANAGEMENT', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(162, NULL, NULL, 'PS_ADMINREFRESH_NOTIFICATION', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(163, NULL, NULL, 'PS_STOCK_MVT_TRANSFER_TO', '7', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(164, NULL, NULL, 'PS_STOCK_MVT_TRANSFER_FROM', '6', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(165, NULL, NULL, 'PS_CARRIER_DEFAULT_ORDER', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(166, NULL, NULL, 'PS_STOCK_MVT_SUPPLY_ORDER', '8', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(167, NULL, NULL, 'PS_STOCK_CUSTOMER_ORDER_REASON', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(168, NULL, NULL, 'PS_UNIDENTIFIED_GROUP', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(169, NULL, NULL, 'PS_GUEST_GROUP', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(170, NULL, NULL, 'PS_CUSTOMER_GROUP', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(171, NULL, NULL, 'PS_SMARTY_CONSOLE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(172, NULL, NULL, 'PS_INVOICE_MODEL', 'invoice', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(173, NULL, NULL, 'PS_LIMIT_UPLOAD_IMAGE_VALUE', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(174, NULL, NULL, 'PS_LIMIT_UPLOAD_FILE_VALUE', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(175, NULL, NULL, 'MB_PAY_TO_EMAIL', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(176, NULL, NULL, 'MB_SECRET_WORD', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(177, NULL, NULL, 'MB_HIDE_LOGIN', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(178, NULL, NULL, 'MB_ID_LOGO', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(179, NULL, NULL, 'MB_ID_LOGO_WALLET', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(180, NULL, NULL, 'MB_PARAMETERS', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(181, NULL, NULL, 'MB_PARAMETERS_2', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(182, NULL, NULL, 'MB_DISPLAY_MODE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(183, NULL, NULL, 'MB_CANCEL_URL', 'http://www.yoursite.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(184, NULL, NULL, 'MB_LOCAL_METHODS', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(185, NULL, NULL, 'MB_INTER_METHODS', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(186, NULL, NULL, 'BANK_WIRE_CURRENCIES', '2,1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(187, NULL, NULL, 'CHEQUE_CURRENCIES', '2,1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(188, NULL, NULL, 'PRODUCTS_VIEWED_NBR', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(189, NULL, NULL, 'BLOCK_CATEG_DHTML', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(190, NULL, NULL, 'BLOCK_CATEG_MAX_DEPTH', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(191, NULL, NULL, 'MANUFACTURER_DISPLAY_FORM', '1', '0000-00-00 00:00:00', '2018-12-03 17:18:28'),
(192, NULL, NULL, 'MANUFACTURER_DISPLAY_TEXT', '1', '0000-00-00 00:00:00', '2018-12-03 17:18:28'),
(193, NULL, NULL, 'MANUFACTURER_DISPLAY_TEXT_NB', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(194, NULL, NULL, 'NEW_PRODUCTS_NBR', '8', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(195, NULL, NULL, 'PS_TOKEN_ENABLE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(196, NULL, NULL, 'PS_STATS_RENDER', 'graphnvd3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(197, NULL, NULL, 'PS_STATS_OLD_CONNECT_AUTO_CLEAN', 'never', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(198, NULL, NULL, 'PS_STATS_GRID_RENDER', 'gridhtml', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(199, NULL, NULL, 'BLOCKTAGS_NBR', '10', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(200, NULL, NULL, 'CHECKUP_DESCRIPTIONS_LT', '100', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(201, NULL, NULL, 'CHECKUP_DESCRIPTIONS_GT', '400', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(202, NULL, NULL, 'CHECKUP_IMAGES_LT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(203, NULL, NULL, 'CHECKUP_IMAGES_GT', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(204, NULL, NULL, 'CHECKUP_SALES_LT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(205, NULL, NULL, 'CHECKUP_SALES_GT', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(206, NULL, NULL, 'CHECKUP_STOCK_LT', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(207, NULL, NULL, 'CHECKUP_STOCK_GT', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(208, NULL, NULL, 'FOOTER_CMS', '0_3|0_4', '0000-00-00 00:00:00', '2018-12-03 17:18:28'),
(209, NULL, NULL, 'FOOTER_BLOCK_ACTIVATION', '0_3|0_4', '0000-00-00 00:00:00', '2018-12-03 17:18:28'),
(210, NULL, NULL, 'FOOTER_POWEREDBY', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(211, NULL, NULL, 'BLOCKADVERT_LINK', 'http://www.prestashop.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(212, NULL, NULL, 'BLOCKSTORE_IMG', 'store.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(213, NULL, NULL, 'BLOCKADVERT_IMG_EXT', 'jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(214, NULL, NULL, 'MOD_BLOCKTOPMENU_ITEMS', 'CAT3,CAT8,CAT5,LNK1', '0000-00-00 00:00:00', '2018-12-03 17:18:29'),
(215, NULL, NULL, 'MOD_BLOCKTOPMENU_SEARCH', '0', '0000-00-00 00:00:00', '2018-12-03 17:18:29'),
(216, NULL, NULL, 'BLOCKSOCIAL_FACEBOOK', 'http://www.facebook.com/prestashop', '0000-00-00 00:00:00', '2018-12-03 17:18:26'),
(217, NULL, NULL, 'BLOCKSOCIAL_TWITTER', 'http://www.twitter.com/prestashop', '0000-00-00 00:00:00', '2018-12-03 17:18:26'),
(218, NULL, NULL, 'BLOCKSOCIAL_RSS', 'http://www.prestashop.com/blog/en/', '0000-00-00 00:00:00', '2018-12-03 17:18:26'),
(219, NULL, NULL, 'BLOCKCONTACTINFOS_COMPANY', 'Otomoto', '0000-00-00 00:00:00', '2018-12-04 00:48:45'),
(220, NULL, NULL, 'BLOCKCONTACTINFOS_ADDRESS', 'ul. Spacerowa 254\r\n80-247\r\nGdańsk', '0000-00-00 00:00:00', '2018-12-04 00:48:45'),
(221, NULL, NULL, 'BLOCKCONTACTINFOS_PHONE', '512 - 878 - 875', '0000-00-00 00:00:00', '2018-12-04 00:48:45'),
(222, NULL, NULL, 'BLOCKCONTACTINFOS_EMAIL', 'paweljerzygis@gmail.com', '0000-00-00 00:00:00', '2018-12-04 00:48:45'),
(223, NULL, NULL, 'BLOCKCONTACT_TELNUMBER', '0123-456-789', '0000-00-00 00:00:00', '2018-12-03 17:18:28'),
(224, NULL, NULL, 'BLOCKCONTACT_EMAIL', 'sales@yourcompany.com', '0000-00-00 00:00:00', '2018-12-03 17:18:28'),
(225, NULL, NULL, 'SUPPLIER_DISPLAY_TEXT', '1', '0000-00-00 00:00:00', '2018-12-03 17:18:29'),
(226, NULL, NULL, 'SUPPLIER_DISPLAY_TEXT_NB', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(227, NULL, NULL, 'SUPPLIER_DISPLAY_FORM', '1', '0000-00-00 00:00:00', '2018-12-03 17:18:29'),
(228, NULL, NULL, 'BLOCK_CATEG_NBR_COLUMN_FOOTER', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(229, NULL, NULL, 'UPGRADER_BACKUPDB_FILENAME', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(230, NULL, NULL, 'UPGRADER_BACKUPFILES_FILENAME', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(231, NULL, NULL, 'BLOCKREINSURANCE_NBBLOCKS', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(232, NULL, NULL, 'HOMESLIDER_WIDTH', '1808080', '0000-00-00 00:00:00', '2018-12-04 01:18:47'),
(233, NULL, NULL, 'HOMESLIDER_SPEED', '500', '0000-00-00 00:00:00', '2018-12-03 17:18:30'),
(234, NULL, NULL, 'HOMESLIDER_PAUSE', '5000', '0000-00-00 00:00:00', '2018-12-04 01:13:38'),
(235, NULL, NULL, 'HOMESLIDER_LOOP', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(236, NULL, NULL, 'PS_BASE_DISTANCE_UNIT', 'm', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(237, NULL, NULL, 'PS_SHOP_DOMAIN', '172.20.83.60', '0000-00-00 00:00:00', '2018-12-03 17:18:04'),
(238, NULL, NULL, 'PS_SHOP_DOMAIN_SSL', '172.20.83.60', '0000-00-00 00:00:00', '2018-12-03 17:18:05'),
(239, NULL, NULL, 'PS_SHOP_NAME', 'otomoto', '0000-00-00 00:00:00', '2018-12-03 17:18:05'),
(240, NULL, NULL, 'PS_SHOP_EMAIL', 'felo.dawid@gmail.com', '0000-00-00 00:00:00', '2018-12-03 17:18:09'),
(241, NULL, NULL, 'PS_MAIL_METHOD', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(242, NULL, NULL, 'PS_SHOP_ACTIVITY', '6', '0000-00-00 00:00:00', '2018-12-03 17:18:05'),
(243, NULL, NULL, 'PS_LOGO', 'otomoto-logo-1543884141.jpg', '0000-00-00 00:00:00', '2018-12-04 01:42:21'),
(244, NULL, NULL, 'PS_FAVICON', 'favicon.ico', '0000-00-00 00:00:00', '2018-12-04 01:42:21'),
(245, NULL, NULL, 'PS_STORES_ICON', 'logo_stores.png', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(246, NULL, NULL, 'PS_ROOT_CATEGORY', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(247, NULL, NULL, 'PS_HOME_CATEGORY', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(248, NULL, NULL, 'PS_CONFIGURATION_AGREMENT', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(249, NULL, NULL, 'PS_MAIL_SERVER', 'smtp.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(250, NULL, NULL, 'PS_MAIL_USER', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(251, NULL, NULL, 'PS_MAIL_PASSWD', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(252, NULL, NULL, 'PS_MAIL_SMTP_ENCRYPTION', 'off', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(253, NULL, NULL, 'PS_MAIL_SMTP_PORT', '25', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(254, NULL, NULL, 'PS_MAIL_COLOR', '#db3484', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(255, NULL, NULL, 'NW_SALT', 'EwXmGD1X6kEERZ4J', '0000-00-00 00:00:00', '2018-12-03 17:18:29'),
(256, NULL, NULL, 'PS_PAYMENT_LOGO_CMS_ID', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(257, NULL, NULL, 'HOME_FEATURED_NBR', '8', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(258, NULL, NULL, 'SEK_MIN_OCCURENCES', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(259, NULL, NULL, 'SEK_FILTER_KW', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(260, NULL, NULL, 'PS_ALLOW_MOBILE_DEVICE', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(261, NULL, NULL, 'PS_CUSTOMER_CREATION_EMAIL', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(262, NULL, NULL, 'PS_SMARTY_CONSOLE_KEY', 'SMARTY_DEBUG', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(263, NULL, NULL, 'PS_DASHBOARD_USE_PUSH', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(264, NULL, NULL, 'PS_ATTRIBUTE_ANCHOR_SEPARATOR', '-', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(265, NULL, NULL, 'CONF_AVERAGE_PRODUCT_MARGIN', '40', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(266, NULL, NULL, 'PS_DASHBOARD_SIMULATION', '0', '0000-00-00 00:00:00', '2018-12-03 22:35:23'),
(267, NULL, NULL, 'PS_QUICK_VIEW', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(268, NULL, NULL, 'PS_USE_HTMLPURIFIER', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(269, NULL, NULL, 'PS_SMARTY_CACHING_TYPE', 'filesystem', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(270, NULL, NULL, 'PS_SMARTY_CLEAR_CACHE', 'everytime', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(271, NULL, NULL, 'PS_DETECT_LANG', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(272, NULL, NULL, 'PS_DETECT_COUNTRY', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(273, NULL, NULL, 'PS_ROUND_TYPE', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(274, NULL, NULL, 'PS_PRICE_DISPLAY_PRECISION', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(275, NULL, NULL, 'PS_LOG_EMAILS', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(276, NULL, NULL, 'PS_CUSTOMER_NWSL', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(277, NULL, NULL, 'PS_CUSTOMER_OPTIN', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(278, NULL, NULL, 'PS_PACK_STOCK_TYPE', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(279, NULL, NULL, 'PS_LOG_MODULE_PERFS_MODULO', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(280, NULL, NULL, 'PS_DISALLOW_HISTORY_REORDERING', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(281, NULL, NULL, 'PS_DISPLAY_PRODUCT_WEIGHT', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(282, NULL, NULL, 'PS_PRODUCT_WEIGHT_PRECISION', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(283, NULL, NULL, 'PS_ADVANCED_PAYMENT_API', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(284, NULL, NULL, 'PS_SC_TWITTER', '1', '2018-12-03 17:18:19', '2018-12-03 17:18:19'),
(285, NULL, NULL, 'PS_SC_FACEBOOK', '1', '2018-12-03 17:18:19', '2018-12-03 17:18:19'),
(286, NULL, NULL, 'PS_SC_GOOGLE', '1', '2018-12-03 17:18:19', '2018-12-03 17:18:19'),
(287, NULL, NULL, 'PS_SC_PINTEREST', '1', '2018-12-03 17:18:19', '2018-12-03 17:18:19'),
(288, NULL, NULL, 'BLOCKBANNER_IMG', NULL, '2018-12-03 17:18:22', '2018-12-03 17:18:22'),
(289, NULL, NULL, 'BLOCKBANNER_LINK', NULL, '2018-12-03 17:18:22', '2018-12-03 17:18:22'),
(290, NULL, NULL, 'BLOCKBANNER_DESC', NULL, '2018-12-03 17:18:22', '2018-12-03 17:18:22'),
(291, NULL, NULL, 'CONF_BANKWIRE_FIXED', '0.2', '2018-12-03 17:18:23', '2018-12-03 17:18:23'),
(292, NULL, NULL, 'CONF_BANKWIRE_VAR', '2', '2018-12-03 17:18:23', '2018-12-03 17:18:23'),
(293, NULL, NULL, 'CONF_BANKWIRE_FIXED_FOREIGN', '0.2', '2018-12-03 17:18:23', '2018-12-03 17:18:23'),
(294, NULL, NULL, 'CONF_BANKWIRE_VAR_FOREIGN', '2', '2018-12-03 17:18:23', '2018-12-03 17:18:23'),
(295, NULL, NULL, 'PS_BLOCK_BESTSELLERS_TO_DISPLAY', '10', '2018-12-03 17:18:25', '2018-12-03 17:18:25'),
(296, NULL, NULL, 'PS_BLOCK_CART_XSELL_LIMIT', '12', '2018-12-03 17:18:25', '2018-12-03 17:18:25'),
(297, NULL, NULL, 'PS_BLOCK_CART_SHOW_CROSSSELLING', '1', '2018-12-03 17:18:25', '2018-12-03 17:18:25'),
(298, NULL, NULL, 'BLOCKSOCIAL_YOUTUBE', NULL, '2018-12-03 17:18:26', '2018-12-03 17:18:26'),
(299, NULL, NULL, 'BLOCKSOCIAL_GOOGLE_PLUS', 'https://www.google.com/+prestashop', '2018-12-03 17:18:26', '2018-12-03 17:18:26'),
(300, NULL, NULL, 'BLOCKSOCIAL_PINTEREST', NULL, '2018-12-03 17:18:26', '2018-12-03 17:18:26'),
(301, NULL, NULL, 'BLOCKSOCIAL_VIMEO', NULL, '2018-12-03 17:18:26', '2018-12-03 17:18:26'),
(302, NULL, NULL, 'BLOCKSOCIAL_INSTAGRAM', NULL, '2018-12-03 17:18:26', '2018-12-03 17:18:26'),
(303, NULL, NULL, 'BLOCK_CATEG_ROOT_CATEGORY', '1', '2018-12-03 17:18:26', '2018-12-03 17:18:26'),
(304, NULL, NULL, 'blockfacebook_url', 'https://www.facebook.com/prestashop', '2018-12-03 17:18:26', '2018-12-03 17:18:26'),
(305, NULL, NULL, 'PS_LAYERED_HIDE_0_VALUES', '1', '2018-12-03 17:18:26', '2018-12-03 17:18:26'),
(306, NULL, NULL, 'PS_LAYERED_SHOW_QTIES', '1', '2018-12-03 17:18:26', '2018-12-03 17:18:26'),
(307, NULL, NULL, 'PS_LAYERED_FULL_TREE', '1', '2018-12-03 17:18:26', '2018-12-03 17:18:26'),
(308, NULL, NULL, 'PS_LAYERED_FILTER_PRICE_USETAX', '1', '2018-12-03 17:18:26', '2018-12-03 17:18:26'),
(309, NULL, NULL, 'PS_LAYERED_FILTER_CATEGORY_DEPTH', '1', '2018-12-03 17:18:26', '2018-12-03 17:18:26'),
(310, NULL, NULL, 'PS_LAYERED_FILTER_INDEX_QTY', '0', '2018-12-03 17:18:26', '2018-12-03 17:18:26'),
(311, NULL, NULL, 'PS_LAYERED_FILTER_INDEX_CDT', '0', '2018-12-03 17:18:26', '2018-12-03 17:18:26'),
(312, NULL, NULL, 'PS_LAYERED_FILTER_INDEX_MNF', '0', '2018-12-03 17:18:26', '2018-12-03 17:18:26'),
(313, NULL, NULL, 'PS_LAYERED_FILTER_INDEX_CAT', '0', '2018-12-03 17:18:26', '2018-12-03 17:18:26'),
(314, NULL, NULL, 'PS_LAYERED_FILTER_PRICE_ROUNDING', '1', '2018-12-03 17:18:26', '2018-12-03 17:18:26'),
(315, NULL, NULL, 'PS_LAYERED_INDEXED', '1', '2018-12-03 17:18:27', '2018-12-03 17:18:27'),
(316, NULL, NULL, 'FOOTER_PRICE-DROP', '1', '2018-12-03 17:18:28', '2018-12-03 17:18:28'),
(317, NULL, NULL, 'FOOTER_NEW-PRODUCTS', '1', '2018-12-03 17:18:28', '2018-12-03 17:18:28'),
(318, NULL, NULL, 'FOOTER_BEST-SALES', '1', '2018-12-03 17:18:28', '2018-12-03 17:18:28'),
(319, NULL, NULL, 'FOOTER_CONTACT', '1', '2018-12-03 17:18:28', '2018-12-03 17:18:28'),
(320, NULL, NULL, 'FOOTER_SITEMAP', '1', '2018-12-03 17:18:28', '2018-12-03 17:18:28'),
(321, NULL, NULL, 'PS_NEWSLETTER_RAND', '1399679023946531735', '2018-12-03 17:18:29', '2018-12-03 17:18:29'),
(322, NULL, NULL, 'BLOCKSPECIALS_NB_CACHES', '20', '2018-12-03 17:18:29', '2018-12-03 17:18:29'),
(323, NULL, NULL, 'BLOCKSPECIALS_SPECIALS_NBR', '5', '2018-12-03 17:18:29', '2018-12-03 17:18:29'),
(324, NULL, NULL, 'BLOCKTAGS_MAX_LEVEL', '3', '2018-12-03 17:18:29', '2018-12-03 17:18:29'),
(325, NULL, NULL, 'CONF_CHEQUE_FIXED', '0.2', '2018-12-03 17:18:29', '2018-12-03 17:18:29'),
(326, NULL, NULL, 'CONF_CHEQUE_VAR', '2', '2018-12-03 17:18:29', '2018-12-03 17:18:29'),
(327, NULL, NULL, 'CONF_CHEQUE_FIXED_FOREIGN', '0.2', '2018-12-03 17:18:29', '2018-12-03 17:18:29'),
(328, NULL, NULL, 'CONF_CHEQUE_VAR_FOREIGN', '2', '2018-12-03 17:18:29', '2018-12-03 17:18:29'),
(329, NULL, NULL, 'DASHACTIVITY_CART_ACTIVE', '30', '2018-12-03 17:18:29', '2018-12-03 17:18:29'),
(330, NULL, NULL, 'DASHACTIVITY_CART_ABANDONED_MIN', '24', '2018-12-03 17:18:29', '2018-12-03 17:18:29'),
(331, NULL, NULL, 'DASHACTIVITY_CART_ABANDONED_MAX', '48', '2018-12-03 17:18:29', '2018-12-03 17:18:29'),
(332, NULL, NULL, 'DASHACTIVITY_VISITOR_ONLINE', '30', '2018-12-03 17:18:29', '2018-12-03 17:18:29'),
(333, NULL, NULL, 'PS_DASHGOALS_CURRENT_YEAR', '2018', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(334, NULL, NULL, 'DASHPRODUCT_NBR_SHOW_LAST_ORDER', '10', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(335, NULL, NULL, 'DASHPRODUCT_NBR_SHOW_BEST_SELLER', '10', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(336, NULL, NULL, 'DASHPRODUCT_NBR_SHOW_MOST_VIEWED', '10', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(337, NULL, NULL, 'DASHPRODUCT_NBR_SHOW_TOP_SEARCH', '10', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(338, NULL, NULL, 'HOME_FEATURED_CAT', '2', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(339, NULL, NULL, 'PRODUCTPAYMENTLOGOS_IMG', 'payment-logo.png', '2018-12-03 17:18:31', '2018-12-03 17:18:31'),
(340, NULL, NULL, 'PRODUCTPAYMENTLOGOS_LINK', NULL, '2018-12-03 17:18:31', '2018-12-03 17:18:31'),
(341, NULL, NULL, 'PRODUCTPAYMENTLOGOS_TITLE', NULL, '2018-12-03 17:18:31', '2018-12-03 17:18:31'),
(342, NULL, NULL, 'PS_TC_THEMES', 'a:9:{i:0;s:6:\"theme1\";i:1;s:6:\"theme2\";i:2;s:6:\"theme3\";i:3;s:6:\"theme4\";i:4;s:6:\"theme5\";i:5;s:6:\"theme6\";i:6;s:6:\"theme7\";i:7;s:6:\"theme8\";i:8;s:6:\"theme9\";}', '2018-12-03 17:18:32', '2018-12-03 17:18:32'),
(343, NULL, NULL, 'PS_TC_FONTS', 'a:10:{s:5:\"font1\";s:9:\"Open Sans\";s:5:\"font2\";s:12:\"Josefin Slab\";s:5:\"font3\";s:4:\"Arvo\";s:5:\"font4\";s:4:\"Lato\";s:5:\"font5\";s:7:\"Volkorn\";s:5:\"font6\";s:13:\"Abril Fatface\";s:5:\"font7\";s:6:\"Ubuntu\";s:5:\"font8\";s:7:\"PT Sans\";s:5:\"font9\";s:15:\"Old Standard TT\";s:6:\"font10\";s:10:\"Droid Sans\";}', '2018-12-03 17:18:32', '2018-12-03 17:18:32'),
(344, NULL, NULL, 'PS_TC_THEME', NULL, '2018-12-03 17:18:32', '2018-12-03 17:18:32'),
(345, NULL, NULL, 'PS_TC_FONT', NULL, '2018-12-03 17:18:32', '2018-12-03 17:18:32'),
(346, NULL, NULL, 'PS_TC_ACTIVE', '1', '2018-12-03 17:18:32', '2018-12-03 17:18:32'),
(347, NULL, NULL, 'PS_SET_DISPLAY_SUBCATEGORIES', '1', '2018-12-03 17:18:32', '2018-12-03 17:18:32'),
(348, NULL, NULL, 'GF_INSTALL_CALC', '1', '2018-12-03 17:18:41', '2018-12-03 22:13:37'),
(349, NULL, NULL, 'GF_CURRENT_LEVEL', '1', '2018-12-03 17:18:41', '2018-12-03 17:18:41'),
(350, NULL, NULL, 'GF_CURRENT_LEVEL_PERCENT', '0', '2018-12-03 17:18:41', '2018-12-03 17:18:41'),
(351, NULL, NULL, 'GF_NOTIFICATION', '0', '2018-12-03 17:18:41', '2018-12-03 17:18:41'),
(352, NULL, NULL, 'CRONJOBS_ADMIN_DIR', '00bb9b933c822829b7366637a1b8cb27', '2018-12-03 17:18:41', '2018-12-03 22:12:25'),
(353, NULL, NULL, 'CRONJOBS_MODE', 'webservice', '2018-12-03 17:18:41', '2018-12-03 17:18:41'),
(354, NULL, NULL, 'CRONJOBS_MODULE_VERSION', '1.4.0', '2018-12-03 17:18:41', '2018-12-03 17:18:41'),
(355, NULL, NULL, 'CRONJOBS_WEBSERVICE_ID', '0', '2018-12-03 17:18:41', '2018-12-03 17:18:41'),
(356, NULL, NULL, 'CRONJOBS_EXECUTION_TOKEN', 'd1d2b77212bfe9cad63bd70d15968668', '2018-12-03 17:18:41', '2018-12-03 17:18:41'),
(357, NULL, NULL, 'GF_NOT_VIEWED_BADGE', NULL, '2018-12-03 22:13:37', '2018-12-04 12:13:21'),
(358, NULL, NULL, 'PS_CCCJS_VERSION', '4', '2018-12-04 00:28:57', '2018-12-04 00:33:41'),
(359, NULL, NULL, 'PS_CCCCSS_VERSION', '4', '2018-12-04 00:28:57', '2018-12-04 00:33:41'),
(360, NULL, NULL, 'PS_PRODUCT_SHORT_DESC_LIMIT', '0', '2018-12-04 00:28:57', '2018-12-04 00:28:57'),
(361, NULL, NULL, 'PS_QTY_DISCOUNT_ON_COMBINATION', '0', '2018-12-04 00:28:57', '2018-12-04 00:28:57'),
(362, NULL, NULL, 'PS_FORCE_FRIENDLY_PRODUCT', '0', '2018-12-04 00:28:57', '2018-12-04 00:28:57'),
(363, NULL, NULL, 'PS_DISPLAY_DISCOUNT_PRICE', '0', '2018-12-04 00:28:57', '2018-12-04 00:31:40'),
(364, NULL, NULL, 'PS_FORCE_ASM_NEW_PRODUCT', '0', '2018-12-04 00:28:57', '2018-12-04 00:28:57'),
(365, NULL, NULL, 'PS_INSTANT_SEARCH', '1', '2018-12-04 00:29:34', '2018-12-04 00:30:03'),
(366, NULL, NULL, 'PS_SEARCH_START', '1', '2018-12-04 00:29:34', '2018-12-04 00:30:03'),
(367, NULL, NULL, 'PS_SEARCH_END', '1', '2018-12-04 00:29:34', '2018-12-04 00:30:03'),
(368, NULL, NULL, 'PS_SHOW_CAT_MODULES_2', 'dashboard', '2018-12-04 00:39:06', '2018-12-04 01:38:41'),
(369, NULL, NULL, 'PS_GRID_PRODUCT', '0', '2018-12-04 00:45:50', '2018-12-04 00:45:50'),
(370, NULL, NULL, 'GA_ACCOUNT_ID', 'UA-129750921-2', '2018-12-04 01:06:59', '2018-12-04 01:06:59'),
(371, NULL, NULL, 'GANALYTICS_CONFIGURATION_OK', '1', '2018-12-04 01:06:59', '2018-12-04 01:24:58'),
(372, NULL, NULL, 'GA_USERID_ENABLED', '1', '2018-12-04 01:06:59', '2018-12-04 01:24:58');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_configuration_kpi`
--

DROP TABLE IF EXISTS `ps_configuration_kpi`;
CREATE TABLE `ps_configuration_kpi` (
  `id_configuration_kpi` int(10) UNSIGNED NOT NULL,
  `id_shop_group` int(11) UNSIGNED DEFAULT NULL,
  `id_shop` int(11) UNSIGNED DEFAULT NULL,
  `name` varchar(64) NOT NULL,
  `value` text DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_configuration_kpi`
--

INSERT INTO `ps_configuration_kpi` (`id_configuration_kpi`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES
(1, NULL, NULL, 'DASHGOALS_TRAFFIC_01_2018', '600', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(2, NULL, NULL, 'DASHGOALS_CONVERSION_01_2018', '2', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(3, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_01_2018', '80', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(4, NULL, NULL, 'DASHGOALS_TRAFFIC_02_2018', '600', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(5, NULL, NULL, 'DASHGOALS_CONVERSION_02_2018', '2', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(6, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_02_2018', '80', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(7, NULL, NULL, 'DASHGOALS_TRAFFIC_03_2018', '600', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(8, NULL, NULL, 'DASHGOALS_CONVERSION_03_2018', '2', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(9, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_03_2018', '80', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(10, NULL, NULL, 'DASHGOALS_TRAFFIC_04_2018', '600', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(11, NULL, NULL, 'DASHGOALS_CONVERSION_04_2018', '2', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(12, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_04_2018', '80', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(13, NULL, NULL, 'DASHGOALS_TRAFFIC_05_2018', '600', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(14, NULL, NULL, 'DASHGOALS_CONVERSION_05_2018', '2', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(15, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_05_2018', '80', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(16, NULL, NULL, 'DASHGOALS_TRAFFIC_06_2018', '600', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(17, NULL, NULL, 'DASHGOALS_CONVERSION_06_2018', '2', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(18, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_06_2018', '80', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(19, NULL, NULL, 'DASHGOALS_TRAFFIC_07_2018', '600', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(20, NULL, NULL, 'DASHGOALS_CONVERSION_07_2018', '2', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(21, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_07_2018', '80', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(22, NULL, NULL, 'DASHGOALS_TRAFFIC_08_2018', '600', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(23, NULL, NULL, 'DASHGOALS_CONVERSION_08_2018', '2', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(24, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_08_2018', '80', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(25, NULL, NULL, 'DASHGOALS_TRAFFIC_09_2018', '600', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(26, NULL, NULL, 'DASHGOALS_CONVERSION_09_2018', '2', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(27, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_09_2018', '80', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(28, NULL, NULL, 'DASHGOALS_TRAFFIC_10_2018', '600', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(29, NULL, NULL, 'DASHGOALS_CONVERSION_10_2018', '2', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(30, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_10_2018', '80', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(31, NULL, NULL, 'DASHGOALS_TRAFFIC_11_2018', '600', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(32, NULL, NULL, 'DASHGOALS_CONVERSION_11_2018', '2', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(33, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_11_2018', '80', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(34, NULL, NULL, 'DASHGOALS_TRAFFIC_12_2018', '600', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(35, NULL, NULL, 'DASHGOALS_CONVERSION_12_2018', '2', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(36, NULL, NULL, 'DASHGOALS_AVG_CART_VALUE_12_2018', '80', '2018-12-03 17:18:30', '2018-12-03 17:18:30'),
(37, NULL, NULL, 'UPDATE_MODULES', '0', '2018-12-04 00:38:24', '2018-12-04 00:38:24');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_configuration_kpi_lang`
--

DROP TABLE IF EXISTS `ps_configuration_kpi_lang`;
CREATE TABLE `ps_configuration_kpi_lang` (
  `id_configuration_kpi` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `value` text DEFAULT NULL,
  `date_upd` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_configuration_lang`
--

DROP TABLE IF EXISTS `ps_configuration_lang`;
CREATE TABLE `ps_configuration_lang` (
  `id_configuration` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `value` text DEFAULT NULL,
  `date_upd` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_configuration_lang`
--

INSERT INTO `ps_configuration_lang` (`id_configuration`, `id_lang`, `value`, `date_upd`) VALUES
(41, 1, '#W', NULL),
(44, 1, '#DE', NULL),
(46, 1, '#RE', NULL),
(52, 1, '', '2018-12-03 23:30:03'),
(74, 1, '0', NULL),
(80, 1, 'Szanowny Kliencie,\r\n\r\nZ wyrazami szacunku,\r\nDzial obslugi klienta', NULL),
(288, 1, 'sale70.png', '2018-12-03 17:18:22'),
(289, 1, '', '2018-12-03 17:18:22'),
(290, 1, '', '2018-12-03 17:18:22');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_connections`
--

DROP TABLE IF EXISTS `ps_connections`;
CREATE TABLE `ps_connections` (
  `id_connections` int(10) UNSIGNED NOT NULL,
  `id_shop_group` int(11) UNSIGNED NOT NULL DEFAULT 1,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT 1,
  `id_guest` int(10) UNSIGNED NOT NULL,
  `id_page` int(10) UNSIGNED NOT NULL,
  `ip_address` bigint(20) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `http_referer` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_connections`
--

INSERT INTO `ps_connections` (`id_connections`, `id_shop_group`, `id_shop`, `id_guest`, `id_page`, `ip_address`, `date_add`, `http_referer`) VALUES
(1, 1, 1, 1, 1, 2130706433, '2018-12-03 17:18:14', 'https://www.prestashop.com'),
(2, 1, 1, 2, 1, 2887013374, '2018-12-03 17:23:10', ''),
(3, 1, 1, 3, 1, 2887013374, '2018-12-03 18:17:34', ''),
(4, 1, 1, 2, 1, 2887013374, '2018-12-03 22:11:44', ''),
(5, 1, 1, 4, 1, 2887013374, '2018-12-03 22:34:28', ''),
(6, 1, 1, 4, 1, 2887013374, '2018-12-03 23:07:34', ''),
(7, 1, 1, 4, 1, 2887013374, '2018-12-03 23:44:23', ''),
(8, 1, 1, 5, 1, 2887013374, '2018-12-04 00:14:39', ''),
(9, 1, 1, 5, 2, 2887013374, '2018-12-04 00:49:24', ''),
(10, 1, 1, 4, 1, 2887013374, '2018-12-04 00:50:02', ''),
(11, 1, 1, 5, 1, 2887013374, '2018-12-04 01:20:55', ''),
(12, 1, 1, 6, 1, 2887013374, '2018-12-04 11:51:48', ''),
(13, 1, 1, 7, 1, 2568173086, '2018-12-04 12:09:57', ''),
(14, 1, 1, 8, 1, 2886996471, '2018-12-04 17:43:59', ''),
(15, 1, 1, 9, 1, 2887013374, '2018-12-04 19:18:40', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_connections_page`
--

DROP TABLE IF EXISTS `ps_connections_page`;
CREATE TABLE `ps_connections_page` (
  `id_connections` int(10) UNSIGNED NOT NULL,
  `id_page` int(10) UNSIGNED NOT NULL,
  `time_start` datetime NOT NULL,
  `time_end` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_connections_source`
--

DROP TABLE IF EXISTS `ps_connections_source`;
CREATE TABLE `ps_connections_source` (
  `id_connections_source` int(10) UNSIGNED NOT NULL,
  `id_connections` int(10) UNSIGNED NOT NULL,
  `http_referer` varchar(255) DEFAULT NULL,
  `request_uri` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_contact`
--

DROP TABLE IF EXISTS `ps_contact`;
CREATE TABLE `ps_contact` (
  `id_contact` int(10) UNSIGNED NOT NULL,
  `email` varchar(128) NOT NULL,
  `customer_service` tinyint(1) NOT NULL DEFAULT 0,
  `position` tinyint(2) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_contact`
--

INSERT INTO `ps_contact` (`id_contact`, `email`, `customer_service`, `position`) VALUES
(1, 'felo.dawid@gmail.com', 1, 0),
(2, 'felo.dawid@gmail.com', 1, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_contact_lang`
--

DROP TABLE IF EXISTS `ps_contact_lang`;
CREATE TABLE `ps_contact_lang` (
  `id_contact` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_contact_lang`
--

INSERT INTO `ps_contact_lang` (`id_contact`, `id_lang`, `name`, `description`) VALUES
(1, 1, 'Webmaster', 'JeĹli pojawiĹ siÄ problem techniczny na tej stronie'),
(2, 1, 'ObsĹuga klienta', 'Wszelkie pytania dotyczÄce produktĂłw i zamĂłwieĹ');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_contact_shop`
--

DROP TABLE IF EXISTS `ps_contact_shop`;
CREATE TABLE `ps_contact_shop` (
  `id_contact` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_contact_shop`
--

INSERT INTO `ps_contact_shop` (`id_contact`, `id_shop`) VALUES
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_country`
--

DROP TABLE IF EXISTS `ps_country`;
CREATE TABLE `ps_country` (
  `id_country` int(10) UNSIGNED NOT NULL,
  `id_zone` int(10) UNSIGNED NOT NULL,
  `id_currency` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `iso_code` varchar(3) NOT NULL,
  `call_prefix` int(10) NOT NULL DEFAULT 0,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `contains_states` tinyint(1) NOT NULL DEFAULT 0,
  `need_identification_number` tinyint(1) NOT NULL DEFAULT 0,
  `need_zip_code` tinyint(1) NOT NULL DEFAULT 1,
  `zip_code_format` varchar(12) NOT NULL DEFAULT '',
  `display_tax_label` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_country`
--

INSERT INTO `ps_country` (`id_country`, `id_zone`, `id_currency`, `iso_code`, `call_prefix`, `active`, `contains_states`, `need_identification_number`, `need_zip_code`, `zip_code_format`, `display_tax_label`) VALUES
(1, 1, 0, 'DE', 49, 0, 0, 0, 1, 'NNNNN', 1),
(2, 1, 0, 'AT', 43, 0, 0, 0, 1, 'NNNN', 1),
(3, 1, 0, 'BE', 32, 0, 0, 0, 1, 'NNNN', 1),
(4, 2, 0, 'CA', 1, 0, 1, 0, 1, 'LNL NLN', 0),
(5, 3, 0, 'CN', 86, 0, 0, 0, 1, 'NNNNNN', 1),
(6, 1, 0, 'ES', 34, 0, 0, 1, 1, 'NNNNN', 1),
(7, 1, 0, 'FI', 358, 0, 0, 0, 1, 'NNNNN', 1),
(8, 1, 0, 'FR', 33, 0, 0, 0, 1, 'NNNNN', 1),
(9, 1, 0, 'GR', 30, 0, 0, 0, 1, 'NNNNN', 1),
(10, 1, 0, 'IT', 39, 0, 1, 0, 1, 'NNNNN', 1),
(11, 3, 0, 'JP', 81, 0, 1, 0, 1, 'NNN-NNNN', 1),
(12, 1, 0, 'LU', 352, 0, 0, 0, 1, 'NNNN', 1),
(13, 1, 0, 'NL', 31, 0, 0, 0, 1, 'NNNN LL', 1),
(14, 1, 0, 'PL', 48, 1, 0, 0, 1, 'NN-NNN', 1),
(15, 1, 0, 'PT', 351, 0, 0, 0, 1, 'NNNN-NNN', 1),
(16, 1, 0, 'CZ', 420, 0, 0, 0, 1, 'NNN NN', 1),
(17, 1, 0, 'GB', 44, 0, 0, 0, 1, '', 1),
(18, 1, 0, 'SE', 46, 0, 0, 0, 1, 'NNN NN', 1),
(19, 7, 0, 'CH', 41, 0, 0, 0, 1, 'NNNN', 1),
(20, 1, 0, 'DK', 45, 0, 0, 0, 1, 'NNNN', 1),
(21, 2, 0, 'US', 1, 0, 1, 0, 1, 'NNNNN', 0),
(22, 3, 0, 'HK', 852, 0, 0, 0, 0, '', 1),
(23, 7, 0, 'NO', 47, 0, 0, 0, 1, 'NNNN', 1),
(24, 5, 0, 'AU', 61, 0, 0, 0, 1, 'NNNN', 1),
(25, 3, 0, 'SG', 65, 0, 0, 0, 1, 'NNNNNN', 1),
(26, 1, 0, 'IE', 353, 0, 0, 0, 0, '', 1),
(27, 5, 0, 'NZ', 64, 0, 0, 0, 1, 'NNNN', 1),
(28, 3, 0, 'KR', 82, 0, 0, 0, 1, 'NNNNN', 1),
(29, 3, 0, 'IL', 972, 0, 0, 0, 1, 'NNNNNNN', 1),
(30, 4, 0, 'ZA', 27, 0, 0, 0, 1, 'NNNN', 1),
(31, 4, 0, 'NG', 234, 0, 0, 0, 1, '', 1),
(32, 4, 0, 'CI', 225, 0, 0, 0, 1, '', 1),
(33, 4, 0, 'TG', 228, 0, 0, 0, 1, '', 1),
(34, 6, 0, 'BO', 591, 0, 0, 0, 1, '', 1),
(35, 4, 0, 'MU', 230, 0, 0, 0, 1, '', 1),
(36, 1, 0, 'RO', 40, 0, 0, 0, 1, 'NNNNNN', 1),
(37, 1, 0, 'SK', 421, 0, 0, 0, 1, 'NNN NN', 1),
(38, 4, 0, 'DZ', 213, 0, 0, 0, 1, 'NNNNN', 1),
(39, 2, 0, 'AS', 0, 0, 0, 0, 1, '', 1),
(40, 7, 0, 'AD', 376, 0, 0, 0, 1, 'CNNN', 1),
(41, 4, 0, 'AO', 244, 0, 0, 0, 0, '', 1),
(42, 8, 0, 'AI', 0, 0, 0, 0, 1, '', 1),
(43, 2, 0, 'AG', 0, 0, 0, 0, 1, '', 1),
(44, 6, 0, 'AR', 54, 0, 1, 0, 1, 'LNNNNLLL', 1),
(45, 3, 0, 'AM', 374, 0, 0, 0, 1, 'NNNN', 1),
(46, 8, 0, 'AW', 297, 0, 0, 0, 1, '', 1),
(47, 3, 0, 'AZ', 994, 0, 0, 0, 1, 'CNNNN', 1),
(48, 2, 0, 'BS', 0, 0, 0, 0, 1, '', 1),
(49, 3, 0, 'BH', 973, 0, 0, 0, 1, '', 1),
(50, 3, 0, 'BD', 880, 0, 0, 0, 1, 'NNNN', 1),
(51, 2, 0, 'BB', 0, 0, 0, 0, 1, 'CNNNNN', 1),
(52, 7, 0, 'BY', 0, 0, 0, 0, 1, 'NNNNNN', 1),
(53, 8, 0, 'BZ', 501, 0, 0, 0, 0, '', 1),
(54, 4, 0, 'BJ', 229, 0, 0, 0, 0, '', 1),
(55, 2, 0, 'BM', 0, 0, 0, 0, 1, '', 1),
(56, 3, 0, 'BT', 975, 0, 0, 0, 1, '', 1),
(57, 4, 0, 'BW', 267, 0, 0, 0, 1, '', 1),
(58, 6, 0, 'BR', 55, 0, 0, 0, 1, 'NNNNN-NNN', 1),
(59, 3, 0, 'BN', 673, 0, 0, 0, 1, 'LLNNNN', 1),
(60, 4, 0, 'BF', 226, 0, 0, 0, 1, '', 1),
(61, 3, 0, 'MM', 95, 0, 0, 0, 1, '', 1),
(62, 4, 0, 'BI', 257, 0, 0, 0, 1, '', 1),
(63, 3, 0, 'KH', 855, 0, 0, 0, 1, 'NNNNN', 1),
(64, 4, 0, 'CM', 237, 0, 0, 0, 1, '', 1),
(65, 4, 0, 'CV', 238, 0, 0, 0, 1, 'NNNN', 1),
(66, 4, 0, 'CF', 236, 0, 0, 0, 1, '', 1),
(67, 4, 0, 'TD', 235, 0, 0, 0, 1, '', 1),
(68, 6, 0, 'CL', 56, 0, 0, 0, 1, 'NNN-NNNN', 1),
(69, 6, 0, 'CO', 57, 0, 0, 0, 1, 'NNNNNN', 1),
(70, 4, 0, 'KM', 269, 0, 0, 0, 1, '', 1),
(71, 4, 0, 'CD', 242, 0, 0, 0, 1, '', 1),
(72, 4, 0, 'CG', 243, 0, 0, 0, 1, '', 1),
(73, 8, 0, 'CR', 506, 0, 0, 0, 1, 'NNNNN', 1),
(74, 7, 0, 'HR', 385, 0, 0, 0, 1, 'NNNNN', 1),
(75, 8, 0, 'CU', 53, 0, 0, 0, 1, '', 1),
(76, 1, 0, 'CY', 357, 0, 0, 0, 1, 'NNNN', 1),
(77, 4, 0, 'DJ', 253, 0, 0, 0, 1, '', 1),
(78, 8, 0, 'DM', 0, 0, 0, 0, 1, '', 1),
(79, 8, 0, 'DO', 0, 0, 0, 0, 1, '', 1),
(80, 3, 0, 'TL', 670, 0, 0, 0, 1, '', 1),
(81, 6, 0, 'EC', 593, 0, 0, 0, 1, 'CNNNNNN', 1),
(82, 4, 0, 'EG', 20, 0, 0, 0, 1, 'NNNNN', 1),
(83, 8, 0, 'SV', 503, 0, 0, 0, 1, '', 1),
(84, 4, 0, 'GQ', 240, 0, 0, 0, 1, '', 1),
(85, 4, 0, 'ER', 291, 0, 0, 0, 1, '', 1),
(86, 1, 0, 'EE', 372, 0, 0, 0, 1, 'NNNNN', 1),
(87, 4, 0, 'ET', 251, 0, 0, 0, 1, '', 1),
(88, 8, 0, 'FK', 0, 0, 0, 0, 1, 'LLLL NLL', 1),
(89, 7, 0, 'FO', 298, 0, 0, 0, 1, '', 1),
(90, 5, 0, 'FJ', 679, 0, 0, 0, 1, '', 1),
(91, 4, 0, 'GA', 241, 0, 0, 0, 1, '', 1),
(92, 4, 0, 'GM', 220, 0, 0, 0, 1, '', 1),
(93, 3, 0, 'GE', 995, 0, 0, 0, 1, 'NNNN', 1),
(94, 4, 0, 'GH', 233, 0, 0, 0, 1, '', 1),
(95, 8, 0, 'GD', 0, 0, 0, 0, 1, '', 1),
(96, 7, 0, 'GL', 299, 0, 0, 0, 1, '', 1),
(97, 7, 0, 'GI', 350, 0, 0, 0, 1, '', 1),
(98, 8, 0, 'GP', 590, 0, 0, 0, 1, '', 1),
(99, 5, 0, 'GU', 0, 0, 0, 0, 1, '', 1),
(100, 8, 0, 'GT', 502, 0, 0, 0, 1, '', 1),
(101, 7, 0, 'GG', 0, 0, 0, 0, 1, 'LLN NLL', 1),
(102, 4, 0, 'GN', 224, 0, 0, 0, 1, '', 1),
(103, 4, 0, 'GW', 245, 0, 0, 0, 1, '', 1),
(104, 6, 0, 'GY', 592, 0, 0, 0, 1, '', 1),
(105, 8, 0, 'HT', 509, 0, 0, 0, 1, '', 1),
(106, 5, 0, 'HM', 0, 0, 0, 0, 1, '', 1),
(107, 7, 0, 'VA', 379, 0, 0, 0, 1, 'NNNNN', 1),
(108, 8, 0, 'HN', 504, 0, 0, 0, 1, '', 1),
(109, 7, 0, 'IS', 354, 0, 0, 0, 1, 'NNN', 1),
(110, 3, 0, 'IN', 91, 0, 0, 0, 1, 'NNN NNN', 1),
(111, 3, 0, 'ID', 62, 0, 1, 0, 1, 'NNNNN', 1),
(112, 3, 0, 'IR', 98, 0, 0, 0, 1, 'NNNNN-NNNNN', 1),
(113, 3, 0, 'IQ', 964, 0, 0, 0, 1, 'NNNNN', 1),
(114, 7, 0, 'IM', 0, 0, 0, 0, 1, 'CN NLL', 1),
(115, 8, 0, 'JM', 0, 0, 0, 0, 1, '', 1),
(116, 7, 0, 'JE', 0, 0, 0, 0, 1, 'CN NLL', 1),
(117, 3, 0, 'JO', 962, 0, 0, 0, 1, '', 1),
(118, 3, 0, 'KZ', 7, 0, 0, 0, 1, 'NNNNNN', 1),
(119, 4, 0, 'KE', 254, 0, 0, 0, 1, '', 1),
(120, 5, 0, 'KI', 686, 0, 0, 0, 1, '', 1),
(121, 3, 0, 'KP', 850, 0, 0, 0, 1, '', 1),
(122, 3, 0, 'KW', 965, 0, 0, 0, 1, '', 1),
(123, 3, 0, 'KG', 996, 0, 0, 0, 1, '', 1),
(124, 3, 0, 'LA', 856, 0, 0, 0, 1, '', 1),
(125, 1, 0, 'LV', 371, 0, 0, 0, 1, 'C-NNNN', 1),
(126, 3, 0, 'LB', 961, 0, 0, 0, 1, '', 1),
(127, 4, 0, 'LS', 266, 0, 0, 0, 1, '', 1),
(128, 4, 0, 'LR', 231, 0, 0, 0, 1, '', 1),
(129, 4, 0, 'LY', 218, 0, 0, 0, 1, '', 1),
(130, 1, 0, 'LI', 423, 0, 0, 0, 1, 'NNNN', 1),
(131, 1, 0, 'LT', 370, 0, 0, 0, 1, 'NNNNN', 1),
(132, 3, 0, 'MO', 853, 0, 0, 0, 0, '', 1),
(133, 7, 0, 'MK', 389, 0, 0, 0, 1, '', 1),
(134, 4, 0, 'MG', 261, 0, 0, 0, 1, '', 1),
(135, 4, 0, 'MW', 265, 0, 0, 0, 1, '', 1),
(136, 3, 0, 'MY', 60, 0, 0, 0, 1, 'NNNNN', 1),
(137, 3, 0, 'MV', 960, 0, 0, 0, 1, '', 1),
(138, 4, 0, 'ML', 223, 0, 0, 0, 1, '', 1),
(139, 1, 0, 'MT', 356, 0, 0, 0, 1, 'LLL NNNN', 1),
(140, 5, 0, 'MH', 692, 0, 0, 0, 1, '', 1),
(141, 8, 0, 'MQ', 596, 0, 0, 0, 1, '', 1),
(142, 4, 0, 'MR', 222, 0, 0, 0, 1, '', 1),
(143, 1, 0, 'HU', 36, 0, 0, 0, 1, 'NNNN', 1),
(144, 4, 0, 'YT', 262, 0, 0, 0, 1, '', 1),
(145, 2, 0, 'MX', 52, 0, 1, 1, 1, 'NNNNN', 1),
(146, 5, 0, 'FM', 691, 0, 0, 0, 1, '', 1),
(147, 7, 0, 'MD', 373, 0, 0, 0, 1, 'C-NNNN', 1),
(148, 7, 0, 'MC', 377, 0, 0, 0, 1, '980NN', 1),
(149, 3, 0, 'MN', 976, 0, 0, 0, 1, '', 1),
(150, 7, 0, 'ME', 382, 0, 0, 0, 1, 'NNNNN', 1),
(151, 8, 0, 'MS', 0, 0, 0, 0, 1, '', 1),
(152, 4, 0, 'MA', 212, 0, 0, 0, 1, 'NNNNN', 1),
(153, 4, 0, 'MZ', 258, 0, 0, 0, 1, '', 1),
(154, 4, 0, 'NA', 264, 0, 0, 0, 1, '', 1),
(155, 5, 0, 'NR', 674, 0, 0, 0, 1, '', 1),
(156, 3, 0, 'NP', 977, 0, 0, 0, 1, '', 1),
(157, 8, 0, 'AN', 599, 0, 0, 0, 1, '', 1),
(158, 5, 0, 'NC', 687, 0, 0, 0, 1, '', 1),
(159, 8, 0, 'NI', 505, 0, 0, 0, 1, 'NNNNNN', 1),
(160, 4, 0, 'NE', 227, 0, 0, 0, 1, '', 1),
(161, 5, 0, 'NU', 683, 0, 0, 0, 1, '', 1),
(162, 5, 0, 'NF', 0, 0, 0, 0, 1, '', 1),
(163, 5, 0, 'MP', 0, 0, 0, 0, 1, '', 1),
(164, 3, 0, 'OM', 968, 0, 0, 0, 1, '', 1),
(165, 3, 0, 'PK', 92, 0, 0, 0, 1, '', 1),
(166, 5, 0, 'PW', 680, 0, 0, 0, 1, '', 1),
(167, 3, 0, 'PS', 0, 0, 0, 0, 1, '', 1),
(168, 8, 0, 'PA', 507, 0, 0, 0, 1, 'NNNNNN', 1),
(169, 5, 0, 'PG', 675, 0, 0, 0, 1, '', 1),
(170, 6, 0, 'PY', 595, 0, 0, 0, 1, '', 1),
(171, 6, 0, 'PE', 51, 0, 0, 0, 1, '', 1),
(172, 3, 0, 'PH', 63, 0, 0, 0, 1, 'NNNN', 1),
(173, 5, 0, 'PN', 0, 0, 0, 0, 1, 'LLLL NLL', 1),
(174, 8, 0, 'PR', 0, 0, 0, 0, 1, 'NNNNN', 1),
(175, 3, 0, 'QA', 974, 0, 0, 0, 1, '', 1),
(176, 4, 0, 'RE', 262, 0, 0, 0, 1, '', 1),
(177, 7, 0, 'RU', 7, 0, 0, 0, 1, 'NNNNNN', 1),
(178, 4, 0, 'RW', 250, 0, 0, 0, 1, '', 1),
(179, 8, 0, 'BL', 0, 0, 0, 0, 1, '', 1),
(180, 8, 0, 'KN', 0, 0, 0, 0, 1, '', 1),
(181, 8, 0, 'LC', 0, 0, 0, 0, 1, '', 1),
(182, 8, 0, 'MF', 0, 0, 0, 0, 1, '', 1),
(183, 8, 0, 'PM', 508, 0, 0, 0, 1, '', 1),
(184, 8, 0, 'VC', 0, 0, 0, 0, 1, '', 1),
(185, 5, 0, 'WS', 685, 0, 0, 0, 1, '', 1),
(186, 7, 0, 'SM', 378, 0, 0, 0, 1, 'NNNNN', 1),
(187, 4, 0, 'ST', 239, 0, 0, 0, 1, '', 1),
(188, 3, 0, 'SA', 966, 0, 0, 0, 1, '', 1),
(189, 4, 0, 'SN', 221, 0, 0, 0, 1, '', 1),
(190, 7, 0, 'RS', 381, 0, 0, 0, 1, 'NNNNN', 1),
(191, 4, 0, 'SC', 248, 0, 0, 0, 1, '', 1),
(192, 4, 0, 'SL', 232, 0, 0, 0, 1, '', 1),
(193, 1, 0, 'SI', 386, 0, 0, 0, 1, 'C-NNNN', 1),
(194, 5, 0, 'SB', 677, 0, 0, 0, 1, '', 1),
(195, 4, 0, 'SO', 252, 0, 0, 0, 1, '', 1),
(196, 8, 0, 'GS', 0, 0, 0, 0, 1, 'LLLL NLL', 1),
(197, 3, 0, 'LK', 94, 0, 0, 0, 1, 'NNNNN', 1),
(198, 4, 0, 'SD', 249, 0, 0, 0, 1, '', 1),
(199, 8, 0, 'SR', 597, 0, 0, 0, 1, '', 1),
(200, 7, 0, 'SJ', 0, 0, 0, 0, 1, '', 1),
(201, 4, 0, 'SZ', 268, 0, 0, 0, 1, '', 1),
(202, 3, 0, 'SY', 963, 0, 0, 0, 1, '', 1),
(203, 3, 0, 'TW', 886, 0, 0, 0, 1, 'NNNNN', 1),
(204, 3, 0, 'TJ', 992, 0, 0, 0, 1, '', 1),
(205, 4, 0, 'TZ', 255, 0, 0, 0, 1, '', 1),
(206, 3, 0, 'TH', 66, 0, 0, 0, 1, 'NNNNN', 1),
(207, 5, 0, 'TK', 690, 0, 0, 0, 1, '', 1),
(208, 5, 0, 'TO', 676, 0, 0, 0, 1, '', 1),
(209, 6, 0, 'TT', 0, 0, 0, 0, 1, '', 1),
(210, 4, 0, 'TN', 216, 0, 0, 0, 1, '', 1),
(211, 7, 0, 'TR', 90, 0, 0, 0, 1, 'NNNNN', 1),
(212, 3, 0, 'TM', 993, 0, 0, 0, 1, '', 1),
(213, 8, 0, 'TC', 0, 0, 0, 0, 1, 'LLLL NLL', 1),
(214, 5, 0, 'TV', 688, 0, 0, 0, 1, '', 1),
(215, 4, 0, 'UG', 256, 0, 0, 0, 1, '', 1),
(216, 1, 0, 'UA', 380, 0, 0, 0, 1, 'NNNNN', 1),
(217, 3, 0, 'AE', 971, 0, 0, 0, 1, '', 1),
(218, 6, 0, 'UY', 598, 0, 0, 0, 1, '', 1),
(219, 3, 0, 'UZ', 998, 0, 0, 0, 1, '', 1),
(220, 5, 0, 'VU', 678, 0, 0, 0, 1, '', 1),
(221, 6, 0, 'VE', 58, 0, 0, 0, 1, '', 1),
(222, 3, 0, 'VN', 84, 0, 0, 0, 1, 'NNNNNN', 1),
(223, 2, 0, 'VG', 0, 0, 0, 0, 1, 'CNNNN', 1),
(224, 2, 0, 'VI', 0, 0, 0, 0, 1, '', 1),
(225, 5, 0, 'WF', 681, 0, 0, 0, 1, '', 1),
(226, 4, 0, 'EH', 0, 0, 0, 0, 1, '', 1),
(227, 3, 0, 'YE', 967, 0, 0, 0, 1, '', 1),
(228, 4, 0, 'ZM', 260, 0, 0, 0, 1, '', 1),
(229, 4, 0, 'ZW', 263, 0, 0, 0, 1, '', 1),
(230, 7, 0, 'AL', 355, 0, 0, 0, 1, 'NNNN', 1),
(231, 3, 0, 'AF', 93, 0, 0, 0, 1, 'NNNN', 1),
(232, 5, 0, 'AQ', 0, 0, 0, 0, 1, '', 1),
(233, 1, 0, 'BA', 387, 0, 0, 0, 1, '', 1),
(234, 5, 0, 'BV', 0, 0, 0, 0, 1, '', 1),
(235, 5, 0, 'IO', 0, 0, 0, 0, 1, 'LLLL NLL', 1),
(236, 1, 0, 'BG', 359, 0, 0, 0, 1, 'NNNN', 1),
(237, 8, 0, 'KY', 0, 0, 0, 0, 1, '', 1),
(238, 3, 0, 'CX', 0, 0, 0, 0, 1, '', 1),
(239, 3, 0, 'CC', 0, 0, 0, 0, 1, '', 1),
(240, 5, 0, 'CK', 682, 0, 0, 0, 1, '', 1),
(241, 6, 0, 'GF', 594, 0, 0, 0, 1, '', 1),
(242, 5, 0, 'PF', 689, 0, 0, 0, 1, '', 1),
(243, 5, 0, 'TF', 0, 0, 0, 0, 1, '', 1),
(244, 7, 0, 'AX', 0, 0, 0, 0, 1, 'NNNNN', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_country_lang`
--

DROP TABLE IF EXISTS `ps_country_lang`;
CREATE TABLE `ps_country_lang` (
  `id_country` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_country_lang`
--

INSERT INTO `ps_country_lang` (`id_country`, `id_lang`, `name`) VALUES
(1, 1, 'Niemcy'),
(2, 1, 'Austria'),
(3, 1, 'Belgia'),
(4, 1, 'Kanada'),
(5, 1, 'Chiny'),
(6, 1, 'Hiszpania'),
(7, 1, 'Finlandia'),
(8, 1, 'Francja'),
(9, 1, 'Grecja'),
(10, 1, 'WĹochy'),
(11, 1, 'Japonia'),
(12, 1, 'Luksemburg'),
(13, 1, 'Holandia'),
(14, 1, 'Polska'),
(15, 1, 'Portugalia'),
(16, 1, 'Czechy'),
(17, 1, 'Wielka Brytania'),
(18, 1, 'Szwecja'),
(19, 1, 'Szwajcaria'),
(20, 1, 'Dania'),
(21, 1, 'Stany Zjednoczone'),
(22, 1, 'Hongkong'),
(23, 1, 'Norwegia'),
(24, 1, 'Australia'),
(25, 1, 'Singapur'),
(26, 1, 'Irlandia'),
(27, 1, 'Nowa Zelandia'),
(28, 1, 'Korea PoĹudniowa'),
(29, 1, 'Izrael'),
(30, 1, 'Republika PoĹudniowej Afryki'),
(31, 1, 'Nigeria'),
(32, 1, 'WybrzeĹźe KoĹci SĹoniowej'),
(33, 1, 'Togo'),
(34, 1, 'Boliwia'),
(35, 1, 'Mauritius'),
(36, 1, 'Rumunia'),
(37, 1, 'SĹowacja'),
(38, 1, 'Algieria'),
(39, 1, 'Samoa AmerykaĹskie'),
(40, 1, 'Andora'),
(41, 1, 'Angola'),
(42, 1, 'Anguilla'),
(43, 1, 'Antigua i Barbuda'),
(44, 1, 'Argentyna'),
(45, 1, 'Armenia'),
(46, 1, 'Aruba'),
(47, 1, 'AzerbejdĹźan'),
(48, 1, 'Bahamy'),
(49, 1, 'Bahrajn'),
(50, 1, 'Bangladesz'),
(51, 1, 'Barbados'),
(52, 1, 'BiaĹoruĹ'),
(53, 1, 'Belize'),
(54, 1, 'Benin'),
(55, 1, 'Bermudy'),
(56, 1, 'Bhutan'),
(57, 1, 'Botswana'),
(58, 1, 'Brazylia'),
(59, 1, 'Brunei'),
(60, 1, 'Burkina Faso'),
(61, 1, 'Mjanma'),
(62, 1, 'Burundi'),
(63, 1, 'KambodĹźa'),
(64, 1, 'Kamerun'),
(65, 1, 'Republika Zielonego PrzylÄdka'),
(66, 1, 'Republika ĹrodkowoafrykaĹska'),
(67, 1, 'Czad'),
(68, 1, 'Chile'),
(69, 1, 'Kolumbia'),
(70, 1, 'Komory'),
(71, 1, 'Demokratyczna Republika Konga'),
(72, 1, 'Kongo'),
(73, 1, 'Kostaryka'),
(74, 1, 'Chorwacja'),
(75, 1, 'Kuba'),
(76, 1, 'Cypr'),
(77, 1, 'DĹźibuti'),
(78, 1, 'Dominika'),
(79, 1, 'Dominikana'),
(80, 1, 'Timor Wschodni'),
(81, 1, 'Ekwador'),
(82, 1, 'Egipt'),
(83, 1, 'Salwador'),
(84, 1, 'Gwinea RĂłwnikowa'),
(85, 1, 'Erytrea'),
(86, 1, 'Estonia'),
(87, 1, 'Etiopia'),
(88, 1, 'Falklandy'),
(89, 1, 'Wyspy Owcze'),
(90, 1, 'FidĹźi'),
(91, 1, 'Gabon'),
(92, 1, 'Gambia'),
(93, 1, 'Gruzja'),
(94, 1, 'Ghana'),
(95, 1, 'Grenada'),
(96, 1, 'Grenlandia'),
(97, 1, 'Gibraltar'),
(98, 1, 'Gwadelupa'),
(99, 1, 'Guam'),
(100, 1, 'Gwatemala'),
(101, 1, 'Guernsey'),
(102, 1, 'Gwinea'),
(103, 1, 'Gwinea Bissau'),
(104, 1, 'Gujana'),
(105, 1, 'Haiti'),
(106, 1, 'Wyspy Heard i McDonalda'),
(107, 1, 'Watykan'),
(108, 1, 'Honduras'),
(109, 1, 'Islandia'),
(110, 1, 'Indie'),
(111, 1, 'Indonezja'),
(112, 1, 'Iran'),
(113, 1, 'Irak'),
(114, 1, 'Wyspa Man'),
(115, 1, 'Jamajka'),
(116, 1, 'Jersey'),
(117, 1, 'Jordania'),
(118, 1, 'Kazachstan'),
(119, 1, 'Kenia'),
(120, 1, 'Kiribati'),
(121, 1, 'Korea PĂłĹnocna'),
(122, 1, 'Kuwejt'),
(123, 1, 'Kirgistan'),
(124, 1, 'Laos'),
(125, 1, 'Ĺotwa'),
(126, 1, 'Liban'),
(127, 1, 'Lesotho'),
(128, 1, 'Liberia'),
(129, 1, 'Libia'),
(130, 1, 'Liechtenstein'),
(131, 1, 'Litwa'),
(132, 1, 'Makau'),
(133, 1, 'Macedonia'),
(134, 1, 'Madagaskar'),
(135, 1, 'Malawi'),
(136, 1, 'Malezja'),
(137, 1, 'Malediwy'),
(138, 1, 'Mali'),
(139, 1, 'Malta'),
(140, 1, 'Wyspy Marshalla'),
(141, 1, 'Martynika'),
(142, 1, 'Mauretania'),
(143, 1, 'WÄgry'),
(144, 1, 'Majotta'),
(145, 1, 'Meksyk'),
(146, 1, 'Mikronezja'),
(147, 1, 'MoĹdawia'),
(148, 1, 'Monako'),
(149, 1, 'Mongolia'),
(150, 1, 'CzarnogĂłra'),
(151, 1, 'Montserrat'),
(152, 1, 'Maroko'),
(153, 1, 'Mozambik'),
(154, 1, 'Namibia'),
(155, 1, 'Nauru'),
(156, 1, 'Nepal'),
(158, 1, 'Nowa Kaledonia'),
(159, 1, 'Nikaragua'),
(160, 1, 'Niger'),
(161, 1, 'Niue'),
(162, 1, 'Norfolk'),
(163, 1, 'Mariany PĂłĹnocne'),
(164, 1, 'Oman'),
(165, 1, 'Pakistan'),
(166, 1, 'Palau'),
(167, 1, 'Palestyna'),
(168, 1, 'Panama'),
(169, 1, 'Papua-Nowa Gwinea'),
(170, 1, 'Paragwaj'),
(171, 1, 'Peru'),
(172, 1, 'Filipiny'),
(173, 1, 'Pitcairn'),
(174, 1, 'Portoryko'),
(175, 1, 'Katar'),
(176, 1, 'Reunion'),
(177, 1, 'Rosja'),
(178, 1, 'Rwanda'),
(179, 1, 'Saint-BarthĂŠlemy'),
(180, 1, 'Saint Kitts i Nevis'),
(181, 1, 'Saint Lucia'),
(182, 1, 'Saint-Martin'),
(183, 1, 'Saint-Pierre i Miquelon'),
(184, 1, 'Saint Vincent i Grenadyny'),
(185, 1, 'Samoa'),
(186, 1, 'San Marino'),
(187, 1, 'Wyspy ĹwiÄtego Tomasza i KsiÄĹźÄca'),
(188, 1, 'Arabia Saudyjska'),
(189, 1, 'Senegal'),
(190, 1, 'Serbia'),
(191, 1, 'Seszele'),
(192, 1, 'Sierra Leone'),
(193, 1, 'SĹowenia'),
(194, 1, 'Wyspy Salomona'),
(195, 1, 'Somalia'),
(196, 1, 'Georgia PoĹudniowa i Sandwich PoĹudniowy'),
(197, 1, 'Sri Lanka'),
(198, 1, 'Sudan'),
(199, 1, 'Surinam'),
(200, 1, 'Svalbard i Jan Mayen'),
(201, 1, 'Suazi'),
(202, 1, 'Syria'),
(203, 1, 'Tajwan'),
(204, 1, 'TadĹźykistan'),
(205, 1, 'Tanzania'),
(206, 1, 'Tajlandia'),
(207, 1, 'Tokelau'),
(208, 1, 'Tonga'),
(209, 1, 'Trynidad i Tobago'),
(210, 1, 'Tunezja'),
(211, 1, 'Turcja'),
(212, 1, 'Turkmenistan'),
(213, 1, 'Turks i Caicos'),
(214, 1, 'Tuvalu'),
(215, 1, 'Uganda'),
(216, 1, 'Ukraina'),
(217, 1, 'Zjednoczone Emiraty Arabskie'),
(218, 1, 'Urugwaj'),
(219, 1, 'Uzbekistan'),
(220, 1, 'Vanuatu'),
(221, 1, 'Wenezuela'),
(222, 1, 'Wietnam'),
(223, 1, 'Brytyjskie Wyspy Dziewicze'),
(224, 1, 'Wyspy Dziewicze StanĂłw Zjednoczonych'),
(225, 1, 'Wallis i Futuna'),
(226, 1, 'Sahara Zachodnia'),
(227, 1, 'Jemen'),
(228, 1, 'Zambia'),
(229, 1, 'Zimbabwe'),
(230, 1, 'Albania'),
(231, 1, 'Afganistan'),
(232, 1, 'Antarktyka'),
(233, 1, 'BoĹnia i Hercegowina'),
(234, 1, 'Wyspa Bouveta'),
(235, 1, 'Brytyjskie Terytorium Oceanu Indyjskiego'),
(236, 1, 'BuĹgaria'),
(237, 1, 'Kajmany'),
(238, 1, 'Wyspa BoĹźego Narodzenia'),
(239, 1, 'Wyspy Kokosowe'),
(240, 1, 'Wyspy Cooka'),
(241, 1, 'Gujana Francuska'),
(242, 1, 'Polinezja Francuska'),
(243, 1, 'Francuskie Terytoria PoĹudniowe i Antarktyczne'),
(244, 1, 'Wyspy Alandzkie');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_country_shop`
--

DROP TABLE IF EXISTS `ps_country_shop`;
CREATE TABLE `ps_country_shop` (
  `id_country` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_country_shop`
--

INSERT INTO `ps_country_shop` (`id_country`, `id_shop`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1),
(97, 1),
(98, 1),
(99, 1),
(100, 1),
(101, 1),
(102, 1),
(103, 1),
(104, 1),
(105, 1),
(106, 1),
(107, 1),
(108, 1),
(109, 1),
(110, 1),
(111, 1),
(112, 1),
(113, 1),
(114, 1),
(115, 1),
(116, 1),
(117, 1),
(118, 1),
(119, 1),
(120, 1),
(121, 1),
(122, 1),
(123, 1),
(124, 1),
(125, 1),
(126, 1),
(127, 1),
(128, 1),
(129, 1),
(130, 1),
(131, 1),
(132, 1),
(133, 1),
(134, 1),
(135, 1),
(136, 1),
(137, 1),
(138, 1),
(139, 1),
(140, 1),
(141, 1),
(142, 1),
(143, 1),
(144, 1),
(145, 1),
(146, 1),
(147, 1),
(148, 1),
(149, 1),
(150, 1),
(151, 1),
(152, 1),
(153, 1),
(154, 1),
(155, 1),
(156, 1),
(158, 1),
(159, 1),
(160, 1),
(161, 1),
(162, 1),
(163, 1),
(164, 1),
(165, 1),
(166, 1),
(167, 1),
(168, 1),
(169, 1),
(170, 1),
(171, 1),
(172, 1),
(173, 1),
(174, 1),
(175, 1),
(176, 1),
(177, 1),
(178, 1),
(179, 1),
(180, 1),
(181, 1),
(182, 1),
(183, 1),
(184, 1),
(185, 1),
(186, 1),
(187, 1),
(188, 1),
(189, 1),
(190, 1),
(191, 1),
(192, 1),
(193, 1),
(194, 1),
(195, 1),
(196, 1),
(197, 1),
(198, 1),
(199, 1),
(200, 1),
(201, 1),
(202, 1),
(203, 1),
(204, 1),
(205, 1),
(206, 1),
(207, 1),
(208, 1),
(209, 1),
(210, 1),
(211, 1),
(212, 1),
(213, 1),
(214, 1),
(215, 1),
(216, 1),
(217, 1),
(218, 1),
(219, 1),
(220, 1),
(221, 1),
(222, 1),
(223, 1),
(224, 1),
(225, 1),
(226, 1),
(227, 1),
(228, 1),
(229, 1),
(230, 1),
(231, 1),
(232, 1),
(233, 1),
(234, 1),
(235, 1),
(236, 1),
(237, 1),
(238, 1),
(239, 1),
(240, 1),
(241, 1),
(242, 1),
(243, 1),
(244, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_cronjobs`
--

DROP TABLE IF EXISTS `ps_cronjobs`;
CREATE TABLE `ps_cronjobs` (
  `id_cronjob` int(10) NOT NULL,
  `id_module` int(10) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `task` text DEFAULT NULL,
  `hour` int(11) DEFAULT -1,
  `day` int(11) DEFAULT -1,
  `month` int(11) DEFAULT -1,
  `day_of_week` int(11) DEFAULT -1,
  `updated_at` datetime DEFAULT NULL,
  `one_shot` tinyint(1) NOT NULL DEFAULT 0,
  `active` tinyint(1) DEFAULT 0,
  `id_shop` int(11) DEFAULT 0,
  `id_shop_group` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_currency`
--

DROP TABLE IF EXISTS `ps_currency`;
CREATE TABLE `ps_currency` (
  `id_currency` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL,
  `iso_code` varchar(3) NOT NULL DEFAULT '0',
  `iso_code_num` varchar(3) NOT NULL DEFAULT '0',
  `sign` varchar(8) NOT NULL,
  `blank` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `format` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `decimals` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `conversion_rate` decimal(13,6) NOT NULL,
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_currency`
--

INSERT INTO `ps_currency` (`id_currency`, `name`, `iso_code`, `iso_code_num`, `sign`, `blank`, `format`, `decimals`, `conversion_rate`, `deleted`, `active`) VALUES
(1, 'Złoty', 'PLN', '985', 'zł', 1, 2, 1, '1.000000', 0, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_currency_shop`
--

DROP TABLE IF EXISTS `ps_currency_shop`;
CREATE TABLE `ps_currency_shop` (
  `id_currency` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  `conversion_rate` decimal(13,6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_currency_shop`
--

INSERT INTO `ps_currency_shop` (`id_currency`, `id_shop`, `conversion_rate`) VALUES
(1, 1, '1.000000');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_customer`
--

DROP TABLE IF EXISTS `ps_customer`;
CREATE TABLE `ps_customer` (
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_shop_group` int(11) UNSIGNED NOT NULL DEFAULT 1,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT 1,
  `id_gender` int(10) UNSIGNED NOT NULL,
  `id_default_group` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `id_lang` int(10) UNSIGNED DEFAULT NULL,
  `id_risk` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `company` varchar(64) DEFAULT NULL,
  `siret` varchar(14) DEFAULT NULL,
  `ape` varchar(5) DEFAULT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(128) NOT NULL,
  `passwd` varchar(32) NOT NULL,
  `last_passwd_gen` timestamp NOT NULL DEFAULT current_timestamp(),
  `birthday` date DEFAULT NULL,
  `newsletter` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `ip_registration_newsletter` varchar(15) DEFAULT NULL,
  `newsletter_date_add` datetime DEFAULT NULL,
  `optin` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `website` varchar(128) DEFAULT NULL,
  `outstanding_allow_amount` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `show_public_prices` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `max_payment_days` int(10) UNSIGNED NOT NULL DEFAULT 60,
  `secure_key` varchar(32) NOT NULL DEFAULT '-1',
  `note` text DEFAULT NULL,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `is_guest` tinyint(1) NOT NULL DEFAULT 0,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_customer`
--

INSERT INTO `ps_customer` (`id_customer`, `id_shop_group`, `id_shop`, `id_gender`, `id_default_group`, `id_lang`, `id_risk`, `company`, `siret`, `ape`, `firstname`, `lastname`, `email`, `passwd`, `last_passwd_gen`, `birthday`, `newsletter`, `ip_registration_newsletter`, `newsletter_date_add`, `optin`, `website`, `outstanding_allow_amount`, `show_public_prices`, `max_payment_days`, `secure_key`, `note`, `active`, `is_guest`, `deleted`, `date_add`, `date_upd`) VALUES
(1, 1, 1, 1, 3, 1, 0, '', '', '', 'John', 'DOE', 'pub@prestashop.com', '80111022c23f7e7f5c971c3f1dc0f7f6', '2018-12-03 11:18:12', '1970-01-15', 1, '', '2013-12-13 08:19:15', 1, '', '0.000000', 0, 0, '9bfd021b95fa782aa8b5f875705c34b1', '', 1, 0, 0, '2018-12-03 17:18:12', '2018-12-03 17:18:12'),
(2, 1, 1, 2, 3, 1, 0, NULL, NULL, NULL, 'Asdad', 'FDSFSF', 'AFDSFDSFS@GMAIL.COM', '54ab9392fdcda4d710d2e6228132fc88', '2018-12-04 06:10:29', '2002-09-16', 0, NULL, '0000-00-00 00:00:00', 0, NULL, '0.000000', 0, 0, '86cfa6358d0176a108f7a1e958cdbeee', NULL, 1, 0, 0, '2018-12-04 12:10:29', '2018-12-04 12:10:29');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_customer_group`
--

DROP TABLE IF EXISTS `ps_customer_group`;
CREATE TABLE `ps_customer_group` (
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_customer_group`
--

INSERT INTO `ps_customer_group` (`id_customer`, `id_group`) VALUES
(1, 3),
(2, 3);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_customer_message`
--

DROP TABLE IF EXISTS `ps_customer_message`;
CREATE TABLE `ps_customer_message` (
  `id_customer_message` int(10) UNSIGNED NOT NULL,
  `id_customer_thread` int(11) DEFAULT NULL,
  `id_employee` int(10) UNSIGNED DEFAULT NULL,
  `message` mediumtext NOT NULL,
  `file_name` varchar(18) DEFAULT NULL,
  `ip_address` varchar(16) DEFAULT NULL,
  `user_agent` varchar(128) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `private` tinyint(4) NOT NULL DEFAULT 0,
  `read` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_customer_message_sync_imap`
--

DROP TABLE IF EXISTS `ps_customer_message_sync_imap`;
CREATE TABLE `ps_customer_message_sync_imap` (
  `md5_header` varbinary(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_customer_thread`
--

DROP TABLE IF EXISTS `ps_customer_thread`;
CREATE TABLE `ps_customer_thread` (
  `id_customer_thread` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT 1,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `id_contact` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED DEFAULT NULL,
  `id_order` int(10) UNSIGNED DEFAULT NULL,
  `id_product` int(10) UNSIGNED DEFAULT NULL,
  `status` enum('open','closed','pending1','pending2') NOT NULL DEFAULT 'open',
  `email` varchar(128) NOT NULL,
  `token` varchar(12) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_customization`
--

DROP TABLE IF EXISTS `ps_customization`;
CREATE TABLE `ps_customization` (
  `id_customization` int(10) UNSIGNED NOT NULL,
  `id_product_attribute` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `id_address_delivery` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `id_cart` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) NOT NULL,
  `quantity` int(10) NOT NULL,
  `quantity_refunded` int(11) NOT NULL DEFAULT 0,
  `quantity_returned` int(11) NOT NULL DEFAULT 0,
  `in_cart` tinyint(1) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_customization_field`
--

DROP TABLE IF EXISTS `ps_customization_field`;
CREATE TABLE `ps_customization_field` (
  `id_customization_field` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `type` tinyint(1) NOT NULL,
  `required` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_customization_field_lang`
--

DROP TABLE IF EXISTS `ps_customization_field_lang`;
CREATE TABLE `ps_customization_field_lang` (
  `id_customization_field` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_customized_data`
--

DROP TABLE IF EXISTS `ps_customized_data`;
CREATE TABLE `ps_customized_data` (
  `id_customization` int(10) UNSIGNED NOT NULL,
  `type` tinyint(1) NOT NULL,
  `index` int(3) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_date_range`
--

DROP TABLE IF EXISTS `ps_date_range`;
CREATE TABLE `ps_date_range` (
  `id_date_range` int(10) UNSIGNED NOT NULL,
  `time_start` datetime NOT NULL,
  `time_end` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_delivery`
--

DROP TABLE IF EXISTS `ps_delivery`;
CREATE TABLE `ps_delivery` (
  `id_delivery` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED DEFAULT NULL,
  `id_shop_group` int(10) UNSIGNED DEFAULT NULL,
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `id_range_price` int(10) UNSIGNED DEFAULT NULL,
  `id_range_weight` int(10) UNSIGNED DEFAULT NULL,
  `id_zone` int(10) UNSIGNED NOT NULL,
  `price` decimal(20,6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_delivery`
--

INSERT INTO `ps_delivery` (`id_delivery`, `id_shop`, `id_shop_group`, `id_carrier`, `id_range_price`, `id_range_weight`, `id_zone`, `price`) VALUES
(1, NULL, NULL, 2, 0, 1, 1, '5.000000'),
(2, NULL, NULL, 2, 0, 1, 2, '5.000000'),
(3, NULL, NULL, 2, 1, 0, 1, '5.000000'),
(4, NULL, NULL, 2, 1, 0, 2, '5.000000');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_employee`
--

DROP TABLE IF EXISTS `ps_employee`;
CREATE TABLE `ps_employee` (
  `id_employee` int(10) UNSIGNED NOT NULL,
  `id_profile` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `lastname` varchar(32) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `email` varchar(128) NOT NULL,
  `passwd` varchar(32) NOT NULL,
  `last_passwd_gen` timestamp NOT NULL DEFAULT current_timestamp(),
  `stats_date_from` date DEFAULT NULL,
  `stats_date_to` date DEFAULT NULL,
  `stats_compare_from` date DEFAULT NULL,
  `stats_compare_to` date DEFAULT NULL,
  `stats_compare_option` int(1) UNSIGNED NOT NULL DEFAULT 1,
  `preselect_date_range` varchar(32) DEFAULT NULL,
  `bo_color` varchar(32) DEFAULT NULL,
  `bo_theme` varchar(32) DEFAULT NULL,
  `bo_css` varchar(64) DEFAULT NULL,
  `default_tab` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `bo_width` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `bo_menu` tinyint(1) NOT NULL DEFAULT 1,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `optin` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `id_last_order` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `id_last_customer_message` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `id_last_customer` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `last_connection_date` date DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_employee`
--

INSERT INTO `ps_employee` (`id_employee`, `id_profile`, `id_lang`, `lastname`, `firstname`, `email`, `passwd`, `last_passwd_gen`, `stats_date_from`, `stats_date_to`, `stats_compare_from`, `stats_compare_to`, `stats_compare_option`, `preselect_date_range`, `bo_color`, `bo_theme`, `bo_css`, `default_tab`, `bo_width`, `bo_menu`, `active`, `optin`, `id_last_order`, `id_last_customer_message`, `id_last_customer`, `last_connection_date`) VALUES
(1, 1, 1, 'F', 'Dawid', 'felo.dawid@gmail.com', '9e0e53cb26e6bab5a1cf8c47599cf7dc', '2018-12-03 11:18:09', '2018-11-03', '2018-12-03', '0000-00-00', '0000-00-00', 1, NULL, NULL, 'default', 'admin-theme.css', 1, 0, 1, 1, 1, 0, 0, 0, '2018-12-03'),
(2, 1, 1, 'Testowy', 'Admin', 'o1202185@nwytg.net', '10a56d97a5ccb19ee206a2e960d7c0a6', '2018-12-03 16:14:18', '2018-11-03', '2018-12-03', '0000-00-00', '0000-00-00', 1, NULL, NULL, 'default', 'admin-theme.css', 1, 0, 1, 1, 1, 5, 0, 1, '2018-12-04');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_employee_shop`
--

DROP TABLE IF EXISTS `ps_employee_shop`;
CREATE TABLE `ps_employee_shop` (
  `id_employee` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_employee_shop`
--

INSERT INTO `ps_employee_shop` (`id_employee`, `id_shop`) VALUES
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_feature`
--

DROP TABLE IF EXISTS `ps_feature`;
CREATE TABLE `ps_feature` (
  `id_feature` int(10) UNSIGNED NOT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_feature_lang`
--

DROP TABLE IF EXISTS `ps_feature_lang`;
CREATE TABLE `ps_feature_lang` (
  `id_feature` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_feature_product`
--

DROP TABLE IF EXISTS `ps_feature_product`;
CREATE TABLE `ps_feature_product` (
  `id_feature` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_feature_value` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_feature_shop`
--

DROP TABLE IF EXISTS `ps_feature_shop`;
CREATE TABLE `ps_feature_shop` (
  `id_feature` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_feature_value`
--

DROP TABLE IF EXISTS `ps_feature_value`;
CREATE TABLE `ps_feature_value` (
  `id_feature_value` int(10) UNSIGNED NOT NULL,
  `id_feature` int(10) UNSIGNED NOT NULL,
  `custom` tinyint(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_feature_value_lang`
--

DROP TABLE IF EXISTS `ps_feature_value_lang`;
CREATE TABLE `ps_feature_value_lang` (
  `id_feature_value` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_ganalytics`
--

DROP TABLE IF EXISTS `ps_ganalytics`;
CREATE TABLE `ps_ganalytics` (
  `id_google_analytics` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `id_customer` int(10) NOT NULL,
  `id_shop` int(11) NOT NULL,
  `sent` tinyint(1) DEFAULT NULL,
  `date_add` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_ganalytics`
--

INSERT INTO `ps_ganalytics` (`id_google_analytics`, `id_order`, `id_customer`, `id_shop`, `sent`, `date_add`) VALUES
(1, 6, 0, 1, 1, '2018-12-04 11:11:54');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_gender`
--

DROP TABLE IF EXISTS `ps_gender`;
CREATE TABLE `ps_gender` (
  `id_gender` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_gender`
--

INSERT INTO `ps_gender` (`id_gender`, `type`) VALUES
(1, 0),
(2, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_gender_lang`
--

DROP TABLE IF EXISTS `ps_gender_lang`;
CREATE TABLE `ps_gender_lang` (
  `id_gender` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_gender_lang`
--

INSERT INTO `ps_gender_lang` (`id_gender`, `id_lang`, `name`) VALUES
(1, 1, 'Pan'),
(2, 1, 'Pani');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_group`
--

DROP TABLE IF EXISTS `ps_group`;
CREATE TABLE `ps_group` (
  `id_group` int(10) UNSIGNED NOT NULL,
  `reduction` decimal(17,2) NOT NULL DEFAULT 0.00,
  `price_display_method` tinyint(4) NOT NULL DEFAULT 0,
  `show_prices` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_group`
--

INSERT INTO `ps_group` (`id_group`, `reduction`, `price_display_method`, `show_prices`, `date_add`, `date_upd`) VALUES
(1, '0.00', 0, 1, '2018-12-03 17:18:00', '2018-12-03 17:18:00'),
(2, '0.00', 0, 1, '2018-12-03 17:18:00', '2018-12-03 17:18:00'),
(3, '0.00', 0, 1, '2018-12-03 17:18:00', '2018-12-03 17:18:00');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_group_lang`
--

DROP TABLE IF EXISTS `ps_group_lang`;
CREATE TABLE `ps_group_lang` (
  `id_group` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_group_lang`
--

INSERT INTO `ps_group_lang` (`id_group`, `id_lang`, `name`) VALUES
(1, 1, 'OdwiedzajÄcy'),
(2, 1, 'GoĹÄ'),
(3, 1, 'Klient');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_group_reduction`
--

DROP TABLE IF EXISTS `ps_group_reduction`;
CREATE TABLE `ps_group_reduction` (
  `id_group_reduction` mediumint(8) UNSIGNED NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL,
  `id_category` int(10) UNSIGNED NOT NULL,
  `reduction` decimal(4,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_group_shop`
--

DROP TABLE IF EXISTS `ps_group_shop`;
CREATE TABLE `ps_group_shop` (
  `id_group` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_group_shop`
--

INSERT INTO `ps_group_shop` (`id_group`, `id_shop`) VALUES
(1, 1),
(2, 1),
(3, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_guest`
--

DROP TABLE IF EXISTS `ps_guest`;
CREATE TABLE `ps_guest` (
  `id_guest` int(10) UNSIGNED NOT NULL,
  `id_operating_system` int(10) UNSIGNED DEFAULT NULL,
  `id_web_browser` int(10) UNSIGNED DEFAULT NULL,
  `id_customer` int(10) UNSIGNED DEFAULT NULL,
  `javascript` tinyint(1) DEFAULT 0,
  `screen_resolution_x` smallint(5) UNSIGNED DEFAULT NULL,
  `screen_resolution_y` smallint(5) UNSIGNED DEFAULT NULL,
  `screen_color` tinyint(3) UNSIGNED DEFAULT NULL,
  `sun_java` tinyint(1) DEFAULT NULL,
  `adobe_flash` tinyint(1) DEFAULT NULL,
  `adobe_director` tinyint(1) DEFAULT NULL,
  `apple_quicktime` tinyint(1) DEFAULT NULL,
  `real_player` tinyint(1) DEFAULT NULL,
  `windows_media` tinyint(1) DEFAULT NULL,
  `accept_language` varchar(8) DEFAULT NULL,
  `mobile_theme` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_guest`
--

INSERT INTO `ps_guest` (`id_guest`, `id_operating_system`, `id_web_browser`, `id_customer`, `javascript`, `screen_resolution_x`, `screen_resolution_y`, `screen_color`, `sun_java`, `adobe_flash`, `adobe_director`, `apple_quicktime`, `real_player`, `windows_media`, `accept_language`, `mobile_theme`) VALUES
(1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(2, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'pl', 0),
(3, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'pl', 0),
(4, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'pl', 0),
(5, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'pl', 0),
(6, 0, 3, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'pl', 0),
(7, 0, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0),
(8, 0, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'pl', 0),
(9, 0, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'pl', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_homeslider`
--

DROP TABLE IF EXISTS `ps_homeslider`;
CREATE TABLE `ps_homeslider` (
  `id_homeslider_slides` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_homeslider`
--

INSERT INTO `ps_homeslider` (`id_homeslider_slides`, `id_shop`) VALUES
(5, 1),
(7, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_homeslider_slides`
--

DROP TABLE IF EXISTS `ps_homeslider_slides`;
CREATE TABLE `ps_homeslider_slides` (
  `id_homeslider_slides` int(10) UNSIGNED NOT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_homeslider_slides`
--

INSERT INTO `ps_homeslider_slides` (`id_homeslider_slides`, `position`, `active`) VALUES
(5, 0, 1),
(7, 0, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_homeslider_slides_lang`
--

DROP TABLE IF EXISTS `ps_homeslider_slides_lang`;
CREATE TABLE `ps_homeslider_slides_lang` (
  `id_homeslider_slides` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `legend` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_homeslider_slides_lang`
--

INSERT INTO `ps_homeslider_slides_lang` (`id_homeslider_slides`, `id_lang`, `title`, `description`, `legend`, `url`, `image`) VALUES
(5, 1, 'DLA MIŁOŚNIKÓW', '<h1 style=\"text-align:left;\"><span style=\"color:#ffffff;\">DLA MIŁOŚNIKÓW</span></h1>\n<h2><span style=\"color:#ffffff;\">Szybkiej jazdy</span></h2>\n<p></p>\n<h4><span style=\"color:#ffffff;\">Oferujemy szeroki wachlarz</span></h4>\n<h4><span style=\"color:#ffffff;\"> wyboru samochodów sportowych</span></h4>\n<h4><span style=\"color:#ffffff;\"> gwarantujących wysoką wydajność i moc w</span></h4>\n<h4><span style=\"color:#ffffff;\"> bardzo korzystnych przedziałach cenowych.</span></h4>', 'DLA MIŁOŚNIKÓW', '172.20.83.60/index.php?id_category=15&controller=category', '6eef479e598369f7b56ab777638e71dfabf5ad8a_3.jpg'),
(7, 1, 'PIERWSZE AUTO', '<h1><span style=\"color:#ffffff;\">PIERWSZE AUTO</span></h1>\n<h4><br /><span style=\"color:#ffffff;\">Szeroki wybór aut używanych</span><br /><span style=\"color:#ffffff;\">Bogata oferta niezawodnych pojazdów w bardzo</span><br /><span style=\"color:#ffffff;\">atrakcyjnych cenach, w sam raz dla młodych,</span><br /><span style=\"color:#ffffff;\">mniej doświadczonych kierowców</span><br /><br /></h4>', 'PIERWSZE AUTO', 'http://172.20.83.60/index.php?id_category=19&controller=category', '2eb28c37187a8d329a25e566937622f77a0b7e46_1.jpg');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_hook`
--

DROP TABLE IF EXISTS `ps_hook`;
CREATE TABLE `ps_hook` (
  `id_hook` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` text DEFAULT NULL,
  `position` tinyint(1) NOT NULL DEFAULT 1,
  `live_edit` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_hook`
--

INSERT INTO `ps_hook` (`id_hook`, `name`, `title`, `description`, `position`, `live_edit`) VALUES
(1, 'displayPayment', 'Payment', 'This hook displays new elements on the payment page', 1, 1),
(2, 'actionValidateOrder', 'New orders', '', 1, 0),
(3, 'displayMaintenance', 'Maintenance Page', 'This hook displays new elements on the maintenance page', 1, 0),
(4, 'actionPaymentConfirmation', 'Payment confirmation', 'This hook displays new elements after the payment is validated', 1, 0),
(5, 'displayPaymentReturn', 'Payment return', '', 1, 0),
(6, 'actionUpdateQuantity', 'Quantity update', 'Quantity is updated only when a customer effectively places their order', 1, 0),
(7, 'displayRightColumn', 'Right column blocks', 'This hook displays new elements in the right-hand column', 1, 1),
(8, 'displayLeftColumn', 'Left column blocks', 'This hook displays new elements in the left-hand column', 1, 1),
(9, 'displayHome', 'Homepage content', 'This hook displays new elements on the homepage', 1, 1),
(10, 'Header', 'Pages html head section', 'This hook adds additional elements in the head section of your pages (head section of html)', 1, 0),
(11, 'actionCartSave', 'Cart creation and update', 'This hook is displayed when a product is added to the cart or if the cart\'s content is modified', 1, 0),
(12, 'actionAuthentication', 'Successful customer authentication', 'This hook is displayed after a customer successfully signs in', 1, 0),
(13, 'actionProductAdd', 'Product creation', 'This hook is displayed after a product is created', 1, 0),
(14, 'actionProductUpdate', 'Product update', 'This hook is displayed after a product has been updated', 1, 0),
(15, 'displayTop', 'Top of pages', 'This hook displays additional elements at the top of your pages', 1, 0),
(16, 'displayRightColumnProduct', 'New elements on the product page (right column)', 'This hook displays new elements in the right-hand column of the product page', 1, 0),
(17, 'actionProductDelete', 'Product deletion', 'This hook is called when a product is deleted', 1, 0),
(18, 'displayFooterProduct', 'Product footer', 'This hook adds new blocks under the product\'s description', 1, 1),
(19, 'displayInvoice', 'Invoice', 'This hook displays new blocks on the invoice (order)', 1, 0),
(20, 'actionOrderStatusUpdate', 'Order status update - Event', 'This hook launches modules when the status of an order changes.', 1, 0),
(21, 'displayAdminOrder', 'Display new elements in the Back Office, tab AdminOrder', 'This hook launches modules when the AdminOrder tab is displayed in the Back Office', 1, 0),
(22, 'displayAdminOrderTabOrder', 'Display new elements in Back Office, AdminOrder, panel Order', 'This hook launches modules when the AdminOrder tab is displayed in the Back Office and extends / override Order panel tabs', 1, 0),
(23, 'displayAdminOrderTabShip', 'Display new elements in Back Office, AdminOrder, panel Shipping', 'This hook launches modules when the AdminOrder tab is displayed in the Back Office and extends / override Shipping panel tabs', 1, 0),
(24, 'displayAdminOrderContentOrder', 'Display new elements in Back Office, AdminOrder, panel Order', 'This hook launches modules when the AdminOrder tab is displayed in the Back Office and extends / override Order panel content', 1, 0),
(25, 'displayAdminOrderContentShip', 'Display new elements in Back Office, AdminOrder, panel Shipping', 'This hook launches modules when the AdminOrder tab is displayed in the Back Office and extends / override Shipping panel content', 1, 0),
(26, 'displayFooter', 'Footer', 'This hook displays new blocks in the footer', 1, 0),
(27, 'displayPDFInvoice', 'PDF Invoice', 'This hook allows you to display additional information on PDF invoices', 1, 0),
(28, 'displayInvoiceLegalFreeText', 'PDF Invoice - Legal Free Text', 'This hook allows you to modify the legal free text on PDF invoices', 1, 0),
(29, 'displayAdminCustomers', 'Display new elements in the Back Office, tab AdminCustomers', 'This hook launches modules when the AdminCustomers tab is displayed in the Back Office', 1, 0),
(30, 'displayOrderConfirmation', 'Order confirmation page', 'This hook is called within an order\'s confirmation page', 1, 0),
(31, 'actionCustomerAccountAdd', 'Successful customer account creation', 'This hook is called when a new customer creates an account successfully', 1, 0),
(32, 'displayCustomerAccount', 'Customer account displayed in Front Office', 'This hook displays new elements on the customer account page', 1, 0),
(33, 'displayCustomerIdentityForm', 'Customer identity form displayed in Front Office', 'This hook displays new elements on the form to update a customer identity', 1, 0),
(34, 'actionOrderSlipAdd', 'Order slip creation', 'This hook is called when a new credit slip is added regarding client order', 1, 0),
(35, 'displayProductTab', 'Tabs on product page', 'This hook is called on the product page\'s tab', 1, 0),
(36, 'displayProductTabContent', 'Tabs content on the product page', 'This hook is called on the product page\'s tab', 1, 0),
(37, 'displayShoppingCartFooter', 'Shopping cart footer', 'This hook displays some specific information on the shopping cart\'s page', 1, 0),
(38, 'displayCustomerAccountForm', 'Customer account creation form', 'This hook displays some information on the form to create a customer account', 1, 0),
(39, 'displayAdminStatsModules', 'Stats - Modules', '', 1, 0),
(40, 'displayAdminStatsGraphEngine', 'Graph engines', '', 1, 0),
(41, 'actionOrderReturn', 'Returned product', 'This hook is displayed when a customer returns a product ', 1, 0),
(42, 'displayProductButtons', 'Product page actions', 'This hook adds new action buttons on the product page', 1, 0),
(43, 'displayBackOfficeHome', 'Administration panel homepage', 'This hook is displayed on the admin panel\'s homepage', 1, 0),
(44, 'displayAdminStatsGridEngine', 'Grid engines', '', 1, 0),
(45, 'actionWatermark', 'Watermark', '', 1, 0),
(46, 'actionProductCancel', 'Product cancelled', 'This hook is called when you cancel a product in an order', 1, 0),
(47, 'displayLeftColumnProduct', 'New elements on the product page (left column)', 'This hook displays new elements in the left-hand column of the product page', 1, 0),
(48, 'actionProductOutOfStock', 'Out-of-stock product', 'This hook displays new action buttons if a product is out of stock', 1, 0),
(49, 'actionProductAttributeUpdate', 'Product attribute update', 'This hook is displayed when a product\'s attribute is updated', 1, 0),
(50, 'displayCarrierList', 'Extra carrier (module mode)', '', 1, 0),
(51, 'displayShoppingCart', 'Shopping cart - Additional button', 'This hook displays new action buttons within the shopping cart', 1, 0),
(52, 'actionSearch', 'Search', '', 1, 0),
(53, 'displayBeforePayment', 'Redirect during the order process', 'This hook redirects the user to the module instead of displaying payment modules', 1, 0),
(54, 'actionCarrierUpdate', 'Carrier Update', 'This hook is called when a carrier is updated', 1, 0),
(55, 'actionOrderStatusPostUpdate', 'Post update of order status', '', 1, 0),
(56, 'displayCustomerAccountFormTop', 'Block above the form for create an account', 'This hook is displayed above the customer\'s account creation form', 1, 0),
(57, 'displayBackOfficeHeader', 'Administration panel header', 'This hook is displayed in the header of the admin panel', 1, 0),
(58, 'displayBackOfficeTop', 'Administration panel hover the tabs', 'This hook is displayed on the roll hover of the tabs within the admin panel', 1, 0),
(59, 'displayBackOfficeFooter', 'Administration panel footer', 'This hook is displayed within the admin panel\'s footer', 1, 0),
(60, 'actionProductAttributeDelete', 'Product attribute deletion', 'This hook is displayed when a product\'s attribute is deleted', 1, 0),
(61, 'actionCarrierProcess', 'Carrier process', '', 1, 0),
(62, 'actionOrderDetail', 'Order detail', 'This hook is used to set the follow-up in Smarty when an order\'s detail is called', 1, 0),
(63, 'displayBeforeCarrier', 'Before carriers list', 'This hook is displayed before the carrier list in Front Office', 1, 0),
(64, 'displayOrderDetail', 'Order detail', 'This hook is displayed within the order\'s details in Front Office', 1, 0),
(65, 'actionPaymentCCAdd', 'Payment CC added', '', 1, 0),
(66, 'displayProductComparison', 'Extra product comparison', '', 1, 0),
(67, 'actionCategoryAdd', 'Category creation', 'This hook is displayed when a category is created', 1, 0),
(68, 'actionCategoryUpdate', 'Category modification', 'This hook is displayed when a category is modified', 1, 0),
(69, 'actionCategoryDelete', 'Category deletion', 'This hook is displayed when a category is deleted', 1, 0),
(70, 'actionBeforeAuthentication', 'Before authentication', 'This hook is displayed before the customer\'s authentication', 1, 0),
(71, 'displayPaymentTop', 'Top of payment page', 'This hook is displayed at the top of the payment page', 1, 0),
(72, 'actionHtaccessCreate', 'After htaccess creation', 'This hook is displayed after the htaccess creation', 1, 0),
(73, 'actionAdminMetaSave', 'After saving the configuration in AdminMeta', 'This hook is displayed after saving the configuration in AdminMeta', 1, 0),
(74, 'displayAttributeGroupForm', 'Add fields to the form \'attribute group\'', 'This hook adds fields to the form \'attribute group\'', 1, 0),
(75, 'actionAttributeGroupSave', 'Saving an attribute group', 'This hook is called while saving an attributes group', 1, 0),
(76, 'actionAttributeGroupDelete', 'Deleting attribute group', 'This hook is called while deleting an attributes  group', 1, 0),
(77, 'displayFeatureForm', 'Add fields to the form \'feature\'', 'This hook adds fields to the form \'feature\'', 1, 0),
(78, 'actionFeatureSave', 'Saving attributes\' features', 'This hook is called while saving an attributes features', 1, 0),
(79, 'actionFeatureDelete', 'Deleting attributes\' features', 'This hook is called while deleting an attributes features', 1, 0),
(80, 'actionProductSave', 'Saving products', 'This hook is called while saving products', 1, 0),
(81, 'actionProductListOverride', 'Assign a products list to a category', 'This hook assigns a products list to a category', 1, 0),
(82, 'displayAttributeGroupPostProcess', 'On post-process in admin attribute group', 'This hook is called on post-process in admin attribute group', 1, 0),
(83, 'displayFeaturePostProcess', 'On post-process in admin feature', 'This hook is called on post-process in admin feature', 1, 0),
(84, 'displayFeatureValueForm', 'Add fields to the form \'feature value\'', 'This hook adds fields to the form \'feature value\'', 1, 0),
(85, 'displayFeatureValuePostProcess', 'On post-process in admin feature value', 'This hook is called on post-process in admin feature value', 1, 0),
(86, 'actionFeatureValueDelete', 'Deleting attributes\' features\' values', 'This hook is called while deleting an attributes features value', 1, 0),
(87, 'actionFeatureValueSave', 'Saving an attributes features value', 'This hook is called while saving an attributes features value', 1, 0),
(88, 'displayAttributeForm', 'Add fields to the form \'attribute value\'', 'This hook adds fields to the form \'attribute value\'', 1, 0),
(89, 'actionAttributePostProcess', 'On post-process in admin feature value', 'This hook is called on post-process in admin feature value', 1, 0),
(90, 'actionAttributeDelete', 'Deleting an attributes features value', 'This hook is called while deleting an attributes features value', 1, 0),
(91, 'actionAttributeSave', 'Saving an attributes features value', 'This hook is called while saving an attributes features value', 1, 0),
(92, 'actionTaxManager', 'Tax Manager Factory', '', 1, 0),
(93, 'displayMyAccountBlock', 'My account block', 'This hook displays extra information within the \'my account\' block\"', 1, 0),
(94, 'actionModuleInstallBefore', 'actionModuleInstallBefore', '', 1, 0),
(95, 'actionModuleInstallAfter', 'actionModuleInstallAfter', '', 1, 0),
(96, 'displayHomeTab', 'Home Page Tabs', 'This hook displays new elements on the homepage tabs', 1, 1),
(97, 'displayHomeTabContent', 'Home Page Tabs Content', 'This hook displays new elements on the homepage tabs content', 1, 1),
(98, 'displayTopColumn', 'Top column blocks', 'This hook displays new elements in the top of columns', 1, 1),
(99, 'displayBackOfficeCategory', 'Display new elements in the Back Office, tab AdminCategories', 'This hook launches modules when the AdminCategories tab is displayed in the Back Office', 1, 0),
(100, 'displayProductListFunctionalButtons', 'Display new elements in the Front Office, products list', 'This hook launches modules when the products list is displayed in the Front Office', 1, 0),
(101, 'displayNav', 'Navigation', '', 1, 1),
(102, 'displayOverrideTemplate', 'Change the default template of current controller', '', 1, 0),
(103, 'actionAdminLoginControllerSetMedia', 'Set media on admin login page header', 'This hook is called after adding media to admin login page header', 1, 0),
(104, 'actionOrderEdited', 'Order edited', 'This hook is called when an order is edited.', 1, 0),
(105, 'actionEmailAddBeforeContent', 'Add extra content before mail content', 'This hook is called just before fetching mail template', 1, 0),
(106, 'actionEmailAddAfterContent', 'Add extra content after mail content', 'This hook is called just after fetching mail template', 1, 0),
(107, 'displayCartExtraProductActions', 'Extra buttons in shopping cart', 'This hook adds extra buttons to the product lines, in the shopping cart', 1, 0),
(108, 'actionObjectProductUpdateAfter', 'actionObjectProductUpdateAfter', '', 0, 0),
(109, 'actionObjectProductDeleteAfter', 'actionObjectProductDeleteAfter', '', 0, 0),
(110, 'displayCompareExtraInformation', 'displayCompareExtraInformation', '', 1, 1),
(111, 'displaySocialSharing', 'displaySocialSharing', '', 1, 1),
(112, 'displayBanner', 'displayBanner', '', 1, 1),
(113, 'actionObjectLanguageAddAfter', 'actionObjectLanguageAddAfter', '', 0, 0),
(114, 'displayPaymentEU', 'displayPaymentEU', '', 1, 1),
(115, 'actionCartListOverride', 'actionCartListOverride', '', 0, 0),
(116, 'actionAdminMetaControllerUpdate_optionsBefore', 'actionAdminMetaControllerUpdate_optionsBefore', '', 0, 0),
(117, 'actionAdminLanguagesControllerStatusBefore', 'actionAdminLanguagesControllerStatusBefore', '', 0, 0),
(118, 'actionObjectCmsUpdateAfter', 'actionObjectCmsUpdateAfter', '', 0, 0),
(119, 'actionObjectCmsDeleteAfter', 'actionObjectCmsDeleteAfter', '', 0, 0),
(120, 'actionShopDataDuplication', 'actionShopDataDuplication', '', 0, 0),
(121, 'actionAdminStoresControllerUpdate_optionsAfter', 'actionAdminStoresControllerUpdate_optionsAfter', '', 0, 0),
(122, 'actionObjectManufacturerDeleteAfter', 'actionObjectManufacturerDeleteAfter', '', 0, 0),
(123, 'actionObjectManufacturerAddAfter', 'actionObjectManufacturerAddAfter', '', 0, 0),
(124, 'actionObjectManufacturerUpdateAfter', 'actionObjectManufacturerUpdateAfter', '', 0, 0),
(126, 'actionModuleRegisterHookAfter', 'actionModuleRegisterHookAfter', '', 0, 0),
(127, 'actionModuleUnRegisterHookAfter', 'actionModuleUnRegisterHookAfter', '', 0, 0),
(128, 'displayMyAccountBlockfooter', 'My account block', 'Display extra informations inside the \"my account\" block', 1, 0),
(129, 'registerGDPRConsent', 'registerGDPRConsent', '', 0, 0),
(130, 'actionExportGDPRData', 'actionExportGDPRData', '', 0, 0),
(131, 'actionDeleteGDPRCustomer', 'actionDeleteGDPRCustomer', '', 0, 0),
(132, 'displayMobileTopSiteMap', 'displayMobileTopSiteMap', '', 1, 1),
(133, 'displaySearch', 'displaySearch', '', 1, 1),
(134, 'actionObjectSupplierDeleteAfter', 'actionObjectSupplierDeleteAfter', '', 0, 0),
(135, 'actionObjectSupplierAddAfter', 'actionObjectSupplierAddAfter', '', 0, 0),
(136, 'actionObjectSupplierUpdateAfter', 'actionObjectSupplierUpdateAfter', '', 0, 0),
(137, 'actionObjectCategoryUpdateAfter', 'actionObjectCategoryUpdateAfter', '', 0, 0),
(138, 'actionObjectCategoryDeleteAfter', 'actionObjectCategoryDeleteAfter', '', 0, 0),
(139, 'actionObjectCategoryAddAfter', 'actionObjectCategoryAddAfter', '', 0, 0),
(140, 'actionObjectCmsAddAfter', 'actionObjectCmsAddAfter', '', 0, 0),
(141, 'actionObjectProductAddAfter', 'actionObjectProductAddAfter', '', 0, 0),
(142, 'dashboardZoneOne', 'dashboardZoneOne', '', 0, 0),
(143, 'dashboardData', 'dashboardData', '', 0, 0),
(144, 'actionObjectOrderAddAfter', 'actionObjectOrderAddAfter', '', 0, 0),
(145, 'actionObjectCustomerAddAfter', 'actionObjectCustomerAddAfter', '', 0, 0),
(146, 'actionObjectCustomerMessageAddAfter', 'actionObjectCustomerMessageAddAfter', '', 0, 0),
(147, 'actionObjectCustomerThreadAddAfter', 'actionObjectCustomerThreadAddAfter', '', 0, 0),
(148, 'actionObjectOrderReturnAddAfter', 'actionObjectOrderReturnAddAfter', '', 0, 0),
(149, 'actionAdminControllerSetMedia', 'actionAdminControllerSetMedia', '', 0, 0),
(150, 'dashboardZoneTwo', 'dashboardZoneTwo', '', 0, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_hook_alias`
--

DROP TABLE IF EXISTS `ps_hook_alias`;
CREATE TABLE `ps_hook_alias` (
  `id_hook_alias` int(10) UNSIGNED NOT NULL,
  `alias` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_hook_alias`
--

INSERT INTO `ps_hook_alias` (`id_hook_alias`, `alias`, `name`) VALUES
(1, 'payment', 'displayPayment'),
(2, 'newOrder', 'actionValidateOrder'),
(3, 'paymentConfirm', 'actionPaymentConfirmation'),
(4, 'paymentReturn', 'displayPaymentReturn'),
(5, 'updateQuantity', 'actionUpdateQuantity'),
(6, 'rightColumn', 'displayRightColumn'),
(7, 'leftColumn', 'displayLeftColumn'),
(8, 'home', 'displayHome'),
(9, 'displayHeader', 'Header'),
(10, 'cart', 'actionCartSave'),
(11, 'authentication', 'actionAuthentication'),
(12, 'addproduct', 'actionProductAdd'),
(13, 'updateproduct', 'actionProductUpdate'),
(14, 'top', 'displayTop'),
(15, 'extraRight', 'displayRightColumnProduct'),
(16, 'deleteproduct', 'actionProductDelete'),
(17, 'productfooter', 'displayFooterProduct'),
(18, 'invoice', 'displayInvoice'),
(19, 'updateOrderStatus', 'actionOrderStatusUpdate'),
(20, 'adminOrder', 'displayAdminOrder'),
(21, 'footer', 'displayFooter'),
(22, 'PDFInvoice', 'displayPDFInvoice'),
(23, 'adminCustomers', 'displayAdminCustomers'),
(24, 'orderConfirmation', 'displayOrderConfirmation'),
(25, 'createAccount', 'actionCustomerAccountAdd'),
(26, 'customerAccount', 'displayCustomerAccount'),
(27, 'orderSlip', 'actionOrderSlipAdd'),
(28, 'productTab', 'displayProductTab'),
(29, 'productTabContent', 'displayProductTabContent'),
(30, 'shoppingCart', 'displayShoppingCartFooter'),
(31, 'createAccountForm', 'displayCustomerAccountForm'),
(32, 'AdminStatsModules', 'displayAdminStatsModules'),
(33, 'GraphEngine', 'displayAdminStatsGraphEngine'),
(34, 'orderReturn', 'actionOrderReturn'),
(35, 'productActions', 'displayProductButtons'),
(36, 'backOfficeHome', 'displayBackOfficeHome'),
(37, 'GridEngine', 'displayAdminStatsGridEngine'),
(38, 'watermark', 'actionWatermark'),
(39, 'cancelProduct', 'actionProductCancel'),
(40, 'extraLeft', 'displayLeftColumnProduct'),
(41, 'productOutOfStock', 'actionProductOutOfStock'),
(42, 'updateProductAttribute', 'actionProductAttributeUpdate'),
(43, 'extraCarrier', 'displayCarrierList'),
(44, 'shoppingCartExtra', 'displayShoppingCart'),
(45, 'search', 'actionSearch'),
(46, 'backBeforePayment', 'displayBeforePayment'),
(47, 'updateCarrier', 'actionCarrierUpdate'),
(48, 'postUpdateOrderStatus', 'actionOrderStatusPostUpdate'),
(49, 'createAccountTop', 'displayCustomerAccountFormTop'),
(50, 'backOfficeHeader', 'displayBackOfficeHeader'),
(51, 'backOfficeTop', 'displayBackOfficeTop'),
(52, 'backOfficeFooter', 'displayBackOfficeFooter'),
(53, 'deleteProductAttribute', 'actionProductAttributeDelete'),
(54, 'processCarrier', 'actionCarrierProcess'),
(55, 'orderDetail', 'actionOrderDetail'),
(56, 'beforeCarrier', 'displayBeforeCarrier'),
(57, 'orderDetailDisplayed', 'displayOrderDetail'),
(58, 'paymentCCAdded', 'actionPaymentCCAdd'),
(59, 'extraProductComparison', 'displayProductComparison'),
(60, 'categoryAddition', 'actionCategoryAdd'),
(61, 'categoryUpdate', 'actionCategoryUpdate'),
(62, 'categoryDeletion', 'actionCategoryDelete'),
(63, 'beforeAuthentication', 'actionBeforeAuthentication'),
(64, 'paymentTop', 'displayPaymentTop'),
(65, 'afterCreateHtaccess', 'actionHtaccessCreate'),
(66, 'afterSaveAdminMeta', 'actionAdminMetaSave'),
(67, 'attributeGroupForm', 'displayAttributeGroupForm'),
(68, 'afterSaveAttributeGroup', 'actionAttributeGroupSave'),
(69, 'afterDeleteAttributeGroup', 'actionAttributeGroupDelete'),
(70, 'featureForm', 'displayFeatureForm'),
(71, 'afterSaveFeature', 'actionFeatureSave'),
(72, 'afterDeleteFeature', 'actionFeatureDelete'),
(73, 'afterSaveProduct', 'actionProductSave'),
(74, 'productListAssign', 'actionProductListOverride'),
(75, 'postProcessAttributeGroup', 'displayAttributeGroupPostProcess'),
(76, 'postProcessFeature', 'displayFeaturePostProcess'),
(77, 'featureValueForm', 'displayFeatureValueForm'),
(78, 'postProcessFeatureValue', 'displayFeatureValuePostProcess'),
(79, 'afterDeleteFeatureValue', 'actionFeatureValueDelete'),
(80, 'afterSaveFeatureValue', 'actionFeatureValueSave'),
(81, 'attributeForm', 'displayAttributeForm'),
(82, 'postProcessAttribute', 'actionAttributePostProcess'),
(83, 'afterDeleteAttribute', 'actionAttributeDelete'),
(84, 'afterSaveAttribute', 'actionAttributeSave'),
(85, 'taxManager', 'actionTaxManager'),
(86, 'myAccountBlock', 'displayMyAccountBlock');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_hook_module`
--

DROP TABLE IF EXISTS `ps_hook_module`;
CREATE TABLE `ps_hook_module` (
  `id_module` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT 1,
  `id_hook` int(10) UNSIGNED NOT NULL,
  `position` tinyint(2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_hook_module`
--

INSERT INTO `ps_hook_module` (`id_module`, `id_shop`, `id_hook`, `position`) VALUES
(1, 1, 10, 1),
(1, 1, 16, 1),
(1, 1, 108, 1),
(1, 1, 109, 1),
(1, 1, 110, 1),
(1, 1, 111, 1),
(2, 1, 112, 1),
(2, 1, 113, 1),
(3, 1, 1, 1),
(3, 1, 5, 1),
(3, 1, 114, 1),
(4, 1, 8, 1),
(4, 1, 13, 1),
(4, 1, 14, 1),
(4, 1, 17, 1),
(4, 1, 55, 1),
(5, 1, 115, 1),
(7, 1, 67, 1),
(7, 1, 68, 1),
(7, 1, 69, 1),
(7, 1, 99, 1),
(7, 1, 116, 1),
(7, 1, 117, 1),
(11, 1, 74, 1),
(11, 1, 75, 1),
(11, 1, 76, 1),
(11, 1, 77, 1),
(11, 1, 78, 1),
(11, 1, 79, 1),
(11, 1, 80, 1),
(11, 1, 81, 1),
(11, 1, 82, 1),
(11, 1, 83, 1),
(11, 1, 84, 1),
(11, 1, 85, 1),
(11, 1, 86, 1),
(11, 1, 87, 1),
(11, 1, 88, 1),
(11, 1, 89, 1),
(11, 1, 90, 1),
(11, 1, 91, 1),
(12, 1, 118, 1),
(12, 1, 119, 1),
(12, 1, 120, 1),
(12, 1, 121, 1),
(16, 1, 122, 1),
(16, 1, 123, 1),
(16, 1, 124, 1),
(17, 1, 126, 1),
(17, 1, 127, 1),
(19, 1, 96, 1),
(19, 1, 97, 1),
(20, 1, 26, 1),
(20, 1, 31, 1),
(20, 1, 129, 1),
(20, 1, 130, 1),
(20, 1, 131, 1),
(22, 1, 15, 1),
(22, 1, 132, 1),
(22, 1, 133, 1),
(25, 1, 134, 1),
(25, 1, 135, 1),
(25, 1, 136, 1),
(27, 1, 137, 1),
(27, 1, 138, 1),
(27, 1, 139, 1),
(27, 1, 140, 1),
(27, 1, 141, 1),
(28, 1, 101, 1),
(31, 1, 143, 1),
(31, 1, 144, 1),
(31, 1, 145, 1),
(31, 1, 146, 1),
(31, 1, 147, 1),
(31, 1, 148, 1),
(31, 1, 149, 1),
(32, 1, 150, 1),
(34, 1, 52, 1),
(35, 1, 40, 1),
(36, 1, 44, 1),
(37, 1, 98, 1),
(40, 1, 39, 1),
(50, 1, 12, 1),
(63, 1, 9, 1),
(63, 1, 57, 1),
(66, 1, 142, 1),
(67, 1, 11, 1),
(67, 1, 18, 1),
(67, 1, 21, 1),
(67, 1, 30, 1),
(67, 1, 46, 1),
(2, 1, 10, 2),
(5, 1, 15, 2),
(6, 1, 26, 2),
(7, 1, 8, 2),
(8, 1, 101, 2),
(9, 1, 9, 2),
(11, 1, 67, 2),
(11, 1, 68, 2),
(11, 1, 69, 2),
(18, 1, 126, 2),
(18, 1, 127, 2),
(19, 1, 13, 2),
(19, 1, 14, 2),
(19, 1, 17, 2),
(27, 1, 108, 2),
(27, 1, 109, 2),
(27, 1, 118, 2),
(27, 1, 119, 2),
(27, 1, 120, 2),
(27, 1, 122, 2),
(27, 1, 123, 2),
(27, 1, 124, 2),
(27, 1, 134, 2),
(27, 1, 135, 2),
(27, 1, 136, 2),
(30, 1, 1, 2),
(30, 1, 5, 2),
(30, 1, 114, 2),
(31, 1, 142, 2),
(32, 1, 55, 2),
(32, 1, 143, 2),
(32, 1, 149, 2),
(33, 1, 150, 2),
(34, 1, 144, 2),
(38, 1, 96, 2),
(38, 1, 97, 2),
(39, 1, 42, 2),
(41, 1, 39, 2),
(50, 1, 31, 2),
(60, 1, 52, 2),
(63, 1, 98, 2),
(63, 1, 113, 2),
(64, 1, 57, 2),
(4, 1, 10, 3),
(4, 1, 96, 3),
(4, 1, 97, 3),
(7, 1, 26, 3),
(10, 1, 101, 3),
(11, 1, 8, 3),
(13, 1, 9, 3),
(23, 1, 13, 3),
(23, 1, 14, 3),
(23, 1, 17, 3),
(23, 1, 96, 3),
(23, 1, 97, 3),
(27, 1, 15, 3),
(27, 1, 68, 3),
(33, 1, 143, 3),
(33, 1, 149, 3),
(34, 1, 150, 3),
(37, 1, 120, 3),
(42, 1, 39, 3),
(65, 1, 57, 3),
(65, 1, 126, 3),
(65, 1, 127, 3),
(5, 1, 10, 4),
(12, 1, 8, 4),
(12, 1, 26, 4),
(14, 1, 101, 4),
(26, 1, 13, 4),
(26, 1, 14, 4),
(26, 1, 17, 4),
(28, 1, 15, 4),
(34, 1, 143, 4),
(35, 1, 149, 4),
(38, 1, 68, 4),
(43, 1, 39, 4),
(67, 1, 9, 4),
(67, 1, 57, 4),
(6, 1, 10, 5),
(16, 1, 8, 5),
(18, 1, 26, 5),
(38, 1, 13, 5),
(38, 1, 14, 5),
(38, 1, 17, 5),
(40, 1, 15, 5),
(44, 1, 39, 5),
(7, 1, 10, 6),
(15, 1, 26, 6),
(17, 1, 8, 6),
(41, 1, 15, 6),
(45, 1, 39, 6),
(8, 1, 10, 7),
(19, 1, 8, 7),
(46, 1, 39, 7),
(50, 1, 26, 7),
(9, 1, 10, 8),
(21, 1, 8, 8),
(47, 1, 39, 8),
(63, 1, 26, 8),
(10, 1, 10, 9),
(23, 1, 8, 9),
(48, 1, 39, 9),
(67, 1, 26, 9),
(11, 1, 10, 10),
(24, 1, 8, 10),
(49, 1, 39, 10),
(12, 1, 10, 11),
(25, 1, 8, 11),
(51, 1, 39, 11),
(14, 1, 10, 12),
(26, 1, 8, 12),
(52, 1, 39, 12),
(15, 1, 10, 13),
(29, 1, 8, 13),
(53, 1, 39, 13),
(16, 1, 10, 14),
(54, 1, 39, 14),
(63, 1, 8, 14),
(17, 1, 10, 15),
(55, 1, 39, 15),
(18, 1, 10, 16),
(56, 1, 39, 16),
(19, 1, 10, 17),
(57, 1, 39, 17),
(20, 1, 10, 18),
(58, 1, 39, 18),
(21, 1, 10, 19),
(59, 1, 39, 19),
(22, 1, 10, 20),
(60, 1, 39, 20),
(23, 1, 10, 21),
(61, 1, 39, 21),
(24, 1, 10, 22),
(62, 1, 39, 22),
(25, 1, 10, 23),
(26, 1, 10, 24),
(27, 1, 10, 25),
(28, 1, 10, 26),
(29, 1, 10, 27),
(37, 1, 10, 28),
(38, 1, 10, 29),
(39, 1, 10, 30),
(63, 1, 10, 31),
(67, 1, 10, 32);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_hook_module_exceptions`
--

DROP TABLE IF EXISTS `ps_hook_module_exceptions`;
CREATE TABLE `ps_hook_module_exceptions` (
  `id_hook_module_exceptions` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT 1,
  `id_module` int(10) UNSIGNED NOT NULL,
  `id_hook` int(10) UNSIGNED NOT NULL,
  `file_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_hook_module_exceptions`
--

INSERT INTO `ps_hook_module_exceptions` (`id_hook_module_exceptions`, `id_shop`, `id_module`, `id_hook`, `file_name`) VALUES
(1, 1, 4, 8, 'category'),
(2, 1, 16, 8, 'category'),
(3, 1, 17, 8, 'category'),
(4, 1, 21, 8, 'category'),
(5, 1, 25, 8, 'category');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_image`
--

DROP TABLE IF EXISTS `ps_image`;
CREATE TABLE `ps_image` (
  `id_image` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `position` smallint(2) UNSIGNED NOT NULL DEFAULT 0,
  `cover` tinyint(1) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_image`
--

INSERT INTO `ps_image` (`id_image`, `id_product`, `position`, `cover`) VALUES
(1, 1, 1, 1),
(2, 2, 1, 1),
(3, 3, 1, 1),
(4, 4, 1, 1),
(5, 5, 1, 1),
(6, 6, 1, 1),
(7, 7, 1, 1),
(8, 8, 1, 1),
(9, 9, 1, 1),
(10, 10, 1, 1),
(11, 11, 1, 1),
(12, 12, 1, 1),
(13, 13, 1, 1),
(14, 14, 1, 1),
(15, 15, 1, 1),
(16, 16, 1, 1),
(17, 17, 1, 1),
(18, 18, 1, 1),
(19, 19, 1, 1),
(20, 20, 1, 1),
(21, 21, 1, 1),
(22, 22, 1, 1),
(23, 23, 1, 1),
(24, 24, 1, 1),
(25, 25, 1, 1),
(26, 26, 1, 1),
(27, 27, 1, 1),
(28, 28, 1, 1),
(29, 29, 1, 1),
(30, 30, 1, 1),
(31, 31, 1, 1),
(32, 32, 1, 1),
(33, 33, 1, 1),
(34, 34, 1, 1),
(35, 35, 1, 1),
(36, 36, 1, 1),
(37, 37, 1, 1),
(38, 38, 1, 1),
(39, 39, 1, 1),
(40, 40, 1, 1),
(41, 41, 1, 1),
(42, 42, 1, 1),
(43, 43, 1, 1),
(44, 44, 1, 1),
(45, 45, 1, 1),
(46, 46, 1, 1),
(47, 47, 1, 1),
(48, 48, 1, 1),
(49, 49, 1, 1),
(50, 50, 1, 1),
(51, 51, 1, 1),
(52, 52, 1, 1),
(53, 53, 1, 1),
(54, 54, 1, 1),
(55, 55, 1, 1),
(56, 56, 1, 1),
(57, 57, 1, 1),
(58, 58, 1, 1),
(59, 59, 1, 1),
(60, 60, 1, 1),
(61, 61, 1, 1),
(62, 62, 1, 1),
(66, 66, 1, 1),
(67, 67, 1, 1),
(68, 68, 1, 1),
(69, 69, 1, 1),
(70, 70, 1, 1),
(71, 71, 1, 1),
(72, 72, 1, 1),
(73, 73, 1, 1),
(74, 74, 1, 1),
(75, 75, 1, 1),
(76, 76, 1, 1),
(77, 77, 1, 1),
(78, 78, 1, 1),
(79, 79, 1, 1),
(80, 80, 1, 1),
(81, 81, 1, 1),
(82, 82, 1, 1),
(83, 83, 1, 1),
(84, 84, 1, 1),
(85, 85, 1, 1),
(86, 86, 1, 1),
(87, 87, 1, 1),
(88, 88, 1, 1),
(89, 89, 1, 1),
(90, 90, 1, 1),
(91, 91, 1, 1),
(92, 92, 1, 1),
(93, 93, 1, 1),
(94, 94, 1, 1),
(95, 95, 1, 1),
(96, 96, 1, 1),
(97, 97, 1, 1),
(98, 98, 1, 1),
(99, 99, 1, 1),
(100, 100, 1, 1),
(101, 101, 1, 1),
(102, 102, 1, 1),
(103, 103, 1, 1),
(104, 104, 1, 1),
(105, 105, 1, 1),
(106, 106, 1, 1),
(107, 107, 1, 1),
(108, 108, 1, 1),
(109, 109, 1, 1),
(110, 110, 1, 1),
(111, 111, 1, 1),
(112, 112, 1, 1),
(113, 113, 1, 1),
(114, 114, 1, 1),
(115, 115, 1, 1),
(116, 116, 1, 1),
(117, 117, 1, 1),
(118, 118, 1, 1),
(119, 119, 1, 1),
(120, 120, 1, 1),
(121, 121, 1, 1),
(122, 122, 1, 1),
(123, 123, 1, 1),
(124, 124, 1, 1),
(125, 125, 1, 1),
(126, 126, 1, 1),
(127, 127, 1, 1),
(128, 128, 1, 1),
(129, 129, 1, 1),
(130, 130, 1, 1),
(131, 131, 1, 1),
(132, 132, 1, 1),
(133, 133, 1, 1),
(134, 134, 1, 1),
(135, 135, 1, 1),
(136, 136, 1, 1),
(137, 137, 1, 1),
(138, 138, 1, 1),
(139, 139, 1, 1),
(140, 140, 1, 1),
(141, 141, 1, 1),
(142, 142, 1, 1),
(143, 143, 1, 1),
(144, 144, 1, 1),
(145, 145, 1, 1),
(146, 146, 1, 1),
(147, 147, 1, 1),
(148, 148, 1, 1),
(149, 149, 1, 1),
(150, 150, 1, 1),
(151, 151, 1, 1),
(152, 152, 1, 1),
(153, 153, 1, 1),
(154, 154, 1, 1),
(155, 155, 1, 1),
(156, 156, 1, 1),
(157, 157, 1, 1),
(158, 158, 1, 1),
(159, 159, 1, 1),
(160, 160, 1, 1),
(161, 161, 1, 1),
(162, 162, 1, 1),
(163, 163, 1, 1),
(164, 164, 1, 1),
(165, 165, 1, 1),
(166, 166, 1, 1),
(167, 167, 1, 1),
(168, 168, 1, 1),
(169, 169, 1, 1),
(170, 170, 1, 1),
(171, 171, 1, 1),
(172, 172, 1, 1),
(173, 173, 1, 1),
(174, 174, 1, 1),
(175, 175, 1, 1),
(176, 176, 1, 1),
(177, 177, 1, 1),
(178, 178, 1, 1),
(179, 179, 1, 1),
(180, 180, 1, 1),
(181, 181, 1, 1),
(182, 182, 1, 1),
(183, 183, 1, 1),
(184, 184, 1, 1),
(185, 185, 1, 1),
(186, 186, 1, 1),
(187, 187, 1, 1),
(188, 188, 1, 1),
(189, 189, 1, 1),
(190, 190, 1, 1),
(191, 191, 1, 1),
(192, 192, 1, 1),
(193, 193, 1, 1),
(194, 194, 1, 1),
(195, 195, 1, 1),
(196, 196, 1, 1),
(197, 197, 1, 1),
(198, 198, 1, 1),
(199, 199, 1, 1),
(200, 200, 1, 1),
(201, 201, 1, 1),
(202, 202, 1, 1),
(203, 203, 1, 1),
(204, 204, 1, 1),
(205, 205, 1, 1),
(206, 206, 1, 1),
(207, 207, 1, 1),
(208, 208, 1, 1),
(209, 209, 1, 1),
(210, 210, 1, 1),
(211, 211, 1, 1),
(212, 212, 1, 1),
(213, 213, 1, 1),
(214, 214, 1, 1),
(215, 215, 1, 1),
(216, 216, 1, 1),
(217, 217, 1, 1),
(218, 218, 1, 1),
(219, 219, 1, 1),
(220, 220, 1, 1),
(221, 221, 1, 1),
(222, 222, 1, 1),
(223, 223, 1, 1),
(224, 224, 1, 1),
(225, 225, 1, 1),
(226, 226, 1, 1),
(227, 227, 1, 1),
(228, 228, 1, 1),
(229, 229, 1, 1),
(230, 230, 1, 1),
(231, 231, 1, 1),
(232, 232, 1, 1),
(233, 233, 1, 1),
(234, 234, 1, 1),
(235, 235, 1, 1),
(236, 236, 1, 1),
(237, 237, 1, 1),
(238, 238, 1, 1),
(239, 239, 1, 1),
(240, 240, 1, 1),
(241, 241, 1, 1),
(242, 242, 1, 1),
(243, 243, 1, 1),
(244, 244, 1, 1),
(245, 245, 1, 1),
(246, 246, 1, 1),
(247, 247, 1, 1),
(248, 248, 1, 1),
(249, 249, 1, 1),
(250, 250, 1, 1),
(251, 251, 1, 1),
(252, 252, 1, 1),
(253, 253, 1, 1),
(254, 254, 1, 1),
(255, 255, 1, 1),
(256, 256, 1, 1),
(257, 257, 1, 1),
(258, 258, 1, 1),
(259, 259, 1, 1),
(260, 260, 1, 1),
(261, 261, 1, 1),
(262, 262, 1, 1),
(263, 263, 1, 1),
(264, 264, 1, 1),
(265, 265, 1, 1),
(266, 266, 1, 1),
(267, 267, 1, 1),
(268, 268, 1, 1),
(269, 269, 1, 1),
(270, 270, 1, 1),
(271, 271, 1, 1),
(272, 272, 1, 1),
(273, 273, 1, 1),
(274, 274, 1, 1),
(275, 275, 1, 1),
(276, 276, 1, 1),
(277, 277, 1, 1),
(278, 278, 1, 1),
(279, 279, 1, 1),
(280, 280, 1, 1),
(281, 281, 1, 1),
(282, 282, 1, 1),
(283, 283, 1, 1),
(284, 284, 1, 1),
(285, 285, 1, 1),
(286, 286, 1, 1),
(287, 287, 1, 1),
(288, 288, 1, 1),
(289, 289, 1, 1),
(290, 290, 1, 1),
(291, 291, 1, 1),
(292, 292, 1, 1),
(293, 293, 1, 1),
(294, 294, 1, 1),
(295, 295, 1, 1),
(296, 296, 1, 1),
(297, 297, 1, 1),
(298, 298, 1, 1),
(299, 299, 1, 1),
(300, 300, 1, 1),
(301, 301, 1, 1),
(302, 302, 1, 1),
(303, 303, 1, 1),
(304, 304, 1, 1),
(305, 305, 1, 1),
(306, 306, 1, 1),
(307, 307, 1, 1),
(308, 308, 1, 1),
(309, 309, 1, 1),
(310, 310, 1, 1),
(311, 311, 1, 1),
(312, 312, 1, 1),
(313, 313, 1, 1),
(314, 314, 1, 1),
(315, 315, 1, 1),
(316, 316, 1, 1),
(317, 317, 1, 1),
(318, 318, 1, 1),
(319, 319, 1, 1),
(320, 320, 1, 1),
(321, 321, 1, 1),
(323, 323, 1, 1),
(324, 324, 1, 1),
(325, 325, 1, 1),
(326, 326, 1, 1),
(327, 327, 1, 1),
(328, 328, 1, 1),
(329, 329, 1, 1),
(330, 330, 1, 1),
(331, 331, 1, 1),
(332, 332, 1, 1),
(333, 333, 1, 1),
(334, 334, 1, 1),
(335, 335, 1, 1),
(336, 336, 1, 1),
(337, 337, 1, 1),
(338, 338, 1, 1),
(339, 339, 1, 1),
(340, 340, 1, 1),
(341, 341, 1, 1),
(342, 342, 1, 1),
(343, 343, 1, 1),
(344, 344, 1, 1),
(345, 345, 1, 1),
(346, 346, 1, 1),
(347, 347, 1, 1),
(348, 348, 1, 1),
(349, 349, 1, 1),
(350, 350, 1, 1),
(351, 351, 1, 1),
(352, 352, 1, 1),
(353, 353, 1, 1),
(354, 354, 1, 1),
(355, 355, 1, 1),
(356, 356, 1, 1),
(357, 357, 1, 1),
(358, 358, 1, 1),
(359, 359, 1, 1),
(360, 360, 1, 1),
(361, 361, 1, 1),
(362, 362, 1, 1),
(363, 363, 1, 1),
(364, 364, 1, 1),
(365, 365, 1, 1),
(366, 366, 1, 1),
(367, 367, 1, 1),
(368, 368, 1, 1),
(369, 369, 1, 1),
(370, 370, 1, 1),
(371, 371, 1, 1),
(372, 372, 1, 1),
(373, 373, 1, 1),
(374, 374, 1, 1),
(375, 375, 1, 1),
(376, 376, 1, 1),
(377, 377, 1, 1),
(378, 378, 1, 1),
(379, 379, 1, 1),
(380, 380, 1, 1),
(381, 381, 1, 1),
(382, 382, 1, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_image_lang`
--

DROP TABLE IF EXISTS `ps_image_lang`;
CREATE TABLE `ps_image_lang` (
  `id_image` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `legend` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_image_lang`
--

INSERT INTO `ps_image_lang` (`id_image`, `id_lang`, `legend`) VALUES
(1, 1, ''),
(2, 1, ''),
(3, 1, ''),
(4, 1, ''),
(5, 1, ''),
(6, 1, ''),
(7, 1, ''),
(8, 1, ''),
(9, 1, ''),
(10, 1, ''),
(11, 1, ''),
(12, 1, ''),
(13, 1, ''),
(14, 1, ''),
(15, 1, ''),
(16, 1, ''),
(17, 1, ''),
(18, 1, ''),
(19, 1, ''),
(20, 1, ''),
(21, 1, ''),
(22, 1, ''),
(23, 1, ''),
(24, 1, ''),
(25, 1, ''),
(26, 1, ''),
(27, 1, ''),
(28, 1, ''),
(29, 1, ''),
(30, 1, ''),
(31, 1, ''),
(32, 1, ''),
(33, 1, ''),
(34, 1, ''),
(35, 1, ''),
(36, 1, ''),
(37, 1, ''),
(38, 1, ''),
(39, 1, ''),
(40, 1, ''),
(41, 1, ''),
(42, 1, ''),
(43, 1, ''),
(44, 1, ''),
(45, 1, ''),
(46, 1, ''),
(47, 1, ''),
(48, 1, ''),
(49, 1, ''),
(50, 1, ''),
(51, 1, ''),
(52, 1, ''),
(53, 1, ''),
(54, 1, ''),
(55, 1, ''),
(56, 1, ''),
(57, 1, ''),
(58, 1, ''),
(59, 1, ''),
(60, 1, ''),
(61, 1, ''),
(62, 1, ''),
(66, 1, ''),
(67, 1, ''),
(68, 1, ''),
(69, 1, ''),
(70, 1, ''),
(71, 1, ''),
(72, 1, ''),
(73, 1, ''),
(74, 1, ''),
(75, 1, ''),
(76, 1, ''),
(77, 1, ''),
(78, 1, ''),
(79, 1, ''),
(80, 1, ''),
(81, 1, ''),
(82, 1, ''),
(83, 1, ''),
(84, 1, ''),
(85, 1, ''),
(86, 1, ''),
(87, 1, ''),
(88, 1, ''),
(89, 1, ''),
(90, 1, ''),
(91, 1, ''),
(92, 1, ''),
(93, 1, ''),
(94, 1, ''),
(95, 1, ''),
(96, 1, ''),
(97, 1, ''),
(98, 1, ''),
(99, 1, ''),
(100, 1, ''),
(101, 1, ''),
(102, 1, ''),
(103, 1, ''),
(104, 1, ''),
(105, 1, ''),
(106, 1, ''),
(107, 1, ''),
(108, 1, ''),
(109, 1, ''),
(110, 1, ''),
(111, 1, ''),
(112, 1, ''),
(113, 1, ''),
(114, 1, ''),
(115, 1, ''),
(116, 1, ''),
(117, 1, ''),
(118, 1, ''),
(119, 1, ''),
(120, 1, ''),
(121, 1, ''),
(122, 1, ''),
(123, 1, ''),
(124, 1, ''),
(125, 1, ''),
(126, 1, ''),
(127, 1, ''),
(128, 1, ''),
(129, 1, ''),
(130, 1, ''),
(131, 1, ''),
(132, 1, ''),
(133, 1, ''),
(134, 1, ''),
(135, 1, ''),
(136, 1, ''),
(137, 1, ''),
(138, 1, ''),
(139, 1, ''),
(140, 1, ''),
(141, 1, ''),
(142, 1, ''),
(143, 1, ''),
(144, 1, ''),
(145, 1, ''),
(146, 1, ''),
(147, 1, ''),
(148, 1, ''),
(149, 1, ''),
(150, 1, ''),
(151, 1, ''),
(152, 1, ''),
(153, 1, ''),
(154, 1, ''),
(155, 1, ''),
(156, 1, ''),
(157, 1, ''),
(158, 1, ''),
(159, 1, ''),
(160, 1, ''),
(161, 1, ''),
(162, 1, ''),
(163, 1, ''),
(164, 1, ''),
(165, 1, ''),
(166, 1, ''),
(167, 1, ''),
(168, 1, ''),
(169, 1, ''),
(170, 1, ''),
(171, 1, ''),
(172, 1, ''),
(173, 1, ''),
(174, 1, ''),
(175, 1, ''),
(176, 1, ''),
(177, 1, ''),
(178, 1, ''),
(179, 1, ''),
(180, 1, ''),
(181, 1, ''),
(182, 1, ''),
(183, 1, ''),
(184, 1, ''),
(185, 1, ''),
(186, 1, ''),
(187, 1, ''),
(188, 1, ''),
(189, 1, ''),
(190, 1, ''),
(191, 1, ''),
(192, 1, ''),
(193, 1, ''),
(194, 1, ''),
(195, 1, ''),
(196, 1, ''),
(197, 1, ''),
(198, 1, ''),
(199, 1, ''),
(200, 1, ''),
(201, 1, ''),
(202, 1, ''),
(203, 1, ''),
(204, 1, ''),
(205, 1, ''),
(206, 1, ''),
(207, 1, ''),
(208, 1, ''),
(209, 1, ''),
(210, 1, ''),
(211, 1, ''),
(212, 1, ''),
(213, 1, ''),
(214, 1, ''),
(215, 1, ''),
(216, 1, ''),
(217, 1, ''),
(218, 1, ''),
(219, 1, ''),
(220, 1, ''),
(221, 1, ''),
(222, 1, ''),
(223, 1, ''),
(224, 1, ''),
(225, 1, ''),
(226, 1, ''),
(227, 1, ''),
(228, 1, ''),
(229, 1, ''),
(230, 1, ''),
(231, 1, ''),
(232, 1, ''),
(233, 1, ''),
(234, 1, ''),
(235, 1, ''),
(236, 1, ''),
(237, 1, ''),
(238, 1, ''),
(239, 1, ''),
(240, 1, ''),
(241, 1, ''),
(242, 1, ''),
(243, 1, ''),
(244, 1, ''),
(245, 1, ''),
(246, 1, ''),
(247, 1, ''),
(248, 1, ''),
(249, 1, ''),
(250, 1, ''),
(251, 1, ''),
(252, 1, ''),
(253, 1, ''),
(254, 1, ''),
(255, 1, ''),
(256, 1, ''),
(257, 1, ''),
(258, 1, ''),
(259, 1, ''),
(260, 1, ''),
(261, 1, ''),
(262, 1, ''),
(263, 1, ''),
(264, 1, ''),
(265, 1, ''),
(266, 1, ''),
(267, 1, ''),
(268, 1, ''),
(269, 1, ''),
(270, 1, ''),
(271, 1, ''),
(272, 1, ''),
(273, 1, ''),
(274, 1, ''),
(275, 1, ''),
(276, 1, ''),
(277, 1, ''),
(278, 1, ''),
(279, 1, ''),
(280, 1, ''),
(281, 1, ''),
(282, 1, ''),
(283, 1, ''),
(284, 1, ''),
(285, 1, ''),
(286, 1, ''),
(287, 1, ''),
(288, 1, ''),
(289, 1, ''),
(290, 1, ''),
(291, 1, ''),
(292, 1, ''),
(293, 1, ''),
(294, 1, ''),
(295, 1, ''),
(296, 1, ''),
(297, 1, ''),
(298, 1, ''),
(299, 1, ''),
(300, 1, ''),
(301, 1, ''),
(302, 1, ''),
(303, 1, ''),
(304, 1, ''),
(305, 1, ''),
(306, 1, ''),
(307, 1, ''),
(308, 1, ''),
(309, 1, ''),
(310, 1, ''),
(311, 1, ''),
(312, 1, ''),
(313, 1, ''),
(314, 1, ''),
(315, 1, ''),
(316, 1, ''),
(317, 1, ''),
(318, 1, ''),
(319, 1, ''),
(320, 1, ''),
(321, 1, ''),
(323, 1, ''),
(324, 1, ''),
(325, 1, ''),
(326, 1, ''),
(327, 1, ''),
(328, 1, ''),
(329, 1, ''),
(330, 1, ''),
(331, 1, ''),
(332, 1, ''),
(333, 1, ''),
(334, 1, ''),
(335, 1, ''),
(336, 1, ''),
(337, 1, ''),
(338, 1, ''),
(339, 1, ''),
(340, 1, ''),
(341, 1, ''),
(342, 1, ''),
(343, 1, ''),
(344, 1, ''),
(345, 1, ''),
(346, 1, ''),
(347, 1, ''),
(348, 1, ''),
(349, 1, ''),
(350, 1, ''),
(351, 1, ''),
(352, 1, ''),
(353, 1, ''),
(354, 1, ''),
(355, 1, ''),
(356, 1, ''),
(357, 1, ''),
(358, 1, ''),
(359, 1, ''),
(360, 1, ''),
(361, 1, ''),
(362, 1, ''),
(363, 1, ''),
(364, 1, ''),
(365, 1, ''),
(366, 1, ''),
(367, 1, ''),
(368, 1, ''),
(369, 1, ''),
(370, 1, ''),
(371, 1, ''),
(372, 1, ''),
(373, 1, ''),
(374, 1, ''),
(375, 1, ''),
(376, 1, ''),
(377, 1, ''),
(378, 1, ''),
(379, 1, ''),
(380, 1, ''),
(381, 1, ''),
(382, 1, '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_image_shop`
--

DROP TABLE IF EXISTS `ps_image_shop`;
CREATE TABLE `ps_image_shop` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_image` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  `cover` tinyint(1) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_image_shop`
--

INSERT INTO `ps_image_shop` (`id_product`, `id_image`, `id_shop`, `cover`) VALUES
(1, 1, 1, 1),
(2, 2, 1, 1),
(3, 3, 1, 1),
(4, 4, 1, 1),
(5, 5, 1, 1),
(6, 6, 1, 1),
(7, 7, 1, 1),
(8, 8, 1, 1),
(9, 9, 1, 1),
(10, 10, 1, 1),
(11, 11, 1, 1),
(12, 12, 1, 1),
(13, 13, 1, 1),
(14, 14, 1, 1),
(15, 15, 1, 1),
(16, 16, 1, 1),
(17, 17, 1, 1),
(18, 18, 1, 1),
(19, 19, 1, 1),
(20, 20, 1, 1),
(21, 21, 1, 1),
(22, 22, 1, 1),
(23, 23, 1, 1),
(24, 24, 1, 1),
(25, 25, 1, 1),
(26, 26, 1, 1),
(27, 27, 1, 1),
(28, 28, 1, 1),
(29, 29, 1, 1),
(30, 30, 1, 1),
(31, 31, 1, 1),
(32, 32, 1, 1),
(33, 33, 1, 1),
(34, 34, 1, 1),
(35, 35, 1, 1),
(36, 36, 1, 1),
(37, 37, 1, 1),
(38, 38, 1, 1),
(39, 39, 1, 1),
(40, 40, 1, 1),
(41, 41, 1, 1),
(42, 42, 1, 1),
(43, 43, 1, 1),
(44, 44, 1, 1),
(45, 45, 1, 1),
(46, 46, 1, 1),
(47, 47, 1, 1),
(48, 48, 1, 1),
(49, 49, 1, 1),
(50, 50, 1, 1),
(51, 51, 1, 1),
(52, 52, 1, 1),
(53, 53, 1, 1),
(54, 54, 1, 1),
(55, 55, 1, 1),
(56, 56, 1, 1),
(57, 57, 1, 1),
(58, 58, 1, 1),
(59, 59, 1, 1),
(60, 60, 1, 1),
(61, 61, 1, 1),
(62, 62, 1, 1),
(66, 66, 1, 1),
(67, 67, 1, 1),
(68, 68, 1, 1),
(69, 69, 1, 1),
(70, 70, 1, 1),
(71, 71, 1, 1),
(72, 72, 1, 1),
(73, 73, 1, 1),
(74, 74, 1, 1),
(75, 75, 1, 1),
(76, 76, 1, 1),
(77, 77, 1, 1),
(78, 78, 1, 1),
(79, 79, 1, 1),
(80, 80, 1, 1),
(81, 81, 1, 1),
(82, 82, 1, 1),
(83, 83, 1, 1),
(84, 84, 1, 1),
(85, 85, 1, 1),
(86, 86, 1, 1),
(87, 87, 1, 1),
(88, 88, 1, 1),
(89, 89, 1, 1),
(90, 90, 1, 1),
(91, 91, 1, 1),
(92, 92, 1, 1),
(93, 93, 1, 1),
(94, 94, 1, 1),
(95, 95, 1, 1),
(96, 96, 1, 1),
(97, 97, 1, 1),
(98, 98, 1, 1),
(99, 99, 1, 1),
(100, 100, 1, 1),
(101, 101, 1, 1),
(102, 102, 1, 1),
(103, 103, 1, 1),
(104, 104, 1, 1),
(105, 105, 1, 1),
(106, 106, 1, 1),
(107, 107, 1, 1),
(108, 108, 1, 1),
(109, 109, 1, 1),
(110, 110, 1, 1),
(111, 111, 1, 1),
(112, 112, 1, 1),
(113, 113, 1, 1),
(114, 114, 1, 1),
(115, 115, 1, 1),
(116, 116, 1, 1),
(117, 117, 1, 1),
(118, 118, 1, 1),
(119, 119, 1, 1),
(120, 120, 1, 1),
(121, 121, 1, 1),
(122, 122, 1, 1),
(123, 123, 1, 1),
(124, 124, 1, 1),
(125, 125, 1, 1),
(126, 126, 1, 1),
(127, 127, 1, 1),
(128, 128, 1, 1),
(129, 129, 1, 1),
(130, 130, 1, 1),
(131, 131, 1, 1),
(132, 132, 1, 1),
(133, 133, 1, 1),
(134, 134, 1, 1),
(135, 135, 1, 1),
(136, 136, 1, 1),
(137, 137, 1, 1),
(138, 138, 1, 1),
(139, 139, 1, 1),
(140, 140, 1, 1),
(141, 141, 1, 1),
(142, 142, 1, 1),
(143, 143, 1, 1),
(144, 144, 1, 1),
(145, 145, 1, 1),
(146, 146, 1, 1),
(147, 147, 1, 1),
(148, 148, 1, 1),
(149, 149, 1, 1),
(150, 150, 1, 1),
(151, 151, 1, 1),
(152, 152, 1, 1),
(153, 153, 1, 1),
(154, 154, 1, 1),
(155, 155, 1, 1),
(156, 156, 1, 1),
(157, 157, 1, 1),
(158, 158, 1, 1),
(159, 159, 1, 1),
(160, 160, 1, 1),
(161, 161, 1, 1),
(162, 162, 1, 1),
(163, 163, 1, 1),
(164, 164, 1, 1),
(165, 165, 1, 1),
(166, 166, 1, 1),
(167, 167, 1, 1),
(168, 168, 1, 1),
(169, 169, 1, 1),
(170, 170, 1, 1),
(171, 171, 1, 1),
(172, 172, 1, 1),
(173, 173, 1, 1),
(174, 174, 1, 1),
(175, 175, 1, 1),
(176, 176, 1, 1),
(177, 177, 1, 1),
(178, 178, 1, 1),
(179, 179, 1, 1),
(180, 180, 1, 1),
(181, 181, 1, 1),
(182, 182, 1, 1),
(183, 183, 1, 1),
(184, 184, 1, 1),
(185, 185, 1, 1),
(186, 186, 1, 1),
(187, 187, 1, 1),
(188, 188, 1, 1),
(189, 189, 1, 1),
(190, 190, 1, 1),
(191, 191, 1, 1),
(192, 192, 1, 1),
(193, 193, 1, 1),
(194, 194, 1, 1),
(195, 195, 1, 1),
(196, 196, 1, 1),
(197, 197, 1, 1),
(198, 198, 1, 1),
(199, 199, 1, 1),
(200, 200, 1, 1),
(201, 201, 1, 1),
(202, 202, 1, 1),
(203, 203, 1, 1),
(204, 204, 1, 1),
(205, 205, 1, 1),
(206, 206, 1, 1),
(207, 207, 1, 1),
(208, 208, 1, 1),
(209, 209, 1, 1),
(210, 210, 1, 1),
(211, 211, 1, 1),
(212, 212, 1, 1),
(213, 213, 1, 1),
(214, 214, 1, 1),
(215, 215, 1, 1),
(216, 216, 1, 1),
(217, 217, 1, 1),
(218, 218, 1, 1),
(219, 219, 1, 1),
(220, 220, 1, 1),
(221, 221, 1, 1),
(222, 222, 1, 1),
(223, 223, 1, 1),
(224, 224, 1, 1),
(225, 225, 1, 1),
(226, 226, 1, 1),
(227, 227, 1, 1),
(228, 228, 1, 1),
(229, 229, 1, 1),
(230, 230, 1, 1),
(231, 231, 1, 1),
(232, 232, 1, 1),
(233, 233, 1, 1),
(234, 234, 1, 1),
(235, 235, 1, 1),
(236, 236, 1, 1),
(237, 237, 1, 1),
(238, 238, 1, 1),
(239, 239, 1, 1),
(240, 240, 1, 1),
(241, 241, 1, 1),
(242, 242, 1, 1),
(243, 243, 1, 1),
(244, 244, 1, 1),
(245, 245, 1, 1),
(246, 246, 1, 1),
(247, 247, 1, 1),
(248, 248, 1, 1),
(249, 249, 1, 1),
(250, 250, 1, 1),
(251, 251, 1, 1),
(252, 252, 1, 1),
(253, 253, 1, 1),
(254, 254, 1, 1),
(255, 255, 1, 1),
(256, 256, 1, 1),
(257, 257, 1, 1),
(258, 258, 1, 1),
(259, 259, 1, 1),
(260, 260, 1, 1),
(261, 261, 1, 1),
(262, 262, 1, 1),
(263, 263, 1, 1),
(264, 264, 1, 1),
(265, 265, 1, 1),
(266, 266, 1, 1),
(267, 267, 1, 1),
(268, 268, 1, 1),
(269, 269, 1, 1),
(270, 270, 1, 1),
(271, 271, 1, 1),
(272, 272, 1, 1),
(273, 273, 1, 1),
(274, 274, 1, 1),
(275, 275, 1, 1),
(276, 276, 1, 1),
(277, 277, 1, 1),
(278, 278, 1, 1),
(279, 279, 1, 1),
(280, 280, 1, 1),
(281, 281, 1, 1),
(282, 282, 1, 1),
(283, 283, 1, 1),
(284, 284, 1, 1),
(285, 285, 1, 1),
(286, 286, 1, 1),
(287, 287, 1, 1),
(288, 288, 1, 1),
(289, 289, 1, 1),
(290, 290, 1, 1),
(291, 291, 1, 1),
(292, 292, 1, 1),
(293, 293, 1, 1),
(294, 294, 1, 1),
(295, 295, 1, 1),
(296, 296, 1, 1),
(297, 297, 1, 1),
(298, 298, 1, 1),
(299, 299, 1, 1),
(300, 300, 1, 1),
(301, 301, 1, 1),
(302, 302, 1, 1),
(303, 303, 1, 1),
(304, 304, 1, 1),
(305, 305, 1, 1),
(306, 306, 1, 1),
(307, 307, 1, 1),
(308, 308, 1, 1),
(309, 309, 1, 1),
(310, 310, 1, 1),
(311, 311, 1, 1),
(312, 312, 1, 1),
(313, 313, 1, 1),
(314, 314, 1, 1),
(315, 315, 1, 1),
(316, 316, 1, 1),
(317, 317, 1, 1),
(318, 318, 1, 1),
(319, 319, 1, 1),
(320, 320, 1, 1),
(321, 321, 1, 1),
(323, 323, 1, 1),
(324, 324, 1, 1),
(325, 325, 1, 1),
(326, 326, 1, 1),
(327, 327, 1, 1),
(328, 328, 1, 1),
(329, 329, 1, 1),
(330, 330, 1, 1),
(331, 331, 1, 1),
(332, 332, 1, 1),
(333, 333, 1, 1),
(334, 334, 1, 1),
(335, 335, 1, 1),
(336, 336, 1, 1),
(337, 337, 1, 1),
(338, 338, 1, 1),
(339, 339, 1, 1),
(340, 340, 1, 1),
(341, 341, 1, 1),
(342, 342, 1, 1),
(343, 343, 1, 1),
(344, 344, 1, 1),
(345, 345, 1, 1),
(346, 346, 1, 1),
(347, 347, 1, 1),
(348, 348, 1, 1),
(349, 349, 1, 1),
(350, 350, 1, 1),
(351, 351, 1, 1),
(352, 352, 1, 1),
(353, 353, 1, 1),
(354, 354, 1, 1),
(355, 355, 1, 1),
(356, 356, 1, 1),
(357, 357, 1, 1),
(358, 358, 1, 1),
(359, 359, 1, 1),
(360, 360, 1, 1),
(361, 361, 1, 1),
(362, 362, 1, 1),
(363, 363, 1, 1),
(364, 364, 1, 1),
(365, 365, 1, 1),
(366, 366, 1, 1),
(367, 367, 1, 1),
(368, 368, 1, 1),
(369, 369, 1, 1),
(370, 370, 1, 1),
(371, 371, 1, 1),
(372, 372, 1, 1),
(373, 373, 1, 1),
(374, 374, 1, 1),
(375, 375, 1, 1),
(376, 376, 1, 1),
(377, 377, 1, 1),
(378, 378, 1, 1),
(379, 379, 1, 1),
(380, 380, 1, 1),
(381, 381, 1, 1),
(382, 382, 1, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_image_type`
--

DROP TABLE IF EXISTS `ps_image_type`;
CREATE TABLE `ps_image_type` (
  `id_image_type` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `width` int(10) UNSIGNED NOT NULL,
  `height` int(10) UNSIGNED NOT NULL,
  `products` tinyint(1) NOT NULL DEFAULT 1,
  `categories` tinyint(1) NOT NULL DEFAULT 1,
  `manufacturers` tinyint(1) NOT NULL DEFAULT 1,
  `suppliers` tinyint(1) NOT NULL DEFAULT 1,
  `scenes` tinyint(1) NOT NULL DEFAULT 1,
  `stores` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_image_type`
--

INSERT INTO `ps_image_type` (`id_image_type`, `name`, `width`, `height`, `products`, `categories`, `manufacturers`, `suppliers`, `scenes`, `stores`) VALUES
(1, 'cart_default', 80, 80, 1, 0, 0, 0, 0, 0),
(2, 'small_default', 98, 98, 1, 0, 1, 1, 0, 0),
(3, 'medium_default', 125, 125, 1, 1, 1, 1, 0, 1),
(4, 'home_default', 250, 250, 1, 0, 0, 0, 0, 0),
(5, 'large_default', 458, 458, 1, 0, 1, 1, 0, 0),
(6, 'thickbox_default', 800, 800, 1, 0, 0, 0, 0, 0),
(7, 'category_default', 870, 217, 0, 1, 0, 0, 0, 0),
(8, 'scene_default', 870, 270, 0, 0, 0, 0, 1, 0),
(9, 'm_scene_default', 161, 58, 0, 0, 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_import_match`
--

DROP TABLE IF EXISTS `ps_import_match`;
CREATE TABLE `ps_import_match` (
  `id_import_match` int(10) NOT NULL,
  `name` varchar(32) NOT NULL,
  `match` text NOT NULL,
  `skip` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_info`
--

DROP TABLE IF EXISTS `ps_info`;
CREATE TABLE `ps_info` (
  `id_info` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_info`
--

INSERT INTO `ps_info` (`id_info`, `id_shop`) VALUES
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_info_lang`
--

DROP TABLE IF EXISTS `ps_info_lang`;
CREATE TABLE `ps_info_lang` (
  `id_info` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_info_lang`
--

INSERT INTO `ps_info_lang` (`id_info`, `id_lang`, `text`) VALUES
(1, 1, '<ul>\n<li><em class=\"icon-truck\" id=\"icon-truck\"></em>\n<div class=\"type-text\">\n<h3>Lorem Ipsum</h3>\n<p>Lorem ipsum dolor sit amet conse ctetur voluptate velit esse cillum dolore eu</p>\n</div>\n</li>\n<li><em class=\"icon-phone\" id=\"icon-phone\"></em>\n<div class=\"type-text\">\n<h3>Dolor Sit Amet</h3>\n<p>Lorem ipsum dolor sit amet conse ctetur voluptate velit esse cillum dolore eu</p>\n</div>\n</li>\n<li><em class=\"icon-credit-card\" id=\"icon-credit-card\"></em>\n<div class=\"type-text\">\n<h3>Ctetur Voluptate</h3>\n<p>Lorem ipsum dolor sit amet conse ctetur voluptate velit esse cillum dolore eu</p>\n</div>\n</li>\n</ul>'),
(2, 1, '<h3>Custom Block</h3>\n<p><strong class=\"dark\">Lorem ipsum dolor sit amet conse ctetu</strong></p>\n<p>Sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.</p>');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_lang`
--

DROP TABLE IF EXISTS `ps_lang`;
CREATE TABLE `ps_lang` (
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL,
  `active` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `iso_code` char(2) NOT NULL,
  `language_code` char(5) NOT NULL,
  `date_format_lite` char(32) NOT NULL DEFAULT 'Y-m-d',
  `date_format_full` char(32) NOT NULL DEFAULT 'Y-m-d H:i:s',
  `is_rtl` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_lang`
--

INSERT INTO `ps_lang` (`id_lang`, `name`, `active`, `iso_code`, `language_code`, `date_format_lite`, `date_format_full`, `is_rtl`) VALUES
(1, 'Polski (Polish)', 1, 'pl', 'pl-pl', 'Y-m-d', 'Y-m-d H:i:s', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_lang_shop`
--

DROP TABLE IF EXISTS `ps_lang_shop`;
CREATE TABLE `ps_lang_shop` (
  `id_lang` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_lang_shop`
--

INSERT INTO `ps_lang_shop` (`id_lang`, `id_shop`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_layered_category`
--

DROP TABLE IF EXISTS `ps_layered_category`;
CREATE TABLE `ps_layered_category` (
  `id_layered_category` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  `id_category` int(10) UNSIGNED NOT NULL,
  `id_value` int(10) UNSIGNED DEFAULT 0,
  `type` enum('category','id_feature','id_attribute_group','quantity','condition','manufacturer','weight','price') NOT NULL,
  `position` int(10) UNSIGNED NOT NULL,
  `filter_type` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `filter_show_limit` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `ps_layered_category`
--

INSERT INTO `ps_layered_category` (`id_layered_category`, `id_shop`, `id_category`, `id_value`, `type`, `position`, `filter_type`, `filter_show_limit`) VALUES
(1, 1, 2, NULL, 'category', 1, 0, 0),
(2, 1, 2, 1, 'id_attribute_group', 2, 0, 0),
(3, 1, 2, 3, 'id_attribute_group', 3, 0, 0),
(4, 1, 2, 5, 'id_feature', 4, 0, 0),
(5, 1, 2, 6, 'id_feature', 5, 0, 0),
(6, 1, 2, 7, 'id_feature', 6, 0, 0),
(7, 1, 2, NULL, 'quantity', 7, 0, 0),
(8, 1, 2, NULL, 'manufacturer', 8, 0, 0),
(9, 1, 2, NULL, 'condition', 9, 0, 0),
(10, 1, 2, NULL, 'weight', 10, 0, 0),
(11, 1, 2, NULL, 'price', 11, 0, 0),
(12, 1, 5, NULL, 'category', 1, 0, 0),
(13, 1, 5, 1, 'id_attribute_group', 2, 0, 0),
(14, 1, 5, 3, 'id_attribute_group', 3, 0, 0),
(15, 1, 5, 5, 'id_feature', 4, 0, 0),
(16, 1, 5, 6, 'id_feature', 5, 0, 0),
(17, 1, 5, 7, 'id_feature', 6, 0, 0),
(18, 1, 5, NULL, 'quantity', 7, 0, 0),
(19, 1, 5, NULL, 'manufacturer', 8, 0, 0),
(20, 1, 5, NULL, 'condition', 9, 0, 0),
(21, 1, 5, NULL, 'weight', 10, 0, 0),
(22, 1, 5, NULL, 'price', 11, 0, 0),
(23, 1, 7, NULL, 'category', 1, 0, 0),
(24, 1, 7, 1, 'id_attribute_group', 2, 0, 0),
(25, 1, 7, 3, 'id_attribute_group', 3, 0, 0),
(26, 1, 7, 5, 'id_feature', 4, 0, 0),
(27, 1, 7, 6, 'id_feature', 5, 0, 0),
(28, 1, 7, 7, 'id_feature', 6, 0, 0),
(29, 1, 7, NULL, 'quantity', 7, 0, 0),
(30, 1, 7, NULL, 'manufacturer', 8, 0, 0),
(31, 1, 7, NULL, 'condition', 9, 0, 0),
(32, 1, 7, NULL, 'weight', 10, 0, 0),
(33, 1, 7, NULL, 'price', 11, 0, 0),
(34, 1, 9, NULL, 'category', 1, 0, 0),
(35, 1, 9, 1, 'id_attribute_group', 2, 0, 0),
(36, 1, 9, 3, 'id_attribute_group', 3, 0, 0),
(37, 1, 9, 5, 'id_feature', 4, 0, 0),
(38, 1, 9, 6, 'id_feature', 5, 0, 0),
(39, 1, 9, 7, 'id_feature', 6, 0, 0),
(40, 1, 9, NULL, 'quantity', 7, 0, 0),
(41, 1, 9, NULL, 'manufacturer', 8, 0, 0),
(42, 1, 9, NULL, 'condition', 9, 0, 0),
(43, 1, 9, NULL, 'weight', 10, 0, 0),
(44, 1, 9, NULL, 'price', 11, 0, 0),
(45, 1, 10, NULL, 'category', 1, 0, 0),
(46, 1, 10, 1, 'id_attribute_group', 2, 0, 0),
(47, 1, 10, 3, 'id_attribute_group', 3, 0, 0),
(48, 1, 10, 5, 'id_feature', 4, 0, 0),
(49, 1, 10, 6, 'id_feature', 5, 0, 0),
(50, 1, 10, 7, 'id_feature', 6, 0, 0),
(51, 1, 10, NULL, 'quantity', 7, 0, 0),
(52, 1, 10, NULL, 'manufacturer', 8, 0, 0),
(53, 1, 10, NULL, 'condition', 9, 0, 0),
(54, 1, 10, NULL, 'weight', 10, 0, 0),
(55, 1, 10, NULL, 'price', 11, 0, 0),
(56, 1, 11, NULL, 'category', 1, 0, 0),
(57, 1, 11, 1, 'id_attribute_group', 2, 0, 0),
(58, 1, 11, 3, 'id_attribute_group', 3, 0, 0),
(59, 1, 11, 5, 'id_feature', 4, 0, 0),
(60, 1, 11, 6, 'id_feature', 5, 0, 0),
(61, 1, 11, 7, 'id_feature', 6, 0, 0),
(62, 1, 11, NULL, 'quantity', 7, 0, 0),
(63, 1, 11, NULL, 'manufacturer', 8, 0, 0),
(64, 1, 11, NULL, 'condition', 9, 0, 0),
(65, 1, 11, NULL, 'weight', 10, 0, 0),
(66, 1, 11, NULL, 'price', 11, 0, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_layered_filter`
--

DROP TABLE IF EXISTS `ps_layered_filter`;
CREATE TABLE `ps_layered_filter` (
  `id_layered_filter` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `filters` text DEFAULT NULL,
  `n_categories` int(10) UNSIGNED NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_layered_filter`
--

INSERT INTO `ps_layered_filter` (`id_layered_filter`, `name`, `filters`, `n_categories`, `date_add`) VALUES
(1, 'MĂłj szablon 2018-12-03', 'a:13:{s:10:\"categories\";a:6:{i:0;i:2;i:3;i:5;i:4;i:7;i:6;i:9;i:7;i:10;i:8;i:11;}s:9:\"shop_list\";a:1:{i:1;i:1;}s:31:\"layered_selection_subcategories\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:22:\"layered_selection_ag_1\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:22:\"layered_selection_ag_3\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:24:\"layered_selection_feat_5\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:24:\"layered_selection_feat_6\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:24:\"layered_selection_feat_7\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:23:\"layered_selection_stock\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:30:\"layered_selection_manufacturer\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:27:\"layered_selection_condition\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:31:\"layered_selection_weight_slider\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:30:\"layered_selection_price_slider\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}}', 9, '2018-12-03 16:18:27');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_layered_filter_shop`
--

DROP TABLE IF EXISTS `ps_layered_filter_shop`;
CREATE TABLE `ps_layered_filter_shop` (
  `id_layered_filter` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_layered_filter_shop`
--

INSERT INTO `ps_layered_filter_shop` (`id_layered_filter`, `id_shop`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_layered_friendly_url`
--

DROP TABLE IF EXISTS `ps_layered_friendly_url`;
CREATE TABLE `ps_layered_friendly_url` (
  `id_layered_friendly_url` int(11) NOT NULL,
  `url_key` varchar(32) NOT NULL,
  `data` varchar(200) NOT NULL,
  `id_lang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_layered_friendly_url`
--

INSERT INTO `ps_layered_friendly_url` (`id_layered_friendly_url`, `url_key`, `data`, `id_lang`) VALUES
(1, '893cdab503f8d250e7c83082c3c41f49', 'a:1:{s:8:\"category\";a:1:{i:1;s:1:\"1\";}}', 1),
(2, '249fbd517206c325c31f5c37d0a504b1', 'a:1:{s:8:\"category\";a:1:{i:7;s:1:\"7\";}}', 1),
(3, 'b923a0c4096a52fd26cbb65587e7b3de', 'a:1:{s:8:\"category\";a:1:{i:9;s:1:\"9\";}}', 1),
(4, '5bbae538124c2c9db20fc31b436ce9de', 'a:1:{s:8:\"category\";a:1:{i:8;s:1:\"8\";}}', 1),
(5, '58d61d397251cab470327e8d0c8f9cf4', 'a:1:{s:8:\"category\";a:1:{i:10;s:2:\"10\";}}', 1),
(6, 'c4b55909a4cecc9d596a5407e20d7bbb', 'a:1:{s:8:\"category\";a:1:{i:2;s:1:\"2\";}}', 1),
(7, 'a4d6a1a51ba848bca78cb8d0da0942de', 'a:1:{s:8:\"category\";a:1:{i:11;s:2:\"11\";}}', 1),
(8, '16956bc8263d8eb0d95d081d427030f9', 'a:1:{s:8:\"category\";a:1:{i:5;s:1:\"5\";}}', 1),
(9, '2b0769b78490b536de784d99b8507c3b', 'a:1:{s:8:\"category\";a:1:{i:4;s:1:\"4\";}}', 1),
(10, '6f4a4923e92fa77baa1be09e18575a8c', 'a:1:{s:8:\"category\";a:1:{i:3;s:1:\"3\";}}', 1),
(11, 'c4d7335317f2f1ba381e038fb625d918', 'a:1:{s:10:\"id_feature\";a:1:{i:1;s:3:\"5_1\";}}', 1),
(12, '18f41c9cab1c150e429f1b670cae3bc1', 'a:1:{s:10:\"id_feature\";a:1:{i:2;s:3:\"5_2\";}}', 1),
(13, '823192a052e44927f06b39b32bcef002', 'a:1:{s:10:\"id_feature\";a:1:{i:3;s:3:\"5_3\";}}', 1),
(14, '905fe5b57eb2e1353911171da4ee7706', 'a:1:{s:10:\"id_feature\";a:1:{i:4;s:3:\"5_4\";}}', 1),
(15, 'ebb42f1bbf0d25b40049c14f1860b952', 'a:1:{s:10:\"id_feature\";a:1:{i:5;s:3:\"5_5\";}}', 1),
(16, 'f9a71edd8befbb99baceadc2b2fbe793', 'a:1:{s:10:\"id_feature\";a:1:{i:6;s:3:\"5_6\";}}', 1),
(17, 'e195459fb3d97a32e94673db75dcf299', 'a:1:{s:10:\"id_feature\";a:1:{i:7;s:3:\"5_7\";}}', 1),
(18, 'b7783cae5eeefc81ff4a69f4ea712ea7', 'a:1:{s:10:\"id_feature\";a:1:{i:8;s:3:\"5_8\";}}', 1),
(19, '45f1d9162a9fe2ffcf9f365eace9eeec', 'a:1:{s:10:\"id_feature\";a:1:{i:9;s:3:\"5_9\";}}', 1),
(20, '7a04872959f09781f3b883a91c5332c7', 'a:1:{s:10:\"id_feature\";a:1:{i:10;s:4:\"6_10\";}}', 1),
(21, '025d11eb379709c8e409a7d712d8e362', 'a:1:{s:10:\"id_feature\";a:1:{i:11;s:4:\"6_11\";}}', 1),
(22, 'e224c427b75f7805c14e8b63ca9e4e0c', 'a:1:{s:10:\"id_feature\";a:1:{i:12;s:4:\"6_12\";}}', 1),
(23, '677717092975926de02151dd9227864e', 'a:1:{s:10:\"id_feature\";a:1:{i:13;s:4:\"6_13\";}}', 1),
(24, '00dff7b63b6f7ddb4b341a9308422730', 'a:1:{s:10:\"id_feature\";a:1:{i:14;s:4:\"6_14\";}}', 1),
(25, 'ff721a9727728b15cd4654a462aaeea0', 'a:1:{s:10:\"id_feature\";a:1:{i:15;s:4:\"6_15\";}}', 1),
(26, '0327a5c6fbcd99ae1fa8ef01f1e7e100', 'a:1:{s:10:\"id_feature\";a:1:{i:16;s:4:\"6_16\";}}', 1),
(27, '58ddd7a988c042c25121ffeb149f3ac7', 'a:1:{s:10:\"id_feature\";a:1:{i:17;s:4:\"7_17\";}}', 1),
(28, 'b7248af6c62c1e54b6f13739739e2d17', 'a:1:{s:10:\"id_feature\";a:1:{i:18;s:4:\"7_18\";}}', 1),
(29, 'b97d201e9d169f46c2a9e6fa356e40d2', 'a:1:{s:10:\"id_feature\";a:1:{i:19;s:4:\"7_19\";}}', 1),
(30, 'de50b73f078d5cde7cc9d8efc61c9e55', 'a:1:{s:10:\"id_feature\";a:1:{i:20;s:4:\"7_20\";}}', 1),
(31, '85a3c64761151fe72e5d027e729072a3', 'a:1:{s:10:\"id_feature\";a:1:{i:21;s:4:\"7_21\";}}', 1),
(32, '97d9dd08827238b39342d37e16ee7fc3', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:1;s:3:\"1_1\";}}', 1),
(33, '2f3d5048a6335cac20241e0f8cb5294e', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:2;s:3:\"1_2\";}}', 1),
(34, '19819345209f29bb2865355fa2cdb800', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:3;s:3:\"1_3\";}}', 1),
(35, '27dd5799da96500f9e0ab61387a556b5', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:4;s:3:\"1_4\";}}', 1),
(36, '6a73ce72468db97129f092fa3d9a0b2e', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:5;s:3:\"3_5\";}}', 1),
(37, 'f1fc935c7d64dfac606eb814dcc6c4a7', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:6;s:3:\"3_6\";}}', 1),
(38, 'f036e061c6e0e9cd6b3c463f72f524a5', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:7;s:3:\"3_7\";}}', 1),
(39, '468a278b79ece55c0ed0d3bd1b2dd01f', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:8;s:3:\"3_8\";}}', 1),
(40, '8996dbd99c9d2240f117ba0d26b39b10', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:9;s:3:\"3_9\";}}', 1),
(41, '601a4dd13077730810f102b18680b537', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:10;s:4:\"3_10\";}}', 1),
(42, '0a68b3ba0819d7126935f51335ef9503', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:11;s:4:\"3_11\";}}', 1),
(43, '5f556205d67d7c26c2726dba638c2d95', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:12;s:4:\"3_12\";}}', 1),
(44, '4b4bb79b20455e8047c972f9ca69cd72', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:13;s:4:\"3_13\";}}', 1),
(45, '54dd539ce8bbf02b44485941f2d8d80b', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:14;s:4:\"3_14\";}}', 1),
(46, '73b845a28e9ced9709fa414f9b97dae9', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:15;s:4:\"3_15\";}}', 1),
(47, 'be50cfae4c360fdb124af017a4e80905', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:16;s:4:\"3_16\";}}', 1),
(48, '4c4550abfc4eec4c91e558fa9b5171c9', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:17;s:4:\"3_17\";}}', 1),
(49, 'ab223cc0ca7ebf34af71e067556ee2aa', 'a:1:{s:18:\"id_attribute_group\";a:1:{i:24;s:4:\"3_24\";}}', 1),
(50, 'c790fd3ae59ce38c295df07ee29b0610', 'a:1:{s:8:\"quantity\";a:1:{i:0;i:0;}}', 1),
(51, '758bf65a9ada0bd5e558e9e034151973', 'a:1:{s:8:\"quantity\";a:1:{i:0;i:1;}}', 1),
(52, 'e2adbf1f81f5c27c9ba016e65baa829e', 'a:1:{s:9:\"condition\";a:1:{s:3:\"new\";s:3:\"new\";}}', 1),
(53, '57c65375e6338ee1bcaca22222fe7fb6', 'a:1:{s:9:\"condition\";a:1:{s:4:\"used\";s:4:\"used\";}}', 1),
(54, '3a46cfaa03a44684be4c937340ab573b', 'a:1:{s:9:\"condition\";a:1:{s:11:\"refurbished\";s:11:\"refurbished\";}}', 1),
(55, 'b4e4a600eb535f83c1a6ddc323d28814', 'a:1:{s:12:\"manufacturer\";a:1:{i:1;s:1:\"1\";}}', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_layered_indexable_attribute_group`
--

DROP TABLE IF EXISTS `ps_layered_indexable_attribute_group`;
CREATE TABLE `ps_layered_indexable_attribute_group` (
  `id_attribute_group` int(11) NOT NULL,
  `indexable` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_layered_indexable_attribute_group_lang_value`
--

DROP TABLE IF EXISTS `ps_layered_indexable_attribute_group_lang_value`;
CREATE TABLE `ps_layered_indexable_attribute_group_lang_value` (
  `id_attribute_group` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `url_name` varchar(128) DEFAULT NULL,
  `meta_title` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_layered_indexable_attribute_lang_value`
--

DROP TABLE IF EXISTS `ps_layered_indexable_attribute_lang_value`;
CREATE TABLE `ps_layered_indexable_attribute_lang_value` (
  `id_attribute` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `url_name` varchar(128) DEFAULT NULL,
  `meta_title` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_layered_indexable_feature`
--

DROP TABLE IF EXISTS `ps_layered_indexable_feature`;
CREATE TABLE `ps_layered_indexable_feature` (
  `id_feature` int(11) NOT NULL,
  `indexable` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_layered_indexable_feature_lang_value`
--

DROP TABLE IF EXISTS `ps_layered_indexable_feature_lang_value`;
CREATE TABLE `ps_layered_indexable_feature_lang_value` (
  `id_feature` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `url_name` varchar(128) NOT NULL,
  `meta_title` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_layered_indexable_feature_value_lang_value`
--

DROP TABLE IF EXISTS `ps_layered_indexable_feature_value_lang_value`;
CREATE TABLE `ps_layered_indexable_feature_value_lang_value` (
  `id_feature_value` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `url_name` varchar(128) DEFAULT NULL,
  `meta_title` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_layered_price_index`
--

DROP TABLE IF EXISTS `ps_layered_price_index`;
CREATE TABLE `ps_layered_price_index` (
  `id_product` int(11) NOT NULL,
  `id_currency` int(11) NOT NULL,
  `id_shop` int(11) NOT NULL,
  `price_min` int(11) NOT NULL,
  `price_max` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_layered_price_index`
--

INSERT INTO `ps_layered_price_index` (`id_product`, `id_currency`, `id_shop`, `price_min`, `price_max`) VALUES
(1, 1, 1, 10031, 12339),
(2, 1, 1, 9039, 11118),
(3, 1, 1, 13611, 16742),
(4, 1, 1, 12867, 15827),
(5, 1, 1, 14942, 18379),
(6, 1, 1, 1894, 2330),
(7, 1, 1, 1691, 2080),
(8, 1, 1, 14830, 18242),
(9, 1, 1, 9621, 11835),
(10, 1, 1, 10047, 12359),
(11, 1, 1, 11404, 14027),
(12, 1, 1, 8575, 10548),
(13, 1, 1, 15338, 18866),
(14, 1, 1, 14698, 18079),
(15, 1, 1, 10340, 12719),
(16, 1, 1, 12584, 15479),
(17, 1, 1, 9447, 11621),
(18, 1, 1, 13726, 16883),
(19, 1, 1, 2321, 2856),
(20, 1, 1, 2308, 2840),
(21, 1, 1, 2074, 2552),
(22, 1, 1, 11599, 14267),
(23, 1, 1, 10486, 12898),
(24, 1, 1, 9617, 11829),
(25, 1, 1, 8591, 10568),
(26, 1, 1, 13175, 16206),
(27, 1, 1, 13086, 16097),
(28, 1, 1, 8152, 10027),
(29, 1, 1, 8508, 10466),
(30, 1, 1, 8329, 10245),
(31, 1, 1, 11714, 14409),
(32, 1, 1, 9082, 11171),
(33, 1, 1, 15018, 18473),
(34, 1, 1, 15765, 19392),
(35, 1, 1, 13185, 16218),
(36, 1, 1, 2160, 2657),
(37, 1, 1, 1958, 2409),
(38, 1, 1, 15014, 18468),
(39, 1, 1, 1460, 1796),
(40, 1, 1, 6145, 7559),
(41, 1, 1, 11667, 14351),
(42, 1, 1, 14976, 18421),
(43, 1, 1, 830, 1021),
(44, 1, 1, 1676, 2062),
(45, 1, 1, 8937, 10993),
(46, 1, 1, 14998, 18448),
(47, 1, 1, 8234, 10129),
(48, 1, 1, 9773, 12021),
(49, 1, 1, 14499, 17834),
(50, 1, 1, 8656, 10647),
(51, 1, 1, 14660, 18032),
(52, 1, 1, 12281, 15106),
(53, 1, 1, 14773, 18172),
(54, 1, 1, 14230, 17504),
(55, 1, 1, 12669, 15584),
(56, 1, 1, 11117, 13674),
(57, 1, 1, 2279, 2804),
(58, 1, 1, 47495, 58419),
(59, 1, 1, 12219, 15030),
(60, 1, 1, 10573, 13005),
(61, 1, 1, 15453, 19008),
(62, 1, 1, 14412, 17727),
(63, 1, 1, 4069, 5005),
(64, 1, 1, 14536, 17880),
(65, 1, 1, 9786, 12037),
(66, 1, 1, 1890, 2325),
(67, 1, 1, 26739, 32890),
(68, 1, 1, 1993, 2452),
(69, 1, 1, 11301, 13901),
(70, 1, 1, 4778, 5877),
(71, 1, 1, 45843, 56388),
(72, 1, 1, 15325, 18850),
(73, 1, 1, 11793, 14506),
(74, 1, 1, 10205, 12553),
(75, 1, 1, 12769, 15706),
(76, 1, 1, 10286, 12653),
(77, 1, 1, 16191, 19915),
(78, 1, 1, 1725, 2122),
(79, 1, 1, 10945, 13463),
(80, 1, 1, 14030, 17257),
(81, 1, 1, 10840, 13334),
(82, 1, 1, 13602, 16731),
(83, 1, 1, 1880, 2313),
(84, 1, 1, 14999, 18449),
(85, 1, 1, 1059, 1303),
(86, 1, 1, 10383, 12772),
(87, 1, 1, 1256, 1546),
(88, 1, 1, 8509, 10467),
(89, 1, 1, 2356, 2899),
(90, 1, 1, 11585, 14250),
(91, 1, 1, 10097, 12420),
(92, 1, 1, 9296, 11435),
(93, 1, 1, 14930, 18365),
(94, 1, 1, 15265, 18777),
(95, 1, 1, 14563, 17913),
(96, 1, 1, 55311, 68033),
(97, 1, 1, 79652, 97973),
(98, 1, 1, 2010, 2473),
(99, 1, 1, 44603, 54862),
(100, 1, 1, 1212, 1491),
(101, 1, 1, 11635, 14312),
(102, 1, 1, 12107, 14892),
(103, 1, 1, 15838, 19481),
(104, 1, 1, 8211, 10100),
(105, 1, 1, 14586, 17941),
(106, 1, 1, 11246, 13833),
(107, 1, 1, 10839, 13333),
(108, 1, 1, 11965, 14718),
(109, 1, 1, 13082, 16091),
(110, 1, 1, 15723, 19340),
(111, 1, 1, 8895, 10942),
(112, 1, 1, 10988, 13516),
(113, 1, 1, 12000, 14760),
(114, 1, 1, 14679, 18056),
(115, 1, 1, 904, 1112),
(116, 1, 1, 1442, 1774),
(117, 1, 1, 10013, 12317),
(118, 1, 1, 2070, 2547),
(119, 1, 1, 1725, 2122),
(120, 1, 1, 5832, 7174),
(121, 1, 1, 12900, 15867),
(122, 1, 1, 5200, 6396),
(123, 1, 1, 2295, 2824),
(124, 1, 1, 12206, 15014),
(125, 1, 1, 46217, 56848),
(126, 1, 1, 34074, 41912),
(127, 1, 1, 10820, 13309),
(128, 1, 1, 15082, 18551),
(129, 1, 1, 16230, 19963),
(130, 1, 1, 11266, 13858),
(131, 1, 1, 8273, 10177),
(132, 1, 1, 2152, 2648),
(133, 1, 1, 14614, 17976),
(134, 1, 1, 14599, 17957),
(135, 1, 1, 36744, 45196),
(136, 1, 1, 988, 1216),
(137, 1, 1, 12844, 15799),
(138, 1, 1, 15030, 18487),
(139, 1, 1, 31769, 39076),
(140, 1, 1, 1281, 1576),
(141, 1, 1, 8856, 10894),
(142, 1, 1, 946, 1164),
(143, 1, 1, 8615, 10597),
(144, 1, 1, 33690, 41439),
(145, 1, 1, 14494, 17828),
(146, 1, 1, 12421, 15279),
(147, 1, 1, 1756, 2161),
(148, 1, 1, 11674, 14360),
(149, 1, 1, 2396, 2948),
(150, 1, 1, 11619, 14292),
(151, 1, 1, 12434, 15295),
(152, 1, 1, 13311, 16373),
(153, 1, 1, 16034, 19723),
(154, 1, 1, 9687, 11916),
(155, 1, 1, 1126, 1385),
(156, 1, 1, 9646, 11865),
(157, 1, 1, 10668, 13122),
(158, 1, 1, 4250, 5228),
(159, 1, 1, 14363, 17667),
(160, 1, 1, 16079, 19778),
(161, 1, 1, 11802, 14517),
(162, 1, 1, 14268, 17550),
(163, 1, 1, 2243, 2760),
(164, 1, 1, 1866, 2296),
(165, 1, 1, 16172, 19892),
(166, 1, 1, 8801, 10826),
(167, 1, 1, 28109, 34575),
(168, 1, 1, 8160, 10037),
(169, 1, 1, 14811, 18218),
(170, 1, 1, 2354, 2896),
(171, 1, 1, 9451, 11625),
(172, 1, 1, 2438, 2999),
(173, 1, 1, 1854, 2281),
(174, 1, 1, 11274, 13868),
(175, 1, 1, 4669, 5743),
(176, 1, 1, 10156, 12492),
(177, 1, 1, 10496, 12911),
(178, 1, 1, 8873, 10914),
(179, 1, 1, 11774, 14483),
(180, 1, 1, 1236, 1521),
(181, 1, 1, 2059, 2533),
(182, 1, 1, 14413, 17729),
(183, 1, 1, 11745, 14447),
(184, 1, 1, 14527, 17869),
(185, 1, 1, 8320, 10234),
(186, 1, 1, 15803, 19438),
(187, 1, 1, 9253, 11382),
(188, 1, 1, 12073, 14851),
(189, 1, 1, 11082, 13631),
(190, 1, 1, 14917, 18348),
(191, 1, 1, 11771, 14479),
(192, 1, 1, 8911, 10961),
(193, 1, 1, 2152, 2648),
(194, 1, 1, 1892, 2328),
(195, 1, 1, 41883, 51517),
(196, 1, 1, 13871, 17062),
(197, 1, 1, 16199, 19925),
(198, 1, 1, 11595, 14262),
(199, 1, 1, 2000, 2460),
(200, 1, 1, 1804, 2219),
(201, 1, 1, 10565, 12996),
(202, 1, 1, 9007, 11079),
(203, 1, 1, 15576, 19159),
(204, 1, 1, 11578, 14241),
(205, 1, 1, 10529, 12951),
(206, 1, 1, 10695, 13155),
(207, 1, 1, 15386, 18925),
(208, 1, 1, 14557, 17906),
(209, 1, 1, 15030, 18488),
(210, 1, 1, 2297, 2826),
(211, 1, 1, 16073, 19771),
(212, 1, 1, 6774, 8333),
(213, 1, 1, 15470, 19029),
(214, 1, 1, 2303, 2833),
(215, 1, 1, 14091, 17332),
(216, 1, 1, 9256, 11385),
(217, 1, 1, 15513, 19082),
(218, 1, 1, 8947, 11006),
(219, 1, 1, 11023, 13559),
(220, 1, 1, 12634, 15540),
(221, 1, 1, 59460, 73137),
(222, 1, 1, 1294, 1592),
(223, 1, 1, 1643, 2022),
(224, 1, 1, 2009, 2472),
(225, 1, 1, 25817, 31755),
(226, 1, 1, 63402, 77985),
(227, 1, 1, 29800, 36655),
(228, 1, 1, 13593, 16720),
(229, 1, 1, 11933, 14678),
(230, 1, 1, 1684, 2072),
(231, 1, 1, 2033, 2501),
(232, 1, 1, 2113, 2599),
(233, 1, 1, 47478, 58398),
(234, 1, 1, 14817, 18225),
(235, 1, 1, 13428, 16517),
(236, 1, 1, 2330, 2866),
(237, 1, 1, 2184, 2687),
(238, 1, 1, 12806, 15752),
(239, 1, 1, 2290, 2817),
(240, 1, 1, 1752, 2155),
(241, 1, 1, 9766, 12013),
(242, 1, 1, 1239, 1525),
(243, 1, 1, 1424, 1752),
(244, 1, 1, 9308, 11449),
(245, 1, 1, 5504, 6770),
(246, 1, 1, 28300, 34810),
(247, 1, 1, 13780, 16950),
(248, 1, 1, 2400, 2953),
(249, 1, 1, 1978, 2434),
(250, 1, 1, 10199, 12545),
(251, 1, 1, 13282, 16338),
(252, 1, 1, 13811, 16988),
(253, 1, 1, 8215, 10105),
(254, 1, 1, 15608, 19199),
(255, 1, 1, 45178, 55569),
(256, 1, 1, 8246, 10143),
(257, 1, 1, 15900, 19557),
(258, 1, 1, 11715, 14410),
(259, 1, 1, 9008, 11080),
(260, 1, 1, 13760, 16925),
(261, 1, 1, 10348, 12729),
(262, 1, 1, 12718, 15644),
(263, 1, 1, 10932, 13447),
(264, 1, 1, 10681, 13138),
(265, 1, 1, 8973, 11038),
(266, 1, 1, 8356, 10279),
(267, 1, 1, 2019, 2484),
(268, 1, 1, 12878, 15840),
(269, 1, 1, 14322, 17617),
(270, 1, 1, 8209, 10098),
(271, 1, 1, 8570, 10542),
(272, 1, 1, 12984, 15971),
(273, 1, 1, 11764, 14470),
(274, 1, 1, 12057, 14831),
(275, 1, 1, 850, 1046),
(276, 1, 1, 1234, 1518),
(277, 1, 1, 2309, 2841),
(278, 1, 1, 1724, 2121),
(279, 1, 1, 8409, 10344),
(280, 1, 1, 9317, 11461),
(281, 1, 1, 8541, 10506),
(282, 1, 1, 14178, 17439),
(283, 1, 1, 14506, 17843),
(284, 1, 1, 8528, 10490),
(285, 1, 1, 2178, 2680),
(286, 1, 1, 13969, 17182),
(287, 1, 1, 15169, 18658),
(288, 1, 1, 57658, 70920),
(289, 1, 1, 1791, 2203),
(290, 1, 1, 71026, 87362),
(291, 1, 1, 8742, 10753),
(292, 1, 1, 8484, 10436),
(293, 1, 1, 10710, 13174),
(294, 1, 1, 9954, 12244),
(295, 1, 1, 1932, 2377),
(296, 1, 1, 13393, 16474),
(297, 1, 1, 10261, 12622),
(298, 1, 1, 12221, 15032),
(299, 1, 1, 8850, 10886),
(300, 1, 1, 2039, 2509),
(301, 1, 1, 4544, 5590),
(302, 1, 1, 5911, 7271),
(303, 1, 1, 68523, 84284),
(304, 1, 1, 15491, 19055),
(305, 1, 1, 14982, 18428),
(306, 1, 1, 8134, 10006),
(307, 1, 1, 13642, 16780),
(308, 1, 1, 12564, 15454),
(309, 1, 1, 14200, 17467),
(310, 1, 1, 14752, 18146),
(311, 1, 1, 13452, 16546),
(312, 1, 1, 1332, 1639),
(313, 1, 1, 11787, 14499),
(314, 1, 1, 2065, 2541),
(315, 1, 1, 2349, 2890),
(316, 1, 1, 2236, 2751),
(317, 1, 1, 1908, 2347),
(318, 1, 1, 2431, 2991),
(319, 1, 1, 15479, 19040),
(320, 1, 1, 14638, 18005),
(321, 1, 1, 8832, 10864),
(322, 1, 1, 16139, 19852),
(323, 1, 1, 10495, 12910),
(324, 1, 1, 1662, 2045),
(325, 1, 1, 1797, 2211),
(326, 1, 1, 1637, 2014),
(327, 1, 1, 2284, 2810),
(328, 1, 1, 12175, 14976),
(329, 1, 1, 9538, 11732),
(330, 1, 1, 6226, 7658),
(331, 1, 1, 1607, 1977),
(332, 1, 1, 10691, 13151),
(333, 1, 1, 1888, 2323),
(334, 1, 1, 5621, 6915),
(335, 1, 1, 8982, 11048),
(336, 1, 1, 13943, 17150),
(337, 1, 1, 15525, 19096),
(338, 1, 1, 70302, 86472),
(339, 1, 1, 2366, 2911),
(340, 1, 1, 15494, 19058),
(341, 1, 1, 14474, 17804),
(342, 1, 1, 14932, 18367),
(343, 1, 1, 11734, 14434),
(344, 1, 1, 1208, 1486),
(345, 1, 1, 1732, 2131),
(346, 1, 1, 2016, 2480),
(347, 1, 1, 1952, 2402),
(348, 1, 1, 2098, 2581),
(349, 1, 1, 1704, 2096),
(350, 1, 1, 1947, 2395),
(351, 1, 1, 14327, 17623),
(352, 1, 1, 15243, 18749),
(353, 1, 1, 13863, 17052),
(354, 1, 1, 10659, 13111),
(355, 1, 1, 1830, 2251),
(356, 1, 1, 13435, 16526),
(357, 1, 1, 10481, 12892),
(358, 1, 1, 13904, 17103),
(359, 1, 1, 2395, 2947),
(360, 1, 1, 1867, 2297),
(361, 1, 1, 1981, 2437),
(362, 1, 1, 15568, 19149),
(363, 1, 1, 6051, 7443),
(364, 1, 1, 12103, 14887),
(365, 1, 1, 1351, 1662),
(366, 1, 1, 1821, 2240),
(367, 1, 1, 9926, 12210),
(368, 1, 1, 12026, 14793),
(369, 1, 1, 13534, 16647),
(370, 1, 1, 8554, 10522),
(371, 1, 1, 14589, 17945),
(372, 1, 1, 9023, 11099),
(373, 1, 1, 14877, 18299),
(374, 1, 1, 13452, 16546),
(375, 1, 1, 10510, 12928),
(376, 1, 1, 12079, 14858),
(377, 1, 1, 9947, 12235),
(378, 1, 1, 9485, 11667),
(379, 1, 1, 15148, 18633),
(380, 1, 1, 2065, 2540),
(381, 1, 1, 15405, 18949),
(382, 1, 1, 15738, 19358);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_layered_product_attribute`
--

DROP TABLE IF EXISTS `ps_layered_product_attribute`;
CREATE TABLE `ps_layered_product_attribute` (
  `id_attribute` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_attribute_group` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_linksmenutop`
--

DROP TABLE IF EXISTS `ps_linksmenutop`;
CREATE TABLE `ps_linksmenutop` (
  `id_linksmenutop` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  `new_window` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_linksmenutop_lang`
--

DROP TABLE IF EXISTS `ps_linksmenutop_lang`;
CREATE TABLE `ps_linksmenutop_lang` (
  `id_linksmenutop` int(11) UNSIGNED NOT NULL,
  `id_lang` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  `label` varchar(128) NOT NULL,
  `link` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_log`
--

DROP TABLE IF EXISTS `ps_log`;
CREATE TABLE `ps_log` (
  `id_log` int(10) UNSIGNED NOT NULL,
  `severity` tinyint(1) NOT NULL,
  `error_code` int(11) DEFAULT NULL,
  `message` text NOT NULL,
  `object_type` varchar(32) DEFAULT NULL,
  `object_id` int(10) UNSIGNED DEFAULT NULL,
  `id_employee` int(10) UNSIGNED DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_log`
--

INSERT INTO `ps_log` (`id_log`, `severity`, `error_code`, `message`, `object_type`, `object_id`, `id_employee`, `date_add`, `date_upd`) VALUES
(1, 1, 0, 'Połączenie z panelem administracyjnym z 172.20.83.254', '', 0, 1, '2018-12-03 22:12:25', '2018-12-03 22:12:25'),
(2, 1, 0, 'dodanie Employee', 'Employee', 2, 1, '2018-12-03 22:14:24', '2018-12-03 22:14:24'),
(3, 1, 0, 'Połączenie z panelem administracyjnym z 172.20.83.254', '', 0, 2, '2018-12-03 22:20:22', '2018-12-03 22:20:22'),
(4, 1, 0, 'Połączenie z panelem administracyjnym z 172.20.83.254', '', 0, 2, '2018-12-03 23:07:51', '2018-12-03 23:07:51'),
(5, 1, 0, 'Połączenie z panelem administracyjnym z 172.20.83.254', '', 0, 2, '2018-12-03 23:11:36', '2018-12-03 23:11:36'),
(6, 1, 0, 'Product modyfikacja', 'Product', 1, 2, '2018-12-03 23:12:41', '2018-12-03 23:12:41'),
(7, 1, 0, 'Language modyfikacja', 'Language', 1, 2, '2018-12-03 23:19:26', '2018-12-03 23:19:26'),
(8, 1, 0, 'usunięcie Category', 'Category', 4, 2, '2018-12-03 23:46:02', '2018-12-03 23:46:02'),
(9, 1, 0, 'usunięcie Category', 'Category', 8, 2, '2018-12-03 23:46:02', '2018-12-03 23:46:02'),
(10, 1, 0, 'usunięcie Feature', 'Feature', 1, 2, '2018-12-03 23:46:21', '2018-12-03 23:46:21'),
(11, 1, 0, 'usunięcie Feature', 'Feature', 2, 2, '2018-12-03 23:46:21', '2018-12-03 23:46:21'),
(12, 1, 0, 'usunięcie Feature', 'Feature', 3, 2, '2018-12-03 23:46:21', '2018-12-03 23:46:21'),
(13, 1, 0, 'usunięcie Feature', 'Feature', 4, 2, '2018-12-03 23:46:21', '2018-12-03 23:46:21'),
(14, 1, 0, 'usunięcie Feature', 'Feature', 5, 2, '2018-12-03 23:46:21', '2018-12-03 23:46:21'),
(15, 1, 0, 'usunięcie Feature', 'Feature', 6, 2, '2018-12-03 23:46:21', '2018-12-03 23:46:21'),
(16, 1, 0, 'usunięcie Feature', 'Feature', 7, 2, '2018-12-03 23:46:21', '2018-12-03 23:46:21'),
(17, 1, 0, 'usunięcie Address', 'Address', 3, 2, '2018-12-03 23:46:31', '2018-12-03 23:46:31'),
(18, 1, 0, 'usunięcie Supplier', 'Supplier', 1, 2, '2018-12-03 23:46:40', '2018-12-03 23:46:40'),
(19, 1, 0, 'import Kategorie', '', 0, 2, '2018-12-03 23:48:31', '2018-12-03 23:48:31'),
(20, 1, 0, 'import Produkty z zaokrągleniem', '', 0, 2, '2018-12-03 23:54:24', '2018-12-03 23:54:24'),
(21, 1, 0, 'Currency modyfikacja', 'Currency', 1, 2, '2018-12-04 00:04:50', '2018-12-04 00:04:50'),
(22, 1, 0, 'Połączenie z panelem administracyjnym z 172.20.83.254', '', 0, 2, '2018-12-04 00:21:45', '2018-12-04 00:21:45'),
(23, 1, 0, 'usunięcie Category', 'Category', 3, 2, '2018-12-04 00:23:16', '2018-12-04 00:23:16'),
(24, 1, 0, 'usunięcie Alias', 'Alias', 1, 2, '2018-12-04 00:29:20', '2018-12-04 00:29:20'),
(25, 1, 0, 'usunięcie Alias', 'Alias', 2, 2, '2018-12-04 00:29:20', '2018-12-04 00:29:20'),
(26, 1, 0, 'Połączenie z panelem administracyjnym z 172.20.83.254', '', 0, 2, '2018-12-04 01:03:01', '2018-12-04 01:03:01'),
(27, 1, 0, 'Połączenie z panelem administracyjnym z 172.20.83.254', '', 0, 2, '2018-12-04 01:23:45', '2018-12-04 01:23:45'),
(28, 1, 0, 'Frontcontroller::init - Cart cannot be loaded or an order has already been placed using this cart', 'Cart', 8, 0, '2018-12-04 12:11:51', '2018-12-04 12:11:51'),
(29, 1, 0, 'Połączenie z panelem administracyjnym z 153.19.54.30', '', 0, 2, '2018-12-04 12:12:30', '2018-12-04 12:12:30');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_mail`
--

DROP TABLE IF EXISTS `ps_mail`;
CREATE TABLE `ps_mail` (
  `id_mail` int(11) UNSIGNED NOT NULL,
  `recipient` varchar(126) NOT NULL,
  `template` varchar(62) NOT NULL,
  `subject` varchar(254) NOT NULL,
  `id_lang` int(11) UNSIGNED NOT NULL,
  `date_add` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_manufacturer`
--

DROP TABLE IF EXISTS `ps_manufacturer`;
CREATE TABLE `ps_manufacturer` (
  `id_manufacturer` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_manufacturer`
--

INSERT INTO `ps_manufacturer` (`id_manufacturer`, `name`, `date_add`, `date_upd`, `active`) VALUES
(1, 'Fashion Manufacturer', '2018-12-03 17:18:12', '2018-12-03 17:18:12', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_manufacturer_lang`
--

DROP TABLE IF EXISTS `ps_manufacturer_lang`;
CREATE TABLE `ps_manufacturer_lang` (
  `id_manufacturer` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `description` text DEFAULT NULL,
  `short_description` text DEFAULT NULL,
  `meta_title` varchar(128) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_manufacturer_lang`
--

INSERT INTO `ps_manufacturer_lang` (`id_manufacturer`, `id_lang`, `description`, `short_description`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
(1, 1, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_manufacturer_shop`
--

DROP TABLE IF EXISTS `ps_manufacturer_shop`;
CREATE TABLE `ps_manufacturer_shop` (
  `id_manufacturer` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_manufacturer_shop`
--

INSERT INTO `ps_manufacturer_shop` (`id_manufacturer`, `id_shop`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_memcached_servers`
--

DROP TABLE IF EXISTS `ps_memcached_servers`;
CREATE TABLE `ps_memcached_servers` (
  `id_memcached_server` int(11) UNSIGNED NOT NULL,
  `ip` varchar(254) NOT NULL,
  `port` int(11) UNSIGNED NOT NULL,
  `weight` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_message`
--

DROP TABLE IF EXISTS `ps_message`;
CREATE TABLE `ps_message` (
  `id_message` int(10) UNSIGNED NOT NULL,
  `id_cart` int(10) UNSIGNED DEFAULT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_employee` int(10) UNSIGNED DEFAULT NULL,
  `id_order` int(10) UNSIGNED NOT NULL,
  `message` text NOT NULL,
  `private` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_message_readed`
--

DROP TABLE IF EXISTS `ps_message_readed`;
CREATE TABLE `ps_message_readed` (
  `id_message` int(10) UNSIGNED NOT NULL,
  `id_employee` int(10) UNSIGNED NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_meta`
--

DROP TABLE IF EXISTS `ps_meta`;
CREATE TABLE `ps_meta` (
  `id_meta` int(10) UNSIGNED NOT NULL,
  `page` varchar(64) NOT NULL,
  `configurable` tinyint(1) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_meta`
--

INSERT INTO `ps_meta` (`id_meta`, `page`, `configurable`) VALUES
(1, 'pagenotfound', 1),
(2, 'best-sales', 1),
(3, 'contact', 1),
(4, 'index', 1),
(5, 'manufacturer', 1),
(6, 'new-products', 1),
(7, 'password', 1),
(8, 'prices-drop', 1),
(9, 'sitemap', 1),
(10, 'supplier', 1),
(11, 'address', 1),
(12, 'addresses', 1),
(13, 'authentication', 1),
(14, 'cart', 1),
(15, 'discount', 1),
(16, 'history', 1),
(17, 'identity', 1),
(18, 'my-account', 1),
(19, 'order-follow', 1),
(20, 'order-slip', 1),
(21, 'order', 1),
(22, 'search', 1),
(23, 'stores', 1),
(24, 'order-opc', 1),
(25, 'guest-tracking', 1),
(26, 'order-confirmation', 1),
(27, 'product', 0),
(28, 'category', 0),
(29, 'cms', 0),
(30, 'module-cheque-payment', 0),
(31, 'module-cheque-validation', 0),
(32, 'module-bankwire-validation', 0),
(33, 'module-bankwire-payment', 0),
(34, 'module-cashondelivery-validation', 0),
(35, 'products-comparison', 1),
(36, 'module-blocknewsletter-verification', 1),
(37, 'module-cronjobs-callback', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_meta_lang`
--

DROP TABLE IF EXISTS `ps_meta_lang`;
CREATE TABLE `ps_meta_lang` (
  `id_meta` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT 1,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `title` varchar(128) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `url_rewrite` varchar(254) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_meta_lang`
--

INSERT INTO `ps_meta_lang` (`id_meta`, `id_shop`, `id_lang`, `title`, `description`, `keywords`, `url_rewrite`) VALUES
(1, 1, 1, 'BĹÄd 404', 'Nie moĹźna odnaleĹşÄ strony', 'bĹÄd, 404, nie znaleziono ', 'nie-znaleziono-strony'),
(2, 1, 1, 'NajczÄĹciej kupowane', 'Nasze najlepsze sprzedaĹźe', 'NajczÄĹciej kupowane', 'najczesciej-kupowane'),
(3, 1, 1, 'Skontaktuj siÄ z nami', 'Skorzystaj z formularza kontaktowego', 'kontakt,e-mail', 'kontakt'),
(4, 1, 1, '', 'Sklep na oprogramowaniu PrestaShop', 'sklep, prestashop', ''),
(5, 1, 1, 'Producenci', 'Lista producentĂłw', 'producent', 'producenci'),
(6, 1, 1, 'Nowe produkty', 'Nasze nowe produkty', 'nowe, produkty', 'nowe-produkty'),
(7, 1, 1, 'Przypomnienie hasĹa', 'Wpisz swĂłj adres e-mail w celu uzyskania nowego hasĹa', 'przypomnienie, hasĹo, e-mail, nowy', 'odzyskiwanie-hasla'),
(8, 1, 1, 'Promocje', 'Produkty w promocji', 'promocje, specjalne, spadek ceny', 'promocje'),
(9, 1, 1, 'Mapa strony', 'ZagubiĹeĹ siÄ? ZnajdĹş to, czego szukasz!', 'mapa strony', 'mapa-strony'),
(10, 1, 1, 'Dostawcy', 'Lista dostawcĂłw', 'dostawca', 'dostawcy'),
(11, 1, 1, 'Adres', '', '', 'adres'),
(12, 1, 1, 'Adresy', '', '', 'adresy'),
(13, 1, 1, 'Logowanie', '', '', 'logowanie'),
(14, 1, 1, 'Koszyk', '', '', 'koszyk'),
(15, 1, 1, 'Rabaty', '', '', 'rabaty'),
(16, 1, 1, 'Historia zamĂłwieĹ', '', '', 'historia-zamowien'),
(17, 1, 1, 'Dane osobiste', '', '', 'dane-osobiste'),
(18, 1, 1, 'Moje konto', '', '', 'moje-konto'),
(19, 1, 1, 'Ĺledzenie zamĂłwienia', '', '', 'sledzenie-zamowienia'),
(20, 1, 1, 'Pokwitowania', '', '', 'pokwitowania'),
(21, 1, 1, 'ZamĂłwienie', '', '', 'zamowienie'),
(22, 1, 1, 'Szukaj', '', '', 'szukaj'),
(23, 1, 1, 'sklepy', '', '', 'sklepy'),
(24, 1, 1, 'ZamĂłwienie', '', '', 'szybkie-zakupy'),
(25, 1, 1, 'Ĺledzenie zamĂłwieĹ goĹci', '', '', 'sledzenie-zamowienia-gosc'),
(26, 1, 1, 'Potwierdzenie zamĂłwienia', '', '', 'potwierdzenie-zamowienia'),
(35, 1, 1, 'Products Comparison', '', '', 'products-comparison'),
(36, 1, 1, '', '', '', ''),
(37, 1, 1, '', '', '', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_module`
--

DROP TABLE IF EXISTS `ps_module`;
CREATE TABLE `ps_module` (
  `id_module` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `version` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_module`
--

INSERT INTO `ps_module` (`id_module`, `name`, `active`, `version`) VALUES
(1, 'socialsharing', 1, '1.4.3'),
(2, 'blockbanner', 1, '1.4.1'),
(3, 'bankwire', 1, '1.1.2'),
(4, 'blockbestsellers', 1, '1.8.1'),
(5, 'blockcart', 1, '1.6.2'),
(6, 'blocksocial', 1, '1.2.2'),
(7, 'blockcategories', 1, '2.9.4'),
(8, 'blockcurrencies', 1, '0.4.1'),
(9, 'blockfacebook', 1, '1.4.1'),
(10, 'blocklanguages', 1, '1.5.1'),
(11, 'blocklayered', 1, '2.2.1'),
(12, 'blockcms', 1, '2.1.2'),
(13, 'blockcmsinfo', 1, '1.6.1'),
(14, 'blockcontact', 1, '1.4.1'),
(15, 'blockcontactinfos', 1, '1.2.1'),
(16, 'blockmanufacturer', 1, '1.4.1'),
(17, 'blockmyaccount', 1, '1.4.1'),
(18, 'blockmyaccountfooter', 1, '1.6.1'),
(19, 'blocknewproducts', 1, '1.10.1'),
(20, 'blocknewsletter', 1, '2.4.0'),
(21, 'blockpaymentlogo', 1, '0.4.1'),
(22, 'blocksearch', 1, '1.7.1'),
(23, 'blockspecials', 1, '1.3.1'),
(24, 'blockstore', 1, '1.3.1'),
(25, 'blocksupplier', 1, '1.2.1'),
(26, 'blocktags', 1, '1.3.1'),
(27, 'blocktopmenu', 1, '2.2.4'),
(28, 'blockuserinfo', 1, '0.4.1'),
(29, 'blockviewed', 1, '1.3.1'),
(30, 'cheque', 1, '2.7.2'),
(31, 'dashactivity', 1, '1.0.0'),
(32, 'dashtrends', 1, '1.0.0'),
(33, 'dashgoals', 1, '1.0.0'),
(34, 'dashproducts', 1, '1.0.0'),
(35, 'graphnvd3', 1, '1.5.1'),
(36, 'gridhtml', 1, '1.3.1'),
(37, 'homeslider', 1, '1.6.1'),
(38, 'homefeatured', 1, '1.8.1'),
(39, 'productpaymentlogos', 1, '1.4.1'),
(40, 'pagesnotfound', 1, '1.5.1'),
(41, 'sekeywords', 1, '1.4.1'),
(42, 'statsbestcategories', 1, '1.5.1'),
(43, 'statsbestcustomers', 1, '1.5.1'),
(44, 'statsbestproducts', 1, '1.5.1'),
(45, 'statsbestsuppliers', 1, '1.4.1'),
(46, 'statsbestvouchers', 1, '1.5.1'),
(47, 'statscarrier', 1, '1.4.1'),
(48, 'statscatalog', 1, '1.4.0'),
(49, 'statscheckup', 1, '1.5.0'),
(50, 'statsdata', 1, '1.6.2'),
(51, 'statsequipment', 1, '1.3.1'),
(52, 'statsforecast', 1, '1.4.1'),
(53, 'statslive', 1, '1.3.1'),
(54, 'statsnewsletter', 1, '1.4.2'),
(55, 'statsorigin', 1, '1.4.1'),
(56, 'statspersonalinfos', 1, '1.4.1'),
(57, 'statsproduct', 1, '1.5.1'),
(58, 'statsregistrations', 1, '1.4.1'),
(59, 'statssales', 1, '1.3.1'),
(60, 'statssearch', 1, '1.4.1'),
(61, 'statsstock', 1, '1.6.0'),
(62, 'statsvisits', 1, '1.6.1'),
(63, 'themeconfigurator', 1, '2.1.3'),
(64, 'gamification', 1, '2.2.1'),
(65, 'cronjobs', 1, '1.4.0'),
(66, 'psaddonsconnect', 1, '1.0.1'),
(67, 'ganalytics', 1, '2.3.4');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_modules_perfs`
--

DROP TABLE IF EXISTS `ps_modules_perfs`;
CREATE TABLE `ps_modules_perfs` (
  `id_modules_perfs` int(11) UNSIGNED NOT NULL,
  `session` int(11) UNSIGNED NOT NULL,
  `module` varchar(62) NOT NULL,
  `method` varchar(126) NOT NULL,
  `time_start` double UNSIGNED NOT NULL,
  `time_end` double UNSIGNED NOT NULL,
  `memory_start` int(10) UNSIGNED NOT NULL,
  `memory_end` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_module_access`
--

DROP TABLE IF EXISTS `ps_module_access`;
CREATE TABLE `ps_module_access` (
  `id_profile` int(10) UNSIGNED NOT NULL,
  `id_module` int(10) UNSIGNED NOT NULL,
  `view` tinyint(1) NOT NULL DEFAULT 0,
  `configure` tinyint(1) NOT NULL DEFAULT 0,
  `uninstall` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_module_access`
--

INSERT INTO `ps_module_access` (`id_profile`, `id_module`, `view`, `configure`, `uninstall`) VALUES
(2, 1, 1, 1, 1),
(2, 2, 1, 1, 1),
(2, 3, 1, 1, 1),
(2, 4, 1, 1, 1),
(2, 5, 1, 1, 1),
(2, 6, 1, 1, 1),
(2, 7, 1, 1, 1),
(2, 8, 1, 1, 1),
(2, 9, 1, 1, 1),
(2, 10, 1, 1, 1),
(2, 11, 1, 1, 1),
(2, 12, 1, 1, 1),
(2, 13, 1, 1, 1),
(2, 14, 1, 1, 1),
(2, 15, 1, 1, 1),
(2, 16, 1, 1, 1),
(2, 17, 1, 1, 1),
(2, 18, 1, 1, 1),
(2, 19, 1, 1, 1),
(2, 20, 1, 1, 1),
(2, 21, 1, 1, 1),
(2, 22, 1, 1, 1),
(2, 23, 1, 1, 1),
(2, 24, 1, 1, 1),
(2, 25, 1, 1, 1),
(2, 26, 1, 1, 1),
(2, 27, 1, 1, 1),
(2, 28, 1, 1, 1),
(2, 29, 1, 1, 1),
(2, 30, 1, 1, 1),
(2, 31, 1, 1, 1),
(2, 32, 1, 1, 1),
(2, 33, 1, 1, 1),
(2, 34, 1, 1, 1),
(2, 35, 1, 1, 1),
(2, 36, 1, 1, 1),
(2, 37, 1, 1, 1),
(2, 38, 1, 1, 1),
(2, 39, 1, 1, 1),
(2, 40, 1, 1, 1),
(2, 41, 1, 1, 1),
(2, 42, 1, 1, 1),
(2, 43, 1, 1, 1),
(2, 44, 1, 1, 1),
(2, 45, 1, 1, 1),
(2, 46, 1, 1, 1),
(2, 47, 1, 1, 1),
(2, 48, 1, 1, 1),
(2, 49, 1, 1, 1),
(2, 50, 1, 1, 1),
(2, 51, 1, 1, 1),
(2, 52, 1, 1, 1),
(2, 53, 1, 1, 1),
(2, 54, 1, 1, 1),
(2, 55, 1, 1, 1),
(2, 56, 1, 1, 1),
(2, 57, 1, 1, 1),
(2, 58, 1, 1, 1),
(2, 59, 1, 1, 1),
(2, 60, 1, 1, 1),
(2, 61, 1, 1, 1),
(2, 62, 1, 1, 1),
(2, 63, 1, 1, 1),
(2, 64, 1, 1, 1),
(2, 65, 1, 1, 1),
(2, 66, 1, 1, 1),
(2, 67, 1, 1, 1),
(3, 1, 1, 0, 0),
(3, 2, 1, 0, 0),
(3, 3, 1, 0, 0),
(3, 4, 1, 0, 0),
(3, 5, 1, 0, 0),
(3, 6, 1, 0, 0),
(3, 7, 1, 0, 0),
(3, 8, 1, 0, 0),
(3, 9, 1, 0, 0),
(3, 10, 1, 0, 0),
(3, 11, 1, 0, 0),
(3, 12, 1, 0, 0),
(3, 13, 1, 0, 0),
(3, 14, 1, 0, 0),
(3, 15, 1, 0, 0),
(3, 16, 1, 0, 0),
(3, 17, 1, 0, 0),
(3, 18, 1, 0, 0),
(3, 19, 1, 0, 0),
(3, 20, 1, 0, 0),
(3, 21, 1, 0, 0),
(3, 22, 1, 0, 0),
(3, 23, 1, 0, 0),
(3, 24, 1, 0, 0),
(3, 25, 1, 0, 0),
(3, 26, 1, 0, 0),
(3, 27, 1, 0, 0),
(3, 28, 1, 0, 0),
(3, 29, 1, 0, 0),
(3, 30, 1, 0, 0),
(3, 31, 1, 0, 0),
(3, 32, 1, 0, 0),
(3, 33, 1, 0, 0),
(3, 34, 1, 0, 0),
(3, 35, 1, 0, 0),
(3, 36, 1, 0, 0),
(3, 37, 1, 0, 0),
(3, 38, 1, 0, 0),
(3, 39, 1, 0, 0),
(3, 40, 1, 0, 0),
(3, 41, 1, 0, 0),
(3, 42, 1, 0, 0),
(3, 43, 1, 0, 0),
(3, 44, 1, 0, 0),
(3, 45, 1, 0, 0),
(3, 46, 1, 0, 0),
(3, 47, 1, 0, 0),
(3, 48, 1, 0, 0),
(3, 49, 1, 0, 0),
(3, 50, 1, 0, 0),
(3, 51, 1, 0, 0),
(3, 52, 1, 0, 0),
(3, 53, 1, 0, 0),
(3, 54, 1, 0, 0),
(3, 55, 1, 0, 0),
(3, 56, 1, 0, 0),
(3, 57, 1, 0, 0),
(3, 58, 1, 0, 0),
(3, 59, 1, 0, 0),
(3, 60, 1, 0, 0),
(3, 61, 1, 0, 0),
(3, 62, 1, 0, 0),
(3, 63, 1, 0, 0),
(3, 64, 1, 0, 0),
(3, 65, 1, 0, 0),
(3, 66, 1, 0, 0),
(3, 67, 1, 0, 0),
(4, 1, 1, 1, 1),
(4, 2, 1, 1, 1),
(4, 3, 1, 1, 1),
(4, 4, 1, 1, 1),
(4, 5, 1, 1, 1),
(4, 6, 1, 1, 1),
(4, 7, 1, 1, 1),
(4, 8, 1, 1, 1),
(4, 9, 1, 1, 1),
(4, 10, 1, 1, 1),
(4, 11, 1, 1, 1),
(4, 12, 1, 1, 1),
(4, 13, 1, 1, 1),
(4, 14, 1, 1, 1),
(4, 15, 1, 1, 1),
(4, 16, 1, 1, 1),
(4, 17, 1, 1, 1),
(4, 18, 1, 1, 1),
(4, 19, 1, 1, 1),
(4, 20, 1, 1, 1),
(4, 21, 1, 1, 1),
(4, 22, 1, 1, 1),
(4, 23, 1, 1, 1),
(4, 24, 1, 1, 1),
(4, 25, 1, 1, 1),
(4, 26, 1, 1, 1),
(4, 27, 1, 1, 1),
(4, 28, 1, 1, 1),
(4, 29, 1, 1, 1),
(4, 30, 1, 1, 1),
(4, 31, 1, 1, 1),
(4, 32, 1, 1, 1),
(4, 33, 1, 1, 1),
(4, 34, 1, 1, 1),
(4, 35, 1, 1, 1),
(4, 36, 1, 1, 1),
(4, 37, 1, 1, 1),
(4, 38, 1, 1, 1),
(4, 39, 1, 1, 1),
(4, 40, 1, 1, 1),
(4, 41, 1, 1, 1),
(4, 42, 1, 1, 1),
(4, 43, 1, 1, 1),
(4, 44, 1, 1, 1),
(4, 45, 1, 1, 1),
(4, 46, 1, 1, 1),
(4, 47, 1, 1, 1),
(4, 48, 1, 1, 1),
(4, 49, 1, 1, 1),
(4, 50, 1, 1, 1),
(4, 51, 1, 1, 1),
(4, 52, 1, 1, 1),
(4, 53, 1, 1, 1),
(4, 54, 1, 1, 1),
(4, 55, 1, 1, 1),
(4, 56, 1, 1, 1),
(4, 57, 1, 1, 1),
(4, 58, 1, 1, 1),
(4, 59, 1, 1, 1),
(4, 60, 1, 1, 1),
(4, 61, 1, 1, 1),
(4, 62, 1, 1, 1),
(4, 63, 1, 1, 1),
(4, 64, 1, 1, 1),
(4, 65, 1, 1, 1),
(4, 66, 1, 1, 1),
(4, 67, 1, 1, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_module_country`
--

DROP TABLE IF EXISTS `ps_module_country`;
CREATE TABLE `ps_module_country` (
  `id_module` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT 1,
  `id_country` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_module_country`
--

INSERT INTO `ps_module_country` (`id_module`, `id_shop`, `id_country`) VALUES
(3, 1, 14),
(30, 1, 14);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_module_currency`
--

DROP TABLE IF EXISTS `ps_module_currency`;
CREATE TABLE `ps_module_currency` (
  `id_module` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT 1,
  `id_currency` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_module_currency`
--

INSERT INTO `ps_module_currency` (`id_module`, `id_shop`, `id_currency`) VALUES
(3, 1, 1),
(30, 1, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_module_group`
--

DROP TABLE IF EXISTS `ps_module_group`;
CREATE TABLE `ps_module_group` (
  `id_module` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT 1,
  `id_group` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_module_group`
--

INSERT INTO `ps_module_group` (`id_module`, `id_shop`, `id_group`) VALUES
(1, 1, 1),
(1, 1, 2),
(1, 1, 3),
(2, 1, 1),
(2, 1, 2),
(2, 1, 3),
(3, 1, 1),
(3, 1, 2),
(3, 1, 3),
(4, 1, 1),
(4, 1, 2),
(4, 1, 3),
(5, 1, 1),
(5, 1, 2),
(5, 1, 3),
(6, 1, 1),
(6, 1, 2),
(6, 1, 3),
(7, 1, 1),
(7, 1, 2),
(7, 1, 3),
(8, 1, 1),
(8, 1, 2),
(8, 1, 3),
(9, 1, 1),
(9, 1, 2),
(9, 1, 3),
(10, 1, 1),
(10, 1, 2),
(10, 1, 3),
(11, 1, 1),
(11, 1, 2),
(11, 1, 3),
(12, 1, 1),
(12, 1, 2),
(12, 1, 3),
(13, 1, 1),
(13, 1, 2),
(13, 1, 3),
(14, 1, 1),
(14, 1, 2),
(14, 1, 3),
(15, 1, 1),
(15, 1, 2),
(15, 1, 3),
(16, 1, 1),
(16, 1, 2),
(16, 1, 3),
(17, 1, 1),
(17, 1, 2),
(17, 1, 3),
(18, 1, 1),
(18, 1, 2),
(18, 1, 3),
(19, 1, 1),
(19, 1, 2),
(19, 1, 3),
(20, 1, 1),
(20, 1, 2),
(20, 1, 3),
(21, 1, 1),
(21, 1, 2),
(21, 1, 3),
(22, 1, 1),
(22, 1, 2),
(22, 1, 3),
(23, 1, 1),
(23, 1, 2),
(23, 1, 3),
(24, 1, 1),
(24, 1, 2),
(24, 1, 3),
(25, 1, 1),
(25, 1, 2),
(25, 1, 3),
(26, 1, 1),
(26, 1, 2),
(26, 1, 3),
(27, 1, 1),
(27, 1, 2),
(27, 1, 3),
(28, 1, 1),
(28, 1, 2),
(28, 1, 3),
(29, 1, 1),
(29, 1, 2),
(29, 1, 3),
(30, 1, 1),
(30, 1, 2),
(30, 1, 3),
(31, 1, 1),
(31, 1, 2),
(31, 1, 3),
(32, 1, 1),
(32, 1, 2),
(32, 1, 3),
(33, 1, 1),
(33, 1, 2),
(33, 1, 3),
(34, 1, 1),
(34, 1, 2),
(34, 1, 3),
(35, 1, 1),
(35, 1, 2),
(35, 1, 3),
(36, 1, 1),
(36, 1, 2),
(36, 1, 3),
(37, 1, 1),
(37, 1, 2),
(37, 1, 3),
(38, 1, 1),
(38, 1, 2),
(38, 1, 3),
(39, 1, 1),
(39, 1, 2),
(39, 1, 3),
(40, 1, 1),
(40, 1, 2),
(40, 1, 3),
(41, 1, 1),
(41, 1, 2),
(41, 1, 3),
(42, 1, 1),
(42, 1, 2),
(42, 1, 3),
(43, 1, 1),
(43, 1, 2),
(43, 1, 3),
(44, 1, 1),
(44, 1, 2),
(44, 1, 3),
(45, 1, 1),
(45, 1, 2),
(45, 1, 3),
(46, 1, 1),
(46, 1, 2),
(46, 1, 3),
(47, 1, 1),
(47, 1, 2),
(47, 1, 3),
(48, 1, 1),
(48, 1, 2),
(48, 1, 3),
(49, 1, 1),
(49, 1, 2),
(49, 1, 3),
(50, 1, 1),
(50, 1, 2),
(50, 1, 3),
(51, 1, 1),
(51, 1, 2),
(51, 1, 3),
(52, 1, 1),
(52, 1, 2),
(52, 1, 3),
(53, 1, 1),
(53, 1, 2),
(53, 1, 3),
(54, 1, 1),
(54, 1, 2),
(54, 1, 3),
(55, 1, 1),
(55, 1, 2),
(55, 1, 3),
(56, 1, 1),
(56, 1, 2),
(56, 1, 3),
(57, 1, 1),
(57, 1, 2),
(57, 1, 3),
(58, 1, 1),
(58, 1, 2),
(58, 1, 3),
(59, 1, 1),
(59, 1, 2),
(59, 1, 3),
(60, 1, 1),
(60, 1, 2),
(60, 1, 3),
(61, 1, 1),
(61, 1, 2),
(61, 1, 3),
(62, 1, 1),
(62, 1, 2),
(62, 1, 3),
(63, 1, 1),
(63, 1, 2),
(63, 1, 3),
(64, 1, 1),
(64, 1, 2),
(64, 1, 3),
(65, 1, 1),
(65, 1, 2),
(65, 1, 3),
(66, 1, 1),
(66, 1, 2),
(66, 1, 3),
(67, 1, 1),
(67, 1, 2),
(67, 1, 3);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_module_preference`
--

DROP TABLE IF EXISTS `ps_module_preference`;
CREATE TABLE `ps_module_preference` (
  `id_module_preference` int(11) NOT NULL,
  `id_employee` int(11) NOT NULL,
  `module` varchar(255) NOT NULL,
  `interest` tinyint(1) DEFAULT NULL,
  `favorite` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_module_shop`
--

DROP TABLE IF EXISTS `ps_module_shop`;
CREATE TABLE `ps_module_shop` (
  `id_module` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  `enable_device` tinyint(1) NOT NULL DEFAULT 7
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_module_shop`
--

INSERT INTO `ps_module_shop` (`id_module`, `id_shop`, `enable_device`) VALUES
(3, 1, 7),
(4, 1, 7),
(5, 1, 7),
(7, 1, 7),
(8, 1, 7),
(10, 1, 7),
(11, 1, 7),
(12, 1, 7),
(14, 1, 7),
(15, 1, 7),
(16, 1, 7),
(17, 1, 7),
(18, 1, 7),
(19, 1, 7),
(20, 1, 7),
(21, 1, 7),
(22, 1, 7),
(23, 1, 7),
(24, 1, 7),
(25, 1, 7),
(26, 1, 7),
(27, 1, 7),
(28, 1, 7),
(29, 1, 7),
(30, 1, 7),
(31, 1, 7),
(32, 1, 7),
(33, 1, 7),
(34, 1, 7),
(35, 1, 7),
(36, 1, 7),
(37, 1, 3),
(38, 1, 7),
(39, 1, 7),
(40, 1, 7),
(41, 1, 7),
(42, 1, 7),
(43, 1, 7),
(44, 1, 7),
(45, 1, 7),
(46, 1, 7),
(47, 1, 7),
(48, 1, 7),
(49, 1, 7),
(50, 1, 7),
(51, 1, 7),
(52, 1, 7),
(53, 1, 7),
(54, 1, 7),
(55, 1, 7),
(56, 1, 7),
(57, 1, 7),
(58, 1, 7),
(59, 1, 7),
(60, 1, 7),
(61, 1, 7),
(62, 1, 7),
(63, 1, 7),
(64, 1, 7),
(65, 1, 7),
(66, 1, 7),
(67, 1, 7);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_newsletter`
--

DROP TABLE IF EXISTS `ps_newsletter`;
CREATE TABLE `ps_newsletter` (
  `id` int(6) NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `id_shop_group` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `email` varchar(255) NOT NULL,
  `newsletter_date_add` datetime DEFAULT NULL,
  `ip_registration_newsletter` varchar(15) NOT NULL,
  `http_referer` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_operating_system`
--

DROP TABLE IF EXISTS `ps_operating_system`;
CREATE TABLE `ps_operating_system` (
  `id_operating_system` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_operating_system`
--

INSERT INTO `ps_operating_system` (`id_operating_system`, `name`) VALUES
(1, 'Windows XP'),
(2, 'Windows Vista'),
(3, 'Windows 7'),
(4, 'Windows 8'),
(5, 'MacOsX'),
(6, 'Linux'),
(7, 'Android');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_orders`
--

DROP TABLE IF EXISTS `ps_orders`;
CREATE TABLE `ps_orders` (
  `id_order` int(10) UNSIGNED NOT NULL,
  `reference` varchar(9) DEFAULT NULL,
  `id_shop_group` int(11) UNSIGNED NOT NULL DEFAULT 1,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT 1,
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_cart` int(10) UNSIGNED NOT NULL,
  `id_currency` int(10) UNSIGNED NOT NULL,
  `id_address_delivery` int(10) UNSIGNED NOT NULL,
  `id_address_invoice` int(10) UNSIGNED NOT NULL,
  `current_state` int(10) UNSIGNED NOT NULL,
  `secure_key` varchar(32) NOT NULL DEFAULT '-1',
  `payment` varchar(255) NOT NULL,
  `conversion_rate` decimal(13,6) NOT NULL DEFAULT 1.000000,
  `module` varchar(255) DEFAULT NULL,
  `recyclable` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `gift` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `gift_message` text DEFAULT NULL,
  `mobile_theme` tinyint(1) NOT NULL DEFAULT 0,
  `shipping_number` varchar(64) DEFAULT NULL,
  `total_discounts` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `total_discounts_tax_incl` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `total_discounts_tax_excl` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `total_paid` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `total_paid_tax_incl` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `total_paid_tax_excl` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `total_paid_real` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `total_products` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `total_products_wt` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `total_shipping` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `total_shipping_tax_incl` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `total_shipping_tax_excl` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `carrier_tax_rate` decimal(10,3) NOT NULL DEFAULT 0.000,
  `total_wrapping` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `total_wrapping_tax_incl` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `total_wrapping_tax_excl` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `round_mode` tinyint(1) NOT NULL DEFAULT 2,
  `round_type` tinyint(1) NOT NULL DEFAULT 1,
  `invoice_number` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `delivery_number` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `invoice_date` datetime NOT NULL,
  `delivery_date` datetime NOT NULL,
  `valid` int(1) UNSIGNED NOT NULL DEFAULT 0,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_orders`
--

INSERT INTO `ps_orders` (`id_order`, `reference`, `id_shop_group`, `id_shop`, `id_carrier`, `id_lang`, `id_customer`, `id_cart`, `id_currency`, `id_address_delivery`, `id_address_invoice`, `current_state`, `secure_key`, `payment`, `conversion_rate`, `module`, `recyclable`, `gift`, `gift_message`, `mobile_theme`, `shipping_number`, `total_discounts`, `total_discounts_tax_incl`, `total_discounts_tax_excl`, `total_paid`, `total_paid_tax_incl`, `total_paid_tax_excl`, `total_paid_real`, `total_products`, `total_products_wt`, `total_shipping`, `total_shipping_tax_incl`, `total_shipping_tax_excl`, `carrier_tax_rate`, `total_wrapping`, `total_wrapping_tax_incl`, `total_wrapping_tax_excl`, `round_mode`, `round_type`, `invoice_number`, `delivery_number`, `invoice_date`, `delivery_date`, `valid`, `date_add`, `date_upd`) VALUES
(1, 'XKBKNABJK', 1, 1, 2, 1, 1, 1, 1, 4, 4, 6, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 'Payment by check', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '55.000000', '55.000000', '55.000000', '0.000000', '53.000000', '53.000000', '2.000000', '2.000000', '2.000000', '0.000', '0.000000', '0.000000', '0.000000', 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2018-12-03 17:18:14', '2018-12-03 17:18:15'),
(2, 'OHSATSERP', 1, 1, 2, 1, 1, 2, 1, 4, 4, 1, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 'Payment by check', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '75.900000', '75.900000', '75.900000', '0.000000', '73.900000', '73.900000', '2.000000', '2.000000', '2.000000', '0.000', '0.000000', '0.000000', '0.000000', 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2018-12-03 17:18:14', '2018-12-03 17:18:15'),
(3, 'UOYEVOLI', 1, 1, 2, 1, 1, 3, 1, 4, 4, 8, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 'Payment by check', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '76.010000', '76.010000', '76.010000', '0.000000', '74.010000', '74.010000', '2.000000', '2.000000', '2.000000', '0.000', '0.000000', '0.000000', '0.000000', 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2018-12-03 17:18:14', '2018-12-03 17:18:15'),
(4, 'FFATNOMMJ', 1, 1, 2, 1, 1, 4, 1, 4, 4, 1, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 'Payment by check', '1.000000', 'cheque', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '89.890000', '89.890000', '89.890000', '0.000000', '87.890000', '87.890000', '2.000000', '2.000000', '2.000000', '0.000', '0.000000', '0.000000', '0.000000', 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2018-12-03 17:18:14', '2018-12-03 17:18:15'),
(5, 'KHWLILZLL', 1, 1, 2, 1, 1, 5, 1, 4, 4, 10, 'b44a6d9efd7a0076a0fbce6b15eaf3b1', 'Bank wire', '1.000000', 'bankwire', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '71.510000', '71.510000', '71.510000', '0.000000', '69.510000', '69.510000', '2.000000', '2.000000', '2.000000', '0.000', '0.000000', '0.000000', '0.000000', 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2018-12-03 17:18:14', '2018-12-03 17:18:15'),
(6, 'POKHAERNN', 1, 1, 1, 1, 2, 8, 1, 5, 5, 5, '86cfa6358d0176a108f7a1e958cdbeee', 'Przelew na konto', '1.000000', 'bankwire', 0, 0, '', 0, '', '0.000000', '0.000000', '0.000000', '11118.000000', '11118.000000', '9039.020000', '11118.000000', '9039.020000', '11118.000000', '0.000000', '0.000000', '0.000000', '23.000', '0.000000', '0.000000', '0.000000', 2, 2, 1, 1, '2018-12-04 12:12:58', '2018-12-04 12:12:59', 1, '2018-12-04 12:11:50', '2018-12-04 12:12:59');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_order_carrier`
--

DROP TABLE IF EXISTS `ps_order_carrier`;
CREATE TABLE `ps_order_carrier` (
  `id_order_carrier` int(11) NOT NULL,
  `id_order` int(11) UNSIGNED NOT NULL,
  `id_carrier` int(11) UNSIGNED NOT NULL,
  `id_order_invoice` int(11) UNSIGNED DEFAULT NULL,
  `weight` decimal(20,6) DEFAULT NULL,
  `shipping_cost_tax_excl` decimal(20,6) DEFAULT NULL,
  `shipping_cost_tax_incl` decimal(20,6) DEFAULT NULL,
  `tracking_number` varchar(64) DEFAULT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_order_carrier`
--

INSERT INTO `ps_order_carrier` (`id_order_carrier`, `id_order`, `id_carrier`, `id_order_invoice`, `weight`, `shipping_cost_tax_excl`, `shipping_cost_tax_incl`, `tracking_number`, `date_add`) VALUES
(1, 1, 2, 0, '0.000000', '2.000000', '2.000000', '', '2018-12-03 17:18:14'),
(2, 2, 2, 0, '0.000000', '2.000000', '2.000000', '', '2018-12-03 17:18:14'),
(3, 3, 2, 0, '0.000000', '2.000000', '2.000000', '', '2018-12-03 17:18:14'),
(4, 4, 2, 0, '0.000000', '2.000000', '2.000000', '', '2018-12-03 17:18:14'),
(5, 5, 2, 0, '0.000000', '2.000000', '2.000000', '', '2018-12-03 17:18:14'),
(6, 6, 1, 1, '2237.000000', '0.000000', '0.000000', '', '2018-12-04 12:11:50');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_order_cart_rule`
--

DROP TABLE IF EXISTS `ps_order_cart_rule`;
CREATE TABLE `ps_order_cart_rule` (
  `id_order_cart_rule` int(10) UNSIGNED NOT NULL,
  `id_order` int(10) UNSIGNED NOT NULL,
  `id_cart_rule` int(10) UNSIGNED NOT NULL,
  `id_order_invoice` int(10) UNSIGNED DEFAULT 0,
  `name` varchar(254) NOT NULL,
  `value` decimal(17,2) NOT NULL DEFAULT 0.00,
  `value_tax_excl` decimal(17,2) NOT NULL DEFAULT 0.00,
  `free_shipping` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_order_detail`
--

DROP TABLE IF EXISTS `ps_order_detail`;
CREATE TABLE `ps_order_detail` (
  `id_order_detail` int(10) UNSIGNED NOT NULL,
  `id_order` int(10) UNSIGNED NOT NULL,
  `id_order_invoice` int(11) DEFAULT NULL,
  `id_warehouse` int(10) UNSIGNED DEFAULT 0,
  `id_shop` int(11) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `product_attribute_id` int(10) UNSIGNED DEFAULT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_quantity` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `product_quantity_in_stock` int(10) NOT NULL DEFAULT 0,
  `product_quantity_refunded` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `product_quantity_return` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `product_quantity_reinjected` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `product_price` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `reduction_percent` decimal(10,2) NOT NULL DEFAULT 0.00,
  `reduction_amount` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `reduction_amount_tax_incl` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `reduction_amount_tax_excl` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `group_reduction` decimal(10,2) NOT NULL DEFAULT 0.00,
  `product_quantity_discount` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `product_ean13` varchar(13) DEFAULT NULL,
  `product_upc` varchar(12) DEFAULT NULL,
  `product_reference` varchar(32) DEFAULT NULL,
  `product_supplier_reference` varchar(32) DEFAULT NULL,
  `product_weight` decimal(20,6) NOT NULL,
  `id_tax_rules_group` int(11) UNSIGNED DEFAULT 0,
  `tax_computation_method` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `tax_name` varchar(16) NOT NULL,
  `tax_rate` decimal(10,3) NOT NULL DEFAULT 0.000,
  `ecotax` decimal(21,6) NOT NULL DEFAULT 0.000000,
  `ecotax_tax_rate` decimal(5,3) NOT NULL DEFAULT 0.000,
  `discount_quantity_applied` tinyint(1) NOT NULL DEFAULT 0,
  `download_hash` varchar(255) DEFAULT NULL,
  `download_nb` int(10) UNSIGNED DEFAULT 0,
  `download_deadline` datetime DEFAULT NULL,
  `total_price_tax_incl` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `total_price_tax_excl` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `unit_price_tax_incl` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `unit_price_tax_excl` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `total_shipping_price_tax_incl` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `total_shipping_price_tax_excl` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `purchase_supplier_price` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `original_product_price` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `original_wholesale_price` decimal(20,6) NOT NULL DEFAULT 0.000000
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_order_detail`
--

INSERT INTO `ps_order_detail` (`id_order_detail`, `id_order`, `id_order_invoice`, `id_warehouse`, `id_shop`, `product_id`, `product_attribute_id`, `product_name`, `product_quantity`, `product_quantity_in_stock`, `product_quantity_refunded`, `product_quantity_return`, `product_quantity_reinjected`, `product_price`, `reduction_percent`, `reduction_amount`, `reduction_amount_tax_incl`, `reduction_amount_tax_excl`, `group_reduction`, `product_quantity_discount`, `product_ean13`, `product_upc`, `product_reference`, `product_supplier_reference`, `product_weight`, `id_tax_rules_group`, `tax_computation_method`, `tax_name`, `tax_rate`, `ecotax`, `ecotax_tax_rate`, `discount_quantity_applied`, `download_hash`, `download_nb`, `download_deadline`, `total_price_tax_incl`, `total_price_tax_excl`, `unit_price_tax_incl`, `unit_price_tax_excl`, `total_shipping_price_tax_incl`, `total_shipping_price_tax_excl`, `purchase_supplier_price`, `original_product_price`, `original_wholesale_price`) VALUES
(1, 1, 0, 0, 1, 2, 10, 'Blouse - Color : White, Size : M', 1, 1, 0, 0, 0, '26.999852', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', 'demo_2', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '27.000000', '27.000000', '27.000000', '27.000000', '0.000000', '0.000000', '0.000000', '26.999852', '8.100000'),
(2, 1, 0, 0, 1, 3, 13, 'Printed Dress - Color : Orange, Size : S', 1, 1, 0, 0, 0, '25.999852', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', 'demo_3', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '26.000000', '26.000000', '26.000000', '26.000000', '0.000000', '0.000000', '0.000000', '25.999852', '7.800000'),
(3, 2, 0, 0, 1, 2, 10, 'Blouse - Color : White, Size : M', 1, 1, 0, 0, 0, '26.999852', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', 'demo_2', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '27.000000', '27.000000', '27.000000', '27.000000', '0.000000', '0.000000', '0.000000', '26.999852', '8.100000'),
(4, 2, 0, 0, 1, 6, 32, 'Printed Summer Dress - Color : Yellow, Size : M', 1, 1, 0, 0, 0, '30.502569', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', 'demo_6', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '30.500000', '30.500000', '30.500000', '30.500000', '0.000000', '0.000000', '0.000000', '30.502569', '9.150000'),
(5, 2, 0, 0, 1, 7, 34, 'Printed Chiffon Dress - Color : Yellow, Size : S', 1, 1, 0, 0, 0, '20.501236', '20.00', '0.000000', '0.000000', '0.000000', '0.00', '17.400000', '', '', 'demo_7', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '16.400000', '16.400000', '16.400000', '16.400000', '0.000000', '0.000000', '0.000000', '20.501236', '6.150000'),
(6, 3, 0, 0, 1, 1, 1, 'Faded Short Sleeve T-shirts - Color : Orange, Size : S', 1, 1, 0, 0, 0, '16.510000', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', 'demo_1', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '16.510000', '16.510000', '16.510000', '16.510000', '0.000000', '0.000000', '0.000000', '16.510000', '4.950000'),
(7, 3, 0, 0, 1, 2, 10, 'Blouse - Color : White, Size : M', 1, 1, 0, 0, 0, '26.999852', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', 'demo_2', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '27.000000', '27.000000', '27.000000', '27.000000', '0.000000', '0.000000', '0.000000', '26.999852', '8.100000'),
(8, 3, 0, 0, 1, 6, 32, 'Printed Summer Dress - Color : Yellow, Size : M', 1, 1, 0, 0, 0, '30.502569', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', 'demo_6', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '30.500000', '30.500000', '30.500000', '30.500000', '0.000000', '0.000000', '0.000000', '30.502569', '9.150000'),
(9, 4, 0, 0, 1, 1, 1, 'Faded Short Sleeve T-shirts - Color : Orange, Size : S', 1, 1, 0, 0, 0, '16.510000', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', 'demo_1', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '16.510000', '16.510000', '16.510000', '16.510000', '0.000000', '0.000000', '0.000000', '16.510000', '4.950000'),
(10, 4, 0, 0, 1, 3, 13, 'Printed Dress - Color : Orange, Size : S', 1, 1, 0, 0, 0, '25.999852', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', 'demo_3', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '26.000000', '26.000000', '26.000000', '26.000000', '0.000000', '0.000000', '0.000000', '25.999852', '7.800000'),
(11, 4, 0, 0, 1, 5, 19, 'Printed Summer Dress - Color : Yellow, Size : S', 1, 1, 0, 0, 0, '30.506321', '5.00', '0.000000', '0.000000', '0.000000', '0.00', '29.980000', '', '', 'demo_5', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '28.980000', '28.980000', '28.980000', '28.980000', '0.000000', '0.000000', '0.000000', '30.506321', '9.150000'),
(12, 4, 0, 0, 1, 7, 34, 'Printed Chiffon Dress - Color : Yellow, Size : S', 1, 1, 0, 0, 0, '20.501236', '20.00', '0.000000', '0.000000', '0.000000', '0.00', '17.400000', '', '', 'demo_7', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '16.400000', '16.400000', '16.400000', '16.400000', '0.000000', '0.000000', '0.000000', '20.501236', '6.150000'),
(13, 5, 0, 0, 1, 1, 1, 'Faded Short Sleeve T-shirts - Color : Orange, Size : S', 1, 1, 0, 0, 0, '16.510000', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', 'demo_1', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '16.510000', '16.510000', '16.510000', '16.510000', '0.000000', '0.000000', '0.000000', '16.510000', '4.950000'),
(14, 5, 0, 0, 1, 2, 7, 'Blouse - Color : Black, Size : S', 1, 1, 0, 0, 0, '26.999852', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', 'demo_2', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '27.000000', '27.000000', '27.000000', '27.000000', '0.000000', '0.000000', '0.000000', '26.999852', '8.100000'),
(15, 5, 0, 0, 1, 3, 13, 'Printed Dress - Color : Orange, Size : S', 1, 1, 0, 0, 0, '25.999852', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', 'demo_3', '', '0.000000', 0, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '26.000000', '26.000000', '26.000000', '26.000000', '0.000000', '0.000000', '0.000000', '25.999852', '7.800000'),
(16, 6, 1, 0, 1, 2, 0, 'AC Frua', 1, 1, 0, 0, 0, '9039.024390', '0.00', '0.000000', '0.000000', '0.000000', '0.00', '0.000000', '', '', '', '', '2237.000000', 1, 0, '', '0.000', '0.000000', '0.000', 0, '', 0, '0000-00-00 00:00:00', '11118.000000', '9039.020000', '11118.000000', '9039.024390', '0.000000', '0.000000', '0.000000', '9039.024390', '0.000000');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_order_detail_tax`
--

DROP TABLE IF EXISTS `ps_order_detail_tax`;
CREATE TABLE `ps_order_detail_tax` (
  `id_order_detail` int(11) NOT NULL,
  `id_tax` int(11) NOT NULL,
  `unit_amount` decimal(16,6) NOT NULL DEFAULT 0.000000,
  `total_amount` decimal(16,6) NOT NULL DEFAULT 0.000000
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_order_detail_tax`
--

INSERT INTO `ps_order_detail_tax` (`id_order_detail`, `id_tax`, `unit_amount`, `total_amount`) VALUES
(16, 1, '2078.975610', '2078.980000');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_order_history`
--

DROP TABLE IF EXISTS `ps_order_history`;
CREATE TABLE `ps_order_history` (
  `id_order_history` int(10) UNSIGNED NOT NULL,
  `id_employee` int(10) UNSIGNED NOT NULL,
  `id_order` int(10) UNSIGNED NOT NULL,
  `id_order_state` int(10) UNSIGNED NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_order_history`
--

INSERT INTO `ps_order_history` (`id_order_history`, `id_employee`, `id_order`, `id_order_state`, `date_add`) VALUES
(1, 0, 1, 1, '2018-12-03 17:18:15'),
(2, 0, 2, 1, '2018-12-03 17:18:15'),
(3, 0, 3, 1, '2018-12-03 17:18:15'),
(4, 0, 4, 1, '2018-12-03 17:18:15'),
(5, 0, 5, 10, '2018-12-03 17:18:15'),
(6, 1, 1, 6, '2018-12-03 17:18:15'),
(7, 1, 3, 8, '2018-12-03 17:18:15'),
(8, 0, 6, 10, '2018-12-04 12:11:50'),
(9, 2, 6, 5, '2018-12-04 12:12:59');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_order_invoice`
--

DROP TABLE IF EXISTS `ps_order_invoice`;
CREATE TABLE `ps_order_invoice` (
  `id_order_invoice` int(11) UNSIGNED NOT NULL,
  `id_order` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `delivery_number` int(11) NOT NULL,
  `delivery_date` datetime DEFAULT NULL,
  `total_discount_tax_excl` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `total_discount_tax_incl` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `total_paid_tax_excl` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `total_paid_tax_incl` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `total_products` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `total_products_wt` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `total_shipping_tax_excl` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `total_shipping_tax_incl` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `shipping_tax_computation_method` int(10) UNSIGNED NOT NULL,
  `total_wrapping_tax_excl` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `total_wrapping_tax_incl` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `shop_address` text DEFAULT NULL,
  `invoice_address` text DEFAULT NULL,
  `delivery_address` text DEFAULT NULL,
  `note` text DEFAULT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_order_invoice`
--

INSERT INTO `ps_order_invoice` (`id_order_invoice`, `id_order`, `number`, `delivery_number`, `delivery_date`, `total_discount_tax_excl`, `total_discount_tax_incl`, `total_paid_tax_excl`, `total_paid_tax_incl`, `total_products`, `total_products_wt`, `total_shipping_tax_excl`, `total_shipping_tax_incl`, `shipping_tax_computation_method`, `total_wrapping_tax_excl`, `total_wrapping_tax_incl`, `shop_address`, `invoice_address`, `delivery_address`, `note`, `date_add`) VALUES
(1, 6, 1, 1, '2018-12-04 12:12:59', '0.000000', '0.000000', '9039.020000', '11118.000000', '9039.020000', '11118.000000', '0.000000', '0.000000', 0, '0.000000', '0.000000', 'otomoto', '', '', '', '2018-12-04 12:12:58');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_order_invoice_payment`
--

DROP TABLE IF EXISTS `ps_order_invoice_payment`;
CREATE TABLE `ps_order_invoice_payment` (
  `id_order_invoice` int(11) UNSIGNED NOT NULL,
  `id_order_payment` int(11) UNSIGNED NOT NULL,
  `id_order` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_order_invoice_payment`
--

INSERT INTO `ps_order_invoice_payment` (`id_order_invoice`, `id_order_payment`, `id_order`) VALUES
(1, 1, 6);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_order_invoice_tax`
--

DROP TABLE IF EXISTS `ps_order_invoice_tax`;
CREATE TABLE `ps_order_invoice_tax` (
  `id_order_invoice` int(11) NOT NULL,
  `type` varchar(15) NOT NULL,
  `id_tax` int(11) NOT NULL,
  `amount` decimal(10,6) NOT NULL DEFAULT 0.000000
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_order_invoice_tax`
--

INSERT INTO `ps_order_invoice_tax` (`id_order_invoice`, `type`, `id_tax`, `amount`) VALUES
(1, 'shipping', 1, '0.000000');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_order_message`
--

DROP TABLE IF EXISTS `ps_order_message`;
CREATE TABLE `ps_order_message` (
  `id_order_message` int(10) UNSIGNED NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_order_message`
--

INSERT INTO `ps_order_message` (`id_order_message`, `date_add`) VALUES
(1, '2018-12-03 17:18:15');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_order_message_lang`
--

DROP TABLE IF EXISTS `ps_order_message_lang`;
CREATE TABLE `ps_order_message_lang` (
  `id_order_message` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_order_message_lang`
--

INSERT INTO `ps_order_message_lang` (`id_order_message`, `id_lang`, `name`, `message`) VALUES
(1, 1, 'Delay', 'Hi,\n\nUnfortunately, an item on your order is currently out of stock. This may cause a slight delay in delivery.\nPlease accept our apologies and rest assured that we are working hard to rectify this.\n\nBest regards,');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_order_payment`
--

DROP TABLE IF EXISTS `ps_order_payment`;
CREATE TABLE `ps_order_payment` (
  `id_order_payment` int(11) NOT NULL,
  `order_reference` varchar(9) DEFAULT NULL,
  `id_currency` int(10) UNSIGNED NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `conversion_rate` decimal(13,6) NOT NULL DEFAULT 1.000000,
  `transaction_id` varchar(254) DEFAULT NULL,
  `card_number` varchar(254) DEFAULT NULL,
  `card_brand` varchar(254) DEFAULT NULL,
  `card_expiration` char(7) DEFAULT NULL,
  `card_holder` varchar(254) DEFAULT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_order_payment`
--

INSERT INTO `ps_order_payment` (`id_order_payment`, `order_reference`, `id_currency`, `amount`, `payment_method`, `conversion_rate`, `transaction_id`, `card_number`, `card_brand`, `card_expiration`, `card_holder`, `date_add`) VALUES
(1, 'POKHAERNN', 1, '11118.00', 'Przelew na konto', '1.000000', '', '', '', '', '', '2018-12-04 12:12:59');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_order_return`
--

DROP TABLE IF EXISTS `ps_order_return`;
CREATE TABLE `ps_order_return` (
  `id_order_return` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_order` int(10) UNSIGNED NOT NULL,
  `state` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `question` text NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_order_return_detail`
--

DROP TABLE IF EXISTS `ps_order_return_detail`;
CREATE TABLE `ps_order_return_detail` (
  `id_order_return` int(10) UNSIGNED NOT NULL,
  `id_order_detail` int(10) UNSIGNED NOT NULL,
  `id_customization` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `product_quantity` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_order_return_state`
--

DROP TABLE IF EXISTS `ps_order_return_state`;
CREATE TABLE `ps_order_return_state` (
  `id_order_return_state` int(10) UNSIGNED NOT NULL,
  `color` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_order_return_state`
--

INSERT INTO `ps_order_return_state` (`id_order_return_state`, `color`) VALUES
(1, '#4169E1'),
(2, '#8A2BE2'),
(3, '#32CD32'),
(4, '#DC143C'),
(5, '#108510');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_order_return_state_lang`
--

DROP TABLE IF EXISTS `ps_order_return_state_lang`;
CREATE TABLE `ps_order_return_state_lang` (
  `id_order_return_state` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_order_return_state_lang`
--

INSERT INTO `ps_order_return_state_lang` (`id_order_return_state`, `id_lang`, `name`) VALUES
(1, 1, 'Oczekiwanie na potwierdzenie'),
(2, 1, 'Oczekiwanie na paczkÄ'),
(3, 1, 'Paczka zostaĹa odebrana'),
(4, 1, 'Brak akceptacji zwrotu'),
(5, 1, 'Dokonanie zwrotu');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_order_slip`
--

DROP TABLE IF EXISTS `ps_order_slip`;
CREATE TABLE `ps_order_slip` (
  `id_order_slip` int(10) UNSIGNED NOT NULL,
  `conversion_rate` decimal(13,6) NOT NULL DEFAULT 1.000000,
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_order` int(10) UNSIGNED NOT NULL,
  `total_products_tax_excl` decimal(20,6) DEFAULT NULL,
  `total_products_tax_incl` decimal(20,6) DEFAULT NULL,
  `total_shipping_tax_excl` decimal(20,6) DEFAULT NULL,
  `total_shipping_tax_incl` decimal(20,6) DEFAULT NULL,
  `shipping_cost` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `amount` decimal(10,2) NOT NULL,
  `shipping_cost_amount` decimal(10,2) NOT NULL,
  `partial` tinyint(1) NOT NULL,
  `order_slip_type` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_order_slip_detail`
--

DROP TABLE IF EXISTS `ps_order_slip_detail`;
CREATE TABLE `ps_order_slip_detail` (
  `id_order_slip` int(10) UNSIGNED NOT NULL,
  `id_order_detail` int(10) UNSIGNED NOT NULL,
  `product_quantity` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `unit_price_tax_excl` decimal(20,6) DEFAULT NULL,
  `unit_price_tax_incl` decimal(20,6) DEFAULT NULL,
  `total_price_tax_excl` decimal(20,6) DEFAULT NULL,
  `total_price_tax_incl` decimal(20,6) DEFAULT NULL,
  `amount_tax_excl` decimal(20,6) DEFAULT NULL,
  `amount_tax_incl` decimal(20,6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_order_slip_detail_tax`
--

DROP TABLE IF EXISTS `ps_order_slip_detail_tax`;
CREATE TABLE `ps_order_slip_detail_tax` (
  `id_order_slip_detail` int(11) UNSIGNED NOT NULL,
  `id_tax` int(11) UNSIGNED NOT NULL,
  `unit_amount` decimal(16,6) NOT NULL DEFAULT 0.000000,
  `total_amount` decimal(16,6) NOT NULL DEFAULT 0.000000
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_order_state`
--

DROP TABLE IF EXISTS `ps_order_state`;
CREATE TABLE `ps_order_state` (
  `id_order_state` int(10) UNSIGNED NOT NULL,
  `invoice` tinyint(1) UNSIGNED DEFAULT 0,
  `send_email` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `module_name` varchar(255) DEFAULT NULL,
  `color` varchar(32) DEFAULT NULL,
  `unremovable` tinyint(1) UNSIGNED NOT NULL,
  `hidden` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `logable` tinyint(1) NOT NULL DEFAULT 0,
  `delivery` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `shipped` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `paid` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `pdf_invoice` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `pdf_delivery` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_order_state`
--

INSERT INTO `ps_order_state` (`id_order_state`, `invoice`, `send_email`, `module_name`, `color`, `unremovable`, `hidden`, `logable`, `delivery`, `shipped`, `paid`, `pdf_invoice`, `pdf_delivery`, `deleted`) VALUES
(1, 0, 1, 'cheque', '#4169E1', 1, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 1, 1, '', '#32CD32', 1, 0, 1, 0, 0, 1, 1, 0, 0),
(3, 1, 1, '', '#FF8C00', 1, 0, 1, 1, 0, 1, 0, 0, 0),
(4, 1, 1, '', '#8A2BE2', 1, 0, 1, 1, 1, 1, 0, 0, 0),
(5, 1, 0, '', '#108510', 1, 0, 1, 1, 1, 1, 0, 0, 0),
(6, 0, 1, '', '#DC143C', 1, 0, 0, 0, 0, 0, 0, 0, 0),
(7, 1, 1, '', '#ec2e15', 1, 0, 0, 0, 0, 0, 0, 0, 0),
(8, 0, 1, '', '#8f0621', 1, 0, 0, 0, 0, 0, 0, 0, 0),
(9, 1, 1, '', '#FF69B4', 1, 0, 0, 0, 0, 1, 0, 0, 0),
(10, 0, 1, 'bankwire', '#4169E1', 1, 0, 0, 0, 0, 0, 0, 0, 0),
(11, 0, 0, '', '#4169E1', 1, 0, 0, 0, 0, 0, 0, 0, 0),
(12, 1, 1, '', '#32CD32', 1, 0, 1, 0, 0, 1, 0, 0, 0),
(13, 0, 1, '', '#FF69B4', 1, 0, 0, 0, 0, 0, 0, 0, 0),
(14, 0, 0, 'cashondelivery', '#4169E1', 1, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_order_state_lang`
--

DROP TABLE IF EXISTS `ps_order_state_lang`;
CREATE TABLE `ps_order_state_lang` (
  `id_order_state` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `template` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_order_state_lang`
--

INSERT INTO `ps_order_state_lang` (`id_order_state`, `id_lang`, `name`, `template`) VALUES
(1, 1, 'Oczekiwanie pĹatnoĹci czekiem', 'cheque'),
(2, 1, 'PĹatnoĹÄ zaakceptowana', 'payment'),
(3, 1, 'Przygotowanie w toku', 'preparation'),
(4, 1, 'WysĹane', 'shipped'),
(5, 1, 'Dostarczone', ''),
(6, 1, 'Anulowane', 'order_canceled'),
(7, 1, 'Zwrot', 'refund'),
(8, 1, 'BĹÄd pĹatonĹci', 'payment_error'),
(9, 1, 'Brak towaru', 'outofstock'),
(10, 1, 'Oczekiwanie na pĹatnoĹÄ przelewem bankowym', 'bankwire'),
(11, 1, 'Oczekiwanie na pĹatnoĹÄ Paypal', ''),
(12, 1, 'PĹatnoĹÄ przyjÄta', 'payment'),
(13, 1, 'Brak towaru', 'outofstock'),
(14, 1, 'Awaiting cod validation', 'cashondelivery');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_pack`
--

DROP TABLE IF EXISTS `ps_pack`;
CREATE TABLE `ps_pack` (
  `id_product_pack` int(10) UNSIGNED NOT NULL,
  `id_product_item` int(10) UNSIGNED NOT NULL,
  `id_product_attribute_item` int(10) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_page`
--

DROP TABLE IF EXISTS `ps_page`;
CREATE TABLE `ps_page` (
  `id_page` int(10) UNSIGNED NOT NULL,
  `id_page_type` int(10) UNSIGNED NOT NULL,
  `id_object` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_page`
--

INSERT INTO `ps_page` (`id_page`, `id_page_type`, `id_object`) VALUES
(1, 1, NULL),
(2, 2, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_pagenotfound`
--

DROP TABLE IF EXISTS `ps_pagenotfound`;
CREATE TABLE `ps_pagenotfound` (
  `id_pagenotfound` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `id_shop_group` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `request_uri` varchar(256) NOT NULL,
  `http_referer` varchar(256) NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_page_type`
--

DROP TABLE IF EXISTS `ps_page_type`;
CREATE TABLE `ps_page_type` (
  `id_page_type` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_page_type`
--

INSERT INTO `ps_page_type` (`id_page_type`, `name`) VALUES
(1, 'index'),
(2, 'product');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_page_viewed`
--

DROP TABLE IF EXISTS `ps_page_viewed`;
CREATE TABLE `ps_page_viewed` (
  `id_page` int(10) UNSIGNED NOT NULL,
  `id_shop_group` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `id_date_range` int(10) UNSIGNED NOT NULL,
  `counter` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_product`
--

DROP TABLE IF EXISTS `ps_product`;
CREATE TABLE `ps_product` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_supplier` int(10) UNSIGNED DEFAULT NULL,
  `id_manufacturer` int(10) UNSIGNED DEFAULT NULL,
  `id_category_default` int(10) UNSIGNED DEFAULT NULL,
  `id_shop_default` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `id_tax_rules_group` int(11) UNSIGNED NOT NULL,
  `on_sale` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `online_only` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `ean13` varchar(13) DEFAULT NULL,
  `upc` varchar(12) DEFAULT NULL,
  `ecotax` decimal(17,6) NOT NULL DEFAULT 0.000000,
  `quantity` int(10) NOT NULL DEFAULT 0,
  `minimal_quantity` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `price` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `wholesale_price` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `unity` varchar(255) DEFAULT NULL,
  `unit_price_ratio` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `additional_shipping_cost` decimal(20,2) NOT NULL DEFAULT 0.00,
  `reference` varchar(32) DEFAULT NULL,
  `supplier_reference` varchar(32) DEFAULT NULL,
  `location` varchar(64) DEFAULT NULL,
  `width` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `height` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `depth` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `weight` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `out_of_stock` int(10) UNSIGNED NOT NULL DEFAULT 2,
  `quantity_discount` tinyint(1) DEFAULT 0,
  `customizable` tinyint(2) NOT NULL DEFAULT 0,
  `uploadable_files` tinyint(4) NOT NULL DEFAULT 0,
  `text_fields` tinyint(4) NOT NULL DEFAULT 0,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `redirect_type` enum('','404','301','302') NOT NULL DEFAULT '',
  `id_product_redirected` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `available_for_order` tinyint(1) NOT NULL DEFAULT 1,
  `available_date` date NOT NULL DEFAULT '0000-00-00',
  `condition` enum('new','used','refurbished') NOT NULL DEFAULT 'new',
  `show_price` tinyint(1) NOT NULL DEFAULT 1,
  `indexed` tinyint(1) NOT NULL DEFAULT 0,
  `visibility` enum('both','catalog','search','none') NOT NULL DEFAULT 'both',
  `cache_is_pack` tinyint(1) NOT NULL DEFAULT 0,
  `cache_has_attachments` tinyint(1) NOT NULL DEFAULT 0,
  `is_virtual` tinyint(1) NOT NULL DEFAULT 0,
  `cache_default_attribute` int(10) UNSIGNED DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `advanced_stock_management` tinyint(1) NOT NULL DEFAULT 0,
  `pack_stock_type` int(11) UNSIGNED NOT NULL DEFAULT 3
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_product`
--

INSERT INTO `ps_product` (`id_product`, `id_supplier`, `id_manufacturer`, `id_category_default`, `id_shop_default`, `id_tax_rules_group`, `on_sale`, `online_only`, `ean13`, `upc`, `ecotax`, `quantity`, `minimal_quantity`, `price`, `wholesale_price`, `unity`, `unit_price_ratio`, `additional_shipping_cost`, `reference`, `supplier_reference`, `location`, `width`, `height`, `depth`, `weight`, `out_of_stock`, `quantity_discount`, `customizable`, `uploadable_files`, `text_fields`, `active`, `redirect_type`, `id_product_redirected`, `available_for_order`, `available_date`, `condition`, `show_price`, `indexed`, `visibility`, `cache_is_pack`, `cache_has_attachments`, `is_virtual`, `cache_default_attribute`, `date_add`, `date_upd`, `advanced_stock_management`, `pack_stock_type`) VALUES
(1, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '10031.707317', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1314.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(2, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '9039.024390', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2237.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(3, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '13611.382114', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2019.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(4, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '12867.479675', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1738.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(5, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14942.276423', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2041.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(6, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1894.308943', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1967.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(7, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1691.056911', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2057.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(8, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14830.894309', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1420.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(9, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '9621.951220', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1706.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(10, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '10047.967480', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2082.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(11, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '11404.065041', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1594.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(12, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8575.609756', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1441.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(13, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '15338.211382', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1503.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(14, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14698.373984', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1778.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(15, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '10340.650407', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1689.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(16, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '12584.552846', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1706.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(17, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '9447.967480', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2271.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(18, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '13726.016260', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1587.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(19, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2321.951220', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2100.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(20, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2308.943089', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2136.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(21, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2074.796748', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2286.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(22, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '11599.186992', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1718.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(23, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '10486.178862', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1384.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(24, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '9617.073171', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2221.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(25, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8591.869919', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2235.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(26, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '13175.609756', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2224.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(27, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '13086.991870', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1339.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(28, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8152.032520', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1768.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(29, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8508.943089', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2111.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(30, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8329.268293', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1484.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(31, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '11714.634146', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1895.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(32, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '9082.113821', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1433.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(33, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '15018.699187', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1790.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(34, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '15765.853659', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1848.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(35, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '13185.365854', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2015.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(36, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2160.162602', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2143.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(37, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1958.536585', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2057.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(38, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '15014.634146', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2048.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(39, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1460.162602', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1609.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(40, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '6145.528455', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2277.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(41, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '11667.479675', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1531.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(42, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14976.422764', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2144.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(43, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '830.081301', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2267.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(44, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1676.422764', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1696.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(45, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8937.398374', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1651.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(46, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14998.373984', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1773.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(47, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8234.959350', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1306.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(48, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '9773.170732', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1325.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(49, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14499.186992', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1677.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(50, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8656.097561', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1923.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(51, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14660.162602', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1408.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(52, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '12281.300813', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2072.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(53, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14773.983740', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1377.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(54, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14230.894309', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1620.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(55, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '12669.918699', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1789.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(56, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '11117.073171', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1844.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(57, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2279.674797', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1578.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(58, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '47495.121951', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2107.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(59, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '12219.512195', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1722.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(60, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '10573.170732', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1924.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(61, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '15453.658537', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1572.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(62, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14412.195122', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1425.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(63, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '4069.105691', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1975.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(64, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14536.585366', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1639.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(65, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '9786.178862', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1830.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(66, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1890.243902', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2120.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(67, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '26739.837398', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1397.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(68, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1993.495935', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1603.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(69, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '11301.626016', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1333.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(70, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '4778.048780', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1974.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(71, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '45843.902439', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1733.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(72, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '15325.203252', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1550.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(73, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '11793.495935', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2063.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(74, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '10205.691057', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1530.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(75, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '12769.105691', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1563.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(76, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '10286.991870', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2205.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(77, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '16191.056911', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2138.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(78, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1725.203252', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2260.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(79, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '10945.528455', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1384.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(80, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14030.081301', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1565.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(81, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '10840.650407', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1921.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(82, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '13602.439024', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1947.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(83, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1880.487805', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1653.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(84, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14999.186992', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2038.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(85, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1059.349593', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1879.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(86, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '10383.739837', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2228.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(87, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1256.910569', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1983.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(88, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8509.756098', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2173.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(89, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2356.910569', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1718.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(90, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '11585.365854', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1519.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(91, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '10097.560976', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1702.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(92, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '9296.747967', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1328.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(93, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14930.894309', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1416.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(94, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '15265.853659', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1482.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(95, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14563.414634', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1802.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(96, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '55311.382114', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1531.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(97, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '79652.845528', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1501.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(98, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2010.569106', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1718.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(99, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '44603.252033', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1973.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(100, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1212.195122', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1324.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(101, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '11635.772358', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1657.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(102, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '12107.317073', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1734.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(103, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '15838.211382', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2123.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(104, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8211.382114', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2197.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(105, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14586.178862', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1376.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(106, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '11246.341463', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1563.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(107, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '10839.837398', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1627.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(108, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '11965.853659', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1706.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(109, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '13082.113821', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1352.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(110, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '15723.577236', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2280.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(111, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8895.934959', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1701.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(112, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '10988.617886', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1381.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(113, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '12000.000000', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2052.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(114, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14679.674797', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1551.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(115, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '904.065041', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1393.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(116, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1442.276423', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2081.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(117, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '10013.821138', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1924.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(118, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2070.731707', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2103.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(119, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1725.203252', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2041.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(120, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '5832.520325', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1384.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(121, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '12900.000000', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1977.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(122, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '5200.000000', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1529.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(123, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2295.934959', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2040.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(124, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '12206.504065', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1458.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(125, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '46217.886179', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2086.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(126, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '34074.796748', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1930.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(127, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '10820.325203', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1630.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(128, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '15082.113821', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1810.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(129, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '16230.081301', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1725.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(130, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '11266.666667', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2273.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(131, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8273.983740', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2200.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(132, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2152.845528', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2213.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(133, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14614.634146', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1769.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(134, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14599.186992', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2040.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(135, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '36744.715447', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1645.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(136, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '988.617886', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2050.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(137, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '12844.715447', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1861.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(138, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '15030.081301', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1416.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(139, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '31769.105691', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1687.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(140, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1281.300813', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1863.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(141, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8856.910569', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1474.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(142, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '946.341463', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1633.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(143, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8615.447154', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1744.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(144, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '33690.243902', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1829.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(145, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14494.308943', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1968.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(146, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '12421.951220', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1365.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(147, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1756.910569', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2087.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(148, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '11674.796748', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1666.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(149, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2396.747967', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2291.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(150, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '11619.512195', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1510.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(151, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '12434.959350', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1452.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(152, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '13311.382114', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1540.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(153, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '16034.959350', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1398.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(154, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '9687.804878', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1439.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(155, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1126.016260', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1502.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(156, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '9646.341463', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1583.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(157, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '10668.292683', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1646.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(158, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '4250.406504', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1420.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(159, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14363.414634', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1925.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(160, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '16079.674797', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1897.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(161, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '11802.439024', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1982.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(162, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14268.292683', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2136.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(163, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2243.902439', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1828.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(164, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1866.666667', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1514.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(165, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '16172.357724', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1583.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(166, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8801.626016', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1500.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(167, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '28109.756098', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1375.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(168, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8160.162602', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1536.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(169, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14811.382114', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1851.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(170, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2354.471545', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2132.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(171, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '9451.219512', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1456.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(172, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2438.211382', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1982.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(173, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1854.471545', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2062.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(174, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '11274.796748', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1388.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(175, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '4669.105691', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1536.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(176, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '10156.097561', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2050.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(177, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '10496.747967', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1361.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(178, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8873.170732', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1858.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(179, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '11774.796748', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1837.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(180, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1236.585366', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1460.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(181, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2059.349593', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1597.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(182, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14413.821138', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1877.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(183, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '11745.528455', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1793.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(184, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14527.642276', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1934.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(185, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8320.325203', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1695.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(186, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '15803.252033', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1845.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(187, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '9253.658537', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1821.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(188, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '12073.983740', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1835.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(189, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '11082.113821', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1673.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(190, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14917.073171', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1353.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(191, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '11771.544715', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2112.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(192, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8911.382114', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1617.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(193, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2152.845528', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1602.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(194, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1892.682927', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2264.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(195, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '41883.739837', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1960.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(196, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '13871.544715', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1521.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(197, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '16199.186992', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2258.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(198, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '11595.121951', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2172.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(199, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2000.000000', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1334.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(200, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1804.065041', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1439.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(201, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '10565.853659', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1858.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(202, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '9007.317073', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2258.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(203, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '15576.422764', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2232.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(204, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '11578.048780', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2295.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(205, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '10529.268293', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1528.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(206, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '10695.121951', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2273.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(207, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '15386.178862', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2103.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(208, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14557.723577', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1888.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(209, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '15030.894309', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2081.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(210, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2297.560976', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1342.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(211, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '16073.983740', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1529.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(212, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '6774.796748', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2269.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(213, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '15470.731707', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1562.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(214, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2303.252033', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1472.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(215, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14091.056911', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1560.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(216, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '9256.097561', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2031.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(217, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '15513.821138', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2047.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(218, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8947.967480', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1577.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(219, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '11023.577236', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1404.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(220, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '12634.146341', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1790.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(221, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '59460.975610', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2297.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(222, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1294.308943', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1545.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(223, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1643.902439', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2023.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(224, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2009.756098', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2098.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(225, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '25817.073171', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2219.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(226, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '63402.439024', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2090.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(227, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '29800.813008', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1696.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(228, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '13593.495935', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1748.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(229, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '11933.333333', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1516.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(230, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1684.552846', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2141.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(231, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2033.333333', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2184.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(232, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2113.008130', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1415.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(233, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '47478.048780', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1512.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(234, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14817.073171', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1788.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(235, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '13428.455285', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1640.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(236, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2330.081301', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1335.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(237, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2184.552846', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2133.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(238, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '12806.504065', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1927.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(239, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2290.243902', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1931.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(240, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1752.032520', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1562.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(241, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '9766.666667', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2083.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(242, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1239.837398', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2004.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(243, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1424.390244', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1445.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(244, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '9308.130081', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1429.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(245, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '5504.065041', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1483.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(246, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '28300.813008', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2096.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(247, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '13780.487805', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2104.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(248, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2400.813008', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1808.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(249, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1978.861789', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2197.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(250, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '10199.186992', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1792.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(251, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '13282.926829', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1679.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(252, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '13811.382114', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1928.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(253, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8215.447154', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2124.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(254, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '15608.943089', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1763.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(255, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '45178.048780', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1728.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(256, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8246.341463', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1698.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(257, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '15900.000000', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1857.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(258, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '11715.447154', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1679.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(259, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '9008.130081', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1436.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(260, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '13760.162602', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1986.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(261, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '10348.780488', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2020.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(262, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '12718.699187', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1424.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(263, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '10932.520325', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1516.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(264, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '10681.300813', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1957.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(265, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8973.983740', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1882.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(266, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8356.910569', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1742.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(267, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2019.512195', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1746.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(268, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '12878.048780', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1865.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(269, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14322.764228', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1782.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(270, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8209.756098', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2033.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(271, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8570.731707', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2137.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(272, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '12984.552846', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1381.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(273, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '11764.227642', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1669.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(274, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '12057.723577', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1327.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(275, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '850.406504', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2186.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(276, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1234.146341', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1734.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(277, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2309.756098', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1733.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(278, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1724.390244', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1955.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(279, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8409.756098', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1359.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(280, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '9317.886179', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1945.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(281, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8541.463415', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1371.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(282, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14178.048780', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1739.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(283, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14506.504065', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2003.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(284, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8528.455285', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2106.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(285, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2178.861789', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1573.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(286, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '13969.105691', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1334.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(287, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '15169.105691', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1567.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(288, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '57658.536585', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1843.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(289, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1791.056911', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1657.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(290, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '71026.016260', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1819.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(291, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8742.276423', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1631.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(292, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8484.552846', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1495.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(293, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '10710.569106', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2110.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(294, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '9954.471545', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1401.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(295, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1932.520325', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1915.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(296, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '13393.495935', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1565.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(297, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '10261.788618', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1524.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(298, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '12221.138211', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1687.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(299, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8850.406504', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1455.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(300, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2039.837398', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2055.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(301, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '4544.715447', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2165.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(302, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '5911.382114', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2122.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(303, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '68523.577236', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1346.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(304, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '15491.869919', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2238.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(305, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14982.113821', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1920.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(306, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8134.959350', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1900.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(307, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '13642.276423', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2094.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(308, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '12564.227642', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1507.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(309, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14200.813008', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1548.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(310, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14752.845528', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1917.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(311, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '13452.032520', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1435.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(312, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1332.520325', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1321.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(313, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '11787.804878', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1406.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(314, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2065.853659', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1523.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(315, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2349.593496', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1959.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(316, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2236.585366', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1849.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(317, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1908.130081', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1894.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(318, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2431.707317', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1307.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(319, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '15479.674797', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1580.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(320, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14638.211382', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2085.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(321, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8832.520325', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1874.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(322, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '16139.837398', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1566.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(323, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '10495.934959', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1643.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(324, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1662.601626', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1589.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(325, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1797.560976', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2282.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(326, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1637.398374', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1340.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(327, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2284.552846', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1637.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(328, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '12175.609756', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1934.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(329, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '9538.211382', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1393.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(330, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '6226.016260', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1857.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(331, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1607.317073', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2002.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(332, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '10691.869919', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1911.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(333, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1888.617886', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2201.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(334, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '5621.951220', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1672.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(335, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8982.113821', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1346.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(336, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '13943.089431', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1857.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(337, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '15525.203252', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1948.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(338, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '70302.439024', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2034.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(339, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2366.666667', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1854.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(340, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '15494.308943', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1729.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(341, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14474.796748', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1523.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(342, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14932.520325', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2154.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(343, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '11734.959350', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1572.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(344, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1208.130081', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1766.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(345, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1732.520325', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1505.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(346, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2016.260163', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1718.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(347, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1952.845528', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1941.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(348, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2098.373984', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1842.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(349, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1704.065041', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2039.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(350, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1947.154472', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1814.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(351, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14327.642276', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1475.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(352, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '15243.089431', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1944.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(353, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '13863.414634', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1889.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(354, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '10659.349593', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1924.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(355, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1830.081301', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1871.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(356, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '13435.772358', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2216.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(357, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '10481.300813', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1653.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(358, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '13904.878049', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1912.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(359, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2395.934959', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1561.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(360, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1867.479675', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1488.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(361, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1981.300813', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1702.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(362, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '15568.292683', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2227.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(363, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '6051.219512', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1569.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(364, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '12103.252033', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1607.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(365, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1351.219512', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2145.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(366, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '1821.138211', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1500.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(367, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '9926.829268', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2014.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(368, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '12026.829268', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1989.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(369, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '13534.146341', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1692.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(370, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '8554.471545', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1616.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(371, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14589.430894', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1911.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(372, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '9023.577236', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2103.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(373, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '14877.235772', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1560.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(374, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '13452.032520', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1796.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(375, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '10510.569106', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1732.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(376, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '12079.674797', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '2290.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(377, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '9947.154472', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1311.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(378, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '9485.365854', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1779.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(379, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '15148.780488', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1561.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(380, 0, 0, 25, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '2065.040650', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1633.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(381, 0, 0, 30, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '15405.691057', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1741.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3),
(382, 0, 0, 12, 1, 1, 0, 0, '', '', '0.000000', 0, 1, '15738.211382', '0.000000', '', '0.000000', '0.00', '', '', '', '0.000000', '0.000000', '0.000000', '1402.000000', 2, 0, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 0, 3);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_product_attachment`
--

DROP TABLE IF EXISTS `ps_product_attachment`;
CREATE TABLE `ps_product_attachment` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_attachment` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_product_attribute`
--

DROP TABLE IF EXISTS `ps_product_attribute`;
CREATE TABLE `ps_product_attribute` (
  `id_product_attribute` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `reference` varchar(32) DEFAULT NULL,
  `supplier_reference` varchar(32) DEFAULT NULL,
  `location` varchar(64) DEFAULT NULL,
  `ean13` varchar(13) DEFAULT NULL,
  `upc` varchar(12) DEFAULT NULL,
  `wholesale_price` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `price` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `ecotax` decimal(17,6) NOT NULL DEFAULT 0.000000,
  `quantity` int(10) NOT NULL DEFAULT 0,
  `weight` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `unit_price_impact` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `default_on` tinyint(1) UNSIGNED DEFAULT NULL,
  `minimal_quantity` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `available_date` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_product_attribute_combination`
--

DROP TABLE IF EXISTS `ps_product_attribute_combination`;
CREATE TABLE `ps_product_attribute_combination` (
  `id_attribute` int(10) UNSIGNED NOT NULL,
  `id_product_attribute` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_product_attribute_image`
--

DROP TABLE IF EXISTS `ps_product_attribute_image`;
CREATE TABLE `ps_product_attribute_image` (
  `id_product_attribute` int(10) UNSIGNED NOT NULL,
  `id_image` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_product_attribute_shop`
--

DROP TABLE IF EXISTS `ps_product_attribute_shop`;
CREATE TABLE `ps_product_attribute_shop` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_product_attribute` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL,
  `wholesale_price` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `price` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `ecotax` decimal(17,6) NOT NULL DEFAULT 0.000000,
  `weight` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `unit_price_impact` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `default_on` tinyint(1) UNSIGNED DEFAULT NULL,
  `minimal_quantity` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `available_date` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_product_carrier`
--

DROP TABLE IF EXISTS `ps_product_carrier`;
CREATE TABLE `ps_product_carrier` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_carrier_reference` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_product_country_tax`
--

DROP TABLE IF EXISTS `ps_product_country_tax`;
CREATE TABLE `ps_product_country_tax` (
  `id_product` int(11) NOT NULL,
  `id_country` int(11) NOT NULL,
  `id_tax` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_product_download`
--

DROP TABLE IF EXISTS `ps_product_download`;
CREATE TABLE `ps_product_download` (
  `id_product_download` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `display_filename` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_expiration` datetime DEFAULT NULL,
  `nb_days_accessible` int(10) UNSIGNED DEFAULT NULL,
  `nb_downloadable` int(10) UNSIGNED DEFAULT 1,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `is_shareable` tinyint(1) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_product_group_reduction_cache`
--

DROP TABLE IF EXISTS `ps_product_group_reduction_cache`;
CREATE TABLE `ps_product_group_reduction_cache` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL,
  `reduction` decimal(4,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_product_lang`
--

DROP TABLE IF EXISTS `ps_product_lang`;
CREATE TABLE `ps_product_lang` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT 1,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `description` text DEFAULT NULL,
  `description_short` text DEFAULT NULL,
  `link_rewrite` varchar(128) NOT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_title` varchar(128) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `available_now` varchar(255) DEFAULT NULL,
  `available_later` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_product_lang`
--

INSERT INTO `ps_product_lang` (`id_product`, `id_shop`, `id_lang`, `description`, `description_short`, `link_rewrite`, `meta_description`, `meta_keywords`, `meta_title`, `name`, `available_now`, `available_later`) VALUES
(1, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 121 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ac-3000me', '', '', '', 'AC 3000ME', '', ''),
(2, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 141 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ac-frua', '', '', '', 'AC Frua', '', ''),
(3, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 144 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'alfa-romeo-75', '', '', '', 'Alfa Romeo 75', '', ''),
(4, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 128 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'alfa-romeo-33', '', '', '', 'Alfa Romeo 33', '', ''),
(5, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 126 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'alfa-romeo-33-stradale', '', '', '', 'Alfa Romeo 33 Stradale', '', ''),
(6, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 157 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'alfa-romeo-155', '', '', '', 'Alfa Romeo 155', '', ''),
(7, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 131 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'alfa-romeo-156', '', '', '', 'Alfa Romeo 156', '', ''),
(8, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 161 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'alfa-romeo-alfasud', '', '', '', 'Alfa Romeo Alfasud', '', ''),
(9, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 160 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'alfa-romeo-arna', '', '', '', 'Alfa Romeo Arna', '', ''),
(10, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 201 km/h i przyspieszczenie 7 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'alfa-romeo-brera', '', '', '', 'Alfa Romeo Brera', '', ''),
(11, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 181 km/h i przyspieszczenie 7 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'alfa-romeo-gt', '', '', '', 'Alfa Romeo GT', '', ''),
(12, 1, 1, 'Samochód posiada automatyczną skrzynię biegów. Pojazd osiąga maksymalna prędkość 121 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'alfa-romeo-montreal', '', '', '', 'Alfa Romeo Montreal', '', ''),
(13, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 131 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'alfa-romeo-spider', '', '', '', 'Alfa Romeo Spider', '', ''),
(14, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 125 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'alpine-a106', '', '', '', 'Alpine A106', '', ''),
(15, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 156 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'alpine-a110', '', '', '', 'Alpine A110', '', ''),
(16, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 124 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'alpine-a310', '', '', '', 'Alpine A310', '', ''),
(17, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 152 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada skórzane obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'amc-gremlin', '', '', '', 'AMC Gremlin', '', ''),
(18, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 129 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'amc-hornet', '', '', '', 'AMC Hornet', '', ''),
(19, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 147 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'audi-a4', '', '', '', 'Audi A4', '', ''),
(20, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 162 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'audi-r8', '', '', '', 'Audi R8', '', ''),
(21, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 121 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'audi-a1', '', '', '', 'Audi A1', '', ''),
(22, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 131 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'austin-allegro', '', '', '', 'Austin Allegro', '', ''),
(23, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 169 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'austin-maestro', '', '', '', 'Austin Maestro', '', ''),
(24, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 138 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'austin-metro', '', '', '', 'Austin Metro', '', ''),
(25, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 138 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'austin-montego', '', '', '', 'Austin Montego', '', ''),
(26, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 161 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'autobianchi-a111', '', '', '', 'Autobianchi A111', '', ''),
(27, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 160 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'autobianchi-a112', '', '', '', 'Autobianchi A112', '', ''),
(28, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 122 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'lancia-y10', '', '', '', 'Lancia Y10', '', ''),
(29, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 137 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'bmc-ado16', '', '', '', 'BMC ADO16', '', ''),
(30, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 166 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'bmc-ado17', '', '', '', 'BMC ADO17', '', ''),
(31, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 128 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'bmw-328', '', '', '', 'BMW 328', '', ''),
(32, 1, 1, 'Samochód posiada automatyczną skrzynię biegów. Pojazd osiąga maksymalna prędkość 139 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada skórzane obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'bmw-507', '', '', '', 'BMW 507', '', ''),
(33, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 164 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'bmw-503', '', '', '', 'BMW 503', '', ''),
(34, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 142 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'bmw-3200-cs', '', '', '', 'BMW 3200 CS', '', ''),
(35, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 130 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'bmw-new-class', '', '', '', 'BMW New Class', '', ''),
(36, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 130 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'bmw-i3', '', '', '', 'BMW i3', '', ''),
(37, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 162 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'bmw-i8', '', '', '', 'BMW i8', '', ''),
(38, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 138 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'bmw-m1', '', '', '', 'BMW M1', '', ''),
(39, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 155 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'bmw-z1', '', '', '', 'BMW Z1', '', ''),
(40, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 115 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'bmw-z8', '', '', '', 'BMW Z8', '', ''),
(41, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 130 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'bmw-3-series', '', '', '', 'BMW 3 Series', '', ''),
(42, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 129 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'bmw-5-series', '', '', '', 'BMW 5 Series', '', ''),
(43, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 137 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'bmw-8-series', '', '', '', 'BMW 8 Series', '', ''),
(44, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 163 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'bmw-x5', '', '', '', 'BMW X5', '', ''),
(45, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 163 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'bugatti-type-30', '', '', '', 'Bugatti Type 30', '', ''),
(46, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 150 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'bugatti-type-35', '', '', '', 'Bugatti Type 35', '', ''),
(47, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 143 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'bugatti-type-37', '', '', '', 'Bugatti Type 37', '', ''),
(48, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 164 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'bugatti-type-38', '', '', '', 'Bugatti Type 38', '', ''),
(49, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 128 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'bugatti-type-39', '', '', '', 'Bugatti Type 39', '', ''),
(50, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 162 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'bugatti-type-40', '', '', '', 'Bugatti Type 40', '', ''),
(51, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 136 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada skórzane obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'bugatti-type-41', '', '', '', 'Bugatti Type 41', '', ''),
(52, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 151 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'bugatti-type-50', '', '', '', 'Bugatti Type 50', '', ''),
(53, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 157 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'bugatti-type-51', '', '', '', 'Bugatti Type 51', '', ''),
(54, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 151 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'bugatti-type-55', '', '', '', 'Bugatti Type 55', '', ''),
(55, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 122 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'bugatti-type-57', '', '', '', 'Bugatti Type 57', '', ''),
(56, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 147 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'bugatti-type-101', '', '', '', 'Bugatti Type 101', '', ''),
(57, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 137 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'bugatti-eb110', '', '', '', 'Bugatti EB110', '', ''),
(58, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 171 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'bugatti-veyron', '', '', '', 'Bugatti Veyron', '', ''),
(59, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 121 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'buick-apollo', '', '', '', 'Buick Apollo', '', ''),
(60, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 153 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'buick-centurion', '', '', '', 'Buick Centurion', '', ''),
(61, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 166 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'buick-electra', '', '', '', 'Buick Electra', '', ''),
(62, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 151 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'buick-invicta', '', '', '', 'Buick Invicta', '', ''),
(63, 1, 1, 'Samochód posiada automatyczną skrzynię biegów. Pojazd osiąga maksymalna prędkość 137 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'buick-lesabre', '', '', '', 'Buick LeSabre', '', ''),
(64, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 148 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'buick-riviera', '', '', '', 'Buick Riviera', '', ''),
(65, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 142 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'buick-wildcat', '', '', '', 'Buick Wildcat', '', ''),
(66, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 134 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada skórzane obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'byd-e6', '', '', '', 'BYD e6', '', ''),
(67, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 205 km/h i przyspieszczenie 5 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'byd-f3dm', '', '', '', 'BYD F3DM', '', ''),
(68, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 153 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'byd-qin', '', '', '', 'BYD Qin', '', ''),
(69, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 160 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'cadillac-cimarron', '', '', '', 'Cadillac Cimarron', '', ''),
(70, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 154 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'cadillac-de-ville', '', '', '', 'Cadillac De Ville', '', ''),
(71, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 185 km/h i przyspieszczenie 7 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'cadillac-elr', '', '', '', 'Cadillac ELR', '', ''),
(72, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 124 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada skórzane obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'checker-marathon', '', '', '', 'Checker Marathon', '', ''),
(73, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 122 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'chevrolet-caprice', '', '', '', 'Chevrolet Caprice', '', ''),
(74, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 144 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'chevrolet-cavalier', '', '', '', 'Chevrolet Cavalier', '', ''),
(75, 1, 1, 'Samochód posiada automatyczną skrzynię biegów. Pojazd osiąga maksymalna prędkość 138 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'chevrolet-citation', '', '', '', 'Chevrolet Citation', '', ''),
(76, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 163 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'chevrolet-corvair', '', '', '', 'Chevrolet Corvair', '', ''),
(77, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 164 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'chevrolet-corvette', '', '', '', 'Chevrolet Corvette', '', ''),
(78, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 152 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'chevrolet-cruze', '', '', '', 'Chevrolet Cruze', '', ''),
(79, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 139 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'chevrolet-monza', '', '', '', 'Chevrolet Monza', '', ''),
(80, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 167 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'chevrolet-opala', '', '', '', 'Chevrolet Opala', '', ''),
(81, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 163 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'chevrolet-suburban', '', '', '', 'Chevrolet Suburban', '', ''),
(82, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 129 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'chevrolet-vega', '', '', '', 'Chevrolet Vega', '', ''),
(83, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 139 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada skórzane obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'chevrolet-volt', '', '', '', 'Chevrolet Volt', '', ''),
(84, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 163 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'chrysler-newport', '', '', '', 'Chrysler Newport', '', ''),
(85, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 150 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'chrysler-tc-by-maserati', '', '', '', 'Chrysler TC by Maserati', '', ''),
(86, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 125 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'chrysler-minivans', '', '', '', 'Chrysler minivans', '', ''),
(87, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 156 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'chrysler-town-country', '', '', '', 'Chrysler Town & Country', '', ''),
(88, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 160 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'continental-mark-ii', '', '', '', 'Continental Mark II', '', ''),
(89, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 142 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'daihatsu-copen', '', '', '', 'Daihatsu Copen', '', ''),
(90, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 160 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'delorean-dmc-12', '', '', '', 'DeLorean DMC-12', '', ''),
(91, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 140 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'de-tomaso-deauville', '', '', '', 'De Tomaso Deauville', '', ''),
(92, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 158 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'de-tomaso-longchamp', '', '', '', 'De Tomaso Longchamp', '', ''),
(93, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 140 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'de-tomaso-mangusta', '', '', '', 'De Tomaso Mangusta', '', ''),
(94, 1, 1, 'Samochód posiada automatyczną skrzynię biegów. Pojazd osiąga maksymalna prędkość 134 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'de-tomaso-pantera', '', '', '', 'De Tomaso Pantera', '', ''),
(95, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 142 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'de-tomaso-vallelunga', '', '', '', 'De Tomaso Vallelunga', '', ''),
(96, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 136 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'desoto-custom', '', '', '', 'DeSoto Custom', '', ''),
(97, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 151 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'dodge-coronet', '', '', '', 'Dodge Coronet', '', ''),
(98, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 128 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'dodge-stealth', '', '', '', 'Dodge Stealth', '', ''),
(99, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 163 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'dodge-viper', '', '', '', 'Dodge Viper', '', ''),
(100, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 133 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'eagle-talon', '', '', '', 'Eagle Talon', '', ''),
(101, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 120 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'edsel-bermuda', '', '', '', 'Edsel Bermuda', '', ''),
(102, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 134 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'edsel-corsair', '', '', '', 'Edsel Corsair', '', ''),
(103, 1, 1, 'Samochód posiada automatyczną skrzynię biegów. Pojazd osiąga maksymalna prędkość 167 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'edsel-citation', '', '', '', 'Edsel Citation', '', ''),
(104, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 136 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'edsel-ranger', '', '', '', 'Edsel Ranger', '', ''),
(105, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 133 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'edsel-villager', '', '', '', 'Edsel Villager', '', ''),
(106, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 156 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'facel-vega-excellence', '', '', '', 'Facel Vega Excellence', '', ''),
(107, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 132 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ferrari-166-s', '', '', '', 'Ferrari 166 S', '', ''),
(108, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 145 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ferrari-250-gto', '', '', '', 'Ferrari 250 GTO', '', ''),
(109, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 163 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ferrari-250-gt-lusso', '', '', '', 'Ferrari 250 GT Lusso', '', ''),
(110, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 156 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ferrari-275', '', '', '', 'Ferrari 275', '', ''),
(111, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 149 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ferrari-dino', '', '', '', 'Ferrari Dino', '', ''),
(112, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 164 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ferrari-mondial', '', '', '', 'Ferrari Mondial', '', ''),
(113, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 132 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ferrari-testarossa', '', '', '', 'Ferrari Testarossa', '', ''),
(114, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 137 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ferrari-288-gto', '', '', '', 'Ferrari 288 GTO', '', ''),
(115, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 122 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ferrari-f40', '', '', '', 'Ferrari F40', '', ''),
(116, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 131 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ferrari-348', '', '', '', 'Ferrari 348', '', ''),
(117, 1, 1, 'Samochód posiada automatyczną skrzynię biegów. Pojazd osiąga maksymalna prędkość 166 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ferrari-456', '', '', '', 'Ferrari 456', '', ''),
(118, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 164 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ferrari-f355', '', '', '', 'Ferrari F355', '', ''),
(119, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 122 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ferrari-f50', '', '', '', 'Ferrari F50', '', ''),
(120, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 133 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ferrari-550', '', '', '', 'Ferrari 550', '', ''),
(121, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 182 km/h i przyspieszczenie 5 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ferrari-575m-maranello', '', '', '', 'Ferrari 575M Maranello', '', ''),
(122, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 173 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ferrari-360', '', '', '', 'Ferrari 360', '', ''),
(123, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 145 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'enzo-ferrari', '', '', '', 'Enzo Ferrari', '', ''),
(124, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 188 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ferrari-f430', '', '', '', 'Ferrari F430', '', ''),
(125, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 146 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ferrari-ff', '', '', '', 'Ferrari FF', '', ''),
(126, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 139 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'laferrari', '', '', '', 'LaFerrari', '', ''),
(127, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 124 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'fiat-124', '', '', '', 'Fiat 124', '', ''),
(128, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 146 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'fiat-126', '', '', '', 'Fiat 126', '', ''),
(129, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 166 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'fiat-127', '', '', '', 'Fiat 127', '', ''),
(130, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 123 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'fiat-500', '', '', '', 'Fiat 500', '', ''),
(131, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 162 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'fiat-panda', '', '', '', 'Fiat Panda', '', ''),
(132, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 131 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'fiat-punto', '', '', '', 'Fiat Punto', '', ''),
(133, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 127 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'fiat-uno', '', '', '', 'Fiat Uno', '', ''),
(134, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 155 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'fiat-131', '', '', '', 'Fiat 131', '', ''),
(135, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 133 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada skórzane obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'fisker-karma', '', '', '', 'Fisker Karma', '', ''),
(136, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 124 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ford-aerostar', '', '', '', 'Ford Aerostar', '', ''),
(137, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 132 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ford-ce14-platform', '', '', '', 'Ford CE14 platform', '', ''),
(138, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 169 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ford-cortina', '', '', '', 'Ford Cortina', '', ''),
(139, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 148 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ford-crown-victoria', '', '', '', 'Ford Crown Victoria', '', ''),
(140, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 159 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada skórzane obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ford-d186-platform', '', '', '', 'Ford D186 platform', '', ''),
(141, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 167 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ford-e-series', '', '', '', 'Ford E-Series', '', ''),
(142, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 136 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ford-explorer', '', '', '', 'Ford Explorer', '', ''),
(143, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 131 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ford-f-series', '', '', '', 'Ford F-Series', '', ''),
(144, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 202 km/h i przyspieszczenie 5 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ford-falcon', '', '', '', 'Ford Falcon', '', ''),
(145, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 167 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ford-fiesta', '', '', '', 'Ford Fiesta', '', ''),
(146, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 153 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ford-fiesta', '', '', '', 'Ford Fiesta', '', ''),
(147, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 158 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ford-focus', '', '', '', 'Ford Focus', '', ''),
(148, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 148 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ford-granada-north-america', '', '', '', 'Ford Granada (North America)', '', ''),
(149, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 143 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ford-gt', '', '', '', 'Ford GT', '', ''),
(150, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 145 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ford-model-a', '', '', '', 'Ford Model A', '', ''),
(151, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 165 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ford-model-t', '', '', '', 'Ford Model T', '', ''),
(152, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 135 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ford-mustang', '', '', '', 'Ford Mustang', '', ''),
(153, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 145 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ford-ranchero', '', '', '', 'Ford Ranchero', '', ''),
(154, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 159 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ford-rs200', '', '', '', 'Ford RS200', '', ''),
(155, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 132 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ford-taurus', '', '', '', 'Ford Taurus', '', ''),
(156, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 137 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ford-tempo', '', '', '', 'Ford Tempo', '', ''),
(157, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 129 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ford-transit', '', '', '', 'Ford Transit', '', ''),
(158, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 112 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'fso-polonez', '', '', '', 'FSO Polonez', '', ''),
(159, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 160 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'gm-b-platform', '', '', '', 'GM B platform', '', ''),
(160, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 167 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'gm-b-platform', '', '', '', 'GM B platform', '', ''),
(161, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 124 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'gm-b-platform', '', '', '', 'GM B platform', '', ''),
(162, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 155 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'gm-b-platform', '', '', '', 'GM B platform', '', ''),
(163, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 135 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'gm-b-platform', '', '', '', 'GM B platform', '', ''),
(164, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 165 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'gm-d-platform', '', '', '', 'GM D platform', '', ''),
(165, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 126 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'gm-j-platform', '', '', '', 'GM J platform', '', ''),
(166, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 197 km/h i przyspieszczenie 6 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'gm-w-platform', '', '', '', 'GM W platform', '', ''),
(167, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 183 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'hindustan-ambassador', '', '', '', 'Hindustan Ambassador', '', ''),
(168, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 141 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'holden-commodore', '', '', '', 'Holden Commodore', '', ''),
(169, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 146 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'honda-accord', '', '', '', 'Honda Accord', '', ''),
(170, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 162 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'honda-beat', '', '', '', 'Honda Beat', '', ''),
(171, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 162 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'honda-civic', '', '', '', 'Honda Civic', '', ''),
(172, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 138 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'honda-cr-v', '', '', '', 'Honda CR-V', '', ''),
(173, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 151 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'honda-fit', '', '', '', 'Honda Fit', '', ''),
(174, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 162 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'honda-nsx', '', '', '', 'Honda NSX', '', ''),
(175, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 181 km/h i przyspieszczenie 6 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'honda-prelude', '', '', '', 'Honda Prelude', '', ''),
(176, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 123 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'honda-s600', '', '', '', 'Honda S600', '', ''),
(177, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 129 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'honda-s500', '', '', '', 'Honda S500', '', ''),
(178, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 123 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'honda-s800', '', '', '', 'Honda S800', '', ''),
(179, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 149 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'honda-s2000', '', '', '', 'Honda S2000', '', ''),
(180, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 123 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'hyundai-elantra', '', '', '', 'Hyundai Elantra', '', ''),
(181, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 158 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'hyundai-accent', '', '', '', 'Hyundai Accent', '', ''),
(182, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 158 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'hyundai-sonata', '', '', '', 'Hyundai Sonata', '', ''),
(183, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 163 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'imperial-crown', '', '', '', 'Imperial Crown', '', ''),
(184, 1, 1, 'Samochód posiada automatyczną skrzynię biegów. Pojazd osiąga maksymalna prędkość 130 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'jaguar-xk120', '', '', '', 'Jaguar XK120', '', ''),
(185, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 148 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'jaguar-c-type', '', '', '', 'Jaguar C-Type', '', ''),
(186, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 151 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'jaguar-d-type', '', '', '', 'Jaguar D-Type', '', ''),
(187, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 140 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'jaguar-mark-1', '', '', '', 'Jaguar Mark 1', '', ''),
(188, 1, 1, 'Samochód posiada automatyczną skrzynię biegów. Pojazd osiąga maksymalna prędkość 132 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'jaguar-xk150', '', '', '', 'Jaguar XK150', '', ''),
(189, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 152 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'jaguar-mark-2', '', '', '', 'Jaguar Mark 2', '', ''),
(190, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 121 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'jaguar-e-type', '', '', '', 'Jaguar E-Type', '', ''),
(191, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 145 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'jaguar-xj', '', '', '', 'Jaguar XJ', '', ''),
(192, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 161 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'jaguar-xjs', '', '', '', 'Jaguar XJS', '', ''),
(193, 1, 1, 'Samochód posiada automatyczną skrzynię biegów. Pojazd osiąga maksymalna prędkość 121 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'jaguar-xjr-15', '', '', '', 'Jaguar XJR-15', '', ''),
(194, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 143 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'jaguar-xj220', '', '', '', 'Jaguar XJ220', '', ''),
(195, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 189 km/h i przyspieszczenie 6 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'jaguar-xk', '', '', '', 'Jaguar XK', '', ''),
(196, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 192 km/h i przyspieszczenie 6 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'jaguar-x-type', '', '', '', 'Jaguar X-Type', '', ''),
(197, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 151 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'jeep-cherokee-xj', '', '', '', 'Jeep Cherokee (XJ)', '', ''),
(198, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 122 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada skórzane obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'jeep-cj', '', '', '', 'Jeep CJ', '', ''),
(199, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 138 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'jeep-cherokee-kl', '', '', '', 'Jeep Cherokee (KL)', '', ''),
(200, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 142 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'jeep-renegade', '', '', '', 'Jeep Renegade', '', ''),
(201, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 163 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'la-marquise', '', '', '', 'La Marquise', '', ''),
(202, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 137 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada skórzane obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'lagonda-rapier', '', '', '', 'Lagonda Rapier', '', ''),
(203, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 126 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'lagonda-lg6', '', '', '', 'Lagonda LG6', '', ''),
(204, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 168 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'lagonda-v12', '', '', '', 'Lagonda V12', '', ''),
(205, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 151 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'lagonda-26-litre', '', '', '', 'Lagonda 2.6-Litre', '', ''),
(206, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 132 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'lagonda-3-litre', '', '', '', 'Lagonda 3-Litre', '', ''),
(207, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 148 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada skórzane obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'lagonda-rapide', '', '', '', 'Lagonda Rapide', '', ''),
(208, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 141 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'lamborghini-350gt', '', '', '', 'Lamborghini 350GT', '', ''),
(209, 1, 1, 'Samochód posiada automatyczną skrzynię biegów. Pojazd osiąga maksymalna prędkość 141 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'lamborghini-400gt', '', '', '', 'Lamborghini 400GT', '', ''),
(210, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 149 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'lamborghini-aventador', '', '', '', 'Lamborghini Aventador', '', ''),
(211, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 129 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'lamborghini-countach', '', '', '', 'Lamborghini Countach', '', ''),
(212, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 113 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'lamborghini-diablo', '', '', '', 'Lamborghini Diablo', '', ''),
(213, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 132 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'lamborghini-espada', '', '', '', 'Lamborghini Espada', '', ''),
(214, 1, 1, 'Samochód posiada automatyczną skrzynię biegów. Pojazd osiąga maksymalna prędkość 147 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'lamborghini-gallardo', '', '', '', 'Lamborghini Gallardo', '', ''),
(215, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 156 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'lamborghini-islero', '', '', '', 'Lamborghini Islero', '', ''),
(216, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 161 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'lamborghini-jalpa', '', '', '', 'Lamborghini Jalpa', '', ''),
(217, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 162 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'lamborghini-jarama', '', '', '', 'Lamborghini Jarama', '', ''),
(218, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 124 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'lamborghini-silhouette', '', '', '', 'Lamborghini Silhouette', '', ''),
(219, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 164 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'lamborghini-urraco', '', '', '', 'Lamborghini Urraco', '', ''),
(220, 1, 1, 'Samochód posiada automatyczną skrzynię biegów. Pojazd osiąga maksymalna prędkość 124 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'lancia-stratos', '', '', '', 'Lancia Stratos', '', ''),
(221, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 201 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'lancia-montecarlo', '', '', '', 'Lancia Montecarlo', '', ''),
(222, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 133 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'lancia-dedra', '', '', '', 'Lancia Dedra', '', ''),
(223, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 148 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'lancia-ypsilon', '', '', '', 'Lancia Ypsilon', '', ''),
(224, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 164 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'range-rover-evoque', '', '', '', 'Range Rover Evoque', '', ''),
(225, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 131 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'lexus-lfa', '', '', '', 'Lexus LFA', '', ''),
(226, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 118 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'lincoln-continental', '', '', '', 'Lincoln Continental', '', ''),
(227, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 153 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'lincoln-town-car', '', '', '', 'Lincoln Town Car', '', ''),
(228, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 134 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'lincoln-versailles', '', '', '', 'Lincoln Versailles', '', ''),
(229, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 168 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'lola-t70', '', '', '', 'Lola T70', '', ''),
(230, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 165 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'lotus-elise', '', '', '', 'Lotus Elise', '', ''),
(231, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 148 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'mclaren-f1', '', '', '', 'McLaren F1', '', ''),
(232, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 125 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'mclaren-p1', '', '', '', 'McLaren P1', '', ''),
(233, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 148 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'mclaren-12c', '', '', '', 'McLaren 12C', '', ''),
(234, 1, 1, 'Samochód posiada automatyczną skrzynię biegów. Pojazd osiąga maksymalna prędkość 202 km/h i przyspieszczenie 8 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'maruti-800', '', '', '', 'Maruti 800', '', ''),
(235, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 135 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'maserati-biturbo', '', '', '', 'Maserati Biturbo', '', ''),
(236, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 129 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'maserati-mc12', '', '', '', 'Maserati MC12', '', ''),
(237, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 133 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'maybach-57-and-62', '', '', '', 'Maybach 57 and 62', '', ''),
(238, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 168 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'matra-rancho', '', '', '', 'Matra Rancho', '', ''),
(239, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 151 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'mazda-3', '', '', '', 'Mazda 3', '', ''),
(240, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 168 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'mazda-6', '', '', '', 'Mazda 6', '', ''),
(241, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 179 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'mazda-familia', '', '', '', 'Mazda Familia', '', ''),
(242, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 120 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'mazda-mpv', '', '', '', 'Mazda MPV', '', ''),
(243, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 124 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'mazda-mx-5', '', '', '', 'Mazda MX-5', '', ''),
(244, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 149 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'mazda-r360', '', '', '', 'Mazda R360', '', ''),
(245, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 204 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'mazda-rx-7', '', '', '', 'Mazda RX-7', '', ''),
(246, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 163 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'mazda-rx-8', '', '', '', 'Mazda RX-8', '', ''),
(247, 1, 1, 'Samochód posiada automatyczną skrzynię biegów. Pojazd osiąga maksymalna prędkość 149 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'mercedes-benz-600', '', '', '', 'Mercedes-Benz 600', '', ''),
(248, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 132 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'mercedes-benz-c-class', '', '', '', 'Mercedes-Benz C-Class', '', ''),
(249, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 129 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'mercedes-benz-clk-gtr', '', '', '', 'Mercedes-Benz CLK GTR', '', ''),
(250, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 143 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'mercedes-benz-slr-mclaren', '', '', '', 'Mercedes-Benz SLR McLaren', '', ''),
(251, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 158 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'mercedes-benz-s-class', '', '', '', 'Mercedes-Benz S-Class', '', ''),
(252, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 145 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'mercedes-benz-sl-class', '', '', '', 'Mercedes-Benz SL-Class', '', ''),
(253, 1, 1, 'Samochód posiada automatyczną skrzynię biegów. Pojazd osiąga maksymalna prędkość 134 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'mercedes-benz-w123', '', '', '', 'Mercedes-Benz W123', '', ''),
(254, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 152 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'mercedes-benz-w201', '', '', '', 'Mercedes-Benz W201', '', ''),
(255, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 129 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'mercury-grand-marquis', '', '', '', 'Mercury Grand Marquis', '', ''),
(256, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 148 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'mercury-monarch', '', '', '', 'Mercury Monarch', '', ''),
(257, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 126 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'mercury-monterey', '', '', '', 'Mercury Monterey', '', ''),
(258, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 126 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'mercury-sable', '', '', '', 'Mercury Sable', '', ''),
(259, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 166 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'messerschmitt-kr175', '', '', '', 'Messerschmitt KR175', '', ''),
(260, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 134 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'messerschmitt-kr200', '', '', '', 'Messerschmitt KR200', '', ''),
(261, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 142 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'mg-mgb', '', '', '', 'MG MGB', '', ''),
(262, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 121 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'mg-midget', '', '', '', 'MG Midget', '', ''),
(263, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 114 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'mg-xpower-sv', '', '', '', 'MG XPower SV', '', ''),
(264, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 165 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'mini', '', '', '', 'Mini', '', ''),
(265, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 165 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'mitsubishi-carisma', '', '', '', 'Mitsubishi Carisma', '', ''),
(266, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 167 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'mitsubishi-galant', '', '', '', 'Mitsubishi Galant', '', ''),
(267, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 169 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'mitsubishi-gto', '', '', '', 'Mitsubishi GTO', '', ''),
(268, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 133 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'mitsubishi-lancer', '', '', '', 'Mitsubishi Lancer', '', ''),
(269, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 134 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'mitsubishi-l200', '', '', '', 'Mitsubishi L200', '', ''),
(270, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 131 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'mitsubishi-pajero', '', '', '', 'Mitsubishi Pajero', '', ''),
(271, 1, 1, 'Samochód posiada automatyczną skrzynię biegów. Pojazd osiąga maksymalna prędkość 120 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'morris-ital', '', '', '', 'Morris Ital', '', ''),
(272, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 126 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'morris-marina', '', '', '', 'Morris Marina', '', ''),
(273, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 165 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'morris-minor', '', '', '', 'Morris Minor', '', ''),
(274, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 155 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'nash-statesman', '', '', '', 'Nash Statesman', '', ''),
(275, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 120 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'nissan-be-1', '', '', '', 'Nissan Be-1', '', ''),
(276, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 125 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'nissan-s-cargo', '', '', '', 'Nissan S-Cargo', '', ''),
(277, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 164 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'nissan-figaro', '', '', '', 'Nissan Figaro', '', ''),
(278, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 138 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'nissan-leaf', '', '', '', 'Nissan Leaf', '', ''),
(279, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 158 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'nissan-maxima', '', '', '', 'Nissan Maxima', '', ''),
(280, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 133 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'nissan-micra', '', '', '', 'Nissan Micra', '', ''),
(281, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 162 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'oldsmobile-aurora', '', '', '', 'Oldsmobile Aurora', '', ''),
(282, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 141 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'oldsmobile-cutlass', '', '', '', 'Oldsmobile Cutlass', '', ''),
(283, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 141 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'oldsmobile-88', '', '', '', 'Oldsmobile 88', '', ''),
(284, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 138 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'opel-ascona', '', '', '', 'Opel Ascona', '', ''),
(285, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 148 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'opel-astra', '', '', '', 'Opel Astra', '', ''),
(286, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 160 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'opel-corsa', '', '', '', 'Opel Corsa', '', ''),
(287, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 153 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'opel-vectra', '', '', '', 'Opel Vectra', '', ''),
(288, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 178 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'packard-eight', '', '', '', 'Packard Eight', '', ''),
(289, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 124 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'pagani-huayra', '', '', '', 'Pagani Huayra', '', ''),
(290, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 135 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'panoz-roadster', '', '', '', 'Panoz Roadster', '', ''),
(291, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 147 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'peel-p50', '', '', '', 'Peel P50', '', ''),
(292, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 136 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'peel-trident', '', '', '', 'Peel Trident', '', ''),
(293, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 156 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'peugeot-204', '', '', '', 'Peugeot 204', '', ''),
(294, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 147 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'peugeot-205', '', '', '', 'Peugeot 205', '', ''),
(295, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 148 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'peugeot-206', '', '', '', 'Peugeot 206', '', ''),
(296, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 115 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'peugeot-504', '', '', '', 'Peugeot 504', '', ''),
(297, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 131 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'plymouth-fury', '', '', '', 'Plymouth Fury', '', ''),
(298, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 150 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada skórzane obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'plymouth-voyager', '', '', '', 'Plymouth Voyager', '', ''),
(299, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 127 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'pontiac-astre', '', '', '', 'Pontiac Astre', '', ''),
(300, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 120 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'pontiac-aztek', '', '', '', 'Pontiac Aztek', '', ''),
(301, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 183 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'pontiac-bonneville', '', '', '', 'Pontiac Bonneville', '', ''),
(302, 1, 1, 'Samochód posiada automatyczną skrzynię biegów. Pojazd osiąga maksymalna prędkość 152 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'pontiac-firebird', '', '', '', 'Pontiac Firebird', '', ''),
(303, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 132 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada skórzane obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'pontiac-grand-am', '', '', '', 'Pontiac Grand Am', '', ''),
(304, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 123 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'porsche-356', '', '', '', 'Porsche 356', '', ''),
(305, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 122 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'porsche-911', '', '', '', 'Porsche 911', '', ''),
(306, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 165 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'porsche-914', '', '', '', 'Porsche 914', '', ''),
(307, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 131 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'porsche-917', '', '', '', 'Porsche 917', '', ''),
(308, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 165 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'porsche-924', '', '', '', 'Porsche 924', '', ''),
(309, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 128 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'porsche-928', '', '', '', 'Porsche 928', '', ''),
(310, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 137 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'porsche-944', '', '', '', 'Porsche 944', '', ''),
(311, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 138 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'porsche-956', '', '', '', 'Porsche 956', '', ''),
(312, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 122 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'porsche-959', '', '', '', 'Porsche 959', '', ''),
(313, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 138 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'porsche-962', '', '', '', 'Porsche 962', '', ''),
(314, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 156 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'porsche-968', '', '', '', 'Porsche 968', '', ''),
(315, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 139 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'porsche-boxster', '', '', '', 'Porsche Boxster', '', ''),
(316, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 128 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'porsche-cayenne', '', '', '', 'Porsche Cayenne', '', ''),
(317, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 168 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'porsche-carrera-gt', '', '', '', 'Porsche Carrera GT', '', ''),
(318, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 158 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'porsche-918', '', '', '', 'Porsche 918', '', ''),
(319, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 153 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'rambler-classic', '', '', '', 'Rambler Classic', '', ''),
(320, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 128 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'renault-4', '', '', '', 'Renault 4', '', ''),
(321, 1, 1, 'Samochód posiada automatyczną skrzynię biegów. Pojazd osiąga maksymalna prędkość 127 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'renault-4cv', '', '', '', 'Renault 4CV', '', ''),
(322, 1, 1, 'Samochód posiada automatyczną skrzynię biegów. Pojazd osiąga maksymalna prędkość 124 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'renault-dauphine', '', '', '', 'Renault Dauphine', '', ''),
(323, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 122 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'renault-5', '', '', '', 'Renault 5', '', ''),
(324, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 162 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'renault-clio', '', '', '', 'Renault Clio', '', ''),
(325, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 125 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'renault-sport-spider', '', '', '', 'Renault Sport Spider', '', ''),
(326, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 147 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'renault-twingo', '', '', '', 'Renault Twingo', '', ''),
(327, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 158 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'renault-zoe', '', '', '', 'Renault Zoe', '', ''),
(328, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 150 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'rover-25', '', '', '', 'Rover 25', '', ''),
(329, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 180 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'rover-45', '', '', '', 'Rover 45', '', ''),
(330, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 200 km/h i przyspieszczenie 5 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'rover-75', '', '', '', 'Rover 75', '', ''),
(331, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 148 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'rover-800', '', '', '', 'Rover 800', '', ''),
(332, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 159 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'saab-900', '', '', '', 'Saab 900', '', ''),
(333, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 147 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'saleen-s7', '', '', '', 'Saleen S7', '', ''),
(334, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 140 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada skórzane obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'saturn-s-series', '', '', '', 'Saturn S-Series', '', ''),
(335, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 161 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'seat-ibiza', '', '', '', 'SEAT Ibiza', '', ''),
(336, 1, 1, 'Samochód posiada automatyczną skrzynię biegów. Pojazd osiąga maksymalna prędkość 168 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'simca-1000', '', '', '', 'Simca 1000', '', ''),
(337, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 147 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'simca-1100', '', '', '', 'Simca 1100', '', ''),
(338, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 157 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'studebaker-champion', '', '', '', 'Studebaker Champion', '', ''),
(339, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 132 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'smart-fortwo', '', '', '', 'Smart Fortwo', '', ''),
(340, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 110 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'smart-roadster', '', '', '', 'Smart Roadster', '', ''),
(341, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 160 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'ss-1', '', '', '', 'SS 1', '', ''),
(342, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 138 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'subaru-360', '', '', '', 'Subaru 360', '', ''),
(343, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 152 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada skórzane obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'subaru-alcyone', '', '', '', 'Subaru Alcyone', '', ''),
(344, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 148 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'subaru-legacy', '', '', '', 'Subaru Legacy', '', ''),
(345, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 153 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'suzuki-cappuccino', '', '', '', 'Suzuki Cappuccino', '', ''),
(346, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 144 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'suzuki-wagon-r', '', '', '', 'Suzuki Wagon R', '', ''),
(347, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 153 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'suzuki-swift', '', '', '', 'Suzuki Swift', '', ''),
(348, 1, 1, 'Samochód posiada automatyczną skrzynię biegów. Pojazd osiąga maksymalna prędkość 151 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'tesla-model-3', '', '', '', 'Tesla Model 3', '', ''),
(349, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 135 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'tesla-model-s', '', '', '', 'Tesla Model S', '', ''),
(350, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 163 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'tesla-model-x', '', '', '', 'Tesla Model X', '', ''),
(351, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 142 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'toyota-2000gt', '', '', '', 'Toyota 2000GT', '', ''),
(352, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 130 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada skórzane obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'toyota-camry', '', '', '', 'Toyota Camry', '', ''),
(353, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 140 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'toyota-celica', '', '', '', 'Toyota Celica', '', ''),
(354, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 124 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'toyota-corolla', '', '', '', 'Toyota Corolla', '', ''),
(355, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 136 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'toyota-curren', '', '', '', 'Toyota Curren', '', ''),
(356, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 147 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'toyota-hilux', '', '', '', 'Toyota Hilux', '', ''),
(357, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 161 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'toyota-ipsum', '', '', '', 'Toyota Ipsum', '', ''),
(358, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 131 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'toyota-land-cruiser', '', '', '', 'Toyota Land Cruiser', '', ''),
(359, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 124 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'toyota-mirai', '', '', '', 'Toyota Mirai', '', ''),
(360, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 152 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'toyota-prius', '', '', '', 'Toyota Prius', '', ''),
(361, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 121 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'toyota-rav4', '', '', '', 'Toyota RAV4', '', ''),
(362, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 149 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'toyota-sports-800', '', '', '', 'Toyota Sports 800', '', ''),
(363, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 150 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'toyota-supra', '', '', '', 'Toyota Supra', '', ''),
(364, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 167 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'vauxhall-viva', '', '', '', 'Vauxhall Viva', '', ''),
(365, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 155 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'vector-w8', '', '', '', 'Vector W8', '', ''),
(366, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 166 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'vector-m12', '', '', '', 'Vector M12', '', ''),
(367, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 170 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'volkswagen-beetle', '', '', '', 'Volkswagen Beetle', '', ''),
(368, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 160 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'volkswagen-gol', '', '', '', 'Volkswagen Gol', '', ''),
(369, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 139 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'volkswagen-golf', '', '', '', 'Volkswagen Golf', '', ''),
(370, 1, 1, 'Samochód posiada automatyczną skrzynię biegów. Pojazd osiąga maksymalna prędkość 139 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'volkswagen-jetta', '', '', '', 'Volkswagen Jetta', '', ''),
(371, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 160 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'volkswagen-passat', '', '', '', 'Volkswagen Passat', '', ''),
(372, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 165 km/h i przyspieszczenie 13 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'volvo-duett', '', '', '', 'Volvo Duett', '', ''),
(373, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 159 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'volvo-amazon', '', '', '', 'Volvo Amazon', '', ''),
(374, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 165 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'volvo-p1800', '', '', '', 'Volvo P1800', '', ''),
(375, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 161 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'volvo-140', '', '', '', 'Volvo 140', '', ''),
(376, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 129 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'volvo-164', '', '', '', 'Volvo 164', '', ''),
(377, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 140 km/h i przyspieszczenie 11 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'volvo-200-series', '', '', '', 'Volvo 200 series', '', ''),
(378, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 144 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'volvo-300-series', '', '', '', 'Volvo 300 series', '', ''),
(379, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 125 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'volvo-700-series', '', '', '', 'Volvo 700 series', '', ''),
(380, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 134 km/h i przyspieszczenie 10 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'volvo-850', '', '', '', 'Volvo 850', '', ''),
(381, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 138 km/h i przyspieszczenie 12 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'willys-77', '', '', '', 'Willys 77', '', ''),
(382, 1, 1, 'Samochód posiada manualną skrzynię biegów. Pojazd osiąga maksymalna prędkość 166 km/h i przyspieszczenie 14 sekund do 100km. Samochód posiada materiałowe obicie foteli. Samochód wymaga przeprowadzenia serwisu.', '', 'zastava-skala', '', '', '', 'Zastava Skala', '', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_product_sale`
--

DROP TABLE IF EXISTS `ps_product_sale`;
CREATE TABLE `ps_product_sale` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `sale_nbr` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `date_upd` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_product_sale`
--

INSERT INTO `ps_product_sale` (`id_product`, `quantity`, `sale_nbr`, `date_upd`) VALUES
(2, 1, 1, '2018-12-04');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_product_shop`
--

DROP TABLE IF EXISTS `ps_product_shop`;
CREATE TABLE `ps_product_shop` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL,
  `id_category_default` int(10) UNSIGNED DEFAULT NULL,
  `id_tax_rules_group` int(11) UNSIGNED NOT NULL,
  `on_sale` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `online_only` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `ecotax` decimal(17,6) NOT NULL DEFAULT 0.000000,
  `minimal_quantity` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `price` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `wholesale_price` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `unity` varchar(255) DEFAULT NULL,
  `unit_price_ratio` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `additional_shipping_cost` decimal(20,2) NOT NULL DEFAULT 0.00,
  `customizable` tinyint(2) NOT NULL DEFAULT 0,
  `uploadable_files` tinyint(4) NOT NULL DEFAULT 0,
  `text_fields` tinyint(4) NOT NULL DEFAULT 0,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `redirect_type` enum('','404','301','302') NOT NULL DEFAULT '',
  `id_product_redirected` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `available_for_order` tinyint(1) NOT NULL DEFAULT 1,
  `available_date` date NOT NULL DEFAULT '0000-00-00',
  `condition` enum('new','used','refurbished') NOT NULL DEFAULT 'new',
  `show_price` tinyint(1) NOT NULL DEFAULT 1,
  `indexed` tinyint(1) NOT NULL DEFAULT 0,
  `visibility` enum('both','catalog','search','none') NOT NULL DEFAULT 'both',
  `cache_default_attribute` int(10) UNSIGNED DEFAULT NULL,
  `advanced_stock_management` tinyint(1) NOT NULL DEFAULT 0,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `pack_stock_type` int(11) UNSIGNED NOT NULL DEFAULT 3
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_product_shop`
--

INSERT INTO `ps_product_shop` (`id_product`, `id_shop`, `id_category_default`, `id_tax_rules_group`, `on_sale`, `online_only`, `ecotax`, `minimal_quantity`, `price`, `wholesale_price`, `unity`, `unit_price_ratio`, `additional_shipping_cost`, `customizable`, `uploadable_files`, `text_fields`, `active`, `redirect_type`, `id_product_redirected`, `available_for_order`, `available_date`, `condition`, `show_price`, `indexed`, `visibility`, `cache_default_attribute`, `advanced_stock_management`, `date_add`, `date_upd`, `pack_stock_type`) VALUES
(1, 1, 12, 1, 0, 0, '0.000000', 1, '10031.707317', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(2, 1, 12, 1, 0, 0, '0.000000', 1, '9039.024390', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(3, 1, 12, 1, 0, 0, '0.000000', 1, '13611.382114', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(4, 1, 25, 1, 0, 0, '0.000000', 1, '12867.479675', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(5, 1, 25, 1, 0, 0, '0.000000', 1, '14942.276423', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(6, 1, 12, 1, 0, 0, '0.000000', 1, '1894.308943', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(7, 1, 30, 1, 0, 0, '0.000000', 1, '1691.056911', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(8, 1, 12, 1, 0, 0, '0.000000', 1, '14830.894309', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(9, 1, 25, 1, 0, 0, '0.000000', 1, '9621.951220', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(10, 1, 12, 1, 0, 0, '0.000000', 1, '10047.967480', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(11, 1, 30, 1, 0, 0, '0.000000', 1, '11404.065041', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(12, 1, 12, 1, 0, 0, '0.000000', 1, '8575.609756', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(13, 1, 25, 1, 0, 0, '0.000000', 1, '15338.211382', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(14, 1, 25, 1, 0, 0, '0.000000', 1, '14698.373984', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(15, 1, 25, 1, 0, 0, '0.000000', 1, '10340.650407', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(16, 1, 30, 1, 0, 0, '0.000000', 1, '12584.552846', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(17, 1, 12, 1, 0, 0, '0.000000', 1, '9447.967480', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(18, 1, 25, 1, 0, 0, '0.000000', 1, '13726.016260', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(19, 1, 30, 1, 0, 0, '0.000000', 1, '2321.951220', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(20, 1, 12, 1, 0, 0, '0.000000', 1, '2308.943089', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(21, 1, 30, 1, 0, 0, '0.000000', 1, '2074.796748', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(22, 1, 30, 1, 0, 0, '0.000000', 1, '11599.186992', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(23, 1, 12, 1, 0, 0, '0.000000', 1, '10486.178862', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(24, 1, 30, 1, 0, 0, '0.000000', 1, '9617.073171', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(25, 1, 12, 1, 0, 0, '0.000000', 1, '8591.869919', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(26, 1, 30, 1, 0, 0, '0.000000', 1, '13175.609756', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(27, 1, 25, 1, 0, 0, '0.000000', 1, '13086.991870', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(28, 1, 12, 1, 0, 0, '0.000000', 1, '8152.032520', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(29, 1, 30, 1, 0, 0, '0.000000', 1, '8508.943089', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(30, 1, 30, 1, 0, 0, '0.000000', 1, '8329.268293', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(31, 1, 25, 1, 0, 0, '0.000000', 1, '11714.634146', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(32, 1, 12, 1, 0, 0, '0.000000', 1, '9082.113821', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(33, 1, 25, 1, 0, 0, '0.000000', 1, '15018.699187', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(34, 1, 25, 1, 0, 0, '0.000000', 1, '15765.853659', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(35, 1, 25, 1, 0, 0, '0.000000', 1, '13185.365854', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(36, 1, 30, 1, 0, 0, '0.000000', 1, '2160.162602', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(37, 1, 12, 1, 0, 0, '0.000000', 1, '1958.536585', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(38, 1, 25, 1, 0, 0, '0.000000', 1, '15014.634146', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(39, 1, 25, 1, 0, 0, '0.000000', 1, '1460.162602', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(40, 1, 30, 1, 0, 0, '0.000000', 1, '6145.528455', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(41, 1, 25, 1, 0, 0, '0.000000', 1, '11667.479675', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(42, 1, 25, 1, 0, 0, '0.000000', 1, '14976.422764', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(43, 1, 12, 1, 0, 0, '0.000000', 1, '830.081301', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(44, 1, 12, 1, 0, 0, '0.000000', 1, '1676.422764', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(45, 1, 30, 1, 0, 0, '0.000000', 1, '8937.398374', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(46, 1, 30, 1, 0, 0, '0.000000', 1, '14998.373984', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(47, 1, 25, 1, 0, 0, '0.000000', 1, '8234.959350', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(48, 1, 12, 1, 0, 0, '0.000000', 1, '9773.170732', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(49, 1, 30, 1, 0, 0, '0.000000', 1, '14499.186992', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(50, 1, 25, 1, 0, 0, '0.000000', 1, '8656.097561', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(51, 1, 25, 1, 0, 0, '0.000000', 1, '14660.162602', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(52, 1, 30, 1, 0, 0, '0.000000', 1, '12281.300813', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(53, 1, 12, 1, 0, 0, '0.000000', 1, '14773.983740', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(54, 1, 12, 1, 0, 0, '0.000000', 1, '14230.894309', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(55, 1, 25, 1, 0, 0, '0.000000', 1, '12669.918699', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(56, 1, 12, 1, 0, 0, '0.000000', 1, '11117.073171', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(57, 1, 12, 1, 0, 0, '0.000000', 1, '2279.674797', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(58, 1, 30, 1, 0, 0, '0.000000', 1, '47495.121951', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(59, 1, 30, 1, 0, 0, '0.000000', 1, '12219.512195', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(60, 1, 25, 1, 0, 0, '0.000000', 1, '10573.170732', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(61, 1, 25, 1, 0, 0, '0.000000', 1, '15453.658537', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(62, 1, 25, 1, 0, 0, '0.000000', 1, '14412.195122', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(63, 1, 12, 1, 0, 0, '0.000000', 1, '4069.105691', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(64, 1, 12, 1, 0, 0, '0.000000', 1, '14536.585366', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(65, 1, 25, 1, 0, 0, '0.000000', 1, '9786.178862', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(66, 1, 25, 1, 0, 0, '0.000000', 1, '1890.243902', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(67, 1, 30, 1, 0, 0, '0.000000', 1, '26739.837398', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(68, 1, 25, 1, 0, 0, '0.000000', 1, '1993.495935', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(69, 1, 25, 1, 0, 0, '0.000000', 1, '11301.626016', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(70, 1, 30, 1, 0, 0, '0.000000', 1, '4778.048780', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(71, 1, 25, 1, 0, 0, '0.000000', 1, '45843.902439', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(72, 1, 12, 1, 0, 0, '0.000000', 1, '15325.203252', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(73, 1, 25, 1, 0, 0, '0.000000', 1, '11793.495935', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(74, 1, 30, 1, 0, 0, '0.000000', 1, '10205.691057', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(75, 1, 25, 1, 0, 0, '0.000000', 1, '12769.105691', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(76, 1, 25, 1, 0, 0, '0.000000', 1, '10286.991870', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(77, 1, 12, 1, 0, 0, '0.000000', 1, '16191.056911', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(78, 1, 12, 1, 0, 0, '0.000000', 1, '1725.203252', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(79, 1, 25, 1, 0, 0, '0.000000', 1, '10945.528455', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(80, 1, 30, 1, 0, 0, '0.000000', 1, '14030.081301', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(81, 1, 30, 1, 0, 0, '0.000000', 1, '10840.650407', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(82, 1, 12, 1, 0, 0, '0.000000', 1, '13602.439024', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(83, 1, 25, 1, 0, 0, '0.000000', 1, '1880.487805', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(84, 1, 12, 1, 0, 0, '0.000000', 1, '14999.186992', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(85, 1, 12, 1, 0, 0, '0.000000', 1, '1059.349593', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(86, 1, 12, 1, 0, 0, '0.000000', 1, '10383.739837', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(87, 1, 25, 1, 0, 0, '0.000000', 1, '1256.910569', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(88, 1, 12, 1, 0, 0, '0.000000', 1, '8509.756098', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(89, 1, 25, 1, 0, 0, '0.000000', 1, '2356.910569', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(90, 1, 30, 1, 0, 0, '0.000000', 1, '11585.365854', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(91, 1, 12, 1, 0, 0, '0.000000', 1, '10097.560976', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(92, 1, 30, 1, 0, 0, '0.000000', 1, '9296.747967', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(93, 1, 25, 1, 0, 0, '0.000000', 1, '14930.894309', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(94, 1, 12, 1, 0, 0, '0.000000', 1, '15265.853659', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(95, 1, 30, 1, 0, 0, '0.000000', 1, '14563.414634', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(96, 1, 30, 1, 0, 0, '0.000000', 1, '55311.382114', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(97, 1, 25, 1, 0, 0, '0.000000', 1, '79652.845528', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(98, 1, 25, 1, 0, 0, '0.000000', 1, '2010.569106', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(99, 1, 30, 1, 0, 0, '0.000000', 1, '44603.252033', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(100, 1, 30, 1, 0, 0, '0.000000', 1, '1212.195122', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(101, 1, 25, 1, 0, 0, '0.000000', 1, '11635.772358', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(102, 1, 12, 1, 0, 0, '0.000000', 1, '12107.317073', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(103, 1, 25, 1, 0, 0, '0.000000', 1, '15838.211382', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(104, 1, 30, 1, 0, 0, '0.000000', 1, '8211.382114', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(105, 1, 30, 1, 0, 0, '0.000000', 1, '14586.178862', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(106, 1, 25, 1, 0, 0, '0.000000', 1, '11246.341463', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(107, 1, 12, 1, 0, 0, '0.000000', 1, '10839.837398', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(108, 1, 25, 1, 0, 0, '0.000000', 1, '11965.853659', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(109, 1, 12, 1, 0, 0, '0.000000', 1, '13082.113821', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(110, 1, 30, 1, 0, 0, '0.000000', 1, '15723.577236', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(111, 1, 30, 1, 0, 0, '0.000000', 1, '8895.934959', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(112, 1, 25, 1, 0, 0, '0.000000', 1, '10988.617886', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(113, 1, 25, 1, 0, 0, '0.000000', 1, '12000.000000', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(114, 1, 12, 1, 0, 0, '0.000000', 1, '14679.674797', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(115, 1, 12, 1, 0, 0, '0.000000', 1, '904.065041', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(116, 1, 30, 1, 0, 0, '0.000000', 1, '1442.276423', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(117, 1, 12, 1, 0, 0, '0.000000', 1, '10013.821138', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(118, 1, 12, 1, 0, 0, '0.000000', 1, '2070.731707', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(119, 1, 25, 1, 0, 0, '0.000000', 1, '1725.203252', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(120, 1, 25, 1, 0, 0, '0.000000', 1, '5832.520325', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(121, 1, 25, 1, 0, 0, '0.000000', 1, '12900.000000', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(122, 1, 25, 1, 0, 0, '0.000000', 1, '5200.000000', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(123, 1, 25, 1, 0, 0, '0.000000', 1, '2295.934959', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(124, 1, 12, 1, 0, 0, '0.000000', 1, '12206.504065', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(125, 1, 12, 1, 0, 0, '0.000000', 1, '46217.886179', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(126, 1, 12, 1, 0, 0, '0.000000', 1, '34074.796748', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(127, 1, 30, 1, 0, 0, '0.000000', 1, '10820.325203', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(128, 1, 30, 1, 0, 0, '0.000000', 1, '15082.113821', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(129, 1, 30, 1, 0, 0, '0.000000', 1, '16230.081301', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(130, 1, 25, 1, 0, 0, '0.000000', 1, '11266.666667', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(131, 1, 25, 1, 0, 0, '0.000000', 1, '8273.983740', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(132, 1, 30, 1, 0, 0, '0.000000', 1, '2152.845528', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(133, 1, 12, 1, 0, 0, '0.000000', 1, '14614.634146', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(134, 1, 25, 1, 0, 0, '0.000000', 1, '14599.186992', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(135, 1, 30, 1, 0, 0, '0.000000', 1, '36744.715447', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(136, 1, 30, 1, 0, 0, '0.000000', 1, '988.617886', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(137, 1, 12, 1, 0, 0, '0.000000', 1, '12844.715447', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(138, 1, 12, 1, 0, 0, '0.000000', 1, '15030.081301', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(139, 1, 30, 1, 0, 0, '0.000000', 1, '31769.105691', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(140, 1, 30, 1, 0, 0, '0.000000', 1, '1281.300813', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(141, 1, 12, 1, 0, 0, '0.000000', 1, '8856.910569', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(142, 1, 25, 1, 0, 0, '0.000000', 1, '946.341463', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(143, 1, 12, 1, 0, 0, '0.000000', 1, '8615.447154', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(144, 1, 30, 1, 0, 0, '0.000000', 1, '33690.243902', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(145, 1, 30, 1, 0, 0, '0.000000', 1, '14494.308943', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(146, 1, 30, 1, 0, 0, '0.000000', 1, '12421.951220', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(147, 1, 25, 1, 0, 0, '0.000000', 1, '1756.910569', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(148, 1, 30, 1, 0, 0, '0.000000', 1, '11674.796748', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(149, 1, 30, 1, 0, 0, '0.000000', 1, '2396.747967', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(150, 1, 25, 1, 0, 0, '0.000000', 1, '11619.512195', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(151, 1, 25, 1, 0, 0, '0.000000', 1, '12434.959350', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(152, 1, 12, 1, 0, 0, '0.000000', 1, '13311.382114', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(153, 1, 25, 1, 0, 0, '0.000000', 1, '16034.959350', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(154, 1, 30, 1, 0, 0, '0.000000', 1, '9687.804878', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(155, 1, 30, 1, 0, 0, '0.000000', 1, '1126.016260', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(156, 1, 12, 1, 0, 0, '0.000000', 1, '9646.341463', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(157, 1, 30, 1, 0, 0, '0.000000', 1, '10668.292683', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(158, 1, 30, 1, 0, 0, '0.000000', 1, '4250.406504', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(159, 1, 12, 1, 0, 0, '0.000000', 1, '14363.414634', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(160, 1, 25, 1, 0, 0, '0.000000', 1, '16079.674797', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(161, 1, 30, 1, 0, 0, '0.000000', 1, '11802.439024', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(162, 1, 12, 1, 0, 0, '0.000000', 1, '14268.292683', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(163, 1, 12, 1, 0, 0, '0.000000', 1, '2243.902439', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(164, 1, 30, 1, 0, 0, '0.000000', 1, '1866.666667', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(165, 1, 30, 1, 0, 0, '0.000000', 1, '16172.357724', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(166, 1, 30, 1, 0, 0, '0.000000', 1, '8801.626016', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(167, 1, 25, 1, 0, 0, '0.000000', 1, '28109.756098', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(168, 1, 25, 1, 0, 0, '0.000000', 1, '8160.162602', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(169, 1, 25, 1, 0, 0, '0.000000', 1, '14811.382114', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(170, 1, 12, 1, 0, 0, '0.000000', 1, '2354.471545', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(171, 1, 30, 1, 0, 0, '0.000000', 1, '9451.219512', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(172, 1, 30, 1, 0, 0, '0.000000', 1, '2438.211382', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(173, 1, 30, 1, 0, 0, '0.000000', 1, '1854.471545', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(174, 1, 12, 1, 0, 0, '0.000000', 1, '11274.796748', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(175, 1, 30, 1, 0, 0, '0.000000', 1, '4669.105691', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(176, 1, 25, 1, 0, 0, '0.000000', 1, '10156.097561', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(177, 1, 12, 1, 0, 0, '0.000000', 1, '10496.747967', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(178, 1, 30, 1, 0, 0, '0.000000', 1, '8873.170732', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(179, 1, 12, 1, 0, 0, '0.000000', 1, '11774.796748', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(180, 1, 30, 1, 0, 0, '0.000000', 1, '1236.585366', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(181, 1, 30, 1, 0, 0, '0.000000', 1, '2059.349593', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(182, 1, 25, 1, 0, 0, '0.000000', 1, '14413.821138', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(183, 1, 30, 1, 0, 0, '0.000000', 1, '11745.528455', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(184, 1, 25, 1, 0, 0, '0.000000', 1, '14527.642276', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(185, 1, 30, 1, 0, 0, '0.000000', 1, '8320.325203', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(186, 1, 12, 1, 0, 0, '0.000000', 1, '15803.252033', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(187, 1, 30, 1, 0, 0, '0.000000', 1, '9253.658537', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(188, 1, 30, 1, 0, 0, '0.000000', 1, '12073.983740', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(189, 1, 30, 1, 0, 0, '0.000000', 1, '11082.113821', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(190, 1, 25, 1, 0, 0, '0.000000', 1, '14917.073171', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(191, 1, 30, 1, 0, 0, '0.000000', 1, '11771.544715', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(192, 1, 12, 1, 0, 0, '0.000000', 1, '8911.382114', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(193, 1, 25, 1, 0, 0, '0.000000', 1, '2152.845528', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(194, 1, 12, 1, 0, 0, '0.000000', 1, '1892.682927', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(195, 1, 30, 1, 0, 0, '0.000000', 1, '41883.739837', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(196, 1, 25, 1, 0, 0, '0.000000', 1, '13871.544715', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(197, 1, 12, 1, 0, 0, '0.000000', 1, '16199.186992', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(198, 1, 30, 1, 0, 0, '0.000000', 1, '11595.121951', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(199, 1, 25, 1, 0, 0, '0.000000', 1, '2000.000000', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(200, 1, 12, 1, 0, 0, '0.000000', 1, '1804.065041', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(201, 1, 12, 1, 0, 0, '0.000000', 1, '10565.853659', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(202, 1, 25, 1, 0, 0, '0.000000', 1, '9007.317073', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(203, 1, 25, 1, 0, 0, '0.000000', 1, '15576.422764', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(204, 1, 25, 1, 0, 0, '0.000000', 1, '11578.048780', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(205, 1, 12, 1, 0, 0, '0.000000', 1, '10529.268293', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(206, 1, 30, 1, 0, 0, '0.000000', 1, '10695.121951', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(207, 1, 25, 1, 0, 0, '0.000000', 1, '15386.178862', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(208, 1, 12, 1, 0, 0, '0.000000', 1, '14557.723577', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(209, 1, 12, 1, 0, 0, '0.000000', 1, '15030.894309', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(210, 1, 25, 1, 0, 0, '0.000000', 1, '2297.560976', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(211, 1, 30, 1, 0, 0, '0.000000', 1, '16073.983740', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(212, 1, 25, 1, 0, 0, '0.000000', 1, '6774.796748', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(213, 1, 25, 1, 0, 0, '0.000000', 1, '15470.731707', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(214, 1, 12, 1, 0, 0, '0.000000', 1, '2303.252033', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(215, 1, 30, 1, 0, 0, '0.000000', 1, '14091.056911', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(216, 1, 12, 1, 0, 0, '0.000000', 1, '9256.097561', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(217, 1, 12, 1, 0, 0, '0.000000', 1, '15513.821138', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(218, 1, 30, 1, 0, 0, '0.000000', 1, '8947.967480', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(219, 1, 25, 1, 0, 0, '0.000000', 1, '11023.577236', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(220, 1, 30, 1, 0, 0, '0.000000', 1, '12634.146341', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(221, 1, 30, 1, 0, 0, '0.000000', 1, '59460.975610', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(222, 1, 25, 1, 0, 0, '0.000000', 1, '1294.308943', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(223, 1, 12, 1, 0, 0, '0.000000', 1, '1643.902439', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(224, 1, 12, 1, 0, 0, '0.000000', 1, '2009.756098', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(225, 1, 25, 1, 0, 0, '0.000000', 1, '25817.073171', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(226, 1, 30, 1, 0, 0, '0.000000', 1, '63402.439024', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(227, 1, 25, 1, 0, 0, '0.000000', 1, '29800.813008', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(228, 1, 25, 1, 0, 0, '0.000000', 1, '13593.495935', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(229, 1, 25, 1, 0, 0, '0.000000', 1, '11933.333333', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(230, 1, 25, 1, 0, 0, '0.000000', 1, '1684.552846', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(231, 1, 12, 1, 0, 0, '0.000000', 1, '2033.333333', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(232, 1, 25, 1, 0, 0, '0.000000', 1, '2113.008130', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(233, 1, 25, 1, 0, 0, '0.000000', 1, '47478.048780', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(234, 1, 25, 1, 0, 0, '0.000000', 1, '14817.073171', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(235, 1, 12, 1, 0, 0, '0.000000', 1, '13428.455285', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(236, 1, 25, 1, 0, 0, '0.000000', 1, '2330.081301', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(237, 1, 12, 1, 0, 0, '0.000000', 1, '2184.552846', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(238, 1, 12, 1, 0, 0, '0.000000', 1, '12806.504065', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(239, 1, 25, 1, 0, 0, '0.000000', 1, '2290.243902', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(240, 1, 25, 1, 0, 0, '0.000000', 1, '1752.032520', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(241, 1, 12, 1, 0, 0, '0.000000', 1, '9766.666667', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(242, 1, 25, 1, 0, 0, '0.000000', 1, '1239.837398', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(243, 1, 12, 1, 0, 0, '0.000000', 1, '1424.390244', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(244, 1, 25, 1, 0, 0, '0.000000', 1, '9308.130081', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(245, 1, 25, 1, 0, 0, '0.000000', 1, '5504.065041', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(246, 1, 30, 1, 0, 0, '0.000000', 1, '28300.813008', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(247, 1, 25, 1, 0, 0, '0.000000', 1, '13780.487805', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(248, 1, 30, 1, 0, 0, '0.000000', 1, '2400.813008', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(249, 1, 30, 1, 0, 0, '0.000000', 1, '1978.861789', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(250, 1, 25, 1, 0, 0, '0.000000', 1, '10199.186992', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(251, 1, 25, 1, 0, 0, '0.000000', 1, '13282.926829', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(252, 1, 30, 1, 0, 0, '0.000000', 1, '13811.382114', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(253, 1, 30, 1, 0, 0, '0.000000', 1, '8215.447154', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(254, 1, 30, 1, 0, 0, '0.000000', 1, '15608.943089', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(255, 1, 12, 1, 0, 0, '0.000000', 1, '45178.048780', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(256, 1, 25, 1, 0, 0, '0.000000', 1, '8246.341463', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(257, 1, 25, 1, 0, 0, '0.000000', 1, '15900.000000', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(258, 1, 25, 1, 0, 0, '0.000000', 1, '11715.447154', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(259, 1, 25, 1, 0, 0, '0.000000', 1, '9008.130081', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(260, 1, 12, 1, 0, 0, '0.000000', 1, '13760.162602', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(261, 1, 30, 1, 0, 0, '0.000000', 1, '10348.780488', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(262, 1, 12, 1, 0, 0, '0.000000', 1, '12718.699187', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(263, 1, 25, 1, 0, 0, '0.000000', 1, '10932.520325', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(264, 1, 30, 1, 0, 0, '0.000000', 1, '10681.300813', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(265, 1, 25, 1, 0, 0, '0.000000', 1, '8973.983740', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(266, 1, 30, 1, 0, 0, '0.000000', 1, '8356.910569', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(267, 1, 30, 1, 0, 0, '0.000000', 1, '2019.512195', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(268, 1, 30, 1, 0, 0, '0.000000', 1, '12878.048780', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(269, 1, 12, 1, 0, 0, '0.000000', 1, '14322.764228', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(270, 1, 25, 1, 0, 0, '0.000000', 1, '8209.756098', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(271, 1, 12, 1, 0, 0, '0.000000', 1, '8570.731707', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(272, 1, 12, 1, 0, 0, '0.000000', 1, '12984.552846', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(273, 1, 30, 1, 0, 0, '0.000000', 1, '11764.227642', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(274, 1, 25, 1, 0, 0, '0.000000', 1, '12057.723577', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(275, 1, 12, 1, 0, 0, '0.000000', 1, '850.406504', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(276, 1, 30, 1, 0, 0, '0.000000', 1, '1234.146341', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(277, 1, 30, 1, 0, 0, '0.000000', 1, '2309.756098', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(278, 1, 30, 1, 0, 0, '0.000000', 1, '1724.390244', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(279, 1, 12, 1, 0, 0, '0.000000', 1, '8409.756098', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(280, 1, 25, 1, 0, 0, '0.000000', 1, '9317.886179', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(281, 1, 12, 1, 0, 0, '0.000000', 1, '8541.463415', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(282, 1, 12, 1, 0, 0, '0.000000', 1, '14178.048780', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(283, 1, 30, 1, 0, 0, '0.000000', 1, '14506.504065', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(284, 1, 30, 1, 0, 0, '0.000000', 1, '8528.455285', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(285, 1, 12, 1, 0, 0, '0.000000', 1, '2178.861789', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(286, 1, 12, 1, 0, 0, '0.000000', 1, '13969.105691', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(287, 1, 30, 1, 0, 0, '0.000000', 1, '15169.105691', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(288, 1, 30, 1, 0, 0, '0.000000', 1, '57658.536585', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(289, 1, 12, 1, 0, 0, '0.000000', 1, '1791.056911', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(290, 1, 25, 1, 0, 0, '0.000000', 1, '71026.016260', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(291, 1, 25, 1, 0, 0, '0.000000', 1, '8742.276423', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(292, 1, 25, 1, 0, 0, '0.000000', 1, '8484.552846', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(293, 1, 12, 1, 0, 0, '0.000000', 1, '10710.569106', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(294, 1, 12, 1, 0, 0, '0.000000', 1, '9954.471545', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(295, 1, 12, 1, 0, 0, '0.000000', 1, '1932.520325', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(296, 1, 12, 1, 0, 0, '0.000000', 1, '13393.495935', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(297, 1, 30, 1, 0, 0, '0.000000', 1, '10261.788618', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(298, 1, 25, 1, 0, 0, '0.000000', 1, '12221.138211', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(299, 1, 25, 1, 0, 0, '0.000000', 1, '8850.406504', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(300, 1, 30, 1, 0, 0, '0.000000', 1, '2039.837398', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(301, 1, 12, 1, 0, 0, '0.000000', 1, '4544.715447', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(302, 1, 25, 1, 0, 0, '0.000000', 1, '5911.382114', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(303, 1, 25, 1, 0, 0, '0.000000', 1, '68523.577236', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(304, 1, 12, 1, 0, 0, '0.000000', 1, '15491.869919', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(305, 1, 12, 1, 0, 0, '0.000000', 1, '14982.113821', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(306, 1, 12, 1, 0, 0, '0.000000', 1, '8134.959350', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(307, 1, 12, 1, 0, 0, '0.000000', 1, '13642.276423', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(308, 1, 30, 1, 0, 0, '0.000000', 1, '12564.227642', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(309, 1, 12, 1, 0, 0, '0.000000', 1, '14200.813008', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(310, 1, 25, 1, 0, 0, '0.000000', 1, '14752.845528', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(311, 1, 12, 1, 0, 0, '0.000000', 1, '13452.032520', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(312, 1, 25, 1, 0, 0, '0.000000', 1, '1332.520325', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(313, 1, 30, 1, 0, 0, '0.000000', 1, '11787.804878', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(314, 1, 30, 1, 0, 0, '0.000000', 1, '2065.853659', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(315, 1, 30, 1, 0, 0, '0.000000', 1, '2349.593496', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(316, 1, 25, 1, 0, 0, '0.000000', 1, '2236.585366', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(317, 1, 25, 1, 0, 0, '0.000000', 1, '1908.130081', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(318, 1, 12, 1, 0, 0, '0.000000', 1, '2431.707317', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(319, 1, 25, 1, 0, 0, '0.000000', 1, '15479.674797', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(320, 1, 25, 1, 0, 0, '0.000000', 1, '14638.211382', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(321, 1, 25, 1, 0, 0, '0.000000', 1, '8832.520325', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(322, 1, 30, 1, 0, 0, '0.000000', 1, '16139.837398', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(323, 1, 12, 1, 0, 0, '0.000000', 1, '10495.934959', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(324, 1, 30, 1, 0, 0, '0.000000', 1, '1662.601626', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(325, 1, 30, 1, 0, 0, '0.000000', 1, '1797.560976', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(326, 1, 25, 1, 0, 0, '0.000000', 1, '1637.398374', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(327, 1, 12, 1, 0, 0, '0.000000', 1, '2284.552846', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(328, 1, 30, 1, 0, 0, '0.000000', 1, '12175.609756', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(329, 1, 30, 1, 0, 0, '0.000000', 1, '9538.211382', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(330, 1, 12, 1, 0, 0, '0.000000', 1, '6226.016260', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(331, 1, 25, 1, 0, 0, '0.000000', 1, '1607.317073', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(332, 1, 12, 1, 0, 0, '0.000000', 1, '10691.869919', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(333, 1, 25, 1, 0, 0, '0.000000', 1, '1888.617886', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(334, 1, 30, 1, 0, 0, '0.000000', 1, '5621.951220', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(335, 1, 25, 1, 0, 0, '0.000000', 1, '8982.113821', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(336, 1, 25, 1, 0, 0, '0.000000', 1, '13943.089431', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(337, 1, 30, 1, 0, 0, '0.000000', 1, '15525.203252', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(338, 1, 25, 1, 0, 0, '0.000000', 1, '70302.439024', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(339, 1, 25, 1, 0, 0, '0.000000', 1, '2366.666667', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(340, 1, 25, 1, 0, 0, '0.000000', 1, '15494.308943', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(341, 1, 30, 1, 0, 0, '0.000000', 1, '14474.796748', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(342, 1, 25, 1, 0, 0, '0.000000', 1, '14932.520325', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(343, 1, 30, 1, 0, 0, '0.000000', 1, '11734.959350', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(344, 1, 30, 1, 0, 0, '0.000000', 1, '1208.130081', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(345, 1, 12, 1, 0, 0, '0.000000', 1, '1732.520325', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(346, 1, 12, 1, 0, 0, '0.000000', 1, '2016.260163', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(347, 1, 30, 1, 0, 0, '0.000000', 1, '1952.845528', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(348, 1, 30, 1, 0, 0, '0.000000', 1, '2098.373984', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(349, 1, 12, 1, 0, 0, '0.000000', 1, '1704.065041', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(350, 1, 12, 1, 0, 0, '0.000000', 1, '1947.154472', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(351, 1, 30, 1, 0, 0, '0.000000', 1, '14327.642276', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(352, 1, 25, 1, 0, 0, '0.000000', 1, '15243.089431', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(353, 1, 12, 1, 0, 0, '0.000000', 1, '13863.414634', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(354, 1, 25, 1, 0, 0, '0.000000', 1, '10659.349593', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(355, 1, 30, 1, 0, 0, '0.000000', 1, '1830.081301', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(356, 1, 12, 1, 0, 0, '0.000000', 1, '13435.772358', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(357, 1, 25, 1, 0, 0, '0.000000', 1, '10481.300813', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(358, 1, 12, 1, 0, 0, '0.000000', 1, '13904.878049', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(359, 1, 12, 1, 0, 0, '0.000000', 1, '2395.934959', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(360, 1, 12, 1, 0, 0, '0.000000', 1, '1867.479675', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(361, 1, 30, 1, 0, 0, '0.000000', 1, '1981.300813', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(362, 1, 25, 1, 0, 0, '0.000000', 1, '15568.292683', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(363, 1, 30, 1, 0, 0, '0.000000', 1, '6051.219512', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(364, 1, 25, 1, 0, 0, '0.000000', 1, '12103.252033', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(365, 1, 25, 1, 0, 0, '0.000000', 1, '1351.219512', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(366, 1, 25, 1, 0, 0, '0.000000', 1, '1821.138211', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(367, 1, 30, 1, 0, 0, '0.000000', 1, '9926.829268', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(368, 1, 30, 1, 0, 0, '0.000000', 1, '12026.829268', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(369, 1, 12, 1, 0, 0, '0.000000', 1, '13534.146341', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(370, 1, 30, 1, 0, 0, '0.000000', 1, '8554.471545', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(371, 1, 12, 1, 0, 0, '0.000000', 1, '14589.430894', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(372, 1, 30, 1, 0, 0, '0.000000', 1, '9023.577236', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(373, 1, 12, 1, 0, 0, '0.000000', 1, '14877.235772', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(374, 1, 30, 1, 0, 0, '0.000000', 1, '13452.032520', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(375, 1, 12, 1, 0, 0, '0.000000', 1, '10510.569106', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(376, 1, 25, 1, 0, 0, '0.000000', 1, '12079.674797', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(377, 1, 12, 1, 0, 0, '0.000000', 1, '9947.154472', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(378, 1, 25, 1, 0, 0, '0.000000', 1, '9485.365854', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(379, 1, 12, 1, 0, 0, '0.000000', 1, '15148.780488', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(380, 1, 25, 1, 0, 0, '0.000000', 1, '2065.040650', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(381, 1, 30, 1, 0, 0, '0.000000', 1, '15405.691057', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3),
(382, 1, 12, 1, 0, 0, '0.000000', 1, '15738.211382', '0.000000', '', '0.000000', '0.00', 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, '2018-12-03 23:49:06', '2018-12-03 23:49:06', 3);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_product_supplier`
--

DROP TABLE IF EXISTS `ps_product_supplier`;
CREATE TABLE `ps_product_supplier` (
  `id_product_supplier` int(11) UNSIGNED NOT NULL,
  `id_product` int(11) UNSIGNED NOT NULL,
  `id_product_attribute` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `id_supplier` int(11) UNSIGNED NOT NULL,
  `product_supplier_reference` varchar(32) DEFAULT NULL,
  `product_supplier_price_te` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `id_currency` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_product_tag`
--

DROP TABLE IF EXISTS `ps_product_tag`;
CREATE TABLE `ps_product_tag` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_tag` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_profile`
--

DROP TABLE IF EXISTS `ps_profile`;
CREATE TABLE `ps_profile` (
  `id_profile` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_profile`
--

INSERT INTO `ps_profile` (`id_profile`) VALUES
(1),
(2),
(3),
(4);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_profile_lang`
--

DROP TABLE IF EXISTS `ps_profile_lang`;
CREATE TABLE `ps_profile_lang` (
  `id_lang` int(10) UNSIGNED NOT NULL,
  `id_profile` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_profile_lang`
--

INSERT INTO `ps_profile_lang` (`id_lang`, `id_profile`, `name`) VALUES
(1, 1, 'Super Admin'),
(1, 2, 'Logistician'),
(1, 3, 'Translator'),
(1, 4, 'Salesman');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_quick_access`
--

DROP TABLE IF EXISTS `ps_quick_access`;
CREATE TABLE `ps_quick_access` (
  `id_quick_access` int(10) UNSIGNED NOT NULL,
  `new_window` tinyint(1) NOT NULL DEFAULT 0,
  `link` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_quick_access`
--

INSERT INTO `ps_quick_access` (`id_quick_access`, `new_window`, `link`) VALUES
(1, 0, 'index.php?controller=AdminCategories&addcategory'),
(2, 0, 'index.php?controller=AdminProducts&addproduct'),
(3, 0, 'index.php?controller=AdminCartRules&addcart_rule');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_quick_access_lang`
--

DROP TABLE IF EXISTS `ps_quick_access_lang`;
CREATE TABLE `ps_quick_access_lang` (
  `id_quick_access` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_quick_access_lang`
--

INSERT INTO `ps_quick_access_lang` (`id_quick_access`, `id_lang`, `name`) VALUES
(1, 1, 'Nowa kategoria'),
(2, 1, 'Nowy produkt'),
(3, 1, 'Nowy kupon');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_range_price`
--

DROP TABLE IF EXISTS `ps_range_price`;
CREATE TABLE `ps_range_price` (
  `id_range_price` int(10) UNSIGNED NOT NULL,
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `delimiter1` decimal(20,6) NOT NULL,
  `delimiter2` decimal(20,6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_range_price`
--

INSERT INTO `ps_range_price` (`id_range_price`, `id_carrier`, `delimiter1`, `delimiter2`) VALUES
(1, 2, '0.000000', '10000.000000');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_range_weight`
--

DROP TABLE IF EXISTS `ps_range_weight`;
CREATE TABLE `ps_range_weight` (
  `id_range_weight` int(10) UNSIGNED NOT NULL,
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `delimiter1` decimal(20,6) NOT NULL,
  `delimiter2` decimal(20,6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_range_weight`
--

INSERT INTO `ps_range_weight` (`id_range_weight`, `id_carrier`, `delimiter1`, `delimiter2`) VALUES
(1, 2, '0.000000', '10000.000000');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_referrer`
--

DROP TABLE IF EXISTS `ps_referrer`;
CREATE TABLE `ps_referrer` (
  `id_referrer` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `passwd` varchar(32) DEFAULT NULL,
  `http_referer_regexp` varchar(64) DEFAULT NULL,
  `http_referer_like` varchar(64) DEFAULT NULL,
  `request_uri_regexp` varchar(64) DEFAULT NULL,
  `request_uri_like` varchar(64) DEFAULT NULL,
  `http_referer_regexp_not` varchar(64) DEFAULT NULL,
  `http_referer_like_not` varchar(64) DEFAULT NULL,
  `request_uri_regexp_not` varchar(64) DEFAULT NULL,
  `request_uri_like_not` varchar(64) DEFAULT NULL,
  `base_fee` decimal(5,2) NOT NULL DEFAULT 0.00,
  `percent_fee` decimal(5,2) NOT NULL DEFAULT 0.00,
  `click_fee` decimal(5,2) NOT NULL DEFAULT 0.00,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_referrer_cache`
--

DROP TABLE IF EXISTS `ps_referrer_cache`;
CREATE TABLE `ps_referrer_cache` (
  `id_connections_source` int(11) UNSIGNED NOT NULL,
  `id_referrer` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_referrer_shop`
--

DROP TABLE IF EXISTS `ps_referrer_shop`;
CREATE TABLE `ps_referrer_shop` (
  `id_referrer` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `cache_visitors` int(11) DEFAULT NULL,
  `cache_visits` int(11) DEFAULT NULL,
  `cache_pages` int(11) DEFAULT NULL,
  `cache_registrations` int(11) DEFAULT NULL,
  `cache_orders` int(11) DEFAULT NULL,
  `cache_sales` decimal(17,2) DEFAULT NULL,
  `cache_reg_rate` decimal(5,4) DEFAULT NULL,
  `cache_order_rate` decimal(5,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_request_sql`
--

DROP TABLE IF EXISTS `ps_request_sql`;
CREATE TABLE `ps_request_sql` (
  `id_request_sql` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `sql` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_required_field`
--

DROP TABLE IF EXISTS `ps_required_field`;
CREATE TABLE `ps_required_field` (
  `id_required_field` int(11) NOT NULL,
  `object_name` varchar(32) NOT NULL,
  `field_name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_risk`
--

DROP TABLE IF EXISTS `ps_risk`;
CREATE TABLE `ps_risk` (
  `id_risk` int(11) UNSIGNED NOT NULL,
  `percent` tinyint(3) NOT NULL,
  `color` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_risk`
--

INSERT INTO `ps_risk` (`id_risk`, `percent`, `color`) VALUES
(1, 0, '#32CD32'),
(2, 35, '#FF8C00'),
(3, 75, '#DC143C'),
(4, 100, '#ec2e15');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_risk_lang`
--

DROP TABLE IF EXISTS `ps_risk_lang`;
CREATE TABLE `ps_risk_lang` (
  `id_risk` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_risk_lang`
--

INSERT INTO `ps_risk_lang` (`id_risk`, `id_lang`, `name`) VALUES
(1, 1, 'Ĺťaden'),
(2, 1, 'Niski'),
(3, 1, 'Ĺredni'),
(4, 1, 'Wysoki');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_scene`
--

DROP TABLE IF EXISTS `ps_scene`;
CREATE TABLE `ps_scene` (
  `id_scene` int(10) UNSIGNED NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_scene_category`
--

DROP TABLE IF EXISTS `ps_scene_category`;
CREATE TABLE `ps_scene_category` (
  `id_scene` int(10) UNSIGNED NOT NULL,
  `id_category` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_scene_lang`
--

DROP TABLE IF EXISTS `ps_scene_lang`;
CREATE TABLE `ps_scene_lang` (
  `id_scene` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_scene_products`
--

DROP TABLE IF EXISTS `ps_scene_products`;
CREATE TABLE `ps_scene_products` (
  `id_scene` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `x_axis` int(4) NOT NULL,
  `y_axis` int(4) NOT NULL,
  `zone_width` int(3) NOT NULL,
  `zone_height` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_scene_shop`
--

DROP TABLE IF EXISTS `ps_scene_shop`;
CREATE TABLE `ps_scene_shop` (
  `id_scene` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_search_engine`
--

DROP TABLE IF EXISTS `ps_search_engine`;
CREATE TABLE `ps_search_engine` (
  `id_search_engine` int(10) UNSIGNED NOT NULL,
  `server` varchar(64) NOT NULL,
  `getvar` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_search_engine`
--

INSERT INTO `ps_search_engine` (`id_search_engine`, `server`, `getvar`) VALUES
(1, 'google', 'q'),
(2, 'aol', 'q'),
(3, 'yandex', 'text'),
(4, 'ask.com', 'q'),
(5, 'nhl.com', 'q'),
(6, 'yahoo', 'p'),
(7, 'baidu', 'wd'),
(8, 'lycos', 'query'),
(9, 'exalead', 'q'),
(10, 'search.live', 'q'),
(11, 'voila', 'rdata'),
(12, 'altavista', 'q'),
(13, 'bing', 'q'),
(14, 'daum', 'q'),
(15, 'eniro', 'search_word'),
(16, 'naver', 'query'),
(17, 'msn', 'q'),
(18, 'netscape', 'query'),
(19, 'cnn', 'query'),
(20, 'about', 'terms'),
(21, 'mamma', 'query'),
(22, 'alltheweb', 'q'),
(23, 'virgilio', 'qs'),
(24, 'alice', 'qs'),
(25, 'najdi', 'q'),
(26, 'mama', 'query'),
(27, 'seznam', 'q'),
(28, 'onet', 'qt'),
(29, 'szukacz', 'q'),
(30, 'yam', 'k'),
(31, 'pchome', 'q'),
(32, 'kvasir', 'q'),
(33, 'sesam', 'q'),
(34, 'ozu', 'q'),
(35, 'terra', 'query'),
(36, 'mynet', 'q'),
(37, 'ekolay', 'q'),
(38, 'rambler', 'words');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_search_index`
--

DROP TABLE IF EXISTS `ps_search_index`;
CREATE TABLE `ps_search_index` (
  `id_product` int(11) UNSIGNED NOT NULL,
  `id_word` int(11) UNSIGNED NOT NULL,
  `weight` smallint(4) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_search_index`
--

INSERT INTO `ps_search_index` (`id_product`, `id_word`, `weight`) VALUES
(1, 586, 1),
(1, 587, 1),
(1, 588, 1),
(1, 589, 1),
(1, 590, 1),
(1, 591, 1),
(1, 592, 1),
(1, 593, 1),
(1, 594, 1),
(1, 595, 1),
(1, 596, 1),
(1, 597, 1),
(1, 598, 1),
(1, 599, 1),
(1, 600, 1),
(1, 601, 1),
(1, 602, 1),
(1, 585, 2),
(1, 584, 3),
(1, 603, 3),
(1, 583, 6),
(2, 586, 1),
(2, 587, 1),
(2, 588, 1),
(2, 589, 1),
(2, 590, 1),
(2, 591, 1),
(2, 592, 1),
(2, 594, 1),
(2, 595, 1),
(2, 596, 1),
(2, 597, 1),
(2, 598, 1),
(2, 599, 1),
(2, 600, 1),
(2, 601, 1),
(2, 602, 1),
(2, 605, 1),
(2, 585, 2),
(2, 584, 3),
(2, 603, 3),
(2, 604, 6),
(3, 586, 1),
(3, 587, 1),
(3, 588, 1),
(3, 589, 1),
(3, 590, 1),
(3, 591, 1),
(3, 592, 1),
(3, 594, 1),
(3, 595, 1),
(3, 596, 1),
(3, 597, 1),
(3, 598, 1),
(3, 599, 1),
(3, 600, 1),
(3, 601, 1),
(3, 602, 1),
(3, 627, 1),
(3, 585, 2),
(3, 584, 3),
(3, 603, 3),
(3, 625, 6),
(3, 626, 6),
(4, 586, 1),
(4, 587, 1),
(4, 588, 1),
(4, 589, 1),
(4, 590, 1),
(4, 591, 1),
(4, 592, 1),
(4, 594, 1),
(4, 595, 1),
(4, 596, 1),
(4, 597, 1),
(4, 598, 1),
(4, 599, 1),
(4, 600, 1),
(4, 601, 1),
(4, 602, 1),
(4, 647, 1),
(4, 585, 2),
(4, 584, 3),
(4, 648, 3),
(4, 625, 6),
(4, 626, 6),
(5, 586, 1),
(5, 587, 1),
(5, 588, 1),
(5, 589, 1),
(5, 590, 1),
(5, 591, 1),
(5, 592, 1),
(5, 594, 1),
(5, 595, 1),
(5, 596, 1),
(5, 597, 1),
(5, 598, 1),
(5, 599, 1),
(5, 600, 1),
(5, 601, 1),
(5, 602, 1),
(5, 670, 1),
(5, 585, 2),
(5, 584, 3),
(5, 648, 3),
(5, 625, 6),
(5, 626, 6),
(5, 669, 6),
(6, 586, 1),
(6, 587, 1),
(6, 588, 1),
(6, 589, 1),
(6, 590, 1),
(6, 591, 1),
(6, 592, 1),
(6, 594, 1),
(6, 595, 1),
(6, 596, 1),
(6, 597, 1),
(6, 598, 1),
(6, 599, 1),
(6, 600, 1),
(6, 601, 1),
(6, 602, 1),
(6, 693, 1),
(6, 585, 2),
(6, 584, 3),
(6, 603, 3),
(6, 625, 6),
(6, 626, 6),
(6, 692, 6),
(7, 586, 1),
(7, 587, 1),
(7, 588, 1),
(7, 589, 1),
(7, 590, 1),
(7, 591, 1),
(7, 592, 1),
(7, 594, 1),
(7, 595, 1),
(7, 596, 1),
(7, 597, 1),
(7, 598, 1),
(7, 599, 1),
(7, 600, 1),
(7, 601, 1),
(7, 602, 1),
(7, 716, 1),
(7, 585, 2),
(7, 584, 3),
(7, 717, 3),
(7, 625, 6),
(7, 626, 6),
(7, 715, 6),
(8, 586, 1),
(8, 587, 1),
(8, 588, 1),
(8, 589, 1),
(8, 590, 1),
(8, 591, 1),
(8, 592, 1),
(8, 594, 1),
(8, 595, 1),
(8, 596, 1),
(8, 597, 1),
(8, 598, 1),
(8, 599, 1),
(8, 600, 1),
(8, 601, 1),
(8, 602, 1),
(8, 739, 1),
(8, 585, 2),
(8, 584, 3),
(8, 603, 3),
(8, 625, 6),
(8, 626, 6),
(8, 738, 6),
(9, 586, 1),
(9, 587, 1),
(9, 588, 1),
(9, 589, 1),
(9, 590, 1),
(9, 591, 1),
(9, 592, 1),
(9, 594, 1),
(9, 595, 1),
(9, 596, 1),
(9, 597, 1),
(9, 598, 1),
(9, 599, 1),
(9, 600, 1),
(9, 601, 1),
(9, 602, 1),
(9, 762, 1),
(9, 585, 2),
(9, 584, 3),
(9, 648, 3),
(9, 625, 6),
(9, 626, 6),
(9, 761, 6),
(10, 586, 1),
(10, 587, 1),
(10, 588, 1),
(10, 589, 1),
(10, 590, 1),
(10, 591, 1),
(10, 592, 1),
(10, 594, 1),
(10, 595, 1),
(10, 596, 1),
(10, 597, 1),
(10, 598, 1),
(10, 599, 1),
(10, 600, 1),
(10, 601, 1),
(10, 602, 1),
(10, 785, 1),
(10, 585, 2),
(10, 584, 3),
(10, 603, 3),
(10, 625, 6),
(10, 626, 6),
(10, 784, 6),
(11, 586, 1),
(11, 587, 1),
(11, 588, 1),
(11, 589, 1),
(11, 590, 1),
(11, 591, 1),
(11, 592, 1),
(11, 594, 1),
(11, 595, 1),
(11, 596, 1),
(11, 597, 1),
(11, 598, 1),
(11, 599, 1),
(11, 600, 1),
(11, 601, 1),
(11, 602, 1),
(11, 807, 1),
(11, 585, 2),
(11, 584, 3),
(11, 717, 3),
(11, 625, 6),
(11, 626, 6),
(12, 587, 1),
(12, 588, 1),
(12, 589, 1),
(12, 590, 1),
(12, 591, 1),
(12, 592, 1),
(12, 593, 1),
(12, 594, 1),
(12, 595, 1),
(12, 596, 1),
(12, 597, 1),
(12, 598, 1),
(12, 599, 1),
(12, 600, 1),
(12, 601, 1),
(12, 602, 1),
(12, 830, 1),
(12, 585, 2),
(12, 584, 3),
(12, 603, 3),
(12, 625, 6),
(12, 626, 6),
(12, 829, 6),
(13, 586, 1),
(13, 587, 1),
(13, 588, 1),
(13, 589, 1),
(13, 590, 1),
(13, 591, 1),
(13, 592, 1),
(13, 594, 1),
(13, 595, 1),
(13, 596, 1),
(13, 597, 1),
(13, 598, 1),
(13, 599, 1),
(13, 600, 1),
(13, 601, 1),
(13, 602, 1),
(13, 716, 1),
(13, 585, 2),
(13, 584, 3),
(13, 648, 3),
(13, 625, 6),
(13, 626, 6),
(13, 852, 6),
(14, 586, 1),
(14, 587, 1),
(14, 588, 1),
(14, 589, 1),
(14, 590, 1),
(14, 591, 1),
(14, 592, 1),
(14, 594, 1),
(14, 595, 1),
(14, 596, 1),
(14, 597, 1),
(14, 598, 1),
(14, 599, 1),
(14, 600, 1),
(14, 601, 1),
(14, 602, 1),
(14, 877, 1),
(14, 585, 2),
(14, 584, 3),
(14, 648, 3),
(14, 875, 6),
(14, 876, 6),
(15, 586, 1),
(15, 587, 1),
(15, 588, 1),
(15, 589, 1),
(15, 590, 1),
(15, 591, 1),
(15, 592, 1),
(15, 594, 1),
(15, 595, 1),
(15, 596, 1),
(15, 597, 1),
(15, 598, 1),
(15, 599, 1),
(15, 600, 1),
(15, 601, 1),
(15, 602, 1),
(15, 715, 1),
(15, 585, 2),
(15, 584, 3),
(15, 648, 3),
(15, 875, 6),
(15, 897, 6),
(16, 586, 1),
(16, 587, 1),
(16, 588, 1),
(16, 589, 1),
(16, 590, 1),
(16, 591, 1),
(16, 592, 1),
(16, 594, 1),
(16, 595, 1),
(16, 596, 1),
(16, 597, 1),
(16, 598, 1),
(16, 599, 1),
(16, 600, 1),
(16, 601, 1),
(16, 602, 1),
(16, 920, 1),
(16, 585, 2),
(16, 584, 3),
(16, 717, 3),
(16, 875, 6),
(16, 919, 6),
(17, 586, 1),
(17, 587, 1),
(17, 588, 1),
(17, 589, 1),
(17, 590, 1),
(17, 591, 1),
(17, 592, 1),
(17, 594, 1),
(17, 595, 1),
(17, 596, 1),
(17, 598, 1),
(17, 599, 1),
(17, 600, 1),
(17, 601, 1),
(17, 602, 1),
(17, 943, 1),
(17, 944, 1),
(17, 585, 2),
(17, 584, 3),
(17, 603, 3),
(17, 941, 6),
(17, 942, 6),
(18, 586, 1),
(18, 587, 1),
(18, 588, 1),
(18, 589, 1),
(18, 590, 1),
(18, 591, 1),
(18, 592, 1),
(18, 594, 1),
(18, 595, 1),
(18, 596, 1),
(18, 597, 1),
(18, 598, 1),
(18, 599, 1),
(18, 600, 1),
(18, 601, 1),
(18, 602, 1),
(18, 964, 1),
(18, 585, 2),
(18, 584, 3),
(18, 648, 3),
(18, 941, 6),
(18, 963, 6),
(19, 586, 1),
(19, 587, 1),
(19, 588, 1),
(19, 589, 1),
(19, 590, 1),
(19, 591, 1),
(19, 592, 1),
(19, 594, 1),
(19, 595, 1),
(19, 596, 1),
(19, 597, 1),
(19, 598, 1),
(19, 599, 1),
(19, 600, 1),
(19, 601, 1),
(19, 602, 1),
(19, 986, 1),
(19, 585, 2),
(19, 584, 3),
(19, 717, 3),
(19, 985, 6),
(20, 586, 1),
(20, 587, 1),
(20, 588, 1),
(20, 589, 1),
(20, 590, 1),
(20, 591, 1),
(20, 592, 1),
(20, 594, 1),
(20, 595, 1),
(20, 596, 1),
(20, 597, 1),
(20, 598, 1),
(20, 599, 1),
(20, 600, 1),
(20, 601, 1),
(20, 602, 1),
(20, 1006, 1),
(20, 585, 2),
(20, 584, 3),
(20, 603, 3),
(20, 985, 6),
(21, 586, 1),
(21, 587, 1),
(21, 588, 1),
(21, 589, 1),
(21, 590, 1),
(21, 591, 1),
(21, 592, 1),
(21, 593, 1),
(21, 594, 1),
(21, 595, 1),
(21, 596, 1),
(21, 597, 1),
(21, 598, 1),
(21, 599, 1),
(21, 600, 1),
(21, 601, 1),
(21, 602, 1),
(21, 585, 2),
(21, 584, 3),
(21, 717, 3),
(21, 985, 6),
(22, 586, 1),
(22, 587, 1),
(22, 588, 1),
(22, 589, 1),
(22, 590, 1),
(22, 591, 1),
(22, 592, 1),
(22, 594, 1),
(22, 595, 1),
(22, 596, 1),
(22, 597, 1),
(22, 598, 1),
(22, 599, 1),
(22, 600, 1),
(22, 601, 1),
(22, 602, 1),
(22, 716, 1),
(22, 585, 2),
(22, 584, 3),
(22, 717, 3),
(22, 1048, 6),
(22, 1049, 6),
(23, 586, 1),
(23, 587, 1),
(23, 588, 1),
(23, 589, 1),
(23, 590, 1),
(23, 591, 1),
(23, 592, 1),
(23, 594, 1),
(23, 595, 1),
(23, 596, 1),
(23, 597, 1),
(23, 598, 1),
(23, 599, 1),
(23, 600, 1),
(23, 601, 1),
(23, 602, 1),
(23, 1071, 1),
(23, 585, 2),
(23, 584, 3),
(23, 603, 3),
(23, 1048, 6),
(23, 1070, 6),
(24, 586, 1),
(24, 587, 1),
(24, 588, 1),
(24, 589, 1),
(24, 590, 1),
(24, 591, 1),
(24, 592, 1),
(24, 594, 1),
(24, 595, 1),
(24, 596, 1),
(24, 597, 1),
(24, 598, 1),
(24, 599, 1),
(24, 600, 1),
(24, 601, 1),
(24, 602, 1),
(24, 1093, 1),
(24, 585, 2),
(24, 584, 3),
(24, 717, 3),
(24, 1048, 6),
(24, 1092, 6),
(25, 586, 1),
(25, 587, 1),
(25, 588, 1),
(25, 589, 1),
(25, 590, 1),
(25, 591, 1),
(25, 592, 1),
(25, 594, 1),
(25, 595, 1),
(25, 596, 1),
(25, 597, 1),
(25, 598, 1),
(25, 599, 1),
(25, 600, 1),
(25, 601, 1),
(25, 602, 1),
(25, 1093, 1),
(25, 585, 2),
(25, 584, 3),
(25, 603, 3),
(25, 1048, 6),
(25, 1114, 6),
(26, 586, 1),
(26, 587, 1),
(26, 588, 1),
(26, 589, 1),
(26, 590, 1),
(26, 591, 1),
(26, 592, 1),
(26, 594, 1),
(26, 595, 1),
(26, 596, 1),
(26, 597, 1),
(26, 598, 1),
(26, 599, 1),
(26, 600, 1),
(26, 601, 1),
(26, 602, 1),
(26, 739, 1),
(26, 585, 2),
(26, 584, 3),
(26, 717, 3),
(26, 1136, 6),
(26, 1137, 6),
(27, 586, 1),
(27, 587, 1),
(27, 588, 1),
(27, 589, 1),
(27, 590, 1),
(27, 591, 1),
(27, 592, 1),
(27, 594, 1),
(27, 595, 1),
(27, 596, 1),
(27, 597, 1),
(27, 598, 1),
(27, 599, 1),
(27, 600, 1),
(27, 601, 1),
(27, 602, 1),
(27, 762, 1),
(27, 585, 2),
(27, 584, 3),
(27, 648, 3),
(27, 1136, 6),
(27, 1158, 6),
(28, 586, 1),
(28, 587, 1),
(28, 588, 1),
(28, 589, 1),
(28, 590, 1),
(28, 591, 1),
(28, 592, 1),
(28, 594, 1),
(28, 595, 1),
(28, 596, 1),
(28, 597, 1),
(28, 598, 1),
(28, 599, 1),
(28, 600, 1),
(28, 601, 1),
(28, 602, 1),
(28, 1182, 1),
(28, 585, 2),
(28, 584, 3),
(28, 603, 3),
(28, 1180, 6),
(28, 1181, 6),
(29, 586, 1),
(29, 587, 1),
(29, 588, 1),
(29, 589, 1),
(29, 590, 1),
(29, 591, 1),
(29, 592, 1),
(29, 594, 1),
(29, 595, 1),
(29, 596, 1),
(29, 597, 1),
(29, 598, 1),
(29, 599, 1),
(29, 600, 1),
(29, 601, 1),
(29, 602, 1),
(29, 1204, 1),
(29, 585, 2),
(29, 584, 3),
(29, 717, 3),
(29, 1202, 6),
(29, 1203, 6),
(30, 586, 1),
(30, 587, 1),
(30, 588, 1),
(30, 589, 1),
(30, 590, 1),
(30, 591, 1),
(30, 592, 1),
(30, 594, 1),
(30, 595, 1),
(30, 596, 1),
(30, 597, 1),
(30, 598, 1),
(30, 599, 1),
(30, 600, 1),
(30, 601, 1),
(30, 602, 1),
(30, 1225, 1),
(30, 585, 2),
(30, 584, 3),
(30, 717, 3),
(30, 1202, 6),
(30, 1224, 6),
(31, 586, 1),
(31, 587, 1),
(31, 588, 1),
(31, 589, 1),
(31, 590, 1),
(31, 591, 1),
(31, 592, 1),
(31, 594, 1),
(31, 595, 1),
(31, 596, 1),
(31, 597, 1),
(31, 598, 1),
(31, 599, 1),
(31, 600, 1),
(31, 601, 1),
(31, 602, 1),
(31, 647, 1),
(31, 585, 2),
(31, 584, 3),
(31, 648, 3),
(31, 1246, 6),
(31, 1247, 6),
(32, 587, 1),
(32, 588, 1),
(32, 589, 1),
(32, 590, 1),
(32, 591, 1),
(32, 592, 1),
(32, 594, 1),
(32, 595, 1),
(32, 596, 1),
(32, 598, 1),
(32, 599, 1),
(32, 600, 1),
(32, 601, 1),
(32, 602, 1),
(32, 830, 1),
(32, 944, 1),
(32, 1269, 1),
(32, 585, 2),
(32, 584, 3),
(32, 603, 3),
(32, 1246, 6),
(32, 1268, 6),
(33, 586, 1),
(33, 587, 1),
(33, 588, 1),
(33, 589, 1),
(33, 590, 1),
(33, 591, 1),
(33, 592, 1),
(33, 594, 1),
(33, 595, 1),
(33, 596, 1),
(33, 597, 1),
(33, 598, 1),
(33, 599, 1),
(33, 600, 1),
(33, 601, 1),
(33, 602, 1),
(33, 1291, 1),
(33, 585, 2),
(33, 584, 3),
(33, 648, 3),
(33, 1246, 6),
(33, 1290, 6),
(34, 586, 1),
(34, 587, 1),
(34, 588, 1),
(34, 589, 1),
(34, 590, 1),
(34, 591, 1),
(34, 592, 1),
(34, 594, 1),
(34, 595, 1),
(34, 596, 1),
(34, 597, 1),
(34, 598, 1),
(34, 599, 1),
(34, 600, 1),
(34, 601, 1),
(34, 602, 1),
(34, 1313, 1),
(34, 585, 2),
(34, 584, 3),
(34, 648, 3),
(34, 1246, 6),
(34, 1312, 6),
(35, 586, 1),
(35, 587, 1),
(35, 588, 1),
(35, 589, 1),
(35, 590, 1),
(35, 591, 1),
(35, 592, 1),
(35, 594, 1),
(35, 595, 1),
(35, 596, 1),
(35, 597, 1),
(35, 598, 1),
(35, 599, 1),
(35, 600, 1),
(35, 601, 1),
(35, 602, 1),
(35, 1336, 1),
(35, 585, 2),
(35, 584, 3),
(35, 648, 3),
(35, 1246, 6),
(35, 1334, 6),
(35, 1335, 6),
(36, 586, 1),
(36, 587, 1),
(36, 588, 1),
(36, 589, 1),
(36, 590, 1),
(36, 591, 1),
(36, 592, 1),
(36, 594, 1),
(36, 595, 1),
(36, 596, 1),
(36, 597, 1),
(36, 598, 1),
(36, 599, 1),
(36, 600, 1),
(36, 601, 1),
(36, 602, 1),
(36, 1336, 1),
(36, 585, 2),
(36, 584, 3),
(36, 717, 3),
(36, 1246, 6),
(37, 586, 1),
(37, 587, 1),
(37, 588, 1),
(37, 589, 1),
(37, 590, 1),
(37, 591, 1),
(37, 592, 1),
(37, 594, 1),
(37, 595, 1),
(37, 596, 1),
(37, 597, 1),
(37, 598, 1),
(37, 599, 1),
(37, 600, 1),
(37, 601, 1),
(37, 602, 1),
(37, 1006, 1),
(37, 585, 2),
(37, 584, 3),
(37, 603, 3),
(37, 1246, 6),
(38, 586, 1),
(38, 587, 1),
(38, 588, 1),
(38, 589, 1),
(38, 590, 1),
(38, 591, 1),
(38, 592, 1),
(38, 594, 1),
(38, 595, 1),
(38, 596, 1),
(38, 597, 1),
(38, 598, 1),
(38, 599, 1),
(38, 600, 1),
(38, 601, 1),
(38, 602, 1),
(38, 1093, 1),
(38, 585, 2),
(38, 584, 3),
(38, 648, 3),
(38, 1246, 6),
(39, 586, 1),
(39, 587, 1),
(39, 588, 1),
(39, 589, 1),
(39, 590, 1),
(39, 591, 1),
(39, 592, 1),
(39, 594, 1),
(39, 595, 1),
(39, 596, 1),
(39, 597, 1),
(39, 598, 1),
(39, 599, 1),
(39, 600, 1),
(39, 601, 1),
(39, 602, 1),
(39, 692, 1),
(39, 585, 2),
(39, 584, 3),
(39, 648, 3),
(39, 1246, 6),
(40, 586, 1),
(40, 587, 1),
(40, 588, 1),
(40, 589, 1),
(40, 590, 1),
(40, 591, 1),
(40, 592, 1),
(40, 594, 1),
(40, 595, 1),
(40, 596, 1),
(40, 597, 1),
(40, 598, 1),
(40, 599, 1),
(40, 600, 1),
(40, 601, 1),
(40, 602, 1),
(40, 1441, 1),
(40, 585, 2),
(40, 584, 3),
(40, 717, 3),
(40, 1246, 6),
(41, 586, 1),
(41, 587, 1),
(41, 588, 1),
(41, 589, 1),
(41, 590, 1),
(41, 591, 1),
(41, 592, 1),
(41, 594, 1),
(41, 595, 1),
(41, 596, 1),
(41, 597, 1),
(41, 598, 1),
(41, 599, 1),
(41, 600, 1),
(41, 601, 1),
(41, 602, 1),
(41, 1336, 1),
(41, 585, 2),
(41, 584, 3),
(41, 648, 3),
(41, 1246, 6),
(41, 1462, 6),
(42, 586, 1),
(42, 587, 1),
(42, 588, 1),
(42, 589, 1),
(42, 590, 1),
(42, 591, 1),
(42, 592, 1),
(42, 594, 1),
(42, 595, 1),
(42, 596, 1),
(42, 597, 1),
(42, 598, 1),
(42, 599, 1),
(42, 600, 1),
(42, 601, 1),
(42, 602, 1),
(42, 964, 1),
(42, 585, 2),
(42, 584, 3),
(42, 648, 3),
(42, 1246, 6),
(42, 1462, 6),
(43, 586, 1),
(43, 587, 1),
(43, 588, 1),
(43, 589, 1),
(43, 590, 1),
(43, 591, 1),
(43, 592, 1),
(43, 594, 1),
(43, 595, 1),
(43, 596, 1),
(43, 597, 1),
(43, 598, 1),
(43, 599, 1),
(43, 600, 1),
(43, 601, 1),
(43, 602, 1),
(43, 1204, 1),
(43, 585, 2),
(43, 584, 3),
(43, 603, 3),
(43, 1246, 6),
(43, 1462, 6),
(44, 586, 1),
(44, 587, 1),
(44, 588, 1),
(44, 589, 1),
(44, 590, 1),
(44, 591, 1),
(44, 592, 1),
(44, 594, 1),
(44, 595, 1),
(44, 596, 1),
(44, 597, 1),
(44, 598, 1),
(44, 599, 1),
(44, 600, 1),
(44, 601, 1),
(44, 602, 1),
(44, 1528, 1),
(44, 585, 2),
(44, 584, 3),
(44, 603, 3),
(44, 1246, 6),
(45, 586, 1),
(45, 587, 1),
(45, 588, 1),
(45, 589, 1),
(45, 590, 1),
(45, 591, 1),
(45, 592, 1),
(45, 594, 1),
(45, 595, 1),
(45, 596, 1),
(45, 597, 1),
(45, 598, 1),
(45, 599, 1),
(45, 600, 1),
(45, 601, 1),
(45, 602, 1),
(45, 1528, 1),
(45, 585, 2),
(45, 584, 3),
(45, 717, 3),
(45, 1549, 6),
(45, 1550, 6),
(46, 586, 1),
(46, 587, 1),
(46, 588, 1),
(46, 589, 1),
(46, 590, 1),
(46, 591, 1),
(46, 592, 1),
(46, 594, 1),
(46, 595, 1),
(46, 596, 1),
(46, 597, 1),
(46, 598, 1),
(46, 599, 1),
(46, 600, 1),
(46, 601, 1),
(46, 602, 1),
(46, 1571, 1),
(46, 585, 2),
(46, 584, 3),
(46, 717, 3),
(46, 1549, 6),
(46, 1550, 6),
(47, 586, 1),
(47, 587, 1),
(47, 588, 1),
(47, 589, 1),
(47, 590, 1),
(47, 591, 1),
(47, 592, 1),
(47, 594, 1),
(47, 595, 1),
(47, 596, 1),
(47, 597, 1),
(47, 598, 1),
(47, 599, 1),
(47, 600, 1),
(47, 601, 1),
(47, 602, 1),
(47, 1593, 1),
(47, 585, 2),
(47, 584, 3),
(47, 648, 3),
(47, 1549, 6),
(47, 1550, 6),
(48, 586, 1),
(48, 587, 1),
(48, 588, 1),
(48, 589, 1),
(48, 590, 1),
(48, 591, 1),
(48, 592, 1),
(48, 594, 1),
(48, 595, 1),
(48, 596, 1),
(48, 597, 1),
(48, 598, 1),
(48, 599, 1),
(48, 600, 1),
(48, 601, 1),
(48, 602, 1),
(48, 1291, 1),
(48, 585, 2),
(48, 584, 3),
(48, 603, 3),
(48, 1549, 6),
(48, 1550, 6),
(49, 586, 1),
(49, 587, 1),
(49, 588, 1),
(49, 589, 1),
(49, 590, 1),
(49, 591, 1),
(49, 592, 1),
(49, 594, 1),
(49, 595, 1),
(49, 596, 1),
(49, 597, 1),
(49, 598, 1),
(49, 599, 1),
(49, 600, 1),
(49, 601, 1),
(49, 602, 1),
(49, 647, 1),
(49, 585, 2),
(49, 584, 3),
(49, 717, 3),
(49, 1549, 6),
(49, 1550, 6),
(50, 586, 1),
(50, 587, 1),
(50, 588, 1),
(50, 589, 1),
(50, 590, 1),
(50, 591, 1),
(50, 592, 1),
(50, 594, 1),
(50, 595, 1),
(50, 596, 1),
(50, 597, 1),
(50, 598, 1),
(50, 599, 1),
(50, 600, 1),
(50, 601, 1),
(50, 602, 1),
(50, 1006, 1),
(50, 585, 2),
(50, 584, 3),
(50, 648, 3),
(50, 1549, 6),
(50, 1550, 6),
(51, 586, 1),
(51, 587, 1),
(51, 588, 1),
(51, 589, 1),
(51, 590, 1),
(51, 591, 1),
(51, 592, 1),
(51, 594, 1),
(51, 595, 1),
(51, 596, 1),
(51, 598, 1),
(51, 599, 1),
(51, 600, 1),
(51, 601, 1),
(51, 602, 1),
(51, 944, 1),
(51, 1681, 1),
(51, 585, 2),
(51, 584, 3),
(51, 648, 3),
(51, 1549, 6),
(51, 1550, 6),
(52, 586, 1),
(52, 587, 1),
(52, 588, 1),
(52, 589, 1),
(52, 590, 1),
(52, 591, 1),
(52, 592, 1),
(52, 594, 1),
(52, 595, 1),
(52, 596, 1),
(52, 597, 1),
(52, 598, 1),
(52, 599, 1),
(52, 600, 1),
(52, 601, 1),
(52, 602, 1),
(52, 1703, 1),
(52, 585, 2),
(52, 584, 3),
(52, 717, 3),
(52, 1549, 6),
(52, 1550, 6),
(53, 586, 1),
(53, 587, 1),
(53, 588, 1),
(53, 589, 1),
(53, 590, 1),
(53, 591, 1),
(53, 592, 1),
(53, 594, 1),
(53, 595, 1),
(53, 596, 1),
(53, 597, 1),
(53, 598, 1),
(53, 599, 1),
(53, 600, 1),
(53, 601, 1),
(53, 602, 1),
(53, 693, 1),
(53, 585, 2),
(53, 584, 3),
(53, 603, 3),
(53, 1549, 6),
(53, 1550, 6),
(54, 586, 1),
(54, 587, 1),
(54, 588, 1),
(54, 589, 1),
(54, 590, 1),
(54, 591, 1),
(54, 592, 1),
(54, 594, 1),
(54, 595, 1),
(54, 596, 1),
(54, 597, 1),
(54, 598, 1),
(54, 599, 1),
(54, 600, 1),
(54, 601, 1),
(54, 602, 1),
(54, 1703, 1),
(54, 585, 2),
(54, 584, 3),
(54, 603, 3),
(54, 1549, 6),
(54, 1550, 6),
(55, 586, 1),
(55, 587, 1),
(55, 588, 1),
(55, 589, 1),
(55, 590, 1),
(55, 591, 1),
(55, 592, 1),
(55, 594, 1),
(55, 595, 1),
(55, 596, 1),
(55, 597, 1),
(55, 598, 1),
(55, 599, 1),
(55, 600, 1),
(55, 601, 1),
(55, 602, 1),
(55, 1182, 1),
(55, 585, 2),
(55, 584, 3),
(55, 648, 3),
(55, 1549, 6),
(55, 1550, 6),
(56, 586, 1),
(56, 587, 1),
(56, 588, 1),
(56, 589, 1),
(56, 590, 1),
(56, 591, 1),
(56, 592, 1),
(56, 594, 1),
(56, 595, 1),
(56, 596, 1),
(56, 597, 1),
(56, 598, 1),
(56, 599, 1),
(56, 600, 1),
(56, 601, 1),
(56, 602, 1),
(56, 986, 1),
(56, 585, 2),
(56, 584, 3),
(56, 603, 3),
(56, 1549, 6),
(56, 1550, 6),
(56, 1791, 6),
(57, 586, 1),
(57, 587, 1),
(57, 588, 1),
(57, 589, 1),
(57, 590, 1),
(57, 591, 1),
(57, 592, 1),
(57, 594, 1),
(57, 595, 1),
(57, 596, 1),
(57, 597, 1),
(57, 598, 1),
(57, 599, 1),
(57, 600, 1),
(57, 601, 1),
(57, 602, 1),
(57, 1204, 1),
(57, 585, 2),
(57, 584, 3),
(57, 603, 3),
(57, 1549, 6),
(57, 1814, 6),
(58, 586, 1),
(58, 587, 1),
(58, 588, 1),
(58, 589, 1),
(58, 590, 1),
(58, 591, 1),
(58, 592, 1),
(58, 594, 1),
(58, 595, 1),
(58, 596, 1),
(58, 597, 1),
(58, 598, 1),
(58, 599, 1),
(58, 600, 1),
(58, 601, 1),
(58, 602, 1),
(58, 1837, 1),
(58, 585, 2),
(58, 584, 3),
(58, 717, 3),
(58, 1549, 6),
(58, 1836, 6),
(59, 586, 1),
(59, 587, 1),
(59, 588, 1),
(59, 589, 1),
(59, 590, 1),
(59, 591, 1),
(59, 592, 1),
(59, 593, 1),
(59, 594, 1),
(59, 595, 1),
(59, 596, 1),
(59, 597, 1),
(59, 598, 1),
(59, 599, 1),
(59, 600, 1),
(59, 601, 1),
(59, 602, 1),
(59, 585, 2),
(59, 584, 3),
(59, 717, 3),
(59, 1858, 6),
(59, 1859, 6),
(60, 586, 1),
(60, 587, 1),
(60, 588, 1),
(60, 589, 1),
(60, 590, 1),
(60, 591, 1),
(60, 592, 1),
(60, 594, 1),
(60, 595, 1),
(60, 596, 1),
(60, 597, 1),
(60, 598, 1),
(60, 599, 1),
(60, 600, 1),
(60, 601, 1),
(60, 602, 1),
(60, 1881, 1),
(60, 585, 2),
(60, 584, 3),
(60, 648, 3),
(60, 1858, 6),
(60, 1880, 6),
(61, 586, 1),
(61, 587, 1),
(61, 588, 1),
(61, 589, 1),
(61, 590, 1),
(61, 591, 1),
(61, 592, 1),
(61, 594, 1),
(61, 595, 1),
(61, 596, 1),
(61, 597, 1),
(61, 598, 1),
(61, 599, 1),
(61, 600, 1),
(61, 601, 1),
(61, 602, 1),
(61, 1225, 1),
(61, 585, 2),
(61, 584, 3),
(61, 648, 3),
(61, 1858, 6),
(61, 1902, 6),
(62, 586, 1),
(62, 587, 1),
(62, 588, 1),
(62, 589, 1),
(62, 590, 1),
(62, 591, 1),
(62, 592, 1),
(62, 594, 1),
(62, 595, 1),
(62, 596, 1),
(62, 597, 1),
(62, 598, 1),
(62, 599, 1),
(62, 600, 1),
(62, 601, 1),
(62, 602, 1),
(62, 1703, 1),
(62, 585, 2),
(62, 584, 3),
(62, 648, 3),
(62, 1858, 6),
(62, 1924, 6),
(63, 587, 1),
(63, 588, 1),
(63, 589, 1),
(63, 590, 1),
(63, 591, 1),
(63, 592, 1),
(63, 594, 1),
(63, 595, 1),
(63, 596, 1),
(63, 597, 1),
(63, 598, 1),
(63, 599, 1),
(63, 600, 1),
(63, 601, 1),
(63, 602, 1),
(63, 830, 1),
(63, 1204, 1),
(63, 585, 2),
(63, 584, 3),
(63, 603, 3),
(63, 1858, 6),
(63, 1946, 6),
(64, 586, 1),
(64, 587, 1),
(64, 588, 1),
(64, 589, 1),
(64, 590, 1),
(64, 591, 1),
(64, 592, 1),
(64, 594, 1),
(64, 595, 1),
(64, 596, 1),
(64, 597, 1),
(64, 598, 1),
(64, 599, 1),
(64, 600, 1),
(64, 601, 1),
(64, 602, 1),
(64, 1969, 1),
(64, 585, 2),
(64, 584, 3),
(64, 603, 3),
(64, 1858, 6),
(64, 1968, 6),
(65, 586, 1),
(65, 587, 1),
(65, 588, 1),
(65, 589, 1),
(65, 590, 1),
(65, 591, 1),
(65, 592, 1),
(65, 594, 1),
(65, 595, 1),
(65, 596, 1),
(65, 597, 1),
(65, 598, 1),
(65, 599, 1),
(65, 600, 1),
(65, 601, 1),
(65, 602, 1),
(65, 1313, 1),
(65, 585, 2),
(65, 584, 3),
(65, 648, 3),
(65, 1858, 6),
(65, 1990, 6),
(66, 586, 1),
(66, 587, 1),
(66, 588, 1),
(66, 589, 1),
(66, 590, 1),
(66, 591, 1),
(66, 592, 1),
(66, 594, 1),
(66, 595, 1),
(66, 596, 1),
(66, 598, 1),
(66, 599, 1),
(66, 600, 1),
(66, 601, 1),
(66, 602, 1),
(66, 944, 1),
(66, 2013, 1),
(66, 585, 2),
(66, 584, 3),
(66, 648, 3),
(66, 2012, 6),
(67, 586, 1),
(67, 587, 1),
(67, 588, 1),
(67, 589, 1),
(67, 590, 1),
(67, 591, 1),
(67, 592, 1),
(67, 594, 1),
(67, 595, 1),
(67, 596, 1),
(67, 597, 1),
(67, 598, 1),
(67, 599, 1),
(67, 600, 1),
(67, 601, 1),
(67, 602, 1),
(67, 2034, 1),
(67, 585, 2),
(67, 584, 3),
(67, 717, 3),
(67, 2012, 6),
(67, 2033, 6),
(68, 586, 1),
(68, 587, 1),
(68, 588, 1),
(68, 589, 1),
(68, 590, 1),
(68, 591, 1),
(68, 592, 1),
(68, 594, 1),
(68, 595, 1),
(68, 596, 1),
(68, 597, 1),
(68, 598, 1),
(68, 599, 1),
(68, 600, 1),
(68, 601, 1),
(68, 602, 1),
(68, 1881, 1),
(68, 585, 2),
(68, 584, 3),
(68, 648, 3),
(68, 2012, 6),
(68, 2055, 6),
(69, 586, 1),
(69, 587, 1),
(69, 588, 1),
(69, 589, 1),
(69, 590, 1),
(69, 591, 1),
(69, 592, 1),
(69, 594, 1),
(69, 595, 1),
(69, 596, 1),
(69, 597, 1),
(69, 598, 1),
(69, 599, 1),
(69, 600, 1),
(69, 601, 1),
(69, 602, 1),
(69, 762, 1),
(69, 585, 2),
(69, 584, 3),
(69, 648, 3),
(69, 2077, 6),
(69, 2078, 6),
(70, 586, 1),
(70, 587, 1),
(70, 588, 1),
(70, 589, 1),
(70, 590, 1),
(70, 591, 1),
(70, 592, 1),
(70, 594, 1),
(70, 595, 1),
(70, 596, 1),
(70, 597, 1),
(70, 598, 1),
(70, 599, 1),
(70, 600, 1),
(70, 601, 1),
(70, 602, 1),
(70, 2100, 1),
(70, 585, 2),
(70, 584, 3),
(70, 717, 3),
(70, 2077, 6),
(70, 2099, 6),
(71, 586, 1),
(71, 587, 1),
(71, 588, 1),
(71, 589, 1),
(71, 590, 1),
(71, 591, 1),
(71, 592, 1),
(71, 594, 1),
(71, 595, 1),
(71, 596, 1),
(71, 597, 1),
(71, 598, 1),
(71, 599, 1),
(71, 600, 1),
(71, 601, 1),
(71, 602, 1),
(71, 2122, 1),
(71, 585, 2),
(71, 584, 3),
(71, 648, 3),
(71, 2077, 6),
(71, 2121, 6),
(72, 586, 1),
(72, 587, 1),
(72, 588, 1),
(72, 589, 1),
(72, 590, 1),
(72, 591, 1),
(72, 592, 1),
(72, 594, 1),
(72, 595, 1),
(72, 596, 1),
(72, 598, 1),
(72, 599, 1),
(72, 600, 1),
(72, 601, 1),
(72, 602, 1),
(72, 920, 1),
(72, 944, 1),
(72, 585, 2),
(72, 584, 3),
(72, 603, 3),
(72, 2143, 6),
(72, 2144, 6),
(73, 586, 1),
(73, 587, 1),
(73, 588, 1),
(73, 589, 1),
(73, 590, 1),
(73, 591, 1),
(73, 592, 1),
(73, 594, 1),
(73, 595, 1),
(73, 596, 1),
(73, 597, 1),
(73, 598, 1),
(73, 599, 1),
(73, 600, 1),
(73, 601, 1),
(73, 602, 1),
(73, 1182, 1),
(73, 585, 2),
(73, 584, 3),
(73, 648, 3),
(73, 2165, 6),
(73, 2166, 6),
(74, 586, 1),
(74, 587, 1),
(74, 588, 1),
(74, 589, 1),
(74, 590, 1),
(74, 591, 1),
(74, 592, 1),
(74, 594, 1),
(74, 595, 1),
(74, 596, 1),
(74, 597, 1),
(74, 598, 1),
(74, 599, 1),
(74, 600, 1),
(74, 601, 1),
(74, 602, 1),
(74, 627, 1),
(74, 585, 2),
(74, 584, 3),
(74, 717, 3),
(74, 2165, 6),
(74, 2187, 6),
(75, 587, 1),
(75, 588, 1),
(75, 589, 1),
(75, 590, 1),
(75, 591, 1),
(75, 592, 1),
(75, 594, 1),
(75, 595, 1),
(75, 596, 1),
(75, 597, 1),
(75, 598, 1),
(75, 599, 1),
(75, 600, 1),
(75, 601, 1),
(75, 602, 1),
(75, 830, 1),
(75, 1093, 1),
(75, 585, 2),
(75, 584, 3),
(75, 648, 3),
(75, 2165, 6),
(75, 2209, 6),
(76, 586, 1),
(76, 587, 1),
(76, 588, 1),
(76, 589, 1),
(76, 590, 1),
(76, 591, 1),
(76, 592, 1),
(76, 594, 1),
(76, 595, 1),
(76, 596, 1),
(76, 597, 1),
(76, 598, 1),
(76, 599, 1),
(76, 600, 1),
(76, 601, 1),
(76, 602, 1),
(76, 1528, 1),
(76, 585, 2),
(76, 584, 3),
(76, 648, 3),
(76, 2165, 6),
(76, 2231, 6),
(77, 586, 1),
(77, 587, 1),
(77, 588, 1),
(77, 589, 1),
(77, 590, 1),
(77, 591, 1),
(77, 592, 1),
(77, 594, 1),
(77, 595, 1),
(77, 596, 1),
(77, 597, 1),
(77, 598, 1),
(77, 599, 1),
(77, 600, 1),
(77, 601, 1),
(77, 602, 1),
(77, 1291, 1),
(77, 585, 2),
(77, 584, 3),
(77, 603, 3),
(77, 2165, 6),
(77, 2253, 6),
(78, 586, 1),
(78, 587, 1),
(78, 588, 1),
(78, 589, 1),
(78, 590, 1),
(78, 591, 1),
(78, 592, 1),
(78, 594, 1),
(78, 595, 1),
(78, 596, 1),
(78, 597, 1),
(78, 598, 1),
(78, 599, 1),
(78, 600, 1),
(78, 601, 1),
(78, 602, 1),
(78, 943, 1),
(78, 585, 2),
(78, 584, 3),
(78, 603, 3),
(78, 2165, 6),
(78, 2275, 6),
(79, 586, 1),
(79, 587, 1),
(79, 588, 1),
(79, 589, 1),
(79, 590, 1),
(79, 591, 1),
(79, 592, 1),
(79, 594, 1),
(79, 595, 1),
(79, 596, 1),
(79, 597, 1),
(79, 598, 1),
(79, 599, 1),
(79, 600, 1),
(79, 601, 1),
(79, 602, 1),
(79, 1269, 1),
(79, 585, 2),
(79, 584, 3),
(79, 648, 3),
(79, 2165, 6),
(79, 2297, 6),
(80, 586, 1),
(80, 587, 1),
(80, 588, 1),
(80, 589, 1),
(80, 590, 1),
(80, 591, 1),
(80, 592, 1),
(80, 594, 1),
(80, 595, 1),
(80, 596, 1),
(80, 597, 1),
(80, 598, 1),
(80, 599, 1),
(80, 600, 1),
(80, 601, 1),
(80, 602, 1),
(80, 2320, 1),
(80, 585, 2),
(80, 584, 3),
(80, 717, 3),
(80, 2165, 6),
(80, 2319, 6),
(81, 586, 1),
(81, 587, 1),
(81, 588, 1),
(81, 589, 1),
(81, 590, 1),
(81, 591, 1),
(81, 592, 1),
(81, 594, 1),
(81, 595, 1),
(81, 596, 1),
(81, 597, 1),
(81, 598, 1),
(81, 599, 1),
(81, 600, 1),
(81, 601, 1),
(81, 602, 1),
(81, 1528, 1),
(81, 585, 2),
(81, 584, 3),
(81, 717, 3),
(81, 2165, 6),
(81, 2341, 6),
(82, 586, 1),
(82, 587, 1),
(82, 588, 1),
(82, 589, 1),
(82, 590, 1),
(82, 591, 1),
(82, 592, 1),
(82, 594, 1),
(82, 595, 1),
(82, 596, 1),
(82, 597, 1),
(82, 598, 1),
(82, 599, 1),
(82, 600, 1),
(82, 601, 1),
(82, 602, 1),
(82, 964, 1),
(82, 585, 2),
(82, 584, 3),
(82, 603, 3),
(82, 2165, 6),
(82, 2363, 6),
(83, 586, 1),
(83, 587, 1),
(83, 588, 1),
(83, 589, 1),
(83, 590, 1),
(83, 591, 1),
(83, 592, 1),
(83, 594, 1),
(83, 595, 1),
(83, 596, 1),
(83, 598, 1),
(83, 599, 1),
(83, 600, 1),
(83, 601, 1),
(83, 602, 1),
(83, 944, 1),
(83, 1269, 1),
(83, 585, 2),
(83, 584, 3),
(83, 648, 3),
(83, 2165, 6),
(83, 2385, 6),
(84, 586, 1),
(84, 587, 1),
(84, 588, 1),
(84, 589, 1),
(84, 590, 1),
(84, 591, 1),
(84, 592, 1),
(84, 594, 1),
(84, 595, 1),
(84, 596, 1),
(84, 597, 1),
(84, 598, 1),
(84, 599, 1),
(84, 600, 1),
(84, 601, 1),
(84, 602, 1),
(84, 1528, 1),
(84, 585, 2),
(84, 584, 3),
(84, 603, 3),
(84, 2407, 6),
(84, 2408, 6),
(85, 586, 1),
(85, 587, 1),
(85, 588, 1),
(85, 589, 1),
(85, 590, 1),
(85, 591, 1),
(85, 592, 1),
(85, 594, 1),
(85, 595, 1),
(85, 596, 1),
(85, 597, 1),
(85, 598, 1),
(85, 599, 1),
(85, 600, 1),
(85, 601, 1),
(85, 602, 1),
(85, 1571, 1),
(85, 585, 2),
(85, 584, 3),
(85, 603, 3),
(85, 2407, 6),
(85, 2429, 6),
(86, 586, 1),
(86, 587, 1),
(86, 588, 1),
(86, 589, 1),
(86, 590, 1),
(86, 591, 1),
(86, 592, 1),
(86, 594, 1),
(86, 595, 1),
(86, 596, 1),
(86, 597, 1),
(86, 598, 1),
(86, 599, 1),
(86, 600, 1),
(86, 601, 1),
(86, 602, 1),
(86, 877, 1),
(86, 585, 2),
(86, 584, 3),
(86, 603, 3),
(86, 2407, 6),
(86, 2451, 6),
(87, 586, 1),
(87, 587, 1),
(87, 588, 1),
(87, 589, 1),
(87, 590, 1),
(87, 591, 1),
(87, 592, 1),
(87, 594, 1),
(87, 595, 1),
(87, 596, 1),
(87, 597, 1),
(87, 598, 1),
(87, 599, 1),
(87, 600, 1),
(87, 601, 1),
(87, 602, 1),
(87, 715, 1),
(87, 585, 2),
(87, 584, 3),
(87, 648, 3),
(87, 2407, 6),
(87, 2473, 6),
(87, 2474, 6),
(88, 586, 1),
(88, 587, 1),
(88, 588, 1),
(88, 589, 1),
(88, 590, 1),
(88, 591, 1),
(88, 592, 1),
(88, 594, 1),
(88, 595, 1),
(88, 596, 1),
(88, 597, 1),
(88, 598, 1),
(88, 599, 1),
(88, 600, 1),
(88, 601, 1),
(88, 602, 1),
(88, 762, 1),
(88, 585, 2),
(88, 584, 3),
(88, 603, 3),
(88, 2496, 6),
(88, 2497, 6),
(89, 586, 1),
(89, 587, 1),
(89, 588, 1),
(89, 589, 1),
(89, 590, 1),
(89, 591, 1),
(89, 592, 1),
(89, 594, 1),
(89, 595, 1),
(89, 596, 1),
(89, 597, 1),
(89, 598, 1),
(89, 599, 1),
(89, 600, 1),
(89, 601, 1),
(89, 602, 1),
(89, 1313, 1),
(89, 585, 2),
(89, 584, 3),
(89, 648, 3),
(89, 2518, 6),
(89, 2519, 6),
(90, 586, 1),
(90, 587, 1),
(90, 588, 1),
(90, 589, 1),
(90, 590, 1),
(90, 591, 1),
(90, 592, 1),
(90, 594, 1),
(90, 595, 1),
(90, 596, 1),
(90, 597, 1),
(90, 598, 1),
(90, 599, 1),
(90, 600, 1),
(90, 601, 1),
(90, 602, 1),
(90, 762, 1),
(90, 585, 2),
(90, 584, 3),
(90, 717, 3),
(90, 2540, 6),
(90, 2541, 6),
(91, 586, 1),
(91, 587, 1),
(91, 588, 1),
(91, 589, 1),
(91, 590, 1),
(91, 591, 1),
(91, 592, 1),
(91, 594, 1),
(91, 595, 1),
(91, 596, 1),
(91, 597, 1),
(91, 598, 1),
(91, 599, 1),
(91, 600, 1),
(91, 601, 1),
(91, 602, 1),
(91, 2564, 1),
(91, 585, 2),
(91, 584, 3),
(91, 603, 3),
(91, 2562, 6),
(91, 2563, 6),
(92, 586, 1),
(92, 587, 1),
(92, 588, 1),
(92, 589, 1),
(92, 590, 1),
(92, 591, 1),
(92, 592, 1),
(92, 594, 1),
(92, 595, 1),
(92, 596, 1),
(92, 597, 1),
(92, 598, 1),
(92, 599, 1),
(92, 600, 1),
(92, 601, 1),
(92, 602, 1),
(92, 2585, 1),
(92, 585, 2),
(92, 584, 3),
(92, 717, 3),
(92, 2562, 6),
(92, 2584, 6),
(93, 586, 1),
(93, 587, 1),
(93, 588, 1),
(93, 589, 1),
(93, 590, 1),
(93, 591, 1),
(93, 592, 1),
(93, 594, 1),
(93, 595, 1),
(93, 596, 1),
(93, 597, 1),
(93, 598, 1),
(93, 599, 1),
(93, 600, 1),
(93, 601, 1),
(93, 602, 1),
(93, 2564, 1),
(93, 585, 2),
(93, 584, 3),
(93, 648, 3),
(93, 2562, 6),
(93, 2606, 6),
(94, 587, 1),
(94, 588, 1),
(94, 589, 1),
(94, 590, 1),
(94, 591, 1),
(94, 592, 1),
(94, 594, 1),
(94, 595, 1),
(94, 596, 1),
(94, 597, 1),
(94, 598, 1),
(94, 599, 1),
(94, 600, 1),
(94, 601, 1),
(94, 602, 1),
(94, 830, 1),
(94, 2013, 1),
(94, 585, 2),
(94, 584, 3),
(94, 603, 3),
(94, 2562, 6),
(94, 2628, 6),
(95, 586, 1),
(95, 587, 1),
(95, 588, 1),
(95, 589, 1),
(95, 590, 1),
(95, 591, 1),
(95, 592, 1),
(95, 594, 1),
(95, 595, 1),
(95, 596, 1),
(95, 597, 1),
(95, 598, 1),
(95, 599, 1),
(95, 600, 1),
(95, 601, 1),
(95, 602, 1),
(95, 1313, 1),
(95, 585, 2),
(95, 584, 3),
(95, 717, 3),
(95, 2562, 6),
(95, 2650, 6),
(96, 586, 1),
(96, 587, 1),
(96, 588, 1),
(96, 589, 1),
(96, 590, 1),
(96, 591, 1),
(96, 592, 1),
(96, 594, 1),
(96, 595, 1),
(96, 596, 1),
(96, 597, 1),
(96, 598, 1),
(96, 599, 1),
(96, 600, 1),
(96, 601, 1),
(96, 602, 1),
(96, 1681, 1),
(96, 585, 2),
(96, 584, 3),
(96, 717, 3),
(96, 2672, 6),
(96, 2673, 6),
(97, 586, 1),
(97, 587, 1),
(97, 588, 1),
(97, 589, 1),
(97, 590, 1),
(97, 591, 1),
(97, 592, 1),
(97, 594, 1),
(97, 595, 1),
(97, 596, 1),
(97, 597, 1),
(97, 598, 1),
(97, 599, 1),
(97, 600, 1),
(97, 601, 1),
(97, 602, 1),
(97, 1703, 1),
(97, 585, 2),
(97, 584, 3),
(97, 648, 3),
(97, 2694, 6),
(97, 2695, 6),
(98, 586, 1),
(98, 587, 1),
(98, 588, 1),
(98, 589, 1),
(98, 590, 1),
(98, 591, 1),
(98, 592, 1),
(98, 594, 1),
(98, 595, 1),
(98, 596, 1),
(98, 597, 1),
(98, 598, 1),
(98, 599, 1),
(98, 600, 1),
(98, 601, 1),
(98, 602, 1),
(98, 647, 1),
(98, 585, 2),
(98, 584, 3),
(98, 648, 3),
(98, 2694, 6),
(98, 2716, 6),
(99, 586, 1),
(99, 587, 1),
(99, 588, 1),
(99, 589, 1),
(99, 590, 1),
(99, 591, 1),
(99, 592, 1),
(99, 594, 1),
(99, 595, 1),
(99, 596, 1),
(99, 597, 1),
(99, 598, 1),
(99, 599, 1),
(99, 600, 1),
(99, 601, 1),
(99, 602, 1),
(99, 1528, 1),
(99, 585, 2),
(99, 584, 3),
(99, 717, 3),
(99, 2694, 6),
(99, 2738, 6),
(100, 586, 1),
(100, 587, 1),
(100, 588, 1),
(100, 589, 1),
(100, 590, 1),
(100, 591, 1),
(100, 592, 1),
(100, 594, 1),
(100, 595, 1),
(100, 596, 1),
(100, 597, 1),
(100, 598, 1),
(100, 599, 1),
(100, 600, 1),
(100, 601, 1),
(100, 602, 1),
(100, 2762, 1),
(100, 585, 2),
(100, 584, 3),
(100, 717, 3),
(100, 2760, 6),
(100, 2761, 6),
(101, 586, 1),
(101, 587, 1),
(101, 588, 1),
(101, 589, 1),
(101, 590, 1),
(101, 591, 1),
(101, 592, 1),
(101, 594, 1),
(101, 595, 1),
(101, 596, 1),
(101, 597, 1),
(101, 598, 1),
(101, 599, 1),
(101, 600, 1),
(101, 601, 1),
(101, 602, 1),
(101, 2784, 1),
(101, 585, 2),
(101, 584, 3),
(101, 648, 3),
(101, 2782, 6),
(101, 2783, 6),
(102, 586, 1),
(102, 587, 1),
(102, 588, 1),
(102, 589, 1),
(102, 590, 1),
(102, 591, 1),
(102, 592, 1),
(102, 594, 1),
(102, 595, 1),
(102, 596, 1),
(102, 597, 1),
(102, 598, 1),
(102, 599, 1),
(102, 600, 1),
(102, 601, 1),
(102, 602, 1),
(102, 2013, 1),
(102, 585, 2),
(102, 584, 3),
(102, 603, 3),
(102, 2782, 6),
(102, 2804, 6),
(103, 587, 1),
(103, 588, 1),
(103, 589, 1),
(103, 590, 1),
(103, 591, 1),
(103, 592, 1),
(103, 594, 1),
(103, 595, 1),
(103, 596, 1),
(103, 597, 1),
(103, 598, 1),
(103, 599, 1),
(103, 600, 1),
(103, 601, 1),
(103, 602, 1),
(103, 830, 1),
(103, 2320, 1),
(103, 585, 2),
(103, 584, 3),
(103, 648, 3),
(103, 2209, 6),
(103, 2782, 6),
(104, 586, 1),
(104, 587, 1),
(104, 588, 1),
(104, 589, 1),
(104, 590, 1),
(104, 591, 1),
(104, 592, 1),
(104, 594, 1),
(104, 595, 1),
(104, 596, 1),
(104, 597, 1),
(104, 598, 1),
(104, 599, 1),
(104, 600, 1),
(104, 601, 1),
(104, 602, 1),
(104, 1681, 1),
(104, 585, 2),
(104, 584, 3),
(104, 717, 3),
(104, 2782, 6),
(104, 2848, 6),
(105, 586, 1),
(105, 587, 1),
(105, 588, 1),
(105, 589, 1),
(105, 590, 1),
(105, 591, 1),
(105, 592, 1),
(105, 594, 1),
(105, 595, 1),
(105, 596, 1),
(105, 597, 1),
(105, 598, 1),
(105, 599, 1),
(105, 600, 1),
(105, 601, 1),
(105, 602, 1),
(105, 2762, 1),
(105, 585, 2),
(105, 584, 3),
(105, 717, 3),
(105, 2782, 6),
(105, 2870, 6),
(106, 586, 1),
(106, 587, 1),
(106, 588, 1),
(106, 589, 1),
(106, 590, 1),
(106, 591, 1),
(106, 592, 1),
(106, 594, 1),
(106, 595, 1),
(106, 596, 1),
(106, 597, 1),
(106, 598, 1),
(106, 599, 1),
(106, 600, 1),
(106, 601, 1),
(106, 602, 1),
(106, 715, 1),
(106, 585, 2),
(106, 584, 3),
(106, 648, 3),
(106, 2363, 6),
(106, 2892, 6),
(106, 2893, 6),
(107, 586, 1),
(107, 587, 1),
(107, 588, 1),
(107, 589, 1),
(107, 590, 1),
(107, 591, 1),
(107, 592, 1),
(107, 594, 1),
(107, 595, 1),
(107, 596, 1),
(107, 597, 1),
(107, 598, 1),
(107, 599, 1),
(107, 600, 1),
(107, 601, 1),
(107, 602, 1),
(107, 2916, 1),
(107, 585, 2),
(107, 584, 3),
(107, 603, 3),
(107, 1225, 6),
(107, 2915, 6),
(108, 586, 1),
(108, 587, 1),
(108, 588, 1),
(108, 589, 1),
(108, 590, 1),
(108, 591, 1),
(108, 592, 1),
(108, 594, 1),
(108, 595, 1),
(108, 596, 1),
(108, 597, 1),
(108, 598, 1),
(108, 599, 1),
(108, 600, 1),
(108, 601, 1),
(108, 602, 1),
(108, 2939, 1),
(108, 585, 2),
(108, 584, 3),
(108, 648, 3),
(108, 2915, 6),
(108, 2937, 6),
(108, 2938, 6),
(109, 586, 1),
(109, 587, 1),
(109, 588, 1),
(109, 589, 1),
(109, 590, 1),
(109, 591, 1),
(109, 592, 1),
(109, 594, 1),
(109, 595, 1),
(109, 596, 1),
(109, 597, 1),
(109, 598, 1),
(109, 599, 1),
(109, 600, 1),
(109, 601, 1),
(109, 602, 1),
(109, 1528, 1),
(109, 585, 2),
(109, 584, 3),
(109, 603, 3),
(109, 2915, 6),
(109, 2937, 6),
(109, 2960, 6),
(110, 586, 1),
(110, 587, 1),
(110, 588, 1),
(110, 589, 1),
(110, 590, 1),
(110, 591, 1),
(110, 592, 1),
(110, 594, 1),
(110, 595, 1),
(110, 596, 1),
(110, 597, 1),
(110, 598, 1),
(110, 599, 1),
(110, 600, 1),
(110, 601, 1),
(110, 602, 1),
(110, 715, 1),
(110, 585, 2),
(110, 584, 3),
(110, 717, 3),
(110, 2915, 6),
(110, 2983, 6),
(111, 586, 1),
(111, 587, 1),
(111, 588, 1),
(111, 589, 1),
(111, 590, 1),
(111, 591, 1),
(111, 592, 1),
(111, 594, 1),
(111, 595, 1),
(111, 596, 1),
(111, 597, 1),
(111, 598, 1),
(111, 599, 1),
(111, 600, 1),
(111, 601, 1),
(111, 602, 1),
(111, 3006, 1),
(111, 585, 2),
(111, 584, 3),
(111, 717, 3),
(111, 2915, 6),
(111, 3005, 6),
(112, 586, 1),
(112, 587, 1),
(112, 588, 1),
(112, 589, 1),
(112, 590, 1),
(112, 591, 1),
(112, 592, 1),
(112, 594, 1),
(112, 595, 1),
(112, 596, 1),
(112, 597, 1),
(112, 598, 1),
(112, 599, 1),
(112, 600, 1),
(112, 601, 1),
(112, 602, 1),
(112, 1291, 1),
(112, 585, 2),
(112, 584, 3),
(112, 648, 3),
(112, 2915, 6),
(112, 3027, 6),
(113, 586, 1),
(113, 587, 1),
(113, 588, 1),
(113, 589, 1),
(113, 590, 1),
(113, 591, 1),
(113, 592, 1),
(113, 594, 1),
(113, 595, 1),
(113, 596, 1),
(113, 597, 1),
(113, 598, 1),
(113, 599, 1),
(113, 600, 1),
(113, 601, 1),
(113, 602, 1),
(113, 2916, 1),
(113, 585, 2),
(113, 584, 3),
(113, 648, 3),
(113, 2915, 6),
(113, 3049, 6),
(114, 586, 1),
(114, 587, 1),
(114, 588, 1),
(114, 589, 1),
(114, 590, 1),
(114, 591, 1),
(114, 592, 1),
(114, 594, 1),
(114, 595, 1),
(114, 596, 1),
(114, 597, 1),
(114, 598, 1),
(114, 599, 1),
(114, 600, 1),
(114, 601, 1),
(114, 602, 1),
(114, 1204, 1),
(114, 585, 2),
(114, 584, 3),
(114, 603, 3),
(114, 2915, 6),
(114, 2938, 6),
(114, 3071, 6),
(115, 586, 1),
(115, 587, 1),
(115, 588, 1),
(115, 589, 1),
(115, 590, 1),
(115, 591, 1),
(115, 592, 1),
(115, 594, 1),
(115, 595, 1),
(115, 596, 1),
(115, 597, 1),
(115, 598, 1),
(115, 599, 1),
(115, 600, 1),
(115, 601, 1),
(115, 602, 1),
(115, 1182, 1),
(115, 585, 2),
(115, 584, 3),
(115, 603, 3),
(115, 2915, 6),
(115, 3094, 6),
(116, 586, 1),
(116, 587, 1),
(116, 588, 1),
(116, 589, 1),
(116, 590, 1),
(116, 591, 1),
(116, 592, 1),
(116, 594, 1),
(116, 595, 1),
(116, 596, 1),
(116, 597, 1),
(116, 598, 1),
(116, 599, 1),
(116, 600, 1),
(116, 601, 1),
(116, 602, 1),
(116, 716, 1),
(116, 585, 2),
(116, 584, 3),
(116, 717, 3),
(116, 2915, 6),
(116, 3116, 6),
(117, 587, 1),
(117, 588, 1),
(117, 589, 1),
(117, 590, 1),
(117, 591, 1),
(117, 592, 1),
(117, 594, 1),
(117, 595, 1),
(117, 596, 1),
(117, 597, 1),
(117, 598, 1),
(117, 599, 1),
(117, 600, 1),
(117, 601, 1),
(117, 602, 1),
(117, 830, 1),
(117, 1225, 1),
(117, 585, 2),
(117, 584, 3),
(117, 603, 3),
(117, 2915, 6),
(117, 3138, 6),
(118, 586, 1),
(118, 587, 1),
(118, 588, 1),
(118, 589, 1),
(118, 590, 1),
(118, 591, 1),
(118, 592, 1),
(118, 594, 1),
(118, 595, 1),
(118, 596, 1),
(118, 597, 1),
(118, 598, 1),
(118, 599, 1),
(118, 600, 1),
(118, 601, 1),
(118, 602, 1),
(118, 1291, 1),
(118, 585, 2),
(118, 584, 3),
(118, 603, 3),
(118, 2915, 6),
(118, 3160, 6),
(119, 586, 1),
(119, 587, 1),
(119, 588, 1),
(119, 589, 1),
(119, 590, 1),
(119, 591, 1),
(119, 592, 1),
(119, 594, 1),
(119, 595, 1),
(119, 596, 1),
(119, 597, 1),
(119, 598, 1),
(119, 599, 1),
(119, 600, 1),
(119, 601, 1),
(119, 602, 1),
(119, 1182, 1),
(119, 585, 2),
(119, 584, 3),
(119, 648, 3),
(119, 2915, 6),
(119, 3182, 6),
(120, 586, 1),
(120, 587, 1),
(120, 588, 1),
(120, 589, 1),
(120, 590, 1),
(120, 591, 1),
(120, 592, 1),
(120, 594, 1),
(120, 595, 1),
(120, 596, 1),
(120, 597, 1),
(120, 598, 1),
(120, 599, 1),
(120, 600, 1),
(120, 601, 1),
(120, 602, 1),
(120, 2762, 1),
(120, 585, 2),
(120, 584, 3),
(120, 648, 3),
(120, 2915, 6),
(120, 3204, 6),
(121, 586, 1),
(121, 587, 1),
(121, 588, 1),
(121, 589, 1),
(121, 590, 1),
(121, 591, 1),
(121, 592, 1),
(121, 594, 1),
(121, 595, 1),
(121, 596, 1),
(121, 597, 1),
(121, 598, 1),
(121, 599, 1),
(121, 600, 1),
(121, 601, 1),
(121, 602, 1),
(121, 3228, 1),
(121, 585, 2),
(121, 584, 3),
(121, 648, 3),
(121, 2915, 6),
(121, 3226, 6),
(121, 3227, 6),
(122, 586, 1),
(122, 587, 1),
(122, 588, 1),
(122, 589, 1),
(122, 590, 1),
(122, 591, 1),
(122, 592, 1),
(122, 594, 1),
(122, 595, 1),
(122, 596, 1),
(122, 597, 1),
(122, 598, 1),
(122, 599, 1),
(122, 600, 1),
(122, 601, 1),
(122, 602, 1),
(122, 3250, 1),
(122, 585, 2),
(122, 584, 3),
(122, 648, 3),
(122, 2915, 6),
(122, 3249, 6),
(123, 586, 1),
(123, 587, 1),
(123, 588, 1),
(123, 589, 1),
(123, 590, 1),
(123, 591, 1),
(123, 592, 1),
(123, 594, 1),
(123, 595, 1),
(123, 596, 1),
(123, 597, 1),
(123, 598, 1),
(123, 599, 1),
(123, 600, 1),
(123, 601, 1),
(123, 602, 1),
(123, 2939, 1),
(123, 585, 2),
(123, 584, 3),
(123, 648, 3),
(123, 2915, 6),
(123, 3271, 6),
(124, 586, 1),
(124, 587, 1),
(124, 588, 1),
(124, 589, 1),
(124, 590, 1),
(124, 591, 1),
(124, 592, 1),
(124, 594, 1),
(124, 595, 1),
(124, 596, 1),
(124, 597, 1),
(124, 598, 1),
(124, 599, 1),
(124, 600, 1),
(124, 601, 1),
(124, 602, 1),
(124, 3294, 1),
(124, 585, 2),
(124, 584, 3),
(124, 603, 3),
(124, 2915, 6),
(124, 3293, 6),
(125, 586, 1),
(125, 587, 1),
(125, 588, 1),
(125, 589, 1),
(125, 590, 1),
(125, 591, 1),
(125, 592, 1),
(125, 594, 1),
(125, 595, 1),
(125, 596, 1),
(125, 597, 1),
(125, 598, 1),
(125, 599, 1),
(125, 600, 1),
(125, 601, 1),
(125, 602, 1),
(125, 3315, 1),
(125, 585, 2),
(125, 584, 3),
(125, 603, 3),
(125, 2915, 6),
(126, 586, 1),
(126, 587, 1),
(126, 588, 1),
(126, 589, 1),
(126, 590, 1),
(126, 591, 1),
(126, 592, 1),
(126, 594, 1),
(126, 595, 1),
(126, 596, 1),
(126, 597, 1),
(126, 598, 1),
(126, 599, 1),
(126, 600, 1),
(126, 601, 1),
(126, 602, 1),
(126, 1269, 1),
(126, 585, 2),
(126, 584, 3),
(126, 603, 3),
(126, 3336, 6),
(127, 586, 1),
(127, 587, 1),
(127, 588, 1),
(127, 589, 1),
(127, 590, 1),
(127, 591, 1),
(127, 592, 1),
(127, 594, 1),
(127, 595, 1),
(127, 596, 1),
(127, 597, 1),
(127, 598, 1),
(127, 599, 1),
(127, 600, 1),
(127, 601, 1),
(127, 602, 1),
(127, 585, 2),
(127, 584, 3),
(127, 717, 3),
(127, 3357, 6),
(127, 920, 7),
(128, 586, 1),
(128, 587, 1),
(128, 588, 1),
(128, 589, 1),
(128, 590, 1),
(128, 591, 1),
(128, 592, 1),
(128, 594, 1),
(128, 595, 1),
(128, 596, 1),
(128, 597, 1),
(128, 598, 1),
(128, 599, 1),
(128, 600, 1),
(128, 601, 1),
(128, 602, 1),
(128, 3315, 1),
(128, 585, 2),
(128, 584, 3),
(128, 717, 3),
(128, 670, 6),
(128, 3357, 6),
(129, 586, 1),
(129, 587, 1),
(129, 588, 1),
(129, 589, 1),
(129, 590, 1),
(129, 591, 1),
(129, 592, 1),
(129, 594, 1),
(129, 595, 1),
(129, 596, 1),
(129, 597, 1),
(129, 598, 1),
(129, 599, 1),
(129, 600, 1),
(129, 601, 1),
(129, 602, 1),
(129, 1225, 1),
(129, 585, 2),
(129, 584, 3),
(129, 717, 3),
(129, 3357, 6),
(129, 3400, 6),
(130, 586, 1),
(130, 587, 1),
(130, 588, 1),
(130, 589, 1),
(130, 590, 1),
(130, 591, 1),
(130, 592, 1),
(130, 594, 1),
(130, 595, 1),
(130, 596, 1),
(130, 597, 1),
(130, 598, 1),
(130, 599, 1),
(130, 600, 1),
(130, 601, 1),
(130, 602, 1),
(130, 3423, 1),
(130, 585, 2),
(130, 584, 3),
(130, 648, 3),
(130, 3357, 6),
(130, 3422, 6),
(131, 586, 1),
(131, 587, 1),
(131, 588, 1),
(131, 589, 1),
(131, 590, 1),
(131, 591, 1),
(131, 592, 1),
(131, 594, 1),
(131, 595, 1),
(131, 596, 1),
(131, 597, 1),
(131, 598, 1),
(131, 599, 1),
(131, 600, 1),
(131, 601, 1),
(131, 602, 1),
(131, 1006, 1),
(131, 585, 2),
(131, 584, 3),
(131, 648, 3),
(131, 3357, 6),
(131, 3444, 6),
(132, 586, 1),
(132, 587, 1),
(132, 588, 1),
(132, 589, 1),
(132, 590, 1),
(132, 591, 1),
(132, 592, 1),
(132, 594, 1),
(132, 595, 1),
(132, 596, 1),
(132, 597, 1),
(132, 598, 1),
(132, 599, 1),
(132, 600, 1),
(132, 601, 1),
(132, 602, 1),
(132, 716, 1),
(132, 585, 2),
(132, 584, 3),
(132, 717, 3),
(132, 3357, 6),
(132, 3466, 6),
(133, 586, 1),
(133, 587, 1),
(133, 588, 1),
(133, 589, 1),
(133, 590, 1),
(133, 591, 1),
(133, 592, 1),
(133, 594, 1),
(133, 595, 1),
(133, 596, 1),
(133, 597, 1),
(133, 598, 1),
(133, 599, 1),
(133, 600, 1),
(133, 601, 1),
(133, 602, 1),
(133, 3400, 1),
(133, 585, 2),
(133, 584, 3),
(133, 603, 3),
(133, 3357, 6),
(133, 3488, 6),
(134, 586, 1),
(134, 587, 1),
(134, 588, 1),
(134, 589, 1),
(134, 590, 1),
(134, 591, 1),
(134, 592, 1),
(134, 594, 1),
(134, 595, 1),
(134, 596, 1),
(134, 597, 1),
(134, 598, 1),
(134, 599, 1),
(134, 600, 1),
(134, 601, 1),
(134, 602, 1),
(134, 692, 1),
(134, 585, 2),
(134, 584, 3),
(134, 648, 3),
(134, 716, 6),
(134, 3357, 6),
(135, 586, 1),
(135, 587, 1),
(135, 588, 1),
(135, 589, 1),
(135, 590, 1),
(135, 591, 1),
(135, 592, 1),
(135, 594, 1),
(135, 595, 1),
(135, 596, 1),
(135, 598, 1),
(135, 599, 1),
(135, 600, 1),
(135, 601, 1),
(135, 602, 1),
(135, 944, 1),
(135, 2762, 1),
(135, 585, 2),
(135, 584, 3),
(135, 717, 3),
(135, 3532, 6),
(135, 3533, 6),
(136, 586, 1),
(136, 587, 1),
(136, 588, 1),
(136, 589, 1),
(136, 590, 1),
(136, 591, 1),
(136, 592, 1),
(136, 594, 1),
(136, 595, 1),
(136, 596, 1),
(136, 597, 1),
(136, 598, 1),
(136, 599, 1),
(136, 600, 1),
(136, 601, 1),
(136, 602, 1),
(136, 920, 1),
(136, 585, 2),
(136, 584, 3),
(136, 717, 3),
(136, 3554, 6),
(136, 3555, 6),
(137, 586, 1),
(137, 587, 1),
(137, 588, 1),
(137, 589, 1),
(137, 590, 1),
(137, 591, 1),
(137, 592, 1),
(137, 594, 1),
(137, 595, 1),
(137, 596, 1),
(137, 597, 1),
(137, 598, 1),
(137, 599, 1),
(137, 600, 1),
(137, 601, 1),
(137, 602, 1),
(137, 2916, 1),
(137, 585, 2),
(137, 584, 3),
(137, 603, 3),
(137, 3554, 6),
(137, 3576, 6),
(137, 3577, 6),
(138, 586, 1),
(138, 587, 1),
(138, 588, 1),
(138, 589, 1),
(138, 590, 1),
(138, 591, 1),
(138, 592, 1),
(138, 594, 1),
(138, 595, 1),
(138, 596, 1),
(138, 597, 1),
(138, 598, 1),
(138, 599, 1),
(138, 600, 1),
(138, 601, 1),
(138, 602, 1),
(138, 1071, 1),
(138, 585, 2),
(138, 584, 3),
(138, 603, 3),
(138, 3554, 6),
(138, 3599, 6),
(139, 586, 1),
(139, 587, 1),
(139, 588, 1),
(139, 589, 1),
(139, 590, 1),
(139, 591, 1),
(139, 592, 1),
(139, 594, 1),
(139, 595, 1),
(139, 596, 1),
(139, 597, 1),
(139, 598, 1),
(139, 599, 1),
(139, 600, 1),
(139, 601, 1),
(139, 602, 1),
(139, 1969, 1),
(139, 585, 2),
(139, 584, 3),
(139, 717, 3),
(139, 3554, 6),
(139, 3621, 6),
(139, 3622, 6),
(140, 586, 1),
(140, 587, 1),
(140, 588, 1),
(140, 589, 1),
(140, 590, 1),
(140, 591, 1),
(140, 592, 1),
(140, 594, 1),
(140, 595, 1),
(140, 596, 1),
(140, 598, 1),
(140, 599, 1),
(140, 600, 1),
(140, 601, 1),
(140, 602, 1),
(140, 944, 1),
(140, 3645, 1),
(140, 585, 2),
(140, 584, 3),
(140, 717, 3),
(140, 3554, 6),
(140, 3577, 6),
(140, 3644, 6),
(141, 586, 1),
(141, 587, 1),
(141, 588, 1),
(141, 589, 1),
(141, 590, 1),
(141, 591, 1),
(141, 592, 1),
(141, 594, 1),
(141, 595, 1),
(141, 596, 1),
(141, 597, 1),
(141, 598, 1),
(141, 599, 1),
(141, 600, 1),
(141, 601, 1),
(141, 602, 1),
(141, 2320, 1),
(141, 585, 2),
(141, 584, 3),
(141, 603, 3),
(141, 1462, 6),
(141, 3554, 6),
(142, 586, 1),
(142, 587, 1),
(142, 588, 1),
(142, 589, 1),
(142, 590, 1),
(142, 591, 1),
(142, 592, 1),
(142, 594, 1),
(142, 595, 1),
(142, 596, 1),
(142, 597, 1),
(142, 598, 1),
(142, 599, 1),
(142, 600, 1),
(142, 601, 1),
(142, 602, 1),
(142, 1681, 1),
(142, 585, 2),
(142, 584, 3),
(142, 648, 3),
(142, 3554, 6),
(142, 3689, 6),
(143, 586, 1),
(143, 587, 1),
(143, 588, 1),
(143, 589, 1),
(143, 590, 1),
(143, 591, 1),
(143, 592, 1),
(143, 594, 1),
(143, 595, 1),
(143, 596, 1),
(143, 597, 1),
(143, 598, 1),
(143, 599, 1),
(143, 600, 1),
(143, 601, 1),
(143, 602, 1),
(143, 716, 1),
(143, 585, 2),
(143, 584, 3),
(143, 603, 3),
(143, 1462, 6),
(143, 3554, 6),
(144, 586, 1),
(144, 587, 1),
(144, 588, 1),
(144, 589, 1),
(144, 590, 1),
(144, 591, 1),
(144, 592, 1),
(144, 594, 1),
(144, 595, 1),
(144, 596, 1),
(144, 597, 1),
(144, 598, 1),
(144, 599, 1),
(144, 600, 1),
(144, 601, 1),
(144, 602, 1),
(144, 3734, 1),
(144, 585, 2),
(144, 584, 3),
(144, 717, 3),
(144, 3554, 6),
(144, 3733, 6),
(145, 586, 1),
(145, 587, 1),
(145, 588, 1),
(145, 589, 1),
(145, 590, 1),
(145, 591, 1),
(145, 592, 1),
(145, 594, 1),
(145, 595, 1),
(145, 596, 1),
(145, 597, 1),
(145, 598, 1),
(145, 599, 1),
(145, 600, 1),
(145, 601, 1),
(145, 602, 1),
(145, 2320, 1),
(145, 585, 2),
(145, 584, 3),
(145, 717, 3),
(145, 3554, 6),
(145, 3755, 6),
(146, 586, 1),
(146, 587, 1),
(146, 588, 1),
(146, 589, 1),
(146, 590, 1),
(146, 591, 1),
(146, 592, 1),
(146, 594, 1),
(146, 595, 1),
(146, 596, 1),
(146, 597, 1),
(146, 598, 1),
(146, 599, 1),
(146, 600, 1),
(146, 601, 1),
(146, 602, 1),
(146, 1881, 1),
(146, 585, 2),
(146, 584, 3),
(146, 717, 3),
(146, 3554, 6),
(146, 3755, 6),
(147, 586, 1),
(147, 587, 1),
(147, 588, 1),
(147, 589, 1),
(147, 590, 1),
(147, 591, 1),
(147, 592, 1),
(147, 594, 1),
(147, 595, 1),
(147, 596, 1),
(147, 597, 1),
(147, 598, 1),
(147, 599, 1),
(147, 600, 1),
(147, 601, 1),
(147, 602, 1),
(147, 2585, 1),
(147, 585, 2),
(147, 584, 3),
(147, 648, 3),
(147, 3554, 6),
(147, 3799, 6),
(148, 586, 1),
(148, 587, 1),
(148, 588, 1),
(148, 589, 1),
(148, 590, 1),
(148, 591, 1),
(148, 592, 1),
(148, 594, 1),
(148, 595, 1),
(148, 596, 1),
(148, 597, 1),
(148, 598, 1),
(148, 599, 1),
(148, 600, 1),
(148, 601, 1),
(148, 602, 1),
(148, 1969, 1),
(148, 585, 2),
(148, 584, 3),
(148, 717, 3),
(148, 3554, 6),
(148, 3821, 6),
(148, 3822, 6),
(148, 3823, 6),
(149, 586, 1),
(149, 587, 1),
(149, 588, 1),
(149, 589, 1),
(149, 590, 1),
(149, 591, 1),
(149, 592, 1),
(149, 594, 1),
(149, 595, 1),
(149, 596, 1),
(149, 597, 1),
(149, 598, 1),
(149, 599, 1),
(149, 600, 1),
(149, 601, 1),
(149, 602, 1),
(149, 1593, 1),
(149, 585, 2),
(149, 584, 3),
(149, 717, 3),
(149, 3554, 6),
(150, 586, 1),
(150, 587, 1),
(150, 588, 1),
(150, 589, 1),
(150, 590, 1),
(150, 591, 1),
(150, 592, 1),
(150, 594, 1),
(150, 595, 1),
(150, 596, 1),
(150, 597, 1),
(150, 598, 1),
(150, 599, 1),
(150, 600, 1),
(150, 601, 1),
(150, 602, 1),
(150, 2939, 1),
(150, 585, 2),
(150, 584, 3),
(150, 648, 3),
(150, 3554, 6),
(150, 3866, 6),
(151, 586, 1),
(151, 587, 1),
(151, 588, 1),
(151, 589, 1),
(151, 590, 1),
(151, 591, 1),
(151, 592, 1),
(151, 594, 1),
(151, 595, 1),
(151, 596, 1),
(151, 597, 1),
(151, 598, 1),
(151, 599, 1),
(151, 600, 1),
(151, 601, 1),
(151, 602, 1),
(151, 3888, 1),
(151, 585, 2),
(151, 584, 3),
(151, 648, 3),
(151, 3554, 6),
(151, 3866, 6),
(152, 586, 1),
(152, 587, 1),
(152, 588, 1),
(152, 589, 1),
(152, 590, 1),
(152, 591, 1),
(152, 592, 1),
(152, 594, 1),
(152, 595, 1),
(152, 596, 1),
(152, 597, 1),
(152, 598, 1),
(152, 599, 1),
(152, 600, 1),
(152, 601, 1),
(152, 602, 1),
(152, 3911, 1),
(152, 585, 2),
(152, 584, 3),
(152, 603, 3),
(152, 3554, 6),
(152, 3910, 6),
(153, 586, 1),
(153, 587, 1),
(153, 588, 1),
(153, 589, 1),
(153, 590, 1),
(153, 591, 1),
(153, 592, 1),
(153, 594, 1),
(153, 595, 1),
(153, 596, 1),
(153, 597, 1),
(153, 598, 1),
(153, 599, 1),
(153, 600, 1),
(153, 601, 1),
(153, 602, 1),
(153, 2939, 1),
(153, 585, 2),
(153, 584, 3),
(153, 648, 3),
(153, 3554, 6),
(153, 3932, 6),
(154, 586, 1),
(154, 587, 1),
(154, 588, 1),
(154, 589, 1),
(154, 590, 1),
(154, 591, 1),
(154, 592, 1),
(154, 594, 1),
(154, 595, 1),
(154, 596, 1),
(154, 597, 1),
(154, 598, 1),
(154, 599, 1),
(154, 600, 1),
(154, 601, 1),
(154, 602, 1),
(154, 3645, 1),
(154, 585, 2),
(154, 584, 3),
(154, 717, 3),
(154, 3554, 6),
(154, 3954, 6),
(155, 586, 1),
(155, 587, 1),
(155, 588, 1),
(155, 589, 1),
(155, 590, 1),
(155, 591, 1),
(155, 592, 1),
(155, 594, 1),
(155, 595, 1),
(155, 596, 1),
(155, 597, 1),
(155, 598, 1),
(155, 599, 1),
(155, 600, 1),
(155, 601, 1),
(155, 602, 1),
(155, 2916, 1),
(155, 585, 2),
(155, 584, 3),
(155, 717, 3),
(155, 3554, 6),
(155, 3976, 6),
(156, 586, 1),
(156, 587, 1),
(156, 588, 1),
(156, 589, 1),
(156, 590, 1),
(156, 591, 1),
(156, 592, 1),
(156, 594, 1),
(156, 595, 1),
(156, 596, 1),
(156, 597, 1),
(156, 598, 1),
(156, 599, 1),
(156, 600, 1),
(156, 601, 1),
(156, 602, 1),
(156, 1204, 1),
(156, 585, 2),
(156, 584, 3),
(156, 603, 3),
(156, 3554, 6),
(156, 3998, 6),
(157, 586, 1),
(157, 587, 1),
(157, 588, 1),
(157, 589, 1),
(157, 590, 1),
(157, 591, 1),
(157, 592, 1),
(157, 594, 1),
(157, 595, 1),
(157, 596, 1),
(157, 597, 1),
(157, 598, 1),
(157, 599, 1),
(157, 600, 1),
(157, 601, 1),
(157, 602, 1),
(157, 964, 1),
(157, 585, 2),
(157, 584, 3),
(157, 717, 3),
(157, 3554, 6),
(157, 4020, 6),
(158, 586, 1),
(158, 587, 1),
(158, 588, 1),
(158, 589, 1),
(158, 590, 1),
(158, 591, 1),
(158, 592, 1),
(158, 594, 1),
(158, 595, 1),
(158, 596, 1),
(158, 597, 1),
(158, 598, 1),
(158, 599, 1),
(158, 600, 1),
(158, 601, 1),
(158, 602, 1),
(158, 4044, 1),
(158, 585, 2),
(158, 584, 3),
(158, 717, 3),
(158, 4042, 6),
(158, 4043, 6),
(159, 586, 1),
(159, 587, 1),
(159, 588, 1),
(159, 589, 1),
(159, 590, 1),
(159, 591, 1),
(159, 592, 1),
(159, 594, 1),
(159, 595, 1),
(159, 596, 1),
(159, 597, 1),
(159, 598, 1),
(159, 599, 1),
(159, 600, 1),
(159, 601, 1),
(159, 602, 1),
(159, 762, 1),
(159, 585, 2),
(159, 584, 3),
(159, 603, 3),
(159, 3577, 6),
(160, 586, 1),
(160, 587, 1),
(160, 588, 1),
(160, 589, 1),
(160, 590, 1),
(160, 591, 1),
(160, 592, 1),
(160, 594, 1),
(160, 595, 1),
(160, 596, 1),
(160, 597, 1),
(160, 598, 1),
(160, 599, 1),
(160, 600, 1),
(160, 601, 1),
(160, 602, 1),
(160, 2320, 1),
(160, 585, 2),
(160, 584, 3),
(160, 648, 3),
(160, 3577, 6),
(161, 586, 1),
(161, 587, 1),
(161, 588, 1),
(161, 589, 1),
(161, 590, 1),
(161, 591, 1),
(161, 592, 1),
(161, 594, 1),
(161, 595, 1),
(161, 596, 1),
(161, 597, 1),
(161, 598, 1),
(161, 599, 1),
(161, 600, 1),
(161, 601, 1),
(161, 602, 1),
(161, 920, 1),
(161, 585, 2),
(161, 584, 3),
(161, 717, 3),
(161, 3577, 6),
(162, 586, 1),
(162, 587, 1),
(162, 588, 1),
(162, 589, 1),
(162, 590, 1),
(162, 591, 1),
(162, 592, 1),
(162, 594, 1),
(162, 595, 1),
(162, 596, 1),
(162, 597, 1),
(162, 598, 1),
(162, 599, 1),
(162, 600, 1),
(162, 601, 1),
(162, 602, 1),
(162, 692, 1),
(162, 585, 2),
(162, 584, 3),
(162, 603, 3),
(162, 3577, 6),
(163, 586, 1),
(163, 587, 1),
(163, 588, 1),
(163, 589, 1),
(163, 590, 1),
(163, 591, 1),
(163, 592, 1),
(163, 594, 1),
(163, 595, 1),
(163, 596, 1),
(163, 597, 1),
(163, 598, 1),
(163, 599, 1),
(163, 600, 1),
(163, 601, 1),
(163, 602, 1),
(163, 3911, 1),
(163, 585, 2),
(163, 584, 3),
(163, 603, 3),
(163, 3577, 6),
(164, 586, 1),
(164, 587, 1),
(164, 588, 1),
(164, 589, 1),
(164, 590, 1),
(164, 591, 1),
(164, 592, 1),
(164, 594, 1),
(164, 595, 1),
(164, 596, 1),
(164, 597, 1),
(164, 598, 1),
(164, 599, 1),
(164, 600, 1),
(164, 601, 1),
(164, 602, 1),
(164, 3888, 1),
(164, 585, 2),
(164, 584, 3),
(164, 717, 3),
(164, 3577, 6),
(165, 586, 1),
(165, 587, 1),
(165, 588, 1),
(165, 589, 1),
(165, 590, 1),
(165, 591, 1),
(165, 592, 1),
(165, 594, 1),
(165, 595, 1),
(165, 596, 1),
(165, 597, 1),
(165, 598, 1),
(165, 599, 1),
(165, 600, 1),
(165, 601, 1),
(165, 602, 1),
(165, 670, 1),
(165, 585, 2),
(165, 584, 3),
(165, 717, 3),
(165, 3577, 6),
(166, 586, 1),
(166, 587, 1),
(166, 588, 1),
(166, 589, 1),
(166, 590, 1),
(166, 591, 1),
(166, 592, 1),
(166, 594, 1),
(166, 595, 1),
(166, 596, 1),
(166, 597, 1),
(166, 598, 1),
(166, 599, 1),
(166, 600, 1),
(166, 601, 1),
(166, 602, 1),
(166, 4211, 1),
(166, 585, 2),
(166, 584, 3),
(166, 717, 3),
(166, 3577, 6),
(167, 586, 1),
(167, 587, 1),
(167, 588, 1),
(167, 589, 1),
(167, 590, 1),
(167, 591, 1),
(167, 592, 1),
(167, 594, 1),
(167, 595, 1),
(167, 596, 1),
(167, 597, 1),
(167, 598, 1),
(167, 599, 1),
(167, 600, 1),
(167, 601, 1),
(167, 602, 1),
(167, 4234, 1),
(167, 585, 2),
(167, 584, 3),
(167, 648, 3),
(167, 4232, 6),
(167, 4233, 6),
(168, 586, 1),
(168, 587, 1),
(168, 588, 1),
(168, 589, 1),
(168, 590, 1),
(168, 591, 1),
(168, 592, 1),
(168, 594, 1),
(168, 595, 1),
(168, 596, 1),
(168, 597, 1),
(168, 598, 1),
(168, 599, 1),
(168, 600, 1),
(168, 601, 1),
(168, 602, 1),
(168, 605, 1),
(168, 585, 2),
(168, 584, 3),
(168, 648, 3),
(168, 4254, 6),
(168, 4255, 6),
(169, 586, 1),
(169, 587, 1),
(169, 588, 1),
(169, 589, 1),
(169, 590, 1),
(169, 591, 1),
(169, 592, 1),
(169, 594, 1),
(169, 595, 1),
(169, 596, 1),
(169, 597, 1),
(169, 598, 1),
(169, 599, 1),
(169, 600, 1),
(169, 601, 1),
(169, 602, 1),
(169, 3315, 1),
(169, 585, 2),
(169, 584, 3),
(169, 648, 3),
(169, 4276, 6),
(169, 4277, 6),
(170, 586, 1),
(170, 587, 1),
(170, 588, 1),
(170, 589, 1),
(170, 590, 1),
(170, 591, 1),
(170, 592, 1),
(170, 594, 1),
(170, 595, 1),
(170, 596, 1),
(170, 597, 1),
(170, 598, 1),
(170, 599, 1),
(170, 600, 1),
(170, 601, 1),
(170, 602, 1),
(170, 1006, 1),
(170, 585, 2),
(170, 584, 3),
(170, 603, 3),
(170, 4276, 6),
(170, 4298, 6),
(171, 586, 1),
(171, 587, 1),
(171, 588, 1),
(171, 589, 1),
(171, 590, 1),
(171, 591, 1),
(171, 592, 1),
(171, 594, 1),
(171, 595, 1),
(171, 596, 1),
(171, 597, 1),
(171, 598, 1),
(171, 599, 1),
(171, 600, 1),
(171, 601, 1),
(171, 602, 1),
(171, 1006, 1),
(171, 585, 2),
(171, 584, 3),
(171, 717, 3),
(171, 4276, 6),
(171, 4320, 6),
(172, 586, 1),
(172, 587, 1),
(172, 588, 1),
(172, 589, 1),
(172, 590, 1),
(172, 591, 1),
(172, 592, 1),
(172, 594, 1),
(172, 595, 1),
(172, 596, 1),
(172, 597, 1),
(172, 598, 1),
(172, 599, 1),
(172, 600, 1),
(172, 601, 1),
(172, 602, 1),
(172, 1093, 1),
(172, 585, 2),
(172, 584, 3),
(172, 717, 3),
(172, 4276, 6),
(173, 586, 1),
(173, 587, 1),
(173, 588, 1),
(173, 589, 1),
(173, 590, 1),
(173, 591, 1),
(173, 592, 1),
(173, 594, 1),
(173, 595, 1),
(173, 596, 1),
(173, 597, 1),
(173, 598, 1),
(173, 599, 1),
(173, 600, 1),
(173, 601, 1),
(173, 602, 1),
(173, 1703, 1),
(173, 585, 2),
(173, 584, 3),
(173, 717, 3),
(173, 4276, 6),
(173, 4363, 6),
(174, 586, 1),
(174, 587, 1),
(174, 588, 1),
(174, 589, 1),
(174, 590, 1),
(174, 591, 1),
(174, 592, 1),
(174, 594, 1),
(174, 595, 1),
(174, 596, 1),
(174, 597, 1),
(174, 598, 1),
(174, 599, 1),
(174, 600, 1),
(174, 601, 1),
(174, 602, 1),
(174, 1006, 1),
(174, 585, 2),
(174, 584, 3),
(174, 603, 3),
(174, 4276, 6),
(174, 4385, 6),
(175, 586, 1),
(175, 587, 1),
(175, 588, 1),
(175, 589, 1),
(175, 590, 1),
(175, 591, 1),
(175, 592, 1),
(175, 594, 1),
(175, 595, 1),
(175, 596, 1),
(175, 597, 1),
(175, 598, 1),
(175, 599, 1),
(175, 600, 1),
(175, 601, 1),
(175, 602, 1),
(175, 807, 1),
(175, 585, 2),
(175, 584, 3),
(175, 717, 3),
(175, 4276, 6),
(175, 4407, 6),
(176, 586, 1),
(176, 587, 1),
(176, 588, 1),
(176, 589, 1),
(176, 590, 1),
(176, 591, 1),
(176, 592, 1),
(176, 594, 1),
(176, 595, 1),
(176, 596, 1),
(176, 597, 1),
(176, 598, 1),
(176, 599, 1),
(176, 600, 1),
(176, 601, 1),
(176, 602, 1),
(176, 3423, 1),
(176, 585, 2),
(176, 584, 3),
(176, 648, 3),
(176, 4276, 6),
(176, 4429, 6),
(177, 586, 1),
(177, 587, 1),
(177, 588, 1),
(177, 589, 1),
(177, 590, 1),
(177, 591, 1),
(177, 592, 1),
(177, 594, 1),
(177, 595, 1),
(177, 596, 1),
(177, 597, 1),
(177, 598, 1),
(177, 599, 1),
(177, 600, 1),
(177, 601, 1),
(177, 602, 1),
(177, 964, 1),
(177, 585, 2),
(177, 584, 3),
(177, 603, 3),
(177, 4276, 6),
(177, 4451, 6),
(178, 586, 1),
(178, 587, 1),
(178, 588, 1),
(178, 589, 1),
(178, 590, 1),
(178, 591, 1),
(178, 592, 1),
(178, 594, 1),
(178, 595, 1),
(178, 596, 1),
(178, 597, 1),
(178, 598, 1),
(178, 599, 1),
(178, 600, 1),
(178, 601, 1),
(178, 602, 1),
(178, 3423, 1),
(178, 585, 2),
(178, 584, 3),
(178, 717, 3),
(178, 4276, 6),
(178, 4473, 6),
(179, 586, 1),
(179, 587, 1),
(179, 588, 1),
(179, 589, 1),
(179, 590, 1),
(179, 591, 1),
(179, 592, 1),
(179, 594, 1),
(179, 595, 1),
(179, 596, 1),
(179, 597, 1),
(179, 598, 1),
(179, 599, 1),
(179, 600, 1),
(179, 601, 1),
(179, 602, 1),
(179, 3006, 1),
(179, 585, 2),
(179, 584, 3),
(179, 603, 3),
(179, 4276, 6),
(179, 4495, 6),
(180, 586, 1),
(180, 587, 1),
(180, 588, 1),
(180, 589, 1),
(180, 590, 1),
(180, 591, 1),
(180, 592, 1),
(180, 594, 1),
(180, 595, 1),
(180, 596, 1),
(180, 597, 1),
(180, 598, 1),
(180, 599, 1),
(180, 600, 1),
(180, 601, 1),
(180, 602, 1),
(180, 3423, 1),
(180, 585, 2),
(180, 584, 3),
(180, 717, 3),
(180, 4517, 6),
(180, 4518, 6),
(181, 586, 1),
(181, 587, 1),
(181, 588, 1),
(181, 589, 1),
(181, 590, 1),
(181, 591, 1),
(181, 592, 1),
(181, 594, 1),
(181, 595, 1),
(181, 596, 1),
(181, 597, 1),
(181, 598, 1),
(181, 599, 1),
(181, 600, 1),
(181, 601, 1),
(181, 602, 1),
(181, 2585, 1),
(181, 585, 2),
(181, 584, 3),
(181, 717, 3),
(181, 4517, 6),
(181, 4539, 6),
(182, 586, 1),
(182, 587, 1),
(182, 588, 1),
(182, 589, 1),
(182, 590, 1),
(182, 591, 1),
(182, 592, 1),
(182, 594, 1),
(182, 595, 1),
(182, 596, 1),
(182, 597, 1),
(182, 598, 1),
(182, 599, 1),
(182, 600, 1),
(182, 601, 1),
(182, 602, 1),
(182, 2585, 1),
(182, 585, 2),
(182, 584, 3),
(182, 648, 3),
(182, 4517, 6),
(182, 4561, 6),
(183, 586, 1),
(183, 587, 1),
(183, 588, 1),
(183, 589, 1),
(183, 590, 1),
(183, 591, 1),
(183, 592, 1),
(183, 594, 1),
(183, 595, 1),
(183, 596, 1),
(183, 597, 1),
(183, 598, 1),
(183, 599, 1),
(183, 600, 1),
(183, 601, 1),
(183, 602, 1),
(183, 1528, 1),
(183, 585, 2),
(183, 584, 3),
(183, 717, 3),
(183, 3621, 6),
(183, 4583, 6),
(184, 587, 1),
(184, 588, 1),
(184, 589, 1),
(184, 590, 1),
(184, 591, 1),
(184, 592, 1),
(184, 594, 1),
(184, 595, 1),
(184, 596, 1),
(184, 597, 1),
(184, 598, 1),
(184, 599, 1),
(184, 600, 1),
(184, 601, 1),
(184, 602, 1),
(184, 830, 1),
(184, 1336, 1),
(184, 585, 2),
(184, 584, 3),
(184, 648, 3),
(184, 4605, 6),
(184, 4606, 6),
(185, 586, 1),
(185, 587, 1),
(185, 588, 1),
(185, 589, 1),
(185, 590, 1),
(185, 591, 1),
(185, 592, 1),
(185, 594, 1),
(185, 595, 1),
(185, 596, 1),
(185, 597, 1),
(185, 598, 1),
(185, 599, 1),
(185, 600, 1),
(185, 601, 1),
(185, 602, 1),
(185, 1969, 1),
(185, 585, 2),
(185, 584, 3),
(185, 717, 3),
(185, 1550, 6),
(185, 4605, 6),
(186, 586, 1),
(186, 587, 1),
(186, 588, 1),
(186, 589, 1),
(186, 590, 1),
(186, 591, 1),
(186, 592, 1),
(186, 594, 1),
(186, 595, 1),
(186, 596, 1),
(186, 597, 1),
(186, 598, 1),
(186, 599, 1),
(186, 600, 1),
(186, 601, 1),
(186, 602, 1),
(186, 1703, 1),
(186, 585, 2),
(186, 584, 3),
(186, 603, 3),
(186, 1550, 6),
(186, 4605, 6),
(187, 586, 1),
(187, 587, 1),
(187, 588, 1),
(187, 589, 1),
(187, 590, 1),
(187, 591, 1),
(187, 592, 1),
(187, 594, 1),
(187, 595, 1),
(187, 596, 1),
(187, 597, 1),
(187, 598, 1),
(187, 599, 1),
(187, 600, 1),
(187, 601, 1),
(187, 602, 1),
(187, 2564, 1),
(187, 585, 2),
(187, 584, 3),
(187, 717, 3),
(187, 2497, 6),
(187, 4605, 6),
(188, 587, 1),
(188, 588, 1),
(188, 589, 1),
(188, 590, 1),
(188, 591, 1),
(188, 592, 1),
(188, 594, 1),
(188, 595, 1),
(188, 596, 1),
(188, 597, 1),
(188, 598, 1),
(188, 599, 1),
(188, 600, 1),
(188, 601, 1),
(188, 602, 1),
(188, 830, 1),
(188, 2916, 1),
(188, 585, 2),
(188, 584, 3),
(188, 717, 3),
(188, 4605, 6),
(188, 4693, 6),
(189, 586, 1),
(189, 587, 1),
(189, 588, 1),
(189, 589, 1),
(189, 590, 1),
(189, 591, 1),
(189, 592, 1),
(189, 594, 1),
(189, 595, 1),
(189, 596, 1),
(189, 597, 1),
(189, 598, 1),
(189, 599, 1),
(189, 600, 1),
(189, 601, 1),
(189, 602, 1),
(189, 943, 1),
(189, 585, 2),
(189, 584, 3),
(189, 717, 3),
(189, 2497, 6),
(189, 4605, 6),
(190, 586, 1),
(190, 587, 1),
(190, 588, 1),
(190, 589, 1),
(190, 590, 1),
(190, 591, 1),
(190, 592, 1),
(190, 593, 1),
(190, 594, 1),
(190, 595, 1),
(190, 596, 1),
(190, 597, 1),
(190, 598, 1),
(190, 599, 1),
(190, 600, 1),
(190, 601, 1),
(190, 602, 1),
(190, 585, 2),
(190, 584, 3),
(190, 648, 3),
(190, 1550, 6),
(190, 4605, 6),
(191, 586, 1),
(191, 587, 1),
(191, 588, 1),
(191, 589, 1),
(191, 590, 1),
(191, 591, 1),
(191, 592, 1),
(191, 594, 1),
(191, 595, 1),
(191, 596, 1),
(191, 597, 1),
(191, 598, 1),
(191, 599, 1),
(191, 600, 1),
(191, 601, 1),
(191, 602, 1),
(191, 2939, 1),
(191, 585, 2),
(191, 584, 3),
(191, 717, 3),
(191, 4605, 6),
(192, 586, 1),
(192, 587, 1),
(192, 588, 1),
(192, 589, 1),
(192, 590, 1),
(192, 591, 1),
(192, 592, 1),
(192, 594, 1),
(192, 595, 1),
(192, 596, 1),
(192, 597, 1),
(192, 598, 1),
(192, 599, 1),
(192, 600, 1),
(192, 601, 1),
(192, 602, 1),
(192, 739, 1),
(192, 585, 2),
(192, 584, 3),
(192, 603, 3),
(192, 4605, 6),
(192, 4780, 6),
(193, 587, 1),
(193, 588, 1),
(193, 589, 1),
(193, 590, 1),
(193, 591, 1),
(193, 592, 1),
(193, 593, 1),
(193, 594, 1),
(193, 595, 1),
(193, 596, 1),
(193, 597, 1),
(193, 598, 1),
(193, 599, 1),
(193, 600, 1),
(193, 601, 1),
(193, 602, 1),
(193, 830, 1),
(193, 585, 2),
(193, 584, 3),
(193, 648, 3),
(193, 4605, 6),
(193, 4802, 6),
(194, 586, 1),
(194, 587, 1),
(194, 588, 1),
(194, 589, 1),
(194, 590, 1),
(194, 591, 1),
(194, 592, 1),
(194, 594, 1),
(194, 595, 1),
(194, 596, 1),
(194, 597, 1),
(194, 598, 1),
(194, 599, 1),
(194, 600, 1),
(194, 601, 1),
(194, 602, 1),
(194, 1593, 1),
(194, 585, 2),
(194, 584, 3),
(194, 603, 3),
(194, 4605, 6),
(194, 4824, 6),
(195, 586, 1),
(195, 587, 1),
(195, 588, 1),
(195, 589, 1),
(195, 590, 1),
(195, 591, 1),
(195, 592, 1),
(195, 594, 1),
(195, 595, 1),
(195, 596, 1),
(195, 597, 1),
(195, 598, 1),
(195, 599, 1),
(195, 600, 1),
(195, 601, 1),
(195, 602, 1),
(195, 4846, 1),
(195, 585, 2),
(195, 584, 3),
(195, 717, 3),
(195, 4605, 6),
(196, 586, 1),
(196, 587, 1),
(196, 588, 1),
(196, 589, 1),
(196, 590, 1),
(196, 591, 1),
(196, 592, 1),
(196, 594, 1),
(196, 595, 1),
(196, 596, 1),
(196, 597, 1),
(196, 598, 1),
(196, 599, 1),
(196, 600, 1),
(196, 601, 1),
(196, 602, 1),
(196, 4867, 1),
(196, 585, 2),
(196, 584, 3),
(196, 648, 3),
(196, 1550, 6),
(196, 4605, 6),
(197, 586, 1),
(197, 587, 1),
(197, 588, 1),
(197, 589, 1),
(197, 590, 1),
(197, 591, 1),
(197, 592, 1),
(197, 594, 1),
(197, 595, 1),
(197, 596, 1),
(197, 597, 1),
(197, 598, 1),
(197, 599, 1),
(197, 600, 1),
(197, 601, 1),
(197, 602, 1),
(197, 1703, 1),
(197, 585, 2),
(197, 584, 3),
(197, 603, 3),
(197, 4889, 6),
(197, 4890, 6),
(198, 586, 1),
(198, 587, 1),
(198, 588, 1),
(198, 589, 1),
(198, 590, 1),
(198, 591, 1),
(198, 592, 1),
(198, 594, 1),
(198, 595, 1),
(198, 596, 1),
(198, 598, 1),
(198, 599, 1),
(198, 600, 1),
(198, 601, 1),
(198, 602, 1),
(198, 944, 1),
(198, 1182, 1),
(198, 585, 2),
(198, 584, 3),
(198, 717, 3),
(198, 4889, 6),
(199, 586, 1),
(199, 587, 1),
(199, 588, 1),
(199, 589, 1),
(199, 590, 1),
(199, 591, 1),
(199, 592, 1),
(199, 594, 1),
(199, 595, 1),
(199, 596, 1),
(199, 597, 1),
(199, 598, 1),
(199, 599, 1),
(199, 600, 1),
(199, 601, 1),
(199, 602, 1),
(199, 1093, 1),
(199, 585, 2),
(199, 584, 3),
(199, 648, 3),
(199, 4889, 6),
(199, 4890, 6),
(200, 586, 1),
(200, 587, 1),
(200, 588, 1),
(200, 589, 1),
(200, 590, 1),
(200, 591, 1),
(200, 592, 1),
(200, 594, 1),
(200, 595, 1),
(200, 596, 1),
(200, 597, 1),
(200, 598, 1),
(200, 599, 1),
(200, 600, 1),
(200, 601, 1),
(200, 602, 1),
(200, 1313, 1),
(200, 585, 2),
(200, 584, 3),
(200, 603, 3),
(200, 4889, 6),
(200, 4954, 6),
(201, 586, 1),
(201, 587, 1),
(201, 588, 1),
(201, 589, 1),
(201, 590, 1),
(201, 591, 1),
(201, 592, 1),
(201, 594, 1),
(201, 595, 1),
(201, 596, 1),
(201, 597, 1),
(201, 598, 1),
(201, 599, 1),
(201, 600, 1),
(201, 601, 1),
(201, 602, 1),
(201, 1528, 1),
(201, 585, 2),
(201, 584, 3),
(201, 603, 3),
(201, 4976, 6),
(202, 586, 1),
(202, 587, 1),
(202, 588, 1),
(202, 589, 1),
(202, 590, 1),
(202, 591, 1),
(202, 592, 1),
(202, 594, 1),
(202, 595, 1),
(202, 596, 1),
(202, 598, 1),
(202, 599, 1),
(202, 600, 1),
(202, 601, 1),
(202, 602, 1),
(202, 944, 1),
(202, 1204, 1),
(202, 585, 2),
(202, 584, 3),
(202, 648, 3),
(202, 4997, 6),
(202, 4998, 6),
(203, 586, 1),
(203, 587, 1),
(203, 588, 1),
(203, 589, 1),
(203, 590, 1),
(203, 591, 1),
(203, 592, 1),
(203, 594, 1),
(203, 595, 1),
(203, 596, 1),
(203, 597, 1),
(203, 598, 1),
(203, 599, 1),
(203, 600, 1),
(203, 601, 1),
(203, 602, 1),
(203, 670, 1),
(203, 585, 2),
(203, 584, 3),
(203, 648, 3),
(203, 4997, 6),
(203, 5019, 6),
(204, 586, 1),
(204, 587, 1),
(204, 588, 1),
(204, 589, 1),
(204, 590, 1),
(204, 591, 1),
(204, 592, 1),
(204, 594, 1),
(204, 595, 1),
(204, 596, 1),
(204, 597, 1),
(204, 598, 1),
(204, 599, 1),
(204, 600, 1),
(204, 601, 1),
(204, 602, 1),
(204, 5042, 1),
(204, 585, 2),
(204, 584, 3),
(204, 648, 3),
(204, 4997, 6),
(204, 5041, 6),
(205, 586, 1),
(205, 587, 1),
(205, 588, 1),
(205, 589, 1),
(205, 590, 1),
(205, 591, 1),
(205, 592, 1),
(205, 594, 1),
(205, 595, 1),
(205, 596, 1),
(205, 597, 1),
(205, 598, 1),
(205, 599, 1),
(205, 600, 1),
(205, 601, 1),
(205, 602, 1),
(205, 1703, 1),
(205, 585, 2),
(205, 584, 3),
(205, 603, 3),
(205, 4997, 6),
(205, 5063, 6),
(206, 586, 1),
(206, 587, 1),
(206, 588, 1),
(206, 589, 1),
(206, 590, 1),
(206, 591, 1),
(206, 592, 1),
(206, 594, 1),
(206, 595, 1),
(206, 596, 1),
(206, 597, 1),
(206, 598, 1),
(206, 599, 1),
(206, 600, 1),
(206, 601, 1),
(206, 602, 1),
(206, 2916, 1),
(206, 585, 2),
(206, 584, 3),
(206, 717, 3),
(206, 4997, 6),
(206, 5063, 6),
(207, 586, 1),
(207, 587, 1),
(207, 588, 1),
(207, 589, 1),
(207, 590, 1),
(207, 591, 1),
(207, 592, 1),
(207, 594, 1),
(207, 595, 1),
(207, 596, 1),
(207, 598, 1),
(207, 599, 1),
(207, 600, 1),
(207, 601, 1),
(207, 602, 1),
(207, 944, 1),
(207, 1969, 1),
(207, 585, 2),
(207, 584, 3),
(207, 648, 3),
(207, 4997, 6),
(207, 5107, 6),
(208, 586, 1),
(208, 587, 1),
(208, 588, 1),
(208, 589, 1),
(208, 590, 1),
(208, 591, 1),
(208, 592, 1),
(208, 594, 1),
(208, 595, 1),
(208, 596, 1),
(208, 597, 1),
(208, 598, 1),
(208, 599, 1),
(208, 600, 1),
(208, 601, 1),
(208, 602, 1),
(208, 605, 1),
(208, 585, 2),
(208, 584, 3),
(208, 603, 3),
(208, 5129, 6),
(208, 5130, 6),
(209, 587, 1),
(209, 588, 1),
(209, 589, 1),
(209, 590, 1),
(209, 591, 1),
(209, 592, 1),
(209, 594, 1),
(209, 595, 1),
(209, 596, 1),
(209, 597, 1),
(209, 598, 1),
(209, 599, 1),
(209, 600, 1),
(209, 601, 1),
(209, 602, 1),
(209, 605, 1),
(209, 830, 1),
(209, 585, 2),
(209, 584, 3),
(209, 603, 3),
(209, 5129, 6),
(209, 5151, 6),
(210, 586, 1),
(210, 587, 1),
(210, 588, 1),
(210, 589, 1),
(210, 590, 1),
(210, 591, 1),
(210, 592, 1),
(210, 594, 1),
(210, 595, 1),
(210, 596, 1),
(210, 597, 1),
(210, 598, 1),
(210, 599, 1),
(210, 600, 1),
(210, 601, 1),
(210, 602, 1),
(210, 3006, 1),
(210, 585, 2),
(210, 584, 3),
(210, 648, 3),
(210, 5129, 6),
(210, 5173, 6),
(211, 586, 1),
(211, 587, 1),
(211, 588, 1),
(211, 589, 1),
(211, 590, 1),
(211, 591, 1),
(211, 592, 1),
(211, 594, 1),
(211, 595, 1),
(211, 596, 1),
(211, 597, 1),
(211, 598, 1),
(211, 599, 1),
(211, 600, 1),
(211, 601, 1),
(211, 602, 1),
(211, 964, 1),
(211, 585, 2),
(211, 584, 3),
(211, 717, 3),
(211, 5129, 6),
(211, 5195, 6),
(212, 586, 1),
(212, 587, 1),
(212, 588, 1),
(212, 589, 1),
(212, 590, 1),
(212, 591, 1),
(212, 592, 1),
(212, 594, 1),
(212, 595, 1),
(212, 596, 1),
(212, 597, 1),
(212, 598, 1),
(212, 599, 1),
(212, 600, 1),
(212, 601, 1),
(212, 602, 1),
(212, 5218, 1),
(212, 585, 2),
(212, 584, 3),
(212, 648, 3),
(212, 5129, 6),
(212, 5217, 6),
(213, 586, 1),
(213, 587, 1),
(213, 588, 1),
(213, 589, 1),
(213, 590, 1),
(213, 591, 1),
(213, 592, 1),
(213, 594, 1),
(213, 595, 1),
(213, 596, 1),
(213, 597, 1),
(213, 598, 1),
(213, 599, 1),
(213, 600, 1),
(213, 601, 1),
(213, 602, 1),
(213, 2916, 1),
(213, 585, 2),
(213, 584, 3),
(213, 648, 3),
(213, 5129, 6),
(213, 5239, 6),
(214, 587, 1),
(214, 588, 1),
(214, 589, 1),
(214, 590, 1),
(214, 591, 1),
(214, 592, 1),
(214, 594, 1),
(214, 595, 1),
(214, 596, 1),
(214, 597, 1),
(214, 598, 1),
(214, 599, 1),
(214, 600, 1),
(214, 601, 1),
(214, 602, 1),
(214, 830, 1),
(214, 986, 1),
(214, 585, 2),
(214, 584, 3),
(214, 603, 3),
(214, 5129, 6),
(214, 5261, 6),
(215, 586, 1),
(215, 587, 1),
(215, 588, 1),
(215, 589, 1),
(215, 590, 1),
(215, 591, 1),
(215, 592, 1),
(215, 594, 1),
(215, 595, 1),
(215, 596, 1),
(215, 597, 1),
(215, 598, 1),
(215, 599, 1),
(215, 600, 1),
(215, 601, 1),
(215, 602, 1),
(215, 715, 1),
(215, 585, 2),
(215, 584, 3),
(215, 717, 3),
(215, 5129, 6),
(215, 5283, 6),
(216, 586, 1),
(216, 587, 1),
(216, 588, 1),
(216, 589, 1),
(216, 590, 1),
(216, 591, 1),
(216, 592, 1),
(216, 594, 1),
(216, 595, 1),
(216, 596, 1),
(216, 597, 1),
(216, 598, 1),
(216, 599, 1),
(216, 600, 1),
(216, 601, 1),
(216, 602, 1),
(216, 739, 1),
(216, 585, 2),
(216, 584, 3),
(216, 603, 3),
(216, 5129, 6),
(216, 5305, 6),
(217, 586, 1),
(217, 587, 1),
(217, 588, 1),
(217, 589, 1),
(217, 590, 1),
(217, 591, 1),
(217, 592, 1),
(217, 594, 1),
(217, 595, 1),
(217, 596, 1),
(217, 597, 1),
(217, 598, 1),
(217, 599, 1),
(217, 600, 1),
(217, 601, 1),
(217, 602, 1),
(217, 1006, 1),
(217, 585, 2),
(217, 584, 3),
(217, 603, 3),
(217, 5129, 6),
(217, 5327, 6),
(218, 586, 1),
(218, 587, 1),
(218, 588, 1),
(218, 589, 1),
(218, 590, 1),
(218, 591, 1),
(218, 592, 1),
(218, 594, 1),
(218, 595, 1),
(218, 596, 1),
(218, 597, 1),
(218, 598, 1),
(218, 599, 1),
(218, 600, 1),
(218, 601, 1),
(218, 602, 1),
(218, 920, 1),
(218, 585, 2),
(218, 584, 3),
(218, 717, 3),
(218, 5129, 6),
(218, 5349, 6),
(219, 586, 1),
(219, 587, 1),
(219, 588, 1),
(219, 589, 1),
(219, 590, 1),
(219, 591, 1),
(219, 592, 1),
(219, 594, 1),
(219, 595, 1),
(219, 596, 1),
(219, 597, 1),
(219, 598, 1),
(219, 599, 1),
(219, 600, 1),
(219, 601, 1),
(219, 602, 1),
(219, 1291, 1),
(219, 585, 2),
(219, 584, 3),
(219, 648, 3),
(219, 5129, 6),
(219, 5371, 6),
(220, 587, 1),
(220, 588, 1),
(220, 589, 1),
(220, 590, 1),
(220, 591, 1),
(220, 592, 1),
(220, 594, 1),
(220, 595, 1),
(220, 596, 1),
(220, 597, 1),
(220, 598, 1),
(220, 599, 1),
(220, 600, 1),
(220, 601, 1),
(220, 602, 1),
(220, 830, 1),
(220, 920, 1),
(220, 585, 2),
(220, 584, 3),
(220, 717, 3),
(220, 1180, 6),
(220, 5393, 6),
(221, 586, 1),
(221, 587, 1),
(221, 588, 1),
(221, 589, 1),
(221, 590, 1),
(221, 591, 1),
(221, 592, 1),
(221, 594, 1),
(221, 595, 1),
(221, 596, 1),
(221, 597, 1),
(221, 598, 1),
(221, 599, 1),
(221, 600, 1),
(221, 601, 1),
(221, 602, 1),
(221, 785, 1),
(221, 585, 2),
(221, 584, 3),
(221, 717, 3),
(221, 1180, 6),
(221, 5415, 6),
(222, 586, 1),
(222, 587, 1),
(222, 588, 1),
(222, 589, 1),
(222, 590, 1),
(222, 591, 1),
(222, 592, 1),
(222, 594, 1),
(222, 595, 1),
(222, 596, 1),
(222, 597, 1),
(222, 598, 1),
(222, 599, 1),
(222, 600, 1),
(222, 601, 1),
(222, 602, 1),
(222, 2762, 1),
(222, 585, 2),
(222, 584, 3),
(222, 648, 3),
(222, 1180, 6),
(222, 5437, 6),
(223, 586, 1),
(223, 587, 1),
(223, 588, 1),
(223, 589, 1),
(223, 590, 1),
(223, 591, 1),
(223, 592, 1),
(223, 594, 1),
(223, 595, 1),
(223, 596, 1),
(223, 597, 1),
(223, 598, 1),
(223, 599, 1),
(223, 600, 1),
(223, 601, 1),
(223, 602, 1),
(223, 1969, 1),
(223, 585, 2),
(223, 584, 3),
(223, 603, 3),
(223, 1180, 6),
(223, 5459, 6),
(224, 586, 1),
(224, 587, 1),
(224, 588, 1),
(224, 589, 1),
(224, 590, 1),
(224, 591, 1),
(224, 592, 1),
(224, 594, 1),
(224, 595, 1),
(224, 596, 1),
(224, 597, 1),
(224, 598, 1),
(224, 599, 1),
(224, 600, 1),
(224, 601, 1),
(224, 602, 1),
(224, 1291, 1),
(224, 585, 2),
(224, 584, 3),
(224, 603, 3),
(224, 5481, 6),
(224, 5482, 6),
(224, 5483, 6),
(225, 586, 1),
(225, 587, 1),
(225, 588, 1),
(225, 589, 1),
(225, 590, 1),
(225, 591, 1),
(225, 592, 1),
(225, 594, 1),
(225, 595, 1),
(225, 596, 1),
(225, 597, 1),
(225, 598, 1),
(225, 599, 1),
(225, 600, 1),
(225, 601, 1),
(225, 602, 1),
(225, 716, 1),
(225, 585, 2),
(225, 584, 3),
(225, 648, 3),
(225, 5504, 6),
(225, 5505, 6),
(226, 586, 1),
(226, 587, 1),
(226, 588, 1),
(226, 589, 1),
(226, 590, 1),
(226, 591, 1),
(226, 592, 1),
(226, 594, 1),
(226, 595, 1),
(226, 596, 1),
(226, 597, 1),
(226, 598, 1),
(226, 599, 1),
(226, 600, 1),
(226, 601, 1),
(226, 602, 1),
(226, 5527, 1),
(226, 585, 2),
(226, 584, 3),
(226, 717, 3),
(226, 2496, 6),
(226, 5526, 6),
(227, 586, 1),
(227, 587, 1),
(227, 588, 1),
(227, 589, 1),
(227, 590, 1),
(227, 591, 1),
(227, 592, 1),
(227, 594, 1),
(227, 595, 1),
(227, 596, 1),
(227, 597, 1),
(227, 598, 1),
(227, 599, 1),
(227, 600, 1),
(227, 601, 1),
(227, 602, 1),
(227, 1881, 1),
(227, 585, 2),
(227, 584, 3),
(227, 648, 3),
(227, 2473, 6),
(227, 5526, 6),
(227, 5548, 6),
(228, 586, 1),
(228, 587, 1),
(228, 588, 1),
(228, 589, 1),
(228, 590, 1),
(228, 591, 1),
(228, 592, 1),
(228, 594, 1),
(228, 595, 1),
(228, 596, 1),
(228, 597, 1),
(228, 598, 1),
(228, 599, 1),
(228, 600, 1),
(228, 601, 1),
(228, 602, 1),
(228, 2013, 1),
(228, 585, 2),
(228, 584, 3),
(228, 648, 3),
(228, 5526, 6),
(228, 5571, 6),
(229, 586, 1),
(229, 587, 1),
(229, 588, 1),
(229, 589, 1),
(229, 590, 1),
(229, 591, 1),
(229, 592, 1),
(229, 594, 1),
(229, 595, 1),
(229, 596, 1),
(229, 597, 1),
(229, 598, 1),
(229, 599, 1),
(229, 600, 1),
(229, 601, 1),
(229, 602, 1),
(229, 5042, 1),
(229, 585, 2),
(229, 584, 3),
(229, 648, 3),
(229, 5593, 6),
(229, 5594, 6),
(230, 586, 1),
(230, 587, 1),
(230, 588, 1),
(230, 589, 1),
(230, 590, 1),
(230, 591, 1),
(230, 592, 1),
(230, 594, 1),
(230, 595, 1),
(230, 596, 1),
(230, 597, 1),
(230, 598, 1),
(230, 599, 1),
(230, 600, 1),
(230, 601, 1),
(230, 602, 1),
(230, 3888, 1),
(230, 585, 2),
(230, 584, 3),
(230, 648, 3),
(230, 5615, 6),
(230, 5616, 6),
(231, 586, 1),
(231, 587, 1),
(231, 588, 1),
(231, 589, 1),
(231, 590, 1),
(231, 591, 1),
(231, 592, 1),
(231, 594, 1),
(231, 595, 1),
(231, 596, 1),
(231, 597, 1),
(231, 598, 1),
(231, 599, 1),
(231, 600, 1),
(231, 601, 1),
(231, 602, 1),
(231, 1969, 1),
(231, 585, 2),
(231, 584, 3),
(231, 603, 3),
(231, 5637, 6),
(232, 586, 1),
(232, 587, 1),
(232, 588, 1),
(232, 589, 1),
(232, 590, 1),
(232, 591, 1),
(232, 592, 1),
(232, 594, 1),
(232, 595, 1),
(232, 596, 1),
(232, 597, 1),
(232, 598, 1),
(232, 599, 1),
(232, 600, 1),
(232, 601, 1),
(232, 602, 1),
(232, 877, 1),
(232, 585, 2),
(232, 584, 3),
(232, 648, 3),
(232, 5637, 6),
(233, 586, 1),
(233, 587, 1),
(233, 588, 1),
(233, 589, 1),
(233, 590, 1),
(233, 591, 1),
(233, 592, 1),
(233, 594, 1),
(233, 595, 1),
(233, 596, 1),
(233, 597, 1),
(233, 598, 1),
(233, 599, 1),
(233, 600, 1),
(233, 601, 1),
(233, 602, 1),
(233, 1969, 1),
(233, 585, 2),
(233, 584, 3),
(233, 648, 3),
(233, 5637, 6),
(233, 5679, 6),
(234, 587, 1),
(234, 588, 1),
(234, 589, 1),
(234, 590, 1),
(234, 591, 1),
(234, 592, 1),
(234, 594, 1),
(234, 595, 1),
(234, 596, 1),
(234, 597, 1),
(234, 598, 1),
(234, 599, 1),
(234, 600, 1),
(234, 601, 1),
(234, 602, 1),
(234, 830, 1),
(234, 3734, 1),
(234, 585, 2),
(234, 584, 3),
(234, 648, 3),
(234, 5701, 6),
(234, 5702, 6),
(235, 586, 1),
(235, 587, 1),
(235, 588, 1),
(235, 589, 1),
(235, 590, 1),
(235, 591, 1),
(235, 592, 1),
(235, 594, 1),
(235, 595, 1),
(235, 596, 1),
(235, 597, 1),
(235, 598, 1),
(235, 599, 1),
(235, 600, 1),
(235, 601, 1),
(235, 602, 1),
(235, 3911, 1),
(235, 585, 2),
(235, 584, 3),
(235, 603, 3),
(235, 2429, 6),
(235, 5723, 6),
(236, 586, 1),
(236, 587, 1),
(236, 588, 1),
(236, 589, 1),
(236, 590, 1),
(236, 591, 1),
(236, 592, 1),
(236, 594, 1),
(236, 595, 1),
(236, 596, 1),
(236, 597, 1),
(236, 598, 1),
(236, 599, 1),
(236, 600, 1),
(236, 601, 1),
(236, 602, 1),
(236, 964, 1),
(236, 585, 2),
(236, 584, 3),
(236, 648, 3),
(236, 2429, 6),
(236, 5745, 6),
(237, 586, 1),
(237, 587, 1),
(237, 588, 1),
(237, 589, 1),
(237, 590, 1),
(237, 591, 1),
(237, 592, 1),
(237, 594, 1),
(237, 595, 1),
(237, 596, 1),
(237, 597, 1),
(237, 598, 1),
(237, 599, 1),
(237, 600, 1),
(237, 601, 1),
(237, 602, 1),
(237, 2762, 1),
(237, 585, 2),
(237, 584, 3),
(237, 603, 3),
(237, 5767, 6),
(237, 5768, 6),
(238, 586, 1),
(238, 587, 1),
(238, 588, 1),
(238, 589, 1),
(238, 590, 1),
(238, 591, 1),
(238, 592, 1),
(238, 594, 1),
(238, 595, 1),
(238, 596, 1),
(238, 597, 1),
(238, 598, 1),
(238, 599, 1),
(238, 600, 1),
(238, 601, 1),
(238, 602, 1),
(238, 5042, 1),
(238, 585, 2),
(238, 584, 3),
(238, 603, 3),
(238, 5789, 6),
(238, 5790, 6),
(239, 586, 1),
(239, 587, 1),
(239, 588, 1),
(239, 589, 1),
(239, 590, 1),
(239, 591, 1),
(239, 592, 1),
(239, 594, 1),
(239, 595, 1),
(239, 596, 1),
(239, 597, 1),
(239, 598, 1),
(239, 599, 1),
(239, 600, 1),
(239, 601, 1),
(239, 602, 1),
(239, 1703, 1),
(239, 585, 2),
(239, 584, 3),
(239, 648, 3),
(239, 5811, 6),
(240, 586, 1),
(240, 587, 1),
(240, 588, 1),
(240, 589, 1),
(240, 590, 1),
(240, 591, 1),
(240, 592, 1),
(240, 594, 1),
(240, 595, 1),
(240, 596, 1),
(240, 597, 1),
(240, 598, 1),
(240, 599, 1),
(240, 600, 1),
(240, 601, 1),
(240, 602, 1),
(240, 5042, 1),
(240, 585, 2),
(240, 584, 3),
(240, 648, 3),
(240, 5811, 6),
(241, 586, 1),
(241, 587, 1),
(241, 588, 1),
(241, 589, 1),
(241, 590, 1),
(241, 591, 1),
(241, 592, 1),
(241, 594, 1),
(241, 595, 1),
(241, 596, 1),
(241, 597, 1),
(241, 598, 1),
(241, 599, 1),
(241, 600, 1),
(241, 601, 1),
(241, 602, 1),
(241, 5854, 1),
(241, 585, 2),
(241, 584, 3),
(241, 603, 3),
(241, 5811, 6),
(241, 5853, 6),
(242, 586, 1),
(242, 587, 1),
(242, 588, 1),
(242, 589, 1),
(242, 590, 1),
(242, 591, 1),
(242, 592, 1),
(242, 594, 1),
(242, 595, 1),
(242, 596, 1),
(242, 597, 1),
(242, 598, 1),
(242, 599, 1),
(242, 600, 1),
(242, 601, 1),
(242, 602, 1),
(242, 2784, 1),
(242, 585, 2),
(242, 584, 3),
(242, 648, 3),
(242, 5811, 6),
(242, 5875, 6),
(243, 586, 1),
(243, 587, 1),
(243, 588, 1),
(243, 589, 1),
(243, 590, 1),
(243, 591, 1),
(243, 592, 1),
(243, 594, 1),
(243, 595, 1),
(243, 596, 1),
(243, 597, 1),
(243, 598, 1),
(243, 599, 1),
(243, 600, 1),
(243, 601, 1),
(243, 602, 1),
(243, 920, 1),
(243, 585, 2),
(243, 584, 3),
(243, 603, 3),
(243, 5811, 6),
(244, 586, 1),
(244, 587, 1),
(244, 588, 1),
(244, 589, 1),
(244, 590, 1),
(244, 591, 1),
(244, 592, 1),
(244, 594, 1),
(244, 595, 1),
(244, 596, 1),
(244, 597, 1),
(244, 598, 1),
(244, 599, 1),
(244, 600, 1),
(244, 601, 1),
(244, 602, 1),
(244, 3006, 1),
(244, 585, 2),
(244, 584, 3),
(244, 648, 3),
(244, 5811, 6),
(244, 5918, 6),
(245, 586, 1),
(245, 587, 1),
(245, 588, 1),
(245, 589, 1),
(245, 590, 1),
(245, 591, 1),
(245, 592, 1),
(245, 594, 1),
(245, 595, 1),
(245, 596, 1),
(245, 597, 1),
(245, 598, 1),
(245, 599, 1),
(245, 600, 1),
(245, 601, 1),
(245, 602, 1),
(245, 5940, 1),
(245, 585, 2),
(245, 584, 3),
(245, 648, 3),
(245, 5811, 6),
(246, 586, 1),
(246, 587, 1),
(246, 588, 1),
(246, 589, 1),
(246, 590, 1),
(246, 591, 1),
(246, 592, 1),
(246, 594, 1),
(246, 595, 1),
(246, 596, 1),
(246, 597, 1),
(246, 598, 1),
(246, 599, 1),
(246, 600, 1),
(246, 601, 1),
(246, 602, 1),
(246, 1528, 1),
(246, 585, 2),
(246, 584, 3),
(246, 717, 3),
(246, 5811, 6),
(247, 587, 1),
(247, 588, 1),
(247, 589, 1),
(247, 590, 1),
(247, 591, 1),
(247, 592, 1),
(247, 594, 1),
(247, 595, 1),
(247, 596, 1),
(247, 597, 1),
(247, 598, 1),
(247, 599, 1),
(247, 600, 1),
(247, 601, 1),
(247, 602, 1),
(247, 830, 1),
(247, 3006, 1),
(247, 585, 2),
(247, 584, 3),
(247, 648, 3),
(247, 5982, 6),
(247, 5983, 6),
(247, 5984, 6),
(248, 586, 1),
(248, 587, 1),
(248, 588, 1),
(248, 589, 1),
(248, 590, 1),
(248, 591, 1),
(248, 592, 1),
(248, 594, 1),
(248, 595, 1),
(248, 596, 1),
(248, 597, 1),
(248, 598, 1),
(248, 599, 1),
(248, 600, 1),
(248, 601, 1),
(248, 602, 1),
(248, 2916, 1),
(248, 585, 2),
(248, 584, 3),
(248, 717, 3),
(248, 1335, 6),
(248, 5982, 6),
(248, 5983, 6),
(249, 586, 1),
(249, 587, 1),
(249, 588, 1),
(249, 589, 1),
(249, 590, 1),
(249, 591, 1),
(249, 592, 1),
(249, 594, 1),
(249, 595, 1),
(249, 596, 1),
(249, 597, 1),
(249, 598, 1),
(249, 599, 1),
(249, 600, 1),
(249, 601, 1),
(249, 602, 1),
(249, 964, 1),
(249, 585, 2),
(249, 584, 3),
(249, 717, 3),
(249, 5982, 6),
(249, 5983, 6),
(249, 6028, 6),
(249, 6029, 6),
(250, 586, 1),
(250, 587, 1),
(250, 588, 1),
(250, 589, 1),
(250, 590, 1),
(250, 591, 1),
(250, 592, 1),
(250, 594, 1),
(250, 595, 1),
(250, 596, 1),
(250, 597, 1),
(250, 598, 1),
(250, 599, 1),
(250, 600, 1),
(250, 601, 1),
(250, 602, 1),
(250, 1593, 1),
(250, 585, 2),
(250, 584, 3),
(250, 648, 3),
(250, 5637, 6),
(250, 5982, 6),
(250, 5983, 6),
(250, 6052, 6),
(251, 586, 1),
(251, 587, 1),
(251, 588, 1),
(251, 589, 1),
(251, 590, 1),
(251, 591, 1),
(251, 592, 1),
(251, 594, 1),
(251, 595, 1),
(251, 596, 1),
(251, 597, 1),
(251, 598, 1),
(251, 599, 1),
(251, 600, 1),
(251, 601, 1),
(251, 602, 1),
(251, 2585, 1),
(251, 585, 2),
(251, 584, 3),
(251, 648, 3),
(251, 1335, 6),
(251, 5982, 6),
(251, 5983, 6),
(252, 586, 1),
(252, 587, 1),
(252, 588, 1),
(252, 589, 1),
(252, 590, 1),
(252, 591, 1),
(252, 592, 1),
(252, 594, 1),
(252, 595, 1),
(252, 596, 1),
(252, 597, 1),
(252, 598, 1),
(252, 599, 1),
(252, 600, 1),
(252, 601, 1),
(252, 602, 1),
(252, 2939, 1),
(252, 585, 2),
(252, 584, 3),
(252, 717, 3),
(252, 1335, 6),
(252, 5982, 6),
(252, 5983, 6),
(253, 587, 1),
(253, 588, 1),
(253, 589, 1),
(253, 590, 1),
(253, 591, 1),
(253, 592, 1),
(253, 594, 1),
(253, 595, 1),
(253, 596, 1),
(253, 597, 1),
(253, 598, 1),
(253, 599, 1),
(253, 600, 1),
(253, 601, 1),
(253, 602, 1),
(253, 830, 1),
(253, 2013, 1),
(253, 585, 2),
(253, 584, 3),
(253, 717, 3),
(253, 5982, 6),
(253, 5983, 6),
(253, 6122, 6),
(254, 586, 1),
(254, 587, 1),
(254, 588, 1),
(254, 589, 1),
(254, 590, 1),
(254, 591, 1),
(254, 592, 1),
(254, 594, 1),
(254, 595, 1),
(254, 596, 1),
(254, 597, 1),
(254, 598, 1),
(254, 599, 1),
(254, 600, 1),
(254, 601, 1),
(254, 602, 1),
(254, 943, 1),
(254, 585, 2),
(254, 584, 3),
(254, 717, 3),
(254, 5982, 6),
(254, 5983, 6),
(254, 6145, 6),
(255, 586, 1),
(255, 587, 1),
(255, 588, 1),
(255, 589, 1),
(255, 590, 1),
(255, 591, 1),
(255, 592, 1),
(255, 594, 1),
(255, 595, 1),
(255, 596, 1),
(255, 597, 1),
(255, 598, 1),
(255, 599, 1),
(255, 600, 1),
(255, 601, 1),
(255, 602, 1),
(255, 964, 1),
(255, 585, 2),
(255, 584, 3),
(255, 603, 3),
(255, 6168, 6),
(255, 6169, 6),
(255, 6170, 6),
(256, 586, 1),
(256, 587, 1),
(256, 588, 1),
(256, 589, 1),
(256, 590, 1),
(256, 591, 1),
(256, 592, 1),
(256, 594, 1),
(256, 595, 1),
(256, 596, 1),
(256, 597, 1),
(256, 598, 1),
(256, 599, 1),
(256, 600, 1),
(256, 601, 1),
(256, 602, 1),
(256, 1969, 1),
(256, 585, 2),
(256, 584, 3),
(256, 648, 3),
(256, 6168, 6),
(256, 6191, 6),
(257, 586, 1),
(257, 587, 1),
(257, 588, 1),
(257, 589, 1),
(257, 590, 1),
(257, 591, 1),
(257, 592, 1),
(257, 594, 1),
(257, 595, 1),
(257, 596, 1),
(257, 597, 1),
(257, 598, 1),
(257, 599, 1),
(257, 600, 1),
(257, 601, 1),
(257, 602, 1),
(257, 670, 1),
(257, 585, 2),
(257, 584, 3),
(257, 648, 3),
(257, 6168, 6),
(257, 6213, 6),
(258, 586, 1),
(258, 587, 1),
(258, 588, 1),
(258, 589, 1),
(258, 590, 1),
(258, 591, 1),
(258, 592, 1),
(258, 594, 1),
(258, 595, 1),
(258, 596, 1),
(258, 597, 1),
(258, 598, 1),
(258, 599, 1),
(258, 600, 1),
(258, 601, 1),
(258, 602, 1),
(258, 670, 1),
(258, 585, 2),
(258, 584, 3),
(258, 648, 3),
(258, 6168, 6),
(258, 6235, 6),
(259, 586, 1),
(259, 587, 1),
(259, 588, 1),
(259, 589, 1),
(259, 590, 1),
(259, 591, 1),
(259, 592, 1),
(259, 594, 1),
(259, 595, 1),
(259, 596, 1),
(259, 597, 1),
(259, 598, 1),
(259, 599, 1),
(259, 600, 1),
(259, 601, 1),
(259, 602, 1),
(259, 1225, 1),
(259, 585, 2),
(259, 584, 3),
(259, 648, 3),
(259, 6257, 6),
(259, 6258, 6),
(260, 586, 1),
(260, 587, 1),
(260, 588, 1),
(260, 589, 1),
(260, 590, 1),
(260, 591, 1),
(260, 592, 1),
(260, 594, 1),
(260, 595, 1),
(260, 596, 1),
(260, 597, 1),
(260, 598, 1),
(260, 599, 1),
(260, 600, 1),
(260, 601, 1),
(260, 602, 1),
(260, 2013, 1),
(260, 585, 2),
(260, 584, 3),
(260, 603, 3),
(260, 6257, 6),
(260, 6279, 6),
(261, 586, 1),
(261, 587, 1),
(261, 588, 1),
(261, 589, 1),
(261, 590, 1),
(261, 591, 1),
(261, 592, 1),
(261, 594, 1),
(261, 595, 1),
(261, 596, 1),
(261, 597, 1),
(261, 598, 1),
(261, 599, 1),
(261, 600, 1),
(261, 601, 1),
(261, 602, 1),
(261, 1313, 1),
(261, 585, 2),
(261, 584, 3),
(261, 717, 3),
(261, 6301, 6),
(262, 586, 1),
(262, 587, 1),
(262, 588, 1),
(262, 589, 1),
(262, 590, 1),
(262, 591, 1),
(262, 592, 1),
(262, 593, 1),
(262, 594, 1),
(262, 595, 1),
(262, 596, 1),
(262, 597, 1),
(262, 598, 1),
(262, 599, 1),
(262, 600, 1),
(262, 601, 1),
(262, 602, 1),
(262, 585, 2),
(262, 584, 3),
(262, 603, 3),
(262, 6322, 6),
(263, 586, 1),
(263, 587, 1),
(263, 588, 1),
(263, 589, 1),
(263, 590, 1),
(263, 591, 1),
(263, 592, 1),
(263, 594, 1),
(263, 595, 1),
(263, 596, 1),
(263, 597, 1),
(263, 598, 1),
(263, 599, 1),
(263, 600, 1),
(263, 601, 1),
(263, 602, 1),
(263, 6344, 1),
(263, 585, 2),
(263, 584, 3),
(263, 648, 3),
(263, 6343, 6),
(264, 586, 1),
(264, 587, 1),
(264, 588, 1),
(264, 589, 1),
(264, 590, 1),
(264, 591, 1),
(264, 592, 1),
(264, 594, 1),
(264, 595, 1),
(264, 596, 1),
(264, 597, 1),
(264, 598, 1),
(264, 599, 1),
(264, 600, 1),
(264, 601, 1),
(264, 602, 1),
(264, 3888, 1),
(264, 585, 2),
(264, 584, 3),
(264, 717, 3),
(264, 6364, 6),
(265, 586, 1),
(265, 587, 1),
(265, 588, 1),
(265, 589, 1),
(265, 590, 1),
(265, 591, 1),
(265, 592, 1),
(265, 594, 1),
(265, 595, 1),
(265, 596, 1),
(265, 597, 1),
(265, 598, 1),
(265, 599, 1),
(265, 600, 1),
(265, 601, 1),
(265, 602, 1),
(265, 3888, 1),
(265, 585, 2),
(265, 584, 3),
(265, 648, 3),
(265, 6385, 6),
(265, 6386, 6),
(266, 586, 1),
(266, 587, 1),
(266, 588, 1),
(266, 589, 1),
(266, 590, 1),
(266, 591, 1),
(266, 592, 1),
(266, 594, 1),
(266, 595, 1),
(266, 596, 1),
(266, 597, 1),
(266, 598, 1),
(266, 599, 1),
(266, 600, 1),
(266, 601, 1),
(266, 602, 1),
(266, 2320, 1),
(266, 585, 2),
(266, 584, 3),
(266, 717, 3),
(266, 6385, 6),
(266, 6407, 6),
(267, 586, 1),
(267, 587, 1),
(267, 588, 1),
(267, 589, 1),
(267, 590, 1),
(267, 591, 1),
(267, 592, 1),
(267, 594, 1),
(267, 595, 1),
(267, 596, 1),
(267, 597, 1),
(267, 598, 1),
(267, 599, 1),
(267, 600, 1),
(267, 601, 1),
(267, 602, 1),
(267, 1071, 1),
(267, 585, 2),
(267, 584, 3),
(267, 717, 3),
(267, 2938, 6),
(267, 6385, 6),
(268, 586, 1),
(268, 587, 1),
(268, 588, 1),
(268, 589, 1),
(268, 590, 1),
(268, 591, 1),
(268, 592, 1),
(268, 594, 1),
(268, 595, 1),
(268, 596, 1),
(268, 597, 1),
(268, 598, 1),
(268, 599, 1),
(268, 600, 1),
(268, 601, 1),
(268, 602, 1),
(268, 2762, 1),
(268, 585, 2),
(268, 584, 3),
(268, 717, 3),
(268, 6385, 6),
(268, 6451, 6),
(269, 586, 1),
(269, 587, 1),
(269, 588, 1),
(269, 589, 1),
(269, 590, 1),
(269, 591, 1),
(269, 592, 1),
(269, 594, 1),
(269, 595, 1),
(269, 596, 1),
(269, 597, 1),
(269, 598, 1),
(269, 599, 1),
(269, 600, 1),
(269, 601, 1),
(269, 602, 1),
(269, 2013, 1),
(269, 585, 2),
(269, 584, 3),
(269, 603, 3),
(269, 6385, 6),
(269, 6473, 6),
(270, 586, 1),
(270, 587, 1),
(270, 588, 1),
(270, 589, 1),
(270, 590, 1),
(270, 591, 1),
(270, 592, 1),
(270, 594, 1),
(270, 595, 1),
(270, 596, 1),
(270, 597, 1),
(270, 598, 1),
(270, 599, 1),
(270, 600, 1),
(270, 601, 1),
(270, 602, 1),
(270, 716, 1),
(270, 585, 2),
(270, 584, 3),
(270, 648, 3),
(270, 6385, 6),
(270, 6495, 6),
(271, 587, 1),
(271, 588, 1),
(271, 589, 1),
(271, 590, 1),
(271, 591, 1),
(271, 592, 1),
(271, 594, 1),
(271, 595, 1),
(271, 596, 1),
(271, 597, 1),
(271, 598, 1),
(271, 599, 1),
(271, 600, 1),
(271, 601, 1),
(271, 602, 1),
(271, 830, 1),
(271, 2784, 1),
(271, 585, 2),
(271, 584, 3),
(271, 603, 3),
(271, 6517, 6),
(271, 6518, 6),
(272, 586, 1),
(272, 587, 1),
(272, 588, 1),
(272, 589, 1),
(272, 590, 1),
(272, 591, 1),
(272, 592, 1),
(272, 594, 1),
(272, 595, 1),
(272, 596, 1),
(272, 597, 1),
(272, 598, 1),
(272, 599, 1),
(272, 600, 1),
(272, 601, 1),
(272, 602, 1),
(272, 670, 1),
(272, 585, 2),
(272, 584, 3),
(272, 603, 3),
(272, 6517, 6),
(272, 6539, 6),
(273, 586, 1),
(273, 587, 1),
(273, 588, 1),
(273, 589, 1),
(273, 590, 1),
(273, 591, 1),
(273, 592, 1),
(273, 594, 1),
(273, 595, 1),
(273, 596, 1),
(273, 597, 1),
(273, 598, 1),
(273, 599, 1),
(273, 600, 1),
(273, 601, 1),
(273, 602, 1),
(273, 3888, 1),
(273, 585, 2),
(273, 584, 3),
(273, 717, 3),
(273, 6517, 6),
(273, 6561, 6),
(274, 586, 1),
(274, 587, 1),
(274, 588, 1),
(274, 589, 1),
(274, 590, 1),
(274, 591, 1),
(274, 592, 1),
(274, 594, 1),
(274, 595, 1),
(274, 596, 1),
(274, 597, 1),
(274, 598, 1),
(274, 599, 1),
(274, 600, 1),
(274, 601, 1),
(274, 602, 1),
(274, 692, 1),
(274, 585, 2),
(274, 584, 3),
(274, 648, 3),
(274, 6583, 6),
(274, 6584, 6),
(275, 586, 1),
(275, 587, 1),
(275, 588, 1),
(275, 589, 1),
(275, 590, 1),
(275, 591, 1),
(275, 592, 1),
(275, 594, 1),
(275, 595, 1),
(275, 596, 1),
(275, 597, 1),
(275, 598, 1),
(275, 599, 1),
(275, 600, 1),
(275, 601, 1),
(275, 602, 1),
(275, 2784, 1),
(275, 585, 2),
(275, 584, 3),
(275, 603, 3),
(275, 6605, 6),
(276, 586, 1),
(276, 587, 1),
(276, 588, 1),
(276, 589, 1),
(276, 590, 1),
(276, 591, 1),
(276, 592, 1),
(276, 594, 1),
(276, 595, 1),
(276, 596, 1),
(276, 597, 1),
(276, 598, 1),
(276, 599, 1),
(276, 600, 1),
(276, 601, 1),
(276, 602, 1),
(276, 877, 1),
(276, 585, 2),
(276, 584, 3),
(276, 717, 3),
(276, 6605, 6),
(276, 6626, 6),
(277, 586, 1),
(277, 587, 1),
(277, 588, 1),
(277, 589, 1),
(277, 590, 1),
(277, 591, 1),
(277, 592, 1),
(277, 594, 1),
(277, 595, 1),
(277, 596, 1),
(277, 597, 1),
(277, 598, 1),
(277, 599, 1),
(277, 600, 1),
(277, 601, 1),
(277, 602, 1),
(277, 1291, 1),
(277, 585, 2),
(277, 584, 3),
(277, 717, 3),
(277, 6605, 6),
(277, 6648, 6),
(278, 586, 1),
(278, 587, 1),
(278, 588, 1),
(278, 589, 1),
(278, 590, 1),
(278, 591, 1),
(278, 592, 1),
(278, 594, 1),
(278, 595, 1),
(278, 596, 1),
(278, 597, 1),
(278, 598, 1),
(278, 599, 1),
(278, 600, 1),
(278, 601, 1),
(278, 602, 1),
(278, 1093, 1),
(278, 585, 2),
(278, 584, 3),
(278, 717, 3),
(278, 6605, 6),
(278, 6670, 6),
(279, 586, 1),
(279, 587, 1),
(279, 588, 1),
(279, 589, 1),
(279, 590, 1),
(279, 591, 1),
(279, 592, 1),
(279, 594, 1),
(279, 595, 1),
(279, 596, 1),
(279, 597, 1),
(279, 598, 1),
(279, 599, 1),
(279, 600, 1),
(279, 601, 1),
(279, 602, 1),
(279, 2585, 1),
(279, 585, 2),
(279, 584, 3),
(279, 603, 3),
(279, 6605, 6),
(279, 6692, 6),
(280, 586, 1),
(280, 587, 1),
(280, 588, 1),
(280, 589, 1),
(280, 590, 1),
(280, 591, 1),
(280, 592, 1),
(280, 594, 1),
(280, 595, 1),
(280, 596, 1),
(280, 597, 1),
(280, 598, 1),
(280, 599, 1),
(280, 600, 1),
(280, 601, 1),
(280, 602, 1),
(280, 2762, 1),
(280, 585, 2),
(280, 584, 3),
(280, 648, 3),
(280, 6605, 6),
(280, 6714, 6),
(281, 586, 1),
(281, 587, 1),
(281, 588, 1),
(281, 589, 1),
(281, 590, 1),
(281, 591, 1),
(281, 592, 1),
(281, 594, 1),
(281, 595, 1),
(281, 596, 1),
(281, 597, 1),
(281, 598, 1),
(281, 599, 1),
(281, 600, 1),
(281, 601, 1),
(281, 602, 1),
(281, 1006, 1),
(281, 585, 2),
(281, 584, 3),
(281, 603, 3),
(281, 6736, 6),
(281, 6737, 6),
(282, 586, 1),
(282, 587, 1),
(282, 588, 1),
(282, 589, 1),
(282, 590, 1),
(282, 591, 1),
(282, 592, 1),
(282, 594, 1),
(282, 595, 1),
(282, 596, 1),
(282, 597, 1),
(282, 598, 1),
(282, 599, 1),
(282, 600, 1),
(282, 601, 1),
(282, 602, 1),
(282, 605, 1),
(282, 585, 2),
(282, 584, 3),
(282, 603, 3),
(282, 6736, 6),
(282, 6758, 6),
(283, 586, 1),
(283, 587, 1),
(283, 588, 1),
(283, 589, 1),
(283, 590, 1),
(283, 591, 1),
(283, 592, 1),
(283, 594, 1),
(283, 595, 1),
(283, 596, 1),
(283, 597, 1),
(283, 598, 1),
(283, 599, 1),
(283, 600, 1),
(283, 601, 1),
(283, 602, 1),
(283, 605, 1),
(283, 585, 2),
(283, 584, 3),
(283, 717, 3),
(283, 6736, 6),
(284, 586, 1),
(284, 587, 1),
(284, 588, 1),
(284, 589, 1),
(284, 590, 1),
(284, 591, 1),
(284, 592, 1),
(284, 594, 1),
(284, 595, 1),
(284, 596, 1),
(284, 597, 1),
(284, 598, 1),
(284, 599, 1),
(284, 600, 1),
(284, 601, 1),
(284, 602, 1),
(284, 1093, 1),
(284, 585, 2),
(284, 584, 3),
(284, 717, 3),
(284, 6801, 6),
(284, 6802, 6),
(285, 586, 1),
(285, 587, 1),
(285, 588, 1),
(285, 589, 1),
(285, 590, 1),
(285, 591, 1),
(285, 592, 1),
(285, 594, 1),
(285, 595, 1),
(285, 596, 1),
(285, 597, 1),
(285, 598, 1),
(285, 599, 1),
(285, 600, 1),
(285, 601, 1),
(285, 602, 1),
(285, 1969, 1),
(285, 585, 2),
(285, 584, 3),
(285, 603, 3),
(285, 6801, 6),
(285, 6823, 6),
(286, 586, 1),
(286, 587, 1),
(286, 588, 1),
(286, 589, 1),
(286, 590, 1),
(286, 591, 1),
(286, 592, 1),
(286, 594, 1),
(286, 595, 1),
(286, 596, 1),
(286, 597, 1),
(286, 598, 1),
(286, 599, 1),
(286, 600, 1),
(286, 601, 1),
(286, 602, 1),
(286, 762, 1),
(286, 585, 2),
(286, 584, 3),
(286, 603, 3),
(286, 6801, 6),
(286, 6845, 6),
(287, 586, 1),
(287, 587, 1),
(287, 588, 1),
(287, 589, 1),
(287, 590, 1),
(287, 591, 1),
(287, 592, 1),
(287, 594, 1),
(287, 595, 1),
(287, 596, 1),
(287, 597, 1),
(287, 598, 1),
(287, 599, 1),
(287, 600, 1),
(287, 601, 1),
(287, 602, 1),
(287, 1881, 1),
(287, 585, 2),
(287, 584, 3),
(287, 717, 3),
(287, 6801, 6),
(287, 6867, 6),
(288, 586, 1),
(288, 587, 1),
(288, 588, 1),
(288, 589, 1),
(288, 590, 1),
(288, 591, 1),
(288, 592, 1),
(288, 594, 1),
(288, 595, 1),
(288, 596, 1),
(288, 597, 1),
(288, 598, 1),
(288, 599, 1),
(288, 600, 1),
(288, 601, 1),
(288, 602, 1),
(288, 6891, 1),
(288, 585, 2),
(288, 584, 3),
(288, 717, 3),
(288, 6889, 6),
(288, 6890, 6),
(289, 586, 1),
(289, 587, 1),
(289, 588, 1),
(289, 589, 1),
(289, 590, 1),
(289, 591, 1),
(289, 592, 1),
(289, 594, 1),
(289, 595, 1),
(289, 596, 1),
(289, 597, 1),
(289, 598, 1),
(289, 599, 1),
(289, 600, 1),
(289, 601, 1),
(289, 602, 1),
(289, 920, 1),
(289, 585, 2),
(289, 584, 3),
(289, 603, 3),
(289, 6911, 6),
(289, 6912, 6),
(290, 586, 1),
(290, 587, 1),
(290, 588, 1),
(290, 589, 1),
(290, 590, 1),
(290, 591, 1),
(290, 592, 1),
(290, 594, 1),
(290, 595, 1),
(290, 596, 1),
(290, 597, 1),
(290, 598, 1),
(290, 599, 1),
(290, 600, 1),
(290, 601, 1),
(290, 602, 1),
(290, 3911, 1),
(290, 585, 2),
(290, 584, 3),
(290, 648, 3),
(290, 6933, 6),
(290, 6934, 6),
(291, 586, 1),
(291, 587, 1),
(291, 588, 1),
(291, 589, 1),
(291, 590, 1),
(291, 591, 1),
(291, 592, 1),
(291, 594, 1),
(291, 595, 1),
(291, 596, 1),
(291, 597, 1),
(291, 598, 1),
(291, 599, 1),
(291, 600, 1),
(291, 601, 1),
(291, 602, 1),
(291, 986, 1),
(291, 585, 2),
(291, 584, 3),
(291, 648, 3),
(291, 6955, 6),
(291, 6956, 6),
(292, 586, 1),
(292, 587, 1),
(292, 588, 1),
(292, 589, 1),
(292, 590, 1),
(292, 591, 1),
(292, 592, 1),
(292, 594, 1),
(292, 595, 1),
(292, 596, 1),
(292, 597, 1),
(292, 598, 1),
(292, 599, 1),
(292, 600, 1),
(292, 601, 1),
(292, 602, 1),
(292, 1681, 1),
(292, 585, 2),
(292, 584, 3),
(292, 648, 3),
(292, 6955, 6),
(292, 6977, 6),
(293, 586, 1),
(293, 587, 1),
(293, 588, 1),
(293, 589, 1),
(293, 590, 1),
(293, 591, 1),
(293, 592, 1),
(293, 594, 1),
(293, 595, 1),
(293, 596, 1),
(293, 597, 1),
(293, 598, 1),
(293, 599, 1),
(293, 600, 1),
(293, 601, 1),
(293, 602, 1),
(293, 715, 1),
(293, 585, 2),
(293, 584, 3),
(293, 603, 3),
(293, 5940, 6),
(293, 6999, 6),
(294, 586, 1),
(294, 587, 1),
(294, 588, 1),
(294, 589, 1),
(294, 590, 1),
(294, 591, 1),
(294, 592, 1),
(294, 594, 1),
(294, 595, 1),
(294, 596, 1),
(294, 597, 1),
(294, 598, 1),
(294, 599, 1),
(294, 600, 1),
(294, 601, 1),
(294, 602, 1),
(294, 986, 1),
(294, 585, 2),
(294, 584, 3),
(294, 603, 3),
(294, 2034, 6),
(294, 6999, 6),
(295, 586, 1),
(295, 587, 1),
(295, 588, 1),
(295, 589, 1),
(295, 590, 1),
(295, 591, 1),
(295, 592, 1),
(295, 594, 1),
(295, 595, 1),
(295, 596, 1),
(295, 597, 1),
(295, 598, 1),
(295, 599, 1),
(295, 600, 1),
(295, 601, 1),
(295, 602, 1),
(295, 1969, 1),
(295, 585, 2),
(295, 584, 3),
(295, 603, 3),
(295, 6999, 6),
(295, 7043, 6),
(296, 586, 1),
(296, 587, 1),
(296, 588, 1),
(296, 589, 1),
(296, 590, 1),
(296, 591, 1),
(296, 592, 1),
(296, 594, 1),
(296, 595, 1),
(296, 596, 1),
(296, 597, 1),
(296, 598, 1),
(296, 599, 1),
(296, 600, 1),
(296, 601, 1),
(296, 602, 1),
(296, 1441, 1),
(296, 585, 2),
(296, 584, 3),
(296, 603, 3),
(296, 6999, 6),
(296, 7065, 6),
(297, 586, 1),
(297, 587, 1),
(297, 588, 1),
(297, 589, 1),
(297, 590, 1),
(297, 591, 1),
(297, 592, 1),
(297, 594, 1),
(297, 595, 1),
(297, 596, 1),
(297, 597, 1),
(297, 598, 1),
(297, 599, 1),
(297, 600, 1),
(297, 601, 1),
(297, 602, 1),
(297, 716, 1),
(297, 585, 2),
(297, 584, 3),
(297, 717, 3),
(297, 7087, 6),
(297, 7088, 6),
(298, 586, 1),
(298, 587, 1),
(298, 588, 1),
(298, 589, 1),
(298, 590, 1),
(298, 591, 1),
(298, 592, 1),
(298, 594, 1),
(298, 595, 1),
(298, 596, 1),
(298, 598, 1),
(298, 599, 1),
(298, 600, 1),
(298, 601, 1),
(298, 602, 1),
(298, 944, 1),
(298, 1571, 1),
(298, 585, 2),
(298, 584, 3),
(298, 648, 3),
(298, 7087, 6),
(298, 7109, 6),
(299, 586, 1),
(299, 587, 1),
(299, 588, 1),
(299, 589, 1),
(299, 590, 1),
(299, 591, 1),
(299, 592, 1),
(299, 594, 1),
(299, 595, 1),
(299, 596, 1),
(299, 597, 1),
(299, 598, 1),
(299, 599, 1),
(299, 600, 1),
(299, 601, 1),
(299, 602, 1),
(299, 3400, 1),
(299, 585, 2),
(299, 584, 3),
(299, 648, 3),
(299, 7131, 6),
(299, 7132, 6),
(300, 586, 1),
(300, 587, 1),
(300, 588, 1),
(300, 589, 1),
(300, 590, 1),
(300, 591, 1),
(300, 592, 1),
(300, 594, 1),
(300, 595, 1),
(300, 596, 1),
(300, 597, 1),
(300, 598, 1),
(300, 599, 1),
(300, 600, 1),
(300, 601, 1),
(300, 602, 1),
(300, 2784, 1),
(300, 585, 2),
(300, 584, 3),
(300, 717, 3),
(300, 7131, 6),
(300, 7153, 6),
(301, 586, 1),
(301, 587, 1),
(301, 588, 1),
(301, 589, 1),
(301, 590, 1),
(301, 591, 1),
(301, 592, 1),
(301, 594, 1),
(301, 595, 1),
(301, 596, 1),
(301, 597, 1),
(301, 598, 1),
(301, 599, 1),
(301, 600, 1),
(301, 601, 1),
(301, 602, 1),
(301, 4234, 1),
(301, 585, 2),
(301, 584, 3),
(301, 603, 3),
(301, 7131, 6),
(301, 7175, 6),
(302, 587, 1),
(302, 588, 1),
(302, 589, 1),
(302, 590, 1),
(302, 591, 1),
(302, 592, 1),
(302, 594, 1),
(302, 595, 1),
(302, 596, 1),
(302, 597, 1),
(302, 598, 1),
(302, 599, 1),
(302, 600, 1),
(302, 601, 1),
(302, 602, 1),
(302, 830, 1),
(302, 943, 1),
(302, 585, 2),
(302, 584, 3),
(302, 648, 3),
(302, 7131, 6),
(302, 7197, 6),
(303, 586, 1),
(303, 587, 1),
(303, 588, 1),
(303, 589, 1),
(303, 590, 1),
(303, 591, 1),
(303, 592, 1),
(303, 594, 1),
(303, 595, 1),
(303, 596, 1),
(303, 598, 1),
(303, 599, 1),
(303, 600, 1),
(303, 601, 1),
(303, 602, 1),
(303, 944, 1),
(303, 2916, 1),
(303, 585, 2),
(303, 584, 3),
(303, 648, 3),
(303, 6169, 6),
(303, 7131, 6),
(304, 586, 1),
(304, 587, 1),
(304, 588, 1),
(304, 589, 1),
(304, 590, 1),
(304, 591, 1),
(304, 592, 1),
(304, 594, 1),
(304, 595, 1),
(304, 596, 1),
(304, 597, 1),
(304, 598, 1),
(304, 599, 1),
(304, 600, 1),
(304, 601, 1),
(304, 602, 1),
(304, 3423, 1),
(304, 585, 2),
(304, 584, 3),
(304, 603, 3),
(304, 7241, 6),
(304, 7242, 6),
(305, 586, 1),
(305, 587, 1),
(305, 588, 1),
(305, 589, 1),
(305, 590, 1),
(305, 591, 1),
(305, 592, 1),
(305, 594, 1),
(305, 595, 1),
(305, 596, 1),
(305, 597, 1),
(305, 598, 1),
(305, 599, 1),
(305, 600, 1),
(305, 601, 1),
(305, 602, 1),
(305, 1182, 1),
(305, 585, 2),
(305, 584, 3),
(305, 603, 3),
(305, 7241, 6),
(305, 7263, 6),
(306, 586, 1),
(306, 587, 1),
(306, 588, 1),
(306, 589, 1),
(306, 590, 1),
(306, 591, 1),
(306, 592, 1),
(306, 594, 1),
(306, 595, 1),
(306, 596, 1),
(306, 597, 1),
(306, 598, 1),
(306, 599, 1),
(306, 600, 1),
(306, 601, 1),
(306, 602, 1),
(306, 3888, 1),
(306, 585, 2),
(306, 584, 3),
(306, 603, 3),
(306, 7241, 6),
(306, 7285, 6),
(307, 586, 1),
(307, 587, 1),
(307, 588, 1),
(307, 589, 1),
(307, 590, 1),
(307, 591, 1),
(307, 592, 1),
(307, 594, 1),
(307, 595, 1),
(307, 596, 1),
(307, 597, 1),
(307, 598, 1),
(307, 599, 1),
(307, 600, 1),
(307, 601, 1),
(307, 602, 1),
(307, 716, 1),
(307, 585, 2),
(307, 584, 3),
(307, 603, 3),
(307, 7241, 6),
(307, 7307, 6),
(308, 586, 1),
(308, 587, 1),
(308, 588, 1),
(308, 589, 1),
(308, 590, 1),
(308, 591, 1),
(308, 592, 1),
(308, 594, 1),
(308, 595, 1),
(308, 596, 1),
(308, 597, 1),
(308, 598, 1),
(308, 599, 1),
(308, 600, 1),
(308, 601, 1),
(308, 602, 1),
(308, 3888, 1),
(308, 585, 2),
(308, 584, 3),
(308, 717, 3),
(308, 7241, 6),
(308, 7329, 6),
(309, 586, 1),
(309, 587, 1),
(309, 588, 1),
(309, 589, 1),
(309, 590, 1),
(309, 591, 1),
(309, 592, 1),
(309, 594, 1),
(309, 595, 1),
(309, 596, 1),
(309, 597, 1),
(309, 598, 1),
(309, 599, 1),
(309, 600, 1),
(309, 601, 1),
(309, 602, 1),
(309, 647, 1),
(309, 585, 2),
(309, 584, 3),
(309, 603, 3),
(309, 7241, 6),
(309, 7351, 6),
(310, 586, 1),
(310, 587, 1),
(310, 588, 1),
(310, 589, 1),
(310, 590, 1),
(310, 591, 1),
(310, 592, 1),
(310, 594, 1),
(310, 595, 1),
(310, 596, 1),
(310, 597, 1),
(310, 598, 1),
(310, 599, 1),
(310, 600, 1),
(310, 601, 1),
(310, 602, 1),
(310, 1204, 1),
(310, 585, 2),
(310, 584, 3),
(310, 648, 3),
(310, 7241, 6),
(310, 7373, 6),
(311, 586, 1),
(311, 587, 1),
(311, 588, 1),
(311, 589, 1),
(311, 590, 1),
(311, 591, 1),
(311, 592, 1),
(311, 594, 1),
(311, 595, 1),
(311, 596, 1),
(311, 597, 1),
(311, 598, 1),
(311, 599, 1),
(311, 600, 1),
(311, 601, 1),
(311, 602, 1),
(311, 1093, 1),
(311, 585, 2),
(311, 584, 3),
(311, 603, 3),
(311, 7241, 6),
(311, 7395, 6),
(312, 586, 1),
(312, 587, 1),
(312, 588, 1),
(312, 589, 1),
(312, 590, 1),
(312, 591, 1),
(312, 592, 1),
(312, 594, 1),
(312, 595, 1),
(312, 596, 1),
(312, 597, 1),
(312, 598, 1),
(312, 599, 1),
(312, 600, 1),
(312, 601, 1),
(312, 602, 1),
(312, 1182, 1),
(312, 585, 2),
(312, 584, 3),
(312, 648, 3),
(312, 7241, 6),
(312, 7417, 6),
(313, 586, 1),
(313, 587, 1),
(313, 588, 1),
(313, 589, 1),
(313, 590, 1),
(313, 591, 1),
(313, 592, 1),
(313, 594, 1),
(313, 595, 1),
(313, 596, 1),
(313, 597, 1),
(313, 598, 1),
(313, 599, 1),
(313, 600, 1),
(313, 601, 1),
(313, 602, 1),
(313, 1093, 1),
(313, 585, 2),
(313, 584, 3),
(313, 717, 3),
(313, 7241, 6),
(313, 7439, 6),
(314, 586, 1),
(314, 587, 1),
(314, 588, 1),
(314, 589, 1),
(314, 590, 1),
(314, 591, 1),
(314, 592, 1),
(314, 594, 1),
(314, 595, 1),
(314, 596, 1),
(314, 597, 1),
(314, 598, 1),
(314, 599, 1),
(314, 600, 1),
(314, 601, 1),
(314, 602, 1),
(314, 715, 1),
(314, 585, 2),
(314, 584, 3),
(314, 717, 3),
(314, 7241, 6),
(314, 7461, 6),
(315, 586, 1),
(315, 587, 1),
(315, 588, 1),
(315, 589, 1),
(315, 590, 1),
(315, 591, 1),
(315, 592, 1),
(315, 594, 1),
(315, 595, 1),
(315, 596, 1),
(315, 597, 1),
(315, 598, 1),
(315, 599, 1),
(315, 600, 1),
(315, 601, 1),
(315, 602, 1),
(315, 1269, 1),
(315, 585, 2),
(315, 584, 3),
(315, 717, 3),
(315, 7241, 6),
(315, 7483, 6),
(316, 586, 1),
(316, 587, 1),
(316, 588, 1),
(316, 589, 1),
(316, 590, 1),
(316, 591, 1),
(316, 592, 1),
(316, 594, 1),
(316, 595, 1),
(316, 596, 1),
(316, 597, 1),
(316, 598, 1),
(316, 599, 1),
(316, 600, 1),
(316, 601, 1),
(316, 602, 1),
(316, 647, 1),
(316, 585, 2),
(316, 584, 3),
(316, 648, 3),
(316, 7241, 6),
(316, 7505, 6),
(317, 586, 1),
(317, 587, 1),
(317, 588, 1),
(317, 589, 1),
(317, 590, 1),
(317, 591, 1),
(317, 592, 1),
(317, 594, 1),
(317, 595, 1),
(317, 596, 1),
(317, 597, 1),
(317, 598, 1),
(317, 599, 1),
(317, 600, 1),
(317, 601, 1),
(317, 602, 1),
(317, 5042, 1),
(317, 585, 2),
(317, 584, 3),
(317, 648, 3),
(317, 7241, 6),
(317, 7527, 6),
(318, 586, 1),
(318, 587, 1),
(318, 588, 1),
(318, 589, 1),
(318, 590, 1),
(318, 591, 1),
(318, 592, 1),
(318, 594, 1),
(318, 595, 1),
(318, 596, 1),
(318, 597, 1),
(318, 598, 1),
(318, 599, 1),
(318, 600, 1),
(318, 601, 1),
(318, 602, 1),
(318, 2585, 1),
(318, 585, 2),
(318, 584, 3),
(318, 603, 3),
(318, 7241, 6),
(318, 7549, 6),
(319, 586, 1),
(319, 587, 1),
(319, 588, 1),
(319, 589, 1),
(319, 590, 1),
(319, 591, 1),
(319, 592, 1),
(319, 594, 1),
(319, 595, 1),
(319, 596, 1),
(319, 597, 1),
(319, 598, 1),
(319, 599, 1),
(319, 600, 1),
(319, 601, 1),
(319, 602, 1),
(319, 1881, 1),
(319, 585, 2),
(319, 584, 3),
(319, 648, 3),
(319, 7571, 6),
(319, 7572, 6),
(320, 586, 1),
(320, 587, 1),
(320, 588, 1),
(320, 589, 1),
(320, 590, 1),
(320, 591, 1),
(320, 592, 1),
(320, 594, 1),
(320, 595, 1),
(320, 596, 1),
(320, 597, 1),
(320, 598, 1),
(320, 599, 1),
(320, 600, 1),
(320, 601, 1),
(320, 602, 1),
(320, 647, 1),
(320, 585, 2),
(320, 584, 3),
(320, 648, 3),
(320, 7593, 6),
(321, 587, 1),
(321, 588, 1),
(321, 589, 1),
(321, 590, 1),
(321, 591, 1),
(321, 592, 1),
(321, 594, 1),
(321, 595, 1),
(321, 596, 1),
(321, 597, 1),
(321, 598, 1),
(321, 599, 1),
(321, 600, 1),
(321, 601, 1),
(321, 602, 1),
(321, 830, 1),
(321, 3400, 1),
(321, 585, 2),
(321, 584, 3),
(321, 648, 3),
(321, 7593, 6),
(321, 7614, 6),
(322, 587, 1),
(322, 588, 1),
(322, 589, 1),
(322, 590, 1),
(322, 591, 1),
(322, 592, 1),
(322, 594, 1),
(322, 595, 1),
(322, 596, 1),
(322, 597, 1),
(322, 598, 1),
(322, 599, 1),
(322, 600, 1),
(322, 601, 1),
(322, 602, 1),
(322, 830, 1),
(322, 920, 1),
(322, 585, 2),
(322, 584, 3),
(322, 717, 3),
(322, 7593, 6),
(322, 7636, 6),
(323, 586, 1),
(323, 587, 1),
(323, 588, 1),
(323, 589, 1),
(323, 590, 1),
(323, 591, 1),
(323, 592, 1),
(323, 594, 1),
(323, 595, 1),
(323, 596, 1),
(323, 597, 1),
(323, 598, 1),
(323, 599, 1),
(323, 600, 1),
(323, 601, 1),
(323, 602, 1),
(323, 1182, 1),
(323, 585, 2),
(323, 584, 3),
(323, 603, 3),
(323, 7593, 6),
(324, 586, 1),
(324, 587, 1),
(324, 588, 1),
(324, 589, 1),
(324, 590, 1),
(324, 591, 1),
(324, 592, 1),
(324, 594, 1),
(324, 595, 1),
(324, 596, 1),
(324, 597, 1),
(324, 598, 1),
(324, 599, 1),
(324, 600, 1),
(324, 601, 1),
(324, 602, 1),
(324, 1006, 1),
(324, 585, 2),
(324, 584, 3),
(324, 717, 3),
(324, 7593, 6),
(324, 7679, 6),
(325, 586, 1),
(325, 587, 1),
(325, 588, 1),
(325, 589, 1),
(325, 590, 1),
(325, 591, 1),
(325, 592, 1),
(325, 594, 1),
(325, 595, 1),
(325, 596, 1),
(325, 597, 1),
(325, 598, 1),
(325, 599, 1),
(325, 600, 1),
(325, 601, 1),
(325, 602, 1),
(325, 877, 1),
(325, 585, 2),
(325, 584, 3),
(325, 717, 3),
(325, 852, 6),
(325, 7593, 6),
(325, 7701, 6),
(326, 586, 1),
(326, 587, 1),
(326, 588, 1),
(326, 589, 1),
(326, 590, 1),
(326, 591, 1),
(326, 592, 1),
(326, 594, 1),
(326, 595, 1),
(326, 596, 1),
(326, 597, 1),
(326, 598, 1),
(326, 599, 1),
(326, 600, 1),
(326, 601, 1),
(326, 602, 1),
(326, 986, 1),
(326, 585, 2),
(326, 584, 3),
(326, 648, 3),
(326, 7593, 6),
(326, 7724, 6),
(327, 586, 1),
(327, 587, 1),
(327, 588, 1),
(327, 589, 1),
(327, 590, 1),
(327, 591, 1),
(327, 592, 1),
(327, 594, 1),
(327, 595, 1),
(327, 596, 1),
(327, 597, 1),
(327, 598, 1),
(327, 599, 1),
(327, 600, 1),
(327, 601, 1),
(327, 602, 1),
(327, 2585, 1),
(327, 585, 2),
(327, 584, 3),
(327, 603, 3),
(327, 7593, 6),
(327, 7746, 6),
(328, 586, 1),
(328, 587, 1),
(328, 588, 1),
(328, 589, 1),
(328, 590, 1),
(328, 591, 1),
(328, 592, 1),
(328, 594, 1),
(328, 595, 1),
(328, 596, 1),
(328, 597, 1),
(328, 598, 1),
(328, 599, 1),
(328, 600, 1),
(328, 601, 1),
(328, 602, 1),
(328, 1571, 1),
(328, 585, 2),
(328, 584, 3),
(328, 717, 3),
(328, 5482, 6),
(329, 586, 1),
(329, 587, 1),
(329, 588, 1),
(329, 589, 1),
(329, 590, 1),
(329, 591, 1),
(329, 592, 1),
(329, 594, 1),
(329, 595, 1),
(329, 596, 1),
(329, 597, 1),
(329, 598, 1),
(329, 599, 1),
(329, 600, 1),
(329, 601, 1),
(329, 602, 1),
(329, 7789, 1),
(329, 585, 2),
(329, 584, 3),
(329, 717, 3),
(329, 5482, 6),
(330, 586, 1),
(330, 587, 1),
(330, 588, 1),
(330, 589, 1),
(330, 590, 1),
(330, 591, 1),
(330, 592, 1),
(330, 594, 1),
(330, 595, 1),
(330, 596, 1),
(330, 597, 1),
(330, 598, 1),
(330, 599, 1),
(330, 600, 1),
(330, 601, 1),
(330, 602, 1),
(330, 7810, 1),
(330, 585, 2),
(330, 584, 3),
(330, 603, 3),
(330, 5482, 6),
(331, 586, 1),
(331, 587, 1),
(331, 588, 1),
(331, 589, 1),
(331, 590, 1),
(331, 591, 1),
(331, 592, 1),
(331, 594, 1),
(331, 595, 1),
(331, 596, 1),
(331, 597, 1),
(331, 598, 1),
(331, 599, 1),
(331, 600, 1),
(331, 601, 1),
(331, 602, 1),
(331, 1969, 1),
(331, 585, 2),
(331, 584, 3),
(331, 648, 3),
(331, 5482, 6),
(331, 5702, 6),
(332, 586, 1),
(332, 587, 1),
(332, 588, 1),
(332, 589, 1),
(332, 590, 1),
(332, 591, 1),
(332, 592, 1),
(332, 594, 1),
(332, 595, 1),
(332, 596, 1),
(332, 597, 1),
(332, 598, 1),
(332, 599, 1),
(332, 600, 1),
(332, 601, 1),
(332, 602, 1),
(332, 3645, 1),
(332, 585, 2),
(332, 584, 3),
(332, 603, 3),
(332, 7853, 6),
(332, 7854, 6),
(333, 586, 1),
(333, 587, 1),
(333, 588, 1),
(333, 589, 1),
(333, 590, 1),
(333, 591, 1),
(333, 592, 1),
(333, 594, 1),
(333, 595, 1),
(333, 596, 1),
(333, 597, 1),
(333, 598, 1),
(333, 599, 1),
(333, 600, 1),
(333, 601, 1),
(333, 602, 1),
(333, 986, 1),
(333, 585, 2),
(333, 584, 3),
(333, 648, 3),
(333, 7875, 6),
(334, 586, 1),
(334, 587, 1),
(334, 588, 1),
(334, 589, 1),
(334, 590, 1),
(334, 591, 1),
(334, 592, 1),
(334, 594, 1),
(334, 595, 1),
(334, 596, 1),
(334, 598, 1),
(334, 599, 1),
(334, 600, 1),
(334, 601, 1),
(334, 602, 1),
(334, 944, 1),
(334, 2564, 1),
(334, 585, 2),
(334, 584, 3),
(334, 717, 3),
(334, 1462, 6),
(334, 7896, 6),
(335, 586, 1),
(335, 587, 1),
(335, 588, 1),
(335, 589, 1),
(335, 590, 1),
(335, 591, 1),
(335, 592, 1),
(335, 594, 1),
(335, 595, 1),
(335, 596, 1),
(335, 597, 1),
(335, 598, 1),
(335, 599, 1),
(335, 600, 1),
(335, 601, 1),
(335, 602, 1),
(335, 739, 1),
(335, 585, 2),
(335, 584, 3),
(335, 648, 3),
(335, 7918, 6),
(335, 7919, 6),
(336, 587, 1),
(336, 588, 1),
(336, 589, 1),
(336, 590, 1),
(336, 591, 1),
(336, 592, 1),
(336, 594, 1),
(336, 595, 1),
(336, 596, 1),
(336, 597, 1),
(336, 598, 1),
(336, 599, 1),
(336, 600, 1),
(336, 601, 1),
(336, 602, 1),
(336, 830, 1),
(336, 5042, 1),
(336, 585, 2),
(336, 584, 3),
(336, 648, 3),
(336, 7940, 6),
(336, 7941, 6),
(337, 586, 1),
(337, 587, 1),
(337, 588, 1),
(337, 589, 1),
(337, 590, 1),
(337, 591, 1),
(337, 592, 1),
(337, 594, 1),
(337, 595, 1),
(337, 596, 1),
(337, 597, 1),
(337, 598, 1),
(337, 599, 1),
(337, 600, 1),
(337, 601, 1),
(337, 602, 1),
(337, 986, 1),
(337, 585, 2),
(337, 584, 3),
(337, 717, 3),
(337, 7940, 6),
(337, 7962, 6),
(338, 586, 1),
(338, 587, 1),
(338, 588, 1),
(338, 589, 1),
(338, 590, 1),
(338, 591, 1),
(338, 592, 1),
(338, 594, 1),
(338, 595, 1),
(338, 596, 1),
(338, 597, 1),
(338, 598, 1),
(338, 599, 1),
(338, 600, 1),
(338, 601, 1),
(338, 602, 1),
(338, 693, 1),
(338, 585, 2),
(338, 584, 3),
(338, 648, 3),
(338, 7984, 6),
(338, 7985, 6),
(339, 586, 1),
(339, 587, 1),
(339, 588, 1),
(339, 589, 1),
(339, 590, 1),
(339, 591, 1),
(339, 592, 1),
(339, 594, 1),
(339, 595, 1),
(339, 596, 1),
(339, 597, 1),
(339, 598, 1),
(339, 599, 1),
(339, 600, 1),
(339, 601, 1),
(339, 602, 1),
(339, 2916, 1),
(339, 585, 2),
(339, 584, 3),
(339, 648, 3),
(339, 8006, 6),
(339, 8007, 6),
(340, 586, 1),
(340, 587, 1),
(340, 588, 1),
(340, 589, 1),
(340, 590, 1),
(340, 591, 1),
(340, 592, 1),
(340, 594, 1),
(340, 595, 1),
(340, 596, 1),
(340, 597, 1),
(340, 598, 1),
(340, 599, 1),
(340, 600, 1),
(340, 601, 1),
(340, 602, 1),
(340, 8028, 1),
(340, 585, 2),
(340, 584, 3),
(340, 648, 3),
(340, 6934, 6),
(340, 8006, 6),
(341, 586, 1),
(341, 587, 1),
(341, 588, 1),
(341, 589, 1),
(341, 590, 1),
(341, 591, 1),
(341, 592, 1),
(341, 594, 1),
(341, 595, 1),
(341, 596, 1),
(341, 597, 1),
(341, 598, 1),
(341, 599, 1),
(341, 600, 1),
(341, 601, 1),
(341, 602, 1),
(341, 762, 1),
(341, 585, 2),
(341, 584, 3),
(341, 717, 3),
(342, 586, 1),
(342, 587, 1),
(342, 588, 1),
(342, 589, 1),
(342, 590, 1),
(342, 591, 1),
(342, 592, 1),
(342, 594, 1),
(342, 595, 1),
(342, 596, 1),
(342, 597, 1),
(342, 598, 1),
(342, 599, 1),
(342, 600, 1),
(342, 601, 1),
(342, 602, 1),
(342, 1093, 1),
(342, 585, 2),
(342, 584, 3),
(342, 648, 3),
(342, 3249, 6),
(342, 8070, 6),
(343, 586, 1),
(343, 587, 1),
(343, 588, 1),
(343, 589, 1),
(343, 590, 1),
(343, 591, 1),
(343, 592, 1),
(343, 594, 1),
(343, 595, 1),
(343, 596, 1),
(343, 598, 1),
(343, 599, 1),
(343, 600, 1),
(343, 601, 1),
(343, 602, 1),
(343, 943, 1),
(343, 944, 1),
(343, 585, 2),
(343, 584, 3),
(343, 717, 3),
(343, 8070, 6),
(343, 8092, 6),
(344, 586, 1),
(344, 587, 1),
(344, 588, 1),
(344, 589, 1),
(344, 590, 1),
(344, 591, 1),
(344, 592, 1),
(344, 594, 1),
(344, 595, 1),
(344, 596, 1),
(344, 597, 1),
(344, 598, 1),
(344, 599, 1),
(344, 600, 1),
(344, 601, 1),
(344, 602, 1),
(344, 1969, 1),
(344, 585, 2),
(344, 584, 3),
(344, 717, 3),
(344, 8070, 6),
(344, 8114, 6),
(345, 586, 1),
(345, 587, 1),
(345, 588, 1),
(345, 589, 1),
(345, 590, 1),
(345, 591, 1),
(345, 592, 1),
(345, 594, 1),
(345, 595, 1),
(345, 596, 1),
(345, 597, 1),
(345, 598, 1),
(345, 599, 1),
(345, 600, 1),
(345, 601, 1),
(345, 602, 1),
(345, 1881, 1),
(345, 585, 2),
(345, 584, 3),
(345, 603, 3),
(345, 8136, 6),
(345, 8137, 6),
(346, 586, 1),
(346, 587, 1),
(346, 588, 1),
(346, 589, 1),
(346, 590, 1),
(346, 591, 1),
(346, 592, 1),
(346, 594, 1),
(346, 595, 1),
(346, 596, 1),
(346, 597, 1),
(346, 598, 1),
(346, 599, 1),
(346, 600, 1),
(346, 601, 1),
(346, 602, 1),
(346, 627, 1),
(346, 585, 2),
(346, 584, 3),
(346, 603, 3),
(346, 8136, 6),
(346, 8158, 6),
(347, 586, 1),
(347, 587, 1),
(347, 588, 1),
(347, 589, 1),
(347, 590, 1),
(347, 591, 1),
(347, 592, 1),
(347, 594, 1),
(347, 595, 1),
(347, 596, 1),
(347, 597, 1),
(347, 598, 1),
(347, 599, 1),
(347, 600, 1),
(347, 601, 1),
(347, 602, 1),
(347, 1881, 1),
(347, 585, 2),
(347, 584, 3),
(347, 717, 3),
(347, 8136, 6),
(347, 8180, 6),
(348, 587, 1),
(348, 588, 1),
(348, 589, 1),
(348, 590, 1),
(348, 591, 1),
(348, 592, 1),
(348, 594, 1),
(348, 595, 1),
(348, 596, 1),
(348, 597, 1),
(348, 598, 1),
(348, 599, 1),
(348, 600, 1),
(348, 601, 1),
(348, 602, 1),
(348, 830, 1),
(348, 1703, 1),
(348, 585, 2),
(348, 584, 3),
(348, 717, 3),
(348, 3866, 6),
(348, 8202, 6),
(349, 586, 1),
(349, 587, 1),
(349, 588, 1),
(349, 589, 1),
(349, 590, 1),
(349, 591, 1),
(349, 592, 1),
(349, 594, 1),
(349, 595, 1),
(349, 596, 1),
(349, 597, 1),
(349, 598, 1),
(349, 599, 1),
(349, 600, 1),
(349, 601, 1),
(349, 602, 1),
(349, 3911, 1),
(349, 585, 2),
(349, 584, 3),
(349, 603, 3),
(349, 3866, 6),
(349, 8202, 6),
(350, 586, 1),
(350, 587, 1),
(350, 588, 1),
(350, 589, 1),
(350, 590, 1),
(350, 591, 1),
(350, 592, 1),
(350, 594, 1),
(350, 595, 1),
(350, 596, 1),
(350, 597, 1),
(350, 598, 1),
(350, 599, 1),
(350, 600, 1),
(350, 601, 1),
(350, 602, 1),
(350, 1528, 1),
(350, 585, 2),
(350, 584, 3),
(350, 603, 3),
(350, 3866, 6),
(350, 8202, 6),
(351, 586, 1),
(351, 587, 1),
(351, 588, 1),
(351, 589, 1),
(351, 590, 1),
(351, 591, 1),
(351, 592, 1),
(351, 594, 1),
(351, 595, 1),
(351, 596, 1),
(351, 597, 1),
(351, 598, 1),
(351, 599, 1),
(351, 600, 1),
(351, 601, 1),
(351, 602, 1),
(351, 1313, 1),
(351, 585, 2),
(351, 584, 3),
(351, 717, 3),
(351, 8268, 6),
(351, 8269, 6),
(352, 586, 1),
(352, 587, 1),
(352, 588, 1),
(352, 589, 1),
(352, 590, 1),
(352, 591, 1),
(352, 592, 1),
(352, 594, 1),
(352, 595, 1),
(352, 596, 1),
(352, 598, 1),
(352, 599, 1),
(352, 600, 1),
(352, 601, 1),
(352, 602, 1),
(352, 944, 1),
(352, 1336, 1),
(352, 585, 2),
(352, 584, 3),
(352, 648, 3),
(352, 8268, 6),
(352, 8290, 6),
(353, 586, 1),
(353, 587, 1),
(353, 588, 1),
(353, 589, 1),
(353, 590, 1),
(353, 591, 1),
(353, 592, 1),
(353, 594, 1),
(353, 595, 1),
(353, 596, 1),
(353, 597, 1),
(353, 598, 1),
(353, 599, 1),
(353, 600, 1),
(353, 601, 1),
(353, 602, 1),
(353, 2564, 1),
(353, 585, 2),
(353, 584, 3),
(353, 603, 3),
(353, 8268, 6),
(353, 8312, 6),
(354, 586, 1),
(354, 587, 1),
(354, 588, 1),
(354, 589, 1),
(354, 590, 1),
(354, 591, 1),
(354, 592, 1),
(354, 594, 1),
(354, 595, 1),
(354, 596, 1),
(354, 597, 1),
(354, 598, 1),
(354, 599, 1),
(354, 600, 1),
(354, 601, 1),
(354, 602, 1),
(354, 920, 1),
(354, 585, 2),
(354, 584, 3),
(354, 648, 3),
(354, 8268, 6),
(354, 8334, 6),
(355, 586, 1),
(355, 587, 1),
(355, 588, 1),
(355, 589, 1),
(355, 590, 1),
(355, 591, 1),
(355, 592, 1),
(355, 594, 1),
(355, 595, 1),
(355, 596, 1),
(355, 597, 1),
(355, 598, 1),
(355, 599, 1),
(355, 600, 1),
(355, 601, 1),
(355, 602, 1),
(355, 1681, 1),
(355, 585, 2),
(355, 584, 3),
(355, 717, 3),
(355, 8268, 6),
(355, 8356, 6),
(356, 586, 1),
(356, 587, 1),
(356, 588, 1),
(356, 589, 1),
(356, 590, 1),
(356, 591, 1),
(356, 592, 1),
(356, 594, 1),
(356, 595, 1),
(356, 596, 1),
(356, 597, 1),
(356, 598, 1),
(356, 599, 1),
(356, 600, 1),
(356, 601, 1),
(356, 602, 1),
(356, 986, 1),
(356, 585, 2),
(356, 584, 3),
(356, 603, 3),
(356, 8268, 6),
(356, 8378, 6),
(357, 586, 1),
(357, 587, 1),
(357, 588, 1),
(357, 589, 1),
(357, 590, 1),
(357, 591, 1),
(357, 592, 1),
(357, 594, 1),
(357, 595, 1),
(357, 596, 1),
(357, 597, 1),
(357, 598, 1),
(357, 599, 1),
(357, 600, 1),
(357, 601, 1),
(357, 602, 1),
(357, 739, 1),
(357, 585, 2),
(357, 584, 3),
(357, 648, 3),
(357, 8268, 6),
(357, 8400, 6),
(358, 586, 1),
(358, 587, 1),
(358, 588, 1),
(358, 589, 1),
(358, 590, 1),
(358, 591, 1),
(358, 592, 1),
(358, 594, 1),
(358, 595, 1),
(358, 596, 1),
(358, 597, 1),
(358, 598, 1),
(358, 599, 1),
(358, 600, 1),
(358, 601, 1),
(358, 602, 1),
(358, 716, 1),
(358, 585, 2),
(358, 584, 3),
(358, 603, 3),
(358, 8268, 6),
(358, 8422, 6),
(358, 8423, 6),
(359, 586, 1),
(359, 587, 1),
(359, 588, 1),
(359, 589, 1),
(359, 590, 1),
(359, 591, 1),
(359, 592, 1),
(359, 594, 1),
(359, 595, 1),
(359, 596, 1),
(359, 597, 1),
(359, 598, 1),
(359, 599, 1),
(359, 600, 1),
(359, 601, 1),
(359, 602, 1),
(359, 920, 1),
(359, 585, 2),
(359, 584, 3),
(359, 603, 3),
(359, 8268, 6),
(359, 8445, 6),
(360, 586, 1),
(360, 587, 1),
(360, 588, 1),
(360, 589, 1),
(360, 590, 1),
(360, 591, 1),
(360, 592, 1),
(360, 594, 1),
(360, 595, 1),
(360, 596, 1),
(360, 597, 1),
(360, 598, 1),
(360, 599, 1),
(360, 600, 1),
(360, 601, 1),
(360, 602, 1),
(360, 943, 1),
(360, 585, 2),
(360, 584, 3),
(360, 603, 3),
(360, 8268, 6),
(360, 8467, 6),
(361, 586, 1),
(361, 587, 1),
(361, 588, 1),
(361, 589, 1),
(361, 590, 1),
(361, 591, 1),
(361, 592, 1),
(361, 593, 1),
(361, 594, 1),
(361, 595, 1),
(361, 596, 1),
(361, 597, 1),
(361, 598, 1),
(361, 599, 1),
(361, 600, 1),
(361, 601, 1),
(361, 602, 1),
(361, 585, 2),
(361, 584, 3),
(361, 717, 3),
(361, 8268, 6),
(361, 8489, 6),
(362, 586, 1),
(362, 587, 1),
(362, 588, 1),
(362, 589, 1),
(362, 590, 1),
(362, 591, 1),
(362, 592, 1),
(362, 594, 1),
(362, 595, 1),
(362, 596, 1),
(362, 597, 1),
(362, 598, 1),
(362, 599, 1),
(362, 600, 1),
(362, 601, 1),
(362, 602, 1),
(362, 3006, 1),
(362, 585, 2),
(362, 584, 3),
(362, 648, 3),
(362, 5702, 6),
(362, 8268, 6),
(362, 8511, 6),
(363, 586, 1),
(363, 587, 1),
(363, 588, 1),
(363, 589, 1),
(363, 590, 1),
(363, 591, 1),
(363, 592, 1),
(363, 594, 1),
(363, 595, 1),
(363, 596, 1),
(363, 597, 1),
(363, 598, 1),
(363, 599, 1),
(363, 600, 1),
(363, 601, 1),
(363, 602, 1),
(363, 1571, 1),
(363, 585, 2),
(363, 584, 3),
(363, 717, 3),
(363, 8268, 6),
(363, 8534, 6),
(364, 586, 1),
(364, 587, 1),
(364, 588, 1),
(364, 589, 1),
(364, 590, 1),
(364, 591, 1),
(364, 592, 1),
(364, 594, 1),
(364, 595, 1),
(364, 596, 1),
(364, 597, 1),
(364, 598, 1),
(364, 599, 1),
(364, 600, 1),
(364, 601, 1),
(364, 602, 1),
(364, 2320, 1),
(364, 585, 2),
(364, 584, 3),
(364, 648, 3),
(364, 8556, 6),
(364, 8557, 6),
(365, 586, 1),
(365, 587, 1),
(365, 588, 1),
(365, 589, 1),
(365, 590, 1),
(365, 591, 1),
(365, 592, 1),
(365, 594, 1),
(365, 595, 1),
(365, 596, 1),
(365, 597, 1),
(365, 598, 1),
(365, 599, 1),
(365, 600, 1),
(365, 601, 1),
(365, 602, 1),
(365, 692, 1),
(365, 585, 2),
(365, 584, 3),
(365, 648, 3),
(365, 8578, 6),
(366, 586, 1),
(366, 587, 1),
(366, 588, 1),
(366, 589, 1),
(366, 590, 1),
(366, 591, 1),
(366, 592, 1),
(366, 594, 1),
(366, 595, 1),
(366, 596, 1),
(366, 597, 1),
(366, 598, 1),
(366, 599, 1),
(366, 600, 1),
(366, 601, 1),
(366, 602, 1),
(366, 1225, 1),
(366, 585, 2),
(366, 584, 3),
(366, 648, 3),
(366, 8578, 6),
(366, 8599, 6),
(367, 586, 1),
(367, 587, 1),
(367, 588, 1),
(367, 589, 1),
(367, 590, 1),
(367, 591, 1),
(367, 592, 1),
(367, 594, 1),
(367, 595, 1),
(367, 596, 1),
(367, 597, 1),
(367, 598, 1),
(367, 599, 1),
(367, 600, 1),
(367, 601, 1),
(367, 602, 1),
(367, 8623, 1),
(367, 585, 2),
(367, 584, 3),
(367, 717, 3),
(367, 8621, 6),
(367, 8622, 6),
(368, 586, 1),
(368, 587, 1),
(368, 588, 1),
(368, 589, 1),
(368, 590, 1),
(368, 591, 1),
(368, 592, 1),
(368, 594, 1),
(368, 595, 1),
(368, 596, 1),
(368, 597, 1),
(368, 598, 1),
(368, 599, 1),
(368, 600, 1),
(368, 601, 1),
(368, 602, 1),
(368, 762, 1),
(368, 585, 2),
(368, 584, 3),
(368, 717, 3),
(368, 8621, 6),
(368, 8643, 6),
(369, 586, 1),
(369, 587, 1),
(369, 588, 1),
(369, 589, 1),
(369, 590, 1),
(369, 591, 1),
(369, 592, 1),
(369, 594, 1),
(369, 595, 1),
(369, 596, 1),
(369, 597, 1),
(369, 598, 1),
(369, 599, 1),
(369, 600, 1),
(369, 601, 1),
(369, 602, 1),
(369, 1269, 1),
(369, 585, 2),
(369, 584, 3),
(369, 603, 3),
(369, 8621, 6),
(369, 8665, 6),
(370, 587, 1),
(370, 588, 1),
(370, 589, 1),
(370, 590, 1),
(370, 591, 1),
(370, 592, 1),
(370, 594, 1),
(370, 595, 1),
(370, 596, 1),
(370, 597, 1),
(370, 598, 1),
(370, 599, 1),
(370, 600, 1),
(370, 601, 1),
(370, 602, 1),
(370, 830, 1),
(370, 1269, 1),
(370, 585, 2),
(370, 584, 3),
(370, 717, 3),
(370, 8621, 6),
(370, 8687, 6),
(371, 586, 1),
(371, 587, 1),
(371, 588, 1),
(371, 589, 1),
(371, 590, 1),
(371, 591, 1),
(371, 592, 1),
(371, 594, 1),
(371, 595, 1),
(371, 596, 1),
(371, 597, 1),
(371, 598, 1),
(371, 599, 1),
(371, 600, 1),
(371, 601, 1),
(371, 602, 1),
(371, 762, 1),
(371, 585, 2),
(371, 584, 3),
(371, 603, 3),
(371, 8621, 6),
(371, 8709, 6),
(372, 586, 1),
(372, 587, 1),
(372, 588, 1),
(372, 589, 1),
(372, 590, 1),
(372, 591, 1),
(372, 592, 1),
(372, 594, 1),
(372, 595, 1),
(372, 596, 1),
(372, 597, 1),
(372, 598, 1),
(372, 599, 1),
(372, 600, 1),
(372, 601, 1),
(372, 602, 1),
(372, 3888, 1),
(372, 585, 2),
(372, 584, 3),
(372, 717, 3),
(372, 8731, 6),
(372, 8732, 6),
(373, 586, 1),
(373, 587, 1),
(373, 588, 1),
(373, 589, 1),
(373, 590, 1),
(373, 591, 1),
(373, 592, 1),
(373, 594, 1),
(373, 595, 1),
(373, 596, 1),
(373, 597, 1),
(373, 598, 1),
(373, 599, 1),
(373, 600, 1),
(373, 601, 1),
(373, 602, 1),
(373, 3645, 1),
(373, 585, 2),
(373, 584, 3),
(373, 603, 3),
(373, 8731, 6),
(373, 8753, 6),
(374, 586, 1),
(374, 587, 1),
(374, 588, 1),
(374, 589, 1),
(374, 590, 1),
(374, 591, 1),
(374, 592, 1),
(374, 594, 1),
(374, 595, 1),
(374, 596, 1),
(374, 597, 1),
(374, 598, 1),
(374, 599, 1),
(374, 600, 1),
(374, 601, 1),
(374, 602, 1),
(374, 3888, 1),
(374, 585, 2),
(374, 584, 3),
(374, 717, 3),
(374, 8731, 6),
(374, 8775, 6),
(375, 586, 1),
(375, 587, 1),
(375, 588, 1),
(375, 589, 1),
(375, 590, 1),
(375, 591, 1),
(375, 592, 1),
(375, 594, 1),
(375, 595, 1),
(375, 596, 1),
(375, 597, 1),
(375, 598, 1),
(375, 599, 1),
(375, 600, 1),
(375, 601, 1),
(375, 602, 1),
(375, 739, 1),
(375, 585, 2),
(375, 584, 3),
(375, 603, 3),
(375, 2564, 6),
(375, 8731, 6),
(376, 586, 1),
(376, 587, 1),
(376, 588, 1),
(376, 589, 1),
(376, 590, 1),
(376, 591, 1),
(376, 592, 1),
(376, 594, 1),
(376, 595, 1),
(376, 596, 1),
(376, 597, 1),
(376, 598, 1),
(376, 599, 1),
(376, 600, 1),
(376, 601, 1),
(376, 602, 1),
(376, 964, 1),
(376, 585, 2),
(376, 584, 3),
(376, 648, 3),
(376, 1291, 6),
(376, 8731, 6),
(377, 586, 1),
(377, 587, 1),
(377, 588, 1),
(377, 589, 1),
(377, 590, 1),
(377, 591, 1),
(377, 592, 1),
(377, 594, 1),
(377, 595, 1),
(377, 596, 1),
(377, 597, 1),
(377, 598, 1),
(377, 599, 1),
(377, 600, 1),
(377, 601, 1),
(377, 602, 1),
(377, 2564, 1),
(377, 585, 2),
(377, 584, 3),
(377, 603, 3),
(377, 1462, 6),
(377, 7810, 6),
(377, 8731, 6),
(378, 586, 1),
(378, 587, 1),
(378, 588, 1),
(378, 589, 1),
(378, 590, 1),
(378, 591, 1),
(378, 592, 1),
(378, 594, 1),
(378, 595, 1),
(378, 596, 1),
(378, 597, 1),
(378, 598, 1),
(378, 599, 1),
(378, 600, 1),
(378, 601, 1),
(378, 602, 1),
(378, 627, 1),
(378, 585, 2),
(378, 584, 3),
(378, 648, 3),
(378, 1462, 6),
(378, 8731, 6),
(378, 8864, 6),
(379, 586, 1),
(379, 587, 1),
(379, 588, 1),
(379, 589, 1),
(379, 590, 1),
(379, 591, 1),
(379, 592, 1),
(379, 594, 1),
(379, 595, 1),
(379, 596, 1),
(379, 597, 1),
(379, 598, 1),
(379, 599, 1),
(379, 600, 1),
(379, 601, 1),
(379, 602, 1),
(379, 877, 1),
(379, 585, 2),
(379, 584, 3),
(379, 603, 3),
(379, 1462, 6),
(379, 8731, 6),
(379, 8887, 6),
(380, 586, 1),
(380, 587, 1),
(380, 588, 1),
(380, 589, 1),
(380, 590, 1),
(380, 591, 1),
(380, 592, 1),
(380, 594, 1),
(380, 595, 1),
(380, 596, 1),
(380, 597, 1),
(380, 598, 1),
(380, 599, 1),
(380, 600, 1),
(380, 601, 1),
(380, 602, 1),
(380, 2013, 1),
(380, 585, 2),
(380, 584, 3),
(380, 648, 3),
(380, 8731, 6),
(380, 8910, 6),
(381, 586, 1),
(381, 587, 1),
(381, 588, 1),
(381, 589, 1),
(381, 590, 1),
(381, 591, 1),
(381, 592, 1),
(381, 594, 1),
(381, 595, 1),
(381, 596, 1),
(381, 597, 1),
(381, 598, 1),
(381, 599, 1),
(381, 600, 1),
(381, 601, 1),
(381, 602, 1),
(381, 1093, 1),
(381, 585, 2),
(381, 584, 3),
(381, 717, 3),
(381, 8932, 6),
(382, 586, 1),
(382, 587, 1),
(382, 588, 1),
(382, 589, 1),
(382, 590, 1),
(382, 591, 1),
(382, 592, 1),
(382, 594, 1),
(382, 595, 1),
(382, 596, 1),
(382, 597, 1),
(382, 598, 1),
(382, 599, 1),
(382, 600, 1),
(382, 601, 1),
(382, 602, 1),
(382, 1225, 1),
(382, 585, 2),
(382, 584, 3),
(382, 603, 3),
(382, 8953, 6),
(382, 8954, 6);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_search_word`
--

DROP TABLE IF EXISTS `ps_search_word`;
CREATE TABLE `ps_search_word` (
  `id_word` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT 1,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `word` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_search_word`
--

INSERT INTO `ps_search_word` (`id_word`, `id_shop`, `id_lang`, `word`) VALUES
(7941, 1, 1, '1000'),
(596, 1, 1, '100km'),
(1791, 1, 1, '101'),
(8028, 1, 1, '110'),
(7962, 1, 1, '1100'),
(4044, 1, 1, '112'),
(5218, 1, 1, '113'),
(6344, 1, 1, '114'),
(1441, 1, 1, '115'),
(5527, 1, 1, '118'),
(2784, 1, 1, '120'),
(593, 1, 1, '121'),
(1182, 1, 1, '122'),
(3423, 1, 1, '123'),
(920, 1, 1, '124'),
(877, 1, 1, '125'),
(670, 1, 1, '126'),
(3400, 1, 1, '127'),
(647, 1, 1, '128'),
(964, 1, 1, '129'),
(5679, 1, 1, '12c'),
(1336, 1, 1, '130'),
(716, 1, 1, '131'),
(2916, 1, 1, '132'),
(2762, 1, 1, '133'),
(2013, 1, 1, '134'),
(3911, 1, 1, '135'),
(1681, 1, 1, '136'),
(1204, 1, 1, '137'),
(1093, 1, 1, '138'),
(1269, 1, 1, '139'),
(2564, 1, 1, '140'),
(605, 1, 1, '141'),
(1313, 1, 1, '142'),
(1593, 1, 1, '143'),
(627, 1, 1, '144'),
(2939, 1, 1, '145'),
(3315, 1, 1, '146'),
(986, 1, 1, '147'),
(1969, 1, 1, '148'),
(3006, 1, 1, '149'),
(1571, 1, 1, '150'),
(1703, 1, 1, '151'),
(943, 1, 1, '152'),
(1881, 1, 1, '153'),
(2100, 1, 1, '154'),
(692, 1, 1, '155'),
(715, 1, 1, '156'),
(693, 1, 1, '157'),
(2585, 1, 1, '158'),
(3645, 1, 1, '159'),
(762, 1, 1, '160'),
(739, 1, 1, '161'),
(1006, 1, 1, '162'),
(1528, 1, 1, '163'),
(1291, 1, 1, '164'),
(3888, 1, 1, '165'),
(1225, 1, 1, '166'),
(2320, 1, 1, '167'),
(5042, 1, 1, '168'),
(1071, 1, 1, '169'),
(8623, 1, 1, '170'),
(1837, 1, 1, '171'),
(3250, 1, 1, '173'),
(6891, 1, 1, '178'),
(5854, 1, 1, '179'),
(7789, 1, 1, '180'),
(807, 1, 1, '181'),
(3228, 1, 1, '182'),
(4234, 1, 1, '183'),
(2122, 1, 1, '185'),
(3294, 1, 1, '188'),
(4846, 1, 1, '189'),
(4867, 1, 1, '192'),
(4211, 1, 1, '197'),
(7810, 1, 1, '200'),
(8269, 1, 1, '2000gt'),
(785, 1, 1, '201'),
(3734, 1, 1, '202'),
(5940, 1, 1, '204'),
(2034, 1, 1, '205'),
(7043, 1, 1, '206'),
(2937, 1, 1, '250'),
(2983, 1, 1, '275'),
(3071, 1, 1, '288'),
(8864, 1, 1, '300'),
(583, 1, 1, '3000me'),
(1312, 1, 1, '3200'),
(1247, 1, 1, '328'),
(3116, 1, 1, '348'),
(5130, 1, 1, '350gt'),
(7242, 1, 1, '356'),
(3249, 1, 1, '360'),
(5151, 1, 1, '400gt'),
(3138, 1, 1, '456'),
(7614, 1, 1, '4cv'),
(3422, 1, 1, '500'),
(1290, 1, 1, '503'),
(7065, 1, 1, '504'),
(1268, 1, 1, '507'),
(3204, 1, 1, '550'),
(3226, 1, 1, '575m'),
(5984, 1, 1, '600'),
(8887, 1, 1, '700'),
(5702, 1, 1, '800'),
(8910, 1, 1, '850'),
(7854, 1, 1, '900'),
(7263, 1, 1, '911'),
(7285, 1, 1, '914'),
(7307, 1, 1, '917'),
(7549, 1, 1, '918'),
(7329, 1, 1, '924'),
(7351, 1, 1, '928'),
(7373, 1, 1, '944'),
(7395, 1, 1, '956'),
(7417, 1, 1, '959'),
(7439, 1, 1, '962'),
(7461, 1, 1, '968'),
(876, 1, 1, 'a106'),
(897, 1, 1, 'a110'),
(1137, 1, 1, 'a111'),
(1158, 1, 1, 'a112'),
(919, 1, 1, 'a310'),
(4539, 1, 1, 'accent'),
(4277, 1, 1, 'accord'),
(1203, 1, 1, 'ado16'),
(1224, 1, 1, 'ado17'),
(3555, 1, 1, 'aerostar'),
(8092, 1, 1, 'alcyone'),
(625, 1, 1, 'alfa'),
(738, 1, 1, 'alfasud'),
(1049, 1, 1, 'allegro'),
(875, 1, 1, 'alpine'),
(8753, 1, 1, 'amazon'),
(4233, 1, 1, 'ambassador'),
(941, 1, 1, 'amc'),
(3823, 1, 1, 'america'),
(5768, 1, 1, 'and'),
(1859, 1, 1, 'apollo'),
(761, 1, 1, 'arna'),
(6802, 1, 1, 'ascona'),
(6823, 1, 1, 'astra'),
(7132, 1, 1, 'astre'),
(985, 1, 1, 'audi'),
(6737, 1, 1, 'aurora'),
(1048, 1, 1, 'austin'),
(1136, 1, 1, 'autobianchi'),
(830, 1, 1, 'automatyczna'),
(5173, 1, 1, 'aventador'),
(7153, 1, 1, 'aztek'),
(4298, 1, 1, 'beat'),
(8622, 1, 1, 'beetle'),
(5983, 1, 1, 'benz'),
(2783, 1, 1, 'bermuda'),
(588, 1, 1, 'biegow'),
(5723, 1, 1, 'biturbo'),
(1202, 1, 1, 'bmc'),
(1246, 1, 1, 'bmw'),
(7175, 1, 1, 'bonneville'),
(7483, 1, 1, 'boxster'),
(784, 1, 1, 'brera'),
(1549, 1, 1, 'bugatti'),
(1858, 1, 1, 'buick'),
(2012, 1, 1, 'byd'),
(2077, 1, 1, 'cadillac'),
(8290, 1, 1, 'camry'),
(8137, 1, 1, 'cappuccino'),
(2166, 1, 1, 'caprice'),
(5548, 1, 1, 'car'),
(6626, 1, 1, 'cargo'),
(6386, 1, 1, 'carisma'),
(7527, 1, 1, 'carrera'),
(2187, 1, 1, 'cavalier'),
(7505, 1, 1, 'cayenne'),
(3576, 1, 1, 'ce14'),
(8312, 1, 1, 'celica'),
(1880, 1, 1, 'centurion'),
(7985, 1, 1, 'champion'),
(2143, 1, 1, 'checker'),
(4890, 1, 1, 'cherokee'),
(2165, 1, 1, 'chevrolet'),
(2407, 1, 1, 'chrysler'),
(2078, 1, 1, 'cimarron'),
(2209, 1, 1, 'citation'),
(4320, 1, 1, 'civic'),
(1335, 1, 1, 'class'),
(7572, 1, 1, 'classic'),
(7679, 1, 1, 'clio'),
(6028, 1, 1, 'clk'),
(4255, 1, 1, 'commodore'),
(2496, 1, 1, 'continental'),
(2519, 1, 1, 'copen'),
(8334, 1, 1, 'corolla'),
(2695, 1, 1, 'coronet'),
(6845, 1, 1, 'corsa'),
(2804, 1, 1, 'corsair'),
(3599, 1, 1, 'cortina'),
(2231, 1, 1, 'corvair'),
(2253, 1, 1, 'corvette'),
(5195, 1, 1, 'countach'),
(2474, 1, 1, 'country'),
(3621, 1, 1, 'crown'),
(8423, 1, 1, 'cruiser'),
(2275, 1, 1, 'cruze'),
(8356, 1, 1, 'curren'),
(2673, 1, 1, 'custom'),
(6758, 1, 1, 'cutlass'),
(3644, 1, 1, 'd186'),
(2518, 1, 1, 'daihatsu'),
(7636, 1, 1, 'dauphine'),
(2563, 1, 1, 'deauville'),
(5437, 1, 1, 'dedra'),
(2540, 1, 1, 'delorean'),
(2672, 1, 1, 'desoto'),
(5217, 1, 1, 'diablo'),
(3005, 1, 1, 'dino'),
(2541, 1, 1, 'dmc'),
(2694, 1, 1, 'dodge'),
(8732, 1, 1, 'duett'),
(2760, 1, 1, 'eagle'),
(1814, 1, 1, 'eb110'),
(2782, 1, 1, 'edsel'),
(6890, 1, 1, 'eight'),
(4518, 1, 1, 'elantra'),
(1902, 1, 1, 'electra'),
(5616, 1, 1, 'elise'),
(2121, 1, 1, 'elr'),
(3271, 1, 1, 'enzo'),
(5239, 1, 1, 'espada'),
(5483, 1, 1, 'evoque'),
(2893, 1, 1, 'excellence'),
(3689, 1, 1, 'explorer'),
(3160, 1, 1, 'f355'),
(2033, 1, 1, 'f3dm'),
(3094, 1, 1, 'f40'),
(3293, 1, 1, 'f430'),
(3182, 1, 1, 'f50'),
(2892, 1, 1, 'facel'),
(3733, 1, 1, 'falcon'),
(5853, 1, 1, 'familia'),
(2915, 1, 1, 'ferrari'),
(3357, 1, 1, 'fiat'),
(3755, 1, 1, 'fiesta'),
(6648, 1, 1, 'figaro'),
(7197, 1, 1, 'firebird'),
(3532, 1, 1, 'fisker'),
(4363, 1, 1, 'fit'),
(3799, 1, 1, 'focus'),
(3554, 1, 1, 'ford'),
(8007, 1, 1, 'fortwo'),
(599, 1, 1, 'foteli'),
(604, 1, 1, 'frua'),
(4042, 1, 1, 'fso'),
(7088, 1, 1, 'fury'),
(6407, 1, 1, 'galant'),
(5261, 1, 1, 'gallardo'),
(8643, 1, 1, 'gol'),
(8665, 1, 1, 'golf'),
(3821, 1, 1, 'granada'),
(6169, 1, 1, 'grand'),
(942, 1, 1, 'gremlin'),
(2938, 1, 1, 'gto'),
(6029, 1, 1, 'gtr'),
(8378, 1, 1, 'hilux'),
(4232, 1, 1, 'hindustan'),
(4254, 1, 1, 'holden'),
(4276, 1, 1, 'honda'),
(963, 1, 1, 'hornet'),
(6912, 1, 1, 'huayra'),
(4517, 1, 1, 'hyundai'),
(7919, 1, 1, 'ibiza'),
(4583, 1, 1, 'imperial'),
(1924, 1, 1, 'invicta'),
(8400, 1, 1, 'ipsum'),
(5283, 1, 1, 'islero'),
(6518, 1, 1, 'ital'),
(4605, 1, 1, 'jaguar'),
(5305, 1, 1, 'jalpa'),
(5327, 1, 1, 'jarama'),
(4889, 1, 1, 'jeep'),
(8687, 1, 1, 'jetta'),
(3533, 1, 1, 'karma'),
(6258, 1, 1, 'kr175'),
(6279, 1, 1, 'kr200'),
(6473, 1, 1, 'l200'),
(3336, 1, 1, 'laferrari'),
(4997, 1, 1, 'lagonda'),
(5129, 1, 1, 'lamborghini'),
(6451, 1, 1, 'lancer'),
(1180, 1, 1, 'lancia'),
(8422, 1, 1, 'land'),
(6670, 1, 1, 'leaf'),
(8114, 1, 1, 'legacy'),
(1946, 1, 1, 'lesabre'),
(5504, 1, 1, 'lexus'),
(5505, 1, 1, 'lfa'),
(5019, 1, 1, 'lg6'),
(5526, 1, 1, 'lincoln'),
(5063, 1, 1, 'litre'),
(5593, 1, 1, 'lola'),
(2584, 1, 1, 'longchamp'),
(5615, 1, 1, 'lotus'),
(2960, 1, 1, 'lusso'),
(8599, 1, 1, 'm12'),
(1070, 1, 1, 'maestro'),
(591, 1, 1, 'maksymalna'),
(2606, 1, 1, 'mangusta'),
(586, 1, 1, 'manualna'),
(3227, 1, 1, 'maranello'),
(2144, 1, 1, 'marathon'),
(6539, 1, 1, 'marina'),
(2497, 1, 1, 'mark'),
(6170, 1, 1, 'marquis'),
(4976, 1, 1, 'marquise'),
(5701, 1, 1, 'maruti'),
(2429, 1, 1, 'maserati'),
(597, 1, 1, 'materialowe'),
(648, 1, 1, 'matowy'),
(5789, 1, 1, 'matra'),
(6692, 1, 1, 'maxima'),
(5767, 1, 1, 'maybach'),
(5811, 1, 1, 'mazda'),
(5745, 1, 1, 'mc12'),
(5637, 1, 1, 'mclaren'),
(5982, 1, 1, 'mercedes'),
(6168, 1, 1, 'mercury'),
(6257, 1, 1, 'messerschmitt'),
(603, 1, 1, 'metalik'),
(1092, 1, 1, 'metro'),
(6301, 1, 1, 'mgb'),
(6714, 1, 1, 'micra'),
(6322, 1, 1, 'midget'),
(6364, 1, 1, 'mini'),
(2451, 1, 1, 'minivans'),
(6561, 1, 1, 'minor'),
(8445, 1, 1, 'mirai'),
(6385, 1, 1, 'mitsubishi'),
(3866, 1, 1, 'model'),
(6191, 1, 1, 'monarch'),
(3027, 1, 1, 'mondial'),
(5415, 1, 1, 'montecarlo'),
(1114, 1, 1, 'montego'),
(6213, 1, 1, 'monterey'),
(829, 1, 1, 'montreal'),
(2297, 1, 1, 'monza'),
(6517, 1, 1, 'morris'),
(5875, 1, 1, 'mpv'),
(3910, 1, 1, 'mustang'),
(6583, 1, 1, 'nash'),
(1334, 1, 1, 'new'),
(2408, 1, 1, 'newport'),
(6605, 1, 1, 'nissan'),
(3822, 1, 1, 'north'),
(4385, 1, 1, 'nsx'),
(598, 1, 1, 'obicie'),
(6736, 1, 1, 'oldsmobile'),
(2319, 1, 1, 'opala'),
(6801, 1, 1, 'opel'),
(590, 1, 1, 'osiaga'),
(8775, 1, 1, 'p1800'),
(6956, 1, 1, 'p50'),
(6889, 1, 1, 'packard'),
(6911, 1, 1, 'pagani'),
(6495, 1, 1, 'pajero'),
(3444, 1, 1, 'panda'),
(6933, 1, 1, 'panoz'),
(2628, 1, 1, 'pantera'),
(8709, 1, 1, 'passat'),
(6955, 1, 1, 'peel'),
(717, 1, 1, 'perlowy'),
(6999, 1, 1, 'peugeot'),
(3577, 1, 1, 'platform'),
(7087, 1, 1, 'plymouth'),
(589, 1, 1, 'pojazd'),
(4043, 1, 1, 'polonez'),
(7131, 1, 1, 'pontiac'),
(7241, 1, 1, 'porsche'),
(585, 1, 1, 'posiada'),
(592, 1, 1, 'predkosc'),
(4407, 1, 1, 'prelude'),
(8467, 1, 1, 'prius'),
(601, 1, 1, 'przeprowadzenia'),
(594, 1, 1, 'przyspieszczeni'),
(3466, 1, 1, 'punto'),
(2055, 1, 1, 'qin'),
(5918, 1, 1, 'r360'),
(7571, 1, 1, 'rambler'),
(3932, 1, 1, 'ranchero'),
(5790, 1, 1, 'rancho'),
(5481, 1, 1, 'range'),
(2848, 1, 1, 'ranger'),
(5107, 1, 1, 'rapide'),
(4998, 1, 1, 'rapier'),
(8489, 1, 1, 'rav4'),
(7593, 1, 1, 'renault'),
(4954, 1, 1, 'renegade'),
(1968, 1, 1, 'riviera'),
(6934, 1, 1, 'roadster'),
(626, 1, 1, 'romeo'),
(5482, 1, 1, 'rover'),
(3954, 1, 1, 'rs200'),
(4495, 1, 1, 's2000'),
(4451, 1, 1, 's500'),
(4429, 1, 1, 's600'),
(4473, 1, 1, 's800'),
(7853, 1, 1, 'saab'),
(6235, 1, 1, 'sable'),
(7875, 1, 1, 'saleen'),
(584, 1, 1, 'samochod'),
(7896, 1, 1, 'saturn'),
(7918, 1, 1, 'seat'),
(595, 1, 1, 'sekund'),
(1462, 1, 1, 'series'),
(602, 1, 1, 'serwisu'),
(5349, 1, 1, 'silhouette'),
(7940, 1, 1, 'simca'),
(8954, 1, 1, 'skala'),
(944, 1, 1, 'skorzane'),
(587, 1, 1, 'skrzynie'),
(6052, 1, 1, 'slr'),
(8006, 1, 1, 'smart'),
(4561, 1, 1, 'sonata'),
(852, 1, 1, 'spider'),
(7701, 1, 1, 'sport'),
(8511, 1, 1, 'sports'),
(6584, 1, 1, 'statesman'),
(2716, 1, 1, 'stealth'),
(669, 1, 1, 'stradale'),
(5393, 1, 1, 'stratos'),
(7984, 1, 1, 'studebaker'),
(8070, 1, 1, 'subaru'),
(2341, 1, 1, 'suburban'),
(8534, 1, 1, 'supra'),
(8136, 1, 1, 'suzuki'),
(8180, 1, 1, 'swift'),
(5594, 1, 1, 't70'),
(2761, 1, 1, 'talon'),
(3976, 1, 1, 'taurus'),
(3998, 1, 1, 'tempo'),
(8202, 1, 1, 'tesla'),
(3049, 1, 1, 'testarossa'),
(2562, 1, 1, 'tomaso'),
(2473, 1, 1, 'town'),
(8268, 1, 1, 'toyota'),
(4020, 1, 1, 'transit'),
(6977, 1, 1, 'trident'),
(7724, 1, 1, 'twingo'),
(1550, 1, 1, 'type'),
(3488, 1, 1, 'uno'),
(5371, 1, 1, 'urraco'),
(5041, 1, 1, 'v12'),
(2650, 1, 1, 'vallelunga'),
(8556, 1, 1, 'vauxhall'),
(8578, 1, 1, 'vector'),
(6867, 1, 1, 'vectra'),
(2363, 1, 1, 'vega'),
(5571, 1, 1, 'versailles'),
(1836, 1, 1, 'veyron'),
(3622, 1, 1, 'victoria'),
(2870, 1, 1, 'villager'),
(2099, 1, 1, 'ville'),
(2738, 1, 1, 'viper'),
(8557, 1, 1, 'viva'),
(8621, 1, 1, 'volkswagen'),
(2385, 1, 1, 'volt'),
(8731, 1, 1, 'volvo'),
(7109, 1, 1, 'voyager'),
(6122, 1, 1, 'w123'),
(6145, 1, 1, 'w201'),
(8158, 1, 1, 'wagon'),
(1990, 1, 1, 'wildcat'),
(8932, 1, 1, 'willys'),
(600, 1, 1, 'wymaga'),
(4824, 1, 1, 'xj220'),
(4802, 1, 1, 'xjr'),
(4780, 1, 1, 'xjs'),
(4606, 1, 1, 'xk120'),
(4693, 1, 1, 'xk150'),
(6343, 1, 1, 'xpower'),
(1181, 1, 1, 'y10'),
(5459, 1, 1, 'ypsilon'),
(8953, 1, 1, 'zastava'),
(7746, 1, 1, 'zoe');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_sekeyword`
--

DROP TABLE IF EXISTS `ps_sekeyword`;
CREATE TABLE `ps_sekeyword` (
  `id_sekeyword` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `id_shop_group` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `keyword` varchar(256) NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_shop`
--

DROP TABLE IF EXISTS `ps_shop`;
CREATE TABLE `ps_shop` (
  `id_shop` int(11) UNSIGNED NOT NULL,
  `id_shop_group` int(11) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `id_category` int(11) UNSIGNED NOT NULL DEFAULT 1,
  `id_theme` int(1) UNSIGNED NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_shop`
--

INSERT INTO `ps_shop` (`id_shop`, `id_shop_group`, `name`, `id_category`, `id_theme`, `active`, `deleted`) VALUES
(1, 1, 'otomoto', 2, 1, 1, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_shop_group`
--

DROP TABLE IF EXISTS `ps_shop_group`;
CREATE TABLE `ps_shop_group` (
  `id_shop_group` int(11) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `share_customer` tinyint(1) NOT NULL,
  `share_order` tinyint(1) NOT NULL,
  `share_stock` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_shop_group`
--

INSERT INTO `ps_shop_group` (`id_shop_group`, `name`, `share_customer`, `share_order`, `share_stock`, `active`, `deleted`) VALUES
(1, 'Default', 0, 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_shop_url`
--

DROP TABLE IF EXISTS `ps_shop_url`;
CREATE TABLE `ps_shop_url` (
  `id_shop_url` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  `domain` varchar(150) NOT NULL,
  `domain_ssl` varchar(150) NOT NULL,
  `physical_uri` varchar(64) NOT NULL,
  `virtual_uri` varchar(64) NOT NULL,
  `main` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_shop_url`
--

INSERT INTO `ps_shop_url` (`id_shop_url`, `id_shop`, `domain`, `domain_ssl`, `physical_uri`, `virtual_uri`, `main`, `active`) VALUES
(1, 1, '172.20.83.60', '172.20.83.60', '/', '', 1, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_smarty_cache`
--

DROP TABLE IF EXISTS `ps_smarty_cache`;
CREATE TABLE `ps_smarty_cache` (
  `id_smarty_cache` char(40) NOT NULL,
  `name` char(40) NOT NULL,
  `cache_id` varchar(254) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `content` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_smarty_last_flush`
--

DROP TABLE IF EXISTS `ps_smarty_last_flush`;
CREATE TABLE `ps_smarty_last_flush` (
  `type` enum('compile','template') NOT NULL,
  `last_flush` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_smarty_last_flush`
--

INSERT INTO `ps_smarty_last_flush` (`type`, `last_flush`) VALUES
('compile', '2018-12-03 23:33:41'),
('template', '2018-12-04 00:42:22');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_smarty_lazy_cache`
--

DROP TABLE IF EXISTS `ps_smarty_lazy_cache`;
CREATE TABLE `ps_smarty_lazy_cache` (
  `template_hash` varchar(32) NOT NULL DEFAULT '',
  `cache_id` varchar(255) NOT NULL DEFAULT '',
  `compile_id` varchar(32) NOT NULL DEFAULT '',
  `filepath` varchar(255) NOT NULL DEFAULT '',
  `last_update` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_smarty_lazy_cache`
--

INSERT INTO `ps_smarty_lazy_cache` (`template_hash`, `cache_id`, `compile_id`, `filepath`, `last_update`) VALUES
('073db4b24be9294adb8875f6c6bcc9a5', 'blocknewproducts|20181204|3|3|14', '', 'blocknewproducts/20181204/3/3/14/5b/3f/a5/5b3fa53881f85e796313ba8b6a28dc2b1239402d.blocknewproducts.tpl.php', '2018-12-04 11:11:54'),
('17e36eec92e685dbd4e4df1b9011309f', 'blockstore|3|3|14', '', 'blockstore/3/3/14/96/47/f2/9647f2bfb40be35a425efa9b5cdf4eea88f1e945.blockstore.tpl.php', '2018-12-04 11:11:54'),
('1c2cb083a2501296262fc9eacdebdcbd', 'homeslider|1|1|14', '', 'homeslider/1/1/14/b7/99/17/b79917e19c39a5c2f6896b068d19cda5bdc51987.homeslider.tpl.php', '2018-12-04 10:51:48'),
('1cdf7e0610a586e2208481caae275dda', 'blockmanufacturer|3|3|14', '', 'blockmanufacturer/3/3/14/bd/6d/07/bd6d075f59b3bfa48bab4aac370402e0689d4036.blockmanufacturer.tpl.php', '2018-12-04 11:11:54'),
('2132324ad7612ed7cb91de8444f03dae', 'blocksearch-top|1|1|14', '', 'blocksearch_top/1/1/14/4e/c5/f0/4ec5f09962f5f0725e3faf31c01e69428a9d500b.blocksearch-top.tpl.php', '2018-12-04 11:07:10'),
('2132324ad7612ed7cb91de8444f03dae', 'blocksearch-top|3|3|14', '', 'blocksearch_top/3/3/14/4e/c5/f0/4ec5f09962f5f0725e3faf31c01e69428a9d500b.blocksearch-top.tpl.php', '2018-12-04 11:10:29'),
('35cd305a7376d26fe34a5dbc3549c31d', 'blockcontact|1|1|14', '', 'blockcontact/1/1/14/3d/5b/15/3d5b1584ffcf7c8b56d0c79b9f00a62f9454dc0f.nav.tpl.php', '2018-12-04 10:51:48'),
('35cd305a7376d26fe34a5dbc3549c31d', 'blockcontact|3|3|14', '', 'blockcontact/3/3/14/3d/5b/15/3d5b1584ffcf7c8b56d0c79b9f00a62f9454dc0f.nav.tpl.php', '2018-12-04 11:10:31'),
('399f3a8c743ef048e012f3c13f1bc314', 'blockcategories|3|3|14|2|3', '', 'blockcategories/3/3/14/2/3/1b/1c/21/1b1c215ce32173764aa2a2d1f148af37261154be.blockcategories.tpl.php', '2018-12-04 11:11:53'),
('4f296089919889fb5b29e0c5220afb8a', 'blockcontactinfos|1|1|14', '', 'blockcontactinfos/1/1/14/44/a3/ca/44a3ca6f70fb7cf18ad9ccaabb7c9b7695475dd2.blockcontactinfos.tpl.php', '2018-12-04 10:51:48'),
('4f296089919889fb5b29e0c5220afb8a', 'blockcontactinfos|3|3|14', '', 'blockcontactinfos/3/3/14/44/a3/ca/44a3ca6f70fb7cf18ad9ccaabb7c9b7695475dd2.blockcontactinfos.tpl.php', '2018-12-04 11:10:30'),
('614548d004ca83d561fc28811f573952', 'blockspecials-tab|20181204|1|1|14', '', 'blockspecials_tab/20181204/1/1/14/ff/a2/f3/ffa2f3cf0f343957a57c8a99d56c5295164533de.tab.tpl.php', '2018-12-04 10:51:48'),
('704e55cc1ebad8ca8d9ca6a5b7b2240d', 'blockbestsellers-home|1|1|14', '', 'blockbestsellers_home/1/1/14/2d/22/db/2d22db7b3d282a8d505bf2d26f4fe2cf5c580dc6.blockbestsellers-home.tpl.php', '2018-12-04 16:43:59'),
('729e08a789498e11db4ee7ed1dafd3af', 'blockspecials-home|20181204|1|1|14', '', 'blockspecials_home/20181204/1/1/14/b7/13/c1/b713c100fedffddfa260c35747bbce951f5ad6f6.blockspecials-home.tpl.php', '2018-12-04 10:51:48'),
('8cce06d74fb470f1ea86b9243a28d1c9', 'homefeatured-tab|1|1|14', '', 'homefeatured_tab/1/1/14/d1/ec/35/d1ec35425663d01dcc0e04833a4aa96a7fdfb7ba.tab.tpl.php', '2018-12-04 10:51:48'),
('9b076b330c3b383e100e4c23e6c16d43', 'blocktopmenu|1|1|14|index', '', 'blocktopmenu/1/1/14/index/20/22/61/202261737f7471ff70a95274dd663f2dc7c8ef47.blocktopmenu.tpl.php', '2018-12-04 10:51:48'),
('9b076b330c3b383e100e4c23e6c16d43', 'blocktopmenu|3|3|14|index', '', 'blocktopmenu/3/3/14/index/20/22/61/202261737f7471ff70a95274dd663f2dc7c8ef47.blocktopmenu.tpl.php', '2018-12-04 11:10:29'),
('a4edb6e4925cf4f684fdf466d509564a', 'blockpaymentlogo|3|3|14', '', '', '2018-12-04 11:11:54'),
('a818646d437d1d648322d07d6c385caa', 'blockspecials|20181204|9|20181204|3|3|14', '', 'blockspecials/20181204/9/20181204/3/3/14/c7/cb/5a/c7cb5a1b8f4aaa78439a29b3cc549b5c1e053c83.blockspecials.tpl.php', '2018-12-04 11:11:54'),
('b616481032ef196caf77e93f8740ba2b', 'blocksupplier|3|3|14', '', 'blocksupplier/3/3/14/4f/a7/f2/4fa7f22c9b08ac2c7995b5c4efea27f59c142246.blocksupplier.tpl.php', '2018-12-04 11:11:54'),
('b96e09f72175b88e25c97cde76542dd6', 'blockmyaccountfooter|1|1|14', '', 'blockmyaccountfooter/1/1/14/05/44/40/054440a5b928799e8ca909a0b328db11279aee9a.blockmyaccountfooter.tpl.php', '2018-12-04 10:51:48'),
('b96e09f72175b88e25c97cde76542dd6', 'blockmyaccountfooter|3|3|14', '', 'blockmyaccountfooter/3/3/14/05/44/40/054440a5b928799e8ca909a0b328db11279aee9a.blockmyaccountfooter.tpl.php', '2018-12-04 11:10:30'),
('bfe7a83fb1ce61839575db203c6a12a1', 'blockcategories|1|1|14|1', '', 'blockcategories/1/1/14/1/a5/88/0b/a5880b37cb81397979bd6724b1e436aac6fc922c.blockcategories_footer.tpl.php', '2018-12-04 10:51:48'),
('bfe7a83fb1ce61839575db203c6a12a1', 'blockcategories|3|3|14|3', '', 'blockcategories/3/3/14/3/a5/88/0b/a5880b37cb81397979bd6724b1e436aac6fc922c.blockcategories_footer.tpl.php', '2018-12-04 11:10:30'),
('c0f69a7a01e460742fded10f6ab3bcab', 'blocktags|3|3|14', '', '', '2018-12-04 11:11:54'),
('c48d03e88434c1b36ef2d67d66d7f58a', 'blockbestsellers-tab|1|1|14', '', 'blockbestsellers_tab/1/1/14/3f/09/f5/3f09f5b8078a6ad6ef0b7a13bf583277fab06f7f.tab.tpl.php', '2018-12-04 16:43:59'),
('d22b953fa4736374feaaf4b2ee38dd9a', 'blockcms|0|3|3|14', '', 'blockcms/0/3/3/14/1d/89/e8/1d89e8014f347982d948469f9543b359301e5949.blockcms.tpl.php', '2018-12-04 11:11:53'),
('d22b953fa4736374feaaf4b2ee38dd9a', 'blockcms|2|1|1|14', '', 'blockcms/2/1/1/14/1d/89/e8/1d89e8014f347982d948469f9543b359301e5949.blockcms.tpl.php', '2018-12-04 10:51:48'),
('d22b953fa4736374feaaf4b2ee38dd9a', 'blockcms|2|3|3|14', '', 'blockcms/2/3/3/14/1d/89/e8/1d89e8014f347982d948469f9543b359301e5949.blockcms.tpl.php', '2018-12-04 11:10:30'),
('d233cdf4166f53d791a695a6bfafa3cc', 'blocknewproducts-home|20181204|1|1|14', '', 'blocknewproducts_home/20181204/1/1/14/d4/f9/31/d4f9319358b5a40d86ff79038511f880f81b70fd.blocknewproducts_home.tpl.php', '2018-12-04 10:51:48'),
('eb92149692f7b06a705be8a062f03a16', 'blocknewproducts-tab|20181204|1|1|14', '', 'blocknewproducts_tab/20181204/1/1/14/72/4f/6d/724f6d692f525ebf1cf9abc827e367ef14b60def.tab.tpl.php', '2018-12-04 10:51:48'),
('f7b2d8122648c39fdccc4e63f44364ce', 'homefeatured|1|1|14', '', 'homefeatured/1/1/14/3a/a0/d6/3aa0d62e1049e5b9d12bc2cae084fdddf0d39b7a.homefeatured.tpl.php', '2018-12-04 10:51:48');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_specific_price`
--

DROP TABLE IF EXISTS `ps_specific_price`;
CREATE TABLE `ps_specific_price` (
  `id_specific_price` int(10) UNSIGNED NOT NULL,
  `id_specific_price_rule` int(11) UNSIGNED NOT NULL,
  `id_cart` int(11) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT 1,
  `id_shop_group` int(11) UNSIGNED NOT NULL,
  `id_currency` int(10) UNSIGNED NOT NULL,
  `id_country` int(10) UNSIGNED NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_product_attribute` int(10) UNSIGNED NOT NULL,
  `price` decimal(20,6) NOT NULL,
  `from_quantity` mediumint(8) UNSIGNED NOT NULL,
  `reduction` decimal(20,6) NOT NULL,
  `reduction_tax` tinyint(1) NOT NULL DEFAULT 1,
  `reduction_type` enum('amount','percentage') NOT NULL,
  `from` datetime NOT NULL,
  `to` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_specific_price`
--

INSERT INTO `ps_specific_price` (`id_specific_price`, `id_specific_price_rule`, `id_cart`, `id_product`, `id_shop`, `id_shop_group`, `id_currency`, `id_country`, `id_group`, `id_customer`, `id_product_attribute`, `price`, `from_quantity`, `reduction`, `reduction_tax`, `reduction_type`, `from`, `to`) VALUES
(1, 0, 0, 9, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.070000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 0, 0, 40, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.110000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 0, 0, 52, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.150000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 0, 0, 70, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.060000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 0, 0, 73, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.080000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 0, 0, 81, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.050000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 0, 0, 116, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.150000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 0, 0, 130, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.160000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 0, 0, 146, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.100000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 0, 0, 152, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.190000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 0, 0, 210, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.120000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 0, 0, 211, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.080000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 0, 0, 216, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.130000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 0, 0, 219, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.080000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 0, 0, 224, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.050000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 0, 0, 238, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.050000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 0, 0, 250, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.090000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 0, 0, 281, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.090000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 0, 0, 288, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.060000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 0, 0, 291, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.110000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 0, 0, 294, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.160000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 0, 0, 295, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.140000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 0, 0, 299, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.140000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 0, 0, 321, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.150000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 0, 0, 333, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.130000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 0, 0, 335, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.070000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 0, 0, 336, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.060000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 0, 0, 340, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.060000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 0, 0, 346, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.150000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 0, 0, 347, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.140000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 0, 0, 353, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.170000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 0, 0, 359, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.110000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 0, 0, 363, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.120000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 0, 0, 366, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.150000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 0, 0, 380, 1, 0, 0, 0, 0, 0, 0, '-1.000000', 1, '0.130000', 1, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_specific_price_priority`
--

DROP TABLE IF EXISTS `ps_specific_price_priority`;
CREATE TABLE `ps_specific_price_priority` (
  `id_specific_price_priority` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `priority` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_specific_price_rule`
--

DROP TABLE IF EXISTS `ps_specific_price_rule`;
CREATE TABLE `ps_specific_price_rule` (
  `id_specific_price_rule` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT 1,
  `id_currency` int(10) UNSIGNED NOT NULL,
  `id_country` int(10) UNSIGNED NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL,
  `from_quantity` mediumint(8) UNSIGNED NOT NULL,
  `price` decimal(20,6) DEFAULT NULL,
  `reduction` decimal(20,6) NOT NULL,
  `reduction_tax` tinyint(1) NOT NULL DEFAULT 1,
  `reduction_type` enum('amount','percentage') NOT NULL,
  `from` datetime NOT NULL,
  `to` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_specific_price_rule_condition`
--

DROP TABLE IF EXISTS `ps_specific_price_rule_condition`;
CREATE TABLE `ps_specific_price_rule_condition` (
  `id_specific_price_rule_condition` int(11) UNSIGNED NOT NULL,
  `id_specific_price_rule_condition_group` int(11) UNSIGNED NOT NULL,
  `type` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_specific_price_rule_condition_group`
--

DROP TABLE IF EXISTS `ps_specific_price_rule_condition_group`;
CREATE TABLE `ps_specific_price_rule_condition_group` (
  `id_specific_price_rule_condition_group` int(11) UNSIGNED NOT NULL,
  `id_specific_price_rule` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_state`
--

DROP TABLE IF EXISTS `ps_state`;
CREATE TABLE `ps_state` (
  `id_state` int(10) UNSIGNED NOT NULL,
  `id_country` int(11) UNSIGNED NOT NULL,
  `id_zone` int(11) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `iso_code` varchar(7) NOT NULL,
  `tax_behavior` smallint(1) NOT NULL DEFAULT 0,
  `active` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_state`
--

INSERT INTO `ps_state` (`id_state`, `id_country`, `id_zone`, `name`, `iso_code`, `tax_behavior`, `active`) VALUES
(1, 21, 2, 'Alabama', 'AL', 0, 1),
(2, 21, 2, 'Alaska', 'AK', 0, 1),
(3, 21, 2, 'Arizona', 'AZ', 0, 1),
(4, 21, 2, 'Arkansas', 'AR', 0, 1),
(5, 21, 2, 'California', 'CA', 0, 1),
(6, 21, 2, 'Colorado', 'CO', 0, 1),
(7, 21, 2, 'Connecticut', 'CT', 0, 1),
(8, 21, 2, 'Delaware', 'DE', 0, 1),
(9, 21, 2, 'Florida', 'FL', 0, 1),
(10, 21, 2, 'Georgia', 'GA', 0, 1),
(11, 21, 2, 'Hawaii', 'HI', 0, 1),
(12, 21, 2, 'Idaho', 'ID', 0, 1),
(13, 21, 2, 'Illinois', 'IL', 0, 1),
(14, 21, 2, 'Indiana', 'IN', 0, 1),
(15, 21, 2, 'Iowa', 'IA', 0, 1),
(16, 21, 2, 'Kansas', 'KS', 0, 1),
(17, 21, 2, 'Kentucky', 'KY', 0, 1),
(18, 21, 2, 'Louisiana', 'LA', 0, 1),
(19, 21, 2, 'Maine', 'ME', 0, 1),
(20, 21, 2, 'Maryland', 'MD', 0, 1),
(21, 21, 2, 'Massachusetts', 'MA', 0, 1),
(22, 21, 2, 'Michigan', 'MI', 0, 1),
(23, 21, 2, 'Minnesota', 'MN', 0, 1),
(24, 21, 2, 'Mississippi', 'MS', 0, 1),
(25, 21, 2, 'Missouri', 'MO', 0, 1),
(26, 21, 2, 'Montana', 'MT', 0, 1),
(27, 21, 2, 'Nebraska', 'NE', 0, 1),
(28, 21, 2, 'Nevada', 'NV', 0, 1),
(29, 21, 2, 'New Hampshire', 'NH', 0, 1),
(30, 21, 2, 'New Jersey', 'NJ', 0, 1),
(31, 21, 2, 'New Mexico', 'NM', 0, 1),
(32, 21, 2, 'New York', 'NY', 0, 1),
(33, 21, 2, 'North Carolina', 'NC', 0, 1),
(34, 21, 2, 'North Dakota', 'ND', 0, 1),
(35, 21, 2, 'Ohio', 'OH', 0, 1),
(36, 21, 2, 'Oklahoma', 'OK', 0, 1),
(37, 21, 2, 'Oregon', 'OR', 0, 1),
(38, 21, 2, 'Pennsylvania', 'PA', 0, 1),
(39, 21, 2, 'Rhode Island', 'RI', 0, 1),
(40, 21, 2, 'South Carolina', 'SC', 0, 1),
(41, 21, 2, 'South Dakota', 'SD', 0, 1),
(42, 21, 2, 'Tennessee', 'TN', 0, 1),
(43, 21, 2, 'Texas', 'TX', 0, 1),
(44, 21, 2, 'Utah', 'UT', 0, 1),
(45, 21, 2, 'Vermont', 'VT', 0, 1),
(46, 21, 2, 'Virginia', 'VA', 0, 1),
(47, 21, 2, 'Washington', 'WA', 0, 1),
(48, 21, 2, 'West Virginia', 'WV', 0, 1),
(49, 21, 2, 'Wisconsin', 'WI', 0, 1),
(50, 21, 2, 'Wyoming', 'WY', 0, 1),
(51, 21, 2, 'Puerto Rico', 'PR', 0, 1),
(52, 21, 2, 'US Virgin Islands', 'VI', 0, 1),
(53, 21, 2, 'District of Columbia', 'DC', 0, 1),
(54, 145, 2, 'Aguascalientes', 'AGS', 0, 1),
(55, 145, 2, 'Baja California', 'BCN', 0, 1),
(56, 145, 2, 'Baja California Sur', 'BCS', 0, 1),
(57, 145, 2, 'Campeche', 'CAM', 0, 1),
(58, 145, 2, 'Chiapas', 'CHP', 0, 1),
(59, 145, 2, 'Chihuahua', 'CHH', 0, 1),
(60, 145, 2, 'Coahuila', 'COA', 0, 1),
(61, 145, 2, 'Colima', 'COL', 0, 1),
(62, 145, 2, 'Distrito Federal', 'DIF', 0, 1),
(63, 145, 2, 'Durango', 'DUR', 0, 1),
(64, 145, 2, 'Guanajuato', 'GUA', 0, 1),
(65, 145, 2, 'Guerrero', 'GRO', 0, 1),
(66, 145, 2, 'Hidalgo', 'HID', 0, 1),
(67, 145, 2, 'Jalisco', 'JAL', 0, 1),
(68, 145, 2, 'Estado de MĂŠxico', 'MEX', 0, 1),
(69, 145, 2, 'MichoacĂĄn', 'MIC', 0, 1),
(70, 145, 2, 'Morelos', 'MOR', 0, 1),
(71, 145, 2, 'Nayarit', 'NAY', 0, 1),
(72, 145, 2, 'Nuevo LeĂłn', 'NLE', 0, 1),
(73, 145, 2, 'Oaxaca', 'OAX', 0, 1),
(74, 145, 2, 'Puebla', 'PUE', 0, 1),
(75, 145, 2, 'QuerĂŠtaro', 'QUE', 0, 1),
(76, 145, 2, 'Quintana Roo', 'ROO', 0, 1),
(77, 145, 2, 'San Luis PotosĂ­', 'SLP', 0, 1),
(78, 145, 2, 'Sinaloa', 'SIN', 0, 1),
(79, 145, 2, 'Sonora', 'SON', 0, 1),
(80, 145, 2, 'Tabasco', 'TAB', 0, 1),
(81, 145, 2, 'Tamaulipas', 'TAM', 0, 1),
(82, 145, 2, 'Tlaxcala', 'TLA', 0, 1),
(83, 145, 2, 'Veracruz', 'VER', 0, 1),
(84, 145, 2, 'YucatĂĄn', 'YUC', 0, 1),
(85, 145, 2, 'Zacatecas', 'ZAC', 0, 1),
(86, 4, 2, 'Ontario', 'ON', 0, 1),
(87, 4, 2, 'Quebec', 'QC', 0, 1),
(88, 4, 2, 'British Columbia', 'BC', 0, 1),
(89, 4, 2, 'Alberta', 'AB', 0, 1),
(90, 4, 2, 'Manitoba', 'MB', 0, 1),
(91, 4, 2, 'Saskatchewan', 'SK', 0, 1),
(92, 4, 2, 'Nova Scotia', 'NS', 0, 1),
(93, 4, 2, 'New Brunswick', 'NB', 0, 1),
(94, 4, 2, 'Newfoundland and Labrador', 'NL', 0, 1),
(95, 4, 2, 'Prince Edward Island', 'PE', 0, 1),
(96, 4, 2, 'Northwest Territories', 'NT', 0, 1),
(97, 4, 2, 'Yukon', 'YT', 0, 1),
(98, 4, 2, 'Nunavut', 'NU', 0, 1),
(99, 44, 6, 'Buenos Aires', 'B', 0, 1),
(100, 44, 6, 'Catamarca', 'K', 0, 1),
(101, 44, 6, 'Chaco', 'H', 0, 1),
(102, 44, 6, 'Chubut', 'U', 0, 1),
(103, 44, 6, 'Ciudad de Buenos Aires', 'C', 0, 1),
(104, 44, 6, 'CĂłrdoba', 'X', 0, 1),
(105, 44, 6, 'Corrientes', 'W', 0, 1),
(106, 44, 6, 'Entre RĂ­os', 'E', 0, 1),
(107, 44, 6, 'Formosa', 'P', 0, 1),
(108, 44, 6, 'Jujuy', 'Y', 0, 1),
(109, 44, 6, 'La Pampa', 'L', 0, 1),
(110, 44, 6, 'La Rioja', 'F', 0, 1),
(111, 44, 6, 'Mendoza', 'M', 0, 1),
(112, 44, 6, 'Misiones', 'N', 0, 1),
(113, 44, 6, 'NeuquĂŠn', 'Q', 0, 1),
(114, 44, 6, 'RĂ­o Negro', 'R', 0, 1),
(115, 44, 6, 'Salta', 'A', 0, 1),
(116, 44, 6, 'San Juan', 'J', 0, 1),
(117, 44, 6, 'San Luis', 'D', 0, 1),
(118, 44, 6, 'Santa Cruz', 'Z', 0, 1),
(119, 44, 6, 'Santa Fe', 'S', 0, 1),
(120, 44, 6, 'Santiago del Estero', 'G', 0, 1),
(121, 44, 6, 'Tierra del Fuego', 'V', 0, 1),
(122, 44, 6, 'TucumĂĄn', 'T', 0, 1),
(123, 10, 1, 'Agrigento', 'AG', 0, 1),
(124, 10, 1, 'Alessandria', 'AL', 0, 1),
(125, 10, 1, 'Ancona', 'AN', 0, 1),
(126, 10, 1, 'Aosta', 'AO', 0, 1),
(127, 10, 1, 'Arezzo', 'AR', 0, 1),
(128, 10, 1, 'Ascoli Piceno', 'AP', 0, 1),
(129, 10, 1, 'Asti', 'AT', 0, 1),
(130, 10, 1, 'Avellino', 'AV', 0, 1),
(131, 10, 1, 'Bari', 'BA', 0, 1),
(132, 10, 1, 'Barletta-Andria-Trani', 'BT', 0, 1),
(133, 10, 1, 'Belluno', 'BL', 0, 1),
(134, 10, 1, 'Benevento', 'BN', 0, 1),
(135, 10, 1, 'Bergamo', 'BG', 0, 1),
(136, 10, 1, 'Biella', 'BI', 0, 1),
(137, 10, 1, 'Bologna', 'BO', 0, 1),
(138, 10, 1, 'Bolzano', 'BZ', 0, 1),
(139, 10, 1, 'Brescia', 'BS', 0, 1),
(140, 10, 1, 'Brindisi', 'BR', 0, 1),
(141, 10, 1, 'Cagliari', 'CA', 0, 1),
(142, 10, 1, 'Caltanissetta', 'CL', 0, 1),
(143, 10, 1, 'Campobasso', 'CB', 0, 1),
(144, 10, 1, 'Carbonia-Iglesias', 'CI', 0, 1),
(145, 10, 1, 'Caserta', 'CE', 0, 1),
(146, 10, 1, 'Catania', 'CT', 0, 1),
(147, 10, 1, 'Catanzaro', 'CZ', 0, 1),
(148, 10, 1, 'Chieti', 'CH', 0, 1),
(149, 10, 1, 'Como', 'CO', 0, 1),
(150, 10, 1, 'Cosenza', 'CS', 0, 1),
(151, 10, 1, 'Cremona', 'CR', 0, 1),
(152, 10, 1, 'Crotone', 'KR', 0, 1),
(153, 10, 1, 'Cuneo', 'CN', 0, 1),
(154, 10, 1, 'Enna', 'EN', 0, 1),
(155, 10, 1, 'Fermo', 'FM', 0, 1),
(156, 10, 1, 'Ferrara', 'FE', 0, 1),
(157, 10, 1, 'Firenze', 'FI', 0, 1),
(158, 10, 1, 'Foggia', 'FG', 0, 1),
(159, 10, 1, 'ForlĂŹ-Cesena', 'FC', 0, 1),
(160, 10, 1, 'Frosinone', 'FR', 0, 1),
(161, 10, 1, 'Genova', 'GE', 0, 1),
(162, 10, 1, 'Gorizia', 'GO', 0, 1),
(163, 10, 1, 'Grosseto', 'GR', 0, 1),
(164, 10, 1, 'Imperia', 'IM', 0, 1),
(165, 10, 1, 'Isernia', 'IS', 0, 1),
(166, 10, 1, 'L\'Aquila', 'AQ', 0, 1),
(167, 10, 1, 'La Spezia', 'SP', 0, 1),
(168, 10, 1, 'Latina', 'LT', 0, 1),
(169, 10, 1, 'Lecce', 'LE', 0, 1),
(170, 10, 1, 'Lecco', 'LC', 0, 1),
(171, 10, 1, 'Livorno', 'LI', 0, 1),
(172, 10, 1, 'Lodi', 'LO', 0, 1),
(173, 10, 1, 'Lucca', 'LU', 0, 1),
(174, 10, 1, 'Macerata', 'MC', 0, 1),
(175, 10, 1, 'Mantova', 'MN', 0, 1),
(176, 10, 1, 'Massa', 'MS', 0, 1),
(177, 10, 1, 'Matera', 'MT', 0, 1),
(178, 10, 1, 'Medio Campidano', 'VS', 0, 1),
(179, 10, 1, 'Messina', 'ME', 0, 1),
(180, 10, 1, 'Milano', 'MI', 0, 1),
(181, 10, 1, 'Modena', 'MO', 0, 1),
(182, 10, 1, 'Monza e della Brianza', 'MB', 0, 1),
(183, 10, 1, 'Napoli', 'NA', 0, 1),
(184, 10, 1, 'Novara', 'NO', 0, 1),
(185, 10, 1, 'Nuoro', 'NU', 0, 1),
(186, 10, 1, 'Ogliastra', 'OG', 0, 1),
(187, 10, 1, 'Olbia-Tempio', 'OT', 0, 1),
(188, 10, 1, 'Oristano', 'OR', 0, 1),
(189, 10, 1, 'Padova', 'PD', 0, 1),
(190, 10, 1, 'Palermo', 'PA', 0, 1),
(191, 10, 1, 'Parma', 'PR', 0, 1),
(192, 10, 1, 'Pavia', 'PV', 0, 1),
(193, 10, 1, 'Perugia', 'PG', 0, 1),
(194, 10, 1, 'Pesaro-Urbino', 'PU', 0, 1),
(195, 10, 1, 'Pescara', 'PE', 0, 1),
(196, 10, 1, 'Piacenza', 'PC', 0, 1),
(197, 10, 1, 'Pisa', 'PI', 0, 1),
(198, 10, 1, 'Pistoia', 'PT', 0, 1),
(199, 10, 1, 'Pordenone', 'PN', 0, 1),
(200, 10, 1, 'Potenza', 'PZ', 0, 1),
(201, 10, 1, 'Prato', 'PO', 0, 1),
(202, 10, 1, 'Ragusa', 'RG', 0, 1),
(203, 10, 1, 'Ravenna', 'RA', 0, 1),
(204, 10, 1, 'Reggio Calabria', 'RC', 0, 1),
(205, 10, 1, 'Reggio Emilia', 'RE', 0, 1),
(206, 10, 1, 'Rieti', 'RI', 0, 1),
(207, 10, 1, 'Rimini', 'RN', 0, 1),
(208, 10, 1, 'Roma', 'RM', 0, 1),
(209, 10, 1, 'Rovigo', 'RO', 0, 1),
(210, 10, 1, 'Salerno', 'SA', 0, 1),
(211, 10, 1, 'Sassari', 'SS', 0, 1),
(212, 10, 1, 'Savona', 'SV', 0, 1),
(213, 10, 1, 'Siena', 'SI', 0, 1),
(214, 10, 1, 'Siracusa', 'SR', 0, 1),
(215, 10, 1, 'Sondrio', 'SO', 0, 1),
(216, 10, 1, 'Taranto', 'TA', 0, 1),
(217, 10, 1, 'Teramo', 'TE', 0, 1),
(218, 10, 1, 'Terni', 'TR', 0, 1),
(219, 10, 1, 'Torino', 'TO', 0, 1),
(220, 10, 1, 'Trapani', 'TP', 0, 1),
(221, 10, 1, 'Trento', 'TN', 0, 1),
(222, 10, 1, 'Treviso', 'TV', 0, 1),
(223, 10, 1, 'Trieste', 'TS', 0, 1),
(224, 10, 1, 'Udine', 'UD', 0, 1),
(225, 10, 1, 'Varese', 'VA', 0, 1),
(226, 10, 1, 'Venezia', 'VE', 0, 1),
(227, 10, 1, 'Verbano-Cusio-Ossola', 'VB', 0, 1),
(228, 10, 1, 'Vercelli', 'VC', 0, 1),
(229, 10, 1, 'Verona', 'VR', 0, 1),
(230, 10, 1, 'Vibo Valentia', 'VV', 0, 1),
(231, 10, 1, 'Vicenza', 'VI', 0, 1),
(232, 10, 1, 'Viterbo', 'VT', 0, 1),
(233, 111, 3, 'Aceh', 'AC', 0, 1),
(234, 111, 3, 'Bali', 'BA', 0, 1),
(235, 111, 3, 'Bangka', 'BB', 0, 1),
(236, 111, 3, 'Banten', 'BT', 0, 1),
(237, 111, 3, 'Bengkulu', 'BE', 0, 1),
(238, 111, 3, 'Central Java', 'JT', 0, 1),
(239, 111, 3, 'Central Kalimantan', 'KT', 0, 1),
(240, 111, 3, 'Central Sulawesi', 'ST', 0, 1),
(241, 111, 3, 'Coat of arms of East Java', 'JI', 0, 1),
(242, 111, 3, 'East kalimantan', 'KI', 0, 1),
(243, 111, 3, 'East Nusa Tenggara', 'NT', 0, 1),
(244, 111, 3, 'Lambang propinsi', 'GO', 0, 1),
(245, 111, 3, 'Jakarta', 'JK', 0, 1),
(246, 111, 3, 'Jambi', 'JA', 0, 1),
(247, 111, 3, 'Lampung', 'LA', 0, 1),
(248, 111, 3, 'Maluku', 'MA', 0, 1),
(249, 111, 3, 'North Maluku', 'MU', 0, 1),
(250, 111, 3, 'North Sulawesi', 'SA', 0, 1),
(251, 111, 3, 'North Sumatra', 'SU', 0, 1),
(252, 111, 3, 'Papua', 'PA', 0, 1),
(253, 111, 3, 'Riau', 'RI', 0, 1),
(254, 111, 3, 'Lambang Riau', 'KR', 0, 1),
(255, 111, 3, 'Southeast Sulawesi', 'SG', 0, 1),
(256, 111, 3, 'South Kalimantan', 'KS', 0, 1),
(257, 111, 3, 'South Sulawesi', 'SN', 0, 1),
(258, 111, 3, 'South Sumatra', 'SS', 0, 1),
(259, 111, 3, 'West Java', 'JB', 0, 1),
(260, 111, 3, 'West Kalimantan', 'KB', 0, 1),
(261, 111, 3, 'West Nusa Tenggara', 'NB', 0, 1),
(262, 111, 3, 'Lambang Provinsi Papua Barat', 'PB', 0, 1),
(263, 111, 3, 'West Sulawesi', 'SR', 0, 1),
(264, 111, 3, 'West Sumatra', 'SB', 0, 1),
(265, 111, 3, 'Yogyakarta', 'YO', 0, 1),
(266, 11, 3, 'Aichi', '23', 0, 1),
(267, 11, 3, 'Akita', '05', 0, 1),
(268, 11, 3, 'Aomori', '02', 0, 1),
(269, 11, 3, 'Chiba', '12', 0, 1),
(270, 11, 3, 'Ehime', '38', 0, 1),
(271, 11, 3, 'Fukui', '18', 0, 1),
(272, 11, 3, 'Fukuoka', '40', 0, 1),
(273, 11, 3, 'Fukushima', '07', 0, 1),
(274, 11, 3, 'Gifu', '21', 0, 1),
(275, 11, 3, 'Gunma', '10', 0, 1),
(276, 11, 3, 'Hiroshima', '34', 0, 1),
(277, 11, 3, 'Hokkaido', '01', 0, 1),
(278, 11, 3, 'Hyogo', '28', 0, 1),
(279, 11, 3, 'Ibaraki', '08', 0, 1),
(280, 11, 3, 'Ishikawa', '17', 0, 1),
(281, 11, 3, 'Iwate', '03', 0, 1),
(282, 11, 3, 'Kagawa', '37', 0, 1),
(283, 11, 3, 'Kagoshima', '46', 0, 1),
(284, 11, 3, 'Kanagawa', '14', 0, 1),
(285, 11, 3, 'Kochi', '39', 0, 1),
(286, 11, 3, 'Kumamoto', '43', 0, 1),
(287, 11, 3, 'Kyoto', '26', 0, 1),
(288, 11, 3, 'Mie', '24', 0, 1),
(289, 11, 3, 'Miyagi', '04', 0, 1),
(290, 11, 3, 'Miyazaki', '45', 0, 1),
(291, 11, 3, 'Nagano', '20', 0, 1),
(292, 11, 3, 'Nagasaki', '42', 0, 1),
(293, 11, 3, 'Nara', '29', 0, 1),
(294, 11, 3, 'Niigata', '15', 0, 1),
(295, 11, 3, 'Oita', '44', 0, 1),
(296, 11, 3, 'Okayama', '33', 0, 1),
(297, 11, 3, 'Okinawa', '47', 0, 1),
(298, 11, 3, 'Osaka', '27', 0, 1),
(299, 11, 3, 'Saga', '41', 0, 1),
(300, 11, 3, 'Saitama', '11', 0, 1),
(301, 11, 3, 'Shiga', '25', 0, 1),
(302, 11, 3, 'Shimane', '32', 0, 1),
(303, 11, 3, 'Shizuoka', '22', 0, 1),
(304, 11, 3, 'Tochigi', '09', 0, 1),
(305, 11, 3, 'Tokushima', '36', 0, 1),
(306, 11, 3, 'Tokyo', '13', 0, 1),
(307, 11, 3, 'Tottori', '31', 0, 1),
(308, 11, 3, 'Toyama', '16', 0, 1),
(309, 11, 3, 'Wakayama', '30', 0, 1),
(310, 11, 3, 'Yamagata', '06', 0, 1),
(311, 11, 3, 'Yamaguchi', '35', 0, 1),
(312, 11, 3, 'Yamanashi', '19', 0, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_statssearch`
--

DROP TABLE IF EXISTS `ps_statssearch`;
CREATE TABLE `ps_statssearch` (
  `id_statssearch` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `id_shop_group` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `keywords` varchar(255) NOT NULL,
  `results` int(6) NOT NULL DEFAULT 0,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_statssearch`
--

INSERT INTO `ps_statssearch` (`id_statssearch`, `id_shop`, `id_shop_group`, `keywords`, `results`, `date_add`) VALUES
(1, 1, 1, 'audi', 0, '2018-12-03 22:56:58'),
(2, 1, 1, 'audi', 0, '2018-12-03 22:56:59'),
(3, 1, 1, 'audi', 3, '2018-12-03 23:37:12'),
(4, 1, 1, 'Audi R8', 3, '2018-12-03 23:37:15');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_stock`
--

DROP TABLE IF EXISTS `ps_stock`;
CREATE TABLE `ps_stock` (
  `id_stock` int(11) UNSIGNED NOT NULL,
  `id_warehouse` int(11) UNSIGNED NOT NULL,
  `id_product` int(11) UNSIGNED NOT NULL,
  `id_product_attribute` int(11) UNSIGNED NOT NULL,
  `reference` varchar(32) NOT NULL,
  `ean13` varchar(13) DEFAULT NULL,
  `upc` varchar(12) DEFAULT NULL,
  `physical_quantity` int(11) UNSIGNED NOT NULL,
  `usable_quantity` int(11) UNSIGNED NOT NULL,
  `price_te` decimal(20,6) DEFAULT 0.000000
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_stock_available`
--

DROP TABLE IF EXISTS `ps_stock_available`;
CREATE TABLE `ps_stock_available` (
  `id_stock_available` int(11) UNSIGNED NOT NULL,
  `id_product` int(11) UNSIGNED NOT NULL,
  `id_product_attribute` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  `id_shop_group` int(11) UNSIGNED NOT NULL,
  `quantity` int(10) NOT NULL DEFAULT 0,
  `depends_on_stock` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `out_of_stock` tinyint(1) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_stock_available`
--

INSERT INTO `ps_stock_available` (`id_stock_available`, `id_product`, `id_product_attribute`, `id_shop`, `id_shop_group`, `quantity`, `depends_on_stock`, `out_of_stock`) VALUES
(1, 1, 0, 1, 0, 1, 0, 0),
(2, 2, 0, 1, 0, 0, 0, 0),
(3, 3, 0, 1, 0, 1, 0, 0),
(4, 4, 0, 1, 0, 1, 0, 0),
(5, 5, 0, 1, 0, 1, 0, 0),
(6, 6, 0, 1, 0, 1, 0, 0),
(7, 7, 0, 1, 0, 1, 0, 0),
(8, 8, 0, 1, 0, 1, 0, 0),
(9, 9, 0, 1, 0, 1, 0, 0),
(10, 10, 0, 1, 0, 1, 0, 0),
(11, 11, 0, 1, 0, 1, 0, 0),
(12, 12, 0, 1, 0, 1, 0, 0),
(13, 13, 0, 1, 0, 1, 0, 0),
(14, 14, 0, 1, 0, 1, 0, 0),
(15, 15, 0, 1, 0, 1, 0, 0),
(16, 16, 0, 1, 0, 1, 0, 0),
(17, 17, 0, 1, 0, 1, 0, 0),
(18, 18, 0, 1, 0, 1, 0, 0),
(19, 19, 0, 1, 0, 1, 0, 0),
(20, 20, 0, 1, 0, 1, 0, 0),
(21, 21, 0, 1, 0, 1, 0, 0),
(22, 22, 0, 1, 0, 1, 0, 0),
(23, 23, 0, 1, 0, 1, 0, 0),
(24, 24, 0, 1, 0, 1, 0, 0),
(25, 25, 0, 1, 0, 1, 0, 0),
(26, 26, 0, 1, 0, 1, 0, 0),
(27, 27, 0, 1, 0, 1, 0, 0),
(28, 28, 0, 1, 0, 1, 0, 0),
(29, 29, 0, 1, 0, 1, 0, 0),
(30, 30, 0, 1, 0, 1, 0, 0),
(31, 31, 0, 1, 0, 1, 0, 0),
(32, 32, 0, 1, 0, 1, 0, 0),
(33, 33, 0, 1, 0, 1, 0, 0),
(34, 34, 0, 1, 0, 1, 0, 0),
(35, 35, 0, 1, 0, 1, 0, 0),
(36, 36, 0, 1, 0, 1, 0, 0),
(37, 37, 0, 1, 0, 1, 0, 0),
(38, 38, 0, 1, 0, 1, 0, 0),
(39, 39, 0, 1, 0, 1, 0, 0),
(40, 40, 0, 1, 0, 1, 0, 0),
(41, 41, 0, 1, 0, 1, 0, 0),
(42, 42, 0, 1, 0, 1, 0, 0),
(43, 43, 0, 1, 0, 1, 0, 0),
(44, 44, 0, 1, 0, 1, 0, 0),
(45, 45, 0, 1, 0, 1, 0, 0),
(46, 46, 0, 1, 0, 1, 0, 0),
(47, 47, 0, 1, 0, 1, 0, 0),
(48, 48, 0, 1, 0, 1, 0, 0),
(49, 49, 0, 1, 0, 1, 0, 0),
(50, 50, 0, 1, 0, 1, 0, 0),
(51, 51, 0, 1, 0, 1, 0, 0),
(52, 52, 0, 1, 0, 1, 0, 0),
(53, 53, 0, 1, 0, 1, 0, 0),
(54, 54, 0, 1, 0, 1, 0, 0),
(55, 55, 0, 1, 0, 1, 0, 0),
(56, 56, 0, 1, 0, 1, 0, 0),
(57, 57, 0, 1, 0, 1, 0, 0),
(58, 58, 0, 1, 0, 1, 0, 0),
(59, 59, 0, 1, 0, 1, 0, 0),
(60, 60, 0, 1, 0, 1, 0, 0),
(61, 61, 0, 1, 0, 1, 0, 0),
(62, 62, 0, 1, 0, 1, 0, 0),
(63, 63, 0, 1, 0, 1, 0, 0),
(64, 64, 0, 1, 0, 1, 0, 0),
(65, 65, 0, 1, 0, 1, 0, 0),
(66, 66, 0, 1, 0, 1, 0, 0),
(67, 67, 0, 1, 0, 1, 0, 0),
(68, 68, 0, 1, 0, 1, 0, 0),
(69, 69, 0, 1, 0, 1, 0, 0),
(70, 70, 0, 1, 0, 1, 0, 0),
(71, 71, 0, 1, 0, 1, 0, 0),
(72, 72, 0, 1, 0, 1, 0, 0),
(73, 73, 0, 1, 0, 1, 0, 0),
(74, 74, 0, 1, 0, 1, 0, 0),
(75, 75, 0, 1, 0, 1, 0, 0),
(76, 76, 0, 1, 0, 1, 0, 0),
(77, 77, 0, 1, 0, 1, 0, 0),
(78, 78, 0, 1, 0, 1, 0, 0),
(79, 79, 0, 1, 0, 1, 0, 0),
(80, 80, 0, 1, 0, 1, 0, 0),
(81, 81, 0, 1, 0, 1, 0, 0),
(82, 82, 0, 1, 0, 1, 0, 0),
(83, 83, 0, 1, 0, 1, 0, 0),
(84, 84, 0, 1, 0, 1, 0, 0),
(85, 85, 0, 1, 0, 1, 0, 0),
(86, 86, 0, 1, 0, 1, 0, 0),
(87, 87, 0, 1, 0, 1, 0, 0),
(88, 88, 0, 1, 0, 1, 0, 0),
(89, 89, 0, 1, 0, 1, 0, 0),
(90, 90, 0, 1, 0, 1, 0, 0),
(91, 91, 0, 1, 0, 1, 0, 0),
(92, 92, 0, 1, 0, 1, 0, 0),
(93, 93, 0, 1, 0, 1, 0, 0),
(94, 94, 0, 1, 0, 1, 0, 0),
(95, 95, 0, 1, 0, 1, 0, 0),
(96, 96, 0, 1, 0, 1, 0, 0),
(97, 97, 0, 1, 0, 1, 0, 0),
(98, 98, 0, 1, 0, 1, 0, 0),
(99, 99, 0, 1, 0, 1, 0, 0),
(100, 100, 0, 1, 0, 1, 0, 0),
(101, 101, 0, 1, 0, 1, 0, 0),
(102, 102, 0, 1, 0, 1, 0, 0),
(103, 103, 0, 1, 0, 1, 0, 0),
(104, 104, 0, 1, 0, 1, 0, 0),
(105, 105, 0, 1, 0, 1, 0, 0),
(106, 106, 0, 1, 0, 1, 0, 0),
(107, 107, 0, 1, 0, 1, 0, 0),
(108, 108, 0, 1, 0, 1, 0, 0),
(109, 109, 0, 1, 0, 1, 0, 0),
(110, 110, 0, 1, 0, 1, 0, 0),
(111, 111, 0, 1, 0, 1, 0, 0),
(112, 112, 0, 1, 0, 1, 0, 0),
(113, 113, 0, 1, 0, 1, 0, 0),
(114, 114, 0, 1, 0, 1, 0, 0),
(115, 115, 0, 1, 0, 1, 0, 0),
(116, 116, 0, 1, 0, 1, 0, 0),
(117, 117, 0, 1, 0, 1, 0, 0),
(118, 118, 0, 1, 0, 1, 0, 0),
(119, 119, 0, 1, 0, 1, 0, 0),
(120, 120, 0, 1, 0, 1, 0, 0),
(121, 121, 0, 1, 0, 1, 0, 0),
(122, 122, 0, 1, 0, 1, 0, 0),
(123, 123, 0, 1, 0, 1, 0, 0),
(124, 124, 0, 1, 0, 1, 0, 0),
(125, 125, 0, 1, 0, 1, 0, 0),
(126, 126, 0, 1, 0, 1, 0, 0),
(127, 127, 0, 1, 0, 1, 0, 0),
(128, 128, 0, 1, 0, 1, 0, 0),
(129, 129, 0, 1, 0, 1, 0, 0),
(130, 130, 0, 1, 0, 1, 0, 0),
(131, 131, 0, 1, 0, 1, 0, 0),
(132, 132, 0, 1, 0, 1, 0, 0),
(133, 133, 0, 1, 0, 1, 0, 0),
(134, 134, 0, 1, 0, 1, 0, 0),
(135, 135, 0, 1, 0, 1, 0, 0),
(136, 136, 0, 1, 0, 1, 0, 0),
(137, 137, 0, 1, 0, 1, 0, 0),
(138, 138, 0, 1, 0, 1, 0, 0),
(139, 139, 0, 1, 0, 1, 0, 0),
(140, 140, 0, 1, 0, 1, 0, 0),
(141, 141, 0, 1, 0, 1, 0, 0),
(142, 142, 0, 1, 0, 1, 0, 0),
(143, 143, 0, 1, 0, 1, 0, 0),
(144, 144, 0, 1, 0, 1, 0, 0),
(145, 145, 0, 1, 0, 1, 0, 0),
(146, 146, 0, 1, 0, 1, 0, 0),
(147, 147, 0, 1, 0, 1, 0, 0),
(148, 148, 0, 1, 0, 1, 0, 0),
(149, 149, 0, 1, 0, 1, 0, 0),
(150, 150, 0, 1, 0, 1, 0, 0),
(151, 151, 0, 1, 0, 1, 0, 0),
(152, 152, 0, 1, 0, 1, 0, 0),
(153, 153, 0, 1, 0, 1, 0, 0),
(154, 154, 0, 1, 0, 1, 0, 0),
(155, 155, 0, 1, 0, 1, 0, 0),
(156, 156, 0, 1, 0, 1, 0, 0),
(157, 157, 0, 1, 0, 1, 0, 0),
(158, 158, 0, 1, 0, 1, 0, 0),
(159, 159, 0, 1, 0, 1, 0, 0),
(160, 160, 0, 1, 0, 1, 0, 0),
(161, 161, 0, 1, 0, 1, 0, 0),
(162, 162, 0, 1, 0, 1, 0, 0),
(163, 163, 0, 1, 0, 1, 0, 0),
(164, 164, 0, 1, 0, 1, 0, 0),
(165, 165, 0, 1, 0, 1, 0, 0),
(166, 166, 0, 1, 0, 1, 0, 0),
(167, 167, 0, 1, 0, 1, 0, 0),
(168, 168, 0, 1, 0, 1, 0, 0),
(169, 169, 0, 1, 0, 1, 0, 0),
(170, 170, 0, 1, 0, 1, 0, 0),
(171, 171, 0, 1, 0, 1, 0, 0),
(172, 172, 0, 1, 0, 1, 0, 0),
(173, 173, 0, 1, 0, 1, 0, 0),
(174, 174, 0, 1, 0, 1, 0, 0),
(175, 175, 0, 1, 0, 1, 0, 0),
(176, 176, 0, 1, 0, 1, 0, 0),
(177, 177, 0, 1, 0, 1, 0, 0),
(178, 178, 0, 1, 0, 1, 0, 0),
(179, 179, 0, 1, 0, 1, 0, 0),
(180, 180, 0, 1, 0, 1, 0, 0),
(181, 181, 0, 1, 0, 1, 0, 0),
(182, 182, 0, 1, 0, 1, 0, 0),
(183, 183, 0, 1, 0, 1, 0, 0),
(184, 184, 0, 1, 0, 1, 0, 0),
(185, 185, 0, 1, 0, 1, 0, 0),
(186, 186, 0, 1, 0, 1, 0, 0),
(187, 187, 0, 1, 0, 1, 0, 0),
(188, 188, 0, 1, 0, 1, 0, 0),
(189, 189, 0, 1, 0, 1, 0, 0),
(190, 190, 0, 1, 0, 1, 0, 0),
(191, 191, 0, 1, 0, 1, 0, 0),
(192, 192, 0, 1, 0, 1, 0, 0),
(193, 193, 0, 1, 0, 1, 0, 0),
(194, 194, 0, 1, 0, 1, 0, 0),
(195, 195, 0, 1, 0, 1, 0, 0),
(196, 196, 0, 1, 0, 1, 0, 0),
(197, 197, 0, 1, 0, 1, 0, 0),
(198, 198, 0, 1, 0, 1, 0, 0),
(199, 199, 0, 1, 0, 1, 0, 0),
(200, 200, 0, 1, 0, 1, 0, 0),
(201, 201, 0, 1, 0, 1, 0, 0),
(202, 202, 0, 1, 0, 1, 0, 0),
(203, 203, 0, 1, 0, 1, 0, 0),
(204, 204, 0, 1, 0, 1, 0, 0),
(205, 205, 0, 1, 0, 1, 0, 0),
(206, 206, 0, 1, 0, 1, 0, 0),
(207, 207, 0, 1, 0, 1, 0, 0),
(208, 208, 0, 1, 0, 1, 0, 0),
(209, 209, 0, 1, 0, 1, 0, 0),
(210, 210, 0, 1, 0, 1, 0, 0),
(211, 211, 0, 1, 0, 1, 0, 0),
(212, 212, 0, 1, 0, 1, 0, 0),
(213, 213, 0, 1, 0, 1, 0, 0),
(214, 214, 0, 1, 0, 1, 0, 0),
(215, 215, 0, 1, 0, 1, 0, 0),
(216, 216, 0, 1, 0, 1, 0, 0),
(217, 217, 0, 1, 0, 1, 0, 0),
(218, 218, 0, 1, 0, 1, 0, 0),
(219, 219, 0, 1, 0, 1, 0, 0),
(220, 220, 0, 1, 0, 1, 0, 0),
(221, 221, 0, 1, 0, 1, 0, 0),
(222, 222, 0, 1, 0, 1, 0, 0),
(223, 223, 0, 1, 0, 1, 0, 0),
(224, 224, 0, 1, 0, 1, 0, 0),
(225, 225, 0, 1, 0, 1, 0, 0),
(226, 226, 0, 1, 0, 1, 0, 0),
(227, 227, 0, 1, 0, 1, 0, 0),
(228, 228, 0, 1, 0, 1, 0, 0),
(229, 229, 0, 1, 0, 1, 0, 0),
(230, 230, 0, 1, 0, 1, 0, 0),
(231, 231, 0, 1, 0, 1, 0, 0),
(232, 232, 0, 1, 0, 1, 0, 0),
(233, 233, 0, 1, 0, 1, 0, 0),
(234, 234, 0, 1, 0, 1, 0, 0),
(235, 235, 0, 1, 0, 1, 0, 0),
(236, 236, 0, 1, 0, 1, 0, 0),
(237, 237, 0, 1, 0, 1, 0, 0),
(238, 238, 0, 1, 0, 1, 0, 0),
(239, 239, 0, 1, 0, 1, 0, 0),
(240, 240, 0, 1, 0, 1, 0, 0),
(241, 241, 0, 1, 0, 1, 0, 0),
(242, 242, 0, 1, 0, 1, 0, 0),
(243, 243, 0, 1, 0, 1, 0, 0),
(244, 244, 0, 1, 0, 1, 0, 0),
(245, 245, 0, 1, 0, 1, 0, 0),
(246, 246, 0, 1, 0, 1, 0, 0),
(247, 247, 0, 1, 0, 1, 0, 0),
(248, 248, 0, 1, 0, 1, 0, 0),
(249, 249, 0, 1, 0, 1, 0, 0),
(250, 250, 0, 1, 0, 1, 0, 0),
(251, 251, 0, 1, 0, 1, 0, 0),
(252, 252, 0, 1, 0, 1, 0, 0),
(253, 253, 0, 1, 0, 1, 0, 0),
(254, 254, 0, 1, 0, 1, 0, 0),
(255, 255, 0, 1, 0, 1, 0, 0),
(256, 256, 0, 1, 0, 1, 0, 0),
(257, 257, 0, 1, 0, 1, 0, 0),
(258, 258, 0, 1, 0, 1, 0, 0),
(259, 259, 0, 1, 0, 1, 0, 0),
(260, 260, 0, 1, 0, 1, 0, 0),
(261, 261, 0, 1, 0, 1, 0, 0),
(262, 262, 0, 1, 0, 1, 0, 0),
(263, 263, 0, 1, 0, 1, 0, 0),
(264, 264, 0, 1, 0, 1, 0, 0),
(265, 265, 0, 1, 0, 1, 0, 0),
(266, 266, 0, 1, 0, 1, 0, 0),
(267, 267, 0, 1, 0, 1, 0, 0),
(268, 268, 0, 1, 0, 1, 0, 0),
(269, 269, 0, 1, 0, 1, 0, 0),
(270, 270, 0, 1, 0, 1, 0, 0),
(271, 271, 0, 1, 0, 1, 0, 0),
(272, 272, 0, 1, 0, 1, 0, 0),
(273, 273, 0, 1, 0, 1, 0, 0),
(274, 274, 0, 1, 0, 1, 0, 0),
(275, 275, 0, 1, 0, 1, 0, 0),
(276, 276, 0, 1, 0, 1, 0, 0),
(277, 277, 0, 1, 0, 1, 0, 0),
(278, 278, 0, 1, 0, 1, 0, 0),
(279, 279, 0, 1, 0, 1, 0, 0),
(280, 280, 0, 1, 0, 1, 0, 0),
(281, 281, 0, 1, 0, 1, 0, 0),
(282, 282, 0, 1, 0, 1, 0, 0),
(283, 283, 0, 1, 0, 1, 0, 0),
(284, 284, 0, 1, 0, 1, 0, 0),
(285, 285, 0, 1, 0, 1, 0, 0),
(286, 286, 0, 1, 0, 1, 0, 0),
(287, 287, 0, 1, 0, 1, 0, 0),
(288, 288, 0, 1, 0, 1, 0, 0),
(289, 289, 0, 1, 0, 1, 0, 0),
(290, 290, 0, 1, 0, 1, 0, 0),
(291, 291, 0, 1, 0, 1, 0, 0),
(292, 292, 0, 1, 0, 1, 0, 0),
(293, 293, 0, 1, 0, 1, 0, 0),
(294, 294, 0, 1, 0, 1, 0, 0),
(295, 295, 0, 1, 0, 1, 0, 0),
(296, 296, 0, 1, 0, 1, 0, 0),
(297, 297, 0, 1, 0, 1, 0, 0),
(298, 298, 0, 1, 0, 1, 0, 0),
(299, 299, 0, 1, 0, 1, 0, 0),
(300, 300, 0, 1, 0, 1, 0, 0),
(301, 301, 0, 1, 0, 1, 0, 0),
(302, 302, 0, 1, 0, 1, 0, 0),
(303, 303, 0, 1, 0, 1, 0, 0),
(304, 304, 0, 1, 0, 1, 0, 0),
(305, 305, 0, 1, 0, 1, 0, 0),
(306, 306, 0, 1, 0, 1, 0, 0),
(307, 307, 0, 1, 0, 1, 0, 0),
(308, 308, 0, 1, 0, 1, 0, 0),
(309, 309, 0, 1, 0, 1, 0, 0),
(310, 310, 0, 1, 0, 1, 0, 0),
(311, 311, 0, 1, 0, 1, 0, 0),
(312, 312, 0, 1, 0, 1, 0, 0),
(313, 313, 0, 1, 0, 1, 0, 0),
(314, 314, 0, 1, 0, 1, 0, 0),
(315, 315, 0, 1, 0, 1, 0, 0),
(316, 316, 0, 1, 0, 1, 0, 0),
(317, 317, 0, 1, 0, 1, 0, 0),
(318, 318, 0, 1, 0, 1, 0, 0),
(319, 319, 0, 1, 0, 1, 0, 0),
(320, 320, 0, 1, 0, 1, 0, 0),
(321, 321, 0, 1, 0, 1, 0, 0),
(322, 322, 0, 1, 0, 1, 0, 0),
(323, 323, 0, 1, 0, 1, 0, 0),
(324, 324, 0, 1, 0, 1, 0, 0),
(325, 325, 0, 1, 0, 1, 0, 0),
(326, 326, 0, 1, 0, 1, 0, 0),
(327, 327, 0, 1, 0, 1, 0, 0),
(328, 328, 0, 1, 0, 1, 0, 0),
(329, 329, 0, 1, 0, 1, 0, 0),
(330, 330, 0, 1, 0, 1, 0, 0),
(331, 331, 0, 1, 0, 1, 0, 0),
(332, 332, 0, 1, 0, 1, 0, 0),
(333, 333, 0, 1, 0, 1, 0, 0),
(334, 334, 0, 1, 0, 1, 0, 0),
(335, 335, 0, 1, 0, 1, 0, 0),
(336, 336, 0, 1, 0, 1, 0, 0),
(337, 337, 0, 1, 0, 1, 0, 0),
(338, 338, 0, 1, 0, 1, 0, 0),
(339, 339, 0, 1, 0, 1, 0, 0),
(340, 340, 0, 1, 0, 1, 0, 0),
(341, 341, 0, 1, 0, 1, 0, 0),
(342, 342, 0, 1, 0, 1, 0, 0),
(343, 343, 0, 1, 0, 1, 0, 0),
(344, 344, 0, 1, 0, 1, 0, 0),
(345, 345, 0, 1, 0, 1, 0, 0),
(346, 346, 0, 1, 0, 1, 0, 0),
(347, 347, 0, 1, 0, 1, 0, 0),
(348, 348, 0, 1, 0, 1, 0, 0),
(349, 349, 0, 1, 0, 1, 0, 0),
(350, 350, 0, 1, 0, 1, 0, 0),
(351, 351, 0, 1, 0, 1, 0, 0),
(352, 352, 0, 1, 0, 1, 0, 0),
(353, 353, 0, 1, 0, 1, 0, 0),
(354, 354, 0, 1, 0, 1, 0, 0),
(355, 355, 0, 1, 0, 1, 0, 0),
(356, 356, 0, 1, 0, 1, 0, 0),
(357, 357, 0, 1, 0, 1, 0, 0),
(358, 358, 0, 1, 0, 1, 0, 0),
(359, 359, 0, 1, 0, 1, 0, 0),
(360, 360, 0, 1, 0, 1, 0, 0),
(361, 361, 0, 1, 0, 1, 0, 0),
(362, 362, 0, 1, 0, 1, 0, 0),
(363, 363, 0, 1, 0, 1, 0, 0),
(364, 364, 0, 1, 0, 1, 0, 0),
(365, 365, 0, 1, 0, 1, 0, 0),
(366, 366, 0, 1, 0, 1, 0, 0),
(367, 367, 0, 1, 0, 1, 0, 0),
(368, 368, 0, 1, 0, 1, 0, 0),
(369, 369, 0, 1, 0, 1, 0, 0),
(370, 370, 0, 1, 0, 1, 0, 0),
(371, 371, 0, 1, 0, 1, 0, 0),
(372, 372, 0, 1, 0, 1, 0, 0),
(373, 373, 0, 1, 0, 1, 0, 0),
(374, 374, 0, 1, 0, 1, 0, 0),
(375, 375, 0, 1, 0, 1, 0, 0),
(376, 376, 0, 1, 0, 1, 0, 0),
(377, 377, 0, 1, 0, 1, 0, 0),
(378, 378, 0, 1, 0, 1, 0, 0),
(379, 379, 0, 1, 0, 1, 0, 0),
(380, 380, 0, 1, 0, 1, 0, 0),
(381, 381, 0, 1, 0, 1, 0, 0),
(382, 382, 0, 1, 0, 1, 0, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_stock_mvt`
--

DROP TABLE IF EXISTS `ps_stock_mvt`;
CREATE TABLE `ps_stock_mvt` (
  `id_stock_mvt` bigint(20) UNSIGNED NOT NULL,
  `id_stock` int(11) UNSIGNED NOT NULL,
  `id_order` int(11) UNSIGNED DEFAULT NULL,
  `id_supply_order` int(11) UNSIGNED DEFAULT NULL,
  `id_stock_mvt_reason` int(11) UNSIGNED NOT NULL,
  `id_employee` int(11) UNSIGNED NOT NULL,
  `employee_lastname` varchar(32) DEFAULT '',
  `employee_firstname` varchar(32) DEFAULT '',
  `physical_quantity` int(11) UNSIGNED NOT NULL,
  `date_add` datetime NOT NULL,
  `sign` tinyint(1) NOT NULL DEFAULT 1,
  `price_te` decimal(20,6) DEFAULT 0.000000,
  `last_wa` decimal(20,6) DEFAULT 0.000000,
  `current_wa` decimal(20,6) DEFAULT 0.000000,
  `referer` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_stock_mvt_reason`
--

DROP TABLE IF EXISTS `ps_stock_mvt_reason`;
CREATE TABLE `ps_stock_mvt_reason` (
  `id_stock_mvt_reason` int(11) UNSIGNED NOT NULL,
  `sign` tinyint(1) NOT NULL DEFAULT 1,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_stock_mvt_reason`
--

INSERT INTO `ps_stock_mvt_reason` (`id_stock_mvt_reason`, `sign`, `date_add`, `date_upd`, `deleted`) VALUES
(1, 1, '2018-12-03 17:18:02', '2018-12-03 17:18:02', 0),
(2, -1, '2018-12-03 17:18:02', '2018-12-03 17:18:02', 0),
(3, -1, '2018-12-03 17:18:02', '2018-12-03 17:18:02', 0),
(4, -1, '2018-12-03 17:18:02', '2018-12-03 17:18:02', 0),
(5, 1, '2018-12-03 17:18:02', '2018-12-03 17:18:02', 0),
(6, -1, '2018-12-03 17:18:02', '2018-12-03 17:18:02', 0),
(7, 1, '2018-12-03 17:18:02', '2018-12-03 17:18:02', 0),
(8, 1, '2018-12-03 17:18:02', '2018-12-03 17:18:02', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_stock_mvt_reason_lang`
--

DROP TABLE IF EXISTS `ps_stock_mvt_reason_lang`;
CREATE TABLE `ps_stock_mvt_reason_lang` (
  `id_stock_mvt_reason` int(11) UNSIGNED NOT NULL,
  `id_lang` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_stock_mvt_reason_lang`
--

INSERT INTO `ps_stock_mvt_reason_lang` (`id_stock_mvt_reason`, `id_lang`, `name`) VALUES
(1, 1, 'Wzrost'),
(2, 1, 'Spadek'),
(3, 1, 'ZamĂłwienie klienta'),
(4, 1, 'RozporzÄdzenie dotyczÄce inwentaryzacji zapasĂłw'),
(5, 1, 'RozporzÄdzenie dotyczÄce inwentaryzacji zapasĂłw'),
(6, 1, 'Przeniesienie do innego magazynu'),
(7, 1, 'Przeniesienie z innego magazynu'),
(8, 1, 'ZamĂłwienie dostawcy');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_store`
--

DROP TABLE IF EXISTS `ps_store`;
CREATE TABLE `ps_store` (
  `id_store` int(10) UNSIGNED NOT NULL,
  `id_country` int(10) UNSIGNED NOT NULL,
  `id_state` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `address1` varchar(128) NOT NULL,
  `address2` varchar(128) DEFAULT NULL,
  `city` varchar(64) NOT NULL,
  `postcode` varchar(12) NOT NULL,
  `latitude` decimal(13,8) DEFAULT NULL,
  `longitude` decimal(13,8) DEFAULT NULL,
  `hours` varchar(254) DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `fax` varchar(16) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `note` text DEFAULT NULL,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_store`
--

INSERT INTO `ps_store` (`id_store`, `id_country`, `id_state`, `name`, `address1`, `address2`, `city`, `postcode`, `latitude`, `longitude`, `hours`, `phone`, `fax`, `email`, `note`, `active`, `date_add`, `date_upd`) VALUES
(1, 21, 9, 'Dade County', '3030 SW 8th St Miami', '', 'Miami', ' 33135', '25.76500500', '-80.24379700', 'a:7:{i:0;s:13:\"09:00 - 19:00\";i:1;s:13:\"09:00 - 19:00\";i:2;s:13:\"09:00 - 19:00\";i:3;s:13:\"09:00 - 19:00\";i:4;s:13:\"09:00 - 19:00\";i:5;s:13:\"10:00 - 16:00\";i:6;s:13:\"10:00 - 16:00\";}', '', '', '', '', 1, '2018-12-03 17:18:16', '2018-12-03 17:18:16'),
(2, 21, 9, 'E Fort Lauderdale', '1000 Northeast 4th Ave Fort Lauderdale', '', 'Miami', ' 33304', '26.13793600', '-80.13943500', 'a:7:{i:0;s:13:\"09:00 - 19:00\";i:1;s:13:\"09:00 - 19:00\";i:2;s:13:\"09:00 - 19:00\";i:3;s:13:\"09:00 - 19:00\";i:4;s:13:\"09:00 - 19:00\";i:5;s:13:\"10:00 - 16:00\";i:6;s:13:\"10:00 - 16:00\";}', '', '', '', '', 1, '2018-12-03 17:18:16', '2018-12-03 17:18:16'),
(3, 21, 9, 'Pembroke Pines', '11001 Pines Blvd Pembroke Pines', '', 'Miami', '33026', '26.00998700', '-80.29447200', 'a:7:{i:0;s:13:\"09:00 - 19:00\";i:1;s:13:\"09:00 - 19:00\";i:2;s:13:\"09:00 - 19:00\";i:3;s:13:\"09:00 - 19:00\";i:4;s:13:\"09:00 - 19:00\";i:5;s:13:\"10:00 - 16:00\";i:6;s:13:\"10:00 - 16:00\";}', '', '', '', '', 1, '2018-12-03 17:18:16', '2018-12-03 17:18:16'),
(4, 21, 9, 'Coconut Grove', '2999 SW 32nd Avenue', '', 'Miami', ' 33133', '25.73629600', '-80.24479700', 'a:7:{i:0;s:13:\"09:00 - 19:00\";i:1;s:13:\"09:00 - 19:00\";i:2;s:13:\"09:00 - 19:00\";i:3;s:13:\"09:00 - 19:00\";i:4;s:13:\"09:00 - 19:00\";i:5;s:13:\"10:00 - 16:00\";i:6;s:13:\"10:00 - 16:00\";}', '', '', '', '', 1, '2018-12-03 17:18:16', '2018-12-03 17:18:16'),
(5, 21, 9, 'N Miami/Biscayne', '12055 Biscayne Blvd', '', 'Miami', '33181', '25.88674000', '-80.16329200', 'a:7:{i:0;s:13:\"09:00 - 19:00\";i:1;s:13:\"09:00 - 19:00\";i:2;s:13:\"09:00 - 19:00\";i:3;s:13:\"09:00 - 19:00\";i:4;s:13:\"09:00 - 19:00\";i:5;s:13:\"10:00 - 16:00\";i:6;s:13:\"10:00 - 16:00\";}', '', '', '', '', 1, '2018-12-03 17:18:16', '2018-12-03 17:18:16');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_store_shop`
--

DROP TABLE IF EXISTS `ps_store_shop`;
CREATE TABLE `ps_store_shop` (
  `id_store` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_store_shop`
--

INSERT INTO `ps_store_shop` (`id_store`, `id_shop`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_supplier`
--

DROP TABLE IF EXISTS `ps_supplier`;
CREATE TABLE `ps_supplier` (
  `id_supplier` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_supplier_lang`
--

DROP TABLE IF EXISTS `ps_supplier_lang`;
CREATE TABLE `ps_supplier_lang` (
  `id_supplier` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `description` text DEFAULT NULL,
  `meta_title` varchar(128) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_supplier_shop`
--

DROP TABLE IF EXISTS `ps_supplier_shop`;
CREATE TABLE `ps_supplier_shop` (
  `id_supplier` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_supply_order`
--

DROP TABLE IF EXISTS `ps_supply_order`;
CREATE TABLE `ps_supply_order` (
  `id_supply_order` int(11) UNSIGNED NOT NULL,
  `id_supplier` int(11) UNSIGNED NOT NULL,
  `supplier_name` varchar(64) NOT NULL,
  `id_lang` int(11) UNSIGNED NOT NULL,
  `id_warehouse` int(11) UNSIGNED NOT NULL,
  `id_supply_order_state` int(11) UNSIGNED NOT NULL,
  `id_currency` int(11) UNSIGNED NOT NULL,
  `id_ref_currency` int(11) UNSIGNED NOT NULL,
  `reference` varchar(64) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `date_delivery_expected` datetime DEFAULT NULL,
  `total_te` decimal(20,6) DEFAULT 0.000000,
  `total_with_discount_te` decimal(20,6) DEFAULT 0.000000,
  `total_tax` decimal(20,6) DEFAULT 0.000000,
  `total_ti` decimal(20,6) DEFAULT 0.000000,
  `discount_rate` decimal(20,6) DEFAULT 0.000000,
  `discount_value_te` decimal(20,6) DEFAULT 0.000000,
  `is_template` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_supply_order_detail`
--

DROP TABLE IF EXISTS `ps_supply_order_detail`;
CREATE TABLE `ps_supply_order_detail` (
  `id_supply_order_detail` int(11) UNSIGNED NOT NULL,
  `id_supply_order` int(11) UNSIGNED NOT NULL,
  `id_currency` int(11) UNSIGNED NOT NULL,
  `id_product` int(11) UNSIGNED NOT NULL,
  `id_product_attribute` int(11) UNSIGNED NOT NULL,
  `reference` varchar(32) NOT NULL,
  `supplier_reference` varchar(32) NOT NULL,
  `name` varchar(128) NOT NULL,
  `ean13` varchar(13) DEFAULT NULL,
  `upc` varchar(12) DEFAULT NULL,
  `exchange_rate` decimal(20,6) DEFAULT 0.000000,
  `unit_price_te` decimal(20,6) DEFAULT 0.000000,
  `quantity_expected` int(11) UNSIGNED NOT NULL,
  `quantity_received` int(11) UNSIGNED NOT NULL,
  `price_te` decimal(20,6) DEFAULT 0.000000,
  `discount_rate` decimal(20,6) DEFAULT 0.000000,
  `discount_value_te` decimal(20,6) DEFAULT 0.000000,
  `price_with_discount_te` decimal(20,6) DEFAULT 0.000000,
  `tax_rate` decimal(20,6) DEFAULT 0.000000,
  `tax_value` decimal(20,6) DEFAULT 0.000000,
  `price_ti` decimal(20,6) DEFAULT 0.000000,
  `tax_value_with_order_discount` decimal(20,6) DEFAULT 0.000000,
  `price_with_order_discount_te` decimal(20,6) DEFAULT 0.000000
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_supply_order_history`
--

DROP TABLE IF EXISTS `ps_supply_order_history`;
CREATE TABLE `ps_supply_order_history` (
  `id_supply_order_history` int(11) UNSIGNED NOT NULL,
  `id_supply_order` int(11) UNSIGNED NOT NULL,
  `id_employee` int(11) UNSIGNED NOT NULL,
  `employee_lastname` varchar(32) DEFAULT '',
  `employee_firstname` varchar(32) DEFAULT '',
  `id_state` int(11) UNSIGNED NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_supply_order_receipt_history`
--

DROP TABLE IF EXISTS `ps_supply_order_receipt_history`;
CREATE TABLE `ps_supply_order_receipt_history` (
  `id_supply_order_receipt_history` int(11) UNSIGNED NOT NULL,
  `id_supply_order_detail` int(11) UNSIGNED NOT NULL,
  `id_employee` int(11) UNSIGNED NOT NULL,
  `employee_lastname` varchar(32) DEFAULT '',
  `employee_firstname` varchar(32) DEFAULT '',
  `id_supply_order_state` int(11) UNSIGNED NOT NULL,
  `quantity` int(11) UNSIGNED NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_supply_order_state`
--

DROP TABLE IF EXISTS `ps_supply_order_state`;
CREATE TABLE `ps_supply_order_state` (
  `id_supply_order_state` int(11) UNSIGNED NOT NULL,
  `delivery_note` tinyint(1) NOT NULL DEFAULT 0,
  `editable` tinyint(1) NOT NULL DEFAULT 0,
  `receipt_state` tinyint(1) NOT NULL DEFAULT 0,
  `pending_receipt` tinyint(1) NOT NULL DEFAULT 0,
  `enclosed` tinyint(1) NOT NULL DEFAULT 0,
  `color` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_supply_order_state`
--

INSERT INTO `ps_supply_order_state` (`id_supply_order_state`, `delivery_note`, `editable`, `receipt_state`, `pending_receipt`, `enclosed`, `color`) VALUES
(1, 0, 1, 0, 0, 0, '#faab00'),
(2, 1, 0, 0, 0, 0, '#273cff'),
(3, 0, 0, 0, 1, 0, '#ff37f5'),
(4, 0, 0, 1, 1, 0, '#ff3e33'),
(5, 0, 0, 1, 0, 1, '#00d60c'),
(6, 0, 0, 0, 0, 1, '#666666');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_supply_order_state_lang`
--

DROP TABLE IF EXISTS `ps_supply_order_state_lang`;
CREATE TABLE `ps_supply_order_state_lang` (
  `id_supply_order_state` int(11) UNSIGNED NOT NULL,
  `id_lang` int(11) UNSIGNED NOT NULL,
  `name` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_supply_order_state_lang`
--

INSERT INTO `ps_supply_order_state_lang` (`id_supply_order_state`, `id_lang`, `name`) VALUES
(1, 1, '1 - Tworzenie w toku'),
(2, 1, '2 - ZamĂłwienie zostaĹo zatwierdzone'),
(3, 1, '3 - W oczekiwaniu '),
(4, 1, '4 - ZamĂłwienie zostaĹo otrzymane w czÄĹciach'),
(5, 1, '5 - Otrzymano zamĂłwienie '),
(6, 1, '6 - ZamĂłwienie zostaĹo anulowane');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_tab`
--

DROP TABLE IF EXISTS `ps_tab`;
CREATE TABLE `ps_tab` (
  `id_tab` int(10) UNSIGNED NOT NULL,
  `id_parent` int(11) NOT NULL,
  `class_name` varchar(64) NOT NULL,
  `module` varchar(64) DEFAULT NULL,
  `position` int(10) UNSIGNED NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `hide_host_mode` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_tab`
--

INSERT INTO `ps_tab` (`id_tab`, `id_parent`, `class_name`, `module`, `position`, `active`, `hide_host_mode`) VALUES
(1, 0, 'AdminDashboard', '', 0, 1, 0),
(2, -1, 'AdminCms', '', 0, 1, 0),
(3, -1, 'AdminCmsCategories', '', 1, 1, 0),
(4, -1, 'AdminAttributeGenerator', '', 2, 1, 0),
(5, -1, 'AdminSearch', '', 3, 1, 0),
(6, -1, 'AdminLogin', '', 4, 1, 0),
(7, -1, 'AdminShop', '', 5, 1, 0),
(8, -1, 'AdminShopUrl', '', 6, 1, 0),
(9, 0, 'AdminCatalog', '', 1, 1, 0),
(10, 0, 'AdminParentOrders', '', 2, 1, 0),
(11, 0, 'AdminParentCustomer', '', 3, 1, 0),
(12, 0, 'AdminPriceRule', '', 4, 1, 0),
(13, 0, 'AdminParentModules', '', 5, 1, 0),
(14, 0, 'AdminParentShipping', '', 6, 1, 0),
(15, 0, 'AdminParentLocalization', '', 7, 1, 0),
(16, 0, 'AdminParentPreferences', '', 8, 1, 0),
(17, 0, 'AdminTools', '', 9, 1, 0),
(18, 0, 'AdminAdmin', '', 10, 1, 0),
(19, 0, 'AdminParentStats', '', 11, 1, 0),
(20, 0, 'AdminStock', '', 12, 1, 0),
(21, 9, 'AdminProducts', '', 0, 1, 0),
(22, 9, 'AdminCategories', '', 1, 1, 0),
(23, 9, 'AdminTracking', '', 2, 1, 0),
(24, 9, 'AdminAttributesGroups', '', 3, 1, 0),
(25, 9, 'AdminFeatures', '', 4, 1, 0),
(26, 9, 'AdminManufacturers', '', 5, 1, 0),
(27, 9, 'AdminSuppliers', '', 6, 1, 0),
(28, 9, 'AdminTags', '', 7, 1, 0),
(29, 9, 'AdminAttachments', '', 8, 1, 0),
(30, 10, 'AdminOrders', '', 0, 1, 0),
(31, 10, 'AdminInvoices', '', 1, 1, 0),
(32, 10, 'AdminReturn', '', 2, 1, 0),
(33, 10, 'AdminDeliverySlip', '', 3, 1, 0),
(34, 10, 'AdminSlip', '', 4, 1, 0),
(35, 10, 'AdminStatuses', '', 5, 1, 0),
(36, 10, 'AdminOrderMessage', '', 6, 1, 0),
(37, 11, 'AdminCustomers', '', 0, 1, 0),
(38, 11, 'AdminAddresses', '', 1, 1, 0),
(39, 11, 'AdminGroups', '', 2, 1, 0),
(40, 11, 'AdminCarts', '', 3, 1, 0),
(41, 11, 'AdminCustomerThreads', '', 4, 1, 0),
(42, 11, 'AdminContacts', '', 5, 1, 0),
(43, 11, 'AdminGenders', '', 6, 1, 0),
(44, 11, 'AdminOutstanding', '', 7, 0, 0),
(45, 12, 'AdminCartRules', '', 0, 1, 0),
(46, 12, 'AdminSpecificPriceRule', '', 1, 1, 0),
(47, 12, 'AdminMarketing', '', 2, 1, 0),
(48, 14, 'AdminCarriers', '', 0, 1, 0),
(49, 14, 'AdminShipping', '', 1, 1, 0),
(50, 14, 'AdminCarrierWizard', '', 2, 1, 0),
(51, 15, 'AdminLocalization', '', 0, 1, 0),
(52, 15, 'AdminLanguages', '', 1, 1, 0),
(53, 15, 'AdminZones', '', 2, 1, 0),
(54, 15, 'AdminCountries', '', 3, 1, 0),
(55, 15, 'AdminStates', '', 4, 1, 0),
(56, 15, 'AdminCurrencies', '', 5, 1, 0),
(57, 15, 'AdminTaxes', '', 6, 1, 0),
(58, 15, 'AdminTaxRulesGroup', '', 7, 1, 0),
(59, 15, 'AdminTranslations', '', 8, 1, 0),
(60, 13, 'AdminModules', '', 0, 1, 0),
(61, 13, 'AdminAddonsCatalog', '', 1, 1, 0),
(62, 13, 'AdminModulesPositions', '', 2, 1, 0),
(63, 13, 'AdminPayment', '', 3, 1, 0),
(64, 16, 'AdminPreferences', '', 0, 1, 0),
(65, 16, 'AdminOrderPreferences', '', 1, 1, 0),
(66, 16, 'AdminPPreferences', '', 2, 1, 0),
(67, 16, 'AdminCustomerPreferences', '', 3, 1, 0),
(68, 16, 'AdminThemes', '', 4, 1, 0),
(69, 16, 'AdminMeta', '', 5, 1, 0),
(70, 16, 'AdminCmsContent', '', 6, 1, 0),
(71, 16, 'AdminImages', '', 7, 1, 0),
(72, 16, 'AdminStores', '', 8, 1, 0),
(73, 16, 'AdminSearchConf', '', 9, 1, 0),
(74, 16, 'AdminMaintenance', '', 10, 1, 0),
(75, 16, 'AdminGeolocation', '', 11, 1, 0),
(76, 17, 'AdminInformation', '', 0, 1, 0),
(77, 17, 'AdminPerformance', '', 1, 1, 0),
(78, 17, 'AdminEmails', '', 2, 1, 0),
(79, 17, 'AdminShopGroup', '', 3, 0, 0),
(80, 17, 'AdminImport', '', 4, 1, 0),
(81, 17, 'AdminBackup', '', 5, 1, 0),
(82, 17, 'AdminRequestSql', '', 6, 1, 0),
(83, 17, 'AdminLogs', '', 7, 1, 0),
(84, 17, 'AdminWebservice', '', 8, 1, 0),
(85, 18, 'AdminAdminPreferences', '', 0, 1, 0),
(86, 18, 'AdminQuickAccesses', '', 1, 1, 0),
(87, 18, 'AdminEmployees', '', 2, 1, 0),
(88, 18, 'AdminProfiles', '', 3, 1, 0),
(89, 18, 'AdminAccess', '', 4, 1, 0),
(90, 18, 'AdminTabs', '', 5, 1, 0),
(91, 19, 'AdminStats', '', 0, 1, 0),
(92, 19, 'AdminSearchEngines', '', 1, 1, 0),
(93, 19, 'AdminReferrers', '', 2, 1, 0),
(94, 20, 'AdminWarehouses', '', 0, 1, 0),
(95, 20, 'AdminStockManagement', '', 1, 1, 0),
(96, 20, 'AdminStockMvt', '', 2, 1, 0),
(97, 20, 'AdminStockInstantState', '', 3, 1, 0),
(98, 20, 'AdminStockCover', '', 4, 1, 0),
(99, 20, 'AdminSupplyOrders', '', 5, 1, 0),
(100, 20, 'AdminStockConfiguration', '', 6, 1, 0),
(101, -1, 'AdminBlockCategories', 'blockcategories', 7, 1, 0),
(102, -1, 'AdminDashgoals', 'dashgoals', 8, 1, 0),
(103, -1, 'AdminThemeConfigurator', 'themeconfigurator', 9, 1, 0),
(104, 18, 'AdminGamification', 'gamification', 6, 1, 0),
(105, -1, 'AdminCronJobs', 'cronjobs', 10, 1, 0),
(106, -1, 'AdminGanalyticsAjax', 'ganalytics', 11, 0, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_tab_advice`
--

DROP TABLE IF EXISTS `ps_tab_advice`;
CREATE TABLE `ps_tab_advice` (
  `id_tab` int(11) NOT NULL,
  `id_advice` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_tab_lang`
--

DROP TABLE IF EXISTS `ps_tab_lang`;
CREATE TABLE `ps_tab_lang` (
  `id_tab` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_tab_lang`
--

INSERT INTO `ps_tab_lang` (`id_tab`, `id_lang`, `name`) VALUES
(1, 1, 'Pulpit'),
(2, 1, 'Strony CMS'),
(3, 1, 'Kategorie CMS'),
(4, 1, 'Generator kombinacji'),
(5, 1, 'Szukaj'),
(6, 1, 'Nazwa użytkowika'),
(7, 1, 'Sklepy'),
(8, 1, 'Url sklepu'),
(9, 1, 'Katalog'),
(10, 1, 'Zamówienia'),
(11, 1, 'Klienci'),
(12, 1, 'Rabaty grupowe'),
(13, 1, 'Moduły i usługi'),
(14, 1, 'Wysyłka'),
(15, 1, 'Lokalizacja'),
(16, 1, 'Preferencje'),
(17, 1, 'Zaawansowane'),
(18, 1, 'Administracja'),
(19, 1, 'Statystyki'),
(20, 1, 'Magazyn'),
(21, 1, 'Produkty'),
(22, 1, 'Kategorie'),
(23, 1, 'Monitorowanie'),
(24, 1, 'Atrybuty produktu'),
(25, 1, 'Cechy produktu'),
(26, 1, 'Producenci'),
(27, 1, 'Dostawcy'),
(28, 1, 'Tagi'),
(29, 1, 'Załączniki'),
(30, 1, 'Zamówienia'),
(31, 1, 'Faktury'),
(32, 1, 'Zwroty produktów'),
(33, 1, 'Druk wysyłki'),
(34, 1, 'Druki kredytowe'),
(35, 1, 'Statusy'),
(36, 1, 'Wiadomości zamówienia'),
(37, 1, 'Klienci'),
(38, 1, 'Adresy'),
(39, 1, 'Grupy'),
(40, 1, 'Koszyki zakupowe'),
(41, 1, 'Obsługa klienta'),
(42, 1, 'Kontakty'),
(43, 1, 'Tytuły'),
(44, 1, 'Saldo'),
(45, 1, 'Kody rabatowe'),
(46, 1, 'Reguły cenowe katalogu'),
(47, 1, 'Marketing'),
(48, 1, 'Przewoźnicy'),
(49, 1, 'Preferencje'),
(50, 1, 'Przewoźnik'),
(51, 1, 'Lokalizacja'),
(52, 1, 'Języki'),
(53, 1, 'Strefy'),
(54, 1, 'Kraje'),
(55, 1, 'Województwa lub regiony'),
(56, 1, 'Waluty'),
(57, 1, 'Podatki'),
(58, 1, 'Reguły podatków'),
(59, 1, 'Tłumaczenia'),
(60, 1, 'Moduły i usługi'),
(61, 1, 'Katalog modułów i motywów'),
(62, 1, 'Pozycje'),
(63, 1, 'Płatność'),
(64, 1, 'Ogólny'),
(65, 1, 'Zamówienia'),
(66, 1, 'Produkty'),
(67, 1, 'Klienci'),
(68, 1, 'Szablony'),
(69, 1, 'SEO & URL'),
(70, 1, 'CMS'),
(71, 1, 'Zdjęcia'),
(72, 1, 'Kontakty sklepu'),
(73, 1, 'Szukaj'),
(74, 1, 'Przerwa techniczna'),
(75, 1, 'Geolokalizacja'),
(76, 1, 'Informacje konfiguracyjne'),
(77, 1, 'Wydajność'),
(78, 1, 'Adres e-mail'),
(79, 1, 'Multisklep'),
(80, 1, 'Import CSV'),
(81, 1, 'Kopia zapasowa DB'),
(82, 1, 'Menadżer SQL'),
(83, 1, 'Logi'),
(84, 1, 'API'),
(85, 1, 'Preferencje'),
(86, 1, 'Szybki dostęp'),
(87, 1, 'Pracownicy'),
(88, 1, 'Profile'),
(89, 1, 'Uprawnienia'),
(90, 1, 'Menu'),
(91, 1, 'Statystyki'),
(92, 1, 'Wyszukiwarki'),
(93, 1, 'Polecający'),
(94, 1, 'Magazyny'),
(95, 1, 'Zarządzanie magazynem'),
(96, 1, 'Ruch magazynowy'),
(97, 1, 'Stany magazynowe'),
(98, 1, 'Aktualne pokrycie stanu'),
(99, 1, 'Dostawa zamówień'),
(100, 1, 'Konfiguracja'),
(101, 1, 'Blok kategorii'),
(102, 1, 'Dashgoals'),
(103, 1, 'konfigurator szablonu'),
(104, 1, 'Osiągnięcia sprzedawcy'),
(105, 1, 'Zadania Cron'),
(106, 1, 'Google Analytics Ajax');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_tab_module_preference`
--

DROP TABLE IF EXISTS `ps_tab_module_preference`;
CREATE TABLE `ps_tab_module_preference` (
  `id_tab_module_preference` int(11) NOT NULL,
  `id_employee` int(11) NOT NULL,
  `id_tab` int(11) NOT NULL,
  `module` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_tag`
--

DROP TABLE IF EXISTS `ps_tag`;
CREATE TABLE `ps_tag` (
  `id_tag` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_tag_count`
--

DROP TABLE IF EXISTS `ps_tag_count`;
CREATE TABLE `ps_tag_count` (
  `id_group` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `id_tag` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `id_lang` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `counter` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_tax`
--

DROP TABLE IF EXISTS `ps_tax`;
CREATE TABLE `ps_tax` (
  `id_tax` int(10) UNSIGNED NOT NULL,
  `rate` decimal(10,3) NOT NULL,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_tax`
--

INSERT INTO `ps_tax` (`id_tax`, `rate`, `active`, `deleted`) VALUES
(1, '23.000', 1, 0),
(2, '8.000', 1, 0),
(3, '5.000', 1, 0),
(4, '0.000', 1, 0),
(5, '20.000', 1, 0),
(6, '21.000', 1, 0),
(7, '20.000', 1, 0),
(8, '19.000', 1, 0),
(9, '21.000', 1, 0),
(10, '19.000', 1, 0),
(11, '25.000', 1, 0),
(12, '20.000', 1, 0),
(13, '21.000', 1, 0),
(14, '24.000', 1, 0),
(15, '20.000', 1, 0),
(16, '20.000', 1, 0),
(17, '23.000', 1, 0),
(18, '25.000', 1, 0),
(19, '27.000', 1, 0),
(20, '23.000', 1, 0),
(21, '22.000', 1, 0),
(22, '21.000', 1, 0),
(23, '17.000', 1, 0),
(24, '21.000', 1, 0),
(25, '18.000', 1, 0),
(26, '21.000', 1, 0),
(27, '23.000', 1, 0),
(28, '20.000', 1, 0),
(29, '25.000', 1, 0),
(30, '22.000', 1, 0),
(31, '20.000', 1, 0),
(32, '20.000', 1, 0),
(33, '19.000', 1, 0),
(34, '23.000', 1, 0),
(35, '27.000', 1, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_tax_lang`
--

DROP TABLE IF EXISTS `ps_tax_lang`;
CREATE TABLE `ps_tax_lang` (
  `id_tax` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_tax_lang`
--

INSERT INTO `ps_tax_lang` (`id_tax`, `id_lang`, `name`) VALUES
(1, 1, 'PTU PL 23%'),
(2, 1, 'PTU PL 8%'),
(3, 1, 'PTU PL 5%'),
(4, 1, 'PTU PL 0'),
(5, 1, 'USt. AT 20%'),
(6, 1, 'TVA BE 21%'),
(7, 1, 'ĐĐĐĄ BG 20%'),
(8, 1, 'ÎŚÎ Î CY 19%'),
(9, 1, 'DPH CZ 21%'),
(10, 1, 'MwSt. DE 19%'),
(11, 1, 'moms DK 25%'),
(12, 1, 'km EE 20%'),
(13, 1, 'IVA ES 21%'),
(14, 1, 'ALV FI 24%'),
(15, 1, 'TVA FR 20%'),
(16, 1, 'VAT UK 20%'),
(17, 1, 'ÎŚÎ Î GR 23%'),
(18, 1, 'Croatia PDV 25%'),
(19, 1, 'ĂFA HU 27%'),
(20, 1, 'VAT IE 23%'),
(21, 1, 'IVA IT 22%'),
(22, 1, 'PVM LT 21%'),
(23, 1, 'TVA LU 17%'),
(24, 1, 'PVN LV 21%'),
(25, 1, 'VAT MT 18%'),
(26, 1, 'BTW NL 21%'),
(27, 1, 'IVA PT 23%'),
(28, 1, 'TVA RO 20%'),
(29, 1, 'Moms SE 25%'),
(30, 1, 'DDV SI 22%'),
(31, 1, 'DPH SK 20%'),
(32, 1, 'ДДС BG 20%'),
(33, 1, 'ΦΠΑ CY 19%'),
(34, 1, 'ΦΠΑ GR 23%'),
(35, 1, 'ÁFA HU 27%');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_tax_rule`
--

DROP TABLE IF EXISTS `ps_tax_rule`;
CREATE TABLE `ps_tax_rule` (
  `id_tax_rule` int(11) NOT NULL,
  `id_tax_rules_group` int(11) NOT NULL,
  `id_country` int(11) NOT NULL,
  `id_state` int(11) NOT NULL,
  `zipcode_from` varchar(12) NOT NULL,
  `zipcode_to` varchar(12) NOT NULL,
  `id_tax` int(11) NOT NULL,
  `behavior` int(11) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_tax_rule`
--

INSERT INTO `ps_tax_rule` (`id_tax_rule`, `id_tax_rules_group`, `id_country`, `id_state`, `zipcode_from`, `zipcode_to`, `id_tax`, `behavior`, `description`) VALUES
(1, 1, 3, 0, '0', '0', 1, 0, ''),
(2, 1, 236, 0, '0', '0', 1, 0, ''),
(3, 1, 16, 0, '0', '0', 1, 0, ''),
(4, 1, 20, 0, '0', '0', 1, 0, ''),
(5, 1, 1, 0, '0', '0', 1, 0, ''),
(6, 1, 86, 0, '0', '0', 1, 0, ''),
(7, 1, 9, 0, '0', '0', 1, 0, ''),
(8, 1, 6, 0, '0', '0', 1, 0, ''),
(9, 1, 8, 0, '0', '0', 1, 0, ''),
(10, 1, 26, 0, '0', '0', 1, 0, ''),
(11, 1, 10, 0, '0', '0', 1, 0, ''),
(12, 1, 76, 0, '0', '0', 1, 0, ''),
(13, 1, 125, 0, '0', '0', 1, 0, ''),
(14, 1, 131, 0, '0', '0', 1, 0, ''),
(15, 1, 12, 0, '0', '0', 1, 0, ''),
(16, 1, 143, 0, '0', '0', 1, 0, ''),
(17, 1, 139, 0, '0', '0', 1, 0, ''),
(18, 1, 13, 0, '0', '0', 1, 0, ''),
(19, 1, 2, 0, '0', '0', 1, 0, ''),
(20, 1, 14, 0, '0', '0', 1, 0, ''),
(21, 1, 15, 0, '0', '0', 1, 0, ''),
(22, 1, 36, 0, '0', '0', 1, 0, ''),
(23, 1, 193, 0, '0', '0', 1, 0, ''),
(24, 1, 37, 0, '0', '0', 1, 0, ''),
(25, 1, 7, 0, '0', '0', 1, 0, ''),
(26, 1, 18, 0, '0', '0', 1, 0, ''),
(27, 2, 3, 0, '0', '0', 2, 0, ''),
(28, 2, 236, 0, '0', '0', 2, 0, ''),
(29, 2, 16, 0, '0', '0', 2, 0, ''),
(30, 2, 20, 0, '0', '0', 2, 0, ''),
(31, 2, 1, 0, '0', '0', 2, 0, ''),
(32, 2, 86, 0, '0', '0', 2, 0, ''),
(33, 2, 9, 0, '0', '0', 2, 0, ''),
(34, 2, 6, 0, '0', '0', 2, 0, ''),
(35, 2, 8, 0, '0', '0', 2, 0, ''),
(36, 2, 26, 0, '0', '0', 2, 0, ''),
(37, 2, 10, 0, '0', '0', 2, 0, ''),
(38, 2, 76, 0, '0', '0', 2, 0, ''),
(39, 2, 125, 0, '0', '0', 2, 0, ''),
(40, 2, 131, 0, '0', '0', 2, 0, ''),
(41, 2, 12, 0, '0', '0', 2, 0, ''),
(42, 2, 143, 0, '0', '0', 2, 0, ''),
(43, 2, 139, 0, '0', '0', 2, 0, ''),
(44, 2, 13, 0, '0', '0', 2, 0, ''),
(45, 2, 2, 0, '0', '0', 2, 0, ''),
(46, 2, 14, 0, '0', '0', 2, 0, ''),
(47, 2, 15, 0, '0', '0', 2, 0, ''),
(48, 2, 36, 0, '0', '0', 2, 0, ''),
(49, 2, 193, 0, '0', '0', 2, 0, ''),
(50, 2, 37, 0, '0', '0', 2, 0, ''),
(51, 2, 7, 0, '0', '0', 2, 0, ''),
(52, 2, 18, 0, '0', '0', 2, 0, ''),
(53, 3, 3, 0, '0', '0', 3, 0, ''),
(54, 3, 236, 0, '0', '0', 3, 0, ''),
(55, 3, 16, 0, '0', '0', 3, 0, ''),
(56, 3, 20, 0, '0', '0', 3, 0, ''),
(57, 3, 1, 0, '0', '0', 3, 0, ''),
(58, 3, 86, 0, '0', '0', 3, 0, ''),
(59, 3, 9, 0, '0', '0', 3, 0, ''),
(60, 3, 6, 0, '0', '0', 3, 0, ''),
(61, 3, 8, 0, '0', '0', 3, 0, ''),
(62, 3, 10, 0, '0', '0', 3, 0, ''),
(63, 3, 76, 0, '0', '0', 3, 0, ''),
(64, 3, 125, 0, '0', '0', 3, 0, ''),
(65, 3, 131, 0, '0', '0', 3, 0, ''),
(66, 3, 12, 0, '0', '0', 3, 0, ''),
(67, 3, 143, 0, '0', '0', 3, 0, ''),
(68, 3, 139, 0, '0', '0', 3, 0, ''),
(69, 3, 13, 0, '0', '0', 3, 0, ''),
(70, 3, 2, 0, '0', '0', 3, 0, ''),
(71, 3, 14, 0, '0', '0', 3, 0, ''),
(72, 3, 15, 0, '0', '0', 3, 0, ''),
(73, 3, 36, 0, '0', '0', 3, 0, ''),
(74, 3, 193, 0, '0', '0', 3, 0, ''),
(75, 3, 37, 0, '0', '0', 3, 0, ''),
(76, 3, 7, 0, '0', '0', 3, 0, ''),
(77, 3, 18, 0, '0', '0', 3, 0, ''),
(78, 4, 3, 0, '0', '0', 4, 0, ''),
(79, 4, 236, 0, '0', '0', 4, 0, ''),
(80, 4, 16, 0, '0', '0', 4, 0, ''),
(81, 4, 20, 0, '0', '0', 4, 0, ''),
(82, 4, 1, 0, '0', '0', 4, 0, ''),
(83, 4, 86, 0, '0', '0', 4, 0, ''),
(84, 4, 9, 0, '0', '0', 4, 0, ''),
(85, 4, 6, 0, '0', '0', 4, 0, ''),
(86, 4, 8, 0, '0', '0', 4, 0, ''),
(87, 4, 10, 0, '0', '0', 4, 0, ''),
(88, 4, 76, 0, '0', '0', 4, 0, ''),
(89, 4, 125, 0, '0', '0', 4, 0, ''),
(90, 4, 131, 0, '0', '0', 4, 0, ''),
(91, 4, 12, 0, '0', '0', 4, 0, ''),
(92, 4, 143, 0, '0', '0', 4, 0, ''),
(93, 4, 139, 0, '0', '0', 4, 0, ''),
(94, 4, 13, 0, '0', '0', 4, 0, ''),
(95, 4, 2, 0, '0', '0', 4, 0, ''),
(96, 4, 14, 0, '0', '0', 4, 0, ''),
(97, 4, 15, 0, '0', '0', 4, 0, ''),
(98, 4, 36, 0, '0', '0', 4, 0, ''),
(99, 4, 193, 0, '0', '0', 4, 0, ''),
(100, 4, 37, 0, '0', '0', 4, 0, ''),
(101, 4, 7, 0, '0', '0', 4, 0, ''),
(102, 4, 18, 0, '0', '0', 4, 0, ''),
(103, 5, 14, 0, '0', '0', 1, 0, ''),
(104, 5, 2, 0, '0', '0', 5, 0, ''),
(105, 5, 3, 0, '0', '0', 6, 0, ''),
(106, 5, 236, 0, '0', '0', 7, 0, ''),
(107, 5, 76, 0, '0', '0', 8, 0, ''),
(108, 5, 16, 0, '0', '0', 9, 0, ''),
(109, 5, 1, 0, '0', '0', 10, 0, ''),
(110, 5, 20, 0, '0', '0', 11, 0, ''),
(111, 5, 86, 0, '0', '0', 12, 0, ''),
(112, 5, 6, 0, '0', '0', 13, 0, ''),
(113, 5, 7, 0, '0', '0', 14, 0, ''),
(114, 5, 8, 0, '0', '0', 15, 0, ''),
(115, 5, 17, 0, '0', '0', 16, 0, ''),
(116, 5, 9, 0, '0', '0', 17, 0, ''),
(117, 5, 74, 0, '0', '0', 18, 0, ''),
(118, 5, 143, 0, '0', '0', 19, 0, ''),
(119, 5, 26, 0, '0', '0', 20, 0, ''),
(120, 5, 10, 0, '0', '0', 21, 0, ''),
(121, 5, 131, 0, '0', '0', 22, 0, ''),
(122, 5, 12, 0, '0', '0', 23, 0, ''),
(123, 5, 125, 0, '0', '0', 24, 0, ''),
(124, 5, 139, 0, '0', '0', 25, 0, ''),
(125, 5, 13, 0, '0', '0', 26, 0, ''),
(126, 5, 15, 0, '0', '0', 27, 0, ''),
(127, 5, 36, 0, '0', '0', 28, 0, ''),
(128, 5, 18, 0, '0', '0', 29, 0, ''),
(129, 5, 193, 0, '0', '0', 30, 0, ''),
(130, 5, 37, 0, '0', '0', 31, 0, '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_tax_rules_group`
--

DROP TABLE IF EXISTS `ps_tax_rules_group`;
CREATE TABLE `ps_tax_rules_group` (
  `id_tax_rules_group` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `active` int(11) NOT NULL,
  `deleted` tinyint(1) UNSIGNED NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_tax_rules_group`
--

INSERT INTO `ps_tax_rules_group` (`id_tax_rules_group`, `name`, `active`, `deleted`, `date_add`, `date_upd`) VALUES
(1, 'PL Standard Rate (23%)', 1, 0, '2018-12-03 17:18:06', '2018-12-03 17:18:06'),
(2, 'PL Reduced Rate (8%)', 1, 0, '2018-12-03 17:18:06', '2018-12-03 17:18:06'),
(3, 'PL Reduced Rate (5%)', 1, 0, '2018-12-03 17:18:06', '2018-12-03 17:18:06'),
(4, 'PL Exempted Rate (0%)', 1, 0, '2018-12-03 17:18:06', '2018-12-03 17:18:06'),
(5, 'EU VAT For Virtual Products', 1, 0, '2018-12-03 17:18:06', '2018-12-03 17:18:06');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_tax_rules_group_shop`
--

DROP TABLE IF EXISTS `ps_tax_rules_group_shop`;
CREATE TABLE `ps_tax_rules_group_shop` (
  `id_tax_rules_group` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_tax_rules_group_shop`
--

INSERT INTO `ps_tax_rules_group_shop` (`id_tax_rules_group`, `id_shop`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_theme`
--

DROP TABLE IF EXISTS `ps_theme`;
CREATE TABLE `ps_theme` (
  `id_theme` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `directory` varchar(64) NOT NULL,
  `responsive` tinyint(1) NOT NULL DEFAULT 0,
  `default_left_column` tinyint(1) NOT NULL DEFAULT 0,
  `default_right_column` tinyint(1) NOT NULL DEFAULT 0,
  `product_per_page` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_theme`
--

INSERT INTO `ps_theme` (`id_theme`, `name`, `directory`, `responsive`, `default_left_column`, `default_right_column`, `product_per_page`) VALUES
(1, 'default-bootstrap', 'default-bootstrap', 1, 1, 0, 12);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_themeconfigurator`
--

DROP TABLE IF EXISTS `ps_themeconfigurator`;
CREATE TABLE `ps_themeconfigurator` (
  `id_item` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `item_order` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `title_use` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `hook` varchar(100) DEFAULT NULL,
  `url` text DEFAULT NULL,
  `target` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `image` varchar(100) DEFAULT NULL,
  `image_w` varchar(10) DEFAULT NULL,
  `image_h` varchar(10) DEFAULT NULL,
  `html` text DEFAULT NULL,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_theme_meta`
--

DROP TABLE IF EXISTS `ps_theme_meta`;
CREATE TABLE `ps_theme_meta` (
  `id_theme_meta` int(11) NOT NULL,
  `id_theme` int(11) NOT NULL,
  `id_meta` int(10) UNSIGNED NOT NULL,
  `left_column` tinyint(1) NOT NULL DEFAULT 1,
  `right_column` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_theme_meta`
--

INSERT INTO `ps_theme_meta` (`id_theme_meta`, `id_theme`, `id_meta`, `left_column`, `right_column`) VALUES
(1, 1, 1, 0, 0),
(2, 1, 2, 1, 0),
(3, 1, 3, 0, 0),
(4, 1, 4, 0, 0),
(5, 1, 5, 1, 0),
(6, 1, 6, 1, 0),
(7, 1, 7, 0, 0),
(8, 1, 8, 1, 0),
(9, 1, 9, 1, 0),
(10, 1, 10, 0, 0),
(11, 1, 11, 0, 0),
(12, 1, 12, 0, 0),
(13, 1, 13, 0, 0),
(14, 1, 14, 0, 0),
(15, 1, 15, 0, 0),
(16, 1, 16, 0, 0),
(17, 1, 17, 0, 0),
(18, 1, 18, 0, 0),
(19, 1, 19, 0, 0),
(20, 1, 20, 0, 0),
(21, 1, 21, 0, 0),
(22, 1, 22, 1, 0),
(23, 1, 23, 0, 0),
(24, 1, 24, 0, 0),
(25, 1, 25, 0, 0),
(26, 1, 26, 0, 0),
(27, 1, 28, 1, 0),
(28, 1, 29, 0, 0),
(29, 1, 27, 0, 0),
(30, 1, 30, 0, 0),
(31, 1, 31, 0, 0),
(32, 1, 32, 0, 0),
(33, 1, 33, 0, 0),
(34, 1, 34, 0, 0),
(35, 1, 36, 1, 0),
(36, 1, 37, 1, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_theme_specific`
--

DROP TABLE IF EXISTS `ps_theme_specific`;
CREATE TABLE `ps_theme_specific` (
  `id_theme` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  `entity` int(11) UNSIGNED NOT NULL,
  `id_object` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_timezone`
--

DROP TABLE IF EXISTS `ps_timezone`;
CREATE TABLE `ps_timezone` (
  `id_timezone` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_timezone`
--

INSERT INTO `ps_timezone` (`id_timezone`, `name`) VALUES
(1, 'Africa/Abidjan'),
(2, 'Africa/Accra'),
(3, 'Africa/Addis_Ababa'),
(4, 'Africa/Algiers'),
(5, 'Africa/Asmara'),
(6, 'Africa/Asmera'),
(7, 'Africa/Bamako'),
(8, 'Africa/Bangui'),
(9, 'Africa/Banjul'),
(10, 'Africa/Bissau'),
(11, 'Africa/Blantyre'),
(12, 'Africa/Brazzaville'),
(13, 'Africa/Bujumbura'),
(14, 'Africa/Cairo'),
(15, 'Africa/Casablanca'),
(16, 'Africa/Ceuta'),
(17, 'Africa/Conakry'),
(18, 'Africa/Dakar'),
(19, 'Africa/Dar_es_Salaam'),
(20, 'Africa/Djibouti'),
(21, 'Africa/Douala'),
(22, 'Africa/El_Aaiun'),
(23, 'Africa/Freetown'),
(24, 'Africa/Gaborone'),
(25, 'Africa/Harare'),
(26, 'Africa/Johannesburg'),
(27, 'Africa/Kampala'),
(28, 'Africa/Khartoum'),
(29, 'Africa/Kigali'),
(30, 'Africa/Kinshasa'),
(31, 'Africa/Lagos'),
(32, 'Africa/Libreville'),
(33, 'Africa/Lome'),
(34, 'Africa/Luanda'),
(35, 'Africa/Lubumbashi'),
(36, 'Africa/Lusaka'),
(37, 'Africa/Malabo'),
(38, 'Africa/Maputo'),
(39, 'Africa/Maseru'),
(40, 'Africa/Mbabane'),
(41, 'Africa/Mogadishu'),
(42, 'Africa/Monrovia'),
(43, 'Africa/Nairobi'),
(44, 'Africa/Ndjamena'),
(45, 'Africa/Niamey'),
(46, 'Africa/Nouakchott'),
(47, 'Africa/Ouagadougou'),
(48, 'Africa/Porto-Novo'),
(49, 'Africa/Sao_Tome'),
(50, 'Africa/Timbuktu'),
(51, 'Africa/Tripoli'),
(52, 'Africa/Tunis'),
(53, 'Africa/Windhoek'),
(54, 'America/Adak'),
(55, 'America/Anchorage '),
(56, 'America/Anguilla'),
(57, 'America/Antigua'),
(58, 'America/Araguaina'),
(59, 'America/Argentina/Buenos_Aires'),
(60, 'America/Argentina/Catamarca'),
(61, 'America/Argentina/ComodRivadavia'),
(62, 'America/Argentina/Cordoba'),
(63, 'America/Argentina/Jujuy'),
(64, 'America/Argentina/La_Rioja'),
(65, 'America/Argentina/Mendoza'),
(66, 'America/Argentina/Rio_Gallegos'),
(67, 'America/Argentina/Salta'),
(68, 'America/Argentina/San_Juan'),
(69, 'America/Argentina/San_Luis'),
(70, 'America/Argentina/Tucuman'),
(71, 'America/Argentina/Ushuaia'),
(72, 'America/Aruba'),
(73, 'America/Asuncion'),
(74, 'America/Atikokan'),
(75, 'America/Atka'),
(76, 'America/Bahia'),
(77, 'America/Barbados'),
(78, 'America/Belem'),
(79, 'America/Belize'),
(80, 'America/Blanc-Sablon'),
(81, 'America/Boa_Vista'),
(82, 'America/Bogota'),
(83, 'America/Boise'),
(84, 'America/Buenos_Aires'),
(85, 'America/Cambridge_Bay'),
(86, 'America/Campo_Grande'),
(87, 'America/Cancun'),
(88, 'America/Caracas'),
(89, 'America/Catamarca'),
(90, 'America/Cayenne'),
(91, 'America/Cayman'),
(92, 'America/Chicago'),
(93, 'America/Chihuahua'),
(94, 'America/Coral_Harbour'),
(95, 'America/Cordoba'),
(96, 'America/Costa_Rica'),
(97, 'America/Cuiaba'),
(98, 'America/Curacao'),
(99, 'America/Danmarkshavn'),
(100, 'America/Dawson'),
(101, 'America/Dawson_Creek'),
(102, 'America/Denver'),
(103, 'America/Detroit'),
(104, 'America/Dominica'),
(105, 'America/Edmonton'),
(106, 'America/Eirunepe'),
(107, 'America/El_Salvador'),
(108, 'America/Ensenada'),
(109, 'America/Fort_Wayne'),
(110, 'America/Fortaleza'),
(111, 'America/Glace_Bay'),
(112, 'America/Godthab'),
(113, 'America/Goose_Bay'),
(114, 'America/Grand_Turk'),
(115, 'America/Grenada'),
(116, 'America/Guadeloupe'),
(117, 'America/Guatemala'),
(118, 'America/Guayaquil'),
(119, 'America/Guyana'),
(120, 'America/Halifax'),
(121, 'America/Havana'),
(122, 'America/Hermosillo'),
(123, 'America/Indiana/Indianapolis'),
(124, 'America/Indiana/Knox'),
(125, 'America/Indiana/Marengo'),
(126, 'America/Indiana/Petersburg'),
(127, 'America/Indiana/Tell_City'),
(128, 'America/Indiana/Vevay'),
(129, 'America/Indiana/Vincennes'),
(130, 'America/Indiana/Winamac'),
(131, 'America/Indianapolis'),
(132, 'America/Inuvik'),
(133, 'America/Iqaluit'),
(134, 'America/Jamaica'),
(135, 'America/Jujuy'),
(136, 'America/Juneau'),
(137, 'America/Kentucky/Louisville'),
(138, 'America/Kentucky/Monticello'),
(139, 'America/Knox_IN'),
(140, 'America/La_Paz'),
(141, 'America/Lima'),
(142, 'America/Los_Angeles'),
(143, 'America/Louisville'),
(144, 'America/Maceio'),
(145, 'America/Managua'),
(146, 'America/Manaus'),
(147, 'America/Marigot'),
(148, 'America/Martinique'),
(149, 'America/Mazatlan'),
(150, 'America/Mendoza'),
(151, 'America/Menominee'),
(152, 'America/Merida'),
(153, 'America/Mexico_City'),
(154, 'America/Miquelon'),
(155, 'America/Moncton'),
(156, 'America/Monterrey'),
(157, 'America/Montevideo'),
(158, 'America/Montreal'),
(159, 'America/Montserrat'),
(160, 'America/Nassau'),
(161, 'America/New_York'),
(162, 'America/Nipigon'),
(163, 'America/Nome'),
(164, 'America/Noronha'),
(165, 'America/North_Dakota/Center'),
(166, 'America/North_Dakota/New_Salem'),
(167, 'America/Panama'),
(168, 'America/Pangnirtung'),
(169, 'America/Paramaribo'),
(170, 'America/Phoenix'),
(171, 'America/Port-au-Prince'),
(172, 'America/Port_of_Spain'),
(173, 'America/Porto_Acre'),
(174, 'America/Porto_Velho'),
(175, 'America/Puerto_Rico'),
(176, 'America/Rainy_River'),
(177, 'America/Rankin_Inlet'),
(178, 'America/Recife'),
(179, 'America/Regina'),
(180, 'America/Resolute'),
(181, 'America/Rio_Branco'),
(182, 'America/Rosario'),
(183, 'America/Santarem'),
(184, 'America/Santiago'),
(185, 'America/Santo_Domingo'),
(186, 'America/Sao_Paulo'),
(187, 'America/Scoresbysund'),
(188, 'America/Shiprock'),
(189, 'America/St_Barthelemy'),
(190, 'America/St_Johns'),
(191, 'America/St_Kitts'),
(192, 'America/St_Lucia'),
(193, 'America/St_Thomas'),
(194, 'America/St_Vincent'),
(195, 'America/Swift_Current'),
(196, 'America/Tegucigalpa'),
(197, 'America/Thule'),
(198, 'America/Thunder_Bay'),
(199, 'America/Tijuana'),
(200, 'America/Toronto'),
(201, 'America/Tortola'),
(202, 'America/Vancouver'),
(203, 'America/Virgin'),
(204, 'America/Whitehorse'),
(205, 'America/Winnipeg'),
(206, 'America/Yakutat'),
(207, 'America/Yellowknife'),
(208, 'Antarctica/Casey'),
(209, 'Antarctica/Davis'),
(210, 'Antarctica/DumontDUrville'),
(211, 'Antarctica/Mawson'),
(212, 'Antarctica/McMurdo'),
(213, 'Antarctica/Palmer'),
(214, 'Antarctica/Rothera'),
(215, 'Antarctica/South_Pole'),
(216, 'Antarctica/Syowa'),
(217, 'Antarctica/Vostok'),
(218, 'Arctic/Longyearbyen'),
(219, 'Asia/Aden'),
(220, 'Asia/Almaty'),
(221, 'Asia/Amman'),
(222, 'Asia/Anadyr'),
(223, 'Asia/Aqtau'),
(224, 'Asia/Aqtobe'),
(225, 'Asia/Ashgabat'),
(226, 'Asia/Ashkhabad'),
(227, 'Asia/Baghdad'),
(228, 'Asia/Bahrain'),
(229, 'Asia/Baku'),
(230, 'Asia/Bangkok'),
(231, 'Asia/Beirut'),
(232, 'Asia/Bishkek'),
(233, 'Asia/Brunei'),
(234, 'Asia/Calcutta'),
(235, 'Asia/Choibalsan'),
(236, 'Asia/Chongqing'),
(237, 'Asia/Chungking'),
(238, 'Asia/Colombo'),
(239, 'Asia/Dacca'),
(240, 'Asia/Damascus'),
(241, 'Asia/Dhaka'),
(242, 'Asia/Dili'),
(243, 'Asia/Dubai'),
(244, 'Asia/Dushanbe'),
(245, 'Asia/Gaza'),
(246, 'Asia/Harbin'),
(247, 'Asia/Ho_Chi_Minh'),
(248, 'Asia/Hong_Kong'),
(249, 'Asia/Hovd'),
(250, 'Asia/Irkutsk'),
(251, 'Asia/Istanbul'),
(252, 'Asia/Jakarta'),
(253, 'Asia/Jayapura'),
(254, 'Asia/Jerusalem'),
(255, 'Asia/Kabul'),
(256, 'Asia/Kamchatka'),
(257, 'Asia/Karachi'),
(258, 'Asia/Kashgar'),
(259, 'Asia/Kathmandu'),
(260, 'Asia/Katmandu'),
(261, 'Asia/Kolkata'),
(262, 'Asia/Krasnoyarsk'),
(263, 'Asia/Kuala_Lumpur'),
(264, 'Asia/Kuching'),
(265, 'Asia/Kuwait'),
(266, 'Asia/Macao'),
(267, 'Asia/Macau'),
(268, 'Asia/Magadan'),
(269, 'Asia/Makassar'),
(270, 'Asia/Manila'),
(271, 'Asia/Muscat'),
(272, 'Asia/Nicosia'),
(273, 'Asia/Novosibirsk'),
(274, 'Asia/Omsk'),
(275, 'Asia/Oral'),
(276, 'Asia/Phnom_Penh'),
(277, 'Asia/Pontianak'),
(278, 'Asia/Pyongyang'),
(279, 'Asia/Qatar'),
(280, 'Asia/Qyzylorda'),
(281, 'Asia/Rangoon'),
(282, 'Asia/Riyadh'),
(283, 'Asia/Saigon'),
(284, 'Asia/Sakhalin'),
(285, 'Asia/Samarkand'),
(286, 'Asia/Seoul'),
(287, 'Asia/Shanghai'),
(288, 'Asia/Singapore'),
(289, 'Asia/Taipei'),
(290, 'Asia/Tashkent'),
(291, 'Asia/Tbilisi'),
(292, 'Asia/Tehran'),
(293, 'Asia/Tel_Aviv'),
(294, 'Asia/Thimbu'),
(295, 'Asia/Thimphu'),
(296, 'Asia/Tokyo'),
(297, 'Asia/Ujung_Pandang'),
(298, 'Asia/Ulaanbaatar'),
(299, 'Asia/Ulan_Bator'),
(300, 'Asia/Urumqi'),
(301, 'Asia/Vientiane'),
(302, 'Asia/Vladivostok'),
(303, 'Asia/Yakutsk'),
(304, 'Asia/Yekaterinburg'),
(305, 'Asia/Yerevan'),
(306, 'Atlantic/Azores'),
(307, 'Atlantic/Bermuda'),
(308, 'Atlantic/Canary'),
(309, 'Atlantic/Cape_Verde'),
(310, 'Atlantic/Faeroe'),
(311, 'Atlantic/Faroe'),
(312, 'Atlantic/Jan_Mayen'),
(313, 'Atlantic/Madeira'),
(314, 'Atlantic/Reykjavik'),
(315, 'Atlantic/South_Georgia'),
(316, 'Atlantic/St_Helena'),
(317, 'Atlantic/Stanley'),
(318, 'Australia/ACT'),
(319, 'Australia/Adelaide'),
(320, 'Australia/Brisbane'),
(321, 'Australia/Broken_Hill'),
(322, 'Australia/Canberra'),
(323, 'Australia/Currie'),
(324, 'Australia/Darwin'),
(325, 'Australia/Eucla'),
(326, 'Australia/Hobart'),
(327, 'Australia/LHI'),
(328, 'Australia/Lindeman'),
(329, 'Australia/Lord_Howe'),
(330, 'Australia/Melbourne'),
(331, 'Australia/North'),
(332, 'Australia/NSW'),
(333, 'Australia/Perth'),
(334, 'Australia/Queensland'),
(335, 'Australia/South'),
(336, 'Australia/Sydney'),
(337, 'Australia/Tasmania'),
(338, 'Australia/Victoria'),
(339, 'Australia/West'),
(340, 'Australia/Yancowinna'),
(341, 'Europe/Amsterdam'),
(342, 'Europe/Andorra'),
(343, 'Europe/Athens'),
(344, 'Europe/Belfast'),
(345, 'Europe/Belgrade'),
(346, 'Europe/Berlin'),
(347, 'Europe/Bratislava'),
(348, 'Europe/Brussels'),
(349, 'Europe/Bucharest'),
(350, 'Europe/Budapest'),
(351, 'Europe/Chisinau'),
(352, 'Europe/Copenhagen'),
(353, 'Europe/Dublin'),
(354, 'Europe/Gibraltar'),
(355, 'Europe/Guernsey'),
(356, 'Europe/Helsinki'),
(357, 'Europe/Isle_of_Man'),
(358, 'Europe/Istanbul'),
(359, 'Europe/Jersey'),
(360, 'Europe/Kaliningrad'),
(361, 'Europe/Kiev'),
(362, 'Europe/Lisbon'),
(363, 'Europe/Ljubljana'),
(364, 'Europe/London'),
(365, 'Europe/Luxembourg'),
(366, 'Europe/Madrid'),
(367, 'Europe/Malta'),
(368, 'Europe/Mariehamn'),
(369, 'Europe/Minsk'),
(370, 'Europe/Monaco'),
(371, 'Europe/Moscow'),
(372, 'Europe/Nicosia'),
(373, 'Europe/Oslo'),
(374, 'Europe/Paris'),
(375, 'Europe/Podgorica'),
(376, 'Europe/Prague'),
(377, 'Europe/Riga'),
(378, 'Europe/Rome'),
(379, 'Europe/Samara'),
(380, 'Europe/San_Marino'),
(381, 'Europe/Sarajevo'),
(382, 'Europe/Simferopol'),
(383, 'Europe/Skopje'),
(384, 'Europe/Sofia'),
(385, 'Europe/Stockholm'),
(386, 'Europe/Tallinn'),
(387, 'Europe/Tirane'),
(388, 'Europe/Tiraspol'),
(389, 'Europe/Uzhgorod'),
(390, 'Europe/Vaduz'),
(391, 'Europe/Vatican'),
(392, 'Europe/Vienna'),
(393, 'Europe/Vilnius'),
(394, 'Europe/Volgograd'),
(395, 'Europe/Warsaw'),
(396, 'Europe/Zagreb'),
(397, 'Europe/Zaporozhye'),
(398, 'Europe/Zurich'),
(399, 'Indian/Antananarivo'),
(400, 'Indian/Chagos'),
(401, 'Indian/Christmas'),
(402, 'Indian/Cocos'),
(403, 'Indian/Comoro'),
(404, 'Indian/Kerguelen'),
(405, 'Indian/Mahe'),
(406, 'Indian/Maldives'),
(407, 'Indian/Mauritius'),
(408, 'Indian/Mayotte'),
(409, 'Indian/Reunion'),
(410, 'Pacific/Apia'),
(411, 'Pacific/Auckland'),
(412, 'Pacific/Chatham'),
(413, 'Pacific/Easter'),
(414, 'Pacific/Efate'),
(415, 'Pacific/Enderbury'),
(416, 'Pacific/Fakaofo'),
(417, 'Pacific/Fiji'),
(418, 'Pacific/Funafuti'),
(419, 'Pacific/Galapagos'),
(420, 'Pacific/Gambier'),
(421, 'Pacific/Guadalcanal'),
(422, 'Pacific/Guam'),
(423, 'Pacific/Honolulu'),
(424, 'Pacific/Johnston'),
(425, 'Pacific/Kiritimati'),
(426, 'Pacific/Kosrae'),
(427, 'Pacific/Kwajalein'),
(428, 'Pacific/Majuro'),
(429, 'Pacific/Marquesas'),
(430, 'Pacific/Midway'),
(431, 'Pacific/Nauru'),
(432, 'Pacific/Niue'),
(433, 'Pacific/Norfolk'),
(434, 'Pacific/Noumea'),
(435, 'Pacific/Pago_Pago'),
(436, 'Pacific/Palau'),
(437, 'Pacific/Pitcairn'),
(438, 'Pacific/Ponape'),
(439, 'Pacific/Port_Moresby'),
(440, 'Pacific/Rarotonga'),
(441, 'Pacific/Saipan'),
(442, 'Pacific/Samoa'),
(443, 'Pacific/Tahiti'),
(444, 'Pacific/Tarawa'),
(445, 'Pacific/Tongatapu'),
(446, 'Pacific/Truk'),
(447, 'Pacific/Wake'),
(448, 'Pacific/Wallis'),
(449, 'Pacific/Yap'),
(450, 'Brazil/Acre'),
(451, 'Brazil/DeNoronha'),
(452, 'Brazil/East'),
(453, 'Brazil/West'),
(454, 'Canada/Atlantic'),
(455, 'Canada/Central'),
(456, 'Canada/East-Saskatchewan'),
(457, 'Canada/Eastern'),
(458, 'Canada/Mountain'),
(459, 'Canada/Newfoundland'),
(460, 'Canada/Pacific'),
(461, 'Canada/Saskatchewan'),
(462, 'Canada/Yukon'),
(463, 'CET'),
(464, 'Chile/Continental'),
(465, 'Chile/EasterIsland'),
(466, 'CST6CDT'),
(467, 'Cuba'),
(468, 'EET'),
(469, 'Egypt'),
(470, 'Eire'),
(471, 'EST'),
(472, 'EST5EDT'),
(473, 'Etc/GMT'),
(474, 'Etc/GMT+0'),
(475, 'Etc/GMT+1'),
(476, 'Etc/GMT+10'),
(477, 'Etc/GMT+11'),
(478, 'Etc/GMT+12'),
(479, 'Etc/GMT+2'),
(480, 'Etc/GMT+3'),
(481, 'Etc/GMT+4'),
(482, 'Etc/GMT+5'),
(483, 'Etc/GMT+6'),
(484, 'Etc/GMT+7'),
(485, 'Etc/GMT+8'),
(486, 'Etc/GMT+9'),
(487, 'Etc/GMT-0'),
(488, 'Etc/GMT-1'),
(489, 'Etc/GMT-10'),
(490, 'Etc/GMT-11'),
(491, 'Etc/GMT-12'),
(492, 'Etc/GMT-13'),
(493, 'Etc/GMT-14'),
(494, 'Etc/GMT-2'),
(495, 'Etc/GMT-3'),
(496, 'Etc/GMT-4'),
(497, 'Etc/GMT-5'),
(498, 'Etc/GMT-6'),
(499, 'Etc/GMT-7'),
(500, 'Etc/GMT-8'),
(501, 'Etc/GMT-9'),
(502, 'Etc/GMT0'),
(503, 'Etc/Greenwich'),
(504, 'Etc/UCT'),
(505, 'Etc/Universal'),
(506, 'Etc/UTC'),
(507, 'Etc/Zulu'),
(508, 'Factory'),
(509, 'GB'),
(510, 'GB-Eire'),
(511, 'GMT'),
(512, 'GMT+0'),
(513, 'GMT-0'),
(514, 'GMT0'),
(515, 'Greenwich'),
(516, 'Hongkong'),
(517, 'HST'),
(518, 'Iceland'),
(519, 'Iran'),
(520, 'Israel'),
(521, 'Jamaica'),
(522, 'Japan'),
(523, 'Kwajalein'),
(524, 'Libya'),
(525, 'MET'),
(526, 'Mexico/BajaNorte'),
(527, 'Mexico/BajaSur'),
(528, 'Mexico/General'),
(529, 'MST'),
(530, 'MST7MDT'),
(531, 'Navajo'),
(532, 'NZ'),
(533, 'NZ-CHAT'),
(534, 'Poland'),
(535, 'Portugal'),
(536, 'PRC'),
(537, 'PST8PDT'),
(538, 'ROC'),
(539, 'ROK'),
(540, 'Singapore'),
(541, 'Turkey'),
(542, 'UCT'),
(543, 'Universal'),
(544, 'US/Alaska'),
(545, 'US/Aleutian'),
(546, 'US/Arizona'),
(547, 'US/Central'),
(548, 'US/East-Indiana'),
(549, 'US/Eastern'),
(550, 'US/Hawaii'),
(551, 'US/Indiana-Starke'),
(552, 'US/Michigan'),
(553, 'US/Mountain'),
(554, 'US/Pacific'),
(555, 'US/Pacific-New'),
(556, 'US/Samoa'),
(557, 'UTC'),
(558, 'W-SU'),
(559, 'WET'),
(560, 'Zulu');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_warehouse`
--

DROP TABLE IF EXISTS `ps_warehouse`;
CREATE TABLE `ps_warehouse` (
  `id_warehouse` int(11) UNSIGNED NOT NULL,
  `id_currency` int(11) UNSIGNED NOT NULL,
  `id_address` int(11) UNSIGNED NOT NULL,
  `id_employee` int(11) UNSIGNED NOT NULL,
  `reference` varchar(32) DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  `management_type` enum('WA','FIFO','LIFO') NOT NULL DEFAULT 'WA',
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_warehouse_carrier`
--

DROP TABLE IF EXISTS `ps_warehouse_carrier`;
CREATE TABLE `ps_warehouse_carrier` (
  `id_carrier` int(11) UNSIGNED NOT NULL,
  `id_warehouse` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_warehouse_product_location`
--

DROP TABLE IF EXISTS `ps_warehouse_product_location`;
CREATE TABLE `ps_warehouse_product_location` (
  `id_warehouse_product_location` int(11) UNSIGNED NOT NULL,
  `id_product` int(11) UNSIGNED NOT NULL,
  `id_product_attribute` int(11) UNSIGNED NOT NULL,
  `id_warehouse` int(11) UNSIGNED NOT NULL,
  `location` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_warehouse_shop`
--

DROP TABLE IF EXISTS `ps_warehouse_shop`;
CREATE TABLE `ps_warehouse_shop` (
  `id_shop` int(11) UNSIGNED NOT NULL,
  `id_warehouse` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_webservice_account`
--

DROP TABLE IF EXISTS `ps_webservice_account`;
CREATE TABLE `ps_webservice_account` (
  `id_webservice_account` int(11) NOT NULL,
  `key` varchar(32) NOT NULL,
  `description` text DEFAULT NULL,
  `class_name` varchar(50) NOT NULL DEFAULT 'WebserviceRequest',
  `is_module` tinyint(2) NOT NULL DEFAULT 0,
  `module_name` varchar(50) DEFAULT NULL,
  `active` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_webservice_account_shop`
--

DROP TABLE IF EXISTS `ps_webservice_account_shop`;
CREATE TABLE `ps_webservice_account_shop` (
  `id_webservice_account` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_webservice_permission`
--

DROP TABLE IF EXISTS `ps_webservice_permission`;
CREATE TABLE `ps_webservice_permission` (
  `id_webservice_permission` int(11) NOT NULL,
  `resource` varchar(50) NOT NULL,
  `method` enum('GET','POST','PUT','DELETE','HEAD') NOT NULL,
  `id_webservice_account` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_web_browser`
--

DROP TABLE IF EXISTS `ps_web_browser`;
CREATE TABLE `ps_web_browser` (
  `id_web_browser` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_web_browser`
--

INSERT INTO `ps_web_browser` (`id_web_browser`, `name`) VALUES
(1, 'Safari'),
(2, 'Safari iPad'),
(3, 'Firefox'),
(4, 'Opera'),
(5, 'IE 6'),
(6, 'IE 7'),
(7, 'IE 8'),
(8, 'IE 9'),
(9, 'IE 10'),
(10, 'IE 11'),
(11, 'Chrome');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_zone`
--

DROP TABLE IF EXISTS `ps_zone`;
CREATE TABLE `ps_zone` (
  `id_zone` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_zone`
--

INSERT INTO `ps_zone` (`id_zone`, `name`, `active`) VALUES
(1, 'Europe', 1),
(2, 'North America', 1),
(3, 'Asia', 1),
(4, 'Africa', 1),
(5, 'Oceania', 1),
(6, 'South America', 1),
(7, 'Europe (non-EU)', 1),
(8, 'Central America/Antilla', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ps_zone_shop`
--

DROP TABLE IF EXISTS `ps_zone_shop`;
CREATE TABLE `ps_zone_shop` (
  `id_zone` int(11) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ps_zone_shop`
--

INSERT INTO `ps_zone_shop` (`id_zone`, `id_shop`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `ps_access`
--
ALTER TABLE `ps_access`
  ADD PRIMARY KEY (`id_profile`,`id_tab`);

--
-- Indeksy dla tabeli `ps_accessory`
--
ALTER TABLE `ps_accessory`
  ADD KEY `accessory_product` (`id_product_1`,`id_product_2`);

--
-- Indeksy dla tabeli `ps_address`
--
ALTER TABLE `ps_address`
  ADD PRIMARY KEY (`id_address`),
  ADD KEY `address_customer` (`id_customer`),
  ADD KEY `id_country` (`id_country`),
  ADD KEY `id_state` (`id_state`),
  ADD KEY `id_manufacturer` (`id_manufacturer`),
  ADD KEY `id_supplier` (`id_supplier`),
  ADD KEY `id_warehouse` (`id_warehouse`);

--
-- Indeksy dla tabeli `ps_address_format`
--
ALTER TABLE `ps_address_format`
  ADD PRIMARY KEY (`id_country`);

--
-- Indeksy dla tabeli `ps_advice`
--
ALTER TABLE `ps_advice`
  ADD PRIMARY KEY (`id_advice`);

--
-- Indeksy dla tabeli `ps_advice_lang`
--
ALTER TABLE `ps_advice_lang`
  ADD PRIMARY KEY (`id_advice`,`id_lang`);

--
-- Indeksy dla tabeli `ps_alias`
--
ALTER TABLE `ps_alias`
  ADD PRIMARY KEY (`id_alias`),
  ADD UNIQUE KEY `alias` (`alias`);

--
-- Indeksy dla tabeli `ps_attachment`
--
ALTER TABLE `ps_attachment`
  ADD PRIMARY KEY (`id_attachment`);

--
-- Indeksy dla tabeli `ps_attachment_lang`
--
ALTER TABLE `ps_attachment_lang`
  ADD PRIMARY KEY (`id_attachment`,`id_lang`);

--
-- Indeksy dla tabeli `ps_attribute`
--
ALTER TABLE `ps_attribute`
  ADD PRIMARY KEY (`id_attribute`),
  ADD KEY `attribute_group` (`id_attribute_group`);

--
-- Indeksy dla tabeli `ps_attribute_group`
--
ALTER TABLE `ps_attribute_group`
  ADD PRIMARY KEY (`id_attribute_group`);

--
-- Indeksy dla tabeli `ps_attribute_group_lang`
--
ALTER TABLE `ps_attribute_group_lang`
  ADD PRIMARY KEY (`id_attribute_group`,`id_lang`);

--
-- Indeksy dla tabeli `ps_attribute_group_shop`
--
ALTER TABLE `ps_attribute_group_shop`
  ADD PRIMARY KEY (`id_attribute_group`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indeksy dla tabeli `ps_attribute_impact`
--
ALTER TABLE `ps_attribute_impact`
  ADD PRIMARY KEY (`id_attribute_impact`),
  ADD UNIQUE KEY `id_product` (`id_product`,`id_attribute`);

--
-- Indeksy dla tabeli `ps_attribute_lang`
--
ALTER TABLE `ps_attribute_lang`
  ADD PRIMARY KEY (`id_attribute`,`id_lang`),
  ADD KEY `id_lang` (`id_lang`,`name`);

--
-- Indeksy dla tabeli `ps_attribute_shop`
--
ALTER TABLE `ps_attribute_shop`
  ADD PRIMARY KEY (`id_attribute`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indeksy dla tabeli `ps_badge`
--
ALTER TABLE `ps_badge`
  ADD PRIMARY KEY (`id_badge`);

--
-- Indeksy dla tabeli `ps_badge_lang`
--
ALTER TABLE `ps_badge_lang`
  ADD PRIMARY KEY (`id_badge`,`id_lang`);

--
-- Indeksy dla tabeli `ps_carrier`
--
ALTER TABLE `ps_carrier`
  ADD PRIMARY KEY (`id_carrier`),
  ADD KEY `deleted` (`deleted`,`active`),
  ADD KEY `id_tax_rules_group` (`id_tax_rules_group`),
  ADD KEY `reference` (`id_reference`,`deleted`,`active`);

--
-- Indeksy dla tabeli `ps_carrier_group`
--
ALTER TABLE `ps_carrier_group`
  ADD PRIMARY KEY (`id_carrier`,`id_group`);

--
-- Indeksy dla tabeli `ps_carrier_lang`
--
ALTER TABLE `ps_carrier_lang`
  ADD PRIMARY KEY (`id_lang`,`id_shop`,`id_carrier`);

--
-- Indeksy dla tabeli `ps_carrier_shop`
--
ALTER TABLE `ps_carrier_shop`
  ADD PRIMARY KEY (`id_carrier`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indeksy dla tabeli `ps_carrier_tax_rules_group_shop`
--
ALTER TABLE `ps_carrier_tax_rules_group_shop`
  ADD PRIMARY KEY (`id_carrier`,`id_tax_rules_group`,`id_shop`);

--
-- Indeksy dla tabeli `ps_carrier_zone`
--
ALTER TABLE `ps_carrier_zone`
  ADD PRIMARY KEY (`id_carrier`,`id_zone`);

--
-- Indeksy dla tabeli `ps_cart`
--
ALTER TABLE `ps_cart`
  ADD PRIMARY KEY (`id_cart`),
  ADD KEY `cart_customer` (`id_customer`),
  ADD KEY `id_address_delivery` (`id_address_delivery`),
  ADD KEY `id_address_invoice` (`id_address_invoice`),
  ADD KEY `id_carrier` (`id_carrier`),
  ADD KEY `id_lang` (`id_lang`),
  ADD KEY `id_currency` (`id_currency`),
  ADD KEY `id_guest` (`id_guest`),
  ADD KEY `id_shop_group` (`id_shop_group`),
  ADD KEY `id_shop_2` (`id_shop`,`date_upd`),
  ADD KEY `id_shop` (`id_shop`,`date_add`);

--
-- Indeksy dla tabeli `ps_cart_cart_rule`
--
ALTER TABLE `ps_cart_cart_rule`
  ADD PRIMARY KEY (`id_cart`,`id_cart_rule`),
  ADD KEY `id_cart_rule` (`id_cart_rule`);

--
-- Indeksy dla tabeli `ps_cart_product`
--
ALTER TABLE `ps_cart_product`
  ADD PRIMARY KEY (`id_cart`,`id_product`,`id_product_attribute`,`id_address_delivery`),
  ADD KEY `id_product_attribute` (`id_product_attribute`),
  ADD KEY `id_cart_order` (`id_cart`,`date_add`,`id_product`,`id_product_attribute`);

--
-- Indeksy dla tabeli `ps_cart_rule`
--
ALTER TABLE `ps_cart_rule`
  ADD PRIMARY KEY (`id_cart_rule`),
  ADD KEY `id_customer` (`id_customer`,`active`,`date_to`),
  ADD KEY `group_restriction` (`group_restriction`,`active`,`date_to`),
  ADD KEY `id_customer_2` (`id_customer`,`active`,`highlight`,`date_to`),
  ADD KEY `group_restriction_2` (`group_restriction`,`active`,`highlight`,`date_to`);

--
-- Indeksy dla tabeli `ps_cart_rule_carrier`
--
ALTER TABLE `ps_cart_rule_carrier`
  ADD PRIMARY KEY (`id_cart_rule`,`id_carrier`);

--
-- Indeksy dla tabeli `ps_cart_rule_combination`
--
ALTER TABLE `ps_cart_rule_combination`
  ADD PRIMARY KEY (`id_cart_rule_1`,`id_cart_rule_2`),
  ADD KEY `id_cart_rule_1` (`id_cart_rule_1`),
  ADD KEY `id_cart_rule_2` (`id_cart_rule_2`);

--
-- Indeksy dla tabeli `ps_cart_rule_country`
--
ALTER TABLE `ps_cart_rule_country`
  ADD PRIMARY KEY (`id_cart_rule`,`id_country`);

--
-- Indeksy dla tabeli `ps_cart_rule_group`
--
ALTER TABLE `ps_cart_rule_group`
  ADD PRIMARY KEY (`id_cart_rule`,`id_group`);

--
-- Indeksy dla tabeli `ps_cart_rule_lang`
--
ALTER TABLE `ps_cart_rule_lang`
  ADD PRIMARY KEY (`id_cart_rule`,`id_lang`);

--
-- Indeksy dla tabeli `ps_cart_rule_product_rule`
--
ALTER TABLE `ps_cart_rule_product_rule`
  ADD PRIMARY KEY (`id_product_rule`);

--
-- Indeksy dla tabeli `ps_cart_rule_product_rule_group`
--
ALTER TABLE `ps_cart_rule_product_rule_group`
  ADD PRIMARY KEY (`id_product_rule_group`);

--
-- Indeksy dla tabeli `ps_cart_rule_product_rule_value`
--
ALTER TABLE `ps_cart_rule_product_rule_value`
  ADD PRIMARY KEY (`id_product_rule`,`id_item`);

--
-- Indeksy dla tabeli `ps_cart_rule_shop`
--
ALTER TABLE `ps_cart_rule_shop`
  ADD PRIMARY KEY (`id_cart_rule`,`id_shop`);

--
-- Indeksy dla tabeli `ps_category`
--
ALTER TABLE `ps_category`
  ADD PRIMARY KEY (`id_category`),
  ADD KEY `category_parent` (`id_parent`),
  ADD KEY `nleftrightactive` (`nleft`,`nright`,`active`),
  ADD KEY `level_depth` (`level_depth`),
  ADD KEY `nright` (`nright`),
  ADD KEY `activenleft` (`active`,`nleft`),
  ADD KEY `activenright` (`active`,`nright`);

--
-- Indeksy dla tabeli `ps_category_group`
--
ALTER TABLE `ps_category_group`
  ADD PRIMARY KEY (`id_category`,`id_group`),
  ADD KEY `id_category` (`id_category`),
  ADD KEY `id_group` (`id_group`);

--
-- Indeksy dla tabeli `ps_category_lang`
--
ALTER TABLE `ps_category_lang`
  ADD PRIMARY KEY (`id_category`,`id_shop`,`id_lang`),
  ADD KEY `category_name` (`name`);

--
-- Indeksy dla tabeli `ps_category_product`
--
ALTER TABLE `ps_category_product`
  ADD PRIMARY KEY (`id_category`,`id_product`),
  ADD KEY `id_product` (`id_product`),
  ADD KEY `id_category` (`id_category`,`position`);

--
-- Indeksy dla tabeli `ps_category_shop`
--
ALTER TABLE `ps_category_shop`
  ADD PRIMARY KEY (`id_category`,`id_shop`);

--
-- Indeksy dla tabeli `ps_cms`
--
ALTER TABLE `ps_cms`
  ADD PRIMARY KEY (`id_cms`);

--
-- Indeksy dla tabeli `ps_cms_block`
--
ALTER TABLE `ps_cms_block`
  ADD PRIMARY KEY (`id_cms_block`);

--
-- Indeksy dla tabeli `ps_cms_block_lang`
--
ALTER TABLE `ps_cms_block_lang`
  ADD PRIMARY KEY (`id_cms_block`,`id_lang`);

--
-- Indeksy dla tabeli `ps_cms_block_page`
--
ALTER TABLE `ps_cms_block_page`
  ADD PRIMARY KEY (`id_cms_block_page`);

--
-- Indeksy dla tabeli `ps_cms_block_shop`
--
ALTER TABLE `ps_cms_block_shop`
  ADD PRIMARY KEY (`id_cms_block`,`id_shop`);

--
-- Indeksy dla tabeli `ps_cms_category`
--
ALTER TABLE `ps_cms_category`
  ADD PRIMARY KEY (`id_cms_category`),
  ADD KEY `category_parent` (`id_parent`);

--
-- Indeksy dla tabeli `ps_cms_category_lang`
--
ALTER TABLE `ps_cms_category_lang`
  ADD PRIMARY KEY (`id_cms_category`,`id_shop`,`id_lang`),
  ADD KEY `category_name` (`name`);

--
-- Indeksy dla tabeli `ps_cms_category_shop`
--
ALTER TABLE `ps_cms_category_shop`
  ADD PRIMARY KEY (`id_cms_category`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indeksy dla tabeli `ps_cms_lang`
--
ALTER TABLE `ps_cms_lang`
  ADD PRIMARY KEY (`id_cms`,`id_shop`,`id_lang`);

--
-- Indeksy dla tabeli `ps_cms_role`
--
ALTER TABLE `ps_cms_role`
  ADD PRIMARY KEY (`id_cms_role`,`id_cms`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indeksy dla tabeli `ps_cms_role_lang`
--
ALTER TABLE `ps_cms_role_lang`
  ADD PRIMARY KEY (`id_cms_role`,`id_lang`,`id_shop`);

--
-- Indeksy dla tabeli `ps_cms_shop`
--
ALTER TABLE `ps_cms_shop`
  ADD PRIMARY KEY (`id_cms`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indeksy dla tabeli `ps_compare`
--
ALTER TABLE `ps_compare`
  ADD PRIMARY KEY (`id_compare`);

--
-- Indeksy dla tabeli `ps_compare_product`
--
ALTER TABLE `ps_compare_product`
  ADD PRIMARY KEY (`id_compare`,`id_product`);

--
-- Indeksy dla tabeli `ps_condition`
--
ALTER TABLE `ps_condition`
  ADD PRIMARY KEY (`id_condition`,`id_ps_condition`);

--
-- Indeksy dla tabeli `ps_condition_advice`
--
ALTER TABLE `ps_condition_advice`
  ADD PRIMARY KEY (`id_condition`,`id_advice`);

--
-- Indeksy dla tabeli `ps_condition_badge`
--
ALTER TABLE `ps_condition_badge`
  ADD PRIMARY KEY (`id_condition`,`id_badge`);

--
-- Indeksy dla tabeli `ps_configuration`
--
ALTER TABLE `ps_configuration`
  ADD PRIMARY KEY (`id_configuration`),
  ADD KEY `name` (`name`),
  ADD KEY `id_shop` (`id_shop`),
  ADD KEY `id_shop_group` (`id_shop_group`);

--
-- Indeksy dla tabeli `ps_configuration_kpi`
--
ALTER TABLE `ps_configuration_kpi`
  ADD PRIMARY KEY (`id_configuration_kpi`),
  ADD KEY `name` (`name`),
  ADD KEY `id_shop` (`id_shop`),
  ADD KEY `id_shop_group` (`id_shop_group`);

--
-- Indeksy dla tabeli `ps_configuration_kpi_lang`
--
ALTER TABLE `ps_configuration_kpi_lang`
  ADD PRIMARY KEY (`id_configuration_kpi`,`id_lang`);

--
-- Indeksy dla tabeli `ps_configuration_lang`
--
ALTER TABLE `ps_configuration_lang`
  ADD PRIMARY KEY (`id_configuration`,`id_lang`);

--
-- Indeksy dla tabeli `ps_connections`
--
ALTER TABLE `ps_connections`
  ADD PRIMARY KEY (`id_connections`),
  ADD KEY `id_guest` (`id_guest`),
  ADD KEY `date_add` (`date_add`),
  ADD KEY `id_page` (`id_page`);

--
-- Indeksy dla tabeli `ps_connections_page`
--
ALTER TABLE `ps_connections_page`
  ADD PRIMARY KEY (`id_connections`,`id_page`,`time_start`);

--
-- Indeksy dla tabeli `ps_connections_source`
--
ALTER TABLE `ps_connections_source`
  ADD PRIMARY KEY (`id_connections_source`),
  ADD KEY `connections` (`id_connections`),
  ADD KEY `orderby` (`date_add`),
  ADD KEY `http_referer` (`http_referer`),
  ADD KEY `request_uri` (`request_uri`);

--
-- Indeksy dla tabeli `ps_contact`
--
ALTER TABLE `ps_contact`
  ADD PRIMARY KEY (`id_contact`);

--
-- Indeksy dla tabeli `ps_contact_lang`
--
ALTER TABLE `ps_contact_lang`
  ADD PRIMARY KEY (`id_contact`,`id_lang`);

--
-- Indeksy dla tabeli `ps_contact_shop`
--
ALTER TABLE `ps_contact_shop`
  ADD PRIMARY KEY (`id_contact`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indeksy dla tabeli `ps_country`
--
ALTER TABLE `ps_country`
  ADD PRIMARY KEY (`id_country`),
  ADD KEY `country_iso_code` (`iso_code`),
  ADD KEY `country_` (`id_zone`);

--
-- Indeksy dla tabeli `ps_country_lang`
--
ALTER TABLE `ps_country_lang`
  ADD PRIMARY KEY (`id_country`,`id_lang`);

--
-- Indeksy dla tabeli `ps_country_shop`
--
ALTER TABLE `ps_country_shop`
  ADD PRIMARY KEY (`id_country`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indeksy dla tabeli `ps_cronjobs`
--
ALTER TABLE `ps_cronjobs`
  ADD PRIMARY KEY (`id_cronjob`),
  ADD KEY `id_module` (`id_module`);

--
-- Indeksy dla tabeli `ps_currency`
--
ALTER TABLE `ps_currency`
  ADD PRIMARY KEY (`id_currency`);

--
-- Indeksy dla tabeli `ps_currency_shop`
--
ALTER TABLE `ps_currency_shop`
  ADD PRIMARY KEY (`id_currency`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indeksy dla tabeli `ps_customer`
--
ALTER TABLE `ps_customer`
  ADD PRIMARY KEY (`id_customer`),
  ADD KEY `customer_email` (`email`),
  ADD KEY `customer_login` (`email`,`passwd`),
  ADD KEY `id_customer_passwd` (`id_customer`,`passwd`),
  ADD KEY `id_gender` (`id_gender`),
  ADD KEY `id_shop_group` (`id_shop_group`),
  ADD KEY `id_shop` (`id_shop`,`date_add`);

--
-- Indeksy dla tabeli `ps_customer_group`
--
ALTER TABLE `ps_customer_group`
  ADD PRIMARY KEY (`id_customer`,`id_group`),
  ADD KEY `customer_login` (`id_group`),
  ADD KEY `id_customer` (`id_customer`);

--
-- Indeksy dla tabeli `ps_customer_message`
--
ALTER TABLE `ps_customer_message`
  ADD PRIMARY KEY (`id_customer_message`),
  ADD KEY `id_customer_thread` (`id_customer_thread`),
  ADD KEY `id_employee` (`id_employee`);

--
-- Indeksy dla tabeli `ps_customer_message_sync_imap`
--
ALTER TABLE `ps_customer_message_sync_imap`
  ADD KEY `md5_header_index` (`md5_header`(4));

--
-- Indeksy dla tabeli `ps_customer_thread`
--
ALTER TABLE `ps_customer_thread`
  ADD PRIMARY KEY (`id_customer_thread`),
  ADD KEY `id_shop` (`id_shop`),
  ADD KEY `id_lang` (`id_lang`),
  ADD KEY `id_contact` (`id_contact`),
  ADD KEY `id_customer` (`id_customer`),
  ADD KEY `id_order` (`id_order`),
  ADD KEY `id_product` (`id_product`);

--
-- Indeksy dla tabeli `ps_customization`
--
ALTER TABLE `ps_customization`
  ADD PRIMARY KEY (`id_customization`,`id_cart`,`id_product`,`id_address_delivery`),
  ADD KEY `id_product_attribute` (`id_product_attribute`),
  ADD KEY `id_cart_product` (`id_cart`,`id_product`,`id_product_attribute`);

--
-- Indeksy dla tabeli `ps_customization_field`
--
ALTER TABLE `ps_customization_field`
  ADD PRIMARY KEY (`id_customization_field`),
  ADD KEY `id_product` (`id_product`);

--
-- Indeksy dla tabeli `ps_customization_field_lang`
--
ALTER TABLE `ps_customization_field_lang`
  ADD PRIMARY KEY (`id_customization_field`,`id_lang`,`id_shop`);

--
-- Indeksy dla tabeli `ps_customized_data`
--
ALTER TABLE `ps_customized_data`
  ADD PRIMARY KEY (`id_customization`,`type`,`index`);

--
-- Indeksy dla tabeli `ps_date_range`
--
ALTER TABLE `ps_date_range`
  ADD PRIMARY KEY (`id_date_range`);

--
-- Indeksy dla tabeli `ps_delivery`
--
ALTER TABLE `ps_delivery`
  ADD PRIMARY KEY (`id_delivery`),
  ADD KEY `id_zone` (`id_zone`),
  ADD KEY `id_carrier` (`id_carrier`,`id_zone`),
  ADD KEY `id_range_price` (`id_range_price`),
  ADD KEY `id_range_weight` (`id_range_weight`);

--
-- Indeksy dla tabeli `ps_employee`
--
ALTER TABLE `ps_employee`
  ADD PRIMARY KEY (`id_employee`),
  ADD KEY `employee_login` (`email`,`passwd`),
  ADD KEY `id_employee_passwd` (`id_employee`,`passwd`),
  ADD KEY `id_profile` (`id_profile`);

--
-- Indeksy dla tabeli `ps_employee_shop`
--
ALTER TABLE `ps_employee_shop`
  ADD PRIMARY KEY (`id_employee`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indeksy dla tabeli `ps_feature`
--
ALTER TABLE `ps_feature`
  ADD PRIMARY KEY (`id_feature`);

--
-- Indeksy dla tabeli `ps_feature_lang`
--
ALTER TABLE `ps_feature_lang`
  ADD PRIMARY KEY (`id_feature`,`id_lang`),
  ADD KEY `id_lang` (`id_lang`,`name`);

--
-- Indeksy dla tabeli `ps_feature_product`
--
ALTER TABLE `ps_feature_product`
  ADD PRIMARY KEY (`id_feature`,`id_product`),
  ADD KEY `id_feature_value` (`id_feature_value`),
  ADD KEY `id_product` (`id_product`);

--
-- Indeksy dla tabeli `ps_feature_shop`
--
ALTER TABLE `ps_feature_shop`
  ADD PRIMARY KEY (`id_feature`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indeksy dla tabeli `ps_feature_value`
--
ALTER TABLE `ps_feature_value`
  ADD PRIMARY KEY (`id_feature_value`),
  ADD KEY `feature` (`id_feature`);

--
-- Indeksy dla tabeli `ps_feature_value_lang`
--
ALTER TABLE `ps_feature_value_lang`
  ADD PRIMARY KEY (`id_feature_value`,`id_lang`);

--
-- Indeksy dla tabeli `ps_ganalytics`
--
ALTER TABLE `ps_ganalytics`
  ADD PRIMARY KEY (`id_google_analytics`),
  ADD KEY `id_order` (`id_order`),
  ADD KEY `sent` (`sent`);

--
-- Indeksy dla tabeli `ps_gender`
--
ALTER TABLE `ps_gender`
  ADD PRIMARY KEY (`id_gender`);

--
-- Indeksy dla tabeli `ps_gender_lang`
--
ALTER TABLE `ps_gender_lang`
  ADD PRIMARY KEY (`id_gender`,`id_lang`),
  ADD KEY `id_gender` (`id_gender`);

--
-- Indeksy dla tabeli `ps_group`
--
ALTER TABLE `ps_group`
  ADD PRIMARY KEY (`id_group`);

--
-- Indeksy dla tabeli `ps_group_lang`
--
ALTER TABLE `ps_group_lang`
  ADD PRIMARY KEY (`id_group`,`id_lang`);

--
-- Indeksy dla tabeli `ps_group_reduction`
--
ALTER TABLE `ps_group_reduction`
  ADD PRIMARY KEY (`id_group_reduction`),
  ADD UNIQUE KEY `id_group` (`id_group`,`id_category`);

--
-- Indeksy dla tabeli `ps_group_shop`
--
ALTER TABLE `ps_group_shop`
  ADD PRIMARY KEY (`id_group`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indeksy dla tabeli `ps_guest`
--
ALTER TABLE `ps_guest`
  ADD PRIMARY KEY (`id_guest`),
  ADD KEY `id_customer` (`id_customer`),
  ADD KEY `id_operating_system` (`id_operating_system`),
  ADD KEY `id_web_browser` (`id_web_browser`);

--
-- Indeksy dla tabeli `ps_homeslider`
--
ALTER TABLE `ps_homeslider`
  ADD PRIMARY KEY (`id_homeslider_slides`,`id_shop`);

--
-- Indeksy dla tabeli `ps_homeslider_slides`
--
ALTER TABLE `ps_homeslider_slides`
  ADD PRIMARY KEY (`id_homeslider_slides`);

--
-- Indeksy dla tabeli `ps_homeslider_slides_lang`
--
ALTER TABLE `ps_homeslider_slides_lang`
  ADD PRIMARY KEY (`id_homeslider_slides`,`id_lang`);

--
-- Indeksy dla tabeli `ps_hook`
--
ALTER TABLE `ps_hook`
  ADD PRIMARY KEY (`id_hook`),
  ADD UNIQUE KEY `hook_name` (`name`);

--
-- Indeksy dla tabeli `ps_hook_alias`
--
ALTER TABLE `ps_hook_alias`
  ADD PRIMARY KEY (`id_hook_alias`),
  ADD UNIQUE KEY `alias` (`alias`);

--
-- Indeksy dla tabeli `ps_hook_module`
--
ALTER TABLE `ps_hook_module`
  ADD PRIMARY KEY (`id_module`,`id_hook`,`id_shop`),
  ADD KEY `id_hook` (`id_hook`),
  ADD KEY `id_module` (`id_module`),
  ADD KEY `position` (`id_shop`,`position`);

--
-- Indeksy dla tabeli `ps_hook_module_exceptions`
--
ALTER TABLE `ps_hook_module_exceptions`
  ADD PRIMARY KEY (`id_hook_module_exceptions`),
  ADD KEY `id_module` (`id_module`),
  ADD KEY `id_hook` (`id_hook`);

--
-- Indeksy dla tabeli `ps_image`
--
ALTER TABLE `ps_image`
  ADD PRIMARY KEY (`id_image`),
  ADD UNIQUE KEY `id_product_cover` (`id_product`,`cover`),
  ADD UNIQUE KEY `idx_product_image` (`id_image`,`id_product`,`cover`),
  ADD KEY `image_product` (`id_product`);

--
-- Indeksy dla tabeli `ps_image_lang`
--
ALTER TABLE `ps_image_lang`
  ADD PRIMARY KEY (`id_image`,`id_lang`),
  ADD KEY `id_image` (`id_image`);

--
-- Indeksy dla tabeli `ps_image_shop`
--
ALTER TABLE `ps_image_shop`
  ADD PRIMARY KEY (`id_image`,`id_shop`),
  ADD UNIQUE KEY `id_product` (`id_product`,`id_shop`,`cover`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indeksy dla tabeli `ps_image_type`
--
ALTER TABLE `ps_image_type`
  ADD PRIMARY KEY (`id_image_type`),
  ADD KEY `image_type_name` (`name`);

--
-- Indeksy dla tabeli `ps_import_match`
--
ALTER TABLE `ps_import_match`
  ADD PRIMARY KEY (`id_import_match`);

--
-- Indeksy dla tabeli `ps_info`
--
ALTER TABLE `ps_info`
  ADD PRIMARY KEY (`id_info`);

--
-- Indeksy dla tabeli `ps_info_lang`
--
ALTER TABLE `ps_info_lang`
  ADD PRIMARY KEY (`id_info`,`id_lang`);

--
-- Indeksy dla tabeli `ps_lang`
--
ALTER TABLE `ps_lang`
  ADD PRIMARY KEY (`id_lang`),
  ADD KEY `lang_iso_code` (`iso_code`);

--
-- Indeksy dla tabeli `ps_lang_shop`
--
ALTER TABLE `ps_lang_shop`
  ADD PRIMARY KEY (`id_lang`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indeksy dla tabeli `ps_layered_category`
--
ALTER TABLE `ps_layered_category`
  ADD PRIMARY KEY (`id_layered_category`),
  ADD KEY `id_category` (`id_category`,`type`);

--
-- Indeksy dla tabeli `ps_layered_filter`
--
ALTER TABLE `ps_layered_filter`
  ADD PRIMARY KEY (`id_layered_filter`);

--
-- Indeksy dla tabeli `ps_layered_filter_shop`
--
ALTER TABLE `ps_layered_filter_shop`
  ADD PRIMARY KEY (`id_layered_filter`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indeksy dla tabeli `ps_layered_friendly_url`
--
ALTER TABLE `ps_layered_friendly_url`
  ADD PRIMARY KEY (`id_layered_friendly_url`),
  ADD KEY `id_lang` (`id_lang`),
  ADD KEY `url_key` (`url_key`(5));

--
-- Indeksy dla tabeli `ps_layered_indexable_attribute_group`
--
ALTER TABLE `ps_layered_indexable_attribute_group`
  ADD PRIMARY KEY (`id_attribute_group`);

--
-- Indeksy dla tabeli `ps_layered_indexable_attribute_group_lang_value`
--
ALTER TABLE `ps_layered_indexable_attribute_group_lang_value`
  ADD PRIMARY KEY (`id_attribute_group`,`id_lang`);

--
-- Indeksy dla tabeli `ps_layered_indexable_attribute_lang_value`
--
ALTER TABLE `ps_layered_indexable_attribute_lang_value`
  ADD PRIMARY KEY (`id_attribute`,`id_lang`);

--
-- Indeksy dla tabeli `ps_layered_indexable_feature`
--
ALTER TABLE `ps_layered_indexable_feature`
  ADD PRIMARY KEY (`id_feature`);

--
-- Indeksy dla tabeli `ps_layered_indexable_feature_lang_value`
--
ALTER TABLE `ps_layered_indexable_feature_lang_value`
  ADD PRIMARY KEY (`id_feature`,`id_lang`);

--
-- Indeksy dla tabeli `ps_layered_indexable_feature_value_lang_value`
--
ALTER TABLE `ps_layered_indexable_feature_value_lang_value`
  ADD PRIMARY KEY (`id_feature_value`,`id_lang`);

--
-- Indeksy dla tabeli `ps_layered_price_index`
--
ALTER TABLE `ps_layered_price_index`
  ADD PRIMARY KEY (`id_product`,`id_currency`,`id_shop`),
  ADD KEY `id_currency` (`id_currency`),
  ADD KEY `price_min` (`price_min`),
  ADD KEY `price_max` (`price_max`);

--
-- Indeksy dla tabeli `ps_layered_product_attribute`
--
ALTER TABLE `ps_layered_product_attribute`
  ADD PRIMARY KEY (`id_attribute`,`id_product`,`id_shop`),
  ADD UNIQUE KEY `id_attribute_group` (`id_attribute_group`,`id_attribute`,`id_product`,`id_shop`);

--
-- Indeksy dla tabeli `ps_linksmenutop`
--
ALTER TABLE `ps_linksmenutop`
  ADD PRIMARY KEY (`id_linksmenutop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indeksy dla tabeli `ps_linksmenutop_lang`
--
ALTER TABLE `ps_linksmenutop_lang`
  ADD KEY `id_linksmenutop` (`id_linksmenutop`,`id_lang`,`id_shop`);

--
-- Indeksy dla tabeli `ps_log`
--
ALTER TABLE `ps_log`
  ADD PRIMARY KEY (`id_log`);

--
-- Indeksy dla tabeli `ps_mail`
--
ALTER TABLE `ps_mail`
  ADD PRIMARY KEY (`id_mail`),
  ADD KEY `recipient` (`recipient`(10));

--
-- Indeksy dla tabeli `ps_manufacturer`
--
ALTER TABLE `ps_manufacturer`
  ADD PRIMARY KEY (`id_manufacturer`);

--
-- Indeksy dla tabeli `ps_manufacturer_lang`
--
ALTER TABLE `ps_manufacturer_lang`
  ADD PRIMARY KEY (`id_manufacturer`,`id_lang`);

--
-- Indeksy dla tabeli `ps_manufacturer_shop`
--
ALTER TABLE `ps_manufacturer_shop`
  ADD PRIMARY KEY (`id_manufacturer`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indeksy dla tabeli `ps_memcached_servers`
--
ALTER TABLE `ps_memcached_servers`
  ADD PRIMARY KEY (`id_memcached_server`);

--
-- Indeksy dla tabeli `ps_message`
--
ALTER TABLE `ps_message`
  ADD PRIMARY KEY (`id_message`),
  ADD KEY `message_order` (`id_order`),
  ADD KEY `id_cart` (`id_cart`),
  ADD KEY `id_customer` (`id_customer`),
  ADD KEY `id_employee` (`id_employee`);

--
-- Indeksy dla tabeli `ps_message_readed`
--
ALTER TABLE `ps_message_readed`
  ADD PRIMARY KEY (`id_message`,`id_employee`);

--
-- Indeksy dla tabeli `ps_meta`
--
ALTER TABLE `ps_meta`
  ADD PRIMARY KEY (`id_meta`),
  ADD UNIQUE KEY `page` (`page`);

--
-- Indeksy dla tabeli `ps_meta_lang`
--
ALTER TABLE `ps_meta_lang`
  ADD PRIMARY KEY (`id_meta`,`id_shop`,`id_lang`),
  ADD KEY `id_shop` (`id_shop`),
  ADD KEY `id_lang` (`id_lang`);

--
-- Indeksy dla tabeli `ps_module`
--
ALTER TABLE `ps_module`
  ADD PRIMARY KEY (`id_module`),
  ADD KEY `name` (`name`);

--
-- Indeksy dla tabeli `ps_modules_perfs`
--
ALTER TABLE `ps_modules_perfs`
  ADD PRIMARY KEY (`id_modules_perfs`),
  ADD KEY `session` (`session`);

--
-- Indeksy dla tabeli `ps_module_access`
--
ALTER TABLE `ps_module_access`
  ADD PRIMARY KEY (`id_profile`,`id_module`);

--
-- Indeksy dla tabeli `ps_module_country`
--
ALTER TABLE `ps_module_country`
  ADD PRIMARY KEY (`id_module`,`id_shop`,`id_country`);

--
-- Indeksy dla tabeli `ps_module_currency`
--
ALTER TABLE `ps_module_currency`
  ADD PRIMARY KEY (`id_module`,`id_shop`,`id_currency`),
  ADD KEY `id_module` (`id_module`);

--
-- Indeksy dla tabeli `ps_module_group`
--
ALTER TABLE `ps_module_group`
  ADD PRIMARY KEY (`id_module`,`id_shop`,`id_group`);

--
-- Indeksy dla tabeli `ps_module_preference`
--
ALTER TABLE `ps_module_preference`
  ADD PRIMARY KEY (`id_module_preference`),
  ADD UNIQUE KEY `employee_module` (`id_employee`,`module`);

--
-- Indeksy dla tabeli `ps_module_shop`
--
ALTER TABLE `ps_module_shop`
  ADD PRIMARY KEY (`id_module`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indeksy dla tabeli `ps_newsletter`
--
ALTER TABLE `ps_newsletter`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `ps_operating_system`
--
ALTER TABLE `ps_operating_system`
  ADD PRIMARY KEY (`id_operating_system`);

--
-- Indeksy dla tabeli `ps_orders`
--
ALTER TABLE `ps_orders`
  ADD PRIMARY KEY (`id_order`),
  ADD KEY `reference` (`reference`),
  ADD KEY `id_customer` (`id_customer`),
  ADD KEY `id_cart` (`id_cart`),
  ADD KEY `invoice_number` (`invoice_number`),
  ADD KEY `id_carrier` (`id_carrier`),
  ADD KEY `id_lang` (`id_lang`),
  ADD KEY `id_currency` (`id_currency`),
  ADD KEY `id_address_delivery` (`id_address_delivery`),
  ADD KEY `id_address_invoice` (`id_address_invoice`),
  ADD KEY `id_shop_group` (`id_shop_group`),
  ADD KEY `current_state` (`current_state`),
  ADD KEY `id_shop` (`id_shop`),
  ADD KEY `date_add` (`date_add`);

--
-- Indeksy dla tabeli `ps_order_carrier`
--
ALTER TABLE `ps_order_carrier`
  ADD PRIMARY KEY (`id_order_carrier`),
  ADD KEY `id_order` (`id_order`),
  ADD KEY `id_carrier` (`id_carrier`),
  ADD KEY `id_order_invoice` (`id_order_invoice`);

--
-- Indeksy dla tabeli `ps_order_cart_rule`
--
ALTER TABLE `ps_order_cart_rule`
  ADD PRIMARY KEY (`id_order_cart_rule`),
  ADD KEY `id_order` (`id_order`),
  ADD KEY `id_cart_rule` (`id_cart_rule`);

--
-- Indeksy dla tabeli `ps_order_detail`
--
ALTER TABLE `ps_order_detail`
  ADD PRIMARY KEY (`id_order_detail`),
  ADD KEY `order_detail_order` (`id_order`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `product_attribute_id` (`product_attribute_id`),
  ADD KEY `id_tax_rules_group` (`id_tax_rules_group`),
  ADD KEY `id_order_id_order_detail` (`id_order`,`id_order_detail`);

--
-- Indeksy dla tabeli `ps_order_detail_tax`
--
ALTER TABLE `ps_order_detail_tax`
  ADD KEY `id_order_detail` (`id_order_detail`),
  ADD KEY `id_tax` (`id_tax`);

--
-- Indeksy dla tabeli `ps_order_history`
--
ALTER TABLE `ps_order_history`
  ADD PRIMARY KEY (`id_order_history`),
  ADD KEY `order_history_order` (`id_order`),
  ADD KEY `id_employee` (`id_employee`),
  ADD KEY `id_order_state` (`id_order_state`);

--
-- Indeksy dla tabeli `ps_order_invoice`
--
ALTER TABLE `ps_order_invoice`
  ADD PRIMARY KEY (`id_order_invoice`),
  ADD KEY `id_order` (`id_order`);

--
-- Indeksy dla tabeli `ps_order_invoice_payment`
--
ALTER TABLE `ps_order_invoice_payment`
  ADD PRIMARY KEY (`id_order_invoice`,`id_order_payment`),
  ADD KEY `order_payment` (`id_order_payment`),
  ADD KEY `id_order` (`id_order`);

--
-- Indeksy dla tabeli `ps_order_invoice_tax`
--
ALTER TABLE `ps_order_invoice_tax`
  ADD KEY `id_tax` (`id_tax`);

--
-- Indeksy dla tabeli `ps_order_message`
--
ALTER TABLE `ps_order_message`
  ADD PRIMARY KEY (`id_order_message`);

--
-- Indeksy dla tabeli `ps_order_message_lang`
--
ALTER TABLE `ps_order_message_lang`
  ADD PRIMARY KEY (`id_order_message`,`id_lang`);

--
-- Indeksy dla tabeli `ps_order_payment`
--
ALTER TABLE `ps_order_payment`
  ADD PRIMARY KEY (`id_order_payment`),
  ADD KEY `order_reference` (`order_reference`);

--
-- Indeksy dla tabeli `ps_order_return`
--
ALTER TABLE `ps_order_return`
  ADD PRIMARY KEY (`id_order_return`),
  ADD KEY `order_return_customer` (`id_customer`),
  ADD KEY `id_order` (`id_order`);

--
-- Indeksy dla tabeli `ps_order_return_detail`
--
ALTER TABLE `ps_order_return_detail`
  ADD PRIMARY KEY (`id_order_return`,`id_order_detail`,`id_customization`);

--
-- Indeksy dla tabeli `ps_order_return_state`
--
ALTER TABLE `ps_order_return_state`
  ADD PRIMARY KEY (`id_order_return_state`);

--
-- Indeksy dla tabeli `ps_order_return_state_lang`
--
ALTER TABLE `ps_order_return_state_lang`
  ADD PRIMARY KEY (`id_order_return_state`,`id_lang`);

--
-- Indeksy dla tabeli `ps_order_slip`
--
ALTER TABLE `ps_order_slip`
  ADD PRIMARY KEY (`id_order_slip`),
  ADD KEY `order_slip_customer` (`id_customer`),
  ADD KEY `id_order` (`id_order`);

--
-- Indeksy dla tabeli `ps_order_slip_detail`
--
ALTER TABLE `ps_order_slip_detail`
  ADD PRIMARY KEY (`id_order_slip`,`id_order_detail`);

--
-- Indeksy dla tabeli `ps_order_slip_detail_tax`
--
ALTER TABLE `ps_order_slip_detail_tax`
  ADD KEY `id_order_slip_detail` (`id_order_slip_detail`),
  ADD KEY `id_tax` (`id_tax`);

--
-- Indeksy dla tabeli `ps_order_state`
--
ALTER TABLE `ps_order_state`
  ADD PRIMARY KEY (`id_order_state`),
  ADD KEY `module_name` (`module_name`);

--
-- Indeksy dla tabeli `ps_order_state_lang`
--
ALTER TABLE `ps_order_state_lang`
  ADD PRIMARY KEY (`id_order_state`,`id_lang`);

--
-- Indeksy dla tabeli `ps_pack`
--
ALTER TABLE `ps_pack`
  ADD PRIMARY KEY (`id_product_pack`,`id_product_item`,`id_product_attribute_item`),
  ADD KEY `product_item` (`id_product_item`,`id_product_attribute_item`);

--
-- Indeksy dla tabeli `ps_page`
--
ALTER TABLE `ps_page`
  ADD PRIMARY KEY (`id_page`),
  ADD KEY `id_page_type` (`id_page_type`),
  ADD KEY `id_object` (`id_object`);

--
-- Indeksy dla tabeli `ps_pagenotfound`
--
ALTER TABLE `ps_pagenotfound`
  ADD PRIMARY KEY (`id_pagenotfound`),
  ADD KEY `date_add` (`date_add`);

--
-- Indeksy dla tabeli `ps_page_type`
--
ALTER TABLE `ps_page_type`
  ADD PRIMARY KEY (`id_page_type`),
  ADD KEY `name` (`name`);

--
-- Indeksy dla tabeli `ps_page_viewed`
--
ALTER TABLE `ps_page_viewed`
  ADD PRIMARY KEY (`id_page`,`id_date_range`,`id_shop`);

--
-- Indeksy dla tabeli `ps_product`
--
ALTER TABLE `ps_product`
  ADD PRIMARY KEY (`id_product`),
  ADD KEY `product_supplier` (`id_supplier`),
  ADD KEY `product_manufacturer` (`id_manufacturer`,`id_product`),
  ADD KEY `id_category_default` (`id_category_default`),
  ADD KEY `indexed` (`indexed`),
  ADD KEY `date_add` (`date_add`);

--
-- Indeksy dla tabeli `ps_product_attachment`
--
ALTER TABLE `ps_product_attachment`
  ADD PRIMARY KEY (`id_product`,`id_attachment`);

--
-- Indeksy dla tabeli `ps_product_attribute`
--
ALTER TABLE `ps_product_attribute`
  ADD PRIMARY KEY (`id_product_attribute`),
  ADD UNIQUE KEY `product_default` (`id_product`,`default_on`),
  ADD KEY `product_attribute_product` (`id_product`),
  ADD KEY `reference` (`reference`),
  ADD KEY `supplier_reference` (`supplier_reference`),
  ADD KEY `id_product_id_product_attribute` (`id_product_attribute`,`id_product`);

--
-- Indeksy dla tabeli `ps_product_attribute_combination`
--
ALTER TABLE `ps_product_attribute_combination`
  ADD PRIMARY KEY (`id_attribute`,`id_product_attribute`),
  ADD KEY `id_product_attribute` (`id_product_attribute`);

--
-- Indeksy dla tabeli `ps_product_attribute_image`
--
ALTER TABLE `ps_product_attribute_image`
  ADD PRIMARY KEY (`id_product_attribute`,`id_image`),
  ADD KEY `id_image` (`id_image`);

--
-- Indeksy dla tabeli `ps_product_attribute_shop`
--
ALTER TABLE `ps_product_attribute_shop`
  ADD PRIMARY KEY (`id_product_attribute`,`id_shop`),
  ADD UNIQUE KEY `id_product` (`id_product`,`id_shop`,`default_on`);

--
-- Indeksy dla tabeli `ps_product_carrier`
--
ALTER TABLE `ps_product_carrier`
  ADD PRIMARY KEY (`id_product`,`id_carrier_reference`,`id_shop`);

--
-- Indeksy dla tabeli `ps_product_country_tax`
--
ALTER TABLE `ps_product_country_tax`
  ADD PRIMARY KEY (`id_product`,`id_country`);

--
-- Indeksy dla tabeli `ps_product_download`
--
ALTER TABLE `ps_product_download`
  ADD PRIMARY KEY (`id_product_download`),
  ADD UNIQUE KEY `id_product` (`id_product`),
  ADD KEY `product_active` (`id_product`,`active`);

--
-- Indeksy dla tabeli `ps_product_group_reduction_cache`
--
ALTER TABLE `ps_product_group_reduction_cache`
  ADD PRIMARY KEY (`id_product`,`id_group`);

--
-- Indeksy dla tabeli `ps_product_lang`
--
ALTER TABLE `ps_product_lang`
  ADD PRIMARY KEY (`id_product`,`id_shop`,`id_lang`),
  ADD KEY `id_lang` (`id_lang`),
  ADD KEY `name` (`name`);

--
-- Indeksy dla tabeli `ps_product_sale`
--
ALTER TABLE `ps_product_sale`
  ADD PRIMARY KEY (`id_product`),
  ADD KEY `quantity` (`quantity`);

--
-- Indeksy dla tabeli `ps_product_shop`
--
ALTER TABLE `ps_product_shop`
  ADD PRIMARY KEY (`id_product`,`id_shop`),
  ADD KEY `id_category_default` (`id_category_default`),
  ADD KEY `date_add` (`date_add`,`active`,`visibility`),
  ADD KEY `indexed` (`indexed`,`active`,`id_product`);

--
-- Indeksy dla tabeli `ps_product_supplier`
--
ALTER TABLE `ps_product_supplier`
  ADD PRIMARY KEY (`id_product_supplier`),
  ADD UNIQUE KEY `id_product` (`id_product`,`id_product_attribute`,`id_supplier`),
  ADD KEY `id_supplier` (`id_supplier`,`id_product`);

--
-- Indeksy dla tabeli `ps_product_tag`
--
ALTER TABLE `ps_product_tag`
  ADD PRIMARY KEY (`id_product`,`id_tag`),
  ADD KEY `id_tag` (`id_tag`),
  ADD KEY `id_lang` (`id_lang`,`id_tag`);

--
-- Indeksy dla tabeli `ps_profile`
--
ALTER TABLE `ps_profile`
  ADD PRIMARY KEY (`id_profile`);

--
-- Indeksy dla tabeli `ps_profile_lang`
--
ALTER TABLE `ps_profile_lang`
  ADD PRIMARY KEY (`id_profile`,`id_lang`);

--
-- Indeksy dla tabeli `ps_quick_access`
--
ALTER TABLE `ps_quick_access`
  ADD PRIMARY KEY (`id_quick_access`);

--
-- Indeksy dla tabeli `ps_quick_access_lang`
--
ALTER TABLE `ps_quick_access_lang`
  ADD PRIMARY KEY (`id_quick_access`,`id_lang`);

--
-- Indeksy dla tabeli `ps_range_price`
--
ALTER TABLE `ps_range_price`
  ADD PRIMARY KEY (`id_range_price`),
  ADD UNIQUE KEY `id_carrier` (`id_carrier`,`delimiter1`,`delimiter2`);

--
-- Indeksy dla tabeli `ps_range_weight`
--
ALTER TABLE `ps_range_weight`
  ADD PRIMARY KEY (`id_range_weight`),
  ADD UNIQUE KEY `id_carrier` (`id_carrier`,`delimiter1`,`delimiter2`);

--
-- Indeksy dla tabeli `ps_referrer`
--
ALTER TABLE `ps_referrer`
  ADD PRIMARY KEY (`id_referrer`);

--
-- Indeksy dla tabeli `ps_referrer_cache`
--
ALTER TABLE `ps_referrer_cache`
  ADD PRIMARY KEY (`id_connections_source`,`id_referrer`);

--
-- Indeksy dla tabeli `ps_referrer_shop`
--
ALTER TABLE `ps_referrer_shop`
  ADD PRIMARY KEY (`id_referrer`,`id_shop`);

--
-- Indeksy dla tabeli `ps_request_sql`
--
ALTER TABLE `ps_request_sql`
  ADD PRIMARY KEY (`id_request_sql`);

--
-- Indeksy dla tabeli `ps_required_field`
--
ALTER TABLE `ps_required_field`
  ADD PRIMARY KEY (`id_required_field`),
  ADD KEY `object_name` (`object_name`);

--
-- Indeksy dla tabeli `ps_risk`
--
ALTER TABLE `ps_risk`
  ADD PRIMARY KEY (`id_risk`);

--
-- Indeksy dla tabeli `ps_risk_lang`
--
ALTER TABLE `ps_risk_lang`
  ADD PRIMARY KEY (`id_risk`,`id_lang`),
  ADD KEY `id_risk` (`id_risk`);

--
-- Indeksy dla tabeli `ps_scene`
--
ALTER TABLE `ps_scene`
  ADD PRIMARY KEY (`id_scene`);

--
-- Indeksy dla tabeli `ps_scene_category`
--
ALTER TABLE `ps_scene_category`
  ADD PRIMARY KEY (`id_scene`,`id_category`);

--
-- Indeksy dla tabeli `ps_scene_lang`
--
ALTER TABLE `ps_scene_lang`
  ADD PRIMARY KEY (`id_scene`,`id_lang`);

--
-- Indeksy dla tabeli `ps_scene_products`
--
ALTER TABLE `ps_scene_products`
  ADD PRIMARY KEY (`id_scene`,`id_product`,`x_axis`,`y_axis`);

--
-- Indeksy dla tabeli `ps_scene_shop`
--
ALTER TABLE `ps_scene_shop`
  ADD PRIMARY KEY (`id_scene`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indeksy dla tabeli `ps_search_engine`
--
ALTER TABLE `ps_search_engine`
  ADD PRIMARY KEY (`id_search_engine`);

--
-- Indeksy dla tabeli `ps_search_index`
--
ALTER TABLE `ps_search_index`
  ADD PRIMARY KEY (`id_word`,`id_product`),
  ADD KEY `id_product` (`id_product`,`weight`);

--
-- Indeksy dla tabeli `ps_search_word`
--
ALTER TABLE `ps_search_word`
  ADD PRIMARY KEY (`id_word`),
  ADD UNIQUE KEY `id_lang` (`id_lang`,`id_shop`,`word`);

--
-- Indeksy dla tabeli `ps_sekeyword`
--
ALTER TABLE `ps_sekeyword`
  ADD PRIMARY KEY (`id_sekeyword`);

--
-- Indeksy dla tabeli `ps_shop`
--
ALTER TABLE `ps_shop`
  ADD PRIMARY KEY (`id_shop`),
  ADD KEY `id_shop_group` (`id_shop_group`,`deleted`),
  ADD KEY `id_category` (`id_category`),
  ADD KEY `id_theme` (`id_theme`);

--
-- Indeksy dla tabeli `ps_shop_group`
--
ALTER TABLE `ps_shop_group`
  ADD PRIMARY KEY (`id_shop_group`),
  ADD KEY `deleted` (`deleted`,`name`);

--
-- Indeksy dla tabeli `ps_shop_url`
--
ALTER TABLE `ps_shop_url`
  ADD PRIMARY KEY (`id_shop_url`),
  ADD UNIQUE KEY `full_shop_url` (`domain`,`physical_uri`,`virtual_uri`),
  ADD UNIQUE KEY `full_shop_url_ssl` (`domain_ssl`,`physical_uri`,`virtual_uri`),
  ADD KEY `id_shop` (`id_shop`,`main`);

--
-- Indeksy dla tabeli `ps_smarty_cache`
--
ALTER TABLE `ps_smarty_cache`
  ADD PRIMARY KEY (`id_smarty_cache`),
  ADD KEY `name` (`name`),
  ADD KEY `cache_id` (`cache_id`),
  ADD KEY `modified` (`modified`);

--
-- Indeksy dla tabeli `ps_smarty_last_flush`
--
ALTER TABLE `ps_smarty_last_flush`
  ADD PRIMARY KEY (`type`);

--
-- Indeksy dla tabeli `ps_smarty_lazy_cache`
--
ALTER TABLE `ps_smarty_lazy_cache`
  ADD PRIMARY KEY (`template_hash`,`cache_id`,`compile_id`);

--
-- Indeksy dla tabeli `ps_specific_price`
--
ALTER TABLE `ps_specific_price`
  ADD PRIMARY KEY (`id_specific_price`),
  ADD UNIQUE KEY `id_product_2` (`id_product`,`id_product_attribute`,`id_customer`,`id_cart`,`from`,`to`,`id_shop`,`id_shop_group`,`id_currency`,`id_country`,`id_group`,`from_quantity`,`id_specific_price_rule`),
  ADD KEY `id_product` (`id_product`,`id_shop`,`id_currency`,`id_country`,`id_group`,`id_customer`,`from_quantity`,`from`,`to`),
  ADD KEY `from_quantity` (`from_quantity`),
  ADD KEY `id_specific_price_rule` (`id_specific_price_rule`),
  ADD KEY `id_cart` (`id_cart`),
  ADD KEY `id_product_attribute` (`id_product_attribute`),
  ADD KEY `id_shop` (`id_shop`),
  ADD KEY `id_customer` (`id_customer`),
  ADD KEY `from` (`from`),
  ADD KEY `to` (`to`);

--
-- Indeksy dla tabeli `ps_specific_price_priority`
--
ALTER TABLE `ps_specific_price_priority`
  ADD PRIMARY KEY (`id_specific_price_priority`,`id_product`),
  ADD UNIQUE KEY `id_product` (`id_product`);

--
-- Indeksy dla tabeli `ps_specific_price_rule`
--
ALTER TABLE `ps_specific_price_rule`
  ADD PRIMARY KEY (`id_specific_price_rule`),
  ADD KEY `id_product` (`id_shop`,`id_currency`,`id_country`,`id_group`,`from_quantity`,`from`,`to`);

--
-- Indeksy dla tabeli `ps_specific_price_rule_condition`
--
ALTER TABLE `ps_specific_price_rule_condition`
  ADD PRIMARY KEY (`id_specific_price_rule_condition`),
  ADD KEY `id_specific_price_rule_condition_group` (`id_specific_price_rule_condition_group`);

--
-- Indeksy dla tabeli `ps_specific_price_rule_condition_group`
--
ALTER TABLE `ps_specific_price_rule_condition_group`
  ADD PRIMARY KEY (`id_specific_price_rule_condition_group`,`id_specific_price_rule`);

--
-- Indeksy dla tabeli `ps_state`
--
ALTER TABLE `ps_state`
  ADD PRIMARY KEY (`id_state`),
  ADD KEY `id_country` (`id_country`),
  ADD KEY `name` (`name`),
  ADD KEY `id_zone` (`id_zone`);

--
-- Indeksy dla tabeli `ps_statssearch`
--
ALTER TABLE `ps_statssearch`
  ADD PRIMARY KEY (`id_statssearch`);

--
-- Indeksy dla tabeli `ps_stock`
--
ALTER TABLE `ps_stock`
  ADD PRIMARY KEY (`id_stock`),
  ADD KEY `id_warehouse` (`id_warehouse`),
  ADD KEY `id_product` (`id_product`),
  ADD KEY `id_product_attribute` (`id_product_attribute`);

--
-- Indeksy dla tabeli `ps_stock_available`
--
ALTER TABLE `ps_stock_available`
  ADD PRIMARY KEY (`id_stock_available`),
  ADD UNIQUE KEY `product_sqlstock` (`id_product`,`id_product_attribute`,`id_shop`,`id_shop_group`),
  ADD KEY `id_shop` (`id_shop`),
  ADD KEY `id_shop_group` (`id_shop_group`),
  ADD KEY `id_product` (`id_product`),
  ADD KEY `id_product_attribute` (`id_product_attribute`);

--
-- Indeksy dla tabeli `ps_stock_mvt`
--
ALTER TABLE `ps_stock_mvt`
  ADD PRIMARY KEY (`id_stock_mvt`),
  ADD KEY `id_stock` (`id_stock`),
  ADD KEY `id_stock_mvt_reason` (`id_stock_mvt_reason`);

--
-- Indeksy dla tabeli `ps_stock_mvt_reason`
--
ALTER TABLE `ps_stock_mvt_reason`
  ADD PRIMARY KEY (`id_stock_mvt_reason`);

--
-- Indeksy dla tabeli `ps_stock_mvt_reason_lang`
--
ALTER TABLE `ps_stock_mvt_reason_lang`
  ADD PRIMARY KEY (`id_stock_mvt_reason`,`id_lang`);

--
-- Indeksy dla tabeli `ps_store`
--
ALTER TABLE `ps_store`
  ADD PRIMARY KEY (`id_store`);

--
-- Indeksy dla tabeli `ps_store_shop`
--
ALTER TABLE `ps_store_shop`
  ADD PRIMARY KEY (`id_store`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indeksy dla tabeli `ps_supplier`
--
ALTER TABLE `ps_supplier`
  ADD PRIMARY KEY (`id_supplier`);

--
-- Indeksy dla tabeli `ps_supplier_lang`
--
ALTER TABLE `ps_supplier_lang`
  ADD PRIMARY KEY (`id_supplier`,`id_lang`);

--
-- Indeksy dla tabeli `ps_supplier_shop`
--
ALTER TABLE `ps_supplier_shop`
  ADD PRIMARY KEY (`id_supplier`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indeksy dla tabeli `ps_supply_order`
--
ALTER TABLE `ps_supply_order`
  ADD PRIMARY KEY (`id_supply_order`),
  ADD KEY `id_supplier` (`id_supplier`),
  ADD KEY `id_warehouse` (`id_warehouse`),
  ADD KEY `reference` (`reference`);

--
-- Indeksy dla tabeli `ps_supply_order_detail`
--
ALTER TABLE `ps_supply_order_detail`
  ADD PRIMARY KEY (`id_supply_order_detail`),
  ADD KEY `id_supply_order` (`id_supply_order`,`id_product`),
  ADD KEY `id_product_attribute` (`id_product_attribute`),
  ADD KEY `id_product_product_attribute` (`id_product`,`id_product_attribute`);

--
-- Indeksy dla tabeli `ps_supply_order_history`
--
ALTER TABLE `ps_supply_order_history`
  ADD PRIMARY KEY (`id_supply_order_history`),
  ADD KEY `id_supply_order` (`id_supply_order`),
  ADD KEY `id_employee` (`id_employee`),
  ADD KEY `id_state` (`id_state`);

--
-- Indeksy dla tabeli `ps_supply_order_receipt_history`
--
ALTER TABLE `ps_supply_order_receipt_history`
  ADD PRIMARY KEY (`id_supply_order_receipt_history`),
  ADD KEY `id_supply_order_detail` (`id_supply_order_detail`),
  ADD KEY `id_supply_order_state` (`id_supply_order_state`);

--
-- Indeksy dla tabeli `ps_supply_order_state`
--
ALTER TABLE `ps_supply_order_state`
  ADD PRIMARY KEY (`id_supply_order_state`);

--
-- Indeksy dla tabeli `ps_supply_order_state_lang`
--
ALTER TABLE `ps_supply_order_state_lang`
  ADD PRIMARY KEY (`id_supply_order_state`,`id_lang`);

--
-- Indeksy dla tabeli `ps_tab`
--
ALTER TABLE `ps_tab`
  ADD PRIMARY KEY (`id_tab`),
  ADD KEY `class_name` (`class_name`),
  ADD KEY `id_parent` (`id_parent`);

--
-- Indeksy dla tabeli `ps_tab_advice`
--
ALTER TABLE `ps_tab_advice`
  ADD PRIMARY KEY (`id_tab`,`id_advice`);

--
-- Indeksy dla tabeli `ps_tab_lang`
--
ALTER TABLE `ps_tab_lang`
  ADD PRIMARY KEY (`id_tab`,`id_lang`);

--
-- Indeksy dla tabeli `ps_tab_module_preference`
--
ALTER TABLE `ps_tab_module_preference`
  ADD PRIMARY KEY (`id_tab_module_preference`),
  ADD UNIQUE KEY `employee_module` (`id_employee`,`id_tab`,`module`);

--
-- Indeksy dla tabeli `ps_tag`
--
ALTER TABLE `ps_tag`
  ADD PRIMARY KEY (`id_tag`),
  ADD KEY `tag_name` (`name`),
  ADD KEY `id_lang` (`id_lang`);

--
-- Indeksy dla tabeli `ps_tag_count`
--
ALTER TABLE `ps_tag_count`
  ADD PRIMARY KEY (`id_group`,`id_tag`),
  ADD KEY `id_group` (`id_group`,`id_lang`,`id_shop`,`counter`);

--
-- Indeksy dla tabeli `ps_tax`
--
ALTER TABLE `ps_tax`
  ADD PRIMARY KEY (`id_tax`);

--
-- Indeksy dla tabeli `ps_tax_lang`
--
ALTER TABLE `ps_tax_lang`
  ADD PRIMARY KEY (`id_tax`,`id_lang`);

--
-- Indeksy dla tabeli `ps_tax_rule`
--
ALTER TABLE `ps_tax_rule`
  ADD PRIMARY KEY (`id_tax_rule`),
  ADD KEY `id_tax_rules_group` (`id_tax_rules_group`),
  ADD KEY `id_tax` (`id_tax`),
  ADD KEY `category_getproducts` (`id_tax_rules_group`,`id_country`,`id_state`,`zipcode_from`);

--
-- Indeksy dla tabeli `ps_tax_rules_group`
--
ALTER TABLE `ps_tax_rules_group`
  ADD PRIMARY KEY (`id_tax_rules_group`);

--
-- Indeksy dla tabeli `ps_tax_rules_group_shop`
--
ALTER TABLE `ps_tax_rules_group_shop`
  ADD PRIMARY KEY (`id_tax_rules_group`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indeksy dla tabeli `ps_theme`
--
ALTER TABLE `ps_theme`
  ADD PRIMARY KEY (`id_theme`);

--
-- Indeksy dla tabeli `ps_themeconfigurator`
--
ALTER TABLE `ps_themeconfigurator`
  ADD PRIMARY KEY (`id_item`);

--
-- Indeksy dla tabeli `ps_theme_meta`
--
ALTER TABLE `ps_theme_meta`
  ADD PRIMARY KEY (`id_theme_meta`),
  ADD UNIQUE KEY `id_theme_2` (`id_theme`,`id_meta`),
  ADD KEY `id_theme` (`id_theme`),
  ADD KEY `id_meta` (`id_meta`);

--
-- Indeksy dla tabeli `ps_theme_specific`
--
ALTER TABLE `ps_theme_specific`
  ADD PRIMARY KEY (`id_theme`,`id_shop`,`entity`,`id_object`);

--
-- Indeksy dla tabeli `ps_timezone`
--
ALTER TABLE `ps_timezone`
  ADD PRIMARY KEY (`id_timezone`);

--
-- Indeksy dla tabeli `ps_warehouse`
--
ALTER TABLE `ps_warehouse`
  ADD PRIMARY KEY (`id_warehouse`);

--
-- Indeksy dla tabeli `ps_warehouse_carrier`
--
ALTER TABLE `ps_warehouse_carrier`
  ADD PRIMARY KEY (`id_warehouse`,`id_carrier`),
  ADD KEY `id_warehouse` (`id_warehouse`),
  ADD KEY `id_carrier` (`id_carrier`);

--
-- Indeksy dla tabeli `ps_warehouse_product_location`
--
ALTER TABLE `ps_warehouse_product_location`
  ADD PRIMARY KEY (`id_warehouse_product_location`),
  ADD UNIQUE KEY `id_product` (`id_product`,`id_product_attribute`,`id_warehouse`);

--
-- Indeksy dla tabeli `ps_warehouse_shop`
--
ALTER TABLE `ps_warehouse_shop`
  ADD PRIMARY KEY (`id_warehouse`,`id_shop`),
  ADD KEY `id_warehouse` (`id_warehouse`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indeksy dla tabeli `ps_webservice_account`
--
ALTER TABLE `ps_webservice_account`
  ADD PRIMARY KEY (`id_webservice_account`),
  ADD KEY `key` (`key`);

--
-- Indeksy dla tabeli `ps_webservice_account_shop`
--
ALTER TABLE `ps_webservice_account_shop`
  ADD PRIMARY KEY (`id_webservice_account`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- Indeksy dla tabeli `ps_webservice_permission`
--
ALTER TABLE `ps_webservice_permission`
  ADD PRIMARY KEY (`id_webservice_permission`),
  ADD UNIQUE KEY `resource_2` (`resource`,`method`,`id_webservice_account`),
  ADD KEY `resource` (`resource`),
  ADD KEY `method` (`method`),
  ADD KEY `id_webservice_account` (`id_webservice_account`);

--
-- Indeksy dla tabeli `ps_web_browser`
--
ALTER TABLE `ps_web_browser`
  ADD PRIMARY KEY (`id_web_browser`);

--
-- Indeksy dla tabeli `ps_zone`
--
ALTER TABLE `ps_zone`
  ADD PRIMARY KEY (`id_zone`);

--
-- Indeksy dla tabeli `ps_zone_shop`
--
ALTER TABLE `ps_zone_shop`
  ADD PRIMARY KEY (`id_zone`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `ps_address`
--
ALTER TABLE `ps_address`
  MODIFY `id_address` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT dla tabeli `ps_advice`
--
ALTER TABLE `ps_advice`
  MODIFY `id_advice` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_alias`
--
ALTER TABLE `ps_alias`
  MODIFY `id_alias` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `ps_attachment`
--
ALTER TABLE `ps_attachment`
  MODIFY `id_attachment` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_attachment_lang`
--
ALTER TABLE `ps_attachment_lang`
  MODIFY `id_attachment` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_attribute`
--
ALTER TABLE `ps_attribute`
  MODIFY `id_attribute` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT dla tabeli `ps_attribute_group`
--
ALTER TABLE `ps_attribute_group`
  MODIFY `id_attribute_group` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `ps_attribute_impact`
--
ALTER TABLE `ps_attribute_impact`
  MODIFY `id_attribute_impact` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_badge`
--
ALTER TABLE `ps_badge`
  MODIFY `id_badge` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_carrier`
--
ALTER TABLE `ps_carrier`
  MODIFY `id_carrier` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `ps_cart`
--
ALTER TABLE `ps_cart`
  MODIFY `id_cart` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT dla tabeli `ps_cart_rule`
--
ALTER TABLE `ps_cart_rule`
  MODIFY `id_cart_rule` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_cart_rule_product_rule`
--
ALTER TABLE `ps_cart_rule_product_rule`
  MODIFY `id_product_rule` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_cart_rule_product_rule_group`
--
ALTER TABLE `ps_cart_rule_product_rule_group`
  MODIFY `id_product_rule_group` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_category`
--
ALTER TABLE `ps_category`
  MODIFY `id_category` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT dla tabeli `ps_cms`
--
ALTER TABLE `ps_cms`
  MODIFY `id_cms` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT dla tabeli `ps_cms_block`
--
ALTER TABLE `ps_cms_block`
  MODIFY `id_cms_block` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `ps_cms_block_page`
--
ALTER TABLE `ps_cms_block_page`
  MODIFY `id_cms_block_page` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT dla tabeli `ps_cms_block_shop`
--
ALTER TABLE `ps_cms_block_shop`
  MODIFY `id_cms_block` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `ps_cms_category`
--
ALTER TABLE `ps_cms_category`
  MODIFY `id_cms_category` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `ps_cms_category_shop`
--
ALTER TABLE `ps_cms_category_shop`
  MODIFY `id_cms_category` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `ps_cms_role`
--
ALTER TABLE `ps_cms_role`
  MODIFY `id_cms_role` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_compare`
--
ALTER TABLE `ps_compare`
  MODIFY `id_compare` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_condition`
--
ALTER TABLE `ps_condition`
  MODIFY `id_condition` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_configuration`
--
ALTER TABLE `ps_configuration`
  MODIFY `id_configuration` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=373;

--
-- AUTO_INCREMENT dla tabeli `ps_configuration_kpi`
--
ALTER TABLE `ps_configuration_kpi`
  MODIFY `id_configuration_kpi` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT dla tabeli `ps_connections`
--
ALTER TABLE `ps_connections`
  MODIFY `id_connections` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT dla tabeli `ps_connections_source`
--
ALTER TABLE `ps_connections_source`
  MODIFY `id_connections_source` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_contact`
--
ALTER TABLE `ps_contact`
  MODIFY `id_contact` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `ps_country`
--
ALTER TABLE `ps_country`
  MODIFY `id_country` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=245;

--
-- AUTO_INCREMENT dla tabeli `ps_cronjobs`
--
ALTER TABLE `ps_cronjobs`
  MODIFY `id_cronjob` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_currency`
--
ALTER TABLE `ps_currency`
  MODIFY `id_currency` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `ps_customer`
--
ALTER TABLE `ps_customer`
  MODIFY `id_customer` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `ps_customer_message`
--
ALTER TABLE `ps_customer_message`
  MODIFY `id_customer_message` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_customer_thread`
--
ALTER TABLE `ps_customer_thread`
  MODIFY `id_customer_thread` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_customization`
--
ALTER TABLE `ps_customization`
  MODIFY `id_customization` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_customization_field`
--
ALTER TABLE `ps_customization_field`
  MODIFY `id_customization_field` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_date_range`
--
ALTER TABLE `ps_date_range`
  MODIFY `id_date_range` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_delivery`
--
ALTER TABLE `ps_delivery`
  MODIFY `id_delivery` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `ps_employee`
--
ALTER TABLE `ps_employee`
  MODIFY `id_employee` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `ps_feature`
--
ALTER TABLE `ps_feature`
  MODIFY `id_feature` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT dla tabeli `ps_feature_value`
--
ALTER TABLE `ps_feature_value`
  MODIFY `id_feature_value` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT dla tabeli `ps_ganalytics`
--
ALTER TABLE `ps_ganalytics`
  MODIFY `id_google_analytics` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `ps_gender`
--
ALTER TABLE `ps_gender`
  MODIFY `id_gender` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `ps_group`
--
ALTER TABLE `ps_group`
  MODIFY `id_group` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `ps_group_reduction`
--
ALTER TABLE `ps_group_reduction`
  MODIFY `id_group_reduction` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_guest`
--
ALTER TABLE `ps_guest`
  MODIFY `id_guest` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT dla tabeli `ps_homeslider`
--
ALTER TABLE `ps_homeslider`
  MODIFY `id_homeslider_slides` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT dla tabeli `ps_homeslider_slides`
--
ALTER TABLE `ps_homeslider_slides`
  MODIFY `id_homeslider_slides` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT dla tabeli `ps_hook`
--
ALTER TABLE `ps_hook`
  MODIFY `id_hook` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;

--
-- AUTO_INCREMENT dla tabeli `ps_hook_alias`
--
ALTER TABLE `ps_hook_alias`
  MODIFY `id_hook_alias` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT dla tabeli `ps_hook_module_exceptions`
--
ALTER TABLE `ps_hook_module_exceptions`
  MODIFY `id_hook_module_exceptions` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT dla tabeli `ps_image`
--
ALTER TABLE `ps_image`
  MODIFY `id_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=383;

--
-- AUTO_INCREMENT dla tabeli `ps_image_type`
--
ALTER TABLE `ps_image_type`
  MODIFY `id_image_type` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT dla tabeli `ps_import_match`
--
ALTER TABLE `ps_import_match`
  MODIFY `id_import_match` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_info`
--
ALTER TABLE `ps_info`
  MODIFY `id_info` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `ps_lang`
--
ALTER TABLE `ps_lang`
  MODIFY `id_lang` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `ps_layered_category`
--
ALTER TABLE `ps_layered_category`
  MODIFY `id_layered_category` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT dla tabeli `ps_layered_filter`
--
ALTER TABLE `ps_layered_filter`
  MODIFY `id_layered_filter` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `ps_layered_friendly_url`
--
ALTER TABLE `ps_layered_friendly_url`
  MODIFY `id_layered_friendly_url` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT dla tabeli `ps_linksmenutop`
--
ALTER TABLE `ps_linksmenutop`
  MODIFY `id_linksmenutop` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_log`
--
ALTER TABLE `ps_log`
  MODIFY `id_log` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT dla tabeli `ps_mail`
--
ALTER TABLE `ps_mail`
  MODIFY `id_mail` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_manufacturer`
--
ALTER TABLE `ps_manufacturer`
  MODIFY `id_manufacturer` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `ps_memcached_servers`
--
ALTER TABLE `ps_memcached_servers`
  MODIFY `id_memcached_server` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_message`
--
ALTER TABLE `ps_message`
  MODIFY `id_message` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_meta`
--
ALTER TABLE `ps_meta`
  MODIFY `id_meta` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT dla tabeli `ps_module`
--
ALTER TABLE `ps_module`
  MODIFY `id_module` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT dla tabeli `ps_modules_perfs`
--
ALTER TABLE `ps_modules_perfs`
  MODIFY `id_modules_perfs` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_module_preference`
--
ALTER TABLE `ps_module_preference`
  MODIFY `id_module_preference` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_newsletter`
--
ALTER TABLE `ps_newsletter`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_operating_system`
--
ALTER TABLE `ps_operating_system`
  MODIFY `id_operating_system` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT dla tabeli `ps_orders`
--
ALTER TABLE `ps_orders`
  MODIFY `id_order` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT dla tabeli `ps_order_carrier`
--
ALTER TABLE `ps_order_carrier`
  MODIFY `id_order_carrier` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT dla tabeli `ps_order_cart_rule`
--
ALTER TABLE `ps_order_cart_rule`
  MODIFY `id_order_cart_rule` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_order_detail`
--
ALTER TABLE `ps_order_detail`
  MODIFY `id_order_detail` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT dla tabeli `ps_order_history`
--
ALTER TABLE `ps_order_history`
  MODIFY `id_order_history` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT dla tabeli `ps_order_invoice`
--
ALTER TABLE `ps_order_invoice`
  MODIFY `id_order_invoice` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `ps_order_message`
--
ALTER TABLE `ps_order_message`
  MODIFY `id_order_message` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `ps_order_payment`
--
ALTER TABLE `ps_order_payment`
  MODIFY `id_order_payment` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `ps_order_return`
--
ALTER TABLE `ps_order_return`
  MODIFY `id_order_return` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_order_return_state`
--
ALTER TABLE `ps_order_return_state`
  MODIFY `id_order_return_state` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT dla tabeli `ps_order_slip`
--
ALTER TABLE `ps_order_slip`
  MODIFY `id_order_slip` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_order_state`
--
ALTER TABLE `ps_order_state`
  MODIFY `id_order_state` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT dla tabeli `ps_page`
--
ALTER TABLE `ps_page`
  MODIFY `id_page` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `ps_pagenotfound`
--
ALTER TABLE `ps_pagenotfound`
  MODIFY `id_pagenotfound` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_page_type`
--
ALTER TABLE `ps_page_type`
  MODIFY `id_page_type` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `ps_product`
--
ALTER TABLE `ps_product`
  MODIFY `id_product` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=383;

--
-- AUTO_INCREMENT dla tabeli `ps_product_attribute`
--
ALTER TABLE `ps_product_attribute`
  MODIFY `id_product_attribute` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_product_download`
--
ALTER TABLE `ps_product_download`
  MODIFY `id_product_download` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_product_supplier`
--
ALTER TABLE `ps_product_supplier`
  MODIFY `id_product_supplier` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_profile`
--
ALTER TABLE `ps_profile`
  MODIFY `id_profile` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `ps_quick_access`
--
ALTER TABLE `ps_quick_access`
  MODIFY `id_quick_access` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `ps_range_price`
--
ALTER TABLE `ps_range_price`
  MODIFY `id_range_price` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `ps_range_weight`
--
ALTER TABLE `ps_range_weight`
  MODIFY `id_range_weight` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `ps_referrer`
--
ALTER TABLE `ps_referrer`
  MODIFY `id_referrer` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_referrer_shop`
--
ALTER TABLE `ps_referrer_shop`
  MODIFY `id_referrer` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_request_sql`
--
ALTER TABLE `ps_request_sql`
  MODIFY `id_request_sql` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_required_field`
--
ALTER TABLE `ps_required_field`
  MODIFY `id_required_field` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_risk`
--
ALTER TABLE `ps_risk`
  MODIFY `id_risk` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `ps_scene`
--
ALTER TABLE `ps_scene`
  MODIFY `id_scene` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_search_engine`
--
ALTER TABLE `ps_search_engine`
  MODIFY `id_search_engine` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT dla tabeli `ps_search_word`
--
ALTER TABLE `ps_search_word`
  MODIFY `id_word` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8975;

--
-- AUTO_INCREMENT dla tabeli `ps_sekeyword`
--
ALTER TABLE `ps_sekeyword`
  MODIFY `id_sekeyword` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_shop`
--
ALTER TABLE `ps_shop`
  MODIFY `id_shop` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `ps_shop_group`
--
ALTER TABLE `ps_shop_group`
  MODIFY `id_shop_group` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `ps_shop_url`
--
ALTER TABLE `ps_shop_url`
  MODIFY `id_shop_url` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `ps_specific_price`
--
ALTER TABLE `ps_specific_price`
  MODIFY `id_specific_price` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT dla tabeli `ps_specific_price_priority`
--
ALTER TABLE `ps_specific_price_priority`
  MODIFY `id_specific_price_priority` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_specific_price_rule`
--
ALTER TABLE `ps_specific_price_rule`
  MODIFY `id_specific_price_rule` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_specific_price_rule_condition`
--
ALTER TABLE `ps_specific_price_rule_condition`
  MODIFY `id_specific_price_rule_condition` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_specific_price_rule_condition_group`
--
ALTER TABLE `ps_specific_price_rule_condition_group`
  MODIFY `id_specific_price_rule_condition_group` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_state`
--
ALTER TABLE `ps_state`
  MODIFY `id_state` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=313;

--
-- AUTO_INCREMENT dla tabeli `ps_statssearch`
--
ALTER TABLE `ps_statssearch`
  MODIFY `id_statssearch` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `ps_stock`
--
ALTER TABLE `ps_stock`
  MODIFY `id_stock` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_stock_available`
--
ALTER TABLE `ps_stock_available`
  MODIFY `id_stock_available` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=383;

--
-- AUTO_INCREMENT dla tabeli `ps_stock_mvt`
--
ALTER TABLE `ps_stock_mvt`
  MODIFY `id_stock_mvt` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_stock_mvt_reason`
--
ALTER TABLE `ps_stock_mvt_reason`
  MODIFY `id_stock_mvt_reason` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT dla tabeli `ps_store`
--
ALTER TABLE `ps_store`
  MODIFY `id_store` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT dla tabeli `ps_supplier`
--
ALTER TABLE `ps_supplier`
  MODIFY `id_supplier` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `ps_supply_order`
--
ALTER TABLE `ps_supply_order`
  MODIFY `id_supply_order` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_supply_order_detail`
--
ALTER TABLE `ps_supply_order_detail`
  MODIFY `id_supply_order_detail` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_supply_order_history`
--
ALTER TABLE `ps_supply_order_history`
  MODIFY `id_supply_order_history` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_supply_order_receipt_history`
--
ALTER TABLE `ps_supply_order_receipt_history`
  MODIFY `id_supply_order_receipt_history` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_supply_order_state`
--
ALTER TABLE `ps_supply_order_state`
  MODIFY `id_supply_order_state` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT dla tabeli `ps_tab`
--
ALTER TABLE `ps_tab`
  MODIFY `id_tab` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;

--
-- AUTO_INCREMENT dla tabeli `ps_tab_module_preference`
--
ALTER TABLE `ps_tab_module_preference`
  MODIFY `id_tab_module_preference` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_tag`
--
ALTER TABLE `ps_tag`
  MODIFY `id_tag` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_tax`
--
ALTER TABLE `ps_tax`
  MODIFY `id_tax` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT dla tabeli `ps_tax_rule`
--
ALTER TABLE `ps_tax_rule`
  MODIFY `id_tax_rule` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT dla tabeli `ps_tax_rules_group`
--
ALTER TABLE `ps_tax_rules_group`
  MODIFY `id_tax_rules_group` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT dla tabeli `ps_theme`
--
ALTER TABLE `ps_theme`
  MODIFY `id_theme` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `ps_themeconfigurator`
--
ALTER TABLE `ps_themeconfigurator`
  MODIFY `id_item` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT dla tabeli `ps_theme_meta`
--
ALTER TABLE `ps_theme_meta`
  MODIFY `id_theme_meta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT dla tabeli `ps_timezone`
--
ALTER TABLE `ps_timezone`
  MODIFY `id_timezone` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=561;

--
-- AUTO_INCREMENT dla tabeli `ps_warehouse`
--
ALTER TABLE `ps_warehouse`
  MODIFY `id_warehouse` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_warehouse_product_location`
--
ALTER TABLE `ps_warehouse_product_location`
  MODIFY `id_warehouse_product_location` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_webservice_account`
--
ALTER TABLE `ps_webservice_account`
  MODIFY `id_webservice_account` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_webservice_permission`
--
ALTER TABLE `ps_webservice_permission`
  MODIFY `id_webservice_permission` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ps_web_browser`
--
ALTER TABLE `ps_web_browser`
  MODIFY `id_web_browser` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT dla tabeli `ps_zone`
--
ALTER TABLE `ps_zone`
  MODIFY `id_zone` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
